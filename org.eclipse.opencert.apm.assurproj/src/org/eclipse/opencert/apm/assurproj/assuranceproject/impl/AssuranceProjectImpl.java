/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assurance Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getResponsible <em>Responsible</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getDate <em>Date</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getAssetsPackage <em>Assets Package</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getPermissionConf <em>Permission Conf</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getBaselineConfig <em>Baseline Config</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl#getSubProject <em>Sub Project</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssuranceProjectImpl extends DescribableElementImpl implements AssuranceProject {
	/**
	 * The default value of the '{@link #getCreatedBy() <em>Created By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreatedBy()
	 * @generated
	 * @ordered
	 */
	protected static final String CREATED_BY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getResponsible() <em>Responsible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsible()
	 * @generated
	 * @ordered
	 */
	protected static final String RESPONSIBLE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssuranceProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssuranceprojectPackage.Literals.ASSURANCE_PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCreatedBy() {
		return (String)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__CREATED_BY, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__CREATED_BY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreatedBy(String newCreatedBy) {
		eDynamicSet(AssuranceprojectPackage.ASSURANCE_PROJECT__CREATED_BY, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__CREATED_BY, newCreatedBy);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResponsible() {
		return (String)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__RESPONSIBLE, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__RESPONSIBLE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponsible(String newResponsible) {
		eDynamicSet(AssuranceprojectPackage.ASSURANCE_PROJECT__RESPONSIBLE, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__RESPONSIBLE, newResponsible);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDate() {
		return (Date)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__DATE, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__DATE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDate(Date newDate) {
		eDynamicSet(AssuranceprojectPackage.ASSURANCE_PROJECT__DATE, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__DATE, newDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return (String)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__VERSION, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__VERSION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		eDynamicSet(AssuranceprojectPackage.ASSURANCE_PROJECT__VERSION, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__VERSION, newVersion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssetsPackage> getAssetsPackage() {
		return (EList<AssetsPackage>)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__ASSETS_PACKAGE, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__ASSETS_PACKAGE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<PermissionConfig> getPermissionConf() {
		return (EList<PermissionConfig>)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__PERMISSION_CONF, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__PERMISSION_CONF, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaselineConfig> getBaselineConfig() {
		return (EList<BaselineConfig>)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__BASELINE_CONFIG, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__BASELINE_CONFIG, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceProject> getSubProject() {
		return (EList<AssuranceProject>)eDynamicGet(AssuranceprojectPackage.ASSURANCE_PROJECT__SUB_PROJECT, AssuranceprojectPackage.Literals.ASSURANCE_PROJECT__SUB_PROJECT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSURANCE_PROJECT__ASSETS_PACKAGE:
				return ((InternalEList<?>)getAssetsPackage()).basicRemove(otherEnd, msgs);
			case AssuranceprojectPackage.ASSURANCE_PROJECT__PERMISSION_CONF:
				return ((InternalEList<?>)getPermissionConf()).basicRemove(otherEnd, msgs);
			case AssuranceprojectPackage.ASSURANCE_PROJECT__BASELINE_CONFIG:
				return ((InternalEList<?>)getBaselineConfig()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSURANCE_PROJECT__CREATED_BY:
				return getCreatedBy();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__RESPONSIBLE:
				return getResponsible();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__DATE:
				return getDate();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__VERSION:
				return getVersion();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__ASSETS_PACKAGE:
				return getAssetsPackage();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__PERMISSION_CONF:
				return getPermissionConf();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__BASELINE_CONFIG:
				return getBaselineConfig();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__SUB_PROJECT:
				return getSubProject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSURANCE_PROJECT__CREATED_BY:
				setCreatedBy((String)newValue);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__RESPONSIBLE:
				setResponsible((String)newValue);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__DATE:
				setDate((Date)newValue);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__VERSION:
				setVersion((String)newValue);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__ASSETS_PACKAGE:
				getAssetsPackage().clear();
				getAssetsPackage().addAll((Collection<? extends AssetsPackage>)newValue);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__PERMISSION_CONF:
				getPermissionConf().clear();
				getPermissionConf().addAll((Collection<? extends PermissionConfig>)newValue);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__BASELINE_CONFIG:
				getBaselineConfig().clear();
				getBaselineConfig().addAll((Collection<? extends BaselineConfig>)newValue);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__SUB_PROJECT:
				getSubProject().clear();
				getSubProject().addAll((Collection<? extends AssuranceProject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSURANCE_PROJECT__CREATED_BY:
				setCreatedBy(CREATED_BY_EDEFAULT);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__RESPONSIBLE:
				setResponsible(RESPONSIBLE_EDEFAULT);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__DATE:
				setDate(DATE_EDEFAULT);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__ASSETS_PACKAGE:
				getAssetsPackage().clear();
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__PERMISSION_CONF:
				getPermissionConf().clear();
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__BASELINE_CONFIG:
				getBaselineConfig().clear();
				return;
			case AssuranceprojectPackage.ASSURANCE_PROJECT__SUB_PROJECT:
				getSubProject().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AssuranceprojectPackage.ASSURANCE_PROJECT__CREATED_BY:
				return CREATED_BY_EDEFAULT == null ? getCreatedBy() != null : !CREATED_BY_EDEFAULT.equals(getCreatedBy());
			case AssuranceprojectPackage.ASSURANCE_PROJECT__RESPONSIBLE:
				return RESPONSIBLE_EDEFAULT == null ? getResponsible() != null : !RESPONSIBLE_EDEFAULT.equals(getResponsible());
			case AssuranceprojectPackage.ASSURANCE_PROJECT__DATE:
				return DATE_EDEFAULT == null ? getDate() != null : !DATE_EDEFAULT.equals(getDate());
			case AssuranceprojectPackage.ASSURANCE_PROJECT__VERSION:
				return VERSION_EDEFAULT == null ? getVersion() != null : !VERSION_EDEFAULT.equals(getVersion());
			case AssuranceprojectPackage.ASSURANCE_PROJECT__ASSETS_PACKAGE:
				return !getAssetsPackage().isEmpty();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__PERMISSION_CONF:
				return !getPermissionConf().isEmpty();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__BASELINE_CONFIG:
				return !getBaselineConfig().isEmpty();
			case AssuranceprojectPackage.ASSURANCE_PROJECT__SUB_PROJECT:
				return !getSubProject().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssuranceProjectImpl
