/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assurance Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getResponsible <em>Responsible</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getDate <em>Date</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getVersion <em>Version</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getAssetsPackage <em>Assets Package</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getPermissionConf <em>Permission Conf</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getBaselineConfig <em>Baseline Config</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getSubProject <em>Sub Project</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject()
 * @model
 * @generated
 */
public interface AssuranceProject extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Created By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Created By</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Created By</em>' attribute.
	 * @see #setCreatedBy(String)
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_CreatedBy()
	 * @model
	 * @generated
	 */
	String getCreatedBy();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getCreatedBy <em>Created By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Created By</em>' attribute.
	 * @see #getCreatedBy()
	 * @generated
	 */
	void setCreatedBy(String value);

	/**
	 * Returns the value of the '<em><b>Responsible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible</em>' attribute.
	 * @see #setResponsible(String)
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_Responsible()
	 * @model
	 * @generated
	 */
	String getResponsible();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getResponsible <em>Responsible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible</em>' attribute.
	 * @see #getResponsible()
	 * @generated
	 */
	void setResponsible(String value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_Date()
	 * @model
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Assets Package</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assets Package</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assets Package</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_AssetsPackage()
	 * @model containment="true"
	 * @generated
	 */
	EList<AssetsPackage> getAssetsPackage();

	/**
	 * Returns the value of the '<em><b>Permission Conf</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Permission Conf</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permission Conf</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_PermissionConf()
	 * @model containment="true"
	 * @generated
	 */
	EList<PermissionConfig> getPermissionConf();

	/**
	 * Returns the value of the '<em><b>Baseline Config</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Baseline Config</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Baseline Config</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_BaselineConfig()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaselineConfig> getBaselineConfig();

	/**
	 * Returns the value of the '<em><b>Sub Project</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Project</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Project</em>' reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssuranceProject_SubProject()
	 * @model
	 * @generated
	 */
	EList<AssuranceProject> getSubProject();

} // AssuranceProject
