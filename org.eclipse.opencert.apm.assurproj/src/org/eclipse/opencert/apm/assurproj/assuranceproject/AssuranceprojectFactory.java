/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage
 * @generated
 */
public interface AssuranceprojectFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AssuranceprojectFactory eINSTANCE = org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceprojectFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Assurance Project</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assurance Project</em>'.
	 * @generated
	 */
	AssuranceProject createAssuranceProject();

	/**
	 * Returns a new object of class '<em>Permission Config</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Permission Config</em>'.
	 * @generated
	 */
	PermissionConfig createPermissionConfig();

	/**
	 * Returns a new object of class '<em>Assets Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assets Package</em>'.
	 * @generated
	 */
	AssetsPackage createAssetsPackage();

	/**
	 * Returns a new object of class '<em>Baseline Config</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Baseline Config</em>'.
	 * @generated
	 */
	BaselineConfig createBaselineConfig();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AssuranceprojectPackage getAssuranceprojectPackage();

} //AssuranceprojectFactory
