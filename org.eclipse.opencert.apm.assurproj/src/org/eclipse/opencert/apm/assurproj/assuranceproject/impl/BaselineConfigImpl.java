/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Baseline Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.BaselineConfigImpl#getComplianceMapGroup <em>Compliance Map Group</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.BaselineConfigImpl#isIsActive <em>Is Active</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.impl.BaselineConfigImpl#getRefFramework <em>Ref Framework</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaselineConfigImpl extends DescribableElementImpl implements BaselineConfig {
	/**
	 * The default value of the '{@link #isIsActive() <em>Is Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsActive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ACTIVE_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaselineConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssuranceprojectPackage.Literals.BASELINE_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapGroup getComplianceMapGroup() {
		return (MapGroup)eDynamicGet(AssuranceprojectPackage.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP, AssuranceprojectPackage.Literals.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapGroup basicGetComplianceMapGroup() {
		return (MapGroup)eDynamicGet(AssuranceprojectPackage.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP, AssuranceprojectPackage.Literals.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComplianceMapGroup(MapGroup newComplianceMapGroup) {
		eDynamicSet(AssuranceprojectPackage.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP, AssuranceprojectPackage.Literals.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP, newComplianceMapGroup);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsActive() {
		return (Boolean)eDynamicGet(AssuranceprojectPackage.BASELINE_CONFIG__IS_ACTIVE, AssuranceprojectPackage.Literals.BASELINE_CONFIG__IS_ACTIVE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsActive(boolean newIsActive) {
		eDynamicSet(AssuranceprojectPackage.BASELINE_CONFIG__IS_ACTIVE, AssuranceprojectPackage.Literals.BASELINE_CONFIG__IS_ACTIVE, newIsActive);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseFramework> getRefFramework() {
		return (EList<BaseFramework>)eDynamicGet(AssuranceprojectPackage.BASELINE_CONFIG__REF_FRAMEWORK, AssuranceprojectPackage.Literals.BASELINE_CONFIG__REF_FRAMEWORK, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AssuranceprojectPackage.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP:
				if (resolve) return getComplianceMapGroup();
				return basicGetComplianceMapGroup();
			case AssuranceprojectPackage.BASELINE_CONFIG__IS_ACTIVE:
				return isIsActive();
			case AssuranceprojectPackage.BASELINE_CONFIG__REF_FRAMEWORK:
				return getRefFramework();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AssuranceprojectPackage.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP:
				setComplianceMapGroup((MapGroup)newValue);
				return;
			case AssuranceprojectPackage.BASELINE_CONFIG__IS_ACTIVE:
				setIsActive((Boolean)newValue);
				return;
			case AssuranceprojectPackage.BASELINE_CONFIG__REF_FRAMEWORK:
				getRefFramework().clear();
				getRefFramework().addAll((Collection<? extends BaseFramework>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AssuranceprojectPackage.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP:
				setComplianceMapGroup((MapGroup)null);
				return;
			case AssuranceprojectPackage.BASELINE_CONFIG__IS_ACTIVE:
				setIsActive(IS_ACTIVE_EDEFAULT);
				return;
			case AssuranceprojectPackage.BASELINE_CONFIG__REF_FRAMEWORK:
				getRefFramework().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AssuranceprojectPackage.BASELINE_CONFIG__COMPLIANCE_MAP_GROUP:
				return basicGetComplianceMapGroup() != null;
			case AssuranceprojectPackage.BASELINE_CONFIG__IS_ACTIVE:
				return isIsActive() != IS_ACTIVE_EDEFAULT;
			case AssuranceprojectPackage.BASELINE_CONFIG__REF_FRAMEWORK:
				return !getRefFramework().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BaselineConfigImpl
