/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.sam.arg.arg.Case;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assets Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#isIsActive <em>Is Active</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getArtefactsModel <em>Artefacts Model</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getArgumentationModel <em>Argumentation Model</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#getProcessModel <em>Process Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssetsPackage()
 * @model
 * @generated
 */
public interface AssetsPackage extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Active</em>' attribute.
	 * @see #setIsActive(boolean)
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssetsPackage_IsActive()
	 * @model
	 * @generated
	 */
	boolean isIsActive();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage#isIsActive <em>Is Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Active</em>' attribute.
	 * @see #isIsActive()
	 * @generated
	 */
	void setIsActive(boolean value);

	/**
	 * Returns the value of the '<em><b>Artefacts Model</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Artefacts Model</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artefacts Model</em>' reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssetsPackage_ArtefactsModel()
	 * @model
	 * @generated
	 */
	EList<ArtefactModel> getArtefactsModel();

	/**
	 * Returns the value of the '<em><b>Argumentation Model</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Case}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argumentation Model</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argumentation Model</em>' reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssetsPackage_ArgumentationModel()
	 * @model
	 * @generated
	 */
	EList<Case> getArgumentationModel();

	/**
	 * Returns the value of the '<em><b>Process Model</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.ProcessModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Model</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Model</em>' reference list.
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#getAssetsPackage_ProcessModel()
	 * @model
	 * @generated
	 */
	EList<ProcessModel> getProcessModel();

} // AssetsPackage
