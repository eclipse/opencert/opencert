/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.etl;

public class EpfTransformParameters {

	private String pluginID;
	private String etlTransform;
	
	// Parameters for input models
	
	private String confName;
	private String confModelFilePath;
	private Boolean confReadOnLoad;
	private Boolean confStoreOnDisposal;
	//MCP
	private String libName;
	private String libModelFilePath;
	private boolean libReadOnLoad;
	private boolean libStoreOnDisposal;
	
	//CRF problem	
	private String epfMetamodelURI;
	private String epfMetamodelFilePath;
	
	//MCP
	//CRF problem	
	//private Object oldEPackage;
	private Object IoldEPackage;
	private Object OoldEPackage;	
	
	// Paremeters for ouput models
	// Process
	private String processName;
	private String processMetamodelURI;
	//CRF problem	
	private String processMetaModelFilePath;
	//CRF problem	
	private String processModelFilePath;
	private boolean processReadOnLoad;
	private boolean processStoreOnDisposal;
	// Artefact
	private String artName;
	private String artMetamodelURI;
	//CRF problem	
	private String artMetaModelFilePath;
	//CRF problem	
	private String artModelFilePath;
	private boolean artReadOnLoad;
	private boolean artStoreOnDisposal;
	

	public String getPluginID() {
		return pluginID;
	}

	public void setPluginID(String pluginID) {
		this.pluginID = pluginID;
	}

	public String getEtlTransform() {
		return etlTransform;
	}

	public void setEtlTransform(String etlTransform) {
		this.etlTransform = etlTransform;
	}

	public String getSourceName() {
		return libName;
	}

	public void setSourceName(String sourceName) {
		this.libName = sourceName;
	}

	public String getEPFMetaModelURI() {
		return epfMetamodelURI;
	}

	public void setEPFMetaModelURI(String sourceMetaModelURI) {
		this.epfMetamodelURI = sourceMetaModelURI;
	}


	public String getEPFMetaModelFilePath() {
		return epfMetamodelFilePath;
	}

	public void setEPFMetaModelFilePath(String sourceMetaModelFilePath) {
		this.epfMetamodelFilePath = sourceMetaModelFilePath;
	}
	
	public String getLibModelFilePath() {
		return libModelFilePath;
	}

	public void setLibModelFilePath(String sourceModelFilePath) {
		this.libModelFilePath = sourceModelFilePath;
	}

	public boolean isLibReadOnLoad() {
		return libReadOnLoad;
	}

	public void setLibReadOnLoad(boolean sourceReadOnLoad) {
		this.libReadOnLoad = sourceReadOnLoad;
	}

	public boolean isLibStoreOnDisposal() {
		return libStoreOnDisposal;
	}

	public void setLibStoreOnDisposal(boolean sourceStoreOnDisposal) {
		this.libStoreOnDisposal = sourceStoreOnDisposal;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String targetName) {
		this.processName = targetName;
	}

	public String getProcessMetamodelURI() {
		return processMetamodelURI;
	}

	public void setProcessMetaModelURI(String targetMetaModelURI) {
		this.processMetamodelURI = targetMetaModelURI;
	}

	public String getProcessMetamodelFilePath() {
		return processMetaModelFilePath;
	}

	public void setProcessMetaModelFilePath(String targetMetaModelFilePath) {
		this.processMetaModelFilePath = targetMetaModelFilePath;
	}
	
	
	public String getProcessModelFilePath() {
		return processModelFilePath;
	}

	public void setProcessModelFilePath(String targetModelFilePath) {
		this.processModelFilePath = targetModelFilePath;
	}

	public boolean isProcessReadOnLoad() {
		return processReadOnLoad;
	}

	public void setProcessReadOnLoad(boolean targetReadOnLoad) {
		this.processReadOnLoad = targetReadOnLoad;
	}

	public boolean isProcessStoreOnDisposal() {
		return processStoreOnDisposal;
	}

	public void setProcessStoreOnDisposal(boolean targetStoreOnDisposal) {
		this.processStoreOnDisposal = targetStoreOnDisposal;
	}

	public String getConfName() {
		return confName;
	}

	public void setConfName(String confName) {
		this.confName = confName;
	}

	public String getConfModelFilePath() {
		return confModelFilePath;
	}

	public void setConfModelFilePath(String confModelFilePath) {
		this.confModelFilePath = confModelFilePath;
	}

	public Boolean isConfReadOnLoad() {
		return confReadOnLoad;
	}

	public void setConfReadOnLoad(Boolean confReadOnLoad) {
		this.confReadOnLoad = confReadOnLoad;
	}

	public Boolean isConfStoreOnDisposal() {
		return confStoreOnDisposal;
	}

	public void setConfStoreOnDisposal(Boolean confStoreOnDisposal) {
		this.confStoreOnDisposal = confStoreOnDisposal;
	}

	public String getLibName() {
		return libName;
	}

	public void setLibName(String libName) {
		this.libName = libName;
	}

	public String getArtName() {
		return artName;
	}

	public void setArtName(String artName) {
		this.artName = artName;
	}

	public String getArtMetamodelURI() {
		return artMetamodelURI;
	}

	public void setArtMetamodelURI(String artMetamodelURI) {
		this.artMetamodelURI = artMetamodelURI;
	}

	public String getArtMetamodelFilePath() {
		return artMetaModelFilePath;
	}

	public void setArtMetaModelFilePath(String artMetaModelFilePath) {
		this.artMetaModelFilePath = artMetaModelFilePath;
	}

	public String getArtModelFilePath() {
		return artModelFilePath;
	}

	public void setArtModelFilePath(String artModelFilePath) {
		this.artModelFilePath = artModelFilePath;
	}

	public boolean isArtReadOnLoad() {
		return artReadOnLoad;
	}

	public void setArtReadOnLoad(boolean artReadOnLoad) {
		this.artReadOnLoad = artReadOnLoad;
	}

	public boolean isArtStoreOnDisposal() {
		return artStoreOnDisposal;
	}

	public void setArtStoreOnDisposal(boolean artStoreOnDisposal) {
		this.artStoreOnDisposal = artStoreOnDisposal;
	}
	
	

}
