/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts;

/**
 * 
 * 
 */
public class AssuranceprojectViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * AssuranceProject view descriptor
	 * 
	 */
	public static class AssuranceProject_ {
		public static class Properties {
	
			
			public static String id = "assuranceproject::AssuranceProject_::properties::id";
			
			
			public static String name = "assuranceproject::AssuranceProject_::properties::name";
			
			
			public static String description = "assuranceproject::AssuranceProject_::properties::description";
			
			
			public static String createdBy = "assuranceproject::AssuranceProject_::properties::createdBy";
			
			
			public static String responsible = "assuranceproject::AssuranceProject_::properties::responsible";
			
			
			public static String date = "assuranceproject::AssuranceProject_::properties::date";
			
			
			public static String version = "assuranceproject::AssuranceProject_::properties::version";
			
			
			public static String subProject = "assuranceproject::AssuranceProject_::properties::subProject";
			
	
		}
	
	}

	/**
	 * PermissionConfig view descriptor
	 * 
	 */
	public static class PermissionConfig {
		public static class Properties {
	
			
			public static String id = "assuranceproject::PermissionConfig::properties::id";
			
			
			public static String name = "assuranceproject::PermissionConfig::properties::name";
			
			
			public static String description = "assuranceproject::PermissionConfig::properties::description";
			
			
			public static String isActive = "assuranceproject::PermissionConfig::properties::isActive";
			
	
		}
	
	}

	/**
	 * AssetsPackage view descriptor
	 * 
	 */
	public static class AssetsPackage {
		public static class Properties {
	
			
			public static String id = "assuranceproject::AssetsPackage::properties::id";
			
			
			public static String name = "assuranceproject::AssetsPackage::properties::name";
			
			
			public static String description = "assuranceproject::AssetsPackage::properties::description";
			
			
			public static String isActive = "assuranceproject::AssetsPackage::properties::isActive";
			
			
			public static String artefactsModel = "assuranceproject::AssetsPackage::properties::artefactsModel";
			
			
			public static String argumentationModel = "assuranceproject::AssetsPackage::properties::argumentationModel";
			
			
			public static String processModel = "assuranceproject::AssetsPackage::properties::processModel";
			
	
		}
	
	}

	/**
	 * BaselineConfig view descriptor
	 * 
	 */
	public static class BaselineConfig {
		public static class Properties {
	
			
			public static String id = "assuranceproject::BaselineConfig::properties::id";
			
			
			public static String name = "assuranceproject::BaselineConfig::properties::name";
			
			
			public static String description = "assuranceproject::BaselineConfig::properties::description";
			
			
			public static String complianceMapGroup = "assuranceproject::BaselineConfig::properties::complianceMapGroup";
			
			
			public static String isActive = "assuranceproject::BaselineConfig::properties::isActive";
			
			
			public static String refFramework = "assuranceproject::BaselineConfig::properties::refFramework";
			
	
		}
	
	}

	/**
	 * AssuranceAssets view descriptor
	 * 
	 */
	public static class AssuranceAssets {
		public static class Properties {
	
			
			public static String assetsPackage = "assuranceproject::AssuranceAssets::properties::assetsPackage";
			
			
			public static String assetsPackageTable = "assuranceproject::AssuranceAssets::properties::assetsPackageTable";
			
	
		}
	
	}

	/**
	 * PermissionConfigurations view descriptor
	 * 
	 */
	public static class PermissionConfigurations {
		public static class Properties {
	
			
			public static String permissionConf = "assuranceproject::PermissionConfigurations::properties::permissionConf";
			
			
			public static String permissionConfTable = "assuranceproject::PermissionConfigurations::properties::permissionConfTable";
			
	
		}
	
	}

	/**
	 * ProjectBaselines view descriptor
	 * 
	 */
	public static class ProjectBaselines {
		public static class Properties {
	
			
			public static String baselineConfig = "assuranceproject::ProjectBaselines::properties::baselineConfig";
			
			
			public static String baselineConfigTable = "assuranceproject::ProjectBaselines::properties::baselineConfigTable";
			
	
		}
	
	}

}
