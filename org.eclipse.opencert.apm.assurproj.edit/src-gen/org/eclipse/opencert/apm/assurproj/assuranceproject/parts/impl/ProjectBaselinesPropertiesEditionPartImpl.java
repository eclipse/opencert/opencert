/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.providers.AssuranceprojectMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;

// End of user code

/**
 * 
 * 
 */
public class ProjectBaselinesPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ProjectBaselinesPropertiesEditionPart {

	protected ReferencesTable baselineConfig;
	protected List<ViewerFilter> baselineConfigBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> baselineConfigFilters = new ArrayList<ViewerFilter>();
	protected TableViewer baselineConfigTable;
	protected List<ViewerFilter> baselineConfigTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> baselineConfigTableFilters = new ArrayList<ViewerFilter>();
	protected Button addBaselineConfigTable;
	protected Button removeBaselineConfigTable;
	protected Button editBaselineConfigTable;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ProjectBaselinesPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence projectBaselinesStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = projectBaselinesStep.addStep(AssuranceprojectViewsRepository.ProjectBaselines.Properties.class);
		// Start IRR
		// propertiesStep.addStep(AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig);
		// End IRR
		propertiesStep.addStep(AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable);
		
		
		composer = new PartComposer(projectBaselinesStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceprojectViewsRepository.ProjectBaselines.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig) {
					return createBaselineConfigAdvancedTableComposition(parent);
				}
				if (key == AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable) {
					return createBaselineConfigTableTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(AssuranceprojectMessages.ProjectBaselinesPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createBaselineConfigAdvancedTableComposition(Composite parent) {
		this.baselineConfig = new ReferencesTable(getDescription(AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig, AssuranceprojectMessages.ProjectBaselinesPropertiesEditionPart_BaselineConfigLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				baselineConfig.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				baselineConfig.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				baselineConfig.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				baselineConfig.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.baselineConfigFilters) {
			this.baselineConfig.addFilter(filter);
		}
		this.baselineConfig.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig, AssuranceprojectViewsRepository.SWT_KIND));
		this.baselineConfig.createControls(parent);
		this.baselineConfig.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData baselineConfigData = new GridData(GridData.FILL_HORIZONTAL);
		baselineConfigData.horizontalSpan = 3;
		this.baselineConfig.setLayoutData(baselineConfigData);
		this.baselineConfig.setLowerBound(0);
		this.baselineConfig.setUpperBound(-1);
		baselineConfig.setID(AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig);
		baselineConfig.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createBaselineConfigAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createBaselineConfigTableTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableBaselineConfigTable = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableBaselineConfigTable.setHeaderVisible(true);
		GridData gdBaselineConfigTable = new GridData();
		gdBaselineConfigTable.grabExcessHorizontalSpace = true;
		gdBaselineConfigTable.horizontalAlignment = GridData.FILL;
		gdBaselineConfigTable.grabExcessVerticalSpace = true;
		gdBaselineConfigTable.verticalAlignment = GridData.FILL;
		tableBaselineConfigTable.setLayoutData(gdBaselineConfigTable);
		tableBaselineConfigTable.setLinesVisible(true);

		// Start of user code for columns definition for BaselineConfigTable
		// Start IRR
		// TableColumn name = new TableColumn(tableBaselineConfigTable,
		// SWT.NONE);
		// name.setWidth(80);
		//name.setText("Label"); //$NON-NLS-1$

		TableColumn name = new TableColumn(tableBaselineConfigTable, SWT.NONE);
		name.setWidth(80);
		name.setText("Name"); //$NON-NLS-1$

		TableColumn name1 = new TableColumn(tableBaselineConfigTable, SWT.NONE);
		name1.setWidth(150);
		name1.setText("Last Version"); //$NON-NLS-1$ 

		// End IRR
		// End of user code

		baselineConfigTable = new TableViewer(tableBaselineConfigTable);
		baselineConfigTable.setContentProvider(new ArrayContentProvider());
		baselineConfigTable.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
			// BaselineConfigTable
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */

				if (object instanceof EObject) {
					BaselineConfig baselineConfig = (BaselineConfig) object;
					switch (columnIndex) {
					case 0:
						return baselineConfig.getName();
					case 1:
						if (baselineConfig.isIsActive())
							return "Yes";
						else
							return "No";

					}
				}

				// End IRR

				return ""; //$NON-NLS-1$
			}

			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		baselineConfigTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (baselineConfigTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) baselineConfigTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						baselineConfigTable.refresh();
					}
				}
			}

		});
		GridData baselineConfigTableData = new GridData(GridData.FILL_HORIZONTAL);
		baselineConfigTableData.minimumHeight = 120;
		baselineConfigTableData.heightHint = 120;
		baselineConfigTable.getTable().setLayoutData(baselineConfigTableData);
		for (ViewerFilter filter : this.baselineConfigTableFilters) {
			baselineConfigTable.addFilter(filter);
		}
		EditingUtils.setID(baselineConfigTable.getTable(), AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable);
		EditingUtils.setEEFtype(baselineConfigTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createBaselineConfigTablePanel(tableContainer);
		// Start of user code for createBaselineConfigTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createBaselineConfigTablePanel(Composite container) {
		Composite baselineConfigTablePanel = new Composite(container, SWT.NONE);
		GridLayout baselineConfigTablePanelLayout = new GridLayout();
		baselineConfigTablePanelLayout.numColumns = 1;
		baselineConfigTablePanel.setLayout(baselineConfigTablePanelLayout);
		addBaselineConfigTable = new Button(baselineConfigTablePanel, SWT.NONE);
		addBaselineConfigTable.setText(AssuranceprojectMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addBaselineConfigTableData = new GridData(GridData.FILL_HORIZONTAL);
		addBaselineConfigTable.setLayoutData(addBaselineConfigTableData);
		addBaselineConfigTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				baselineConfigTable.refresh();
			}
		});
		EditingUtils.setID(addBaselineConfigTable, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable);
		EditingUtils.setEEFtype(addBaselineConfigTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeBaselineConfigTable = new Button(baselineConfigTablePanel, SWT.NONE);
		removeBaselineConfigTable.setText(AssuranceprojectMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeBaselineConfigTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeBaselineConfigTable.setLayoutData(removeBaselineConfigTableData);
		removeBaselineConfigTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (baselineConfigTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) baselineConfigTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						baselineConfigTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeBaselineConfigTable, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable);
		EditingUtils.setEEFtype(removeBaselineConfigTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editBaselineConfigTable = new Button(baselineConfigTablePanel, SWT.NONE);
		editBaselineConfigTable.setText(AssuranceprojectMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editBaselineConfigTableData = new GridData(GridData.FILL_HORIZONTAL);
		editBaselineConfigTable.setLayoutData(editBaselineConfigTableData);
		editBaselineConfigTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (baselineConfigTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) baselineConfigTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ProjectBaselinesPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						baselineConfigTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editBaselineConfigTable, AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable);
		EditingUtils.setEEFtype(editBaselineConfigTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createBaselineConfigTablePanel

		// End of user code
		return baselineConfigTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#initBaselineConfig(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initBaselineConfig(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		baselineConfig.setContentProvider(contentProvider);
		baselineConfig.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfig);
		if (eefElementEditorReadOnlyState && baselineConfig.isEnabled()) {
			baselineConfig.setEnabled(false);
			baselineConfig.setToolTipText(AssuranceprojectMessages.ProjectBaselines_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !baselineConfig.isEnabled()) {
			baselineConfig.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#updateBaselineConfig()
	 * 
	 */
	public void updateBaselineConfig() {
	baselineConfig.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#addFilterBaselineConfig(ViewerFilter filter)
	 * 
	 */
	public void addFilterToBaselineConfig(ViewerFilter filter) {
		baselineConfigFilters.add(filter);
		if (this.baselineConfig != null) {
			this.baselineConfig.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#addBusinessFilterBaselineConfig(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToBaselineConfig(ViewerFilter filter) {
		baselineConfigBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#isContainedInBaselineConfigTable(EObject element)
	 * 
	 */
	public boolean isContainedInBaselineConfigTable(EObject element) {
		return ((ReferencesTableSettings)baselineConfig.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#initBaselineConfigTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initBaselineConfigTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		baselineConfigTable.setContentProvider(contentProvider);
		baselineConfigTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.ProjectBaselines.Properties.baselineConfigTable);
		if (eefElementEditorReadOnlyState && baselineConfigTable.getTable().isEnabled()) {
			baselineConfigTable.getTable().setEnabled(false);
			baselineConfigTable.getTable().setToolTipText(AssuranceprojectMessages.ProjectBaselines_ReadOnly);
			addBaselineConfigTable.setEnabled(false);
			addBaselineConfigTable.setToolTipText(AssuranceprojectMessages.ProjectBaselines_ReadOnly);
			removeBaselineConfigTable.setEnabled(false);
			removeBaselineConfigTable.setToolTipText(AssuranceprojectMessages.ProjectBaselines_ReadOnly);
			editBaselineConfigTable.setEnabled(false);
			editBaselineConfigTable.setToolTipText(AssuranceprojectMessages.ProjectBaselines_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !baselineConfigTable.getTable().isEnabled()) {
			baselineConfigTable.getTable().setEnabled(true);
			addBaselineConfigTable.setEnabled(true);
			removeBaselineConfigTable.setEnabled(true);
			editBaselineConfigTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#updateBaselineConfigTable()
	 * 
	 */
	public void updateBaselineConfigTable() {
	baselineConfigTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#addFilterBaselineConfigTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToBaselineConfigTable(ViewerFilter filter) {
		baselineConfigTableFilters.add(filter);
		if (this.baselineConfigTable != null) {
			this.baselineConfigTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#addBusinessFilterBaselineConfigTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToBaselineConfigTable(ViewerFilter filter) {
		baselineConfigTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart#isContainedInBaselineConfigTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInBaselineConfigTableTable(EObject element) {
		return ((ReferencesTableSettings)baselineConfigTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceprojectMessages.ProjectBaselines_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
