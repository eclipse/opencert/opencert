/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface PermissionConfigurationsPropertiesEditionPart {



	/**
	 * Init the permissionConf
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initPermissionConf(ReferencesTableSettings settings);

	/**
	 * Update the permissionConf
	 * @param newValue the permissionConf to update
	 * 
	 */
	public void updatePermissionConf();

	/**
	 * Adds the given filter to the permissionConf edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToPermissionConf(ViewerFilter filter);

	/**
	 * Adds the given filter to the permissionConf edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToPermissionConf(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the permissionConf table
	 * 
	 */
	public boolean isContainedInPermissionConfTable(EObject element);




	/**
	 * Init the permissionConfTable
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initPermissionConfTable(ReferencesTableSettings settings);

	/**
	 * Update the permissionConfTable
	 * @param newValue the permissionConfTable to update
	 * 
	 */
	public void updatePermissionConfTable();

	/**
	 * Adds the given filter to the permissionConfTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToPermissionConfTable(ViewerFilter filter);

	/**
	 * Adds the given filter to the permissionConfTable edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToPermissionConfTable(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the permissionConfTable table
	 * 
	 */
	public boolean isContainedInPermissionConfTableTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
