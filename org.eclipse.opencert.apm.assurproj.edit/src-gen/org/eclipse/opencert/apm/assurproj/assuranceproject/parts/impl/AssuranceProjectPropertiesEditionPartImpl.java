/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.providers.AssuranceprojectMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

// End of user code

/**
 * 
 * 
 */
public class AssuranceProjectPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, AssuranceProjectPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text createdBy;
	protected Text responsible;
	protected Text date;
	protected Text version;
	protected ReferencesTable subProject;
	protected List<ViewerFilter> subProjectBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> subProjectFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssuranceProjectPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence assuranceProject_Step = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assuranceProject_Step.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.class);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version);
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject);
		
		
		composer = new PartComposer(assuranceProject_Step) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.id) {
					return createIdText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.name) {
					return createNameText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy) {
					return createCreatedByText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible) {
					return createResponsibleText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.date) {
					return createDateText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.version) {
					return createVersionText(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject) {
					return createSubProjectAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.id, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, AssuranceprojectViewsRepository.AssuranceProject_.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.name, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, AssuranceprojectViewsRepository.AssuranceProject_.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.description, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, AssuranceprojectViewsRepository.AssuranceProject_.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createCreatedByText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_CreatedByLabel);
		createdBy = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData createdByData = new GridData(GridData.FILL_HORIZONTAL);
		createdBy.setLayoutData(createdByData);
		createdBy.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, createdBy.getText()));
			}

		});
		createdBy.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, createdBy.getText()));
				}
			}

		});
		EditingUtils.setID(createdBy, AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy);
		EditingUtils.setEEFtype(createdBy, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createCreatedByText

		// End of user code
		return parent;
	}

	
	protected Composite createResponsibleText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_ResponsibleLabel);
		responsible = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData responsibleData = new GridData(GridData.FILL_HORIZONTAL);
		responsible.setLayoutData(responsibleData);
		responsible.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, responsible.getText()));
			}

		});
		responsible.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, responsible.getText()));
				}
			}

		});
		EditingUtils.setID(responsible, AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible);
		EditingUtils.setEEFtype(responsible, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createResponsibleText

		// End of user code
		return parent;
	}

	
	protected Composite createDateText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.date, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_DateLabel);
		date = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData dateData = new GridData(GridData.FILL_HORIZONTAL);
		date.setLayoutData(dateData);
		date.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.date, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, date.getText()));
			}

		});
		date.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.date, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, date.getText()));
				}
			}

		});
		EditingUtils.setID(date, AssuranceprojectViewsRepository.AssuranceProject_.Properties.date);
		EditingUtils.setEEFtype(date, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDateText

		// End of user code
		return parent;
	}

	
	protected Composite createVersionText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.AssuranceProject_.Properties.version, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_VersionLabel);
		version = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData versionData = new GridData(GridData.FILL_HORIZONTAL);
		version.setLayoutData(versionData);
		version.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.version, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, version.getText()));
			}

		});
		version.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.version, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, version.getText()));
				}
			}

		});
		EditingUtils.setID(version, AssuranceprojectViewsRepository.AssuranceProject_.Properties.version);
		EditingUtils.setEEFtype(version, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createVersionText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createSubProjectAdvancedReferencesTable(Composite parent) {
		String label = getDescription(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, AssuranceprojectMessages.AssuranceProjectPropertiesEditionPart_SubProjectLabel);		 
		this.subProject = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addSubProject(); }
			public void handleEdit(EObject element) { editSubProject(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveSubProject(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromSubProject(element); }
			public void navigateTo(EObject element) { }
		});
		this.subProject.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, AssuranceprojectViewsRepository.SWT_KIND));
		this.subProject.createControls(parent);
		this.subProject.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData subProjectData = new GridData(GridData.FILL_HORIZONTAL);
		subProjectData.horizontalSpan = 3;
		this.subProject.setLayoutData(subProjectData);
		this.subProject.disableMove();
		subProject.setID(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject);
		subProject.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addSubProject() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(subProject.getInput(), subProjectFilters, subProjectBusinessFilters,
		"subProject", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				subProject.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveSubProject(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		subProject.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromSubProject(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceProjectPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		subProject.refresh();
	}

	/**
	 * 
	 */
	protected void editSubProject(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				subProject.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getCreatedBy()
	 * 
	 */
	public String getCreatedBy() {
		return createdBy.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setCreatedBy(String newValue)
	 * 
	 */
	public void setCreatedBy(String newValue) {
		if (newValue != null) {
			createdBy.setText(newValue);
		} else {
			createdBy.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy);
		if (eefElementEditorReadOnlyState && createdBy.isEnabled()) {
			createdBy.setEnabled(false);
			createdBy.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !createdBy.isEnabled()) {
			createdBy.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getResponsible()
	 * 
	 */
	public String getResponsible() {
		return responsible.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setResponsible(String newValue)
	 * 
	 */
	public void setResponsible(String newValue) {
		if (newValue != null) {
			responsible.setText(newValue);
		} else {
			responsible.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible);
		if (eefElementEditorReadOnlyState && responsible.isEnabled()) {
			responsible.setEnabled(false);
			responsible.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !responsible.isEnabled()) {
			responsible.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getDate()
	 * 
	 */
	public String getDate() {
		return date.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setDate(String newValue)
	 * 
	 */
	public void setDate(String newValue) {
		if (newValue != null) {
			date.setText(newValue);
		} else {
			date.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date);
		if (eefElementEditorReadOnlyState && date.isEnabled()) {
			date.setEnabled(false);
			date.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !date.isEnabled()) {
			date.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#getVersion()
	 * 
	 */
	public String getVersion() {
		return version.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#setVersion(String newValue)
	 * 
	 */
	public void setVersion(String newValue) {
		if (newValue != null) {
			version.setText(newValue);
		} else {
			version.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version);
		if (eefElementEditorReadOnlyState && version.isEnabled()) {
			version.setEnabled(false);
			version.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !version.isEnabled()) {
			version.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#initSubProject(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initSubProject(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		subProject.setContentProvider(contentProvider);
		subProject.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject);
		if (eefElementEditorReadOnlyState && subProject.getTable().isEnabled()) {
			subProject.setEnabled(false);
			subProject.setToolTipText(AssuranceprojectMessages.AssuranceProject_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !subProject.getTable().isEnabled()) {
			subProject.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#updateSubProject()
	 * 
	 */
	public void updateSubProject() {
	subProject.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#addFilterSubProject(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSubProject(ViewerFilter filter) {
		subProjectFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#addBusinessFilterSubProject(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSubProject(ViewerFilter filter) {
		subProjectBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart#isContainedInSubProjectTable(EObject element)
	 * 
	 */
	public boolean isContainedInSubProjectTable(EObject element) {
		return ((ReferencesTableSettings)subProject.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceprojectMessages.AssuranceProject_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
