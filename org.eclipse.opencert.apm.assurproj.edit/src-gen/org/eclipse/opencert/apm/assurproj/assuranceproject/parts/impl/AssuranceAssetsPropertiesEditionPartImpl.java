/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.providers.AssuranceprojectMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;

// End of user code

/**
 * 
 * 
 */
public class AssuranceAssetsPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, AssuranceAssetsPropertiesEditionPart {

	protected ReferencesTable assetsPackage;
	protected List<ViewerFilter> assetsPackageBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> assetsPackageFilters = new ArrayList<ViewerFilter>();
	protected TableViewer assetsPackageTable;
	protected List<ViewerFilter> assetsPackageTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> assetsPackageTableFilters = new ArrayList<ViewerFilter>();
	protected Button addAssetsPackageTable;
	protected Button removeAssetsPackageTable;
	protected Button editAssetsPackageTable;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssuranceAssetsPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence assuranceAssetsStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assuranceAssetsStep.addStep(AssuranceprojectViewsRepository.AssuranceAssets.Properties.class);
		// Start IRR
		// propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage);
		// End IRR
		propertiesStep.addStep(AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable);
		
		
		composer = new PartComposer(assuranceAssetsStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceprojectViewsRepository.AssuranceAssets.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage) {
					return createAssetsPackageAdvancedTableComposition(parent);
				}
				if (key == AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable) {
					return createAssetsPackageTableTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(AssuranceprojectMessages.AssuranceAssetsPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createAssetsPackageAdvancedTableComposition(Composite parent) {
		this.assetsPackage = new ReferencesTable(getDescription(AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage, AssuranceprojectMessages.AssuranceAssetsPropertiesEditionPart_AssetsPackageLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				assetsPackage.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				assetsPackage.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				assetsPackage.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				assetsPackage.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.assetsPackageFilters) {
			this.assetsPackage.addFilter(filter);
		}
		this.assetsPackage.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage, AssuranceprojectViewsRepository.SWT_KIND));
		this.assetsPackage.createControls(parent);
		this.assetsPackage.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData assetsPackageData = new GridData(GridData.FILL_HORIZONTAL);
		assetsPackageData.horizontalSpan = 3;
		this.assetsPackage.setLayoutData(assetsPackageData);
		this.assetsPackage.setLowerBound(0);
		this.assetsPackage.setUpperBound(-1);
		assetsPackage.setID(AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage);
		assetsPackage.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createAssetsPackageAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createAssetsPackageTableTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableAssetsPackageTable = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableAssetsPackageTable.setHeaderVisible(true);
		GridData gdAssetsPackageTable = new GridData();
		gdAssetsPackageTable.grabExcessHorizontalSpace = true;
		gdAssetsPackageTable.horizontalAlignment = GridData.FILL;
		gdAssetsPackageTable.grabExcessVerticalSpace = true;
		gdAssetsPackageTable.verticalAlignment = GridData.FILL;
		tableAssetsPackageTable.setLayoutData(gdAssetsPackageTable);
		tableAssetsPackageTable.setLinesVisible(true);

		// Start of user code for columns definition for AssetsPackageTable
		// Start IRR
		// TableColumn name = new TableColumn(tableAssetsPackageTable,
		// SWT.NONE);
		// name.setWidth(80);
		//name.setText("Label"); //$NON-NLS-1$

		TableColumn name = new TableColumn(tableAssetsPackageTable, SWT.NONE);
		name.setWidth(80);
		name.setText("Name"); //$NON-NLS-1$

		TableColumn name1 = new TableColumn(tableAssetsPackageTable, SWT.NONE);
		name1.setWidth(150);
		name1.setText("Is Active"); //$NON-NLS-1$ 

		// End IRR
		// End of user code

		assetsPackageTable = new TableViewer(tableAssetsPackageTable);
		assetsPackageTable.setContentProvider(new ArrayContentProvider());
		assetsPackageTable.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
			// AssetsPackageTable
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */

				if (object instanceof EObject) {
					AssetsPackage assetsPackage = (AssetsPackage) object;
					switch (columnIndex) {
					case 0:
						return assetsPackage.getName();
					case 1:
						if (assetsPackage.isIsActive())
							return "Yes";
						else
							return "No";

					}
				}

				// End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		assetsPackageTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (assetsPackageTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) assetsPackageTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						assetsPackageTable.refresh();
					}
				}
			}

		});
		GridData assetsPackageTableData = new GridData(GridData.FILL_HORIZONTAL);
		assetsPackageTableData.minimumHeight = 120;
		assetsPackageTableData.heightHint = 120;
		assetsPackageTable.getTable().setLayoutData(assetsPackageTableData);
		for (ViewerFilter filter : this.assetsPackageTableFilters) {
			assetsPackageTable.addFilter(filter);
		}
		EditingUtils.setID(assetsPackageTable.getTable(), AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable);
		EditingUtils.setEEFtype(assetsPackageTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createAssetsPackageTablePanel(tableContainer);
		// Start of user code for createAssetsPackageTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createAssetsPackageTablePanel(Composite container) {
		Composite assetsPackageTablePanel = new Composite(container, SWT.NONE);
		GridLayout assetsPackageTablePanelLayout = new GridLayout();
		assetsPackageTablePanelLayout.numColumns = 1;
		assetsPackageTablePanel.setLayout(assetsPackageTablePanelLayout);
		addAssetsPackageTable = new Button(assetsPackageTablePanel, SWT.NONE);
		addAssetsPackageTable.setText(AssuranceprojectMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addAssetsPackageTableData = new GridData(GridData.FILL_HORIZONTAL);
		addAssetsPackageTable.setLayoutData(addAssetsPackageTableData);
		addAssetsPackageTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				assetsPackageTable.refresh();
			}
		});
		EditingUtils.setID(addAssetsPackageTable, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable);
		EditingUtils.setEEFtype(addAssetsPackageTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeAssetsPackageTable = new Button(assetsPackageTablePanel, SWT.NONE);
		removeAssetsPackageTable.setText(AssuranceprojectMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeAssetsPackageTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeAssetsPackageTable.setLayoutData(removeAssetsPackageTableData);
		removeAssetsPackageTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (assetsPackageTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) assetsPackageTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						assetsPackageTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeAssetsPackageTable, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable);
		EditingUtils.setEEFtype(removeAssetsPackageTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editAssetsPackageTable = new Button(assetsPackageTablePanel, SWT.NONE);
		editAssetsPackageTable.setText(AssuranceprojectMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editAssetsPackageTableData = new GridData(GridData.FILL_HORIZONTAL);
		editAssetsPackageTable.setLayoutData(editAssetsPackageTableData);
		editAssetsPackageTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (assetsPackageTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) assetsPackageTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						assetsPackageTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editAssetsPackageTable, AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable);
		EditingUtils.setEEFtype(editAssetsPackageTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createAssetsPackageTablePanel

		// End of user code
		return assetsPackageTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#initAssetsPackage(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initAssetsPackage(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		assetsPackage.setContentProvider(contentProvider);
		assetsPackage.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackage);
		if (eefElementEditorReadOnlyState && assetsPackage.isEnabled()) {
			assetsPackage.setEnabled(false);
			assetsPackage.setToolTipText(AssuranceprojectMessages.AssuranceAssets_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !assetsPackage.isEnabled()) {
			assetsPackage.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#updateAssetsPackage()
	 * 
	 */
	public void updateAssetsPackage() {
	assetsPackage.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#addFilterAssetsPackage(ViewerFilter filter)
	 * 
	 */
	public void addFilterToAssetsPackage(ViewerFilter filter) {
		assetsPackageFilters.add(filter);
		if (this.assetsPackage != null) {
			this.assetsPackage.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#addBusinessFilterAssetsPackage(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToAssetsPackage(ViewerFilter filter) {
		assetsPackageBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#isContainedInAssetsPackageTable(EObject element)
	 * 
	 */
	public boolean isContainedInAssetsPackageTable(EObject element) {
		return ((ReferencesTableSettings)assetsPackage.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#initAssetsPackageTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initAssetsPackageTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		assetsPackageTable.setContentProvider(contentProvider);
		assetsPackageTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.AssuranceAssets.Properties.assetsPackageTable);
		if (eefElementEditorReadOnlyState && assetsPackageTable.getTable().isEnabled()) {
			assetsPackageTable.getTable().setEnabled(false);
			assetsPackageTable.getTable().setToolTipText(AssuranceprojectMessages.AssuranceAssets_ReadOnly);
			addAssetsPackageTable.setEnabled(false);
			addAssetsPackageTable.setToolTipText(AssuranceprojectMessages.AssuranceAssets_ReadOnly);
			removeAssetsPackageTable.setEnabled(false);
			removeAssetsPackageTable.setToolTipText(AssuranceprojectMessages.AssuranceAssets_ReadOnly);
			editAssetsPackageTable.setEnabled(false);
			editAssetsPackageTable.setToolTipText(AssuranceprojectMessages.AssuranceAssets_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !assetsPackageTable.getTable().isEnabled()) {
			assetsPackageTable.getTable().setEnabled(true);
			addAssetsPackageTable.setEnabled(true);
			removeAssetsPackageTable.setEnabled(true);
			editAssetsPackageTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#updateAssetsPackageTable()
	 * 
	 */
	public void updateAssetsPackageTable() {
	assetsPackageTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#addFilterAssetsPackageTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToAssetsPackageTable(ViewerFilter filter) {
		assetsPackageTableFilters.add(filter);
		if (this.assetsPackageTable != null) {
			this.assetsPackageTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#addBusinessFilterAssetsPackageTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToAssetsPackageTable(ViewerFilter filter) {
		assetsPackageTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart#isContainedInAssetsPackageTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInAssetsPackageTableTable(EObject element) {
		return ((ReferencesTableSettings)assetsPackageTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceprojectMessages.AssuranceAssets_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
