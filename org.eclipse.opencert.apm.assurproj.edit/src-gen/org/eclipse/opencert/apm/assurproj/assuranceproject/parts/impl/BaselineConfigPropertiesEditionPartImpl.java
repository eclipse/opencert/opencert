/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.providers.AssuranceprojectMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

// End of user code

/**
 * 
 * 
 */
public class BaselineConfigPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaselineConfigPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected EObjectFlatComboViewer complianceMapGroup;
	protected Button isActive;
	protected ReferencesTable refFramework;
	protected List<ViewerFilter> refFrameworkBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> refFrameworkFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaselineConfigPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baselineConfigStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baselineConfigStep.addStep(AssuranceprojectViewsRepository.BaselineConfig.Properties.class);
		propertiesStep.addStep(AssuranceprojectViewsRepository.BaselineConfig.Properties.id);
		propertiesStep.addStep(AssuranceprojectViewsRepository.BaselineConfig.Properties.name);
		propertiesStep.addStep(AssuranceprojectViewsRepository.BaselineConfig.Properties.description);
		propertiesStep.addStep(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup);
		propertiesStep.addStep(AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive);
		propertiesStep.addStep(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework);
		
		
		composer = new PartComposer(baselineConfigStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceprojectViewsRepository.BaselineConfig.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == AssuranceprojectViewsRepository.BaselineConfig.Properties.id) {
					return createIdText(parent);
				}
				if (key == AssuranceprojectViewsRepository.BaselineConfig.Properties.name) {
					return createNameText(parent);
				}
				if (key == AssuranceprojectViewsRepository.BaselineConfig.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup) {
					return createComplianceMapGroupFlatComboViewer(parent);
				}
				if (key == AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive) {
					return createIsActiveCheckbox(parent);
				}
				if (key == AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework) {
					return createRefFrameworkAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(AssuranceprojectMessages.BaselineConfigPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.BaselineConfig.Properties.id, AssuranceprojectMessages.BaselineConfigPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, AssuranceprojectViewsRepository.BaselineConfig.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.BaselineConfig.Properties.id, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.BaselineConfig.Properties.name, AssuranceprojectMessages.BaselineConfigPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, AssuranceprojectViewsRepository.BaselineConfig.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.BaselineConfig.Properties.name, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.BaselineConfig.Properties.description, AssuranceprojectMessages.BaselineConfigPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, AssuranceprojectViewsRepository.BaselineConfig.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.BaselineConfig.Properties.description, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createComplianceMapGroupFlatComboViewer(Composite parent) {
		createDescription(parent, AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup, AssuranceprojectMessages.BaselineConfigPropertiesEditionPart_ComplianceMapGroupLabel);
		complianceMapGroup = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup, AssuranceprojectViewsRepository.SWT_KIND));
		complianceMapGroup.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		complianceMapGroup.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getComplianceMapGroup()));
			}

		});
		GridData complianceMapGroupData = new GridData(GridData.FILL_HORIZONTAL);
		complianceMapGroup.setLayoutData(complianceMapGroupData);
		complianceMapGroup.setID(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createComplianceMapGroupFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createIsActiveCheckbox(Composite parent) {
		isActive = new Button(parent, SWT.CHECK);
		isActive.setText(getDescription(AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive, AssuranceprojectMessages.BaselineConfigPropertiesEditionPart_IsActiveLabel));
		isActive.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(isActive.getSelection())));
			}

		});
		GridData isActiveData = new GridData(GridData.FILL_HORIZONTAL);
		isActiveData.horizontalSpan = 2;
		isActive.setLayoutData(isActiveData);
		EditingUtils.setID(isActive, AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive);
		EditingUtils.setEEFtype(isActive, "eef::Checkbox"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive, AssuranceprojectViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIsActiveCheckbox

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createRefFrameworkAdvancedReferencesTable(Composite parent) {
		String label = getDescription(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework, AssuranceprojectMessages.BaselineConfigPropertiesEditionPart_RefFrameworkLabel);		 
		this.refFramework = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addRefFramework(); }
			public void handleEdit(EObject element) { editRefFramework(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveRefFramework(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromRefFramework(element); }
			public void navigateTo(EObject element) { }
		});
		this.refFramework.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework, AssuranceprojectViewsRepository.SWT_KIND));
		this.refFramework.createControls(parent);
		this.refFramework.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData refFrameworkData = new GridData(GridData.FILL_HORIZONTAL);
		refFrameworkData.horizontalSpan = 3;
		this.refFramework.setLayoutData(refFrameworkData);
		this.refFramework.disableMove();
		refFramework.setID(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework);
		refFramework.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addRefFramework() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(refFramework.getInput(), refFrameworkFilters, refFrameworkBusinessFilters,
		"refFramework", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				refFramework.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveRefFramework(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		refFramework.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromRefFramework(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaselineConfigPropertiesEditionPartImpl.this, AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		refFramework.refresh();
	}

	/**
	 * 
	 */
	protected void editRefFramework(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				refFramework.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.BaselineConfig.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(AssuranceprojectMessages.BaselineConfig_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.BaselineConfig.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(AssuranceprojectMessages.BaselineConfig_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.BaselineConfig.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(AssuranceprojectMessages.BaselineConfig_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#getComplianceMapGroup()
	 * 
	 */
	public EObject getComplianceMapGroup() {
		if (complianceMapGroup.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) complianceMapGroup.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#initComplianceMapGroup(EObjectFlatComboSettings)
	 */
	public void initComplianceMapGroup(EObjectFlatComboSettings settings) {
		complianceMapGroup.setInput(settings);
		if (current != null) {
			complianceMapGroup.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup);
		if (eefElementEditorReadOnlyState && complianceMapGroup.isEnabled()) {
			complianceMapGroup.setEnabled(false);
			complianceMapGroup.setToolTipText(AssuranceprojectMessages.BaselineConfig_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !complianceMapGroup.isEnabled()) {
			complianceMapGroup.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#setComplianceMapGroup(EObject newValue)
	 * 
	 */
	public void setComplianceMapGroup(EObject newValue) {
		if (newValue != null) {
			complianceMapGroup.setSelection(new StructuredSelection(newValue));
		} else {
			complianceMapGroup.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.BaselineConfig.Properties.complianceMapGroup);
		if (eefElementEditorReadOnlyState && complianceMapGroup.isEnabled()) {
			complianceMapGroup.setEnabled(false);
			complianceMapGroup.setToolTipText(AssuranceprojectMessages.BaselineConfig_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !complianceMapGroup.isEnabled()) {
			complianceMapGroup.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#setComplianceMapGroupButtonMode(ButtonsModeEnum newValue)
	 */
	public void setComplianceMapGroupButtonMode(ButtonsModeEnum newValue) {
		complianceMapGroup.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#addFilterComplianceMapGroup(ViewerFilter filter)
	 * 
	 */
	public void addFilterToComplianceMapGroup(ViewerFilter filter) {
		complianceMapGroup.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#addBusinessFilterComplianceMapGroup(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToComplianceMapGroup(ViewerFilter filter) {
		complianceMapGroup.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#getIsActive()
	 * 
	 */
	public Boolean getIsActive() {
		return Boolean.valueOf(isActive.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#setIsActive(Boolean newValue)
	 * 
	 */
	public void setIsActive(Boolean newValue) {
		if (newValue != null) {
			isActive.setSelection(newValue.booleanValue());
		} else {
			isActive.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.BaselineConfig.Properties.isActive);
		if (eefElementEditorReadOnlyState && isActive.isEnabled()) {
			isActive.setEnabled(false);
			isActive.setToolTipText(AssuranceprojectMessages.BaselineConfig_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !isActive.isEnabled()) {
			isActive.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#initRefFramework(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initRefFramework(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		refFramework.setContentProvider(contentProvider);
		refFramework.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceprojectViewsRepository.BaselineConfig.Properties.refFramework);
		if (eefElementEditorReadOnlyState && refFramework.getTable().isEnabled()) {
			refFramework.setEnabled(false);
			refFramework.setToolTipText(AssuranceprojectMessages.BaselineConfig_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !refFramework.getTable().isEnabled()) {
			refFramework.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#updateRefFramework()
	 * 
	 */
	public void updateRefFramework() {
	refFramework.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#addFilterRefFramework(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRefFramework(ViewerFilter filter) {
		refFrameworkFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#addBusinessFilterRefFramework(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRefFramework(ViewerFilter filter) {
		refFrameworkBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.parts.BaselineConfigPropertiesEditionPart#isContainedInRefFrameworkTable(EObject element)
	 * 
	 */
	public boolean isContainedInRefFrameworkTable(EObject element) {
		return ((ReferencesTableSettings)refFramework.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceprojectMessages.BaselineConfig_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
