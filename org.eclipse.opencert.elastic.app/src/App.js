/*-------------------------------------------------------------------------------
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 *------------------------------------------------------------------------------*/
import React, { Component } from 'react';
import {
  ActionBar,
  ActionBarRow,
  HierarchicalMenuFilter,
//  HitItem,
  Hits,
  HitsStats,
  Layout,
  LayoutBody,
  LayoutResults,
  NoHits,
  Pagination,
  PageSizeSelector,
  RefinementListFilter,
  ResetFilters,
  SearchkitComponent,
  SearchkitManager,
  SearchkitProvider,
  SearchBox,
  SelectedFilters,
  SideBar,
  Toggle,
  TopBar
} from 'searchkit';
import logo from './logo.svg';
import './App.css';
import * as _ from 'lodash';

// bind searchkit to a FIXED local address
const searchkit = new SearchkitManager("http://localhost:9200/amass-test")

const HitItem = (props) => (
  <div className="hit">
    <div className="hit__header">
      <a href={props.result._source.uri} title="Drag to your running medini analyze instance.">
      {props.result._source.label}
      </a>
    </div>
    <div className="hit__body">
      <table className="hit__hits">
        <tbody>
        {
          _.toPairs(props.result.highlight).map(
            function(pair, index) {
              return (
                <tr key={index}>
                  <td className="hit__highlight__key">{pair[0]}</td>
                  <td className="hit__highlight__value" dangerouslySetInnerHTML={{__html: pair[1]}}/>
                </tr>
              )
            }
          )
        }
        </tbody>
      </table>
    </div>
  </div>
)

class App extends Component {
  render() {
    return (
      <SearchkitProvider searchkit={searchkit}>
        <Layout>
          <TopBar>
            <SearchBox
              autofocus={true}
              searchOnChange={true}
              prefixQueryFields={["type^2","name^10", "description^10"]}/>
          </TopBar>
          <LayoutBody>
            <SideBar>
              <HierarchicalMenuFilter
                fields={["_type", "_score"]}
                title="Types"
                id="types"/>
              <RefinementListFilter
                id="storage"
                title="Storage"
                field="uriKind.keyword"
                operator="AND"
                size={10}/>
            </SideBar>
            <LayoutResults>
              <ActionBar>
                <ActionBarRow>
                  <HitsStats/>
                </ActionBarRow>
                <ActionBarRow>
                  <SelectedFilters/>
                  <ResetFilters/>
                </ActionBarRow>
              </ActionBar>
              <Hits
                mod="sk-hits-list"
                hitsPerPage={10}
                itemComponent={HitItem}
                // the following fields will be highlighted by ES
                highlightFields={[ "description", "label" ]}
                sourceFilter={[ "_type", "_id", "name", "label", "description", "uri" ]} />
              <NoHits/>
            </LayoutResults>
          </LayoutBody>
        </Layout>
      </SearchkitProvider>
    );
  }
}

export default App;
