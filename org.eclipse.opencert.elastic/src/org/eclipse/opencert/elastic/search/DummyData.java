/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * XXX Dummy hack - TO BE REMOVED LATER
 * 
 * @author mauersberger
 */
class DummyData {

	static DummyData INSTANCE = new DummyData();

	@SuppressWarnings("unchecked")
	Collection<Object> convert(Object data) {
		// some EMF support first
		if (data instanceof Resource) {
			data = ((Resource) data).getAllContents();
		} else if (data instanceof ResourceSet) {
			data = ((ResourceSet) data).getAllContents();
		} else if (data instanceof EObject) {
			data = ((EObject) data).eAllContents();
		}
		
		//
		if (data instanceof Iterator) {
			Iterator<Object> it = (Iterator<Object>) data;
			List<Object> collector = new ArrayList<>();
			while (it.hasNext()) {
				collector.add(it.next());
			}
			return collector;
		} else if (data instanceof Iterable) {
			List<Object> collector = new ArrayList<>();
			for (Object object : (Iterable<Object>) data) {
				collector.add(object);
			}
			return collector;
		} else if (data instanceof Collection) {
			return (Collection<Object>) data;
		} else if (data instanceof Object[]) {
			return Arrays.asList((Object[]) data);
		} else {
			return null;
		}
	}

	String asDocument(Object object) {
		return object.toString();
	}

	String getId(String document) {
		return DigestUtils.md5Hex(document);
	}
}
