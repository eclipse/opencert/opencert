/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.text.MessageFormat;

/**
 * Simple console for all Elastic code.
 *
 * @author mauersberger
 */
public interface ElasticConsole {

	/**
	 * @param any
	 */
	void log(Object any);

	/**
	 * @param format
	 * @param arguments
	 * @see MessageFormat
	 */
	void info(String format, Object... arguments);

	/**
	 * @param format
	 * @param arguments
	 * @see MessageFormat
	 */
	public void log(String format, Object... arguments);

	/**
	 * @param format
	 * @param arguments
	 * @see MessageFormat
	 */
	public void error(String format, Object... arguments);
}
