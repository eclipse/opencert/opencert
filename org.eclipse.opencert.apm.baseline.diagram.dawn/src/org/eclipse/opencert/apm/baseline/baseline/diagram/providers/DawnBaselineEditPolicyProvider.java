/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.baseline.baseline.diagram.providers;

import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.DawnBaseFrameworkCanonicalEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditorPlugin;

import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.services.editpolicy.CreateEditPoliciesOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.editpolicy.IEditPolicyProvider;

public class DawnBaselineEditPolicyProvider extends AbstractProvider
		implements
			IEditPolicyProvider {
	public static String ID = "org.eclipse.opencert.apm.baseline.baseline.diagram.providers.DawnBaselineEditPolicyProvider";

	public boolean provides(IOperation operation) {
		if (operation instanceof CreateEditPoliciesOperation) {
			CreateEditPoliciesOperation editPoliciesOperation = (CreateEditPoliciesOperation) operation;
			if (editPoliciesOperation.getEditPart() instanceof BaseFrameworkEditPart) {
				return true;
			}
		}
		return false;
	}

	public void createEditPolicies(EditPart editPart) {
		if (editPart instanceof BaseFrameworkEditPart) {
			editPart.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
					new DawnBaseFrameworkCanonicalEditPolicy());
		}
	}
}
