/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceassetViewsRepository;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.forms.AssuranceAssetEvaluationPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.forms.AssuranceAssetEventPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.forms.AssuranceAssetsModelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.forms.ManageableAssuranceAssetPropertiesEditionPartForm;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.impl.AssuranceAssetEvaluationPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.impl.AssuranceAssetEventPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.impl.AssuranceAssetsModelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.impl.ManageableAssuranceAssetPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class AssuranceassetPropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == AssuranceassetViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == AssuranceassetViewsRepository.AssuranceAssetsModel.class) {
			if (kind == AssuranceassetViewsRepository.SWT_KIND)
				return new AssuranceAssetsModelPropertiesEditionPartImpl(component);
			if (kind == AssuranceassetViewsRepository.FORM_KIND)
				return new AssuranceAssetsModelPropertiesEditionPartForm(component);
		}
		if (key == AssuranceassetViewsRepository.ManageableAssuranceAsset.class) {
			if (kind == AssuranceassetViewsRepository.SWT_KIND)
				return new ManageableAssuranceAssetPropertiesEditionPartImpl(component);
			if (kind == AssuranceassetViewsRepository.FORM_KIND)
				return new ManageableAssuranceAssetPropertiesEditionPartForm(component);
		}
		if (key == AssuranceassetViewsRepository.AssuranceAssetEvent.class) {
			if (kind == AssuranceassetViewsRepository.SWT_KIND)
				return new AssuranceAssetEventPropertiesEditionPartImpl(component);
			if (kind == AssuranceassetViewsRepository.FORM_KIND)
				return new AssuranceAssetEventPropertiesEditionPartForm(component);
		}
		if (key == AssuranceassetViewsRepository.AssuranceAssetEvaluation.class) {
			if (kind == AssuranceassetViewsRepository.SWT_KIND)
				return new AssuranceAssetEvaluationPropertiesEditionPartImpl(component);
			if (kind == AssuranceassetViewsRepository.FORM_KIND)
				return new AssuranceAssetEvaluationPropertiesEditionPartForm(component);
		}
		return null;
	}

}
