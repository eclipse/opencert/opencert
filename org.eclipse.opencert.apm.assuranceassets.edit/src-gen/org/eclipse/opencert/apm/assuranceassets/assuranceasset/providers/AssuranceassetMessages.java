/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class AssuranceassetMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers.assuranceassetMessages"; //$NON-NLS-1$

	
	public static String AssuranceAssetsModelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ManageableAssuranceAssetPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssuranceAssetEventPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String AssuranceAssetsModel_ReadOnly;

	
	public static String AssuranceAssetsModel_Part_Title;

	
	public static String ManageableAssuranceAsset_ReadOnly;

	
	public static String ManageableAssuranceAsset_Part_Title;

	
	public static String AssuranceAssetEvent_ReadOnly;

	
	public static String AssuranceAssetEvent_Part_Title;

	
	public static String AssuranceAssetEvaluation_ReadOnly;

	
	public static String AssuranceAssetEvaluation_Part_Title;


	
	public static String AssuranceAssetsModelPropertiesEditionPart_IdLabel;

	
	public static String AssuranceAssetsModelPropertiesEditionPart_NameLabel;

	
	public static String AssuranceAssetsModelPropertiesEditionPart_DescriptionLabel;

	
	public static String AssuranceAssetsModelPropertiesEditionPart_AssuranceAssetLabel;

	
	public static String ManageableAssuranceAssetPropertiesEditionPart_EvaluationLabel;

	
	public static String ManageableAssuranceAssetPropertiesEditionPart_LifecycleEventLabel;

	
	public static String AssuranceAssetEventPropertiesEditionPart_IdLabel;

	
	public static String AssuranceAssetEventPropertiesEditionPart_NameLabel;

	
	public static String AssuranceAssetEventPropertiesEditionPart_DescriptionLabel;

	
	public static String AssuranceAssetEventPropertiesEditionPart_ResultingEvaluationLabel;

	
	public static String AssuranceAssetEventPropertiesEditionPart_TypeLabel;

	
	public static String AssuranceAssetEventPropertiesEditionPart_TimeLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_IdLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_NameLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_EvaluationLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_LifecycleEventLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_CriterionLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_CriterionDescriptionLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_EvaluationResultLabel;

	
	public static String AssuranceAssetEvaluationPropertiesEditionPart_RationaleLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, AssuranceassetMessages.class);
	}

	
	private AssuranceassetMessages() {
		//protect instanciation
	}
}
