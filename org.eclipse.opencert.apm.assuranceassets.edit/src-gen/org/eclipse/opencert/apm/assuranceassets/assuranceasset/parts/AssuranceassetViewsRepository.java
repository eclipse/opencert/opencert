/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts;

/**
 * 
 * 
 */
public class AssuranceassetViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * AssuranceAssetsModel view descriptor
	 * 
	 */
	public static class AssuranceAssetsModel {
		public static class Properties {
	
			
			public static String id = "assuranceasset::AssuranceAssetsModel::properties::id";
			
			
			public static String name = "assuranceasset::AssuranceAssetsModel::properties::name";
			
			
			public static String description = "assuranceasset::AssuranceAssetsModel::properties::description";
			
			
			public static String assuranceAsset_ = "assuranceasset::AssuranceAssetsModel::properties::AssuranceAsset_";
			
	
		}
	
	}

	/**
	 * ManageableAssuranceAsset view descriptor
	 * 
	 */
	public static class ManageableAssuranceAsset {
		public static class Properties {
	
			
			public static String evaluation = "assuranceasset::ManageableAssuranceAsset::properties::evaluation";
			
			
			public static String lifecycleEvent = "assuranceasset::ManageableAssuranceAsset::properties::lifecycleEvent";
			
	
		}
	
	}

	/**
	 * AssuranceAssetEvent view descriptor
	 * 
	 */
	public static class AssuranceAssetEvent {
		public static class Properties {
	
			
			public static String id = "assuranceasset::AssuranceAssetEvent::properties::id";
			
			
			public static String name = "assuranceasset::AssuranceAssetEvent::properties::name";
			
			
			public static String description = "assuranceasset::AssuranceAssetEvent::properties::description";
			
			
			public static String resultingEvaluation = "assuranceasset::AssuranceAssetEvent::properties::resultingEvaluation";
			
			
			public static String type = "assuranceasset::AssuranceAssetEvent::properties::type";
			
			
			public static String time = "assuranceasset::AssuranceAssetEvent::properties::time";
			
	
		}
	
	}

	/**
	 * AssuranceAssetEvaluation view descriptor
	 * 
	 */
	public static class AssuranceAssetEvaluation {
		public static class Properties {
	
			
			public static String id = "assuranceasset::AssuranceAssetEvaluation::properties::id";
			
			
			public static String name = "assuranceasset::AssuranceAssetEvaluation::properties::name";
			
			
			public static String evaluation = "assuranceasset::AssuranceAssetEvaluation::properties::evaluation";
			
			
			public static String lifecycleEvent = "assuranceasset::AssuranceAssetEvaluation::properties::lifecycleEvent";
			
			
			public static String criterion = "assuranceasset::AssuranceAssetEvaluation::properties::criterion";
			
			
			public static String criterionDescription = "assuranceasset::AssuranceAssetEvaluation::properties::criterionDescription";
			
			
			public static String evaluationResult = "assuranceasset::AssuranceAssetEvaluation::properties::evaluationResult";
			
			
			public static String rationale = "assuranceasset::AssuranceAssetEvaluation::properties::rationale";
			
	
		}
	
	}

}
