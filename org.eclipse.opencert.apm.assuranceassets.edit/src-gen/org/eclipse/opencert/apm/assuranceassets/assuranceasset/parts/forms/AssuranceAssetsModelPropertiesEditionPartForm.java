/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceassetViewsRepository;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers.AssuranceassetMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

// End of user code

/**
 * 
 * 
 */
public class AssuranceAssetsModelPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, AssuranceAssetsModelPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected ReferencesTable assuranceAsset;
	protected List<ViewerFilter> assuranceAssetBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> assuranceAssetFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public AssuranceAssetsModelPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public AssuranceAssetsModelPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence assuranceAssetsModelStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = assuranceAssetsModelStep.addStep(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.class);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description);
		propertiesStep.addStep(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_);
		
		
		composer = new PartComposer(assuranceAssetsModelStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_) {
					return createAssuranceAssetTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(AssuranceassetMessages.AssuranceAssetsModelPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id, AssuranceassetMessages.AssuranceAssetsModelPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetsModelPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetsModelPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetsModelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name, AssuranceassetMessages.AssuranceAssetsModelPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetsModelPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetsModelPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetsModelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description, AssuranceassetMessages.AssuranceAssetsModelPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							AssuranceAssetsModelPropertiesEditionPartForm.this,
							AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetsModelPropertiesEditionPartForm.this,
									AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									AssuranceAssetsModelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description, AssuranceassetViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createAssuranceAssetTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.assuranceAsset = new ReferencesTable(getDescription(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_, AssuranceassetMessages.AssuranceAssetsModelPropertiesEditionPart_AssuranceAssetLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				assuranceAsset.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				assuranceAsset.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				assuranceAsset.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				assuranceAsset.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.assuranceAssetFilters) {
			this.assuranceAsset.addFilter(filter);
		}
		this.assuranceAsset.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_, AssuranceassetViewsRepository.FORM_KIND));
		this.assuranceAsset.createControls(parent, widgetFactory);
		this.assuranceAsset.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(AssuranceAssetsModelPropertiesEditionPartForm.this, AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData assuranceAssetData = new GridData(GridData.FILL_HORIZONTAL);
		assuranceAssetData.horizontalSpan = 3;
		this.assuranceAsset.setLayoutData(assuranceAssetData);
		this.assuranceAsset.setLowerBound(0);
		this.assuranceAsset.setUpperBound(-1);
		assuranceAsset.setID(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_);
		assuranceAsset.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createAssuranceAssetTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(AssuranceassetMessages.AssuranceAssetsModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(AssuranceassetMessages.AssuranceAssetsModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(AssuranceassetMessages.AssuranceAssetsModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#initAssuranceAsset(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initAssuranceAsset(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		assuranceAsset.setContentProvider(contentProvider);
		assuranceAsset.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.AssuranceAssetsModel.Properties.assuranceAsset_);
		if (eefElementEditorReadOnlyState && assuranceAsset.isEnabled()) {
			assuranceAsset.setEnabled(false);
			assuranceAsset.setToolTipText(AssuranceassetMessages.AssuranceAssetsModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !assuranceAsset.isEnabled()) {
			assuranceAsset.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#updateAssuranceAsset()
	 * 
	 */
	public void updateAssuranceAsset() {
	assuranceAsset.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#addFilterAssuranceAsset(ViewerFilter filter)
	 * 
	 */
	public void addFilterToAssuranceAsset(ViewerFilter filter) {
		assuranceAssetFilters.add(filter);
		if (this.assuranceAsset != null) {
			this.assuranceAsset.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#addBusinessFilterAssuranceAsset(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToAssuranceAsset(ViewerFilter filter) {
		assuranceAssetBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetsModelPropertiesEditionPart#isContainedInAssuranceAssetTable(EObject element)
	 * 
	 */
	public boolean isContainedInAssuranceAssetTable(EObject element) {
		return ((ReferencesTableSettings)assuranceAsset.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceassetMessages.AssuranceAssetsModel_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
