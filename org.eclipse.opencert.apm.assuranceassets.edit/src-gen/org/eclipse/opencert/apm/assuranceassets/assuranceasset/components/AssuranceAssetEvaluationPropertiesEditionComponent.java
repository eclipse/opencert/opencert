/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceAssetEvaluationPropertiesEditionPart;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceassetViewsRepository;
import org.eclipse.opencert.infra.general.general.GeneralPackage;


// End of user code

/**
 * 
 * 
 */
public class AssuranceAssetEvaluationPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for evaluation ReferencesTable
	 */
	protected ReferencesTableSettings evaluationSettings;
	
	/**
	 * Settings for lifecycleEvent ReferencesTable
	 */
	protected ReferencesTableSettings lifecycleEventSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public AssuranceAssetEvaluationPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject assuranceAssetEvaluation, String editing_mode) {
		super(editingContext, assuranceAssetEvaluation, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = AssuranceassetViewsRepository.class;
		partKey = AssuranceassetViewsRepository.AssuranceAssetEvaluation.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final AssuranceAssetEvaluation assuranceAssetEvaluation = (AssuranceAssetEvaluation)elt;
			final AssuranceAssetEvaluationPropertiesEditionPart basePart = (AssuranceAssetEvaluationPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetEvaluation.getId()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetEvaluation.getName()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation)) {
				evaluationSettings = new ReferencesTableSettings(assuranceAssetEvaluation, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation());
				basePart.initEvaluation(evaluationSettings);
			}
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent)) {
				lifecycleEventSettings = new ReferencesTableSettings(assuranceAssetEvaluation, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent());
				basePart.initLifecycleEvent(lifecycleEventSettings);
			}
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion))
				basePart.setCriterion(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetEvaluation.getCriterion()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription))
				basePart.setCriterionDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetEvaluation.getCriterionDescription()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult))
				basePart.setEvaluationResult(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetEvaluation.getEvaluationResult()));
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale))
				basePart.setRationale(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceAssetEvaluation.getRationale()));
			
			// init filters
			
			
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation)) {
				basePart.addFilterToEvaluation(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvaluation); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for evaluation
				// End of user code
			}
			if (isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent)) {
				basePart.addFilterToLifecycleEvent(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvent); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for lifecycleEvent
				// End of user code
			}
			
			
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}











	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion) {
			return AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Criterion();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription) {
			return AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_CriterionDescription();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult) {
			return AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_EvaluationResult();
		}
		if (editorKey == AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale) {
			return AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Rationale();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		AssuranceAssetEvaluation assuranceAssetEvaluation = (AssuranceAssetEvaluation)semanticObject;
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id == event.getAffectedEditor()) {
			assuranceAssetEvaluation.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name == event.getAffectedEditor()) {
			assuranceAssetEvaluation.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, evaluationSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				evaluationSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				evaluationSettings.move(event.getNewIndex(), (AssuranceAssetEvaluation) event.getNewValue());
			}
		}
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, lifecycleEventSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				lifecycleEventSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				lifecycleEventSettings.move(event.getNewIndex(), (AssuranceAssetEvent) event.getNewValue());
			}
		}
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion == event.getAffectedEditor()) {
			assuranceAssetEvaluation.setCriterion((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription == event.getAffectedEditor()) {
			assuranceAssetEvaluation.setCriterionDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult == event.getAffectedEditor()) {
			assuranceAssetEvaluation.setEvaluationResult((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale == event.getAffectedEditor()) {
			assuranceAssetEvaluation.setRationale((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			AssuranceAssetEvaluationPropertiesEditionPart basePart = (AssuranceAssetEvaluationPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation().equals(msg.getFeature()) && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluation))
				basePart.updateEvaluation();
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent().equals(msg.getFeature()) && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.lifecycleEvent))
				basePart.updateLifecycleEvent();
			if (AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Criterion().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion)) {
				if (msg.getNewValue() != null) {
					basePart.setCriterion(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setCriterion("");
				}
			}
			if (AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_CriterionDescription().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription)) {
				if (msg.getNewValue() != null) {
					basePart.setCriterionDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setCriterionDescription("");
				}
			}
			if (AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_EvaluationResult().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult)) {
				if (msg.getNewValue() != null) {
					basePart.setEvaluationResult(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setEvaluationResult("");
				}
			}
			if (AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Rationale().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale)) {
				if (msg.getNewValue() != null) {
					basePart.setRationale(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRationale("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_Evaluation(),
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent(),
			AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Criterion(),
			AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_CriterionDescription(),
			AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_EvaluationResult(),
			AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Rationale()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterion == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Criterion().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Criterion().getEAttributeType(), newValue);
				}
				if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.criterionDescription == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_CriterionDescription().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_CriterionDescription().getEAttributeType(), newValue);
				}
				if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.evaluationResult == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_EvaluationResult().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_EvaluationResult().getEAttributeType(), newValue);
				}
				if (AssuranceassetViewsRepository.AssuranceAssetEvaluation.Properties.rationale == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Rationale().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceassetPackage.eINSTANCE.getAssuranceAssetEvaluation_Rationale().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
