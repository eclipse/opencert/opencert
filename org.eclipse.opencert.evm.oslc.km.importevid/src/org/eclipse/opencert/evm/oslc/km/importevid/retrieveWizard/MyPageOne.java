/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.retrieveWizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class MyPageOne extends WizardPage {
	
    private Text fileSelector;
    private Composite container;

    public MyPageOne() {
        super("OSLC KM - Retrieval Page");
        setTitle("Select the Model file to look for similar indexed models");
        setDescription("The Model file will be compared against the OSLC-KM repository to look for similar models");
    }

    @Override
    public void createControl(Composite parent) {
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        container = new Composite(parent, SWT.NONE);
        container.setLayout(layout);
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("Model file:");

        fileSelector = new Text(container, SWT.BORDER | SWT.SINGLE);
        fileSelector.setText("");
        fileSelector.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (!fileSelector.getText().isEmpty()) {
                    setPageComplete(true);
                }
            }

        });
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        fileSelector.setLayoutData(gd);
        /*
        Label labelCheck = new Label(container, SWT.NONE);
        labelCheck.setText("Select a Papyrus file:");
        FileDialog check = new FileDialog(getShell());
        */
        // required to avoid an error in the system
        setControl(container);
        setPageComplete(false);

    }

    public String getFileFullPath() {
        return fileSelector.getText();
    }
}