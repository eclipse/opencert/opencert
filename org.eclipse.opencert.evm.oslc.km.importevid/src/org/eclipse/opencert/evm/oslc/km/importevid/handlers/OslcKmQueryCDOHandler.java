/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.handlers;

import java.io.InputStreamReader;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.common.id.CDOIDUtil;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.opencert.evm.oslc.km.importevid.indexWizard.MyPageOne;
import org.eclipse.opencert.evm.oslc.km.importevid.indexWizard.MyWizard;
import org.eclipse.opencert.evm.oslc.km.importevid.wizard.ProjectSelector;

public class OslcKmQueryCDOHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		// Build up a query to invoke OSLC-KM service, so that we can recover CDOObjects
		
		// CDO session management
		CDOSession cdoSession = CDOConnectionUtil.instance.getCurrentSession();		
		
		// Running Query and getting results
		List<CDOObject> results = RunQuery("SL-C 1", cdoSession);

        return results;
	}
	
	public static List<CDOObject> RunQuery(String query, CDOSession cdoSession) {
		String queryResult = "";		
		try {			
			// Service invoke
			queryResult = MyPageOne.getSimilarRequirementsInOslcKmRepository(query);			
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Results for query '" + query + "': " + queryResult);
		
		CDOView viewCDO = CDOConnectionUtil.instance.openView(cdoSession);
		List<CDOObject> cdoObjects = new ArrayList<CDOObject>();
		List<String> idsFromJson = null;
		if (queryResult != null && queryResult != "") {
			idsFromJson = MyPageOne.GetObjectsFromJson(queryResult);
			if (idsFromJson != null && idsFromJson.size() > 0) {
				
				CDOObject cdoObject = null;
				CDOID cdoID = null;
				String cdoIdAsString = null;
				for (int i = 0; i < idsFromJson.size(); i++) {
					cdoIdAsString = idsFromJson.get(i);
					if (cdoIdAsString != null) {						
						cdoObject = resolveCDOObject(cdoIdAsString, viewCDO);
						if (cdoObject != null && !cdoObjects.contains(cdoObject)) {
							cdoObjects.add(cdoObject);
						}
					}
				}	
			}
		}
		return cdoObjects;
	}
	
	private static CDOObject resolveCDOObject(String objectId, Object context) {
		  // the only context we can handle at the moment is a CDOView
		  if (context instanceof CDOView) {
		   try {
			    CDOID id = CDOIDUtil.read(objectId);
			    CDOView view = (CDOView) context;
			    CDOObject object = view.getObject(id, true);
			    return object;
		   } catch (Exception e) {
		    // ignore
			   int i = 2;
			   e.printStackTrace();
		   }
		  }

		  return null;
		 }
}
