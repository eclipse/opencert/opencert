/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.indexWizard;


import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class MyPageTwo extends WizardPage {
	
    private Text oslcKmWebServiceUri;
    private Composite container;
    private String _defaultUri;

    public MyPageTwo(String defaultUri) {
        super("OSLC-KM Web Service Setup");
        _defaultUri = defaultUri;
        setTitle("OSLC-KM Web Service Setup");
        setDescription("Now the uri for the OSLC-KM Web Service must be provided");
        setControl(oslcKmWebServiceUri);
    }

    @Override
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("OSLC-KM Web Service Uri:");

        oslcKmWebServiceUri = new Text(container, SWT.BORDER | SWT.SINGLE);
        oslcKmWebServiceUri.setText(_defaultUri);
        oslcKmWebServiceUri.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                // TODO Auto-generated method stub
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (!oslcKmWebServiceUri.getText().isEmpty()) {
                    setPageComplete(true);
                }
            }

        });
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        oslcKmWebServiceUri.setLayoutData(gd);

        // required to avoid an error in the system
        setControl(container);
        setPageComplete(!oslcKmWebServiceUri.getText().isEmpty());
    }

    public String getOslcKmWebServiceUri() {
        return oslcKmWebServiceUri.getText();
    }
    
}