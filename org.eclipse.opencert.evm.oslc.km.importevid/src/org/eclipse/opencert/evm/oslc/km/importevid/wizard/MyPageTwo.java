/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.wizard;


import java.util.ArrayList;

import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class MyPageTwo extends WizardPage {
	
	// Literals in the wizard
    private final String PageTitle = "AMASS Certification Project Selection";
    private final String PageDescription = "Select an AMASS Certification Project and a new name to store the new evidences";
    private final String SelectedCertificationProjectLabelText = "Selected Certification Project:";
    private final String EvidenceNameLabelText = "Evidence name:";
    private final String SearchButtonText = "...";
	
    private Text selectedCertificationProjectTextBox;
    private Text evidenceNameTextBox;
    private Composite container;
    private CDOSession session;
    
    public MyPageTwo(CDOSession session) {
        super("AMASS Certification Project Selection");
        this.session = session;
        setTitle(PageTitle);
        setDescription(PageDescription);
        setControl(selectedCertificationProjectTextBox);
    }

    @Override
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 3;
        // Selected Certification Project
        Label selectedCertificationProjectLabel = new Label(container, SWT.NONE);
        selectedCertificationProjectLabel.setText(SelectedCertificationProjectLabelText);

        selectedCertificationProjectTextBox = new Text(container, SWT.READ_ONLY | SWT.BORDER | SWT.SINGLE);
        selectedCertificationProjectTextBox.setText(MyWizard.StringEmpty);
        selectedCertificationProjectTextBox.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                // TODO Auto-generated method stub
            }

            @Override
            public void keyReleased(KeyEvent e) {
            	CheckPageCompleted();
            }

        });
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        selectedCertificationProjectTextBox.setLayoutData(gd);

        Button selectCertificationProjectButton;
        selectCertificationProjectButton = new Button(container, SWT.NONE);
        selectCertificationProjectButton.setText(SearchButtonText);
        selectCertificationProjectButton.addMouseListener(new MouseListener() {
        	

			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDown(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent arg0) {
				ProjectSelector listAssuranceProject; 
				listAssuranceProject= new ProjectSelector(getShell(), session);
				if (listAssuranceProject.open() == Window.OK) {
					ArrayList<String> sResult;
					sResult = new ArrayList<String>();
					sResult.add(listAssuranceProject.getResult().get(0));
					sResult.add(listAssuranceProject.getResult().get(1));
					selectedCertificationProjectTextBox.setText(listAssuranceProject.getResult().get(1));
					CheckPageCompleted();
				}
			}
        	
        });
        
        // Evidence name
        Label evidenceNameLabel = new Label(container, SWT.NONE);
        evidenceNameLabel.setText(EvidenceNameLabelText);

        evidenceNameTextBox = new Text(container, SWT.BORDER | SWT.SINGLE);
        evidenceNameTextBox.setText(MyWizard.StringEmpty);
        
        GridData evidenceNameTextBoxLayout = new GridData(GridData.FILL_HORIZONTAL);
        evidenceNameTextBoxLayout.horizontalSpan = 2;
        evidenceNameTextBox.setLayoutData(evidenceNameTextBoxLayout);
        
        evidenceNameTextBox.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                // TODO Auto-generated method stub
            }

            @Override
            public void keyReleased(KeyEvent e) {
            	CheckPageCompleted();
            }

        });
        
        setControl(container);
        // required to avoid an error in the system
        CheckPageCompleted();
    }
    
    private void CheckPageCompleted() {
    	boolean isCompleted;
    	isCompleted = !selectedCertificationProjectTextBox.getText().isEmpty() && !evidenceNameTextBox.getText().isEmpty(); 
        setPageComplete(isCompleted);
    }
    
    public String getSelectedCertificationProjectUri() {
        return selectedCertificationProjectTextBox.getText();
    }
    
    public String getNewEvidenceName() {
        return evidenceNameTextBox.getText();
    }
    
}