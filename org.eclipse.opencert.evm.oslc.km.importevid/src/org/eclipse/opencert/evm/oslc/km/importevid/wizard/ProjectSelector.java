package org.eclipse.opencert.evm.oslc.km.importevid.wizard;

import java.util.ArrayList;

import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.List;

public class ProjectSelector extends Dialog {
	
	private static final String ASSURANCEPROJECT = ".assuranceproject";
	private static final String Slash = "/";
	private CDOView viewCDO = null;
	protected ArrayList<String> sResult = null;
	protected List refList;
	protected ArrayList<String> refListDirMap;	
	protected boolean bcheck = true;

	public ProjectSelector(Shell parentShell, CDOSession session) {
		super(parentShell);
			viewCDO = CDOConnectionUtil.instance.openView(session);
			setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX );
	}

	public ProjectSelector(IShellProvider parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(final Composite parent) {

		final Composite contents = (Composite)super.createDialogArea(parent);

		GridLayout contentsGridLayout = (GridLayout)contents.getLayout();
		contentsGridLayout.numColumns = 1;

		GridData contentsGridData = (GridData)contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		// Label
		Label idLabel = new Label(contents, SWT.NONE);
		idLabel.setText("Select source assurance project:");
		GridData idLabelGridData = new GridData();
		idLabelGridData.horizontalAlignment = SWT.FILL;
		idLabelGridData.verticalAlignment = SWT.FILL;
		idLabel.setLayoutData(idLabelGridData);
		
		//List
		refList = new List(contents, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		refList.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		refListDirMap = new ArrayList<String>();
		
		CDOResourceNode[] listR=  viewCDO.getElements();			
		
		for (int i = 0; i < listR.length; i++) {
			if (listR[i] instanceof CDOResourceFolder) {
				checkFolderContents((CDOResourceFolder) listR[i],ASSURANCEPROJECT );
			} else if (listR[i].getName().endsWith(ASSURANCEPROJECT)) {
				if(listR[i].cdoRevision().getPermission().isWritable()){
					refList.add(RemoveEnding( listR[i].getName(), ASSURANCEPROJECT));
					refListDirMap.add(listR[i].getPath());
				}
			}
		}			
		return contents;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Assurance Projects");
	}

	@Override
	protected void okPressed()
	{	
		if (refList.getSelectionCount()  == 0) {
			MessageDialog.openError(getShell(),
					"Select assurance project", "It's neccesary to select a project.");
		} else if (refList.getSelectionCount() != 1) {
			MessageDialog.openError(getShell(),
					"Select assurance project", "It's neccesary to select only one project.");
		} else {
			int i = refList.getSelectionIndex();
			String sSelectedProject = refList.getItem(i);
			sResult = new ArrayList<String>();
			sResult.add(sSelectedProject);
			String projectURI;
			projectURI = refListDirMap.get(refList.getSelectionIndex());
			// Navigating two levels to the root to have the main URI path for the project
			int lastIndexOfSlash;
			lastIndexOfSlash = projectURI.lastIndexOf(Slash); 
			if (lastIndexOfSlash >= 0) {
				projectURI = projectURI.substring(0, lastIndexOfSlash);
				lastIndexOfSlash = projectURI.lastIndexOf(Slash); 
				if (lastIndexOfSlash >= 0) {
					projectURI = projectURI.substring(0, lastIndexOfSlash);
				}
			}
			sResult.add(projectURI);
			super.okPressed();
		}
	}

	@Override
	public boolean close()
	{
		return super.close();
	}
	
	public ArrayList<String> getResult()
	{	
		return sResult;
	}
	
	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, String sCadena) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();
			for (int i = 0; i < listN.size(); i++) {
				if (listN.get(i) instanceof CDOResourceFolder) {
					checkFolderContents((CDOResourceFolder) listN.get(i), sCadena);
				} else if (listN.get(i).getName().endsWith(sCadena)) {
					if(listN.get(i).cdoRevision().getPermission().isWritable()){
						refList.add(RemoveEnding(listN.get(i).getName(), sCadena));
						refListDirMap.add(listN.get(i).getPath());
					}
					//System.out.println(listN.get(i).getPath());
				}
			}
		}
	}
	
	private static String RemoveEnding(String text, String toBeRemoved) {
		String output;
		output = text;
		if (output != null && output != MyWizard.StringEmpty) {
			output = output.replace(toBeRemoved, MyWizard.StringEmpty);
		}
		return output;
	}

}
