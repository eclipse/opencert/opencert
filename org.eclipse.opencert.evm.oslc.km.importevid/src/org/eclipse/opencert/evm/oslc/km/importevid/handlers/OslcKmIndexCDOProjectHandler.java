/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.handlers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl;
import org.eclipse.opencert.evm.oslc.km.importevid.indexWizard.MyPageOne;
import org.eclipse.opencert.evm.oslc.km.importevid.indexWizard.MyWizard;
import org.eclipse.opencert.evm.oslc.km.importevid.wizard.ProjectSelector;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.swt.widgets.Shell;

public class OslcKmIndexCDOProjectHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		// Gesti�n de CDO
		CDOSession cdoSession = CDOConnectionUtil.instance.getCurrentSession();
		CDOView viewCDO = CDOConnectionUtil.instance.openView(cdoSession);

		// Mostrar selector de proyecto a indexar, y usar el servicio OSLC-KM para indexar los requirements
        CDOTransaction transaction = cdoSession.openTransaction();
        ProjectSelector listAssuranceProject = new ProjectSelector(null, cdoSession);
        if (listAssuranceProject.open() == Window.OK) {
        	
        	try {        	
        		// Buscar la referencia al proyecto
            	String projectUri = listAssuranceProject.getResult().get(1);
            	if (projectUri != null) {
            		
            		// Obtener elementos indexar a partir de una carpeta seleccionada
            		List<AssuranceProject> assuranceProjectsToIndex = new ArrayList<AssuranceProject>();
            		List<Artefact> artefactsToIndex = new ArrayList<Artefact>();
            		CDOResourceFolder resFolder = viewCDO.getResourceFolder(projectUri);
            		if (resFolder != null) {
            			TreeIterator<EObject> contents = resFolder.eAllContents();
            			if (contents != null) {
            				EObject eObj;
            				while (contents.hasNext()) {
            					eObj = contents.next();
            					if (eObj != null) {
            						if (eObj instanceof AssuranceProject) {
            							//System.out.println("Assurance project found!");
            							assuranceProjectsToIndex.add((AssuranceProject)eObj);
            						} else if (eObj instanceof Artefact) {
            							//System.out.println("Artefact found!");
            							artefactsToIndex.add((Artefact)eObj);
            						}
            					}
            				}
            			}
            		}
            		
            		// Print de elementos a indexar
            		int assuranceProjectsCount = 0;
            		int artefactsCount = 0;
            		if (assuranceProjectsToIndex != null) {
            			assuranceProjectsCount = assuranceProjectsToIndex.size();
            		}
            		if (artefactsToIndex != null) {
            			artefactsCount = artefactsToIndex.size();
            		}
            		System.out.println(assuranceProjectsCount + " assurance projects to index");
            		System.out.println(artefactsCount + " artefacts to index");
            		
            		// First, collecting SIL levels by Artefacts in Assurance Projects
            		Map<Artefact, List<String>> artefactsSilMapping = null;
            		if (assuranceProjectsToIndex != null && assuranceProjectsToIndex.size() > 0) {
            			
            			artefactsSilMapping = MyPageOne.collectSilLevelsByArtefactFromAssuranceProjects(assuranceProjectsToIndex);
            			if (artefactsSilMapping != null && artefactsSilMapping.size() > 0) {
            				Artefact art;
            				List<String> silLevels;
            				Iterator it = artefactsSilMapping.entrySet().iterator();
            				while (it.hasNext()) {
            					Map.Entry pair = (Map.Entry)it.next();
            					art = (Artefact)pair.getKey();
            					silLevels = (List<String>)pair.getValue();
            					System.out.println("Artefact " + art.getName() + ", " + silLevels.size() + " sil levels");
            				}
            			}			
            		}
            		
            		// Then, index Artefacts with the SIL levels
            		if (artefactsToIndex != null && artefactsToIndex.size() > 0) {
            			MyPageOne.indexArtefacts_FocusOnEvidenceReuse(viewCDO, artefactsToIndex, artefactsSilMapping);
            		}
            		
            		// Confirmation message
            		MessageDialog.openInformation(new Shell(), "Index finished", "Process finished successfully. Indexed " + assuranceProjectsCount + " assurance projects and " + artefactsCount + " artefacts");
            	}
        	} catch (Exception ex) {
        		ex.printStackTrace();
        		
        		// Failure message
        		MessageDialog.openInformation(new Shell(), "Error", "Error when trying to index: " + ex.getMessage());
        	}        	        
        }
		
		
		
		// Mientras tanto, obtengo el recurso de CDO de forma cableada
		/*String project1 = "/CS1-RTU-Security-P1/ASSURANCE_PROJECT/CS1-RTU-Security-P1.assuranceproject";
		String project2 = "/CS1-RTU-Security/ASSURANCE_PROJECT/CS1-RTU-Security.assuranceproject";
		CDOResource myResource = viewCDO.getResource(project2);
		if (myResource != null) {
			System.out.println("CDO Resource found --> Indexing in OSLC-KM");
			MyPageOne.indexElementsOfCDOResourceAsAssuranceProject_FocusOnEvidenceReuse(myResource);	
		} else {
			System.out.println("CDO Resource not found!");
		}    	
		*/
		
		
		/*IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		WizardDialog wizardDialog = new WizardDialog(window.getShell(), new MyWizard());
        if (wizardDialog.open() == Window.OK) {
    		// MessageDialog.openInformation(window.getShell(), "OSLC KM Importer","Evidences imported successfully");
        }		
		return null;*/
        
        return null;
	}
	
	private void Test_OpenProjectSelector() {
		
		// Getting open CDO Session
		CDOSession cdoSession = CDOConnectionUtil.instance.getCurrentSession();
        ProjectSelector listAssuranceProject = new ProjectSelector(null, cdoSession);
        if (listAssuranceProject.open() == Window.OK) {
        	
        	// Buscar la referencia al proyecto
        	String projectUri = listAssuranceProject.getResult().get(1);
        	
            CDOTransaction transaction = cdoSession.openTransaction();
        	CDOResource resource = transaction.getResource(projectUri);
        	//CDOResource resource = cdoSession.getView(0).getResource(projectUri);
        	if (resource != null) {
        	
        		System.out.println("Found: " + resource.getName());
        		
        		// Indexing contents
        		MyPageOne.indexElementsOfCDOResourceAsAssuranceProject(resource);
        		
        		// Pruebas
        		
        		// Look for Requirement Items inside of the Assurance Project
        		//resource.getAllContents();
        		/*EList<EObject> contents = resource.getContents();
        		int contentsCount = contents.size();
        		EObject item = null;
        		String itemName =  null;
        		for (int i = 0; i < contentsCount; i++) {
        			item = contents.get(i);
        			itemName = item.toString();
        			System.out.println("Item: " + itemName);
        		}*/
        	} else {
        		
        		// Project not found
        		System.out.println("No project found");
        	}        	
        }
	}
	
}
