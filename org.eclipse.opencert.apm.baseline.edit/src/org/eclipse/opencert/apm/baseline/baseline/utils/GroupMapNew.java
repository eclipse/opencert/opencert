/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.baseline.baseline.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapModel;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;

public class GroupMapNew extends Dialog {
	
	private CDOTransaction transactionGroup = null;
	
	protected MapGroup mapGroup;
	protected MapModel mapModel; 
	
	protected String sPath;	
	protected Text idText;
	protected Text nameText;
	protected Text descText;
	
	protected boolean bcheck = true;
	
	Map<Object, Object> options = new HashMap<Object, Object>();
	
	public GroupMapNew(Shell parentShell, CDOSession sessionCDO, String path) {
		super(parentShell);

		try {
			transactionGroup = sessionCDO.openTransaction();
			
			sPath = path;
			setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX );
		}
		catch (Exception exception) {
			MessageDialog.openError(getShell(), "Not valid model file", exception.toString());
		}
	}

	public GroupMapNew(IShellProvider parentShell) {
		super(parentShell);
	}


	@Override
	protected Control createDialogArea(final Composite parent) {

		final Composite contents = (Composite)super.createDialogArea(parent);

		GridLayout contentsGridLayout = (GridLayout)contents.getLayout();
		contentsGridLayout.numColumns = 2;

		GridData contentsGridData = (GridData)contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		// Label
		Label idLabel = new Label(contents, SWT.NONE);
		idLabel.setText("ID");
		GridData idLabelGridData = new GridData();
		idLabelGridData.horizontalAlignment = SWT.FILL;
		idLabelGridData.verticalAlignment = SWT.FILL;
		idLabel.setLayoutData(idLabelGridData);

		// Text
		idText = new Text(contents, SWT.NONE);
		GridData idTextGridData = new GridData();
		idTextGridData.horizontalAlignment = SWT.FILL;
		idTextGridData.verticalAlignment = SWT.FILL;
		idText.setLayoutData(idTextGridData);
		
		
		// Label
		Label nameLabel = new Label(contents, SWT.NONE);
		nameLabel.setText("Name");
		GridData nameLabelGridData = new GridData();
		nameLabelGridData.grabExcessHorizontalSpace = true;
		nameLabelGridData.minimumWidth = 20;
		nameLabelGridData.horizontalAlignment = SWT.FILL;
		nameLabelGridData.verticalAlignment = SWT.FILL;
		nameLabel.setLayoutData(nameLabelGridData);

		// Text
		nameText = new Text(contents, SWT.NONE);
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 20;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		nameText.setLayoutData(nameTextGridData);
		
		// Label
		Label descLabel = new Label(contents, SWT.NONE);
		descLabel.setText("Description");
		GridData descLabelGridData = new GridData();
		descLabelGridData.horizontalAlignment = SWT.FILL;
		descLabelGridData.verticalAlignment = SWT.FILL;
		descLabel.setLayoutData(descLabelGridData);

		// Text
		descText = new Text(contents, SWT.NONE);
		GridData descTextGridData = new GridData();
		descTextGridData.horizontalAlignment = SWT.FILL;
		descTextGridData.verticalAlignment = SWT.FILL;
		descTextGridData.widthHint = Display.getCurrent().getBounds().width / 5;
		descTextGridData.heightHint = Display.getCurrent().getBounds().height / 9;
		
		descText.setLayoutData(descTextGridData);
		
		return contents;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("New Map Group");
	}

	@Override
	protected void okPressed()
	{
		
		mapGroup = MappingFactory.eINSTANCE.createMapGroup();
		
		mapGroup.setId(idText.getText());
		mapGroup.setName(nameText.getText());
		mapGroup.setDescription(descText.getText());
		try {
			synchronized (transactionGroup) {
				CDOResource mappingResource = transactionGroup
						.getResource(sPath);

				mapModel = (MapModel) mappingResource.getContents().get(0);

				mapModel.getMapGroupModel().add(mapGroup);

				mappingResource.save(options);
				transactionGroup.commit();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (ConcurrentAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			transactionGroup.rollback();
		} catch (CommitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		super.okPressed();
	}

	@Override
	public boolean close()
	{
		return super.close();
	}
	public Boolean getResult()
	{
		return bcheck;
	}
	
	

}
