/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseActivityPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseActivityBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for requiredArtefact ReferencesTable
	 */
	private ReferencesTableSettings requiredArtefactSettings;
	
	/**
	 * Settings for producedArtefact ReferencesTable
	 */
	private ReferencesTableSettings producedArtefactSettings;
	
	/**
	 * Settings for subActivity ReferencesTable
	 */
	protected ReferencesTableSettings subActivitySettings;
	
	/**
	 * Settings for precedingActivity ReferencesTable
	 */
	private ReferencesTableSettings precedingActivitySettings;
	
	/**
	 * Settings for role ReferencesTable
	 */
	private ReferencesTableSettings roleSettings;
	
	/**
	 * Settings for applicableTechnique EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings applicableTechniqueSettings;
	
	/**
	 * Settings for ownedRel ReferencesTable
	 */
	protected ReferencesTableSettings ownedRelSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseActivityBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseActivity, String editing_mode) {
		super(editingContext, baseActivity, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseActivity.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseActivity baseActivity = (BaseActivity)elt;
			final BaseActivityPropertiesEditionPart basePart = (BaseActivityPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseActivity.getId()));
			
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseActivity.getName()));
			
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseActivity.getDescription()));
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.objective))
				basePart.setObjective(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseActivity.getObjective()));
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.scope))
				basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseActivity.getScope()));
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.requiredArtefact)) {
				requiredArtefactSettings = new ReferencesTableSettings(baseActivity, BaselinePackage.eINSTANCE.getBaseActivity_RequiredArtefact());
				basePart.initRequiredArtefact(requiredArtefactSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.producedArtefact)) {
				producedArtefactSettings = new ReferencesTableSettings(baseActivity, BaselinePackage.eINSTANCE.getBaseActivity_ProducedArtefact());
				basePart.initProducedArtefact(producedArtefactSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.subActivity)) {
				subActivitySettings = new ReferencesTableSettings(baseActivity, BaselinePackage.eINSTANCE.getBaseActivity_SubActivity());
				basePart.initSubActivity(subActivitySettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.precedingActivity)) {
				precedingActivitySettings = new ReferencesTableSettings(baseActivity, BaselinePackage.eINSTANCE.getBaseActivity_PrecedingActivity());
				basePart.initPrecedingActivity(precedingActivitySettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.role)) {
				roleSettings = new ReferencesTableSettings(baseActivity, BaselinePackage.eINSTANCE.getBaseActivity_Role());
				basePart.initRole(roleSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.applicableTechnique)) {
				// init part
				applicableTechniqueSettings = new EObjectFlatComboSettings(baseActivity, BaselinePackage.eINSTANCE.getBaseActivity_ApplicableTechnique());
				basePart.initApplicableTechnique(applicableTechniqueSettings);
				// set the button mode
				basePart.setApplicableTechniqueButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.ownedRel)) {
				ownedRelSettings = new ReferencesTableSettings(baseActivity, BaselinePackage.eINSTANCE.getBaseActivity_OwnedRel());
				basePart.initOwnedRel(ownedRelSettings);
			}
			// init filters
			
			
			
			
			
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.requiredArtefact)) {
				basePart.addFilterToRequiredArtefact(new EObjectFilter(BaselinePackage.Literals.BASE_ARTEFACT));
				// Start of user code for additional businessfilters for requiredArtefact
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.producedArtefact)) {
				basePart.addFilterToProducedArtefact(new EObjectFilter(BaselinePackage.Literals.BASE_ARTEFACT));
				// Start of user code for additional businessfilters for producedArtefact
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.subActivity)) {
				basePart.addFilterToSubActivity(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseActivity); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for subActivity
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.precedingActivity)) {
				basePart.addFilterToPrecedingActivity(new EObjectFilter(BaselinePackage.Literals.BASE_ACTIVITY));
				// Start of user code for additional businessfilters for precedingActivity
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.role)) {
				basePart.addFilterToRole(new EObjectFilter(BaselinePackage.Literals.BASE_ROLE));
				// Start of user code for additional businessfilters for role
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.applicableTechnique)) {
				basePart.addFilterToApplicableTechnique(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseTechnique); //$NON-NLS-1$ 
					}
					
				});
				// Start of user code for additional businessfilters for applicableTechnique
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseActivity.Properties.ownedRel)) {
				basePart.addFilterToOwnedRel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseActivityRel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRel
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}















	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.objective) {
			return BaselinePackage.eINSTANCE.getBaseActivity_Objective();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.scope) {
			return BaselinePackage.eINSTANCE.getBaseActivity_Scope();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.requiredArtefact) {
			return BaselinePackage.eINSTANCE.getBaseActivity_RequiredArtefact();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.producedArtefact) {
			return BaselinePackage.eINSTANCE.getBaseActivity_ProducedArtefact();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.subActivity) {
			return BaselinePackage.eINSTANCE.getBaseActivity_SubActivity();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.precedingActivity) {
			return BaselinePackage.eINSTANCE.getBaseActivity_PrecedingActivity();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.role) {
			return BaselinePackage.eINSTANCE.getBaseActivity_Role();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.applicableTechnique) {
			return BaselinePackage.eINSTANCE.getBaseActivity_ApplicableTechnique();
		}
		if (editorKey == BaselineViewsRepository.BaseActivity.Properties.ownedRel) {
			return BaselinePackage.eINSTANCE.getBaseActivity_OwnedRel();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseActivity baseActivity = (BaseActivity)semanticObject;
		if (BaselineViewsRepository.BaseActivity.Properties.id == event.getAffectedEditor()) {
			baseActivity.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseActivity.Properties.name == event.getAffectedEditor()) {
			baseActivity.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseActivity.Properties.description == event.getAffectedEditor()) {
			baseActivity.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseActivity.Properties.objective == event.getAffectedEditor()) {
			baseActivity.setObjective((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseActivity.Properties.scope == event.getAffectedEditor()) {
			baseActivity.setScope((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseActivity.Properties.requiredArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseArtefact) {
					requiredArtefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				requiredArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				requiredArtefactSettings.move(event.getNewIndex(), (BaseArtefact) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseActivity.Properties.producedArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseArtefact) {
					producedArtefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				producedArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				producedArtefactSettings.move(event.getNewIndex(), (BaseArtefact) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseActivity.Properties.subActivity == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, subActivitySettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				subActivitySettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				subActivitySettings.move(event.getNewIndex(), (BaseActivity) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseActivity.Properties.precedingActivity == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseActivity) {
					precedingActivitySettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				precedingActivitySettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				precedingActivitySettings.move(event.getNewIndex(), (BaseActivity) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseActivity.Properties.role == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseRole) {
					roleSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				roleSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				roleSettings.move(event.getNewIndex(), (BaseRole) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseActivity.Properties.applicableTechnique == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				applicableTechniqueSettings.setToReference((BaseTechnique)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				BaseTechnique eObject = BaselineFactory.eINSTANCE.createBaseTechnique();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				applicableTechniqueSettings.setToReference(eObject);
			}
		}
		if (BaselineViewsRepository.BaseActivity.Properties.ownedRel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRelSettings.move(event.getNewIndex(), (BaseActivityRel) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseActivityPropertiesEditionPart basePart = (BaseActivityPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseActivity.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseActivity.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseActivity.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseActivity_Objective().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseActivity.Properties.objective)){
				if (msg.getNewValue() != null) {
					basePart.setObjective(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setObjective("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseActivity_Scope().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseActivity.Properties.scope)){
				if (msg.getNewValue() != null) {
					basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setScope("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseActivity_RequiredArtefact().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseActivity.Properties.requiredArtefact))
				basePart.updateRequiredArtefact();
			if (BaselinePackage.eINSTANCE.getBaseActivity_ProducedArtefact().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseActivity.Properties.producedArtefact))
				basePart.updateProducedArtefact();
			if (BaselinePackage.eINSTANCE.getBaseActivity_SubActivity().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseActivity.Properties.subActivity))
				basePart.updateSubActivity();
			if (BaselinePackage.eINSTANCE.getBaseActivity_PrecedingActivity().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseActivity.Properties.precedingActivity))
				basePart.updatePrecedingActivity();
			if (BaselinePackage.eINSTANCE.getBaseActivity_Role().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseActivity.Properties.role))
				basePart.updateRole();
			if (BaselinePackage.eINSTANCE.getBaseActivity_ApplicableTechnique().equals(msg.getFeature()) && basePart != null && isAccessible(BaselineViewsRepository.BaseActivity.Properties.applicableTechnique))
				basePart.setApplicableTechnique((EObject)msg.getNewValue());
			if (BaselinePackage.eINSTANCE.getBaseActivity_OwnedRel().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseActivity.Properties.ownedRel))
				basePart.updateOwnedRel();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			BaselinePackage.eINSTANCE.getBaseActivity_Objective(),
			BaselinePackage.eINSTANCE.getBaseActivity_Scope(),
			BaselinePackage.eINSTANCE.getBaseActivity_RequiredArtefact(),
			BaselinePackage.eINSTANCE.getBaseActivity_ProducedArtefact(),
			BaselinePackage.eINSTANCE.getBaseActivity_SubActivity(),
			BaselinePackage.eINSTANCE.getBaseActivity_PrecedingActivity(),
			BaselinePackage.eINSTANCE.getBaseActivity_Role(),
			BaselinePackage.eINSTANCE.getBaseActivity_ApplicableTechnique(),
			BaselinePackage.eINSTANCE.getBaseActivity_OwnedRel()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseActivity.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseActivity.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseActivity.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseActivity.Properties.objective == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseActivity_Objective().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseActivity_Objective().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseActivity.Properties.scope == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseActivity_Scope().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseActivity_Scope().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
