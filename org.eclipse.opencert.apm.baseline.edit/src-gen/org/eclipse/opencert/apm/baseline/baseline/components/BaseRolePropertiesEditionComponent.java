/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.apm.baseline.baseline.BaseRole;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRolSelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRoleEquivelanceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRolePropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class BaseRolePropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private BaseRolePropertiesEditionPart basePart;

	/**
	 * The BaseRoleBasePropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRoleBasePropertiesEditionComponent baseRoleBasePropertiesEditionComponent;

	/**
	 * The BaseRolSelection part
	 * 
	 */
	private BaseRolSelectionPropertiesEditionPart baseRolSelectionPart;

	/**
	 * The BaseRoleBaseRolSelectionPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRoleBaseRolSelectionPropertiesEditionComponent baseRoleBaseRolSelectionPropertiesEditionComponent;

	/**
	 * The BaseRoleEquivelanceMap part
	 * 
	 */
	private BaseRoleEquivelanceMapPropertiesEditionPart baseRoleEquivelanceMapPart;

	/**
	 * The BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent baseRoleBaseRoleEquivelanceMapPropertiesEditionComponent;

	/**
	 * The BaseRoleComplianceMap part
	 * 
	 */
	private BaseRoleComplianceMapPropertiesEditionPart baseRoleComplianceMapPart;

	/**
	 * The BaseRoleBaseRoleComplianceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseRoleBaseRoleComplianceMapPropertiesEditionComponent baseRoleBaseRoleComplianceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param baseRole the EObject to edit
	 * 
	 */
	public BaseRolePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseRole, String editing_mode) {
		super(editingContext, editing_mode);
		if (baseRole instanceof BaseRole) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRole, PropertiesEditingProvider.class);
			baseRoleBasePropertiesEditionComponent = (BaseRoleBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRoleBasePropertiesEditionComponent.BASE_PART, BaseRoleBasePropertiesEditionComponent.class);
			addSubComponent(baseRoleBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRole, PropertiesEditingProvider.class);
			baseRoleBaseRolSelectionPropertiesEditionComponent = (BaseRoleBaseRolSelectionPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRoleBaseRolSelectionPropertiesEditionComponent.BASEROLSELECTION_PART, BaseRoleBaseRolSelectionPropertiesEditionComponent.class);
			addSubComponent(baseRoleBaseRolSelectionPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRole, PropertiesEditingProvider.class);
			baseRoleBaseRoleEquivelanceMapPropertiesEditionComponent = (BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.BASEROLEEQUIVELANCEMAP_PART, BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.class);
			addSubComponent(baseRoleBaseRoleEquivelanceMapPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseRole, PropertiesEditingProvider.class);
			baseRoleBaseRoleComplianceMapPropertiesEditionComponent = (BaseRoleBaseRoleComplianceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.BASEROLECOMPLIANCEMAP_PART, BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.class);
			addSubComponent(baseRoleBaseRoleComplianceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (BaseRoleBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (BaseRolePropertiesEditionPart)baseRoleBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (BaseRoleBaseRolSelectionPropertiesEditionComponent.BASEROLSELECTION_PART.equals(key)) {
			baseRolSelectionPart = (BaseRolSelectionPropertiesEditionPart)baseRoleBaseRolSelectionPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseRolSelectionPart;
		}
		if (BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.BASEROLEEQUIVELANCEMAP_PART.equals(key)) {
			baseRoleEquivelanceMapPart = (BaseRoleEquivelanceMapPropertiesEditionPart)baseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseRoleEquivelanceMapPart;
		}
		if (BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.BASEROLECOMPLIANCEMAP_PART.equals(key)) {
			baseRoleComplianceMapPart = (BaseRoleComplianceMapPropertiesEditionPart)baseRoleBaseRoleComplianceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseRoleComplianceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (BaselineViewsRepository.BaseRole.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (BaseRolePropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseRolSelection.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseRolSelectionPart = (BaseRolSelectionPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseRoleEquivelanceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseRoleEquivelanceMapPart = (BaseRoleEquivelanceMapPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseRoleComplianceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseRoleComplianceMapPart = (BaseRoleComplianceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == BaselineViewsRepository.BaseRole.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseRolSelection.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseRoleEquivelanceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseRoleComplianceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
