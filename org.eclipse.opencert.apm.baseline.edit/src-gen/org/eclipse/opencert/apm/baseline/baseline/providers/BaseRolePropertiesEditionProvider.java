/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.providers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.providers.impl.PropertiesEditingProviderImpl;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

import org.eclipse.opencert.apm.baseline.baseline.components.BaseRoleBasePropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseRoleBaseRolSelectionPropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseRoleBaseRoleComplianceMapPropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent;
import org.eclipse.opencert.apm.baseline.baseline.components.BaseRolePropertiesEditionComponent;

/**
 * 
 * 
 */
public class BaseRolePropertiesEditionProvider extends PropertiesEditingProviderImpl {

	/**
	 * Constructor without provider for super types.
	 */
	public BaseRolePropertiesEditionProvider() {
		super();
	}

	/**
	 * Constructor with providers for super types.
	 * @param superProviders providers to use for super types.
	 */
	public BaseRolePropertiesEditionProvider(List<PropertiesEditingProvider> superProviders) {
		super(superProviders);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext) {
		return (editingContext.getEObject() instanceof BaseRole) 
					&& (BaselinePackage.Literals.BASE_ROLE == editingContext.getEObject().eClass());
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext, String part) {
		return (editingContext.getEObject() instanceof BaseRole) && (BaseRoleBasePropertiesEditionComponent.BASE_PART.equals(part) || BaseRoleBaseRolSelectionPropertiesEditionComponent.BASEROLSELECTION_PART.equals(part) || BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.BASEROLEEQUIVELANCEMAP_PART.equals(part) || BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.BASEROLECOMPLIANCEMAP_PART.equals(part));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof BaseRole) && (refinement == BaseRoleBasePropertiesEditionComponent.class || refinement == BaseRoleBaseRolSelectionPropertiesEditionComponent.class || refinement == BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.class || refinement == BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.class);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, String part, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof BaseRole) && ((BaseRoleBasePropertiesEditionComponent.BASE_PART.equals(part) && refinement == BaseRoleBasePropertiesEditionComponent.class) || (BaseRoleBaseRolSelectionPropertiesEditionComponent.BASEROLSELECTION_PART.equals(part) && refinement == BaseRoleBaseRolSelectionPropertiesEditionComponent.class) || (BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.BASEROLEEQUIVELANCEMAP_PART.equals(part) && refinement == BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.class) || (BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.BASEROLECOMPLIANCEMAP_PART.equals(part) && refinement == BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.class));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode) {
		if (editingContext.getEObject() instanceof BaseRole) {
			return new BaseRolePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part) {
		if (editingContext.getEObject() instanceof BaseRole) {
			if (BaseRoleBasePropertiesEditionComponent.BASE_PART.equals(part))
				return new BaseRoleBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseRoleBaseRolSelectionPropertiesEditionComponent.BASEROLSELECTION_PART.equals(part))
				return new BaseRoleBaseRolSelectionPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.BASEROLEEQUIVELANCEMAP_PART.equals(part))
				return new BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.BASEROLECOMPLIANCEMAP_PART.equals(part))
				return new BaseRoleBaseRoleComplianceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part, java.lang.Class refinement) {
		if (editingContext.getEObject() instanceof BaseRole) {
			if (BaseRoleBasePropertiesEditionComponent.BASE_PART.equals(part)
				&& refinement == BaseRoleBasePropertiesEditionComponent.class)
				return new BaseRoleBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseRoleBaseRolSelectionPropertiesEditionComponent.BASEROLSELECTION_PART.equals(part)
				&& refinement == BaseRoleBaseRolSelectionPropertiesEditionComponent.class)
				return new BaseRoleBaseRolSelectionPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.BASEROLEEQUIVELANCEMAP_PART.equals(part)
				&& refinement == BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent.class)
				return new BaseRoleBaseRoleEquivelanceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.BASEROLECOMPLIANCEMAP_PART.equals(part)
				&& refinement == BaseRoleBaseRoleComplianceMapPropertiesEditionComponent.class)
				return new BaseRoleBaseRoleComplianceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part, refinement);
	}

	/**
	 * Provides the filter used by the plugin.xml to assign part forms.
	 */
	public static class EditionFilter implements IFilter {
		
		/**
		 * {@inheritDoc}
		 * 
		 * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
		 */
		public boolean select(Object toTest) {
			EObject eObj = EEFUtils.resolveSemanticObject(toTest);
			return eObj != null && BaselinePackage.Literals.BASE_ROLE == eObj.eClass();
		}
		
	}

}
