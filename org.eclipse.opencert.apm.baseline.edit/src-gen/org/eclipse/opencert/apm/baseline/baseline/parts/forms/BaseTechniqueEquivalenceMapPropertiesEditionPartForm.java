/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;

// End of user code

/**
 * 
 * 
 */
public class BaseTechniqueEquivalenceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseTechniqueEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalenceMap;
	protected List<ViewerFilter> equivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer techniqueEquivalenceMap;
	protected List<ViewerFilter> techniqueEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> techniqueEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addTechniqueEquivalenceMap;
	protected Button removeTechniqueEquivalenceMap;
	protected Button editTechniqueEquivalenceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public BaseTechniqueEquivalenceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseTechniqueEquivalenceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseTechniqueEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseTechniqueEquivalenceMapStep.addStep(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap);
		
		
		composer = new PartComposer(baseTechniqueEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap) {
					return createEquivalenceMapTableComposition(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap) {
					return createTechniqueEquivalenceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseTechniqueEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceMapTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.equivalenceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap, BaselineMessages.BaseTechniqueEquivalenceMapPropertiesEditionPart_EquivalenceMapLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalenceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalenceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalenceMap.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalenceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceMapFilters) {
			this.equivalenceMap.addFilter(filter);
		}
		this.equivalenceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap, BaselineViewsRepository.FORM_KIND));
		this.equivalenceMap.createControls(parent, widgetFactory);
		this.equivalenceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceMapData.horizontalSpan = 3;
		this.equivalenceMap.setLayoutData(equivalenceMapData);
		this.equivalenceMap.setLowerBound(0);
		this.equivalenceMap.setUpperBound(-1);
		equivalenceMap.setID(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap);
		equivalenceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceMapTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createTechniqueEquivalenceMapTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableTechniqueEquivalenceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableTechniqueEquivalenceMap.setHeaderVisible(true);
		GridData gdTechniqueEquivalenceMap = new GridData();
		gdTechniqueEquivalenceMap.grabExcessHorizontalSpace = true;
		gdTechniqueEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdTechniqueEquivalenceMap.grabExcessVerticalSpace = true;
		gdTechniqueEquivalenceMap.verticalAlignment = GridData.FILL;
		tableTechniqueEquivalenceMap.setLayoutData(gdTechniqueEquivalenceMap);
		tableTechniqueEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for TechniqueEquivalenceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableTechniqueEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableTechniqueEquivalenceMap, SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableTechniqueEquivalenceMap, SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableTechniqueEquivalenceMap, SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Techniques"); //$NON-NLS-1$
		// End IRR
		// End of user code
		
		techniqueEquivalenceMap = new TableViewer(tableTechniqueEquivalenceMap);
		techniqueEquivalenceMap.setContentProvider(new ArrayContentProvider());
		techniqueEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for TechniqueEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				/*
				AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					}
				}
				*/
				if (object instanceof EObject) {
					BaseEquivalenceMap baseEquivalenceMap = (BaseEquivalenceMap)object;
					switch (columnIndex) {
					case 0:							
						return baseEquivalenceMap.getId();
					case 1:							
						if (baseEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return baseEquivalenceMap.getMapGroup().getName();
					case 2:							
						if (baseEquivalenceMap.getTarget() == null)
							return "";
						else{
							EList<RefAssurableElement> LstElement = baseEquivalenceMap.getTarget();
							Iterator<RefAssurableElement> iter = LstElement.iterator();
							String sElement = "[";
							while (iter.hasNext()) {
								RefAssurableElement assElement = iter.next();
								if (assElement instanceof  RefTechnique) {
									RefTechnique refTech = (RefTechnique) assElement;
									sElement = sElement + refTech.getName() + ",";  
								}
							
								
							}
							//delete ,
							sElement = sElement.substring(0, sElement.length()-1);
							sElement = sElement + "]";
							return sElement;
						}
							
					}
				}
				//End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		techniqueEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (techniqueEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						techniqueEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData techniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		techniqueEquivalenceMapData.minimumHeight = 120;
		techniqueEquivalenceMapData.heightHint = 120;
		techniqueEquivalenceMap.getTable().setLayoutData(techniqueEquivalenceMapData);
		for (ViewerFilter filter : this.techniqueEquivalenceMapFilters) {
			techniqueEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(techniqueEquivalenceMap.getTable(), BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap);
		EditingUtils.setEEFtype(techniqueEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createTechniqueEquivalenceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createTechniqueEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createTechniqueEquivalenceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite techniqueEquivalenceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout techniqueEquivalenceMapPanelLayout = new GridLayout();
		techniqueEquivalenceMapPanelLayout.numColumns = 1;
		techniqueEquivalenceMapPanel.setLayout(techniqueEquivalenceMapPanelLayout);
		addTechniqueEquivalenceMap = widgetFactory.createButton(techniqueEquivalenceMapPanel, BaselineMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addTechniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addTechniqueEquivalenceMap.setLayoutData(addTechniqueEquivalenceMapData);
		addTechniqueEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				techniqueEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addTechniqueEquivalenceMap, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap);
		EditingUtils.setEEFtype(addTechniqueEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeTechniqueEquivalenceMap = widgetFactory.createButton(techniqueEquivalenceMapPanel, BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeTechniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeTechniqueEquivalenceMap.setLayoutData(removeTechniqueEquivalenceMapData);
		removeTechniqueEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (techniqueEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						techniqueEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeTechniqueEquivalenceMap, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap);
		EditingUtils.setEEFtype(removeTechniqueEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editTechniqueEquivalenceMap = widgetFactory.createButton(techniqueEquivalenceMapPanel, BaselineMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editTechniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editTechniqueEquivalenceMap.setLayoutData(editTechniqueEquivalenceMapData);
		editTechniqueEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (techniqueEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueEquivalenceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						techniqueEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editTechniqueEquivalenceMap, BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap);
		EditingUtils.setEEFtype(editTechniqueEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createTechniqueEquivalenceMapPanel

		// End of user code
		return techniqueEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#initEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalenceMap.setContentProvider(contentProvider);
		equivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.equivalenceMap);
		if (eefElementEditorReadOnlyState && equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(false);
			equivalenceMap.setToolTipText(BaselineMessages.BaseTechniqueEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalenceMap.isEnabled()) {
			equivalenceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#updateEquivalenceMap()
	 * 
	 */
	public void updateEquivalenceMap() {
	equivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#addFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapFilters.add(filter);
		if (this.equivalenceMap != null) {
			this.equivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalenceMap(ViewerFilter filter) {
		equivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)equivalenceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#initTechniqueEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initTechniqueEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		techniqueEquivalenceMap.setContentProvider(contentProvider);
		techniqueEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseTechniqueEquivalenceMap.Properties.techniqueEquivalenceMap);
		if (eefElementEditorReadOnlyState && techniqueEquivalenceMap.getTable().isEnabled()) {
			techniqueEquivalenceMap.getTable().setEnabled(false);
			techniqueEquivalenceMap.getTable().setToolTipText(BaselineMessages.BaseTechniqueEquivalenceMap_ReadOnly);
			addTechniqueEquivalenceMap.setEnabled(false);
			addTechniqueEquivalenceMap.setToolTipText(BaselineMessages.BaseTechniqueEquivalenceMap_ReadOnly);
			removeTechniqueEquivalenceMap.setEnabled(false);
			removeTechniqueEquivalenceMap.setToolTipText(BaselineMessages.BaseTechniqueEquivalenceMap_ReadOnly);
			editTechniqueEquivalenceMap.setEnabled(false);
			editTechniqueEquivalenceMap.setToolTipText(BaselineMessages.BaseTechniqueEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !techniqueEquivalenceMap.getTable().isEnabled()) {
			techniqueEquivalenceMap.getTable().setEnabled(true);
			addTechniqueEquivalenceMap.setEnabled(true);
			removeTechniqueEquivalenceMap.setEnabled(true);
			editTechniqueEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#updateTechniqueEquivalenceMap()
	 * 
	 */
	public void updateTechniqueEquivalenceMap() {
	techniqueEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#addFilterTechniqueEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTechniqueEquivalenceMap(ViewerFilter filter) {
		techniqueEquivalenceMapFilters.add(filter);
		if (this.techniqueEquivalenceMap != null) {
			this.techniqueEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#addBusinessFilterTechniqueEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTechniqueEquivalenceMap(ViewerFilter filter) {
		techniqueEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueEquivalenceMapPropertiesEditionPart#isContainedInTechniqueEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInTechniqueEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)techniqueEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseTechniqueEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
