/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;

// End of user code

/**
 * 
 * 
 */
public class BaseRequirementPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, BaseRequirementPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text reference;
	protected Text assumptions;
	protected Text rationale;
	protected Text image;
	protected Text annotations;
	protected ReferencesTable ownedRel;
	protected List<ViewerFilter> ownedRelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable subRequirement;
	protected List<ViewerFilter> subRequirementBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> subRequirementFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseRequirementPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence baseRequirementStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseRequirementStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.class);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.id);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.name);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.description);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.reference);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.assumptions);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.rationale);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.image);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.annotations);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.ownedRel);
		propertiesStep.addStep(BaselineViewsRepository.BaseRequirement.Properties.subRequirement);
		
		
		composer = new PartComposer(baseRequirementStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseRequirement.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.id) {
					return createIdText(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.name) {
					return createNameText(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.description) {
					return createDescriptionTextarea(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.reference) {
					return createReferenceText(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.assumptions) {
					return createAssumptionsText(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.rationale) {
					return createRationaleText(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.image) {
					return createImageText(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.annotations) {
					return createAnnotationsText(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.ownedRel) {
					return createOwnedRelAdvancedTableComposition(parent);
				}
				if (key == BaselineViewsRepository.BaseRequirement.Properties.subRequirement) {
					return createSubRequirementAdvancedTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(BaselineMessages.BaseRequirementPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.id, BaselineMessages.BaseRequirementPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, BaselineViewsRepository.BaseRequirement.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.id, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.name, BaselineMessages.BaseRequirementPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, BaselineViewsRepository.BaseRequirement.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.name, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionTextarea(Composite parent) {
		Label descriptionLabel = createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.description, BaselineMessages.BaseRequirementPropertiesEditionPart_DescriptionLabel);
		GridData descriptionLabelData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionLabelData.horizontalSpan = 3;
		descriptionLabel.setLayoutData(descriptionLabelData);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		descriptionData.horizontalSpan = 2;
		descriptionData.heightHint = 80;
		descriptionData.widthHint = 200;
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		EditingUtils.setID(description, BaselineViewsRepository.BaseRequirement.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.description, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionTextArea

		// End of user code
		return parent;
	}

	
	protected Composite createReferenceText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.reference, BaselineMessages.BaseRequirementPropertiesEditionPart_ReferenceLabel);
		reference = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData referenceData = new GridData(GridData.FILL_HORIZONTAL);
		reference.setLayoutData(referenceData);
		reference.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.reference, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, reference.getText()));
			}

		});
		reference.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.reference, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, reference.getText()));
				}
			}

		});
		EditingUtils.setID(reference, BaselineViewsRepository.BaseRequirement.Properties.reference);
		EditingUtils.setEEFtype(reference, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.reference, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createReferenceText

		// End of user code
		return parent;
	}

	
	protected Composite createAssumptionsText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.assumptions, BaselineMessages.BaseRequirementPropertiesEditionPart_AssumptionsLabel);
		assumptions = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData assumptionsData = new GridData(GridData.FILL_HORIZONTAL);
		assumptions.setLayoutData(assumptionsData);
		assumptions.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.assumptions, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, assumptions.getText()));
			}

		});
		assumptions.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.assumptions, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, assumptions.getText()));
				}
			}

		});
		EditingUtils.setID(assumptions, BaselineViewsRepository.BaseRequirement.Properties.assumptions);
		EditingUtils.setEEFtype(assumptions, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.assumptions, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createAssumptionsText

		// End of user code
		return parent;
	}

	
	protected Composite createRationaleText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.rationale, BaselineMessages.BaseRequirementPropertiesEditionPart_RationaleLabel);
		rationale = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData rationaleData = new GridData(GridData.FILL_HORIZONTAL);
		rationale.setLayoutData(rationaleData);
		rationale.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.rationale, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, rationale.getText()));
			}

		});
		rationale.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.rationale, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, rationale.getText()));
				}
			}

		});
		EditingUtils.setID(rationale, BaselineViewsRepository.BaseRequirement.Properties.rationale);
		EditingUtils.setEEFtype(rationale, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.rationale, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRationaleText

		// End of user code
		return parent;
	}

	
	protected Composite createImageText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.image, BaselineMessages.BaseRequirementPropertiesEditionPart_ImageLabel);
		image = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData imageData = new GridData(GridData.FILL_HORIZONTAL);
		image.setLayoutData(imageData);
		image.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.image, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, image.getText()));
			}

		});
		image.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.image, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, image.getText()));
				}
			}

		});
		EditingUtils.setID(image, BaselineViewsRepository.BaseRequirement.Properties.image);
		EditingUtils.setEEFtype(image, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.image, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createImageText

		// End of user code
		return parent;
	}

	
	protected Composite createAnnotationsText(Composite parent) {
		createDescription(parent, BaselineViewsRepository.BaseRequirement.Properties.annotations, BaselineMessages.BaseRequirementPropertiesEditionPart_AnnotationsLabel);
		annotations = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData annotationsData = new GridData(GridData.FILL_HORIZONTAL);
		annotations.setLayoutData(annotationsData);
		annotations.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.annotations, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, annotations.getText()));
			}

		});
		annotations.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.annotations, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, annotations.getText()));
				}
			}

		});
		EditingUtils.setID(annotations, BaselineViewsRepository.BaseRequirement.Properties.annotations);
		EditingUtils.setEEFtype(annotations, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.annotations, BaselineViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createAnnotationsText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRelAdvancedTableComposition(Composite parent) {
		this.ownedRel = new ReferencesTable(getDescription(BaselineViewsRepository.BaseRequirement.Properties.ownedRel, BaselineMessages.BaseRequirementPropertiesEditionPart_OwnedRelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRelFilters) {
			this.ownedRel.addFilter(filter);
		}
		this.ownedRel.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.ownedRel, BaselineViewsRepository.SWT_KIND));
		this.ownedRel.createControls(parent);
		this.ownedRel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.ownedRel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRelData.horizontalSpan = 3;
		this.ownedRel.setLayoutData(ownedRelData);
		this.ownedRel.setLowerBound(0);
		this.ownedRel.setUpperBound(-1);
		ownedRel.setID(BaselineViewsRepository.BaseRequirement.Properties.ownedRel);
		ownedRel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRelAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createSubRequirementAdvancedTableComposition(Composite parent) {
		this.subRequirement = new ReferencesTable(getDescription(BaselineViewsRepository.BaseRequirement.Properties.subRequirement, BaselineMessages.BaseRequirementPropertiesEditionPart_SubRequirementLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.subRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				subRequirement.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.subRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				subRequirement.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.subRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				subRequirement.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.subRequirement, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				subRequirement.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.subRequirementFilters) {
			this.subRequirement.addFilter(filter);
		}
		this.subRequirement.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseRequirement.Properties.subRequirement, BaselineViewsRepository.SWT_KIND));
		this.subRequirement.createControls(parent);
		this.subRequirement.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseRequirementPropertiesEditionPartImpl.this, BaselineViewsRepository.BaseRequirement.Properties.subRequirement, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData subRequirementData = new GridData(GridData.FILL_HORIZONTAL);
		subRequirementData.horizontalSpan = 3;
		this.subRequirement.setLayoutData(subRequirementData);
		this.subRequirement.setLowerBound(0);
		this.subRequirement.setUpperBound(-1);
		subRequirement.setID(BaselineViewsRepository.BaseRequirement.Properties.subRequirement);
		subRequirement.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createSubRequirementAdvancedTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setBackground(description.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			description.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getReference()
	 * 
	 */
	public String getReference() {
		return reference.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setReference(String newValue)
	 * 
	 */
	public void setReference(String newValue) {
		if (newValue != null) {
			reference.setText(newValue);
		} else {
			reference.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.reference);
		if (eefElementEditorReadOnlyState && reference.isEnabled()) {
			reference.setEnabled(false);
			reference.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !reference.isEnabled()) {
			reference.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getAssumptions()
	 * 
	 */
	public String getAssumptions() {
		return assumptions.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setAssumptions(String newValue)
	 * 
	 */
	public void setAssumptions(String newValue) {
		if (newValue != null) {
			assumptions.setText(newValue);
		} else {
			assumptions.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.assumptions);
		if (eefElementEditorReadOnlyState && assumptions.isEnabled()) {
			assumptions.setEnabled(false);
			assumptions.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !assumptions.isEnabled()) {
			assumptions.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getRationale()
	 * 
	 */
	public String getRationale() {
		return rationale.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setRationale(String newValue)
	 * 
	 */
	public void setRationale(String newValue) {
		if (newValue != null) {
			rationale.setText(newValue);
		} else {
			rationale.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.rationale);
		if (eefElementEditorReadOnlyState && rationale.isEnabled()) {
			rationale.setEnabled(false);
			rationale.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !rationale.isEnabled()) {
			rationale.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getImage()
	 * 
	 */
	public String getImage() {
		return image.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setImage(String newValue)
	 * 
	 */
	public void setImage(String newValue) {
		if (newValue != null) {
			image.setText(newValue);
		} else {
			image.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.image);
		if (eefElementEditorReadOnlyState && image.isEnabled()) {
			image.setEnabled(false);
			image.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !image.isEnabled()) {
			image.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#getAnnotations()
	 * 
	 */
	public String getAnnotations() {
		return annotations.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#setAnnotations(String newValue)
	 * 
	 */
	public void setAnnotations(String newValue) {
		if (newValue != null) {
			annotations.setText(newValue);
		} else {
			annotations.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.annotations);
		if (eefElementEditorReadOnlyState && annotations.isEnabled()) {
			annotations.setEnabled(false);
			annotations.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !annotations.isEnabled()) {
			annotations.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#initOwnedRel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRel.setContentProvider(contentProvider);
		ownedRel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.ownedRel);
		if (eefElementEditorReadOnlyState && ownedRel.isEnabled()) {
			ownedRel.setEnabled(false);
			ownedRel.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRel.isEnabled()) {
			ownedRel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#updateOwnedRel()
	 * 
	 */
	public void updateOwnedRel() {
	ownedRel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#addFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter) {
		ownedRelFilters.add(filter);
		if (this.ownedRel != null) {
			this.ownedRel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#addBusinessFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter) {
		ownedRelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#isContainedInOwnedRelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element) {
		return ((ReferencesTableSettings)ownedRel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#initSubRequirement(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initSubRequirement(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		subRequirement.setContentProvider(contentProvider);
		subRequirement.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseRequirement.Properties.subRequirement);
		if (eefElementEditorReadOnlyState && subRequirement.isEnabled()) {
			subRequirement.setEnabled(false);
			subRequirement.setToolTipText(BaselineMessages.BaseRequirement_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !subRequirement.isEnabled()) {
			subRequirement.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#updateSubRequirement()
	 * 
	 */
	public void updateSubRequirement() {
	subRequirement.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#addFilterSubRequirement(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSubRequirement(ViewerFilter filter) {
		subRequirementFilters.add(filter);
		if (this.subRequirement != null) {
			this.subRequirement.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#addBusinessFilterSubRequirement(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSubRequirement(ViewerFilter filter) {
		subRequirementBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseRequirementPropertiesEditionPart#isContainedInSubRequirementTable(EObject element)
	 * 
	 */
	public boolean isContainedInSubRequirementTable(EObject element) {
		return ((ReferencesTableSettings)subRequirement.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseRequirement_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
