/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseActivityApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseActivityComplianceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseActivityEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseActivityPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseActivityRelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseActivityRequirementPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseActivitySelectionPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseApplicabilityLevelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseApplicabilityRelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseArtefactComplianceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseArtefactEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseArtefactPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseArtefactRelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseArtefactSelectionPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseComplianceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseControlCategoryPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseCriticalityApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseCriticalityLevelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseFrameworkPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseIndependencyLevelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRecommendationLevelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRequerimentComplianceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRequirementApplicabilityPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRequirementEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRequirementPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRequirementRelPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRequirementSelectionPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRolSelectionPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRoleComplianceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRoleEquivelanceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseRolePropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseTechniqueComplianceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseTechniqueEquivalenceMapPropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseTechniquePropertiesEditionPartForm;
import org.eclipse.opencert.apm.baseline.baseline.parts.forms.BaseTechniqueSelectionPropertiesEditionPartForm;

import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseActivityApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseActivityComplianceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseActivityEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseActivityPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseActivityRelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseActivityRequirementPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseActivitySelectionPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseApplicabilityLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseApplicabilityRelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseArtefactComplianceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseArtefactEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseArtefactPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseArtefactRelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseArtefactSelectionPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseComplianceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseControlCategoryPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseCriticalityApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseCriticalityLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseFrameworkPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseIndependencyLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRecommendationLevelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRequerimentComplianceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRequirementApplicabilityPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRequirementEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRequirementPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRequirementRelPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRequirementSelectionPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRolSelectionPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRoleComplianceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRoleEquivelanceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseRolePropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseTechniqueComplianceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseTechniqueEquivalenceMapPropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseTechniquePropertiesEditionPartImpl;
import org.eclipse.opencert.apm.baseline.baseline.parts.impl.BaseTechniqueSelectionPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class BaselinePropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == BaselineViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == BaselineViewsRepository.BaseFramework.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseFrameworkPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseFrameworkPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRequirement.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRequirementPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRequirementPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseArtefact.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseArtefactPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseArtefactPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseActivity.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseActivityPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseActivityPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRequirementRel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRequirementRelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRequirementRelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRole.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRolePropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRolePropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseApplicabilityLevel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseApplicabilityLevelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseApplicabilityLevelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseCriticalityLevel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseCriticalityLevelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseCriticalityLevelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseTechnique.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseTechniquePropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseTechniquePropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseArtefactRel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseArtefactRelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseArtefactRelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseCriticalityApplicability.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseCriticalityApplicabilityPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseCriticalityApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseActivityRel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseActivityRelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseActivityRelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseIndependencyLevel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseIndependencyLevelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseIndependencyLevelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRecommendationLevel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRecommendationLevelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRecommendationLevelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseControlCategory.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseControlCategoryPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseControlCategoryPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseApplicability.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseApplicabilityPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseApplicabilityRel.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseApplicabilityRelPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseApplicabilityRelPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseEquivalenceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseComplianceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseComplianceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseComplianceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseActivitySelection.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseActivitySelectionPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseActivitySelectionPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseActivityRequirement.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseActivityRequirementPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseActivityRequirementPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseActivityApplicability.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseActivityApplicabilityPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseActivityApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseActivityEquivalenceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseActivityEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseActivityEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseActivityComplianceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseActivityComplianceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseActivityComplianceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseArtefactSelection.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseArtefactSelectionPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseArtefactSelectionPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseArtefactEquivalenceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseArtefactEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseArtefactEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseArtefactComplianceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseArtefactComplianceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseArtefactComplianceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRolSelection.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRolSelectionPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRolSelectionPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRoleEquivelanceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRoleEquivelanceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRoleEquivelanceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRoleComplianceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRoleComplianceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRoleComplianceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRequirementSelection.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRequirementSelectionPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRequirementSelectionPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRequirementApplicability.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRequirementApplicabilityPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRequirementApplicabilityPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRequirementEquivalenceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRequirementEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRequirementEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseRequerimentComplianceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseRequerimentComplianceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseRequerimentComplianceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseTechniqueEquivalenceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseTechniqueEquivalenceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseTechniqueEquivalenceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseTechniqueComplianceMap.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseTechniqueComplianceMapPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseTechniqueComplianceMapPropertiesEditionPartForm(component);
		}
		if (key == BaselineViewsRepository.BaseTechniqueSelection.class) {
			if (kind == BaselineViewsRepository.SWT_KIND)
				return new BaseTechniqueSelectionPropertiesEditionPartImpl(component);
			if (kind == BaselineViewsRepository.FORM_KIND)
				return new BaseTechniqueSelectionPropertiesEditionPartForm(component);
		}
		return null;
	}

}
