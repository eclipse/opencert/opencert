/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.providers;

import org.eclipse.emf.common.notify.Adapter;

import org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory;

/**
 * 
 * 
 */
public class BaselineEEFAdapterFactory extends BaselineAdapterFactory {

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseFrameworkAdapter()
	 * 
	 */
	public Adapter createBaseFrameworkAdapter() {
		return new BaseFrameworkPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseRequirementAdapter()
	 * 
	 */
	public Adapter createBaseRequirementAdapter() {
		return new BaseRequirementPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseArtefactAdapter()
	 * 
	 */
	public Adapter createBaseArtefactAdapter() {
		return new BaseArtefactPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseActivityAdapter()
	 * 
	 */
	public Adapter createBaseActivityAdapter() {
		return new BaseActivityPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseRequirementRelAdapter()
	 * 
	 */
	public Adapter createBaseRequirementRelAdapter() {
		return new BaseRequirementRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseRoleAdapter()
	 * 
	 */
	public Adapter createBaseRoleAdapter() {
		return new BaseRolePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseApplicabilityLevelAdapter()
	 * 
	 */
	public Adapter createBaseApplicabilityLevelAdapter() {
		return new BaseApplicabilityLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseCriticalityLevelAdapter()
	 * 
	 */
	public Adapter createBaseCriticalityLevelAdapter() {
		return new BaseCriticalityLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseTechniqueAdapter()
	 * 
	 */
	public Adapter createBaseTechniqueAdapter() {
		return new BaseTechniquePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseArtefactRelAdapter()
	 * 
	 */
	public Adapter createBaseArtefactRelAdapter() {
		return new BaseArtefactRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseCriticalityApplicabilityAdapter()
	 * 
	 */
	public Adapter createBaseCriticalityApplicabilityAdapter() {
		return new BaseCriticalityApplicabilityPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseActivityRelAdapter()
	 * 
	 */
	public Adapter createBaseActivityRelAdapter() {
		return new BaseActivityRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseIndependencyLevelAdapter()
	 * 
	 */
	public Adapter createBaseIndependencyLevelAdapter() {
		return new BaseIndependencyLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseRecommendationLevelAdapter()
	 * 
	 */
	public Adapter createBaseRecommendationLevelAdapter() {
		return new BaseRecommendationLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseControlCategoryAdapter()
	 * 
	 */
	public Adapter createBaseControlCategoryAdapter() {
		return new BaseControlCategoryPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseApplicabilityAdapter()
	 * 
	 */
	public Adapter createBaseApplicabilityAdapter() {
		return new BaseApplicabilityPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseApplicabilityRelAdapter()
	 * 
	 */
	public Adapter createBaseApplicabilityRelAdapter() {
		return new BaseApplicabilityRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseEquivalenceMapAdapter()
	 * 
	 */
	public Adapter createBaseEquivalenceMapAdapter() {
		return new BaseEquivalenceMapPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.baseline.baseline.util.BaselineAdapterFactory#createBaseComplianceMapAdapter()
	 * 
	 */
	public Adapter createBaseComplianceMapAdapter() {
		return new BaseComplianceMapPropertiesEditionProvider();
	}

}
