/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseFrameworkPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseFrameworkPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for ownedActivities ReferencesTable
	 */
	protected ReferencesTableSettings ownedActivitiesSettings;
	
	/**
	 * Settings for ownedArtefact ReferencesTable
	 */
	protected ReferencesTableSettings ownedArtefactSettings;
	
	/**
	 * Settings for ownedRequirement ReferencesTable
	 */
	protected ReferencesTableSettings ownedRequirementSettings;
	
	/**
	 * Settings for ownedApplicLevel ReferencesTable
	 */
	protected ReferencesTableSettings ownedApplicLevelSettings;
	
	/**
	 * Settings for ownedCriticLevel ReferencesTable
	 */
	protected ReferencesTableSettings ownedCriticLevelSettings;
	
	/**
	 * Settings for ownedRole ReferencesTable
	 */
	protected ReferencesTableSettings ownedRoleSettings;
	
	/**
	 * Settings for ownedTechnique ReferencesTable
	 */
	protected ReferencesTableSettings ownedTechniqueSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseFrameworkPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseFramework, String editing_mode) {
		super(editingContext, baseFramework, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseFramework.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseFramework baseFramework = (BaseFramework)elt;
			final BaseFrameworkPropertiesEditionPart basePart = (BaseFrameworkPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseFramework.getId()));
			
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseFramework.getName()));
			
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseFramework.getDescription()));
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.scope))
				basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseFramework.getScope()));
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.rev))
				basePart.setRev(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseFramework.getRev()));
			
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.purpose))
				basePart.setPurpose(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseFramework.getPurpose()));
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.publisher))
				basePart.setPublisher(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseFramework.getPublisher()));
			
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.issued))
				basePart.setIssued(EEFConverterUtil.convertToString(EcorePackage.Literals.EDATE, baseFramework.getIssued()));
			
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedActivities)) {
				ownedActivitiesSettings = new ReferencesTableSettings(baseFramework, BaselinePackage.eINSTANCE.getBaseFramework_OwnedActivities());
				basePart.initOwnedActivities(ownedActivitiesSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact)) {
				ownedArtefactSettings = new ReferencesTableSettings(baseFramework, BaselinePackage.eINSTANCE.getBaseFramework_OwnedArtefact());
				basePart.initOwnedArtefact(ownedArtefactSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement)) {
				ownedRequirementSettings = new ReferencesTableSettings(baseFramework, BaselinePackage.eINSTANCE.getBaseFramework_OwnedRequirement());
				basePart.initOwnedRequirement(ownedRequirementSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel)) {
				ownedApplicLevelSettings = new ReferencesTableSettings(baseFramework, BaselinePackage.eINSTANCE.getBaseFramework_OwnedApplicLevel());
				basePart.initOwnedApplicLevel(ownedApplicLevelSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel)) {
				ownedCriticLevelSettings = new ReferencesTableSettings(baseFramework, BaselinePackage.eINSTANCE.getBaseFramework_OwnedCriticLevel());
				basePart.initOwnedCriticLevel(ownedCriticLevelSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedRole)) {
				ownedRoleSettings = new ReferencesTableSettings(baseFramework, BaselinePackage.eINSTANCE.getBaseFramework_OwnedRole());
				basePart.initOwnedRole(ownedRoleSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique)) {
				ownedTechniqueSettings = new ReferencesTableSettings(baseFramework, BaselinePackage.eINSTANCE.getBaseFramework_OwnedTechnique());
				basePart.initOwnedTechnique(ownedTechniqueSettings);
			}
			// init filters
			
			
			
			
			
			
			
			
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedActivities)) {
				basePart.addFilterToOwnedActivities(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseActivity); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedActivities
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact)) {
				basePart.addFilterToOwnedArtefact(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseArtefact); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedArtefact
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement)) {
				basePart.addFilterToOwnedRequirement(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseRequirement); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRequirement
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel)) {
				basePart.addFilterToOwnedApplicLevel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseApplicabilityLevel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedApplicLevel
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel)) {
				basePart.addFilterToOwnedCriticLevel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseCriticalityLevel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedCriticLevel
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedRole)) {
				basePart.addFilterToOwnedRole(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseRole); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRole
				// End of user code
			}
			if (isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique)) {
				basePart.addFilterToOwnedTechnique(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof BaseTechnique); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedTechnique
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}


















	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.scope) {
			return BaselinePackage.eINSTANCE.getBaseFramework_Scope();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.rev) {
			return BaselinePackage.eINSTANCE.getBaseFramework_Rev();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.purpose) {
			return BaselinePackage.eINSTANCE.getBaseFramework_Purpose();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.publisher) {
			return BaselinePackage.eINSTANCE.getBaseFramework_Publisher();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.issued) {
			return BaselinePackage.eINSTANCE.getBaseFramework_Issued();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.ownedActivities) {
			return BaselinePackage.eINSTANCE.getBaseFramework_OwnedActivities();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.ownedArtefact) {
			return BaselinePackage.eINSTANCE.getBaseFramework_OwnedArtefact();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.ownedRequirement) {
			return BaselinePackage.eINSTANCE.getBaseFramework_OwnedRequirement();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel) {
			return BaselinePackage.eINSTANCE.getBaseFramework_OwnedApplicLevel();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel) {
			return BaselinePackage.eINSTANCE.getBaseFramework_OwnedCriticLevel();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.ownedRole) {
			return BaselinePackage.eINSTANCE.getBaseFramework_OwnedRole();
		}
		if (editorKey == BaselineViewsRepository.BaseFramework.Properties.ownedTechnique) {
			return BaselinePackage.eINSTANCE.getBaseFramework_OwnedTechnique();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseFramework baseFramework = (BaseFramework)semanticObject;
		if (BaselineViewsRepository.BaseFramework.Properties.id == event.getAffectedEditor()) {
			baseFramework.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.name == event.getAffectedEditor()) {
			baseFramework.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.description == event.getAffectedEditor()) {
			baseFramework.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.scope == event.getAffectedEditor()) {
			baseFramework.setScope((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.rev == event.getAffectedEditor()) {
			baseFramework.setRev((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.purpose == event.getAffectedEditor()) {
			baseFramework.setPurpose((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.publisher == event.getAffectedEditor()) {
			baseFramework.setPublisher((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.issued == event.getAffectedEditor()) {
			baseFramework.setIssued((java.util.Date)EEFConverterUtil.createFromString(EcorePackage.Literals.EDATE, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseFramework.Properties.ownedActivities == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedActivitiesSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedActivitiesSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedActivitiesSettings.move(event.getNewIndex(), (BaseActivity) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseFramework.Properties.ownedArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedArtefactSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedArtefactSettings.move(event.getNewIndex(), (BaseArtefact) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseFramework.Properties.ownedRequirement == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRequirementSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRequirementSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRequirementSettings.move(event.getNewIndex(), (BaseRequirement) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedApplicLevelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedApplicLevelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedApplicLevelSettings.move(event.getNewIndex(), (BaseApplicabilityLevel) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedCriticLevelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedCriticLevelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedCriticLevelSettings.move(event.getNewIndex(), (BaseCriticalityLevel) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseFramework.Properties.ownedRole == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRoleSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRoleSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRoleSettings.move(event.getNewIndex(), (BaseRole) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseFramework.Properties.ownedTechnique == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedTechniqueSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedTechniqueSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedTechniqueSettings.move(event.getNewIndex(), (BaseTechnique) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseFrameworkPropertiesEditionPart basePart = (BaseFrameworkPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseFramework_Scope().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.scope)){
				if (msg.getNewValue() != null) {
					basePart.setScope(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setScope("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseFramework_Rev().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.rev)) {
				if (msg.getNewValue() != null) {
					basePart.setRev(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRev("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseFramework_Purpose().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.purpose)){
				if (msg.getNewValue() != null) {
					basePart.setPurpose(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setPurpose("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseFramework_Publisher().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.publisher)) {
				if (msg.getNewValue() != null) {
					basePart.setPublisher(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setPublisher("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseFramework_Issued().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseFramework.Properties.issued)) {
				if (msg.getNewValue() != null) {
					basePart.setIssued(EcoreUtil.convertToString(EcorePackage.Literals.EDATE, msg.getNewValue()));
				} else {
					basePart.setIssued("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseFramework_OwnedActivities().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedActivities))
				basePart.updateOwnedActivities();
			if (BaselinePackage.eINSTANCE.getBaseFramework_OwnedArtefact().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedArtefact))
				basePart.updateOwnedArtefact();
			if (BaselinePackage.eINSTANCE.getBaseFramework_OwnedRequirement().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedRequirement))
				basePart.updateOwnedRequirement();
			if (BaselinePackage.eINSTANCE.getBaseFramework_OwnedApplicLevel().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedApplicLevel))
				basePart.updateOwnedApplicLevel();
			if (BaselinePackage.eINSTANCE.getBaseFramework_OwnedCriticLevel().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedCriticLevel))
				basePart.updateOwnedCriticLevel();
			if (BaselinePackage.eINSTANCE.getBaseFramework_OwnedRole().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedRole))
				basePart.updateOwnedRole();
			if (BaselinePackage.eINSTANCE.getBaseFramework_OwnedTechnique().equals(msg.getFeature()) && isAccessible(BaselineViewsRepository.BaseFramework.Properties.ownedTechnique))
				basePart.updateOwnedTechnique();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			BaselinePackage.eINSTANCE.getBaseFramework_Scope(),
			BaselinePackage.eINSTANCE.getBaseFramework_Rev(),
			BaselinePackage.eINSTANCE.getBaseFramework_Purpose(),
			BaselinePackage.eINSTANCE.getBaseFramework_Publisher(),
			BaselinePackage.eINSTANCE.getBaseFramework_Issued(),
			BaselinePackage.eINSTANCE.getBaseFramework_OwnedActivities(),
			BaselinePackage.eINSTANCE.getBaseFramework_OwnedArtefact(),
			BaselinePackage.eINSTANCE.getBaseFramework_OwnedRequirement(),
			BaselinePackage.eINSTANCE.getBaseFramework_OwnedApplicLevel(),
			BaselinePackage.eINSTANCE.getBaseFramework_OwnedCriticLevel(),
			BaselinePackage.eINSTANCE.getBaseFramework_OwnedRole(),
			BaselinePackage.eINSTANCE.getBaseFramework_OwnedTechnique()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseFramework.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseFramework.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseFramework.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseFramework.Properties.scope == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseFramework_Scope().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseFramework_Scope().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseFramework.Properties.rev == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseFramework_Rev().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseFramework_Rev().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseFramework.Properties.purpose == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseFramework_Purpose().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseFramework_Purpose().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseFramework.Properties.publisher == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseFramework_Publisher().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseFramework_Publisher().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseFramework.Properties.issued == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseFramework_Issued().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseFramework_Issued().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
