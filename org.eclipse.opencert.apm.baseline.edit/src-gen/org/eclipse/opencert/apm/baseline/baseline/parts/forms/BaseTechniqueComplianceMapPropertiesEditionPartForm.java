/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts.forms;

// Start of user code for imports


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.apm.baseline.baseline.providers.BaselineMessages;
import org.eclipse.opencert.pam.procspec.process.Technique;

// End of user code

/**
 * 
 * 
 */
public class BaseTechniqueComplianceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, BaseTechniqueComplianceMapPropertiesEditionPart {

	protected ReferencesTable complianceMap;
	protected List<ViewerFilter> complianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> complianceMapFilters = new ArrayList<ViewerFilter>();
	protected TableViewer techniqueComplianceMap;
	protected List<ViewerFilter> techniqueComplianceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> techniqueComplianceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addTechniqueComplianceMap;
	protected Button removeTechniqueComplianceMap;
	protected Button editTechniqueComplianceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public BaseTechniqueComplianceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public BaseTechniqueComplianceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence baseTechniqueComplianceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = baseTechniqueComplianceMapStep.addStep(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap);
		// End IRR
		propertiesStep.addStep(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap);
		
		
		composer = new PartComposer(baseTechniqueComplianceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap) {
					return createComplianceMapTableComposition(widgetFactory, parent);
				}
				if (key == BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap) {
					return createTechniqueComplianceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(BaselineMessages.BaseTechniqueComplianceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createComplianceMapTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.complianceMap = new ReferencesTable(getDescription(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap, BaselineMessages.BaseTechniqueComplianceMapPropertiesEditionPart_ComplianceMapLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				complianceMap.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				complianceMap.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				complianceMap.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				complianceMap.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.complianceMapFilters) {
			this.complianceMap.addFilter(filter);
		}
		this.complianceMap.setHelpText(propertiesEditionComponent.getHelpContent(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap, BaselineViewsRepository.FORM_KIND));
		this.complianceMap.createControls(parent, widgetFactory);
		this.complianceMap.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData complianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		complianceMapData.horizontalSpan = 3;
		this.complianceMap.setLayoutData(complianceMapData);
		this.complianceMap.setLowerBound(0);
		this.complianceMap.setUpperBound(-1);
		complianceMap.setID(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap);
		complianceMap.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createComplianceMapTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createTechniqueComplianceMapTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableTechniqueComplianceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableTechniqueComplianceMap.setHeaderVisible(true);
		GridData gdTechniqueComplianceMap = new GridData();
		gdTechniqueComplianceMap.grabExcessHorizontalSpace = true;
		gdTechniqueComplianceMap.horizontalAlignment = GridData.FILL;
		gdTechniqueComplianceMap.grabExcessVerticalSpace = true;
		gdTechniqueComplianceMap.verticalAlignment = GridData.FILL;
		tableTechniqueComplianceMap.setLayoutData(gdTechniqueComplianceMap);
		tableTechniqueComplianceMap.setLinesVisible(true);

		// Start of user code for columns definition for TechniqueComplianceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableTechniqueComplianceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableTechniqueComplianceMap, SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableTechniqueComplianceMap, SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableTechniqueComplianceMap, SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Techniques"); //$NON-NLS-1$
		// End IRR
		// End of user code
		
		techniqueComplianceMap = new TableViewer(tableTechniqueComplianceMap);
		techniqueComplianceMap.setContentProvider(new ArrayContentProvider());
		techniqueComplianceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for TechniqueComplianceMap
			public String getColumnText(Object object, int columnIndex) {
				//Start IRR
				/*AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
				if (object instanceof EObject) {
					switch (columnIndex) {
					case 0:
						return labelProvider.getText(object);
					case 1:
						return labelProvider.getText(object);
					}
				}*/
				if (object instanceof EObject) {
					BaseComplianceMap baselineComplianceMapObject = (BaseComplianceMap)object;
					switch (columnIndex) {
					case 0:							
						return baselineComplianceMapObject.getId();
					case 1:							
						if (baselineComplianceMapObject.getMapGroup() == null)
							return "";
						else
							return baselineComplianceMapObject.getMapGroup().getName();
					case 2:							
						if (baselineComplianceMapObject.getTarget() == null)
							return "";
						else{
							EList<AssuranceAsset> LstAssurance = baselineComplianceMapObject.getTarget();
							Iterator<AssuranceAsset> iter = LstAssurance.iterator();
							String sArtefact = "[";
							while (iter.hasNext()) {
								AssuranceAsset refManAss = (AssuranceAsset) iter.next();
								
								if (refManAss instanceof Technique) {
									Technique art = (Technique) refManAss;
									sArtefact = sArtefact + art.getName() + ",";  
								}										
							}
							//delete ,
							sArtefact = sArtefact.substring(0, sArtefact.length()-1);
							sArtefact = sArtefact + "]";
							return sArtefact;
						}
							
					}
				}
				//End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		techniqueComplianceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (techniqueComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						techniqueComplianceMap.refresh();
					}
				}
			}

		});
		GridData techniqueComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		techniqueComplianceMapData.minimumHeight = 120;
		techniqueComplianceMapData.heightHint = 120;
		techniqueComplianceMap.getTable().setLayoutData(techniqueComplianceMapData);
		for (ViewerFilter filter : this.techniqueComplianceMapFilters) {
			techniqueComplianceMap.addFilter(filter);
		}
		EditingUtils.setID(techniqueComplianceMap.getTable(), BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap);
		EditingUtils.setEEFtype(techniqueComplianceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createTechniqueComplianceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createTechniqueComplianceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createTechniqueComplianceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite techniqueComplianceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout techniqueComplianceMapPanelLayout = new GridLayout();
		techniqueComplianceMapPanelLayout.numColumns = 1;
		techniqueComplianceMapPanel.setLayout(techniqueComplianceMapPanelLayout);
		addTechniqueComplianceMap = widgetFactory.createButton(techniqueComplianceMapPanel, BaselineMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addTechniqueComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addTechniqueComplianceMap.setLayoutData(addTechniqueComplianceMapData);
		addTechniqueComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				techniqueComplianceMap.refresh();
			}
		});
		EditingUtils.setID(addTechniqueComplianceMap, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap);
		EditingUtils.setEEFtype(addTechniqueComplianceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeTechniqueComplianceMap = widgetFactory.createButton(techniqueComplianceMapPanel, BaselineMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeTechniqueComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeTechniqueComplianceMap.setLayoutData(removeTechniqueComplianceMapData);
		removeTechniqueComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (techniqueComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						techniqueComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeTechniqueComplianceMap, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap);
		EditingUtils.setEEFtype(removeTechniqueComplianceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editTechniqueComplianceMap = widgetFactory.createButton(techniqueComplianceMapPanel, BaselineMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editTechniqueComplianceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editTechniqueComplianceMap.setLayoutData(editTechniqueComplianceMapData);
		editTechniqueComplianceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (techniqueComplianceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueComplianceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(BaseTechniqueComplianceMapPropertiesEditionPartForm.this, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						techniqueComplianceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editTechniqueComplianceMap, BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap);
		EditingUtils.setEEFtype(editTechniqueComplianceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createTechniqueComplianceMapPanel

		// End of user code
		return techniqueComplianceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#initComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		complianceMap.setContentProvider(contentProvider);
		complianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.complianceMap);
		if (eefElementEditorReadOnlyState && complianceMap.isEnabled()) {
			complianceMap.setEnabled(false);
			complianceMap.setToolTipText(BaselineMessages.BaseTechniqueComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !complianceMap.isEnabled()) {
			complianceMap.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#updateComplianceMap()
	 * 
	 */
	public void updateComplianceMap() {
	complianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#addFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToComplianceMap(ViewerFilter filter) {
		complianceMapFilters.add(filter);
		if (this.complianceMap != null) {
			this.complianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#addBusinessFilterComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToComplianceMap(ViewerFilter filter) {
		complianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#isContainedInComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)complianceMap.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#initTechniqueComplianceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initTechniqueComplianceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		techniqueComplianceMap.setContentProvider(contentProvider);
		techniqueComplianceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(BaselineViewsRepository.BaseTechniqueComplianceMap.Properties.techniqueComplianceMap);
		if (eefElementEditorReadOnlyState && techniqueComplianceMap.getTable().isEnabled()) {
			techniqueComplianceMap.getTable().setEnabled(false);
			techniqueComplianceMap.getTable().setToolTipText(BaselineMessages.BaseTechniqueComplianceMap_ReadOnly);
			addTechniqueComplianceMap.setEnabled(false);
			addTechniqueComplianceMap.setToolTipText(BaselineMessages.BaseTechniqueComplianceMap_ReadOnly);
			removeTechniqueComplianceMap.setEnabled(false);
			removeTechniqueComplianceMap.setToolTipText(BaselineMessages.BaseTechniqueComplianceMap_ReadOnly);
			editTechniqueComplianceMap.setEnabled(false);
			editTechniqueComplianceMap.setToolTipText(BaselineMessages.BaseTechniqueComplianceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !techniqueComplianceMap.getTable().isEnabled()) {
			techniqueComplianceMap.getTable().setEnabled(true);
			addTechniqueComplianceMap.setEnabled(true);
			removeTechniqueComplianceMap.setEnabled(true);
			editTechniqueComplianceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#updateTechniqueComplianceMap()
	 * 
	 */
	public void updateTechniqueComplianceMap() {
	techniqueComplianceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#addFilterTechniqueComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTechniqueComplianceMap(ViewerFilter filter) {
		techniqueComplianceMapFilters.add(filter);
		if (this.techniqueComplianceMap != null) {
			this.techniqueComplianceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#addBusinessFilterTechniqueComplianceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTechniqueComplianceMap(ViewerFilter filter) {
		techniqueComplianceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniqueComplianceMapPropertiesEditionPart#isContainedInTechniqueComplianceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInTechniqueComplianceMapTable(EObject element) {
		return ((ReferencesTableSettings)techniqueComplianceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return BaselineMessages.BaseTechniqueComplianceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
