/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

public class SvnProperties
{
    private String svnUrl;
    private String svnUser;
    private String svnPassword;
        
    public SvnProperties(String svnUrl, String svnUser, String svnPassword)
    {
        super();
        this.svnUrl = svnUrl;
        this.svnUser = svnUser;
        this.svnPassword = svnPassword;
    }
    
    public String getSvnUrl()
    {
        return svnUrl;
    }
    
    public void setSvnUrl(String svnUrl)
    {
        this.svnUrl = svnUrl;
    }
    
    public String getSvnUser()
    {
        return svnUser;
    }
    
    public void setSvnUser(String svnUser)
    {
        this.svnUser = svnUser;
    }
    
    public String getSvnPassword()
    {
        return svnPassword;
    }
    
    public void setSvnPassword(String svnPassword)
    {
        this.svnPassword = svnPassword;
    }
    
    
}
