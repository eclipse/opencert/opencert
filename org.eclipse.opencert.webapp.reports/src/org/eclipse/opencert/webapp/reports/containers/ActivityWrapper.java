/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import com.vaadin.ui.Label;

public class ActivityWrapper {
    private Long cdoId;
    private Label name;
    private Label id;
    private Label description;
    private Label parentModel;
    private Label startTime;
    private Label endTime;
    private Label participant;
    private Label precedingActivity;
    private Label requiredArtefact;
    private Label producedArtefact;
    private Label evaluation;
    private Label assuranceAssetEvent;
    
    public ActivityWrapper(Long cdoId, Label name, Label id, Label description, Label parentModel, Label startTime, Label endTime,
    		Label participant, Label precedingActivity, Label requiredArtefact, Label producedArtefact, Label evaluation,
    		Label assuranceAssetEvent) {
    	this.cdoId = cdoId;
    	this.name = name;
    	this.id = id;
    	this.description = description;
    	this.parentModel = parentModel;
    	this.startTime = startTime;
    	this.endTime = endTime;
    	this.participant = participant;
    	this.precedingActivity = precedingActivity;
    	this.requiredArtefact = requiredArtefact;
    	this.producedArtefact = producedArtefact;
    	this.evaluation = evaluation;
    	this.assuranceAssetEvent = assuranceAssetEvent;
    }
    
	public Long getCdoId() {
		return cdoId;
	}

	public void setCdoId(Long cdoId) {
		this.cdoId = cdoId;
	}
    
	public Label getName() {
		return name;
	}

	public void setName(Label name) {
		this.name = name;
	}
	
	public Label getId() {
		return id;
	}

	public void setId(Label id) {
		this.id = id;
	}
	
	public Label getDescription() {
		return description;
	}

	public void setDescription(Label description) {
		this.description = description;
	}
	
	public Label getParentModel() {
		return parentModel;
	}

	public void setParentModel(Label parentModel) {
		this.parentModel = parentModel;
	}
	
	public Label getStartTime() {
		return startTime;
	}

	public void setStartTime(Label startTime) {
		this.startTime = startTime;
	}
	
	public Label getEndTime() {
		return endTime;
	}

	public void setEndTime(Label endTime) {
		this.endTime = endTime;
	}
	
	public Label getParticipant() {
		return participant;
	}

	public void setParticipant(Label participant) {
		this.participant = participant;
	}
	
	public Label getPrecedingActivity() {
		return precedingActivity;
	}

	public void setPrecedingActivity(Label precedingActivity) {
		this.precedingActivity = precedingActivity;
	}
	
	public Label getRequiredArtefact() {
		return requiredArtefact;
	}

	public void setRequiredArtefact(Label requiredArtefact) {
		this.requiredArtefact = requiredArtefact;
	}
	
	public Label getProducedArtefact() {
		return producedArtefact;
	}

	public void setProducedArtefact(Label producedArtefact) {
		this.producedArtefact = producedArtefact;
	}
	
	public Label getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Label evaluation) {
		this.evaluation = evaluation;
	}
	
	public Label getAssuranceAssetEvent() {
		return assuranceAssetEvent;
	}

	public void setAssuranceAssetEvent(Label assuranceAssetEvent) {
		this.assuranceAssetEvent = assuranceAssetEvent;
	}
}
