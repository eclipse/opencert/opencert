/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import java.util.Collection;

public class ComplianceDetails {
	private String complianceMapJustification;
	private String complianceMapId;
	private String complianceMapName;
	private String complianceMapType;
	private Collection<ComplianceAsset> complianceAssets;
	private String baseAssetType;
	private Long complianceMapCdoId;
	private Long baseAssetId;
	private String baseAssetName;
	
	public ComplianceDetails(String complianceMapJustification, String complianceMapId, String complianceMapName, String complianceMapType, 
			Collection<ComplianceAsset> complianceAssets, String baseAssetType, Long complianceMapCdoId, Long baseAssetId, String baseAssetName) {
		this.complianceMapJustification = complianceMapJustification;
		this.complianceMapId = complianceMapId;
		this.complianceMapName = complianceMapName;
		this.complianceMapType = complianceMapType;
		this.complianceAssets = complianceAssets;
		this.baseAssetType = baseAssetType;
		this.complianceMapCdoId = complianceMapCdoId;
		this.baseAssetId = baseAssetId;
		this.baseAssetName = baseAssetName;
	}
	
	public ComplianceDetails() {
	}

	public String getComplianceMapJustification() {
		return this.complianceMapJustification;
	}
	
	public void setComplianceMapJustification(String complianceMapJustification) {
		this.complianceMapJustification = complianceMapJustification;
	}
	
	public String getComplianceMapId() {
		return this.complianceMapId;
	}
	
	public void setComplianceMapId(String complianceMapId) {
		this.complianceMapId = complianceMapId;
	}
	
	public String getComplianceMapName() {
		return this.complianceMapName;
	}
	
	public void setComplianceMapName(String complianceMapName) {
		this.complianceMapName = complianceMapName;
	}
	
	public String getComplianceMapType() {
		return this.complianceMapType;
	}

	public void setComplianceMapType(String complianceMapType) {
		this.complianceMapType = complianceMapType;
	}

	public Collection<ComplianceAsset> getComplianceAssets() {
		return this.complianceAssets;
	}
	
	public void setComplianceAssets(Collection<ComplianceAsset> complianceAssets) {
		this.complianceAssets = complianceAssets;
	}
	
	public String getBaseAssetType() {
		return this.baseAssetType;
	}
	
	public void setBaseAssetType(String baseAssetType) {
		this.baseAssetType = baseAssetType;
	}
	
	public Long getComplianceMapCdoId() {
		return this.complianceMapCdoId;
	}
	
	public void setComplianceMapCdoId(Long complianceMapCdoId) {
		this.complianceMapCdoId = complianceMapCdoId;
	}
	
	public Long getBaseAssetId() {
		return this.baseAssetId;
	}
	
	public void setBaseAssetId(Long baseAssetId) {
		this.baseAssetId = baseAssetId;
	}
	
	public String getBaseAssetName() {
		return this.baseAssetName;
	}
	
	public void setBaseAssetName(String baseAssetName) {
		this.baseAssetName = baseAssetName;
	}
}
