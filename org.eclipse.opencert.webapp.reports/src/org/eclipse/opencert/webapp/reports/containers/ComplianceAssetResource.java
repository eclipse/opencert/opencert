/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

public class ComplianceAssetResource {
	private String id;
	private String name;
	private String description;
	private String location;
	private String format;
	private String repoUrl;
	private Long cdoId;
	
	public ComplianceAssetResource(String id, String name, String description, String location, String format, String repoUrl, Long cdoId) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.location = location;
		this.format = format;
		this.repoUrl = repoUrl;
		this.cdoId = cdoId;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getFormat() {
		return format;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
	
	public String getRepoUrl() {
		return repoUrl;
	}
	
	public void setRepoUrl(String repoUrl) {
		this.repoUrl = repoUrl;
	}
	
	public Long getCdoId() {
		return this.cdoId;
	}
	
	public void setId(Long cdoId) {
		this.cdoId = cdoId;
	}
}
