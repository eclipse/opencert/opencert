/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.util.StringUtil;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class BaseArtefactRelationsField 
    extends HorizontalLayout
{
    private static final String RELATED_ID_LABEL_CSS = "relatedIDLabel";

    private static final String RELATED_ID_LABEL_REMOVED_NOT_SAVED_CSS = "relatedIDLabelRemovedNotSaved";

    private static final String RELATED_ID_LABEL_ADDED_NOT_SAVED_CSS = "relatedIDLabelAddedNotSaved";

    private List<Long> relatedIds = new LinkedList<>();
    
    private HorizontalLayout isRelatedPanel;
    private CheckBox isRelatedCheckbox;
    private Image isRelatedHelp;

    private long cdoID;

    private boolean isInitiallyInitialized = false;
    private boolean initiallyChecked;
    private String initialMsg = null;

    private IRelationChangedLister relationChangedLister;

    private VerticalLayout relatedIdLabels;

    public BaseArtefactRelationsField()
    {
        init(-1, null);
    }
    
    public BaseArtefactRelationsField(long itemID, List<BaseArtefact> affectedBaseArtefacts)
    {
        init(itemID, affectedBaseArtefacts);
    }

    public List<Long> getRelationIDs()
    {
        return Collections.unmodifiableList(relatedIds);
    }
    
    public void setCheckboxVisible(boolean isVisible)
    {
        boolean forceVis = isVisible;
        if (ProjectBaselineArtefactTable.RowTempIDGenerator.isTempID(cdoID))
        {
            forceVis = false;
        }
        isRelatedPanel.setVisible(forceVis);
    }

    public void setCheckboxMarked(boolean isMarked, String msg)
    {
        if (!isInitiallyInitialized) {
            initiallyChecked = isMarked;
            initialMsg = msg;
            isInitiallyInitialized = true;
        }
        isRelatedCheckbox.setValue(isMarked);
        setRelationTooltip(msg);
    }
    
    public boolean isChecked()
    {
        return isRelatedCheckbox.getValue();
    }

    private void setRelationTooltip(String msg)
    {
        isRelatedCheckbox.setDescription(msg);
        isRelatedHelp.setDescription(msg);
        isRelatedHelp.setAlternateText(msg);
        isRelatedPanel.setDescription(msg);
    }

    private void init(long cdoID, List<BaseArtefact> affectedBaseArtefacts)
    {
        this.cdoID = cdoID;
        
        isRelatedCheckbox = new CheckBox();
        isRelatedCheckbox.addValueChangeListener(e -> 
        {
            notifyRelationChangedListener(isRelatedCheckbox.getValue());
            if (initiallyChecked && !isRelatedCheckbox.getValue()) {
                setRelationTooltip("The relation has been unmarked. Please press [SAVE] to preserve your changes.");
            } 
            else if (initiallyChecked && isRelatedCheckbox.getValue()) {
                setRelationTooltip(initialMsg);
            }
            else if (!initiallyChecked && !isRelatedCheckbox.getValue()) {
                setRelationTooltip(initialMsg);
            } 
            else if (!initiallyChecked && isRelatedCheckbox.getValue()) {
                setRelationTooltip("The relation between the edited artefacts has been marked. Please press [SAVE] to preserve your changes.");
            }
        });
        
        isRelatedHelp = new Image(null, new ThemeResource("images/help.png"));
        isRelatedPanel = new HorizontalLayout();
        isRelatedPanel.setStyleName("isRelatedPanel");
        isRelatedPanel.addComponent(isRelatedCheckbox);
        isRelatedPanel.addComponent(isRelatedHelp);
        
        relatedIdLabels = new VerticalLayout();
        relatedIdLabels.setWidth("60px");
        if (affectedBaseArtefacts != null) {
            initRelatedIdLabels(affectedBaseArtefacts);
        }
        
        addComponent(relatedIdLabels);
        Label spacer = new Label();
        spacer.setWidth("100%");
        addComponent(spacer);
        setExpandRatio(spacer, 1.0f);
        addComponent(isRelatedPanel);
        
        setCheckboxVisible(false);
    }

    private void notifyRelationChangedListener(boolean isChecked)
    {
        if (relationChangedLister != null) {
            relationChangedLister.relationMarked(isChecked, cdoID);
        }
    }

    public void setRelationChangedListener(IRelationChangedLister relationChangedLister)
    {
        this.relationChangedLister = relationChangedLister;
    }
    
    private void initRelatedIdLabels(List<BaseArtefact> affectedBaseArtefacts)
    {
        for (BaseArtefact affectedBaseArt : affectedBaseArtefacts)
        {            
            final Long cdoID = CDOStorageUtil.getCDOId(affectedBaseArt);
            this.relatedIds.add(cdoID);
            String cdoIDTxt = String.valueOf(cdoID);
            Label label = new Label(cdoIDTxt);
            label.setStyleName(RELATED_ID_LABEL_CSS);
            label.setDescription(
                    StringUtil.ensureNotNull(affectedBaseArt.getName()) 
                    + "<BR/>" 
                    + StringUtil.ensureNotNull(affectedBaseArt.getDescription()));
            relatedIdLabels.addComponent(label);
        }
    }
    
    public void resetRelatedIdLabels()
    {
        Iterator<Component> iterator = relatedIdLabels.iterator();
        List<Label> labelsToRemove = new LinkedList<>();
        
        while (iterator.hasNext())
        {
            Component c = iterator.next();
            if (!(c instanceof Label)) {
                continue;
            }
            Label label = (Label)c;
            if (RELATED_ID_LABEL_ADDED_NOT_SAVED_CSS.equals(label.getStyleName())) 
            {
                labelsToRemove.add(label);
                continue;
            }
            if (RELATED_ID_LABEL_REMOVED_NOT_SAVED_CSS.equals(label.getStyleName()))
            {
                label.setStyleName(RELATED_ID_LABEL_CSS);
            }
        }
        
        labelsToRemove.forEach(e -> relatedIdLabels.removeComponent(e));
    }


    public void markEditedRelatedIDLabel(long relatedID, boolean setMarked)
    {
        Label label = findRelatedIDLabel(relatedID);
        if (setMarked) {        
            if (label == null) {
                label = new Label(String.valueOf(relatedID));
                relatedIdLabels.addComponent(label);
                label.setStyleName(RELATED_ID_LABEL_ADDED_NOT_SAVED_CSS);
            } else {
                label.setStyleName(RELATED_ID_LABEL_CSS);
            }
        } 
        if (!setMarked) {
            if (label == null) {
                throw new IllegalStateException();
            }
            if (relatedIds.contains(relatedID)) {
                label.setStyleName(RELATED_ID_LABEL_REMOVED_NOT_SAVED_CSS);                        
            } else {
                relatedIdLabels.removeComponent(label);
            }
        }
    }

    private Label findRelatedIDLabel(long relatedID)
    {
        Iterator<Component> iterator = relatedIdLabels.iterator();
        Label label = null;
        while (iterator.hasNext())
        {
            Component c = iterator.next();
            if (!(c instanceof Label)) {
                continue;
            }
            if (String.valueOf(relatedID).equals(((Label) c).getValue())) {
                label = (Label)c;
            }
        }
        return label;
    }

    public void setNewRowRelationPreservedMode()
    {
        this.removeAllComponents();
        Label label = new Label("[All relations will be preserved]");
        label.addStyleName("relatedIDLabelToBePreservedInfo");
        this.addComponent(label);
    }
}


