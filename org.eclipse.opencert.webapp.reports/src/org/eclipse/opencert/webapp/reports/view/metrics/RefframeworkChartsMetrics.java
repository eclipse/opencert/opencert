/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.List;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Container.Hierarchical;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

import org.eclipse.opencert.webapp.reports.manager.BaselineManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.metrics.JFreeChartWrapper;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;




@Theme("opencerttheme")
public class RefframeworkChartsMetrics 
	extends CustomComponent

{


	private static final long serialVersionUID = -2664201121356644398L;
	private static final String NO_DATA_AVAILABLE = "No Data Available";
	private static final String REFFRAMEWORK_COVERAGE = "Refframework Coverage";
	
	private byte[] coverage;
	private Hierarchical dataDescription;
    

                      
    public String getRefframeworkCoverage() {
		return REFFRAMEWORK_COVERAGE;
	}

	public byte[] getCoverage() {
		return coverage;
	}

	public Hierarchical getDataContainer() {
		return dataDescription;
	}

	public RefframeworkChartsMetrics(long baselineFrameworkID)
    {      
         
        VerticalLayout mainPanel = new VerticalLayout();
		
		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);

		
		BaselineManager baselineManager = (BaselineManager)SpringContextHelper.getBeanFromWebappContext(BaselineManager.SPRING_NAME);
        
		List<BaseRequirement> nRequirementsList = baselineManager.getBaseRequirements(baselineFrameworkID,true,true);
		int nRequirements = nRequirementsList.size();
		List<BaseRequirement> nUnselectedRequirementsList =  baselineManager.getBaseRequirements(baselineFrameworkID,false,true);
		int nUnselectedRequirements = nUnselectedRequirementsList.size();
		
		List<BaseArtefact> nArtefactList = baselineManager.getBaseArtefacts(baselineFrameworkID,true);
		int nArtefact = nArtefactList.size();
		List<BaseArtefact> nUnselectedArtefactList = baselineManager.getBaseArtefacts(baselineFrameworkID,false);
		int nUnselectedArtefact = nUnselectedArtefactList.size();
		
		List<BaseRole> nRoleList = baselineManager.getBaseRoles(baselineFrameworkID,true);
		int nRole = nRoleList.size();
		List<BaseRole> nUnselectedRoleList = baselineManager.getBaseRoles(baselineFrameworkID,false);
		int nUnselectedRole = nUnselectedRoleList.size();
		
		List<BaseTechnique> nTechniqueList = baselineManager.getBaseTechniques(baselineFrameworkID,true);
		int nTechnique = nTechniqueList.size();
		List<BaseTechnique> nUnselectedTechniqueList = baselineManager.getBaseTechniques(baselineFrameworkID,false);
		int nUnselectedTechnique = nUnselectedTechniqueList.size();
		
		List<BaseActivity> nActivityList = baselineManager.getBaseActivities(baselineFrameworkID,true,true);
		int nActivity = nActivityList.size();
		List<BaseActivity> nUnselectedActivityList = baselineManager.getBaseActivities(baselineFrameworkID,false,true);
		int nUnselectedActivity = nUnselectedActivityList.size();
		

		DefaultCategoryDataset result = new DefaultCategoryDataset();

        if(nRequirements > 0) result.addValue(nRequirements, "Covered", "Requirements");
        if(nUnselectedRequirements > 0) result.addValue(nUnselectedRequirements, "Not Covered", "Requirements");
        if(nRequirements == 0 && nUnselectedRequirements == 0) result.addValue(0, "Not Covered", "Requirements");
        
        if(nArtefact > 0) result.addValue(nArtefact, "Covered", "Artefacts");
        if(nUnselectedArtefact > 0) result.addValue(nUnselectedArtefact, "Not Covered", "Artefacts");
        if(nArtefact == 0 && nUnselectedArtefact == 0) result.addValue(0, "Not Covered", "Artefacts");
        
        if(nRole > 0) result.addValue(nRole, "Covered", "Roles");
        if(nUnselectedRole > 0) result.addValue(nUnselectedRole, "Not Covered", "Roles");
        if(nRole == 0 && nUnselectedRole == 0) result.addValue(0, "Not Covered", "Roles");
        
        if(nTechnique > 0) result.addValue(nTechnique, "Covered", "Techniques");
        if(nUnselectedTechnique > 0) result.addValue(nUnselectedTechnique, "Not Covered", "Techniques");
        if(nTechnique == 0 && nUnselectedTechnique == 0) result.addValue(0, "Not Covered", "Techniques");
        
        if(nActivity > 0) result.addValue(nActivity, "Covered", "Activities");
        if(nUnselectedActivity > 0) result.addValue(nUnselectedActivity, "Not Covered", "Activities");
        if(nActivity == 0 && nUnselectedActivity == 0) result.addValue(0, "Not Covered", "Activities");
        
        mainPanel.addComponent(createNewMetrics(result));
        mainPanel.addComponent(createData(nRequirementsList, nUnselectedRequirementsList, 
        									nArtefactList, nUnselectedArtefactList, 
        									nRoleList, nUnselectedRoleList, 
        									nTechniqueList, nUnselectedTechniqueList, 
        									nActivityList, nUnselectedActivityList));

		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
        
 
    }

    private Component createData(List<BaseRequirement> nRequirementsList, List<BaseRequirement> nUnselectedRequirementsList,
									List<BaseArtefact> nArtefactList, List<BaseArtefact> nUnselectedArtefactList,
									List<BaseRole> nRoleList, List<BaseRole> nUnselectedRoleList,
									List<BaseTechnique> nTechniqueList, List<BaseTechnique> nUnselectedTechniqueList,
									List<BaseActivity> nActivityList, List<BaseActivity> nUnselectedActivityList) {
    	
    	final TreeTable treetable = new TreeTable();
		treetable.setWidth("100%");
		treetable.addStyleName("resourceEfficiencyTable");

		// Define two columns for the built-in container
		treetable.addContainerProperty("Type", String.class, "");
		treetable.addContainerProperty("Description",  String.class, "");
    	
		Object requirement = treetable.addItem(new Object[]{"Requirements ", ""},null); 
		treetable.setCollapsed(requirement, false);
		//requirements
		if (nRequirementsList.size()>0){
			Object covered = treetable.addItem(new Object[]{"Covered: ", ""},null);
			treetable.setParent(covered,requirement);
			treetable.setCollapsed(covered, false);
			for(BaseRequirement baseRequirement : nRequirementsList){
				Object br = treetable.addItem(new Object[]{"Requirement ID: " + baseRequirement.getId() + " Name: " + baseRequirement.getName(), baseRequirement.getDescription()},null);
				treetable.setParent(br,covered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		if (nUnselectedRequirementsList.size()>0){
			Object uncovered = treetable.addItem(new Object[]{"Not Covered: ", ""},null);
			treetable.setParent(uncovered,requirement);
			treetable.setCollapsed(uncovered, false);
			for(BaseRequirement baseRequirement : nUnselectedRequirementsList){
				Object br = treetable.addItem(new Object[]{"Requirement ID: " + baseRequirement.getId() + " Name: " + baseRequirement.getName(), baseRequirement.getDescription()},null);
				treetable.setParent(br,uncovered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		
		Object artefacts = treetable.addItem(new Object[]{"Artefacts ", ""},null);
		treetable.setCollapsed(artefacts, false);
		//artefacts
		if (nArtefactList.size()>0){
			Object covered = treetable.addItem(new Object[]{"Covered: ", ""},null);
			treetable.setParent(covered,artefacts);
			treetable.setCollapsed(covered, false);
			for(BaseArtefact baseArtefact : nArtefactList){
				Object br = treetable.addItem(new Object[]{"Artefact ID: " + baseArtefact.getId() + " Name: " + baseArtefact.getName(), baseArtefact.getDescription()},null);
				treetable.setParent(br,covered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		if (nUnselectedArtefactList.size()>0){
			Object uncovered = treetable.addItem(new Object[]{"Not Covered: ", ""},null);
			treetable.setParent(uncovered,artefacts);
			treetable.setCollapsed(uncovered, false);
			for(BaseArtefact baseArtefact : nUnselectedArtefactList){
				Object br = treetable.addItem(new Object[]{"Artefact ID: " + baseArtefact.getId() + " Name: " + baseArtefact.getName(), baseArtefact.getDescription()},null);
				treetable.setParent(br,uncovered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		
		
		Object roles = treetable.addItem(new Object[]{"Roles ", ""},null);
		treetable.setCollapsed(roles, false);
		//roles
		if (nRoleList.size()>0){
			Object covered = treetable.addItem(new Object[]{"Covered: ", ""},null);
			treetable.setParent(covered,roles);
			treetable.setCollapsed(covered, false);
			for(BaseRole baseRole : nRoleList){
				Object br = treetable.addItem(new Object[]{"Role ID: " + baseRole.getId() + " Name: " + baseRole.getName(), baseRole.getDescription()},null);
				treetable.setParent(br,covered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		if (nUnselectedRoleList.size()>0){
			Object uncovered = treetable.addItem(new Object[]{"Not Covered: ", ""},null);
			treetable.setParent(uncovered,roles);
			treetable.setCollapsed(uncovered, false);
			for(BaseRole baseRole : nUnselectedRoleList){
				Object br = treetable.addItem(new Object[]{"Role ID: " + baseRole.getId() + " Name: " + baseRole.getName(), baseRole.getDescription()},null);
				treetable.setParent(br,uncovered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		
		
		
		Object techniques = treetable.addItem(new Object[]{"Techniques ", ""},null);
		treetable.setCollapsed(techniques, false);
		//techniques
		if (nTechniqueList.size()>0){
			Object covered = treetable.addItem(new Object[]{"Covered: ", ""},null);
			treetable.setParent(covered,techniques);
			treetable.setCollapsed(covered, false);
			for(BaseTechnique baseTech : nTechniqueList){
				Object br = treetable.addItem(new Object[]{"Technique ID: " + baseTech.getId() + " Name: " + baseTech.getName(), baseTech.getDescription()},null);
				treetable.setParent(br,covered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		if (nUnselectedTechniqueList.size()>0){
			Object uncovered = treetable.addItem(new Object[]{"Not Covered: ", ""},null);
			treetable.setParent(uncovered,techniques);
			treetable.setCollapsed(uncovered, false);
			for(BaseTechnique baseTech : nUnselectedTechniqueList){
				Object br = treetable.addItem(new Object[]{"Technique ID: " + baseTech.getId() + " Name: " + baseTech.getName(), baseTech.getDescription()},null);
				treetable.setParent(br,uncovered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		
		
		Object activities = treetable.addItem(new Object[]{"Activities ", ""},null);
		treetable.setCollapsed(activities, false);
		//activities
		if (nActivityList.size()>0){
			Object covered = treetable.addItem(new Object[]{"Covered: ", ""},null);
			treetable.setParent(covered,activities);
			treetable.setCollapsed(covered, false);
			for(BaseActivity baseActivity : nActivityList){
				Object br = treetable.addItem(new Object[]{"Activity ID: " + baseActivity.getId() + " Name: " + baseActivity.getName(), baseActivity.getDescription()},null);
				treetable.setParent(br,covered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		if (nUnselectedActivityList.size()>0){
			Object uncovered = treetable.addItem(new Object[]{"Not Covered: ", ""},null);
			treetable.setParent(uncovered,activities);
			treetable.setCollapsed(uncovered, false);
			for(BaseActivity baseActivity : nUnselectedActivityList){
				Object br = treetable.addItem(new Object[]{"Activity ID: " + baseActivity.getId() + " Name: " + baseActivity.getName(), baseActivity.getDescription()},null);
				treetable.setParent(br,uncovered);
				treetable.setChildrenAllowed(br,false);
			}
		}
		
		
		dataDescription = treetable.getContainerDataSource();
		
		return treetable;
	}

	private Component createNewMetrics(final DefaultCategoryDataset result) {
    	final JFreeChart chart = ChartFactory.createStackedBarChart(
    			REFFRAMEWORK_COVERAGE,      // chart title
                "Types",               // domain axis label
                "", // range axis label
                result,                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        // disable bar outlines...
        final BarRenderer BarRenderer = (BarRenderer) plot.getRenderer();
        BarRenderer.setDrawBarOutline(false);
        //number in bars
        BarRenderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        BarRenderer.setBaseItemLabelsVisible(true);
        BarRenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        BarRenderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));

        
        final CategoryItemRenderer renderer = plot.getRenderer();
        renderer.setBaseItemLabelsVisible(true);
        renderer.setSeriesPaint(0, ChartColor.LIGHT_GREEN);
        renderer.setSeriesPaint(1, ChartColor.LIGHT_RED);
     


        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
        try {
			coverage = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;
	}

	

	
    
	
}

