/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.exttools.ExternalToolInstanceExecutor;
import org.eclipse.opencert.webapp.reports.exttools.ExternalToolInstanceSaver;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotDefinedConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotNeededConnector;
import org.eclipse.opencert.webapp.reports.listeners.ExternalToolResultOrQueryAddedListener;
import org.eclipse.opencert.webapp.reports.util.ICommonCssStyles;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.externaltools.api.AbstractExternalToolConnector;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;
import org.eclipse.opencert.externaltools.api.IExternalToolConnector;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAO;
import org.eclipse.opencert.externaltools.api.dao.ExternalToolQueryDAOImpl;
import org.springframework.dao.DataAccessException;

import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ExternalToolTabContent
        extends VerticalLayout
{
    private long currentBaseElementCdoId = -1;
    
    private Button addEditConnectorButton;
    private Button executeButton;
    private Label connectorNameLabel = new Label();
    private Label connectorUserLabel = new Label();
    private Label connectorURLLabel = new Label();
    private VerticalLayout connectorButtonsLayout;

    private List<ExternalToolResultOrQueryAddedListener> externalToolResultOrQueryAddedListeners = new ArrayList<ExternalToolResultOrQueryAddedListener>();
    
    private static final String BUTTON_WIDTH = "142px";
    private static final String CONNECTOR_NAME_LABEL_STYLE = "connectorNameLabel";
    private static final String CONNECTOR_SETTING_LABEL_STYLE = "connectorSettingLabel";
    private static final String ADD_CONNECTOR_DESCRIPTION = "Define a new Connector to the External Tool";
    private static final String EDIT_CONNECTOR_DESCRIPTION = "Edit the External Tool Connector settings";
    private static final String EXECUTE_CONNECTOR_DESCRIPTION = "Execute the defined External Tool Connector";

    public ExternalToolTabContent()
    {
        createExternalToolPanel();
    }
    
    public void setCurrentBaseElementCdoId(long cdoID)
    {
        currentBaseElementCdoId = cdoID;
    }

    public void generateConnectorLabelsForExternalToolTab(long currentCdoId)
    {
        generateConnectorNameLabel(currentCdoId);
        generateConnectorLabel(AbstractExternalToolConnector.URL_KEY, connectorURLLabel, currentCdoId);
        generateConnectorLabel(AbstractExternalToolConnector.USER_KEY, connectorUserLabel, currentCdoId);
    }

    private void generateConnectorLabel(String labelKey, Label connectorSettingLabel, long currentCdoId)
    {
        ExternalToolQuery externalToolQuery = findExternalQueryForBaseElement(currentCdoId);

        if (externalToolQuery == null || externalToolQuery.getExternalToolConnectorId().equals(NotNeededConnector.class.getName())
                || externalToolQuery.getExternalToolConnectorId().equals(NotDefinedConnector.class.getName())) {
            connectorSettingLabel.setValue("");
            return;
        }

        String setting = externalToolQuery.getSettingsAsMap().get(labelKey);

        connectorSettingLabel.setValue(labelKey + ": " + setting);
    }

    private ExternalToolQuery findExternalQueryForBaseElement(long cdoId)
    {
        ExternalToolQueryDAO externalToolQueryDAO = (ExternalToolQueryDAO) SpringContextHelper.getBeanFromWebappContext(ExternalToolQueryDAOImpl.SPRING_NAME);
        return externalToolQueryDAO.findRecentByBaseElementId(cdoId);
    }
    
    private IExternalToolConnector getConnectorFromExternalToolQuery(ExternalToolQuery externalToolQuery)
    {
        if (externalToolQuery == null) {
            return null;
        }

        IExternalToolConnector connector = null;
        try {
            connector = (IExternalToolConnector) Class.forName(externalToolQuery.getExternalToolConnectorId()).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return connector;
    }
    
    private void generateConnectorNameLabel(long currentCdoId)
    {
        ExternalToolQuery eQuery = findExternalQueryForBaseElement(currentCdoId);
        IExternalToolConnector connector = getConnectorFromExternalToolQuery(eQuery);

        if (connector == null) {
            connectorNameLabel.setValue("Not defined");
            return;
        }

        String connectorName = connector.getName();

        ExternalToolQuery externalToolQuery = findExternalQueryForBaseElement(currentCdoId);

        String instanceName = externalToolQuery.getInstanceLabel();

        String connectorFullName = (instanceName == null || instanceName.equals("")) ? connectorName : connectorName + " - " + instanceName;

        connectorNameLabel.setValue(connectorFullName);
    }
    
    private void createExternalToolPanel()
    {
        addEditConnectorButton = new Button("Add/Edit Connector");
        addEditConnectorButton.setWidth(BUTTON_WIDTH);
        addEditConnectorButton.setStyleName(ICommonCssStyles.COMMON_BUTTON_STYLE);
        addEditConnectorButton.addClickListener((e) -> {
            final ExternalToolConnectorWindow externalToolConnectorWindow = new ExternalToolConnectorWindow(currentBaseElementCdoId);

            externalToolConnectorWindow.getSaveButton().addClickListener(
                    (ev) -> {
                        externalToolConnectorWindow.getExternalToolQuery().setBaseElementId(currentBaseElementCdoId);

                        ExternalToolInstanceSaver externalToolInstanceSaver = new ExternalToolInstanceSaver();

                        try {
                            externalToolInstanceSaver.saveExternalToolConnectorProperties(externalToolConnectorWindow.getExternalToolQuery());
                        } catch (DataAccessException ex) {
                            Notification.show("Problem with connection: " + ex.getMessage(), Type.ERROR_MESSAGE);
                            ex.printStackTrace();
                        }

                        externalToolConnectorWindow.close();

                        for (ExternalToolResultOrQueryAddedListener eListener : externalToolResultOrQueryAddedListeners) {
                            eListener.resultOrQueryAdded(currentBaseElementCdoId);
                        }
                    });

            externalToolConnectorWindow.setResizable(true);
            final int winWidth = 900; 
            externalToolConnectorWindow.setWidth(winWidth + "px");
            final int brW = Page.getCurrent().getBrowserWindowWidth();
            externalToolConnectorWindow.setPositionX((brW - winWidth) / 2);
            externalToolConnectorWindow.setPositionY(20);

            UI.getCurrent().addWindow(externalToolConnectorWindow);
        });

        executeButton = new Button("Execute Connector");
        executeButton.setDescription(EXECUTE_CONNECTOR_DESCRIPTION);
        executeButton.setWidth(BUTTON_WIDTH);
        executeButton.setStyleName(ICommonCssStyles.COMMON_BUTTON_STYLE);
        executeButton.addClickListener((e) -> {
            ExternalToolInstanceExecutor externalToolInstanceExecutor = new ExternalToolInstanceExecutor();

            try {
                if (externalToolInstanceExecutor.executeAndSaveExternalToolResult(currentBaseElementCdoId)) {
                    for (ExternalToolResultOrQueryAddedListener eListener : externalToolResultOrQueryAddedListeners) {
                        eListener.resultOrQueryAdded(currentBaseElementCdoId);
                    }
                }
            } catch (IOException | InstantiationException | IllegalAccessException | ClassNotFoundException | DataAccessException ex) {
                Notification.show("Problem with connection: " + ex.getMessage(), Type.ERROR_MESSAGE);
                ex.printStackTrace();
            }
        });

        connectorButtonsLayout = new VerticalLayout();
        connectorButtonsLayout.addComponent(addEditConnectorButton);
        connectorButtonsLayout.addComponent(executeButton);
        
        if (currentBaseElementCdoId < 0) {
        	connectorButtonsLayout.setVisible(false);
        }

        connectorNameLabel.setStyleName(CONNECTOR_NAME_LABEL_STYLE);
        connectorURLLabel.setStyleName(CONNECTOR_SETTING_LABEL_STYLE);
        connectorUserLabel.setStyleName(CONNECTOR_SETTING_LABEL_STYLE);

        VerticalLayout connectorLabelAndAdditionalParamsLayout = new VerticalLayout();
        connectorLabelAndAdditionalParamsLayout.addComponent(connectorNameLabel);
        connectorLabelAndAdditionalParamsLayout.addComponent(connectorURLLabel);
        connectorLabelAndAdditionalParamsLayout.addComponent(connectorUserLabel);

        HorizontalLayout labelAndConnectorButtonsLayout = new HorizontalLayout();
        labelAndConnectorButtonsLayout.setWidth("100%");
        labelAndConnectorButtonsLayout.addComponent(connectorLabelAndAdditionalParamsLayout);
        labelAndConnectorButtonsLayout.addComponent(connectorButtonsLayout);

        super.addComponent(labelAndConnectorButtonsLayout);
    }
    
    public void refreshVisibilityOfAddOrEditConnectorButtonAndLabels(long cdoId)
    {
        ExternalToolQueryDAO externalToolQueryDAO = (ExternalToolQueryDAO) SpringContextHelper.getBeanFromWebappContext(ExternalToolQueryDAOImpl.SPRING_NAME);
        ExternalToolQuery externalToolQuery = externalToolQueryDAO.findRecentByBaseElementId(cdoId);
        connectorButtonsLayout.setVisible(true);

        if (externalToolQuery == null || externalToolQuery.getExternalToolConnectorId().equals(NotNeededConnector.class.getName())
                || externalToolQuery.getExternalToolConnectorId().equals(NotDefinedConnector.class.getName())) {
            addEditConnectorButton.setCaption("Add Connector");
            addEditConnectorButton.setDescription(ADD_CONNECTOR_DESCRIPTION);
            executeButton.setEnabled(false);
            connectorURLLabel.setVisible(false);
            connectorUserLabel.setVisible(false);
        } else {
            addEditConnectorButton.setCaption("Edit Connector");
            addEditConnectorButton.setDescription(EDIT_CONNECTOR_DESCRIPTION);
            executeButton.setEnabled(true);
            connectorURLLabel.setVisible(true);
            connectorUserLabel.setVisible(true);
        }
    }
    
    public void addExternalToolResultOrQueryAddedListener(ExternalToolResultOrQueryAddedListener listener)
    {
        externalToolResultOrQueryAddedListeners.add(listener);
    }
}
