/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.webapp.reports.containers.EvidenceWrapper;
import org.eclipse.opencert.webapp.reports.manager.compliance.IAEvaluation;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.Value;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

public class EvidenceItemsPage extends ItemsPage {

	private static final String TITLE = "Evidence Items";
	
	private static final String NAME = "name";
	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String ARTEFACT_MODEL = "artefact_model";
	private static final String ARTEFACT_DEFINITION = "artefact_definition";
	private static final String VERSION_ID = "artefact_version_id";
	private static final String PROPERTIES = "properties";
	private static final String EVALUATION = "evaluation";
	private static final String RESOURCE = "resource";
	private static final String EVENTS = "events";
	private static final String IASTATUS = "iastatus";
	
	private static final String NAME_LABEL = "Artefact Name";
	private static final String ID_LABEL = "Artefact Id";
	private static final String DESCRIPTION_LABEL = "Artefact Description";
	private static final String ARTEFACT_MODEL_LABEL = "Artefact Model Name";
	private static final String ARTEFACT_DEFINITION_LABEL = "Artefact Definition Name";
	private static final String VERSION_ID_LABEL = "Version ID";
	private static final String PROPERTIES_LABEL = "Artefact Property";
	private static final String EVALUATION_LABEL = "Artefact Evaluation";
	private static final String RESOURCE_LABEL = "Artefact Resource";
	private static final String EVENTS_LABEL = "Impact Analysis Events History";
	private static final String IASTATUS_LABEL = "Impact Analysis Status";
	
	private static final String PROPERTY_LABEL_DESC = "<b>Properties: </b><br>";
	private static final String RESOURCES_LABEL_DESC = "<b>Resources: </b><br>";

	
	@Override
	public ReportID getReportID() {
		return ReportID.EVIDENCE_ITEMS;
	}

	@Override
	public String getTitle() {
		return TITLE;
	}

	@Override
	protected Component addAdditionalComponent() {
		Label evidenceItemsLabel = new Label("Evidence items:");
		evidenceItemsLabel.setStyleName("complianceDetailsTitle");
		return evidenceItemsLabel;
	}
	
	@Override
	protected HierarchicalContainer createDataContainer(AssuranceProject project) {
		HierarchicalContainer hc = createEmptyTableDataContainer();
		
		EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
		for (AssetsPackage assetsPackage : assetsPackages) {
			if (assetsPackage.isIsActive()) {
				EList<ArtefactModel> artefactModels = assetsPackage.getArtefactsModel();
				for (ArtefactModel artefactModel : artefactModels) {
					String artefactModelName = artefactModel.getName();
					String repoUrl = artefactModel.getRepoUrl();
					EList<ArtefactDefinition> artefactDefs = artefactModel.getArtefact();
					for (ArtefactDefinition artefactDef : artefactDefs) {
						String artefactDefName = artefactDef.getName();
						EList<Artefact> artefacts= artefactDef.getArtefact();
						
						addPartArtefactToContainer(artefacts, artefactModelName, artefactDefName, repoUrl, null, hc);
					}
				}
			}
		}
		
		return hc;
	}
	
	private void addPartArtefactToContainer(List<Artefact> artefacts, String artefactModelName, String artefactDefinitionName, 
			String repoUrl, EvidenceWrapper evidenceParent, HierarchicalContainer hc) {
		for (Artefact artefact : artefacts) {
			Label artefactNameLabel = createLabel(artefact.getName());
			Label artefactIdLabel = createLabel(artefact.getId());
			Label artefactDescLabel = createLabel(artefact.getDescription());
			Label artefactModelNameLabel = createLabel(artefactModelName);
			Label artefactDefNameLabel = createLabel(artefactDefinitionName);
			Label artefactVersionIdLabel = createLabel(artefact.getVersionID());
			Label propertiesLabel = createArtefactPropertyLabel(artefact);
			Label evaluationLabel = createEvaluationLabel(artefact.getEvaluation());
			Label resourceLabel = createArtefactResourceLabel(artefact, repoUrl);
			Label eventLabel = createAssuranceAssetEventsLabel(artefact.getLifecycleEvent(), true);
			Label iaStatusLabel = createIaStatusLabel(artefact);
			EvidenceWrapper evidenceWrapper = new EvidenceWrapper(CDOStorageUtil.getCDOId(artefact), artefactNameLabel, artefactIdLabel, 
					artefactDescLabel, artefactModelNameLabel, artefactDefNameLabel, artefactVersionIdLabel, 
					propertiesLabel, evaluationLabel, resourceLabel, eventLabel, iaStatusLabel);
			
			Item item =  hc.addItem(evidenceWrapper);
			mergePropertiesWithData(item, evidenceWrapper);
			if (evidenceParent != null) {
				hc.setParent(evidenceWrapper, evidenceParent);
			}
			
			EList<Artefact> subArtefactParts = artefact.getArtefactPart();
			if (subArtefactParts.isEmpty()) {
				hc.setChildrenAllowed(evidenceWrapper, false);
			} else {
				addPartArtefactToContainer(subArtefactParts, artefactModelName, artefactDefinitionName, repoUrl, evidenceWrapper, hc);
			}
		}
	}
    
    private Label createIaStatusLabel(Artefact artefact)
    {
        IAEvaluation iaEvaluation = IAEvaluation.createForArtefact(artefact);
        
        if (!iaEvaluation.needsAttention()) {
            return new Label("");
        }
        
        Label iaComplianceLabel = new Label("", ContentMode.HTML);
        iaComplianceLabel.setStyleName("noCompliant");
        iaComplianceLabel.setValue(iaEvaluation.getEvaluationStatus().getUserFriendlyLabel());
        
        iaComplianceLabel.setDescription(iaEvaluation.generateIAStatusDescription(
                "<br/><font color=\"red\">Please inspect Compliance Estimation Report for possible actions</font>"));
    
        return iaComplianceLabel;    
    }

    private Label createArtefactPropertyLabel(Artefact artefact) {
    	EList<Value> properties = artefact.getPropertyValue();
		boolean setPropertyName = true;
		String propertyCaption = "";
		String propertyDesc = PROPERTY_LABEL_DESC;
		int i=1;
		for (Value property : properties) {
			String propertyName = createDataValue(property.getName());
			String propertyValue = createDataValue(property.getValue());
			if (propertyValue == null) {
				propertyValue = "";
			}
			if (setPropertyName) {
				propertyCaption = propertyName + ": " + propertyValue;
				setPropertyName = false;
			}
			propertyDesc += "<b>" + i + "</b>" + ".<br>";
			i++;
			propertyDesc += "<i>Property:</i> " + propertyName + "<br><i>Value:</i> " + propertyValue + "<br>";
		}
		
		if (PROPERTY_LABEL_DESC.equals(propertyDesc)) {
			return createLabel(propertyCaption);
		} else {
			return createLabel(propertyCaption, propertyDesc);
		}
    }
    
    private Label createArtefactResourceLabel(Artefact artefact, String repoUrl) {
    	EList<Resource> resources = artefact.getResource();
    	String resourceName = "";
    	boolean setResourceName = true;
		String resourceDesc = RESOURCES_LABEL_DESC;
		int i=1;
		for (Resource resource : resources) {
			if (setResourceName) {
				resourceName = createDataName(resource.getName());
				setResourceName = false;
			}
			resourceDesc += "<b>" + i + "</b>" + ".<br>";
			i++;
			resourceDesc += "<i>Name:</i> " + createDataValue(resource.getName()) + "<br>";
			resourceDesc += "<i>Description:</i> " + createDataValue(resource.getDescription()) + "<br>";
			resourceDesc += "<i>Location:</i> " + repoUrl + "/" + createDataValue(resource.getLocation()) + "<br>";
		}
		
		if (RESOURCES_LABEL_DESC.equals(resourceDesc)) {
			return createLabel(resourceName);
		} else {
			return createLabel(resourceName, resourceDesc);
		}
    }
	
    @SuppressWarnings("unchecked")
	private void mergePropertiesWithData(Item item, EvidenceWrapper evidenceWrapper) 
    {
        item.getItemProperty(NAME).setValue(evidenceWrapper.getName());
        item.getItemProperty(ID).setValue(evidenceWrapper.getId());
        item.getItemProperty(DESCRIPTION).setValue(evidenceWrapper.getDescription());
        item.getItemProperty(ARTEFACT_MODEL).setValue(evidenceWrapper.getArtefactModel());
        item.getItemProperty(ARTEFACT_DEFINITION).setValue(evidenceWrapper.getArtefactDefinition());
        item.getItemProperty(VERSION_ID).setValue(evidenceWrapper.getVersionId());
        item.getItemProperty(PROPERTIES).setValue(evidenceWrapper.getProperties());
        item.getItemProperty(EVALUATION).setValue(evidenceWrapper.getEvaluation());
        item.getItemProperty(RESOURCE).setValue(evidenceWrapper.getResource());
        item.getItemProperty(EVENTS).setValue(evidenceWrapper.getEvents());
        item.getItemProperty(IASTATUS).setValue(evidenceWrapper.getIAStatus());
    }

    @Override
    protected void addColumnHeaders() {
		itemsTable.setColumnHeader(NAME, NAME_LABEL);
		itemsTable.setColumnHeader(ID, ID_LABEL);
		itemsTable.setColumnHeader(DESCRIPTION, DESCRIPTION_LABEL);
		itemsTable.setColumnHeader(ARTEFACT_MODEL, ARTEFACT_MODEL_LABEL);
		itemsTable.setColumnHeader(ARTEFACT_DEFINITION, ARTEFACT_DEFINITION_LABEL);
		itemsTable.setColumnHeader(VERSION_ID, VERSION_ID_LABEL);
		itemsTable.setColumnHeader(PROPERTIES, PROPERTIES_LABEL);
		itemsTable.setColumnHeader(EVALUATION, EVALUATION_LABEL);
		itemsTable.setColumnHeader(RESOURCE, RESOURCE_LABEL);
		itemsTable.setColumnHeader(EVENTS, EVENTS_LABEL);
		itemsTable.setColumnHeader(IASTATUS, IASTATUS_LABEL);
		
		itemsTable.setColumnExpandRatio(NAME, 10);
		itemsTable.setColumnExpandRatio(ID, 5);
		itemsTable.setColumnExpandRatio(DESCRIPTION, 10);
		itemsTable.setColumnExpandRatio(ARTEFACT_MODEL, 10);
		itemsTable.setColumnExpandRatio(ARTEFACT_DEFINITION, 10);
		itemsTable.setColumnExpandRatio(VERSION_ID, 3);
		itemsTable.setColumnExpandRatio(PROPERTIES, 13);
		itemsTable.setColumnExpandRatio(EVALUATION, 13);
		itemsTable.setColumnExpandRatio(RESOURCE, 13);
		itemsTable.setColumnExpandRatio(EVENTS, 13);
		itemsTable.setColumnExpandRatio(IASTATUS, 7);
	}
	
	@Override
	protected void addContainerProperties(HierarchicalContainer hc) {
        hc.addContainerProperty(NAME, Label.class, "");
        hc.addContainerProperty(ID, Label.class, "");
        hc.addContainerProperty(DESCRIPTION, Label.class, "");
        hc.addContainerProperty(ARTEFACT_MODEL, Label.class, "");  
        hc.addContainerProperty(ARTEFACT_DEFINITION, Label.class, "");
        hc.addContainerProperty(VERSION_ID, Label.class, "");
        hc.addContainerProperty(PROPERTIES, Label.class, "");
        hc.addContainerProperty(EVALUATION, Label.class, "");
        hc.addContainerProperty(RESOURCE, Label.class, "");
        hc.addContainerProperty(EVENTS, Label.class, "");
        hc.addContainerProperty(IASTATUS, Label.class, "");
    }

}
