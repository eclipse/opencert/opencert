/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import org.eclipse.opencert.webapp.reports.util.ICommonCssStyles;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class ConfirmationWindow extends Window {
	private String windowCaption;
	private String confirmQuestion;
	private String bottomNote;
	private FormLayout confirmWindowContent;
	
	private Button yesButton;
	private Button cancelButton;
    
	private String confirmationButtonCaption;
    private String cancelButtonCaption;
	
    
    public ConfirmationWindow(String windowCaption, String confirmQuestion, String bottomNote)
    {
        this(windowCaption, confirmQuestion, bottomNote, "Yes", "Cancel");
    }
    
	public ConfirmationWindow(String windowCaption, String confirmQuestion, String bottomNote, String confirmationButtonCaption, String cancelButtonCaption)
	{
		this.windowCaption = windowCaption;
		this.confirmQuestion = confirmQuestion;
		this.bottomNote = bottomNote;
		this.confirmationButtonCaption = confirmationButtonCaption;
		this.cancelButtonCaption = cancelButtonCaption;
		
		init();
	}
	
	public void init() 
	{
	   	setCaption(windowCaption);
    	setWidth("400px");
    	setResizable(false);
    	setStyleName("assignComplianceEvidenceWindow");
    	setModal(true);
    	center();
    	
    	confirmWindowContent = new FormLayout();
	    generateConfirmWindowContent(confirmQuestion, bottomNote);
	    
	    setContent(confirmWindowContent);
	}
	
    public void addYesAction(ClickListener clickListener)
    {
        yesButton.addClickListener(clickListener);
    }
       
    private void generateConfirmWindowContent(String confirmQuestion, String bottomNote)
    {               
        final Label questionLabel = new Label(confirmQuestion, ContentMode.HTML);
        
        HorizontalLayout hLayout = new HorizontalLayout();
        
        yesButton = new Button(confirmationButtonCaption);
        yesButton.setStyleName("popupButton " + ICommonCssStyles.COMMON_BUTTON_STYLE);        
        
        cancelButton = new Button(cancelButtonCaption);
        cancelButton.setStyleName("popupButton " + ICommonCssStyles.COMMON_BUTTON_STYLE);
        cancelButton.addClickListener((e) -> 
        {
        	close();
		});
        cancelButton.focus();
        
        Label emptyLabel = new Label("");
        emptyLabel.setStyleName("emptyLabel");
        
        hLayout.addComponent(emptyLabel);
        hLayout.addComponent(yesButton);
        hLayout.addComponent(cancelButton);
        
        confirmWindowContent.addComponent(questionLabel);
        confirmWindowContent.addComponent(hLayout);
        
        if (bottomNote != null && !("".equals(bottomNote))) {
        	TextArea bottomEvidenceNote = new TextArea();
	        bottomEvidenceNote.setHeight("40px");
	        bottomEvidenceNote.setValue(bottomNote);
	        bottomEvidenceNote.setWidth("370px");
	        bottomEvidenceNote.setReadOnly(true);
	        
	        confirmWindowContent.addComponent(bottomEvidenceNote);
        }
    }
    
    public void setContentStyleName(String styleName)
    {
    	confirmWindowContent.setStyleName(styleName);
    }
}
