/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Date;

import org.eclipse.opencert.webapp.reports.listeners.EvidenceFileUploaderListener;
import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.util.ICommonCssStyles;
import org.eclipse.opencert.webapp.reports.util.UploadFileMode;

import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.server.StreamVariable;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.DragAndDropWrapper.WrapperTransferable;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Html5File;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class DragAndDropComplianceEvidencePanel
        extends Panel
        implements DropHandler
{

    private static final long serialVersionUID = 1644713007473559196L;

    private UploadFileWindow assignComplianceEvidenceWindow;

    private boolean isDragAndDropFunctionalityEnabled = false;

    private UploadFileMode uploadFileMode = UploadFileMode.ADD_FILE; 
    
    public DragAndDropComplianceEvidencePanel(EvidenceFileUploaderListener evidenceFileUploaderListener, String reportTitle)
    {
        assignComplianceEvidenceWindow = new UploadFileWindow(UploadFileMode.ADD_FILE, null, evidenceFileUploaderListener, "", reportTitle);
        
        init();
    }

	private void init()
    {
		Label dndTitleLabel = new Label("Add a compliance evidence to this project baseline element.");
		prepareLabel(dndTitleLabel, "dndTitleLabel");
		Label dndInstructionLabel = new Label("Please <b>drag and drop</b> a file to this area or press", ContentMode.HTML);
		prepareLabel(dndInstructionLabel, "dbdInstructionLabel");
   		Label dndNoteLabel = new Label("<i>Note: Pressing [Assign] button will commit your file to SVN and assign it as a compliance evidence.</i>",
                ContentMode.HTML);
   		prepareLabel(dndNoteLabel, "dndNoteLabel");

        Button uploadButton = new Button("Upload");
        uploadButton.setImmediate(true);
        uploadButton.setStyleName("uploadButtonStyle " + ICommonCssStyles.COMMON_BUTTON_STYLE);
        uploadButton.addClickListener((e) ->
        {
            assignComplianceEvidenceWindow.setDragAndDropFunctionalityEnabled(false);
            
            assignComplianceEvidenceWindow.createSvnPropertiesData();
            assignComplianceEvidenceWindow.getBottomEvidenceNote().setValue(assignComplianceEvidenceWindow.generateBottomEvidenceNoteValue());
            assignComplianceEvidenceWindow.getDragAndDropFileName().setVisible(false);
            assignComplianceEvidenceWindow.getEvidenceFileUploader().getUpload().setVisible(true);
            assignComplianceEvidenceWindow.getModificationDate().setValue(new Date());

            if (!(UI.getCurrent().getWindows().contains(assignComplianceEvidenceWindow))) {
                UI.getCurrent().addWindow(assignComplianceEvidenceWindow);
            }
        });

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.addComponent(dndTitleLabel);

        HorizontalLayout labelAndButtonLayout = new HorizontalLayout();
        labelAndButtonLayout.addComponent(dndInstructionLabel);
        labelAndButtonLayout.addComponent(uploadButton);

        mainLayout.addComponent(labelAndButtonLayout);
        mainLayout.addComponent(dndNoteLabel);

        DragAndDropWrapper dragAndDropWrapper = new DragAndDropWrapper(mainLayout);
        dragAndDropWrapper.setDropHandler(this);
        
       	setContent(dragAndDropWrapper);
       	setStyleName("dragAndDropComplianceEvidencePanel");
    }

    private void prepareLabel(Label label, String style)
    {
        label.setStyleName("dndLabel");
    }

    public void setInfoMessage(String baseAssetName)
    {
        assignComplianceEvidenceWindow.setInfoMessage(baseAssetName);
    }

    public TextField getArtefactName()
    {
        return assignComplianceEvidenceWindow.getArtefactName();
    }

    public TextField getResourceName()
    {
        return assignComplianceEvidenceWindow.getResourceName();
    }

    public EvidenceFileUploader getEvidenceFileUploader()
    {
        return assignComplianceEvidenceWindow.getEvidenceFileUploader();
    }

    @Override
    public void drop(DragAndDropEvent dropEvent)
    {
        assignComplianceEvidenceWindow.setDragAndDropFunctionalityEnabled(true);

        final WrapperTransferable tr = (WrapperTransferable) dropEvent.getTransferable();

        final Html5File[] files = tr.getFiles();

        if (files != null) {
            for (final Html5File html5File : files) {
                final String fileName = html5File.getFileName();
                final ByteArrayOutputStream bas = new ByteArrayOutputStream();
                final StreamVariable streamVariable = new StreamVariable() {
                    private static final long serialVersionUID = -113712093997815512L;

                    @Override
                    public OutputStream getOutputStream()
                    {
                        return bas;
                    }

                    @Override
                    public boolean listenProgress()
                    {
                        return false;
                    }

                    @Override
                    public void onProgress(final StreamingProgressEvent event)
                    {
                    }

                    @Override
                    public void streamingStarted(final StreamingStartEvent event)
                    {
                    }

                    @Override
                    public void streamingFinished(final StreamingEndEvent event)
                    {
                        showUploadWindow(fileName, html5File.getType(), bas);
                    }

                    @Override
                    public void streamingFailed(final StreamingErrorEvent event)
                    {
                    }

                    @Override
                    public boolean isInterrupted()
                    {
                        return false;
                    }
                };
                html5File.setStreamVariable(streamVariable);
            }
        }
    }

    @Override
    public AcceptCriterion getAcceptCriterion()
    {
        return AcceptAll.get();
    }

    private void showUploadWindow(final String name, final String type, final ByteArrayOutputStream bas)
    {
    	assignComplianceEvidenceWindow.setFileByteArray(bas.toByteArray());

    	assignComplianceEvidenceWindow.getEvidenceFileUploader().getArtefactFileDetails().setFileName(name);

        assignComplianceEvidenceWindow.getResourceName().setValue(name);
        assignComplianceEvidenceWindow.getArtefactName().setValue(name);

        assignComplianceEvidenceWindow.createSvnPropertiesData();
        
        assignComplianceEvidenceWindow.getBottomEvidenceNote().setValue(assignComplianceEvidenceWindow.generateBottomEvidenceNoteValue());

        assignComplianceEvidenceWindow.getDragAndDropFileName().setVisible(true);
        assignComplianceEvidenceWindow.getDragAndDropFileName().setWidth("300px");
        assignComplianceEvidenceWindow.getDragAndDropFileName().setValue(assignComplianceEvidenceWindow.getEvidenceFileUploader().getArtefactFileDetails().getFileName());
        assignComplianceEvidenceWindow.getModificationDate().setValue(new Date());

        assignComplianceEvidenceWindow.getEvidenceFileUploader().getUpload().setVisible(false);

        UI.getCurrent().addWindow(assignComplianceEvidenceWindow);
    }

    public boolean isDragAndDropFunctionalityEnabled()
    {
        return isDragAndDropFunctionalityEnabled;
    }

    public void setDragAndDropFunctionalityEnabled(boolean isDragAndDropFunctionalityEnabled)
    {
        this.isDragAndDropFunctionalityEnabled = isDragAndDropFunctionalityEnabled;
    }

    public Window getAssignComplianceEvidenceWindow()
    {
        return assignComplianceEvidenceWindow;
    }

    public void addUnassignOrAssignArtefactListener(UnassignOrAssignArtefactListener unassignOrAssignArtefactListener)
    {
    	assignComplianceEvidenceWindow.getUnassignOrAssignArtefactListeners().add(unassignOrAssignArtefactListener);
    }
    
    public UploadFileMode getUploadFileMode()
    {
    	return uploadFileMode;
    }
    
}
