/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import org.eclipse.opencert.webapp.reports.view.common.AbstractBaselineFrameworkReport;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalSplitPanel;

public class GapAnalysisReport extends AbstractBaselineFrameworkReport
{
    public GapAnalysisReport()
    {
    }
    
    @Override
    public String getTitle() 
    {
        return "Gap Analysis report";
    }

    @Override
    public Component getMainComponent(GUIMode guiMode) 
    {
        HorizontalSplitPanel mainComponent = new HorizontalSplitPanel();
        
        ProjectBaselineComplianceNumberTable projectBaselineComplianceNumberTable = new ProjectBaselineComplianceNumberTable();
        addBaselineChangeListener(projectBaselineComplianceNumberTable);

        ComplianceDetailsPanel complianceDetailsPanel = new ComplianceDetailsPanel();
        
        ProjectBaselineComplianceNumberTable projectBaselineComplianceTable = projectBaselineComplianceNumberTable;
        projectBaselineComplianceTable.addProjectBaselineComplianceTableListener(complianceDetailsPanel);
        
        mainComponent.setFirstComponent(projectBaselineComplianceNumberTable);
        mainComponent.setSplitPosition(40);
        mainComponent.setSecondComponent(complianceDetailsPanel);

        return mainComponent;
    }

    @Override
    public ReportID getReportID() 
    {
        return ReportID.GAP_ANALYSIS;
    }

	@Override
	public Component getExportToDocxComponent() 
	{
		return null;
	}
}