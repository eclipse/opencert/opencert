/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.listeners.metrics.MetricsEstimationTreeMenuListener;
import org.eclipse.opencert.webapp.reports.view.common.IBaselineFrameworkChangeListener;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;




@Theme("opencerttheme")
public class MetricsEstimationTreeMenu extends CustomComponent implements IBaselineFrameworkChangeListener {
    
	

	private static final long serialVersionUID = 293094038395358879L;
	
	private static final String METRICS_MENU_TITLE_STYLE = "metricsMenuTitle";
    private static final String METRICS_MENU_TREE_STYLE = "metricsMenuDetails";
    
    private static final String METRICS_ESTIMATION_MENU = "Metrics Menu";
    private static final String ALL_METRICS_MENU_ITEM = "All Metrics";
    private static final String BASELINE_METRICS_MENU_ITEM = "Baseline Metrics";
    private static final String MAPPING_METRICS_MENU_ITEM = "Mapping Metrics";
    private static final String ASSURANCE_ASSET_METRICS_MENU_ITEM = "Assurance Asset Metrics";
    private static final String REFFRAMEWORK_METRICS_MENU_ITEM = "Refframework Metrics";
    private static final String PROCESS_METRICS_MENU_ITEM = "Process Metrics";
    private static final String MONITOR_PROCESS_METRICS_MENU_ITEM = "Monitor of Process";
    private static final String TIME_EFFICIENCY_METRICS_MENU_ITEM = "Time Efficiency";
    private static final String RESOURCE_EFFICIENCY_METRICS_MENU_ITEM = "Resource Efficiency";
    private static final String ARGUMENTATION_METRICS_MENU_ITEM = "Argumentation Metrics";    
    
    private VerticalLayout treeLayout;
    private TreeTable metricsMenuTree;
    
    
    private List<MetricsEstimationTreeMenuListener> metricsEstimationTreeMenuListeners = new ArrayList<MetricsEstimationTreeMenuListener>();

	private AssuranceProjectWrapper project;
                      
    public MetricsEstimationTreeMenu(){      
        init();
 
    }
    
    private void init(){
    	   
        VerticalLayout mainPanel = new VerticalLayout();
		//mainPanel.setWidth("100%");
		mainPanel.setStyleName("layoutSpacing");
		mainPanel.setSpacing(true);
		

		
		//Menu data:
		treeLayout = new VerticalLayout();
		treeLayout.setMargin(true);
		treeLayout.setStyleName(METRICS_MENU_TITLE_STYLE);
		
		metricsMenuTree = new TreeTable();	
		
		initTree();
		metricsMenuTree.addStyleName(METRICS_MENU_TREE_STYLE);
		
		metricsMenuTree.setSelectable(true);
		metricsMenuTree.setImmediate(true);
		metricsMenuTree.setBuffered(false);
		metricsMenuTree.setPageLength(metricsMenuTree.size());		
		metricsMenuTree.setSizeFull();
		
		
		
		metricsMenuTree.addValueChangeListener(new ValueChangeListener() {
 
			private static final long serialVersionUID = -2756631243834637095L;

			public void valueChange(final ValueChangeEvent event) {
                final String valueString = String.valueOf(event.getProperty().getValue());         
                for (MetricsEstimationTreeMenuListener metricsEstimationTreeMenuListener : getMetricsEstimationTreeMenuListener()) {
                	metricsEstimationTreeMenuListener.treeElementSelected(valueString,project);
                }
                
            }
        });
		
		
		
		treeLayout.addComponent(metricsMenuTree);
		treeLayout.setSizeFull();
		mainPanel.addComponent(treeLayout);	
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
		
	}

	private void initTree(){
		
		metricsMenuTree.addContainerProperty(METRICS_ESTIMATION_MENU, String.class, "");
		
    	Object all = metricsMenuTree.addItem(new Object[]{ALL_METRICS_MENU_ITEM},ALL_METRICS_MENU_ITEM);
    	Object baseline = metricsMenuTree.addItem(new Object[]{BASELINE_METRICS_MENU_ITEM},BASELINE_METRICS_MENU_ITEM);
    	Object mapping = metricsMenuTree.addItem(new Object[]{MAPPING_METRICS_MENU_ITEM},MAPPING_METRICS_MENU_ITEM);
    	Object assurance = metricsMenuTree.addItem(new Object[]{ASSURANCE_ASSET_METRICS_MENU_ITEM},ASSURANCE_ASSET_METRICS_MENU_ITEM);  		        
    	Object reff = metricsMenuTree.addItem(new Object[]{REFFRAMEWORK_METRICS_MENU_ITEM},REFFRAMEWORK_METRICS_MENU_ITEM);
    	Object processMain = metricsMenuTree.addItem(new Object[]{PROCESS_METRICS_MENU_ITEM},PROCESS_METRICS_MENU_ITEM);
    	Object processMonitor = metricsMenuTree.addItem(new Object[]{MONITOR_PROCESS_METRICS_MENU_ITEM},MONITOR_PROCESS_METRICS_MENU_ITEM);
    	Object processTime = metricsMenuTree.addItem(new Object[]{TIME_EFFICIENCY_METRICS_MENU_ITEM},TIME_EFFICIENCY_METRICS_MENU_ITEM);
    	Object processResource = metricsMenuTree.addItem(new Object[]{RESOURCE_EFFICIENCY_METRICS_MENU_ITEM},RESOURCE_EFFICIENCY_METRICS_MENU_ITEM);
    	Object arg = metricsMenuTree.addItem(new Object[]{ARGUMENTATION_METRICS_MENU_ITEM},ARGUMENTATION_METRICS_MENU_ITEM);		        
    
    	metricsMenuTree.setParent(processMonitor,processMain);
    	metricsMenuTree.setParent(processTime,processMain);
    	metricsMenuTree.setParent(processResource,processMain);
    	
    	metricsMenuTree.setChildrenAllowed(all, false);
    	metricsMenuTree.setChildrenAllowed(baseline, false);
    	metricsMenuTree.setChildrenAllowed(mapping, false);
    	metricsMenuTree.setChildrenAllowed(assurance, false);
    	metricsMenuTree.setChildrenAllowed(reff, false);
    	metricsMenuTree.setChildrenAllowed(processMonitor, false);
    	metricsMenuTree.setChildrenAllowed(processTime, false);
    	metricsMenuTree.setChildrenAllowed(processResource, false);
    	metricsMenuTree.setChildrenAllowed(arg, false);
    	
    	metricsMenuTree.setCollapsed(processMain,false);
    }

	@Override
    public void setBaselineFramework(long baselineFrameworkID){
				
        for (MetricsEstimationTreeMenuListener metricsEstimationTreeMenuListener : getMetricsEstimationTreeMenuListener()) {
        	metricsEstimationTreeMenuListener.treeElementUnselected(baselineFrameworkID,project);
        }
				
		init();
    }


	
    
	 public void addMetricsEstimationTreeMenuListener(MetricsEstimationTreeMenuListener metricsEstimationTreeMenuListener){
		 metricsEstimationTreeMenuListeners.add(metricsEstimationTreeMenuListener);
	 }
	    
	 protected List<MetricsEstimationTreeMenuListener> getMetricsEstimationTreeMenuListener(){
		 return metricsEstimationTreeMenuListeners;
	 }


	public void setCurrentProject(AssuranceProjectWrapper newProject) {
		this.project=newProject;		
	}
 

	
}

