/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.Cookie;

import org.eclipse.opencert.webapp.reports.containers.ArtefactFileDetails;
import org.eclipse.opencert.webapp.reports.containers.SvnProperties;
import org.eclipse.opencert.webapp.reports.impactanalysis.ImpactAnalyserExecutor;
import org.eclipse.opencert.webapp.reports.listeners.EvidenceFileUploaderListener;
import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.manager.ArtefactFileManager;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.IUploadFile;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.util.UploadFileMode;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNErrorCode;
import org.tmatesoft.svn.core.SVNErrorMessage;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServletResponse;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.ChangeEvent;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class EvidenceFileUploader
        extends CustomComponent
        implements Upload.SucceededListener, Upload.FailedListener, Upload.Receiver, Upload.ChangeListener
{
    private static final long serialVersionUID = 3743520661367604416L;

    private VerticalLayout rootVerticalLayout = new VerticalLayout();

    private Upload upload;

    private ArtefactFileDetails artefactFileDetails = new ArtefactFileDetails();

    private EvidenceFileUploaderListener evidenceFileUploaderListener;

    private UploadFileWindow uploadFileWindow;
    private String reportTitle;

    private static final String problemWithSVNPropertiesMsg = "Problem with SVN communication. Please check your SVN settings URL, user and password. "
            + "In order to do it, please go to OPENCERT server and edit server-settings.xml file.";
    protected static final String emptyEvidenceFileNameMsg = "No file has been chosen. Please specify the evidence file.";
    protected static final String problemWithServerSettingsFileMsg = "Problem with OPENCERT server settings. "
            + "The structure of file: \"server-settings.xml\" is broken.";
    protected static final String emptyComplianceGroup = "No compliance level has been selected. Please select the level of compliance.";
    protected static final String emptyJustificationArea = "No justification has been specified. Please enter the text of justification.";
    protected static final String wrongModificationDate = "Wrong date format. Please enter the date in format: 2015-01-08 12:04PM ";
    protected static final String emptySvnUrlMsg = "No SVN Path has been specified. Please enter SVN Path.";
    protected static final String emptySvnUserMsg = "No SVN User has been specified. Please enter SVN User.";
    protected static final String emptySvnPasswordMsg = "No SVN Password has been specified. Please enter SVN Password.";
    protected static final String connectionToSvnCorrect = "The connection to SVN is correct.";
    protected static final String connectionToSvnNotCorrect = "Cannot connect to SVN. Please check the SVN connection settings.";

    public EvidenceFileUploader(UploadFileWindow uploadFileWindow, EvidenceFileUploaderListener evidenceFileUploaderListener,
    		String reportTitle) {
		this.uploadFileWindow = uploadFileWindow;
		this.evidenceFileUploaderListener = evidenceFileUploaderListener;
		this.reportTitle = reportTitle;
		
		readSVNPropertiesFromServerSettings();
		
		init();
		
		readSVNPropertiesFromServerSettings();
		
		rootVerticalLayout.addComponent(upload);
        setCompositionRoot(rootVerticalLayout);
	}

	@Override
    public OutputStream receiveUpload(String fileName, String MIMEType)
    {
        artefactFileDetails.setFileName(fileName);

        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(new File(getPathForEvidenceFile()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return fos;
    }

    @Override
    public void uploadSucceeded(Upload.SucceededEvent event)
    {
        try {
            byte[] file = loadFile(getPathForEvidenceFile());
            commitAndAddingArtefactToDB(file);
        } catch (Exception e) {
            errorNotification(e.getMessage());
            refreshUploadComponent();
            cleaningAfterCommit();
            e.printStackTrace();
        }
    }

    @Override
    public void uploadFailed(FailedEvent event)
    {
        refreshUploadComponent();
    }

    public Upload getUpload()
    {
        return upload;
    }

    public ArtefactFileDetails getArtefactFileDetails()
    {
        return artefactFileDetails;
    }

    public void setEvidenceFileUploaderListener(EvidenceFileUploaderListener evidenceFileUploaderListener)
    {
        this.evidenceFileUploaderListener = evidenceFileUploaderListener;
    }

    @Override
    public void filenameChanged(ChangeEvent event)
    {
        String filePath = event.getFilename();
        
        String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);  //lastIndexOf(File.separatorChar)  - bug when report is on Linux, upload from Windows
        filePath = fileName;
        fileName = filePath.substring(filePath.lastIndexOf("\\") + 1);
        artefactFileDetails.setFileName(fileName);

        uploadFileWindow.getArtefactName().setValue(fileName);
        uploadFileWindow.getResourceName().setValue(fileName);
    }

    protected void errorNotification(String msg)
    {
        Notification.show(msg, Notification.Type.ERROR_MESSAGE);
    }
    
    protected void infoNotification(String msg)
    {
        Notification.show(msg, Notification.Type.HUMANIZED_MESSAGE);
    }
    
    protected void warningNotification(String msg)
    {
        Notification.show(msg, Notification.Type.WARNING_MESSAGE);
    }

    protected void commitAfterDragAndDrop(byte[] file)
    {
        try {
            commitAndAddingArtefactToDB(file);
        } catch (Exception e) {
            errorNotification(e.getMessage());
            refreshUploadComponent();
            cleaningAfterCommit();
            e.printStackTrace();
        }
    }

    protected void saveSVNPropertiesDataToCookies() {
        VaadinServletResponse response = ((VaadinServletResponse) VaadinService.getCurrentResponse());
        SvnProperties svnProperties = artefactFileDetails.getSvnProperties();
    	//String contextPath = VaadinService.getCurrentRequest().getContextPath();   //drag and drop does not work
    	String contextPath = "/";

    	addCookie(response, contextPath, "svnUrl", svnProperties.getSvnUrl());
    	addCookie(response, contextPath, "svnUser", svnProperties.getSvnUser());
    	addCookie(response, contextPath, "svnPassword", svnProperties.getSvnPassword());
    }  
    
    private void addCookie(VaadinServletResponse response, String contextPath, String cookieName, String cookieValue) {
    	try {
			String encodedCookieValue = URLEncoder.encode(cookieValue, "UTF-8");
			
			Cookie cookie = new Cookie(cookieName, encodedCookieValue);
	    	cookie.setPath(contextPath);
	    	cookie.setMaxAge(2147483647);
	    	//cookie.setSecure(true);

	    	response.addCookie(cookie);
    	} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }

    protected void readSVNPropertiesFromServerSettings()
    {
    	artefactFileDetails.setSvnProperties(new SvnProperties("","",""));
        FileInputStream fis = null;

        try {
            Properties prop = new Properties();

            String filePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + File.separator + "server-settings.xml";

            fis = new FileInputStream(filePath);

            prop.loadFromXML(fis);

            artefactFileDetails.getSvnProperties().setSvnUrl(prop.getProperty("svnUrl"));

        } catch (Exception e) {
        } finally {
            try {
                if (fis != null) {
                    fis.close(); 
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected List<SvnProperties> searchSvnPropertiesFromProject() throws Exception
    {
    	ArtefactFileManager artefactFileManager = (ArtefactFileManager) SpringContextHelper.getBeanFromWebappContext(ArtefactFileManager.SPRING_NAME);
    	List<SvnProperties> allSvnProperties = artefactFileManager.searchSvnPropertiesFromProject(artefactFileDetails);
    	
    	return allSvnProperties;
    }
      
    protected boolean checkSVNLocation() {
        SVNURL url = null;
        SVNRepository repository = null;
        String repositoryUrl = artefactFileDetails.getSvnProperties().getSvnUrl();
        if (repositoryUrl == null || "".equals(repositoryUrl)) {
        	return false;
        }
        String projectName = artefactFileDetails.getProjectName();
        if (repositoryUrl.endsWith(IUploadFile.INSERTED_BY_OPENCERT_WEB + "/" + projectName)) {
        	repositoryUrl = repositoryUrl.replace(IUploadFile.INSERTED_BY_OPENCERT_WEB + "/" + projectName, "");
        }
        try {
            url = SVNURL.parseURIEncoded(repositoryUrl);
            repository = SVNRepositoryFactory.create(url);
        } catch (SVNException e) {
            e.printStackTrace();
        }

        try {
        	ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(artefactFileDetails.getSvnProperties().getSvnUser(),
                    artefactFileDetails.getSvnProperties().getSvnPassword());

            repository.setAuthenticationManager(authManager);
        	
        	repository.testConnection();
            	
            SVNNodeKind nodeKind = repository.checkPath("", -1);
            if (nodeKind == SVNNodeKind.DIR) {
            	return true;
            } else {
            	return false;
            }
        } catch (SVNException e) {
            //e.printStackTrace();
        }
        return false;
    }

    protected void commitToSvn(byte[] file, String projectName, String pdfFileName) throws SVNException
    {
        SVNURL url = null;
        SVNRepository repository = null;
        byte[] contents = file;
        String repositoryUrl = artefactFileDetails.getSvnProperties().getSvnUrl();
        if (projectName != null && (!"".equals(projectName))) {
        	if (repositoryUrl.endsWith(IUploadFile.INSERTED_BY_OPENCERT_WEB + "/" + projectName)) {
        		repositoryUrl = repositoryUrl.replace(IUploadFile.INSERTED_BY_OPENCERT_WEB + "/" + projectName, "");
        	}
        }
        try {
            url = SVNURL.parseURIEncoded(repositoryUrl);
            repository = SVNRepositoryFactory.create(url);

        } catch (SVNException e) {
            e.printStackTrace();
            SVNErrorMessage svnErrorMessage = SVNErrorMessage.create(SVNErrorCode.UNKNOWN, problemWithServerSettingsFileMsg);
            throw new SVNException(svnErrorMessage, e);
        }

        try {
            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(artefactFileDetails.getSvnProperties().getSvnUser(),
                    artefactFileDetails.getSvnProperties().getSvnPassword());

            repository.setAuthenticationManager(authManager);
            long latestRevision = repository.getLatestRevision();

            OpencertLogger.info("Repository latest revision (before committing): " + latestRevision);

            String svnComment = "";
            if (uploadFileWindow.getUploadFileMode() == UploadFileMode.MODIFY_FILE) {
            	svnComment = "This evidence file has been modified in the repository using OPENCERT " + reportTitle;
            } else {
            	svnComment = "This evidence file has been added to the repository in OPENCERT " + reportTitle;
            }
            ISVNEditor editor = repository.getCommitEditor(svnComment, null);

            SVNCommitInfo commitInfo = null;
            try {
            	commitInfo = putEvidenceToSvn(editor, IUploadFile.INSERTED_BY_OPENCERT_WEB, projectName, pdfFileName, contents);
            } catch (SVNException e) {                
                e.printStackTrace();
                SVNErrorMessage svnErrorMessage = SVNErrorMessage.create(SVNErrorCode.UNKNOWN, "internal error");
                throw new SVNException(svnErrorMessage, e);
            }

            OpencertLogger.info("The directory has been added to SVN: " + commitInfo);
        } catch (SVNException e) {
            e.printStackTrace();
            SVNErrorMessage svnErrorMessage = SVNErrorMessage.create(SVNErrorCode.UNKNOWN, problemWithSVNPropertiesMsg + "\n" + e.getMessage());
            throw new SVNException(svnErrorMessage, e);
        }
    }

    private void commitAndAddingArtefactToDB(byte[] file) throws Exception
    {
        commitToSvn(file, artefactFileDetails.getProjectName(), artefactFileDetails.getFileName());
        
        Long modifiedArtefactCdoId = null;
        
        ArtefactFileManager artefactFileManager = (ArtefactFileManager) SpringContextHelper.getBeanFromWebappContext(ArtefactFileManager.SPRING_NAME);
        if (uploadFileWindow.getUploadFileMode() == UploadFileMode.MODIFY_FILE) {
            
            String resourceName = uploadFileWindow.getResourceName().getValue();
        	modifiedArtefactCdoId = artefactFileManager.getArtefactCdoIdFromResource(uploadFileWindow.getComplianceResource().getCdoId());
        	artefactFileManager.updateResourceFile(uploadFileWindow.getComplianceResource().getCdoId(), resourceName, artefactFileDetails.getFileName(),
        			artefactFileDetails.getModificationDate());
        	
        } else {
        	
            AssuranceProjectManager projectManager = (AssuranceProjectManager) SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        	String projectResourcePath = projectManager.getAssuranceProjectResourcePath(artefactFileDetails.getAssuranceProject());
        	Artefact createdArtefact = artefactFileManager.createArtefactForEvidenceFile(artefactFileDetails, projectResourcePath);
        	
        	if (createdArtefact != null) {
            	modifiedArtefactCdoId = CDOStorageUtil.getCDOId(createdArtefact);
        	}
        }
        
        generateUploadSuccesfulNotificationWindow();

        cleaningAfterCommit();

        removeAssignComplianceEvidenceWindowFromUI();
        
        for (UnassignOrAssignArtefactListener unassignOrAssignArtefactListener : uploadFileWindow.getUnassignOrAssignArtefactListeners()) {
        	if (getArtefactFileDetails().getBaseAssetComplianceStatus() != null) {
        		unassignOrAssignArtefactListener.artefactAssignedOrUnassigned(getArtefactFileDetails().getBaseAssetComplianceStatus().getItemId());
        	}
       	}
        
        if (modifiedArtefactCdoId != null &&
            OpencertPropertiesReader.getInstance().isImpactAnalysisTrigerringFromWebEnabled()) 
        {
            ImpactAnalyserExecutor impactAnalyserExecutor = new ImpactAnalyserExecutor();
            long baseAssetId = -1;
            if (getArtefactFileDetails().getBaseAssetComplianceStatus() != null) {
            	baseAssetId = getArtefactFileDetails().getBaseAssetComplianceStatus().getItemId();
            }
            impactAnalyserExecutor.performImpactAnalysis(modifiedArtefactCdoId, EventKind.MODIFICATION, 
            		uploadFileWindow.getUnassignOrAssignArtefactListeners(), baseAssetId);
        }
    }

    private void cleaningAfterCommit()
    {
        File file = new File(getPathForEvidenceFile());
        if (file != null) {
            file.delete();
        }

        uploadFileWindow.getArtefactName().setValue("");
        uploadFileWindow.getResourceName().setValue("");
        artefactFileDetails.setFileName("");
    }

    private SVNCommitInfo putEvidenceToSvn(ISVNEditor editor, String insertedbyWebDirPath, String projectDirPath, String filePath, byte[] data)
            throws SVNException
    {
        editor.openRoot(-1);

        //When SVN path is default create directory in SVN: insertedbyWebDirPath and projectDirPath
        String svnUrl = artefactFileDetails.getSvnProperties().getSvnUrl();
        if (svnUrl.endsWith(insertedbyWebDirPath + "/" + projectDirPath)) {
        	addSvnDir(editor, insertedbyWebDirPath);
        	addSvnDir(editor, projectDirPath);
        }
        
        //Add or update file in SVN
        boolean addedFile = false;
        try {
        	editor.addFile(filePath, null, -1);
        	addedFile = true;
        } catch (Exception ex) {
        }

        if (!addedFile) {
	        try {
	        	editor.openFile(filePath, -1);
	        } catch (Exception ex) {
	        }
        }

        editor.applyTextDelta(filePath, null);

        SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
        String checksum = deltaGenerator.sendDelta(filePath, new ByteArrayInputStream(data), editor, true);

        try {
        	editor.closeFile(filePath, checksum);
        } catch (Exception ex) {
        }
        
        if (svnUrl.endsWith(insertedbyWebDirPath + "/" + projectDirPath)) {
        	closeSvnDir(editor);
        	closeSvnDir(editor);
        }

        return editor.closeEdit();
    }
    
    private void addSvnDir(ISVNEditor editor, String dir) {
    	try {
    		editor.addDir(dir, null, -1);
    	} catch (Exception e) {
    	}
    }
    
    private void closeSvnDir(ISVNEditor editor) {
    	try {
    		editor.closeDir();
    	} catch(Exception ex) {
    	}
    }

    private byte[] readStreamToByteArray(InputStream stream) throws IOException
    {
        byte[] buffer = new byte[8192];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int bytesRead;
        while ((bytesRead = stream.read(buffer)) != -1) {
            baos.write(buffer, 0, bytesRead);
        }

        return baos.toByteArray();
    }

    private byte[] loadFile(String sourcePath) throws IOException
    {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(sourcePath);
            return readStreamToByteArray(inputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private void refreshUploadComponent()
    {
        rootVerticalLayout.removeComponent(upload);
        init();
        rootVerticalLayout.addComponent(upload);
    }

    private void generateUploadSuccesfulNotificationWindow()
    {
        final Window uploadSuccesfulWindow = new Window("Upload successful");
        uploadSuccesfulWindow.setStyleName("assignComplianceEvidenceWindow");
        uploadSuccesfulWindow.setModal(true);
        uploadSuccesfulWindow.setResizable(false);
        uploadSuccesfulWindow.center();

        VerticalLayout uploadSuccesfulWindowContent = new VerticalLayout();

        Label info = new Label("");
        info.setContentMode(ContentMode.HTML);
        String uploadMsg;
        if (uploadFileWindow.getUploadFileMode() == UploadFileMode.MODIFY_FILE) {
        	uploadMsg = "<b>\"" + artefactFileDetails.getFileName() + "\"</b> file has been successfully committed to SVN at <br /><b><i>"
                    + artefactFileDetails.getSvnProperties().getSvnUrl()
                    + "</b></i><br />and evidence for the <b>\"" + artefactFileDetails.getBaseElementName() + "\"</b> baseline element has been modified.";
        } else {
        	uploadMsg = "<b>\"" + artefactFileDetails.getFileName() + "\"</b> file has been successfully committed to SVN at <br /><b><i>"
                + artefactFileDetails.getSvnProperties().getSvnUrl()
                + "</b></i><br />and assigned as evidence to the <b>\"" + artefactFileDetails.getBaseElementName() + "\"</b> base asset.";
        }
        info.setValue(uploadMsg);
        info.setStyleName("succesfulInfoLabelStyle");

        Button ok = new Button("OK");
        ok.setStyleName("okAfterSuccesfulCommit");
        ok.addClickListener((e) ->
        {
            UI.getCurrent().removeWindow(uploadSuccesfulWindow);
            evidenceFileUploaderListener.artefactFileUploadingSuccesfully();
        });

        OpencertLogger.info(uploadMsg);
        
        uploadSuccesfulWindowContent.addComponent(info);
        uploadSuccesfulWindowContent.addComponent(ok);
        uploadSuccesfulWindow.setContent(uploadSuccesfulWindowContent);
        UI.getCurrent().addWindow(uploadSuccesfulWindow);
    }

    private void init()
    {
        upload = new Upload("", this);
        upload.setButtonCaption(null);
        upload.addSucceededListener(this);
        upload.addFailedListener(this);
        upload.addChangeListener(this);

        if (uploadFileWindow.isDragAndDropFunctionalityEnabled()) {
        	upload.setVisible(false);
        }
    }

    private String getPathForEvidenceFile()
    {
        return System.getProperty("user.home") + File.separator + artefactFileDetails.getFileName();
    }

    private void removeAssignComplianceEvidenceWindowFromUI()
    {
    	UI.getCurrent().removeWindow(uploadFileWindow);
    }
}