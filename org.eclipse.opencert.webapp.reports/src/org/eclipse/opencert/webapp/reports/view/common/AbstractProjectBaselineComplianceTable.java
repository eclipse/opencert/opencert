/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.listeners.ProjectBaselineComplianceTableListener;
import org.eclipse.opencert.webapp.reports.listeners.util.SelectionEvent;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceManager;

import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;

public abstract class AbstractProjectBaselineComplianceTable
        extends CustomComponent
        implements IBaselineFrameworkChangeListener
{
    private static final long serialVersionUID = -7265149497475281534L;

    protected TreeTable projectBaselineComplianceTable;
    protected ComplianceManager<?> _complianceManager;

    private List<ProjectBaselineComplianceTableListener> projectBaselineComplianceTableListeners = new ArrayList<ProjectBaselineComplianceTableListener>();

    @Override
    public void setBaselineFramework(long baselineFrameworkID)
    {
        HierarchicalContainer dataContainer = _complianceManager.readBaseAssetDataFromDb(baselineFrameworkID);

        projectBaselineComplianceTable.setContainerDataSource(dataContainer);
    }

    public void addProjectBaselineComplianceTableListener(ProjectBaselineComplianceTableListener projectBaselineComplianceTableListener)
    {
        projectBaselineComplianceTableListeners.add(projectBaselineComplianceTableListener);
    }

    protected void notifyAllProjectBaselineComplianceTableListeners(SelectionEvent selectionEvent)
    {
        for (ProjectBaselineComplianceTableListener projectBaselineComplianceTableListener : projectBaselineComplianceTableListeners) {
            projectBaselineComplianceTableListener.baselineElementClicked(selectionEvent);
        }
    }

}
