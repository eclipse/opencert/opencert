/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.export.docx.MetricsEstimationReportDocxGenerator;
import org.eclipse.opencert.webapp.reports.view.common.AbstractBaselineFrameworkReport;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.IReport;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.webapp.reports.view.export.ExportToDocxPanel;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;

@Theme("opencerttheme")
public class MetricsEstimationReport extends AbstractBaselineFrameworkReport implements IReport
{
	private MetricsEstimationTreeMenu metricsEstimationTreeMenu;
	private final ExportToDocxPanel _exportTopDocxPanel;
	private final MetricsEstimationReportDocxGenerator docxGenerator;
	
	public MetricsEstimationReport()
	{
		final String basePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
		docxGenerator = new MetricsEstimationReportDocxGenerator(basePath);
		_exportTopDocxPanel = new ExportToDocxPanel(docxGenerator);
		addBaselineChangeListener(_exportTopDocxPanel);
    }
	
    @Override
    public String getTitle() 
    {
        return "Metrics Estimation report";
    }

    @Override
    public Component getMainComponent(GUIMode guiMode) 
    {	 
    	 HorizontalSplitPanel mainComponent = new HorizontalSplitPanel();
    	 
    	 metricsEstimationTreeMenu = new MetricsEstimationTreeMenu();
    	 addBaselineChangeListener(metricsEstimationTreeMenu);
    	
    	 
    	 MetricsImplementationPanel metricsImplementationPanel = new MetricsImplementationPanel();	 
    	 
    	 MetricsEstimationTreeMenu metricsETM = metricsEstimationTreeMenu;    	 
    	 metricsETM.addMetricsEstimationTreeMenuListener(metricsImplementationPanel);
    	 metricsETM.addMetricsEstimationTreeMenuListener(docxGenerator);
    	

    	 mainComponent.setFirstComponent(metricsEstimationTreeMenu);    	
    	 mainComponent.setSplitPosition(17);
    	 mainComponent.setLocked(true);
    	 mainComponent.setSecondComponent(metricsImplementationPanel);
         
         return mainComponent;
    }

	@Override
    public ReportID getReportID() 
    {
        return ReportID.METRICS_ESTIMATION;
    }

	@Override
	public Component getExportToDocxComponent() {
		
		 HorizontalLayout panel = new HorizontalLayout();
	     panel.addComponent(_exportTopDocxPanel.getButton());
	        
	     return panel;
	}
	
	@Override
    public void projectChanged(AssuranceProjectWrapper newProject)
    {
        super.projectChanged(newProject);
        _exportTopDocxPanel.setNewProject(newProject);
        metricsEstimationTreeMenu.setCurrentProject(newProject);
   }
}