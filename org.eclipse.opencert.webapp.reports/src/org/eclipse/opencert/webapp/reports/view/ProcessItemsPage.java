/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.webapp.reports.containers.ActivityWrapper;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

public class ProcessItemsPage extends ItemsPage {
	private static final String TITLE = "Process Items";
	
	private static final String NAME = "name";
	private static final String ID = "id";
	private static final String PARTICIPANT = "participant";
	private static final String DESCRIPTION = "description";
	private static final String START_TIME = "startTime";
	private static final String END_TIME = "endTime";
	private static final String PARENT_MODEL = "parentModel";
	private static final String PRECEDING_ACTIVITY = "precedingActivity";
	private static final String REQUIRED_ARTEFACT = "requiredArtefact";
	private static final String PRODUCED_ARTEFACT = "producedArtefact";
	private static final String ACTIVITY_EVALUATION = "activityEvaluation";
	private static final String ASSURANCE_ASSET_EVENT = "assuranceAssetEvent";
	
	private static final String NAME_LABEL = "Activity Name";
	private static final String ID_LABEL = "Activity Id";
	private static final String PARTICIPANT_LABEL = "Participant(s)";
	private static final String DESCRIPTION_LABEL = "Activity Description";
	private static final String START_TIME_LABEL = "Activity Start Time";
	private static final String END_TIME_LABEL = "Activity End Time";
	private static final String PARENT_MODEL_LABEL = "Activity Parent Model";
	private static final String PRECEDING_ACTIVITY_LABEL = "Preceding Activity";
	private static final String REQUIRED_ARTEFACT_LABEL = "Required Artefact";
	private static final String PRODUCED_ARTEFACT_LABEL = "Produced Artefact";
	private static final String ACTIVITY_EVALUATION_LABEL = "Activity Evaluation";
	private static final String ASSURANCE_ASSET_EVENT_LABEL = "Activity Asset Event";
	
	private static final String ACTIVITY_PARTICIPANT_LABEL_DESC = "<b>Participant(s): </b><br>";
	private static final String PRECEDING_ACTIVITY_LABEL_DESC = "<b>Preceding Activities: </b><br>";
	private static final String REQUIRED_ARTEFACT_DESC = "<b>Required Artefacts: </b><br>";
	private static final String PRODUCED_ARTEFACT_DESC = "<b>Produced Artefacts: </b><br>";
	
	@Override
	public ReportID getReportID() {
		return ReportID.PROCESS_ITEMS;
	}

	@Override
	public String getTitle() {
		return TITLE;
	}
	
	@Override
	protected void addColumnHeaders() {
		itemsTable.setColumnHeader(NAME, NAME_LABEL);
		itemsTable.setColumnHeader(ID, ID_LABEL);
		itemsTable.setColumnHeader(PARTICIPANT, PARTICIPANT_LABEL);
		itemsTable.setColumnHeader(DESCRIPTION, DESCRIPTION_LABEL);
		itemsTable.setColumnHeader(START_TIME, START_TIME_LABEL);
		itemsTable.setColumnHeader(END_TIME, END_TIME_LABEL);
		itemsTable.setColumnHeader(PARENT_MODEL, PARENT_MODEL_LABEL);
		itemsTable.setColumnHeader(PRECEDING_ACTIVITY, PRECEDING_ACTIVITY_LABEL);
		itemsTable.setColumnHeader(REQUIRED_ARTEFACT, REQUIRED_ARTEFACT_LABEL);
		itemsTable.setColumnHeader(PRODUCED_ARTEFACT, PRODUCED_ARTEFACT_LABEL);
		itemsTable.setColumnHeader(ACTIVITY_EVALUATION, ACTIVITY_EVALUATION_LABEL);
		itemsTable.setColumnHeader(ASSURANCE_ASSET_EVENT, ASSURANCE_ASSET_EVENT_LABEL);
		itemsTable.setColumnExpandRatio(NAME, 7);
		itemsTable.setColumnExpandRatio(ID, 3);
		itemsTable.setColumnExpandRatio(PARTICIPANT, 6);
		itemsTable.setColumnExpandRatio(DESCRIPTION, 13);
		itemsTable.setColumnExpandRatio(START_TIME, 7);
		itemsTable.setColumnExpandRatio(END_TIME, 7);
		itemsTable.setColumnExpandRatio(PARENT_MODEL, 7);
		itemsTable.setColumnExpandRatio(PRECEDING_ACTIVITY, 10);
		itemsTable.setColumnExpandRatio(REQUIRED_ARTEFACT, 10);
		itemsTable.setColumnExpandRatio(PRODUCED_ARTEFACT, 10);
		itemsTable.setColumnExpandRatio(ACTIVITY_EVALUATION, 10);
		itemsTable.setColumnExpandRatio(ASSURANCE_ASSET_EVENT, 10);
	}
	
	@Override
	protected void addContainerProperties(HierarchicalContainer hc) {
        hc.addContainerProperty(NAME, Label.class, "");
        hc.addContainerProperty(ID, Label.class, "");
        hc.addContainerProperty(PARTICIPANT, Label.class, "");
        hc.addContainerProperty(DESCRIPTION, Label.class, "");
        hc.addContainerProperty(START_TIME, Label.class, "");
        hc.addContainerProperty(END_TIME, Label.class, "");
        hc.addContainerProperty(PARENT_MODEL, Label.class, "");
        hc.addContainerProperty(PRECEDING_ACTIVITY, Label.class, "");
        hc.addContainerProperty(REQUIRED_ARTEFACT, Label.class, "");
        hc.addContainerProperty(PRODUCED_ARTEFACT, Label.class, "");
        hc.addContainerProperty(ACTIVITY_EVALUATION, Label.class, "");
        hc.addContainerProperty(ASSURANCE_ASSET_EVENT, Label.class, "");
	}
	
	@Override
	protected Component addAdditionalComponent() {
		Label processItemsLabel = new Label("Process items:");
		processItemsLabel.setStyleName("complianceDetailsTitle");
		return processItemsLabel;
	}
	
	@Override
	protected HierarchicalContainer createDataContainer(AssuranceProject project) {
		HierarchicalContainer hc = createEmptyTableDataContainer();
		
		EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
		for (AssetsPackage assetsPackage : assetsPackages) {
			if (assetsPackage.isIsActive()) {
				EList<ProcessModel> processModels = assetsPackage.getProcessModel();
				for (ProcessModel processModel : processModels) {
					String processModelName = processModel.getName();
					EList<Activity> activities = processModel.getOwnedActivity();
					
					addItemToDataContainer(activities, processModelName, null, hc);
				}
			}
		}
		
		return hc;
	}
	
	private void addItemToDataContainer(List<Activity> activities, String processModelName, ActivityWrapper activityParent, HierarchicalContainer hc) {
		for (Activity activity : activities) {
			Label activityName = createLabel(activity.getName());
			Label activityId = createLabel(activity.getId());
			Label descriptionLabel = createLabel(activity.getDescription());
			Label parentModel = createLabel(processModelName);
			Label startTimeLabel = createTimeLabel(activity.getStartTime());
			Label endTimeLabel = createTimeLabel(activity.getEndTime());
			Label participant = createActivityParticipantLabel(activity);
			Label precedingActivityLabel = createPrecedingActivityLabel(activity);
			Label requiredArtefactLabel = createArtefactLabel(activity.getRequiredArtefact(), REQUIRED_ARTEFACT_DESC);
			Label producedArtefactLabel = createArtefactLabel(activity.getProducedArtefact(), PRODUCED_ARTEFACT_DESC);
			Label activityEvaluationLabel = createEvaluationLabel(activity.getEvaluation());
			Label activityAssuranceAssetEventLabel = createAssuranceAssetEventsLabel(activity.getAssetEvent(), false);
			ActivityWrapper activityWrapper = new ActivityWrapper(CDOStorageUtil.getCDOId(activity), activityName, activityId, 
					descriptionLabel, parentModel, startTimeLabel, endTimeLabel, participant, precedingActivityLabel, 
					requiredArtefactLabel, producedArtefactLabel, activityEvaluationLabel, activityAssuranceAssetEventLabel);
			Item item =  hc.addItem(activityWrapper); 
			mergePropertiesWithData(item, activityWrapper);
			if (activityParent != null) {
				hc.setParent(activityWrapper, activityParent);
			}
			
			EList<Activity> activityChilds = activity.getSubActivity();
			if (activityChilds.isEmpty()) {
				hc.setChildrenAllowed(activityWrapper, false);
			} else {
				addItemToDataContainer(activityChilds, processModelName, activityWrapper, hc);
			}
		}
	}
	
    @SuppressWarnings("unchecked")
	private void mergePropertiesWithData(Item item, ActivityWrapper activityWrapper) 
    {
        item.getItemProperty(NAME).setValue(activityWrapper.getName());
        item.getItemProperty(ID).setValue(activityWrapper.getId());
        item.getItemProperty(PARTICIPANT).setValue(activityWrapper.getParticipant());
        item.getItemProperty(DESCRIPTION).setValue(activityWrapper.getDescription());
        item.getItemProperty(START_TIME).setValue(activityWrapper.getStartTime());
        item.getItemProperty(END_TIME).setValue(activityWrapper.getEndTime());
        item.getItemProperty(PARENT_MODEL).setValue(activityWrapper.getParentModel());
        item.getItemProperty(PRECEDING_ACTIVITY).setValue(activityWrapper.getPrecedingActivity());
        item.getItemProperty(REQUIRED_ARTEFACT).setValue(activityWrapper.getRequiredArtefact());
        item.getItemProperty(PRODUCED_ARTEFACT).setValue(activityWrapper.getProducedArtefact());
        item.getItemProperty(ACTIVITY_EVALUATION).setValue(activityWrapper.getEvaluation());
        item.getItemProperty(ASSURANCE_ASSET_EVENT).setValue(activityWrapper.getAssuranceAssetEvent());
    }
    
    private Label createActivityParticipantLabel(Activity activity) {
    	EList<Participant> participants = activity.getParticipant();
    	String participantName = "";
    	boolean setParticipantName = true;
		String participantDesc = ACTIVITY_PARTICIPANT_LABEL_DESC;
		int i=1;
		for (Participant participant : participants) {
			if (setParticipantName) {
				participantName = createDataName(participant.getName());
				setParticipantName = false;
			}
			participantDesc += "<b>" + i + "</b>" + ".<br>";
			i++;
			participantDesc += "<i>Name:</i> " + participant.getName() + "<br>";
			participantDesc += "<i>Description:</i> " + participant.getDescription() + "<br>";
		}
		
		if (ACTIVITY_PARTICIPANT_LABEL_DESC.equals(participantDesc)) {
			return createLabel(participantName);
		} else {
			return createLabel(participantName, participantDesc);
		}
    }
    
    private Label createPrecedingActivityLabel(Activity activity) {
    	EList<Activity> precedingActivities = activity.getPrecedingActivity();
    	String precedingActivityName = "";
    	boolean setPrecedingActivityName = true;
		String precedingActivityDesc = PRECEDING_ACTIVITY_LABEL_DESC;
		int i=1;
		for (Activity precedingActivity : precedingActivities) {
			if (setPrecedingActivityName) {
				precedingActivityName = createDataName(precedingActivity.getName());
				setPrecedingActivityName = false;
			}
			precedingActivityDesc += "<b>" + i + "</b>" + ".<br>";
			i++;
			precedingActivityDesc += "<i>Id:</i> " + createDataValue(precedingActivity.getId()) + "<br>";
			precedingActivityDesc += "<i>Name:</i> " + createDataValue(precedingActivity.getName()) + "<br>";
			precedingActivityDesc += "<i>Description:</i> " + createDataValue(precedingActivity.getDescription()) + "<br>";
		}
		
		if (PRECEDING_ACTIVITY_LABEL_DESC.equals(precedingActivityDesc)) {
			return createLabel(precedingActivityName);
		} else {
			return createLabel(precedingActivityName, precedingActivityDesc);
		}
    }
    
    private Label createArtefactLabel(List<Artefact> artefacts, String artefactTypeDesc) {
    	String artefactName = "";
    	boolean setArtefactName = true;
		String artefactDesc = artefactTypeDesc;
		int i=1;
		for (Artefact artefact : artefacts) {
			if (setArtefactName) {
				artefactName = createDataName(artefact.getName());
				setArtefactName = false;
			}
			artefactDesc += "<b>" + i + "</b>" + ".<br>";
			i++;
			artefactDesc += "<i>Id:</i> " + createDataValue(artefact.getId()) + "<br>";
			artefactDesc += "<i>Name:</i> " + createDataValue(artefact.getName()) + "<br>";
			artefactDesc += "<i>Description:</i> " + createDataValue(artefact.getDescription()) + "<br>";
		}
		
		if (artefactTypeDesc.equals(artefactDesc)) {
			return createLabel(artefactName);
		} else {
			return createLabel(artefactName, artefactDesc);
		}
    }
    
}
