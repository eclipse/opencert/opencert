/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.Image;
import com.vaadin.ui.TreeTable;

public class CustomFilterForProjectBaselineTableRows implements Container.Filter
{
    private static final long serialVersionUID = -7132173575432725171L;
    private String propertyId;
    private String type;
    private TreeTable baselineTable;


    public CustomFilterForProjectBaselineTableRows(String propertyId, String type, TreeTable baselineTable)
    {
        this.propertyId = propertyId;
        this.type = type;
        this.baselineTable = baselineTable;
    }


    @Override
    public boolean passesFilter(Object itemId, Item item)
            throws UnsupportedOperationException
    {
        Property<?> property = baselineTable.getContainerProperty(itemId, propertyId);
        if (property != null) {
            return ((Image) property.getValue()).getCaption().equals(type);
        }
        return false;
    }


    @Override
    public boolean appliesToProperty(Object propertyId)
    {
        return propertyId != null && propertyId.equals(this.propertyId);
    }
}
