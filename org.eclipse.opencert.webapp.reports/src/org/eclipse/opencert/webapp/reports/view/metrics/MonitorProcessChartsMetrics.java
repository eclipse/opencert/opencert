/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;


import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;











import com.ibm.icu.text.SimpleDateFormat;
import com.vaadin.annotations.Theme;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.metrics.JFreeChartWrapper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;

import java.util.concurrent.TimeUnit;




@Theme("opencerttheme")
public class MonitorProcessChartsMetrics 
	extends CustomComponent

{

	private static final long serialVersionUID = 8918187826514090548L;
	private static final String PARTICIPANTS_EVENTS = "Assurance Asset Events per Participant";
    private static final String PARTICIPANTS_ACTIVITIES = "Participants Performance";
    private static final String WEEKLY_ACTIVITIES = "Activities' Evolution";
    private static final String NO_DATA_AVAILABLE = "No Data Available";
    
    private byte[] participant;
    private byte[] performance; 
    private byte[] evolution;
    
    
    
    public String getParticipantsEvents() {
		return PARTICIPANTS_EVENTS;
	}




	public String getParticipantsActivities() {
		return PARTICIPANTS_ACTIVITIES;
	}




	public String getWeeklyActivities() {
		return WEEKLY_ACTIVITIES;
	}




	public byte[] getParticipant() {
		return participant;
	}




	public byte[] getPerformance() {
		return performance;
	}




	public byte[] getEvolution() {
		return evolution;
	}






	private HashMap<String,Triplet> participantsData;
    List<Activity> activities;
    
    protected class Triplet{
    	protected int activityEvents;
    	protected int artefactEvents;
    	protected List<Activity> activities;
    	
    	protected Triplet(int activityEventy, int artefactEvents){
    		this.activityEvents=activityEventy;
    		this.artefactEvents=artefactEvents;
    		this.activities=new ArrayList<Activity>();
    	}

    	protected int getActivityEvents() {
			return activityEvents;
		}

		protected void setActivityEvents(int activityEvents) {
			this.activityEvents = activityEvents;
		}

		protected int getArtefactEvents() {
			return artefactEvents;
		}

		protected void setArtefactEvents(int artefactEvents) {
			this.artefactEvents = artefactEvents;
		}

		protected List<Activity> getActivities() {
			return activities;
		}

		protected Triplet addActivity(Activity activity) {
			activities.add(activity);
			return this;
		}

	
    }
    
    
 
    class DateComparator implements Comparator<Activity> {
        @Override
        public int compare(Activity a, Activity b) { 
            return a.getStartTime().before(b.getStartTime()) ? -1 : a.getStartTime().after(b.getStartTime()) ? 1 : 0;
        }
    }
    
    
                      
    public MonitorProcessChartsMetrics(AssuranceProjectWrapper project)
    {      
         
        VerticalLayout mainPanel = new VerticalLayout();
		
		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);
		
	
		setParticipantData(project);
		mainPanel.addComponent(participantsEventsMetrics());
		mainPanel.addComponent(participantsActivityMetrics());
		mainPanel.addComponent(weeklyActivitiesMetrics());
		
	
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
        
 
    }




    private Component weeklyActivitiesMetrics() {
    	final JFreeChart chart = ChartFactory.createBarChart(
    			WEEKLY_ACTIVITIES,      // chart title
                "Months",               // domain axis label
                "Number of Activities", // range axis label
                populateWithWeeklyActivityData(),                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        final CategoryAxis axis = plot.getDomainAxis();
        axis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 8.0)
        );

      
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        
      

        

        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
       
        try {
        	evolution = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;
	}




	private CategoryDataset populateWithWeeklyActivityData() {
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
		long today = new Date().getTime();
		long firstDate = activities.isEmpty() ? today : activities.get(0).getStartTime().getTime();
		long interval = createAdecuateInterval(firstDate,today);
		
		int notStarted=0;
		int developing=0;
		int developed=0;
		String from ="";
		String to = "";
		
		if(!activities.isEmpty()){
			long lastDate = firstDate + interval;
			while (lastDate <= today){
				
				for(Activity activity : activities){
					if(activity.getStartTime()!=null){
						if(firstDate < activity.getStartTime().getTime()) notStarted++;
						else if (activity.getEndTime()!= null && firstDate > activity.getEndTime().getTime()) developed++;
						else developing++;
					}
				}		
				from = sdf.format(new Date(firstDate));
				to = sdf.format(new Date(lastDate));
				
				dataset.addValue(notStarted, "Not Started",  from + " - " + to);
				dataset.addValue(developing, "Under Development",  from + " - " + to);
				dataset.addValue(developed, "Developed",  from + " - " + to);
				
				firstDate=lastDate;
				lastDate+=interval;
				notStarted=0;developing=0;developed=0;
			}
		}
		return dataset;
	}




	private long createAdecuateInterval(long firstDate, long today) {
		
		long week = TimeUnit.MILLISECONDS.convert(7, TimeUnit.DAYS);
		long month =  TimeUnit.MILLISECONDS.convert(30L, TimeUnit.DAYS);
		
		//10 series = good visualization
		
		long difference = today - firstDate;
		
		if (difference > 10L * 6L * month) return 12L * month;
		else if (difference > 10L * 2L * month) return 6L * month;
		else if (difference > 10L * month) return 2L * month;
		else if (difference > 10L * week) return month;
		else return week;
		
	}




	private Component participantsActivityMetrics() {
    	final JFreeChart chart = ChartFactory.createBarChart(
    			PARTICIPANTS_ACTIVITIES,      // chart title
                "Participants",               // domain axis label
                "Number of Activities", // range axis label
                populateWithParticipantsActivityData(),                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
      
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        
        

        

        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
       
        try {
			performance = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;
	}




	private CategoryDataset populateWithParticipantsActivityData() {

		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		Set<String> keys = participantsData.keySet();
		Iterator<String> it = keys.iterator();
		while (it.hasNext()){
			String name = it.next();
			Triplet t = participantsData.get(name);
			int notStarted=0;
			int developing=0;
			int developed=0;
			List<Activity> activities=t.getActivities();
			if(!activities.isEmpty()){
				for(Activity activity : activities){
					if(activity.getStartTime()!=null){
						Date actualTime = new Date();
						if(actualTime.before(activity.getStartTime())) notStarted++;
						else if (activity.getEndTime()!= null && actualTime.after(activity.getEndTime())) developed++;
						else developing++;
					}
				}
			}
			dataset.addValue(notStarted, "Not Started", name);
			dataset.addValue(developing, "Under Development", name);
			dataset.addValue(developed, "Developed", name);
		}
		
	
        return dataset;
	}




	private Component participantsEventsMetrics() {
    	final JFreeChart chart = ChartFactory.createBarChart(
    			PARTICIPANTS_EVENTS,      // chart title
                "Participants",               // domain axis label
                "Number of Assurance Asset Events", // range axis label
                populateWithParticipantsAssetsData(),                  // data
                PlotOrientation.VERTICAL, // orientation
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
            );

    	// get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage(NO_DATA_AVAILABLE);
        plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
        plot.setNoDataMessagePaint(Color.RED);  
        
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

      
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
        renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        
     

        

        chart.setBackgroundPaint(Color.white);
        chart.setAntiAlias(true);
        chart.setBorderVisible(false);
		
       
        try {
			participant = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;
	}




	private CategoryDataset populateWithParticipantsAssetsData() {
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		Set<String> keys = participantsData.keySet();
		Iterator<String> it = keys.iterator();
		while (it.hasNext()){
			String name = it.next();
			Triplet t = participantsData.get(name);
			dataset.addValue(t.getActivityEvents(), "Activity Events", name);
			dataset.addValue(t.getArtefactEvents(), "Artefact Events", name);
		}
		
	
        return dataset;
	}




	private void setParticipantData(AssuranceProjectWrapper newProject) {

    	
    	activities = new ArrayList<Activity>();
    	List<Participant> participants = new ArrayList<Participant>();
    	
    	AssuranceProjectManager projectManager = 
				(AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
    	AssuranceProject project = projectManager.getProject(newProject.getId());
    	
    	if(project != null){
    		//Getting MM data
	    	EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
			for (AssetsPackage assetsPackage : assetsPackages) {
				if (assetsPackage.isIsActive()) {				
					EList<ProcessModel> processModels = assetsPackage.getProcessModel();
					for (ProcessModel processModel : processModels) {	
						/**/
						EObject eProcessModel= (EObject) processModel; 
			    		 for (Iterator<EObject> iterator = eProcessModel.eAllContents(); iterator.hasNext();) {
			        		 EObject aEObject = iterator.next();
			        		 if (aEObject instanceof Activity){
			        			 Activity activity = (Activity) aEObject;
			        			 activities.add(activity);
			        		 }
			        		 else if(aEObject instanceof Participant){
			        			 Participant participant = (Participant) aEObject;
			        			 participants.add(participant);
			        		 }
			    		 }
						/**/
						
					}
				}
			}
			//Fulfilling data
			participantsData = new HashMap<String,Triplet>(participants.size());
			// -- Asset Events
			if(!participants.isEmpty()){
				for(Participant participant : participants){
					if(participant.getName()!=null && !participant.getName().equals("")){
						int numberOfActivitiesEvents = participant.getTriggeredAssetEvent().size();
						int numberOfArtefactsEvents = 0;
						List<Artefact> ownedArtefacts = participant.getOwnedArtefact();
						if(!ownedArtefacts.isEmpty()){
							for(Artefact artefact : ownedArtefacts) numberOfArtefactsEvents += artefact.getLifecycleEvent().size();
						}
						Triplet t = new Triplet(numberOfActivitiesEvents,numberOfArtefactsEvents);
						participantsData.put(participant.getName(),t);
					}
				}
			}
			
			if(!activities.isEmpty()){
				for (Activity activity : activities){
					List<Participant> participantsPerActivity = activity.getParticipant();
					if(!participantsPerActivity.isEmpty()){
						for(Participant participant : participantsPerActivity){
							if(participant.getName()!=null && !participant.getName().equals("")){
								if(participantsData.containsKey(participant.getName())) {
									participantsData.put(participant.getName(), participantsData.get(participant.getName()).addActivity(activity));
								}
								else {
									Triplet t = new Triplet(0,0);
									t.addActivity(activity);
									participantsData.put(participant.getName(), t);
								}
							}
						}
					}
				}
			}
    	
			//Sort activities by starting date.
			activities.sort(new DateComparator());
    	}
		
	}



	
	
}

