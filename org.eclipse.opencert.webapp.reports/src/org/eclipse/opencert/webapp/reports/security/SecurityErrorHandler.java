/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.security;

import org.springframework.security.access.AccessDeniedException;

import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.ErrorEvent;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.Page;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class SecurityErrorHandler
    implements ErrorHandler
{
    private static final long serialVersionUID = 1L;

    @Override
    public void error(ErrorEvent event)
    {
        AbstractComponent component = DefaultErrorHandler.findAbstractComponent(event);
        if (component != null) {
            Throwable throwable = event.getThrowable();
            if (throwable instanceof AccessDeniedException) {
                
                new Notification("Access denied", throwable.getMessage(), Type.ERROR_MESSAGE, true).show(Page.getCurrent());
                
                return;
            }
        }
        
        DefaultErrorHandler.doDefault(event);
    }
}
