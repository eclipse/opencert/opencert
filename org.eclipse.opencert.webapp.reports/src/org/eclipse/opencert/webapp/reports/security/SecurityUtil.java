/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.security;

import java.util.Collection;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class SecurityUtil
{
    public static final String ACCESS_RIGHT_ADMIN = "ADMIN";
    public static final String ACCESS_RIGHT_USER = "USER";
    public static final String ACCESS_RIGHT_PROJECT_PREFIX = "PROJECT_ACCESS:";
    
    private static boolean securityEnabled = false;

    public static void verifyAccessRight(String requiredAccessRight)
    {
        if (!isSecurityEnabled()) {
            return;
        }
        
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        
        if (authorities.contains(new SimpleGrantedAuthority(ACCESS_RIGHT_ADMIN))) {
            return;
        }
        
        if (!authorities.contains(new SimpleGrantedAuthority(requiredAccessRight))) {
            throw new AccessDeniedException("Unsufficient privileges:" + ("To perform this action you need the " + requiredAccessRight + " access right."));
        }
    }

    
    public static boolean hasAccessRight(String requiredAccessRight)
    {
        if (!isSecurityEnabled()) {
            return true;
        }
        
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if (authorities.contains(new SimpleGrantedAuthority(ACCESS_RIGHT_ADMIN))) {
            return true;
        }
        
        if (!authorities.contains(new SimpleGrantedAuthority(requiredAccessRight))) {
            return false;
        }
        
        return true;
    }
    
    public static String getProjectsAccess()
    {
    	String projectAccess = "";
    	boolean firstAccess = true;
    	
    	Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority autority: authorities) {
        	String autorityValue = autority.getAuthority();
        	if (autorityValue.contains(ACCESS_RIGHT_PROJECT_PREFIX)) {
        		if (!firstAccess) {
        			projectAccess += ", ";
        		}
        		projectAccess += autorityValue.replaceAll(ACCESS_RIGHT_PROJECT_PREFIX, "");
        		firstAccess = false;
        	}
        }
        
        return projectAccess;
    }


    public static String getLoginName()
    {
        if (!isSecurityEnabled()) {
            return "";
        }
        
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
    
    
    public static boolean isSecurityEnabled()
    {
        return securityEnabled;
    }


    /**
     *  This is just an ugly way to allow enabling it from the Spring context, as we need to keep it simple for the POC 
     */
    public static Boolean enableSecurity()
    {
        securityEnabled = true;
        
        return true;
    }
}
