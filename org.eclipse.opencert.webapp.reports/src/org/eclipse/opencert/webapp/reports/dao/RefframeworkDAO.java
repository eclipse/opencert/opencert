/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class RefframeworkDAO {
        
	public List<RefFramework> getAllRefframeworks() {  
		
        final List<RefFramework> allFrameworks = new ArrayList<RefFramework>();

        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String allFrameworkQuery = "select * from refframework_refframework where" + CDOStorageUtil.getMandatoryCDOQuerySuffix(false);
                CDOQuery allFrameworkCqo = cdoTransaction.createQuery("sql", allFrameworkQuery);
                allFrameworks.addAll(allFrameworkCqo.getResult(RefFramework.class));         
            }
        }.executeReadOnlyOperation();
        
        if (allFrameworks.isEmpty()) {
        	return null;
        }
        return allFrameworks;
    }

	
	 public RefFramework getRefFramework(final long refFrameworkUid) {   
		 
	        final List<RefFramework> refFramework = new ArrayList<RefFramework>();
	        
	        new HttpRequestDrivenInTransactionExecutor()
	        {
	            @Override
	            public void executeInTransaction(CDOTransaction cdoTransaction)
	            {
	                String reffFrameworkQuery = "select * from refframework_refframework where cdo_id = '" + refFrameworkUid + "'" + CDOStorageUtil.getMandatoryCDOQuerySuffix();
	                CDOQuery reffFrameworkCqo = cdoTransaction.createQuery("sql", reffFrameworkQuery);
	                refFramework.add(reffFrameworkCqo.getResultValue(RefFramework.class));         
	            }
	        }.executeReadOnlyOperation();
	        
	        return refFramework.get(0);
	    }



    
}
