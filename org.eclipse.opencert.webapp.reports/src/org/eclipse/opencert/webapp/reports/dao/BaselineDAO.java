/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.webapp.reports.util.IdNameDesc;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class BaselineDAO {
        
    public BaseFramework getBaseFramework(final long baseFrameworkUid) 
    {      
        final List<BaseFramework> baseFrameworks = new ArrayList<BaseFramework>();
        
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                BaseFramework bf = getBaseFrameworkForTransaction(baseFrameworkUid, cdoTransaction);
                baseFrameworks.add(bf);
            }
            
        }.executeReadOnlyOperation();
        
        return baseFrameworks.get(0);
    }

    public void addBaseArtefactToBaseline(Long baseFrameworkUid, IdNameDesc addedArt)
    {
        new HttpRequestDrivenInTransactionExecutor() 
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                BaseFramework baseFramework = getBaseFrameworkForTransaction(baseFrameworkUid, cdoTransaction);
                
                BaseArtefact baseArtefact = BaselineFactory.eINSTANCE.createBaseArtefact();
                baseArtefact.setId(addedArt.name);
                baseArtefact.setName(addedArt.name);
                baseArtefact.setDescription(addedArt.desc);
                baseArtefact.setIsSelected(true);
                
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                resource.getContents().add(baseArtefact);
                
                baseFramework.getOwnedArtefact().add(baseArtefact);
            }
        }.executeReadWriteOperation();        
    }
    
    public void removeBaseArtefactFromBaseline(final long baseFrameworkUid, Long artefactID)
    {
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
    //          //The below does NOT work!
    //          CDOObject cdoBaseArtefact = (CDOObject) cdoTransaction.getObject(CDOUtil.getCDOObject(baseArtefact));
    //          Resource resource = ((BaseArtefact)cdoBaseArtefact).eResource();
    //          EList<EObject> contents = resource.getContents();   // it contains ONLY BaseFramework (so .remove() will not work)!!!! (although we have queried for BaseArtefact!!) 
    //          boolean bb =  resource.getContents().remove(cdoBaseArtefact); // it does not work
                
                BaseFramework baseFramework = getBaseFrameworkForTransaction(baseFrameworkUid, cdoTransaction);
                EList<BaseArtefact> ownedArtefacts = baseFramework.getOwnedArtefact();
                Iterator<BaseArtefact> it = ownedArtefacts.iterator();
                while (it.hasNext())
                {
                    Long cdoId = CDOStorageUtil.getCDOId(it.next());
                    if (artefactID.equals(cdoId)) {
                        it.remove();
                        break;
                    }
                }
            }
        }.executeReadWriteOperation();
    }
    
    public void updateBaselineFrameworkName(final long baseFrameworkUid, String baselineFrameworkName, AssuranceProject assuranceProject)
    {
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {              
            	if (baseFrameworkUid > 0) {
            		BaseFramework baseFramework = getBaseFrameworkForTransaction(baseFrameworkUid, cdoTransaction);
            		baseFramework.setName(baselineFrameworkName);
            	} else {
                    CDOResource baselineResource  = cdoTransaction.getOrCreateResource(baselineFrameworkName + ".baseline");
                    
                    BaseFramework baselineFramework = BaselineFactory.eINSTANCE.createBaseFramework();
                    baselineFramework.setName(baselineFrameworkName); 
                    baselineResource.getContents().add(baselineFramework);

                    AssuranceProject assuranceProj = (AssuranceProject) cdoTransaction.getObject(CDOUtil.getCDOObject(assuranceProject));
                    
                    BaselineConfig baselineConfig = AssuranceprojectFactory.eINSTANCE.createBaselineConfig();
                    baselineConfig.setName(baselineFrameworkName);
                    baselineConfig.setIsActive(true);     
                    assuranceProj.getBaselineConfig().add(baselineConfig);
                    
                    baselineConfig.getRefFramework().add(baselineFramework);
            	}
            }
        }.executeReadWriteOperation();
    }
    
    private static BaseFramework getBaseFrameworkForTransaction(final long baseFrameworkUid, CDOTransaction cdoTransaction)
    {
        String baseFrameworkQuery = "select * from baseline_baseframework where cdo_id = '" + baseFrameworkUid + "'" + CDOStorageUtil.getMandatoryCDOQuerySuffix();
        CDOQuery baseFrameworkCqo = cdoTransaction.createQuery("sql", baseFrameworkQuery);
        return baseFrameworkCqo.getResultValue(BaseFramework.class);
    }
}
