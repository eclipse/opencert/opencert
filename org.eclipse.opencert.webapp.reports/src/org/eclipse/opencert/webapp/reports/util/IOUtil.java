/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtil
{
    public static void close(InputStream in)
    {
        if (in == null) {
            return;
        }
        try {
            in.close();
        } catch (IOException e) {
        }
    }

    public static void close(OutputStream os)
    {
        if (os == null) {
            return;
        }
        try {
            os.close();
        } catch (IOException ex) {
        }
    }
}
