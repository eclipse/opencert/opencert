/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

public enum ComplianceStatusType {
    EMPTY("-", "No compliance evidence provided yet", 0), OK("Compliant", "Fully Compliant", 1), PARTLY("Partial", "Partially compliant - Deviation justification should be specified", 2), NO("Not compliant",
            "Not compliant", 3);

    private String value;

    private String description;

    private int priority;

    private ComplianceStatusType(String value, String description, int priority)
    {
        this.value = value;
        this.description = description;
        this.priority = priority;
    }

    public String getValue()
    {
        return value;
    }

    public String getDescription()
    {
        return description;
    }

    public int getPriority()
    {
        return priority;
    }
}
