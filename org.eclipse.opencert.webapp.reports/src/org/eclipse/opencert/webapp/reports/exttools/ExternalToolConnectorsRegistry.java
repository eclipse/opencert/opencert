/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.exttools.connectors.ComplianceBugzillaConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotDefinedConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.NotNeededConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.ParasoftDTPDefectConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.ParasoftDTPSAConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.ParasoftDTPUTConnector;
import org.eclipse.opencert.webapp.reports.exttools.connectors.TESTConnector;
import org.eclipse.opencert.externaltools.api.IExternalToolConnector;

/**
 * This will be singleton - either in the Spring meaning, or traditional.
 * 
 * Please, construct it traditionally right now - it's cheap.
 *
 */
public class ExternalToolConnectorsRegistry
{
    private List<IExternalToolConnector> externalToolConnectorsDescriptors = 
            new ArrayList<IExternalToolConnector>();
    
    
    public ExternalToolConnectorsRegistry()
    {
        externalToolConnectorsDescriptors.add(new NotDefinedConnector());
        externalToolConnectorsDescriptors.add(new NotNeededConnector());
        externalToolConnectorsDescriptors.add(new ParasoftDTPSAConnector());
        externalToolConnectorsDescriptors.add(new ParasoftDTPUTConnector());
        externalToolConnectorsDescriptors.add(new ParasoftDTPDefectConnector());
        externalToolConnectorsDescriptors.add(new ComplianceBugzillaConnector());
        externalToolConnectorsDescriptors.add(new TESTConnector());
    }
    
    
    public List<IExternalToolConnector> getExternalToolConnectors()
    {
        return externalToolConnectorsDescriptors;
    }
}
