/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.impactanalysis;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.view.common.ConfirmationWindow;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.impactanalysis.ImpactAnalyser;
import org.eclipse.opencert.impactanalysis.ImpactSource;
import org.eclipse.opencert.impactanalysis.relations.ArtefactRelationType;
import org.eclipse.opencert.impactanalysis.relations.IArtefactRelationImpact;
import org.eclipse.opencert.storage.cdo.session.ThreadLocalCDOSessionProvider;

import com.vaadin.ui.UI;


public class ImpactAnalyserExecutor
{
    private ImpactAnalyser impactAnalyser;
    private List<IArtefactRelationImpact> artefactsRelationImpacts;

    
    public void performImpactAnalysis(List<Long> sourceArtefactCDOIds, EventKind impactingEventKind, List<UnassignOrAssignArtefactListener> listeners,
    		Long baseAssetId)
    {
        //this is to use session opened in HTTP request - otherwise I got strange internal state errors from the CDO
        impactAnalyser = new ImpactAnalyser(new ThreadLocalCDOSessionProvider());
        
        generateImpactsList(sourceArtefactCDOIds, impactingEventKind);
        
        if (artefactsRelationImpacts.size() == 0) {
            return;
        }
        
        //this is to skip situation when there is only artefact->baseArtefact impact - so when artefact is simply "added"/mapped to single, standalone baseArtefact
        if (artefactsRelationImpacts.size() == 1) {
            IArtefactRelationImpact artefactRelationImpact = artefactsRelationImpacts.get(0);
            if (ArtefactRelationType.EVIDENCE_TO_BASELINE_VIA_MAP_RELATION.equals(artefactRelationImpact.getArtefactRelationType())) {
                return;
            }
        }
        
        showConfirmationWindow(listeners, baseAssetId);
        
        impactAnalyser.close();
    }
    
    
    public void performImpactAnalysis(Long sourceArtefactCdoId, EventKind impactingEventKind, List<UnassignOrAssignArtefactListener> listeners,
    		Long baseAssetId)
    {
        List<Long> sourceArtefactCDOIds = new ArrayList<Long>();
        sourceArtefactCDOIds.add(sourceArtefactCdoId);
        
        performImpactAnalysis(sourceArtefactCDOIds, impactingEventKind, listeners, baseAssetId);
    }
    

    private void showConfirmationWindow(List<UnassignOrAssignArtefactListener> listeners, Long baseAssetId)
    {
        String confirmQuestion = impactAnalyser.visualizeArtefactsRelationImpacts(artefactsRelationImpacts,
                "&#160;", "<br/><br/>") + "<br/><br/>" + "Should impacts listed above be executed?"; 
        
        ConfirmationWindow confirmationWindow = new ConfirmationWindow("Impact analysis", confirmQuestion,
                "NOTE: Selecting \"No\" will save your changes anyway but impact analysis will not be executed", "Yes", "No");
        
        confirmationWindow.setContentStyleName("impactAnalyserWindowContent");
        confirmationWindow.setWidth("420px");
        
        confirmationWindow.addYesAction((e) -> {
            impactAnalyser.executeArtefactsRelationImpacts(artefactsRelationImpacts);
            confirmationWindow.close();
            for (UnassignOrAssignArtefactListener unassignOrAssignArtefactListener : listeners) {
            	unassignOrAssignArtefactListener.artefactAssignedOrUnassigned(baseAssetId);
           	}
        });
        
        if (!(UI.getCurrent().getWindows().contains(confirmationWindow))) {
            UI.getCurrent().addWindow(confirmationWindow);  
        }
    }

    
    private void generateImpactsList(List<Long> sourceArtefactCDOIds,
            EventKind impactingEventKind)
    {
        List<ImpactSource> impactSources = new ArrayList<ImpactSource>();
        for (Long cdoID : sourceArtefactCDOIds) {
            impactSources.add(new ImpactSource(cdoID, impactingEventKind));
        }
        
        artefactsRelationImpacts = impactAnalyser.listArtefactsRelationImpacts(impactSources);
    }
}
