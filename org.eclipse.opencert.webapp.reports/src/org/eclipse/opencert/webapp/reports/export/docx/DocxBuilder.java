/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.export.docx;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.docProps.core.CoreProperties;
import org.docx4j.docProps.core.dc.elements.SimpleLiteral;
import org.docx4j.docProps.core.dc.terms.W3CDTF;
import org.docx4j.docProps.extended.Properties;
import org.docx4j.jaxb.XPathBinderAssociationIsPartialException;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io.SaveToZipFile;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.DocPropsExtendedPart;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.WordprocessingML.FooterPart;
import org.docx4j.openpackaging.parts.WordprocessingML.HeaderPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.relationships.Relationships;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.Br;
import org.docx4j.wml.Color;
import org.docx4j.wml.Drawing;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.PPrBase.Ind;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.STBrType;
import org.docx4j.wml.Text;
import org.docx4j.wml.U;
import org.docx4j.wml.UnderlineEnumeration;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.manager.BaselineManager;
import org.eclipse.opencert.webapp.reports.manager.RefframeworkManager;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;

public class DocxBuilder 
{
    private final WordprocessingMLPackage _wordMLPackage;
	private final ObjectFactory _objectFactory = new ObjectFactory();
	
	private static final String _TEMPLATE_VAR_PATTERN = "'\\$\\{'{0}'\\}'";
	private final static String _XPATH_FOR_TEXT_NODES = "//w:t";
	
	public enum FontStyle {
		ITALIC, BOLD, UNDERLINED
	}
	
	public enum FontSize {
		/**
	     * Change the font size of the given run properties to the given value.
	     *
	     * @param runProperties
	     * @param fontSize  Twice the size needed, as it is specified as half-point value
	     */
		
		SMALL(18), MEDIUM(24), BIG(26);
		
		private final int size;
		
		private FontSize(int size){
			this.size=size;
		}
		
		public int getSize(){
			return size;
		}
		
	}
	
	public enum FontColor 
	{
	    BLUE("0070C0"), RED("C0504D"), GREEN("64A640");
	    
	    private final String _rgb;
	    
	    private FontColor(String rgb)
	    {
	        _rgb = rgb;
	    }
	    
	    public String getRGB()
	    {
	        return _rgb;
	    }
	}
	
	public enum HeaderStyle 
	{
	    HEADER_1("Heading1"), HEADER_2("Heading2"), HEADER_3("Heading3"), HEADER_4("Heading4"), HEADER_5("Heading5");
		
		private final String _styleID;
		
		private HeaderStyle(String styleID)
		{
		    _styleID = styleID;
		}
		
		public String getStyleID()
		{
		    return _styleID;
		}
	}
	
	public DocxBuilder(File templateDocxFile, long baselineFrameworkID, long projectID, boolean supportsProject) throws Docx4JException
	{        
        if (templateDocxFile == null || !templateDocxFile.exists() || templateDocxFile.isDirectory()) 
        {
            OpencertLogger.error(String.format("Docx template file does not exist: [%s]. Generating from blank docx file...", templateDocxFile));
        	_wordMLPackage = WordprocessingMLPackage.createPackage();
        } 
        else 
        {
        	_wordMLPackage = WordprocessingMLPackage.load(templateDocxFile);
        }

        appendDocxProperties(_wordMLPackage);
        replaceTemplateConsts(_wordMLPackage, baselineFrameworkID, projectID, supportsProject);
	}
	
    public ByteArrayInputStream getResult() throws Docx4JException
	{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
        SaveToZipFile saver = new SaveToZipFile(_wordMLPackage);
        saver.save(os);
        ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());               
        
        return is;
	}
	
	public void appendHeaderNL(String txt, HeaderStyle headerStyle) 
	{
		_wordMLPackage.getMainDocumentPart().addStyledParagraphOfText(headerStyle.getStyleID(), txt);
	}
	
    public P startParagraph()
    {
        final P p = new P();
        applyPPr(p);
        return p;
    }

    public void finishParagraph(P p)
	{
	    _wordMLPackage.getMainDocumentPart().getContent().add(p);
	}

	public void appendText(P p, String txt, EnumSet<FontStyle> fontStyles, FontColor fontColor, FontSize fontSize) 
	{
	    final R r = createTextRun(txt, fontStyles, fontColor, fontSize);
	    p.getContent().add(r);
	}
	
	public void appendText(P p, String txt, EnumSet<FontStyle> fontStyles, FontColor fontColor) 
	{
	    final R r = createTextRun(txt, fontStyles, fontColor, null);
	    p.getContent().add(r);
	}

	public void appendText(P p, String txt, FontStyle fontStyle, FontColor fontColor) 
	{
		appendText(p, txt, EnumSet.of(fontStyle), fontColor, null);
	}

	public void appendText(P p, String txt, FontStyle fontStyle) 
    {
        appendText(p, txt, fontStyle, null);
    }
	
	public void appendText(P p, String txt) 
	{
	    appendText(p, txt, EnumSet.noneOf(DocxBuilder.FontStyle.class), null, null);
	}

	public void appendNL(P p) 
	{
	    Br br = _objectFactory.createBr();
	    p.getContent().add(br);
	}
	
	public void pageBreak(P p){
		Br br = _objectFactory.createBr();
		br.setType(STBrType.PAGE);
		p.getContent().add(br);
	}
	
	public P startParagraphAndAppendImage(String fileNameHint, String alternativeText, byte[] bytes){
		
		P p = new P();
	    applyPPr(p);
	        
		BinaryPartAbstractImage imagePart = null;
		
		try {
			imagePart =  BinaryPartAbstractImage.createImagePart(_wordMLPackage, bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int docPrId = 1;
        int cNvPrId = 2;
        Inline inline = null;
        if(imagePart != null) {
        	try {
        		
        		/**
        		* Create a <wp:inline> element suitable for this image,
        		* which can be linked or embedded in w:p/w:r/w:drawing.
        		* If the image is wider than the page, it will be scaled
        		* automatically. See Javadoc for other signatures.
        		* @param filenameHint Any text, for example the original filename
        		* @param altText Like HTML's alt text
        		* @param id1 An id unique in the document
        		* @param id2 Another id unique in the document
        		* @param link true if this is to be linked not embedded */
        		
				inline = imagePart.createImageInline(fileNameHint, alternativeText, docPrId, cNvPrId, false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        if (inline != null){
        	p = addInlineImageToParagraph(inline);
        }
		return p;
	}
	
	private P addInlineImageToParagraph(Inline inline) {
		// Now add the in-line image to a paragraph
        P paragraph = _objectFactory.createP();
        R run = _objectFactory.createR();
        paragraph.getContent().add(run);
        Drawing drawing = _objectFactory.createDrawing();
        run.getContent().add(drawing);
        drawing.getAnchorOrInline().add(inline);
        return paragraph;
	}
	
	
	
	private void appendDocxProperties(WordprocessingMLPackage wordMLPackage) 
	{
	    DocPropsCorePart docPropsCorePart = wordMLPackage.getDocPropsCorePart();
        DocPropsExtendedPart docPropsExtendedPart = wordMLPackage.getDocPropsExtendedPart();

        CoreProperties coreProps = (CoreProperties)docPropsCorePart.getJaxbElement();
        Properties extProps = (Properties)docPropsExtendedPart.getJaxbElement();

        SimpleLiteral literal = new SimpleLiteral();
        literal.getContent().add("OPENCERT Platform");
        coreProps.setCreator(literal);
        coreProps.setLastModifiedBy("OPENCERT Platform");
        extProps.setCompany("OPENCERT Project - www.opencert-project.eu");
        
        GregorianCalendar calendar = new GregorianCalendar();
        try {
            DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
            XMLGregorianCalendar xmlCalendar = datatypeFactory.newXMLGregorianCalendar(calendar);

            literal = new W3CDTF();
            literal.getContent().add(xmlCalendar.toXMLFormat());
            coreProps.setCreated(literal);
            coreProps.setModified(literal);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
	}
	
	private void replaceTemplateConsts(WordprocessingMLPackage wordMLPackage, long baselineFrameworkID, long projectID, boolean supportsProject)
    {
	    MainDocumentPart mainDocumentPart = wordMLPackage.getMainDocumentPart();
        RelationshipsPart relationshipsPart = mainDocumentPart.getRelationshipsPart();
        Relationships relationships = relationshipsPart.getRelationships();
        List<? extends Relationship> relationshipsList = relationships.getRelationship();
        try {
            List<Object> allTextObjects = new ArrayList<Object>();
            // collect texts from header and footer
            for (Relationship relationship : relationshipsList) 
            {
                Part part = relationshipsPart.getPart(relationship);
                if (part instanceof HeaderPart) {
                    allTextObjects.addAll(((HeaderPart)part).getJAXBNodesViaXPath(_XPATH_FOR_TEXT_NODES, true));
                }
                if (part instanceof FooterPart) {
                    allTextObjects.addAll(((FooterPart)part).getJAXBNodesViaXPath(_XPATH_FOR_TEXT_NODES, true));
                }
            }
            // add texts from body
            allTextObjects.addAll(mainDocumentPart.getJAXBNodesViaXPath(_XPATH_FOR_TEXT_NODES, true));

            final String dateValue = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new GregorianCalendar().getTime());
            replaceVarsWithValue(allTextObjects, "creation.date", dateValue);
            
            if(supportsProject){
	            final String projectName = getProjectName(projectID);
	            replaceVarsWithValue(allTextObjects, "project.name", projectName);
	            
	            final String baselineName = getBaselineName(baselineFrameworkID);
	            if(baselineName == null || baselineName.equals("")) replaceVarsWithValue(allTextObjects, "project.baseline", "No Baseline Framework Selected");
	            else replaceVarsWithValue(allTextObjects, "project.baseline", baselineName);
            }
            else{
            	final String fromName = getRefFrameworkName(baselineFrameworkID);
	            replaceVarsWithValue(allTextObjects, "from.refframework", fromName);
	            
	            final String toName = getRefFrameworkName(projectID);
	            replaceVarsWithValue(allTextObjects, "to.refframework", toName);
            }
            
        } catch (JAXBException | XPathBinderAssociationIsPartialException e) {
            e.printStackTrace();
        }
    }

    private String getRefFrameworkName(long frameworkID) {

    	RefframeworkManager refframeworkManager = (RefframeworkManager)SpringContextHelper.getBeanFromWebappContext(RefframeworkManager.SPRING_NAME);

    	return refframeworkManager.getRefframework(frameworkID).getName();
    	
	}

	private String getProjectName(long projectID)
    {
        AssuranceProjectManager projectManager = (AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        
        final AssuranceProject project = projectManager.getProject(projectID);
        if (project == null) {
            throw new IllegalStateException("Null project for projectID: " + projectID);
        }
        final String result = project.getName();
        return result;
    }

    private String getBaselineName(long baselineFrameworkID)
    {
        BaselineManager baselineManager = (BaselineManager)SpringContextHelper.getBeanFromWebappContext(BaselineManager.SPRING_NAME);
        
        final BaseFramework baseFramework = baselineManager.getBaseFramework(baselineFrameworkID);
        if (baseFramework == null) {
            throw new IllegalStateException("Null baseFramework for baselineFrameworkID: " + baselineFrameworkID);
        }
        final String result = baseFramework.getName(); 
        return result;
    }

    private void replaceVarsWithValue(List<Object> allTextObjects, String varN, String varValue)
    {
        final String varName = MessageFormat.format(_TEMPLATE_VAR_PATTERN, varN);
        
        for (Object obj : allTextObjects) 
        {
            Text text = (Text)((JAXBElement)obj).getValue();
            String newValue = text.getValue().replaceAll(varName, varValue);
            text.setValue(newValue);
        }
    }
	   
	private R createTextRun(String text, EnumSet<FontStyle> fontStyles, FontColor fontColor, FontSize fontSize)
    {
        Text t = _objectFactory.createText();
        t.setSpace("preserve"); //$NON-NLS-1$
        t.setValue(text);

        R r = _objectFactory.createR();
        applyRPr(r, fontStyles, fontColor, fontSize);
        r.getContent().add(t);

        return r;
    }
	
	private void applyRPr(R r, EnumSet<FontStyle> fontStyles, FontColor fontColor, FontSize fontSize)
    {
        RPr rPr = getRPr(r);
        BooleanDefaultTrue bTrue = new BooleanDefaultTrue();
        if (fontStyles.contains(FontStyle.BOLD)) {
        	rPr.setB(bTrue);
        }
        if (fontStyles.contains(FontStyle.ITALIC)) {
        	rPr.setI(bTrue);
        }
        if (fontStyles.contains(FontStyle.UNDERLINED)) {
        	U underline = new U();
            underline.setVal(UnderlineEnumeration.SINGLE);
            rPr.setU(underline);
        }
        
        if (fontColor != null)
        {
            final Color col = _objectFactory.createColor();
            col.setVal(fontColor.getRGB());
            rPr.setColor(col);
        }
        
        if (fontSize != null){
        	HpsMeasure size = new HpsMeasure();
            size.setVal(BigInteger.valueOf(fontSize.getSize()));
            rPr.setSz(size);
        }
    }
	
	private static RPr getRPr(R r)
    {
        if (r.getRPr() == null) {
            r.setRPr(new RPr());
        }
        return r.getRPr();
    }
	
	private static PPr getPPr(P p)
    {
        if (p.getPPr() == null) {
            p.setPPr(new PPr());
        }
        return p.getPPr();
    }
	
	private void applyPPr(P p)
    {
	    final PPr ppr = getPPr(p);
	    Ind ind = new Ind();
	    ind.setLeft(BigInteger.valueOf(200l));
	    ppr.setInd(ind);
	    p.setPPr(ppr);
    }
}
