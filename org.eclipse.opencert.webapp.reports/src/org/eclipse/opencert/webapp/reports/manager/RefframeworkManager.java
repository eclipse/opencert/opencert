/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.dao.RefframeworkDAO;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RefframeworkManager {
public static final String SPRING_NAME = "refframeworkManager";
    
    @Autowired
    private RefframeworkDAO refframweorkDAO;
    
	public List<RefFramework> getAllRefframeworks() 
	{   
		List<RefFramework> allRefframworks = refframweorkDAO.getAllRefframeworks();
	    return allRefframworks;
	}
	
	
	public RefFramework getRefframework(long refframeworkID) 
	{   
		RefFramework refframwork = refframweorkDAO.getRefFramework(refframeworkID);
	    return refframwork;
	}
	
	
	public List<RefArtefact> getRefArtefacts(long refframeworkID)
    {
        RefFramework refFramework = refframweorkDAO.getRefFramework(refframeworkID);

        List<RefArtefact> refArtefacts = new ArrayList<RefArtefact>();
        if (refFramework != null) {
            for (RefArtefact refArtefact : refFramework.getOwnedArtefact()) 
            {        
            	refArtefacts.add(refArtefact);
            }
        }

        return refArtefacts;
    }

    public List<RefActivity> getRefActivities(long refframeworkID)
    {
    	RefFramework refFramework = refframweorkDAO.getRefFramework(refframeworkID);

        List<RefActivity> refActivities = new ArrayList<RefActivity>();
        if (refFramework != null) {
            for (RefActivity refActivity : refFramework.getOwnedActivities()) {
               
                	refActivities.add(refActivity);
               
            }
        }

        return refActivities;
    }



    public List<RefRole> getRefRoles(long refframeworkID)
    {
    	RefFramework refFramework = refframweorkDAO.getRefFramework(refframeworkID);

        List<RefRole> refRoles = new ArrayList<RefRole>();
        if (refFramework != null) {
            for (RefRole refRole : refFramework.getOwnedRole()) {           
            	refRoles.add(refRole);
               
            }
        }

        return refRoles;
    }

    public List<RefTechnique> getRefTechniques(long refframeworkID)
    {
    	RefFramework refFramework = refframweorkDAO.getRefFramework(refframeworkID);

        List<RefTechnique> refTechniques = new ArrayList<RefTechnique>();
        if (refFramework != null) {
            for (RefTechnique refTechnique : refFramework.getOwnedTechnique()) {
               
            	refTechniques.add(refTechnique);
               
            }
        }

        return refTechniques;
    }

    public List<RefRequirement> getRefRequirements(long refframeworkID)
    {
    	RefFramework refFramework = refframweorkDAO.getRefFramework(refframeworkID);

        List<RefRequirement> refRequirements = new ArrayList<RefRequirement>();
        if (refFramework != null) {
            for (RefRequirement refRequirement : refFramework.getOwnedRequirement()) {
              
            	refRequirements.add(refRequirement);
               

            }
        }

        return refRequirements;
    }
    
    public List<RefRequirement> getRefRequirements(long refframeworkID, boolean deeperSearch) {
		
		//deeperSearch: get all ownedRequirements + subRequirements + requirements from Activities
		if (deeperSearch){
			RefFramework refFramework = refframweorkDAO.getRefFramework(refframeworkID);
	
	        List<RefRequirement> refRequirements = new ArrayList<RefRequirement>();
	        if (refFramework != null) {
	        	EObject erefFramework= (EObject) refFramework; 
	        	 for (Iterator<EObject> iterator = erefFramework.eAllContents(); iterator.hasNext();) {
	        		 EObject aEObject = iterator.next();
	        		 if (aEObject instanceof RefRequirement){
	        			 RefRequirement refRequirement = (RefRequirement) aEObject;
	 	                 refRequirements.add(refRequirement);
	        		 }
	                
	            }
	        }
	
	        return refRequirements;
		}
		else return getRefRequirements(refframeworkID);
	}
    
    public List<RefActivity> getRefActivities(long refframeworkID, boolean deeperSearch) {
		
    	//deeperSearch: get all ownedActivities + subActivities 
		if (deeperSearch){
			RefFramework refFramework = refframweorkDAO.getRefFramework(refframeworkID);
	
	        List<RefActivity> refActivities = new ArrayList<RefActivity>();
	        if (refFramework != null) {
	        	EObject erefFramework= (EObject) refFramework; 
	        	 for (Iterator<EObject> iterator = erefFramework.eAllContents(); iterator.hasNext();) {
	        		 EObject aEObject = iterator.next();
	        		 if (aEObject instanceof RefActivity){
	        			 RefActivity refActivity = (RefActivity) aEObject;
	 	                 refActivities.add(refActivity);
	        		 }
	                
	            }
	        }
	
	        return refActivities;
		}
		else return getRefActivities(refframeworkID);
	}
    
	
}
