/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceNumber;
import org.eclipse.opencert.webapp.reports.containers.ComplianceNumber;
import org.eclipse.opencert.webapp.reports.view.IComplianceNumberButtonCreator;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public class ComplianceNumberDataRowProcessor
        implements IDataRowProcessor<BaseAssetComplianceNumber>
{
    private IComplianceNumberButtonCreator _complianceNumberButtonCreator;

    public ComplianceNumberDataRowProcessor(IComplianceNumberButtonCreator complianceNumberButtonCreator)
    {
        _complianceNumberButtonCreator = complianceNumberButtonCreator;
    }

    @Override
    public BaseAssetComplianceNumber createBaseAssetDataRow(long itemId, BaseAssurableElement baseAssetOrActivity, BaseAssetComplianceNumber parentDataRow, Image img)
    {
        final Label nameLabel = createComplianceAssetNameLabel(baseAssetOrActivity);
        BaseAssetComplianceNumber bAssetComplianceNumber = new BaseAssetComplianceNumber(itemId, img, nameLabel, parentDataRow);
        
        ComplianceNumber complianceNumber = countCompliantElements(baseAssetOrActivity);
        Button fullyCompliantNumberButton = _complianceNumberButtonCreator.createComplianceNumberButton(itemId, complianceNumber.getFullyCompliant(), bAssetComplianceNumber, true);
        Button partiallyCompliantNumberButton = _complianceNumberButtonCreator.createComplianceNumberButton(itemId, complianceNumber.getPartiallyCompliant(), bAssetComplianceNumber, false);

        bAssetComplianceNumber.setFullyCompliant(fullyCompliantNumberButton);
        bAssetComplianceNumber.setPartiallyCompliant(partiallyCompliantNumberButton);
        
        return bAssetComplianceNumber;
    }

    @SuppressWarnings("unchecked")
	@Override
    public void setItemPropsFromDataRow(Item item, BaseAssetComplianceNumber baseAssetComplianceNumber)
    {
        item.getItemProperty(IComplianceConsts.TYPE_PROPERTY_ID).setValue(baseAssetComplianceNumber.getType());
        item.getItemProperty(IComplianceConsts.NAME_PROPERTY_ID).setValue(baseAssetComplianceNumber.getName());
        item.getItemProperty(IComplianceConsts.FULLY_COMPLIANT_PROPERTY_ID).setValue(baseAssetComplianceNumber.getFullyCompliant());
        item.getItemProperty(IComplianceConsts.PARTIALLY_COMPLIANT_PROPERTY_ID).setValue(baseAssetComplianceNumber.getPartiallyCompliant());
    }

    @Override
    public void generateParentChildRelationsInContainer(HierarchicalContainer hierarchicalContainer)
    {
        for (Object baseAssetCompliance : hierarchicalContainer.getItemIds()) {
            if (baseAssetCompliance instanceof BaseAssetComplianceNumber) {
                if (((BaseAssetComplianceNumber) baseAssetCompliance).getParent() != null) {
                    hierarchicalContainer.setParent((BaseAssetComplianceNumber) baseAssetCompliance,
                            ((BaseAssetComplianceNumber) baseAssetCompliance).getParent());
                }
            }
        }
    }

    @Override
    public void initEmptyTableDataContainer(HierarchicalContainer hc)
    {
        hc.addContainerProperty(IComplianceConsts.TYPE_PROPERTY_ID, Image.class, null);
        hc.addContainerProperty(IComplianceConsts.NAME_PROPERTY_ID, Label.class, "");
        hc.addContainerProperty(IComplianceConsts.FULLY_COMPLIANT_PROPERTY_ID, Button.class, null);
        hc.addContainerProperty(IComplianceConsts.PARTIALLY_COMPLIANT_PROPERTY_ID, Button.class, null);
    }

    private ComplianceNumber countCompliantElements(BaseAssurableElement baseElement)
    {
        if (baseElement instanceof BaseArtefact) {
            return countComplianceNumberForBaselineElement(baseElement, ArtefactImpl.class);
        } else if (baseElement instanceof BaseActivity) {
            return countComplianceNumberForBaselineElement(baseElement, ActivityImpl.class);
        } else if (baseElement instanceof BaseRequirement) {
        	return countComplianceNumberForBaselineElement(baseElement, ArtefactImpl.class, ActivityImpl.class);
        }

        return new ComplianceNumber();
    }

    private ComplianceNumber countComplianceNumberForBaselineElement(BaseAssurableElement baseElement, Class<? extends EObject> clazz)
    {
    	return countComplianceNumberForBaselineElement(baseElement, clazz, null);
    }
    
    private ComplianceNumber countComplianceNumberForBaselineElement(BaseAssurableElement baseElement, Class<? extends EObject> clazz1, Class<? extends EObject> clazz2)
    {
        ComplianceNumber complianceNumber = new ComplianceNumber();

        for (BaseComplianceMap baseComplianceMap : baseElement.getComplianceMap()) {
            if (baseComplianceMap.getType().equals(MapKind.FULL)) {
                for (AssuranceAsset asset : baseComplianceMap.getTarget()) {
                    if ((clazz1 != null && asset.getClass() == clazz1) || (clazz2 != null && asset.getClass() == clazz2)) {
                        complianceNumber.setFullyCompliant(complianceNumber.getFullyCompliant() + 1);
                    }
                }
            } else if (baseComplianceMap.getType().equals(MapKind.PARTIAL)) {
                for (AssuranceAsset asset : baseComplianceMap.getTarget()) {
                    if ((clazz1 != null && asset.getClass() == clazz1) || (clazz2 != null && asset.getClass() == clazz2)) {
                        complianceNumber.setPartiallyCompliant(complianceNumber.getPartiallyCompliant() + 1);
                    }
                }
            }
        }

        return complianceNumber;
    }

    @Override
    public boolean supportsActivities()
    {
        return true;
    }
}
