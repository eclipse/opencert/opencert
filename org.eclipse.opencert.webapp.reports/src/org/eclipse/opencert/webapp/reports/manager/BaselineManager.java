/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.dao.BaselineDAO;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaselineManager
{
    public static final String SPRING_NAME = "baselineManager";

    @Autowired
    private BaselineDAO baseElementDAO;

    public List<BaseArtefact> getBaseArtefacts(long baseFrameworkUid, boolean selected)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baseFrameworkUid);

        List<BaseArtefact> baseArtefacts = new ArrayList<BaseArtefact>();
        if (baseFramework != null) {
            for (BaseArtefact baseArtefact : baseFramework.getOwnedArtefact()) 
            {
                if (selected && baseArtefact.isIsSelected()) 
                {
                    baseArtefacts.add(baseArtefact);
                } 
                else if (!selected && !baseArtefact.isIsSelected()) 
                {
                    baseArtefacts.add(baseArtefact);
                }
            }
        }

        return baseArtefacts;
    }

    public List<BaseActivity> getBaseActivities(long baseFrameworkUid, boolean selected)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baseFrameworkUid);

        List<BaseActivity> baseActivities = new ArrayList<BaseActivity>();
        if (baseFramework != null) {
            for (BaseActivity baseActivity : baseFramework.getOwnedActivities()) {
                if (selected && baseActivity.isIsSelected()) {
                    baseActivities.add(baseActivity);
                } else if (!selected && !baseActivity.isIsSelected()) {
                    baseActivities.add(baseActivity);
                }
            }
        }

        return baseActivities;
    }

    public BaseFramework getBaseFramework(long baselineFrameworkID)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baselineFrameworkID);

        return baseFramework;
    }

    public List<BaseRole> getBaseRoles(long baseFrameworkUid, boolean selected)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baseFrameworkUid);

        List<BaseRole> baseRoles = new ArrayList<BaseRole>();
        if (baseFramework != null) {
            for (BaseRole baseRole : baseFramework.getOwnedRole()) {
                if (selected && baseRole.isIsSelected()) {
                    baseRoles.add(baseRole);
                } else if (!selected && !baseRole.isIsSelected()) {
                    baseRoles.add(baseRole);
                }
            }
        }

        return baseRoles;
    }

    public List<BaseTechnique> getBaseTechniques(long baseFrameworkUid, boolean selected)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baseFrameworkUid);

        List<BaseTechnique> baseTechniques = new ArrayList<BaseTechnique>();
        if (baseFramework != null) {
            for (BaseTechnique baseTechnique : baseFramework.getOwnedTechnique()) {
                if (selected && baseTechnique.isIsSelected()) {
                    baseTechniques.add(baseTechnique);
                } else if (!selected && !baseTechnique.isIsSelected()) {
                    baseTechniques.add(baseTechnique);
                }
            }
        }

        return baseTechniques;
    }

    public List<BaseRequirement> getBaseRequirements(long baseFrameworkUid, boolean selected)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baseFrameworkUid);

        List<BaseRequirement> baseRequirements = new ArrayList<BaseRequirement>();
        if (baseFramework != null) {
            for (BaseRequirement baseRequirement : baseFramework.getOwnedRequirement()) {
                if (selected && baseRequirement.isIsSelected()) {
                    baseRequirements.add(baseRequirement);
                } else if (!selected && !baseRequirement.isIsSelected()) {
                    baseRequirements.add(baseRequirement);
                }

            }
        }

        return baseRequirements;
    }

    public List<BaseApplicabilityLevel> getBaseApplicabilityLevel(long baseFrameworkUid)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baseFrameworkUid);

        List<BaseApplicabilityLevel> baseApplicabilityLevels = new ArrayList<BaseApplicabilityLevel>();
        if (baseFramework != null) {
            for (BaseApplicabilityLevel baseApplicabilityLevel : baseFramework.getOwnedApplicLevel()) {
                baseApplicabilityLevels.add(baseApplicabilityLevel);
            }
        }

        return baseApplicabilityLevels;
    }

    public List<BaseCriticalityLevel> getBaseCriticalityLevel(long baseFrameworkUid)
    {
        BaseFramework baseFramework = baseElementDAO.getBaseFramework(baseFrameworkUid);

        List<BaseCriticalityLevel> baseCriticalityLevels = new ArrayList<BaseCriticalityLevel>();
        if (baseFramework != null) {
            for (BaseCriticalityLevel baseCriticalityLevel : baseFramework.getOwnedCriticLevel()) {
                baseCriticalityLevels.add(baseCriticalityLevel);
            }
        }

        return baseCriticalityLevels;
    }
    
    public void updateBaselineFrameworkName(Long baseFrameworkUid, String baselineFrameworkName, AssuranceProject assuranceProject)
    {
    	baseElementDAO.updateBaselineFrameworkName(baseFrameworkUid, baselineFrameworkName, assuranceProject);
    }

	
	public List<BaseRequirement> getBaseRequirements(long baselineFrameworkID, boolean selected, boolean deeperSearch) {
		
		//deeperSearch: get all ownedRequirements + subRequirements + requirements from Activities
		if (deeperSearch){
			BaseFramework baseFramework = baseElementDAO.getBaseFramework(baselineFrameworkID);
	
	        List<BaseRequirement> baseRequirements = new ArrayList<BaseRequirement>();
	        if (baseFramework != null) {
	        	EObject eBaseFramework= (EObject) baseFramework; 
	        	 for (Iterator<EObject> iterator = eBaseFramework.eAllContents(); iterator.hasNext();) {
	        		 EObject aEObject = iterator.next();
	        		 if (aEObject instanceof BaseRequirement){
	        			 BaseRequirement baseRequirement = (BaseRequirement) aEObject;
	        			 if (selected && baseRequirement.isIsSelected()) {
	 	                    baseRequirements.add(baseRequirement);
	 	                } else if (!selected && !baseRequirement.isIsSelected()) {
	 	                    baseRequirements.add(baseRequirement);
	 	                }
	        		 }
	                
	            }
	        }
	
	        return baseRequirements;
		}
		else return getBaseRequirements(baselineFrameworkID, selected);
	}
	
	public List<BaseActivity> getBaseActivities(long baselineFrameworkID, boolean selected, boolean deeperSearch) {
		
		//deeperSearch: get all ownedActivities + subActivities 
		if (deeperSearch){
			BaseFramework baseFramework = baseElementDAO.getBaseFramework(baselineFrameworkID);
	
	        List<BaseActivity> baseActivities = new ArrayList<BaseActivity>();
	        if (baseFramework != null) {
	        	EObject eBaseFramework= (EObject) baseFramework; 
	        	 for (Iterator<EObject> iterator = eBaseFramework.eAllContents(); iterator.hasNext();) {
	        		 EObject aEObject = iterator.next();
	        		 if (aEObject instanceof BaseActivity){
	        			 BaseActivity baseActivity = (BaseActivity) aEObject;
	        			 if (selected && baseActivity.isIsSelected()) {
	 	                    baseActivities.add(baseActivity);
	 	                } else if (!selected && !baseActivity.isIsSelected()) {
	 	                    baseActivities.add(baseActivity);
	 	                }
	        		 }
	                
	            }
	        }
	
	        return baseActivities;
		}
		else return getBaseActivities(baselineFrameworkID, selected);
	}
}
