package org.eclipse.opencert.evm.oslc.km.preferences.preferences;
/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	
	public static final String OSLCKM_WS_URL = "OSLCKM_WS_URL";
	
}
