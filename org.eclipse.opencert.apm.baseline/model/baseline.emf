@gmf
@namespace(uri="http://baseline/1.0", prefix="baseline")
package baseline;

import "platform:/resource/org.eclipse.opencert.infra.mappings/model/Mapping.ecore";
import "platform:/resource/org.eclipse.opencert.infra.properties/model/property.ecore";
import "platform:/resource/org.eclipse.opencert.apm.assuranceassets/model/assuranceasset.ecore";
import "platform:/resource/org.eclipse.opencert.pkm.refframework/model/refframework.ecore";
import "platform:/resource/org.eclipse.opencert.infra.general/model/general.ecore";

@gmf.diagram
class BaseFramework extends general.DescribableElement {
  attr String scope;
  attr String rev;
  attr String purpose;
  attr String publisher;
  attr EDate issued;
  val BaseActivity[*] ownedActivities;
  val BaseArtefact[*] ownedArtefact;
  val BaseRequirement[*] ownedRequirement;
  val BaseApplicabilityLevel[*] ownedApplicLevel;
  val BaseCriticalityLevel[*] ownedCriticLevel;
  val BaseRole[*] ownedRole;
  val BaseTechnique[*] ownedTechnique;
}

class BaseRequirement extends BaseAssurableElement, BaselineElement, general.DescribableElement {
  attr String reference;
  attr String assumptions;
  attr String rationale;
  attr String image;

  @ExtendedMetaData
  transient attr String annotations;
  val BaseRequirementRel[*] ownedRel;
  val BaseApplicability[*] applicability;
  val BaseRequirement[*] subRequirement;
}

@gmf.node(label="name", figure="figures.ArtefactFigure", label.icon="false", label.placement="external")
class BaseArtefact extends BaseAssurableElement, BaselineElement, general.DescribableElement {
  attr String reference;
  ref BaseRequirement[*] constrainingRequirement;
  ref BaseTechnique[*] applicableTechnique;
  val BaseArtefactRel[*] ownedRel;
  ref property.Property[*] property;
}

@gmf.node(label="name", border.color="0,0,0", border.width="2")
class BaseActivity extends BaseAssurableElement, BaselineElement, general.DescribableElement {
  attr String objective;
  attr String scope;

  @gmf.link(color="255,0,0", source.decoration="closedarrow", style="dash", tool.small.bundle="org.eclipse.opencert.pkm.refframework", tool.small.path="icons/Require.gif")
  ref BaseArtefact[*] requiredArtefact;

  @gmf.link(color="0,255,0", target.decoration="filledclosedarrow", style="solid", tool.small.bundle="org.eclipse.opencert.pkm.refframework", tool.small.path="icons/Produce.gif")
  ref BaseArtefact[*] producedArtefact;

  @gmf.compartment
  val BaseActivity[*] subActivity;

  @gmf.link(source.decoration="arrow", tool.small.bundle="org.eclipse.opencert.pkm.refframework", tool.small.path="icons/Precedence.gif")
  ref BaseActivity[*] precedingActivity;
  val BaseRequirement[*] ownedRequirement;

  @gmf.link(color="0,0,255", source.decoration="closedarrow", style="dash", tool.small.bundle="org.eclipse.opencert.pkm.refframework", tool.small.path="icons/Executing.gif")
  ref BaseRole[*] role;
  ref BaseTechnique applicableTechnique;
  val BaseActivityRel[*] ownedRel;
  val BaseApplicability[*] applicability;
}

class BaseRequirementRel {
  ref BaseRequirement[1] target;
  ref BaseRequirement[1] source;
  attr general.RequirementRelKind type;
}

@gmf.node(label="name", figure="figures.RefRoleFigure", label.icon="false", label.placement="external")
class BaseRole extends BaseAssurableElement, BaselineElement, general.DescribableElement {
}

class BaseApplicabilityLevel extends general.DescribableElement {
}

class BaseCriticalityLevel extends general.DescribableElement {
}

class BaseTechnique extends BaseAssurableElement, general.DescribableElement, BaselineElement {
  ref BaseCriticalityApplicability[*] criticApplic;
  attr String aim;
}

class BaseArtefactRel extends general.DescribableElement {
  attr int maxMultiplicitySource;
  attr int minMultiplicitySource;
  attr int maxMultiplicityTarget;
  attr int minMultiplicityTarget;
  attr general.ChangeEffectKind modificationEffect;
  attr general.ChangeEffectKind revocationEffect;
  ref BaseArtefact[1] source;
  ref BaseArtefact[1] target;
}

class BaseCriticalityApplicability {
  ref BaseApplicabilityLevel[1] applicLevel;
  ref BaseCriticalityLevel[1] criticLevel;
  attr String comment;
}

class BaseActivityRel {
  attr general.ActivityRelKind type;
  ref BaseActivity[1] source;
  ref BaseActivity[1] target;
}

abstract class BaseAssurableElement {
  val BaseEquivalenceMap[*] equivalenceMap;
  val BaseComplianceMap[*] complianceMap;
}

class BaseIndependencyLevel extends BaseApplicabilityLevel {
}

class BaseRecommendationLevel extends BaseApplicabilityLevel {
}

class BaseControlCategory extends BaseApplicabilityLevel {
}

class BaseApplicability extends general.NamedElement {
  val BaseCriticalityApplicability[*] applicCritic;
  attr String comments;
  ref BaseAssurableElement applicTarget;
  val BaseApplicabilityRel[*] ownedRel;
}

class BaseApplicabilityRel {
  attr general.ApplicabilityKind type;
  ref BaseApplicability[1] source;
  ref BaseApplicability[1] target;
}

abstract class BaselineElement {
  attr boolean isSelected;
  attr String selectionJustification;
}

class BaseEquivalenceMap extends mapping.EquivalenceMap {
  ref refframework.RefAssurableElement[*] target;
}

class BaseComplianceMap extends mapping.ComplianceMap {
  ref assuranceasset.AssuranceAsset[*] target;
}

