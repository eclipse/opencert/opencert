/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.opencert.apm.baseline.baseline.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BaselineFactoryImpl extends EFactoryImpl implements BaselineFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BaselineFactory init() {
		try {
			BaselineFactory theBaselineFactory = (BaselineFactory)EPackage.Registry.INSTANCE.getEFactory(BaselinePackage.eNS_URI);
			if (theBaselineFactory != null) {
				return theBaselineFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BaselineFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaselineFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BaselinePackage.BASE_FRAMEWORK: return (EObject)createBaseFramework();
			case BaselinePackage.BASE_REQUIREMENT: return (EObject)createBaseRequirement();
			case BaselinePackage.BASE_ARTEFACT: return (EObject)createBaseArtefact();
			case BaselinePackage.BASE_ACTIVITY: return (EObject)createBaseActivity();
			case BaselinePackage.BASE_REQUIREMENT_REL: return (EObject)createBaseRequirementRel();
			case BaselinePackage.BASE_ROLE: return (EObject)createBaseRole();
			case BaselinePackage.BASE_APPLICABILITY_LEVEL: return (EObject)createBaseApplicabilityLevel();
			case BaselinePackage.BASE_CRITICALITY_LEVEL: return (EObject)createBaseCriticalityLevel();
			case BaselinePackage.BASE_TECHNIQUE: return (EObject)createBaseTechnique();
			case BaselinePackage.BASE_ARTEFACT_REL: return (EObject)createBaseArtefactRel();
			case BaselinePackage.BASE_CRITICALITY_APPLICABILITY: return (EObject)createBaseCriticalityApplicability();
			case BaselinePackage.BASE_ACTIVITY_REL: return (EObject)createBaseActivityRel();
			case BaselinePackage.BASE_INDEPENDENCY_LEVEL: return (EObject)createBaseIndependencyLevel();
			case BaselinePackage.BASE_RECOMMENDATION_LEVEL: return (EObject)createBaseRecommendationLevel();
			case BaselinePackage.BASE_CONTROL_CATEGORY: return (EObject)createBaseControlCategory();
			case BaselinePackage.BASE_APPLICABILITY: return (EObject)createBaseApplicability();
			case BaselinePackage.BASE_APPLICABILITY_REL: return (EObject)createBaseApplicabilityRel();
			case BaselinePackage.BASE_EQUIVALENCE_MAP: return (EObject)createBaseEquivalenceMap();
			case BaselinePackage.BASE_COMPLIANCE_MAP: return (EObject)createBaseComplianceMap();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseFramework createBaseFramework() {
		BaseFrameworkImpl baseFramework = new BaseFrameworkImpl();
		return baseFramework;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseRequirement createBaseRequirement() {
		BaseRequirementImpl baseRequirement = new BaseRequirementImpl();
		return baseRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseArtefact createBaseArtefact() {
		BaseArtefactImpl baseArtefact = new BaseArtefactImpl();
		return baseArtefact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseActivity createBaseActivity() {
		BaseActivityImpl baseActivity = new BaseActivityImpl();
		return baseActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseRequirementRel createBaseRequirementRel() {
		BaseRequirementRelImpl baseRequirementRel = new BaseRequirementRelImpl();
		return baseRequirementRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseRole createBaseRole() {
		BaseRoleImpl baseRole = new BaseRoleImpl();
		return baseRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseApplicabilityLevel createBaseApplicabilityLevel() {
		BaseApplicabilityLevelImpl baseApplicabilityLevel = new BaseApplicabilityLevelImpl();
		return baseApplicabilityLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseCriticalityLevel createBaseCriticalityLevel() {
		BaseCriticalityLevelImpl baseCriticalityLevel = new BaseCriticalityLevelImpl();
		return baseCriticalityLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseTechnique createBaseTechnique() {
		BaseTechniqueImpl baseTechnique = new BaseTechniqueImpl();
		return baseTechnique;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseArtefactRel createBaseArtefactRel() {
		BaseArtefactRelImpl baseArtefactRel = new BaseArtefactRelImpl();
		return baseArtefactRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseCriticalityApplicability createBaseCriticalityApplicability() {
		BaseCriticalityApplicabilityImpl baseCriticalityApplicability = new BaseCriticalityApplicabilityImpl();
		return baseCriticalityApplicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseActivityRel createBaseActivityRel() {
		BaseActivityRelImpl baseActivityRel = new BaseActivityRelImpl();
		return baseActivityRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseIndependencyLevel createBaseIndependencyLevel() {
		BaseIndependencyLevelImpl baseIndependencyLevel = new BaseIndependencyLevelImpl();
		return baseIndependencyLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseRecommendationLevel createBaseRecommendationLevel() {
		BaseRecommendationLevelImpl baseRecommendationLevel = new BaseRecommendationLevelImpl();
		return baseRecommendationLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseControlCategory createBaseControlCategory() {
		BaseControlCategoryImpl baseControlCategory = new BaseControlCategoryImpl();
		return baseControlCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseApplicability createBaseApplicability() {
		BaseApplicabilityImpl baseApplicability = new BaseApplicabilityImpl();
		return baseApplicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseApplicabilityRel createBaseApplicabilityRel() {
		BaseApplicabilityRelImpl baseApplicabilityRel = new BaseApplicabilityRelImpl();
		return baseApplicabilityRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseEquivalenceMap createBaseEquivalenceMap() {
		BaseEquivalenceMapImpl baseEquivalenceMap = new BaseEquivalenceMapImpl();
		return baseEquivalenceMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseComplianceMap createBaseComplianceMap() {
		BaseComplianceMapImpl baseComplianceMap = new BaseComplianceMapImpl();
		return baseComplianceMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaselinePackage getBaselinePackage() {
		return (BaselinePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BaselinePackage getPackage() {
		return BaselinePackage.eINSTANCE;
	}

} //BaselineFactoryImpl
