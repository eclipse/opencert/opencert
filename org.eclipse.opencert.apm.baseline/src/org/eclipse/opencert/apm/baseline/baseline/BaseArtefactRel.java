/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Artefact Rel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicitySource <em>Max Multiplicity Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicitySource <em>Min Multiplicity Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicityTarget <em>Max Multiplicity Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicityTarget <em>Min Multiplicity Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getModificationEffect <em>Modification Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getRevocationEffect <em>Revocation Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel()
 * @model
 * @generated
 */
public interface BaseArtefactRel extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Max Multiplicity Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Multiplicity Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Multiplicity Source</em>' attribute.
	 * @see #setMaxMultiplicitySource(int)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_MaxMultiplicitySource()
	 * @model
	 * @generated
	 */
	int getMaxMultiplicitySource();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicitySource <em>Max Multiplicity Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Multiplicity Source</em>' attribute.
	 * @see #getMaxMultiplicitySource()
	 * @generated
	 */
	void setMaxMultiplicitySource(int value);

	/**
	 * Returns the value of the '<em><b>Min Multiplicity Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Multiplicity Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Multiplicity Source</em>' attribute.
	 * @see #setMinMultiplicitySource(int)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_MinMultiplicitySource()
	 * @model
	 * @generated
	 */
	int getMinMultiplicitySource();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicitySource <em>Min Multiplicity Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Multiplicity Source</em>' attribute.
	 * @see #getMinMultiplicitySource()
	 * @generated
	 */
	void setMinMultiplicitySource(int value);

	/**
	 * Returns the value of the '<em><b>Max Multiplicity Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Multiplicity Target</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Multiplicity Target</em>' attribute.
	 * @see #setMaxMultiplicityTarget(int)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_MaxMultiplicityTarget()
	 * @model
	 * @generated
	 */
	int getMaxMultiplicityTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicityTarget <em>Max Multiplicity Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Multiplicity Target</em>' attribute.
	 * @see #getMaxMultiplicityTarget()
	 * @generated
	 */
	void setMaxMultiplicityTarget(int value);

	/**
	 * Returns the value of the '<em><b>Min Multiplicity Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Multiplicity Target</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Multiplicity Target</em>' attribute.
	 * @see #setMinMultiplicityTarget(int)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_MinMultiplicityTarget()
	 * @model
	 * @generated
	 */
	int getMinMultiplicityTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicityTarget <em>Min Multiplicity Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Multiplicity Target</em>' attribute.
	 * @see #getMinMultiplicityTarget()
	 * @generated
	 */
	void setMinMultiplicityTarget(int value);

	/**
	 * Returns the value of the '<em><b>Modification Effect</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.infra.general.general.ChangeEffectKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modification Effect</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modification Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #setModificationEffect(ChangeEffectKind)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_ModificationEffect()
	 * @model
	 * @generated
	 */
	ChangeEffectKind getModificationEffect();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getModificationEffect <em>Modification Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Modification Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #getModificationEffect()
	 * @generated
	 */
	void setModificationEffect(ChangeEffectKind value);

	/**
	 * Returns the value of the '<em><b>Revocation Effect</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.infra.general.general.ChangeEffectKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Revocation Effect</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Revocation Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #setRevocationEffect(ChangeEffectKind)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_RevocationEffect()
	 * @model
	 * @generated
	 */
	ChangeEffectKind getRevocationEffect();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getRevocationEffect <em>Revocation Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Revocation Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #getRevocationEffect()
	 * @generated
	 */
	void setRevocationEffect(ChangeEffectKind value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(BaseArtefact)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_Source()
	 * @model required="true"
	 * @generated
	 */
	BaseArtefact getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(BaseArtefact value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(BaseArtefact)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseArtefactRel_Target()
	 * @model required="true"
	 * @generated
	 */
	BaseArtefact getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(BaseArtefact value);

} // BaseArtefactRel
