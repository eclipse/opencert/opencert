/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAssumptions <em>Assumptions</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getRationale <em>Rationale</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getApplicability <em>Applicability</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getSubRequirement <em>Sub Requirement</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement()
 * @model
 * @generated
 */
public interface BaseRequirement extends BaseAssurableElement, BaselineElement, DescribableElement {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' attribute.
	 * @see #setReference(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_Reference()
	 * @model
	 * @generated
	 */
	String getReference();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getReference <em>Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' attribute.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(String value);

	/**
	 * Returns the value of the '<em><b>Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assumptions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assumptions</em>' attribute.
	 * @see #setAssumptions(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_Assumptions()
	 * @model
	 * @generated
	 */
	String getAssumptions();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAssumptions <em>Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assumptions</em>' attribute.
	 * @see #getAssumptions()
	 * @generated
	 */
	void setAssumptions(String value);

	/**
	 * Returns the value of the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rationale</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rationale</em>' attribute.
	 * @see #setRationale(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_Rationale()
	 * @model
	 * @generated
	 */
	String getRationale();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getRationale <em>Rationale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rationale</em>' attribute.
	 * @see #getRationale()
	 * @generated
	 */
	void setRationale(String value);

	/**
	 * Returns the value of the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image</em>' attribute.
	 * @see #setImage(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_Image()
	 * @model
	 * @generated
	 */
	String getImage();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getImage <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image</em>' attribute.
	 * @see #getImage()
	 * @generated
	 */
	void setImage(String value);

	/**
	 * Returns the value of the '<em><b>Annotations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotations</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations</em>' attribute.
	 * @see #setAnnotations(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_Annotations()
	 * @model transient="true"
	 * @generated
	 */
	String getAnnotations();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAnnotations <em>Annotations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotations</em>' attribute.
	 * @see #getAnnotations()
	 * @generated
	 */
	void setAnnotations(String value);

	/**
	 * Returns the value of the '<em><b>Owned Rel</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Rel</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Rel</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_OwnedRel()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseRequirementRel> getOwnedRel();

	/**
	 * Returns the value of the '<em><b>Applicability</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applicability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applicability</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_Applicability()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseApplicability> getApplicability();

	/**
	 * Returns the value of the '<em><b>Sub Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Requirement</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseRequirement_SubRequirement()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseRequirement> getSubRequirement();

} // BaseRequirement
