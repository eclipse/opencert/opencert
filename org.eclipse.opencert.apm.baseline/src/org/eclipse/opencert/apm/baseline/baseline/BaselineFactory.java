/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage
 * @generated
 */
public interface BaselineFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BaselineFactory eINSTANCE = org.eclipse.opencert.apm.baseline.baseline.impl.BaselineFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Base Framework</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Framework</em>'.
	 * @generated
	 */
	BaseFramework createBaseFramework();

	/**
	 * Returns a new object of class '<em>Base Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Requirement</em>'.
	 * @generated
	 */
	BaseRequirement createBaseRequirement();

	/**
	 * Returns a new object of class '<em>Base Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Artefact</em>'.
	 * @generated
	 */
	BaseArtefact createBaseArtefact();

	/**
	 * Returns a new object of class '<em>Base Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Activity</em>'.
	 * @generated
	 */
	BaseActivity createBaseActivity();

	/**
	 * Returns a new object of class '<em>Base Requirement Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Requirement Rel</em>'.
	 * @generated
	 */
	BaseRequirementRel createBaseRequirementRel();

	/**
	 * Returns a new object of class '<em>Base Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Role</em>'.
	 * @generated
	 */
	BaseRole createBaseRole();

	/**
	 * Returns a new object of class '<em>Base Applicability Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Applicability Level</em>'.
	 * @generated
	 */
	BaseApplicabilityLevel createBaseApplicabilityLevel();

	/**
	 * Returns a new object of class '<em>Base Criticality Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Criticality Level</em>'.
	 * @generated
	 */
	BaseCriticalityLevel createBaseCriticalityLevel();

	/**
	 * Returns a new object of class '<em>Base Technique</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Technique</em>'.
	 * @generated
	 */
	BaseTechnique createBaseTechnique();

	/**
	 * Returns a new object of class '<em>Base Artefact Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Artefact Rel</em>'.
	 * @generated
	 */
	BaseArtefactRel createBaseArtefactRel();

	/**
	 * Returns a new object of class '<em>Base Criticality Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Criticality Applicability</em>'.
	 * @generated
	 */
	BaseCriticalityApplicability createBaseCriticalityApplicability();

	/**
	 * Returns a new object of class '<em>Base Activity Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Activity Rel</em>'.
	 * @generated
	 */
	BaseActivityRel createBaseActivityRel();

	/**
	 * Returns a new object of class '<em>Base Independency Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Independency Level</em>'.
	 * @generated
	 */
	BaseIndependencyLevel createBaseIndependencyLevel();

	/**
	 * Returns a new object of class '<em>Base Recommendation Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Recommendation Level</em>'.
	 * @generated
	 */
	BaseRecommendationLevel createBaseRecommendationLevel();

	/**
	 * Returns a new object of class '<em>Base Control Category</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Control Category</em>'.
	 * @generated
	 */
	BaseControlCategory createBaseControlCategory();

	/**
	 * Returns a new object of class '<em>Base Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Applicability</em>'.
	 * @generated
	 */
	BaseApplicability createBaseApplicability();

	/**
	 * Returns a new object of class '<em>Base Applicability Rel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Applicability Rel</em>'.
	 * @generated
	 */
	BaseApplicabilityRel createBaseApplicabilityRel();

	/**
	 * Returns a new object of class '<em>Base Equivalence Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Equivalence Map</em>'.
	 * @generated
	 */
	BaseEquivalenceMap createBaseEquivalenceMap();

	/**
	 * Returns a new object of class '<em>Base Compliance Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Compliance Map</em>'.
	 * @generated
	 */
	BaseComplianceMap createBaseComplianceMap();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	BaselinePackage getBaselinePackage();

} //BaselineFactory
