/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselineFactory
 * @model kind="package"
 * @generated
 */
public interface BaselinePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "baseline";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://baseline/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "baseline";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BaselinePackage eINSTANCE = org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl <em>Base Framework</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseFramework()
	 * @generated
	 */
	int BASE_FRAMEWORK = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__SCOPE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rev</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__REV = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Purpose</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__PURPOSE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__PUBLISHER = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Issued</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__ISSUED = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Owned Activities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__OWNED_ACTIVITIES = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Owned Artefact</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__OWNED_ARTEFACT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Owned Requirement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__OWNED_REQUIREMENT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Owned Applic Level</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__OWNED_APPLIC_LEVEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Owned Critic Level</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__OWNED_CRITIC_LEVEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Owned Role</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__OWNED_ROLE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Owned Technique</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__OWNED_TECHNIQUE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Ref Framework</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK__REF_FRAMEWORK = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Base Framework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The number of operations of the '<em>Base Framework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FRAMEWORK_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseAssurableElementImpl <em>Base Assurable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseAssurableElementImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseAssurableElement()
	 * @generated
	 */
	int BASE_ASSURABLE_ELEMENT = 12;

	/**
	 * The feature id for the '<em><b>Equivalence Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP = 0;

	/**
	 * The feature id for the '<em><b>Compliance Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP = 1;

	/**
	 * The feature id for the '<em><b>Ref Assurable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT = 2;

	/**
	 * The number of structural features of the '<em>Base Assurable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ASSURABLE_ELEMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Base Assurable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ASSURABLE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl <em>Base Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRequirement()
	 * @generated
	 */
	int BASE_REQUIREMENT = 1;

	/**
	 * The feature id for the '<em><b>Equivalence Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__EQUIVALENCE_MAP = BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP;

	/**
	 * The feature id for the '<em><b>Compliance Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__COMPLIANCE_MAP = BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP;

	/**
	 * The feature id for the '<em><b>Ref Assurable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__REF_ASSURABLE_ELEMENT = BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__IS_SELECTED = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Justification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__SELECTION_JUSTIFICATION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__ID = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__NAME = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__DESCRIPTION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__REFERENCE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__ASSUMPTIONS = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__RATIONALE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__IMAGE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__ANNOTATIONS = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__OWNED_REL = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Applicability</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__APPLICABILITY = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Sub Requirement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT__SUB_REQUIREMENT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Base Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT_FEATURE_COUNT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The number of operations of the '<em>Base Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT_OPERATION_COUNT = BASE_ASSURABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactImpl <em>Base Artefact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseArtefact()
	 * @generated
	 */
	int BASE_ARTEFACT = 2;

	/**
	 * The feature id for the '<em><b>Equivalence Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__EQUIVALENCE_MAP = BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP;

	/**
	 * The feature id for the '<em><b>Compliance Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__COMPLIANCE_MAP = BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP;

	/**
	 * The feature id for the '<em><b>Ref Assurable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__REF_ASSURABLE_ELEMENT = BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__IS_SELECTED = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Justification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__SELECTION_JUSTIFICATION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__ID = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__NAME = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__DESCRIPTION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__REFERENCE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Constraining Requirement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__CONSTRAINING_REQUIREMENT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Applicable Technique</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__APPLICABLE_TECHNIQUE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__OWNED_REL = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT__PROPERTY = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Base Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_FEATURE_COUNT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The number of operations of the '<em>Base Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_OPERATION_COUNT = BASE_ASSURABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl <em>Base Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseActivity()
	 * @generated
	 */
	int BASE_ACTIVITY = 3;

	/**
	 * The feature id for the '<em><b>Equivalence Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__EQUIVALENCE_MAP = BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP;

	/**
	 * The feature id for the '<em><b>Compliance Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__COMPLIANCE_MAP = BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP;

	/**
	 * The feature id for the '<em><b>Ref Assurable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__REF_ASSURABLE_ELEMENT = BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__IS_SELECTED = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Justification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__SELECTION_JUSTIFICATION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__ID = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__NAME = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__DESCRIPTION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Objective</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__OBJECTIVE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__SCOPE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Required Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__REQUIRED_ARTEFACT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Produced Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__PRODUCED_ARTEFACT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Sub Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__SUB_ACTIVITY = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Preceding Activity</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__PRECEDING_ACTIVITY = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Owned Requirement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__OWNED_REQUIREMENT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__ROLE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Applicable Technique</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__APPLICABLE_TECHNIQUE = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__OWNED_REL = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Applicability</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY__APPLICABILITY = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The number of structural features of the '<em>Base Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY_FEATURE_COUNT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The number of operations of the '<em>Base Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY_OPERATION_COUNT = BASE_ASSURABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementRelImpl <em>Base Requirement Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementRelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRequirementRel()
	 * @generated
	 */
	int BASE_REQUIREMENT_REL = 4;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT_REL__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT_REL__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT_REL__TYPE = 2;

	/**
	 * The number of structural features of the '<em>Base Requirement Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT_REL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Base Requirement Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_REQUIREMENT_REL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRoleImpl <em>Base Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRoleImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRole()
	 * @generated
	 */
	int BASE_ROLE = 5;

	/**
	 * The feature id for the '<em><b>Equivalence Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__EQUIVALENCE_MAP = BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP;

	/**
	 * The feature id for the '<em><b>Compliance Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__COMPLIANCE_MAP = BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP;

	/**
	 * The feature id for the '<em><b>Ref Assurable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__REF_ASSURABLE_ELEMENT = BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__IS_SELECTED = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Justification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__SELECTION_JUSTIFICATION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__ID = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__NAME = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE__DESCRIPTION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Base Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE_FEATURE_COUNT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Base Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ROLE_OPERATION_COUNT = BASE_ASSURABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityLevelImpl <em>Base Applicability Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityLevelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseApplicabilityLevel()
	 * @generated
	 */
	int BASE_APPLICABILITY_LEVEL = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_LEVEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_LEVEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_LEVEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Base Applicability Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_LEVEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Applicability Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_LEVEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityLevelImpl <em>Base Criticality Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityLevelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseCriticalityLevel()
	 * @generated
	 */
	int BASE_CRITICALITY_LEVEL = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_LEVEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_LEVEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_LEVEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Base Criticality Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_LEVEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Criticality Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_LEVEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseTechniqueImpl <em>Base Technique</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseTechniqueImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseTechnique()
	 * @generated
	 */
	int BASE_TECHNIQUE = 8;

	/**
	 * The feature id for the '<em><b>Equivalence Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__EQUIVALENCE_MAP = BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP;

	/**
	 * The feature id for the '<em><b>Compliance Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__COMPLIANCE_MAP = BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP;

	/**
	 * The feature id for the '<em><b>Ref Assurable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__REF_ASSURABLE_ELEMENT = BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__ID = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__NAME = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__DESCRIPTION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__IS_SELECTED = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Selection Justification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__SELECTION_JUSTIFICATION = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Critic Applic</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__CRITIC_APPLIC = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Aim</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE__AIM = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Base Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE_FEATURE_COUNT = BASE_ASSURABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Base Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TECHNIQUE_OPERATION_COUNT = BASE_ASSURABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl <em>Base Artefact Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseArtefactRel()
	 * @generated
	 */
	int BASE_ARTEFACT_REL = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Max Multiplicity Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Multiplicity Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max Multiplicity Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Min Multiplicity Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Modification Effect</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__MODIFICATION_EFFECT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Revocation Effect</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__REVOCATION_EFFECT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__SOURCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL__TARGET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Base Artefact Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Base Artefact Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ARTEFACT_REL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityApplicabilityImpl <em>Base Criticality Applicability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityApplicabilityImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseCriticalityApplicability()
	 * @generated
	 */
	int BASE_CRITICALITY_APPLICABILITY = 10;

	/**
	 * The feature id for the '<em><b>Applic Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_APPLICABILITY__APPLIC_LEVEL = 0;

	/**
	 * The feature id for the '<em><b>Critic Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_APPLICABILITY__CRITIC_LEVEL = 1;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_APPLICABILITY__COMMENT = 2;

	/**
	 * The number of structural features of the '<em>Base Criticality Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_APPLICABILITY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Base Criticality Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CRITICALITY_APPLICABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityRelImpl <em>Base Activity Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityRelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseActivityRel()
	 * @generated
	 */
	int BASE_ACTIVITY_REL = 11;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY_REL__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY_REL__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY_REL__TARGET = 2;

	/**
	 * The number of structural features of the '<em>Base Activity Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY_REL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Base Activity Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_ACTIVITY_REL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseIndependencyLevelImpl <em>Base Independency Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseIndependencyLevelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseIndependencyLevel()
	 * @generated
	 */
	int BASE_INDEPENDENCY_LEVEL = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INDEPENDENCY_LEVEL__ID = BASE_APPLICABILITY_LEVEL__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INDEPENDENCY_LEVEL__NAME = BASE_APPLICABILITY_LEVEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INDEPENDENCY_LEVEL__DESCRIPTION = BASE_APPLICABILITY_LEVEL__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Base Independency Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INDEPENDENCY_LEVEL_FEATURE_COUNT = BASE_APPLICABILITY_LEVEL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Independency Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INDEPENDENCY_LEVEL_OPERATION_COUNT = BASE_APPLICABILITY_LEVEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRecommendationLevelImpl <em>Base Recommendation Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRecommendationLevelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRecommendationLevel()
	 * @generated
	 */
	int BASE_RECOMMENDATION_LEVEL = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_RECOMMENDATION_LEVEL__ID = BASE_APPLICABILITY_LEVEL__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_RECOMMENDATION_LEVEL__NAME = BASE_APPLICABILITY_LEVEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_RECOMMENDATION_LEVEL__DESCRIPTION = BASE_APPLICABILITY_LEVEL__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Base Recommendation Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_RECOMMENDATION_LEVEL_FEATURE_COUNT = BASE_APPLICABILITY_LEVEL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Recommendation Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_RECOMMENDATION_LEVEL_OPERATION_COUNT = BASE_APPLICABILITY_LEVEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseControlCategoryImpl <em>Base Control Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseControlCategoryImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseControlCategory()
	 * @generated
	 */
	int BASE_CONTROL_CATEGORY = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONTROL_CATEGORY__ID = BASE_APPLICABILITY_LEVEL__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONTROL_CATEGORY__NAME = BASE_APPLICABILITY_LEVEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONTROL_CATEGORY__DESCRIPTION = BASE_APPLICABILITY_LEVEL__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Base Control Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONTROL_CATEGORY_FEATURE_COUNT = BASE_APPLICABILITY_LEVEL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Control Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONTROL_CATEGORY_OPERATION_COUNT = BASE_APPLICABILITY_LEVEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityImpl <em>Base Applicability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseApplicability()
	 * @generated
	 */
	int BASE_APPLICABILITY = 16;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY__ID = GeneralPackage.NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY__NAME = GeneralPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Applic Critic</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY__APPLIC_CRITIC = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY__COMMENTS = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Applic Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY__APPLIC_TARGET = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY__OWNED_REL = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Base Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_FEATURE_COUNT = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Base Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_OPERATION_COUNT = GeneralPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityRelImpl <em>Base Applicability Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityRelImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseApplicabilityRel()
	 * @generated
	 */
	int BASE_APPLICABILITY_REL = 17;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_REL__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_REL__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_REL__TARGET = 2;

	/**
	 * The number of structural features of the '<em>Base Applicability Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_REL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Base Applicability Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_APPLICABILITY_REL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaselineElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselineElementImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaselineElement()
	 * @generated
	 */
	int BASELINE_ELEMENT = 18;

	/**
	 * The feature id for the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_ELEMENT__IS_SELECTED = 0;

	/**
	 * The feature id for the '<em><b>Selection Justification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_ELEMENT__SELECTION_JUSTIFICATION = 1;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASELINE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseEquivalenceMapImpl <em>Base Equivalence Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseEquivalenceMapImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseEquivalenceMap()
	 * @generated
	 */
	int BASE_EQUIVALENCE_MAP = 19;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP__ID = MappingPackage.EQUIVALENCE_MAP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP__NAME = MappingPackage.EQUIVALENCE_MAP__NAME;

	/**
	 * The feature id for the '<em><b>Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP__MAP_GROUP = MappingPackage.EQUIVALENCE_MAP__MAP_GROUP;

	/**
	 * The feature id for the '<em><b>Map Justification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP__MAP_JUSTIFICATION = MappingPackage.EQUIVALENCE_MAP__MAP_JUSTIFICATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP__TYPE = MappingPackage.EQUIVALENCE_MAP__TYPE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP__TARGET = MappingPackage.EQUIVALENCE_MAP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Base Equivalence Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP_FEATURE_COUNT = MappingPackage.EQUIVALENCE_MAP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Base Equivalence Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EQUIVALENCE_MAP_OPERATION_COUNT = MappingPackage.EQUIVALENCE_MAP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseComplianceMapImpl <em>Base Compliance Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseComplianceMapImpl
	 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseComplianceMap()
	 * @generated
	 */
	int BASE_COMPLIANCE_MAP = 20;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP__ID = MappingPackage.COMPLIANCE_MAP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP__NAME = MappingPackage.COMPLIANCE_MAP__NAME;

	/**
	 * The feature id for the '<em><b>Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP__MAP_GROUP = MappingPackage.COMPLIANCE_MAP__MAP_GROUP;

	/**
	 * The feature id for the '<em><b>Map Justification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP__MAP_JUSTIFICATION = MappingPackage.COMPLIANCE_MAP__MAP_JUSTIFICATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP__TYPE = MappingPackage.COMPLIANCE_MAP__TYPE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP__TARGET = MappingPackage.COMPLIANCE_MAP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Base Compliance Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP_FEATURE_COUNT = MappingPackage.COMPLIANCE_MAP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Base Compliance Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_COMPLIANCE_MAP_OPERATION_COUNT = MappingPackage.COMPLIANCE_MAP_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework <em>Base Framework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Framework</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework
	 * @generated
	 */
	EClass getBaseFramework();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getScope()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EAttribute getBaseFramework_Scope();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRev <em>Rev</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rev</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRev()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EAttribute getBaseFramework_Rev();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPurpose <em>Purpose</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Purpose</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPurpose()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EAttribute getBaseFramework_Purpose();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPublisher <em>Publisher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Publisher</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPublisher()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EAttribute getBaseFramework_Publisher();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getIssued <em>Issued</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Issued</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getIssued()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EAttribute getBaseFramework_Issued();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedActivities <em>Owned Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Activities</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedActivities()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_OwnedActivities();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedArtefact <em>Owned Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Artefact</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedArtefact()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_OwnedArtefact();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedRequirement <em>Owned Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Requirement</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedRequirement()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_OwnedRequirement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedApplicLevel <em>Owned Applic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Applic Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedApplicLevel()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_OwnedApplicLevel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedCriticLevel <em>Owned Critic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Critic Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedCriticLevel()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_OwnedCriticLevel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedRole <em>Owned Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Role</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedRole()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_OwnedRole();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedTechnique <em>Owned Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Technique</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedTechnique()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_OwnedTechnique();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRefFramework <em>Ref Framework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref Framework</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRefFramework()
	 * @see #getBaseFramework()
	 * @generated
	 */
	EReference getBaseFramework_RefFramework();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement <em>Base Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Requirement</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement
	 * @generated
	 */
	EClass getBaseRequirement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getReference()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EAttribute getBaseRequirement_Reference();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAssumptions <em>Assumptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assumptions</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAssumptions()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EAttribute getBaseRequirement_Assumptions();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getRationale <em>Rationale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rationale</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getRationale()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EAttribute getBaseRequirement_Rationale();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getImage()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EAttribute getBaseRequirement_Image();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Annotations</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getAnnotations()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EAttribute getBaseRequirement_Annotations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getOwnedRel()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EReference getBaseRequirement_OwnedRel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getApplicability <em>Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applicability</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getApplicability()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EReference getBaseRequirement_Applicability();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getSubRequirement <em>Sub Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Requirement</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirement#getSubRequirement()
	 * @see #getBaseRequirement()
	 * @generated
	 */
	EReference getBaseRequirement_SubRequirement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact <em>Base Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Artefact</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefact
	 * @generated
	 */
	EClass getBaseArtefact();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getReference()
	 * @see #getBaseArtefact()
	 * @generated
	 */
	EAttribute getBaseArtefact_Reference();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getConstrainingRequirement <em>Constraining Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraining Requirement</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getConstrainingRequirement()
	 * @see #getBaseArtefact()
	 * @generated
	 */
	EReference getBaseArtefact_ConstrainingRequirement();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getApplicableTechnique <em>Applicable Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Applicable Technique</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getApplicableTechnique()
	 * @see #getBaseArtefact()
	 * @generated
	 */
	EReference getBaseArtefact_ApplicableTechnique();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getOwnedRel()
	 * @see #getBaseArtefact()
	 * @generated
	 */
	EReference getBaseArtefact_OwnedRel();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Property</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefact#getProperty()
	 * @see #getBaseArtefact()
	 * @generated
	 */
	EReference getBaseArtefact_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity <em>Base Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Activity</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity
	 * @generated
	 */
	EClass getBaseActivity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getObjective <em>Objective</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Objective</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getObjective()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EAttribute getBaseActivity_Objective();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getScope()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EAttribute getBaseActivity_Scope();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getRequiredArtefact <em>Required Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Artefact</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getRequiredArtefact()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_RequiredArtefact();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getProducedArtefact <em>Produced Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Produced Artefact</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getProducedArtefact()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_ProducedArtefact();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getSubActivity <em>Sub Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Activity</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getSubActivity()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_SubActivity();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getPrecedingActivity <em>Preceding Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Preceding Activity</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getPrecedingActivity()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_PrecedingActivity();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getOwnedRequirement <em>Owned Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Requirement</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getOwnedRequirement()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_OwnedRequirement();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Role</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getRole()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_Role();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getApplicableTechnique <em>Applicable Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applicable Technique</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getApplicableTechnique()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_ApplicableTechnique();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getOwnedRel()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_OwnedRel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getApplicability <em>Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applicability</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivity#getApplicability()
	 * @see #getBaseActivity()
	 * @generated
	 */
	EReference getBaseActivity_Applicability();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel <em>Base Requirement Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Requirement Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel
	 * @generated
	 */
	EClass getBaseRequirementRel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel#getTarget()
	 * @see #getBaseRequirementRel()
	 * @generated
	 */
	EReference getBaseRequirementRel_Target();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel#getSource()
	 * @see #getBaseRequirementRel()
	 * @generated
	 */
	EReference getBaseRequirementRel_Source();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel#getType()
	 * @see #getBaseRequirementRel()
	 * @generated
	 */
	EAttribute getBaseRequirementRel_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRole <em>Base Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Role</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRole
	 * @generated
	 */
	EClass getBaseRole();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel <em>Base Applicability Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Applicability Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel
	 * @generated
	 */
	EClass getBaseApplicabilityLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel <em>Base Criticality Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Criticality Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel
	 * @generated
	 */
	EClass getBaseCriticalityLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique <em>Base Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Technique</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseTechnique
	 * @generated
	 */
	EClass getBaseTechnique();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique#getCriticApplic <em>Critic Applic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Critic Applic</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseTechnique#getCriticApplic()
	 * @see #getBaseTechnique()
	 * @generated
	 */
	EReference getBaseTechnique_CriticApplic();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique#getAim <em>Aim</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Aim</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseTechnique#getAim()
	 * @see #getBaseTechnique()
	 * @generated
	 */
	EAttribute getBaseTechnique_Aim();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel <em>Base Artefact Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Artefact Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel
	 * @generated
	 */
	EClass getBaseArtefactRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicitySource <em>Max Multiplicity Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Multiplicity Source</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicitySource()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EAttribute getBaseArtefactRel_MaxMultiplicitySource();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicitySource <em>Min Multiplicity Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Multiplicity Source</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicitySource()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EAttribute getBaseArtefactRel_MinMultiplicitySource();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicityTarget <em>Max Multiplicity Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Multiplicity Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMaxMultiplicityTarget()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EAttribute getBaseArtefactRel_MaxMultiplicityTarget();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicityTarget <em>Min Multiplicity Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Multiplicity Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getMinMultiplicityTarget()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EAttribute getBaseArtefactRel_MinMultiplicityTarget();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getModificationEffect <em>Modification Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Modification Effect</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getModificationEffect()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EAttribute getBaseArtefactRel_ModificationEffect();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getRevocationEffect <em>Revocation Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Revocation Effect</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getRevocationEffect()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EAttribute getBaseArtefactRel_RevocationEffect();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getSource()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EReference getBaseArtefactRel_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel#getTarget()
	 * @see #getBaseArtefactRel()
	 * @generated
	 */
	EReference getBaseArtefactRel_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability <em>Base Criticality Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Criticality Applicability</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability
	 * @generated
	 */
	EClass getBaseCriticalityApplicability();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability#getApplicLevel <em>Applic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applic Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability#getApplicLevel()
	 * @see #getBaseCriticalityApplicability()
	 * @generated
	 */
	EReference getBaseCriticalityApplicability_ApplicLevel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability#getCriticLevel <em>Critic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Critic Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability#getCriticLevel()
	 * @see #getBaseCriticalityApplicability()
	 * @generated
	 */
	EReference getBaseCriticalityApplicability_CriticLevel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comment</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability#getComment()
	 * @see #getBaseCriticalityApplicability()
	 * @generated
	 */
	EAttribute getBaseCriticalityApplicability_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel <em>Base Activity Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Activity Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel
	 * @generated
	 */
	EClass getBaseActivityRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel#getType()
	 * @see #getBaseActivityRel()
	 * @generated
	 */
	EAttribute getBaseActivityRel_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel#getSource()
	 * @see #getBaseActivityRel()
	 * @generated
	 */
	EReference getBaseActivityRel_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel#getTarget()
	 * @see #getBaseActivityRel()
	 * @generated
	 */
	EReference getBaseActivityRel_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement <em>Base Assurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Assurable Element</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement
	 * @generated
	 */
	EClass getBaseAssurableElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getEquivalenceMap <em>Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equivalence Map</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getEquivalenceMap()
	 * @see #getBaseAssurableElement()
	 * @generated
	 */
	EReference getBaseAssurableElement_EquivalenceMap();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getComplianceMap <em>Compliance Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Compliance Map</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getComplianceMap()
	 * @see #getBaseAssurableElement()
	 * @generated
	 */
	EReference getBaseAssurableElement_ComplianceMap();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getRefAssurableElement <em>Ref Assurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref Assurable Element</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement#getRefAssurableElement()
	 * @see #getBaseAssurableElement()
	 * @generated
	 */
	EReference getBaseAssurableElement_RefAssurableElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseIndependencyLevel <em>Base Independency Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Independency Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseIndependencyLevel
	 * @generated
	 */
	EClass getBaseIndependencyLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseRecommendationLevel <em>Base Recommendation Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Recommendation Level</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseRecommendationLevel
	 * @generated
	 */
	EClass getBaseRecommendationLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseControlCategory <em>Base Control Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Control Category</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseControlCategory
	 * @generated
	 */
	EClass getBaseControlCategory();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability <em>Base Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Applicability</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicability
	 * @generated
	 */
	EClass getBaseApplicability();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getApplicCritic <em>Applic Critic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applic Critic</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getApplicCritic()
	 * @see #getBaseApplicability()
	 * @generated
	 */
	EReference getBaseApplicability_ApplicCritic();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comments</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getComments()
	 * @see #getBaseApplicability()
	 * @generated
	 */
	EAttribute getBaseApplicability_Comments();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getApplicTarget <em>Applic Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applic Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getApplicTarget()
	 * @see #getBaseApplicability()
	 * @generated
	 */
	EReference getBaseApplicability_ApplicTarget();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicability#getOwnedRel()
	 * @see #getBaseApplicability()
	 * @generated
	 */
	EReference getBaseApplicability_OwnedRel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel <em>Base Applicability Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Applicability Rel</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel
	 * @generated
	 */
	EClass getBaseApplicabilityRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel#getType()
	 * @see #getBaseApplicabilityRel()
	 * @generated
	 */
	EAttribute getBaseApplicabilityRel_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel#getSource()
	 * @see #getBaseApplicabilityRel()
	 * @generated
	 */
	EReference getBaseApplicabilityRel_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel#getTarget()
	 * @see #getBaseApplicabilityRel()
	 * @generated
	 */
	EReference getBaseApplicabilityRel_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselineElement
	 * @generated
	 */
	EClass getBaselineElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement#isIsSelected <em>Is Selected</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Selected</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselineElement#isIsSelected()
	 * @see #getBaselineElement()
	 * @generated
	 */
	EAttribute getBaselineElement_IsSelected();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement#getSelectionJustification <em>Selection Justification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection Justification</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselineElement#getSelectionJustification()
	 * @see #getBaselineElement()
	 * @generated
	 */
	EAttribute getBaselineElement_SelectionJustification();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap <em>Base Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Equivalence Map</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap
	 * @generated
	 */
	EClass getBaseEquivalenceMap();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap#getTarget()
	 * @see #getBaseEquivalenceMap()
	 * @generated
	 */
	EReference getBaseEquivalenceMap_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap <em>Base Compliance Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Compliance Map</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap
	 * @generated
	 */
	EClass getBaseComplianceMap();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap#getTarget()
	 * @see #getBaseComplianceMap()
	 * @generated
	 */
	EReference getBaseComplianceMap_Target();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BaselineFactory getBaselineFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl <em>Base Framework</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseFramework()
		 * @generated
		 */
		EClass BASE_FRAMEWORK = eINSTANCE.getBaseFramework();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_FRAMEWORK__SCOPE = eINSTANCE.getBaseFramework_Scope();

		/**
		 * The meta object literal for the '<em><b>Rev</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_FRAMEWORK__REV = eINSTANCE.getBaseFramework_Rev();

		/**
		 * The meta object literal for the '<em><b>Purpose</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_FRAMEWORK__PURPOSE = eINSTANCE.getBaseFramework_Purpose();

		/**
		 * The meta object literal for the '<em><b>Publisher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_FRAMEWORK__PUBLISHER = eINSTANCE.getBaseFramework_Publisher();

		/**
		 * The meta object literal for the '<em><b>Issued</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_FRAMEWORK__ISSUED = eINSTANCE.getBaseFramework_Issued();

		/**
		 * The meta object literal for the '<em><b>Owned Activities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__OWNED_ACTIVITIES = eINSTANCE.getBaseFramework_OwnedActivities();

		/**
		 * The meta object literal for the '<em><b>Owned Artefact</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__OWNED_ARTEFACT = eINSTANCE.getBaseFramework_OwnedArtefact();

		/**
		 * The meta object literal for the '<em><b>Owned Requirement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__OWNED_REQUIREMENT = eINSTANCE.getBaseFramework_OwnedRequirement();

		/**
		 * The meta object literal for the '<em><b>Owned Applic Level</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__OWNED_APPLIC_LEVEL = eINSTANCE.getBaseFramework_OwnedApplicLevel();

		/**
		 * The meta object literal for the '<em><b>Owned Critic Level</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__OWNED_CRITIC_LEVEL = eINSTANCE.getBaseFramework_OwnedCriticLevel();

		/**
		 * The meta object literal for the '<em><b>Owned Role</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__OWNED_ROLE = eINSTANCE.getBaseFramework_OwnedRole();

		/**
		 * The meta object literal for the '<em><b>Owned Technique</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__OWNED_TECHNIQUE = eINSTANCE.getBaseFramework_OwnedTechnique();

		/**
		 * The meta object literal for the '<em><b>Ref Framework</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FRAMEWORK__REF_FRAMEWORK = eINSTANCE.getBaseFramework_RefFramework();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl <em>Base Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRequirement()
		 * @generated
		 */
		EClass BASE_REQUIREMENT = eINSTANCE.getBaseRequirement();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_REQUIREMENT__REFERENCE = eINSTANCE.getBaseRequirement_Reference();

		/**
		 * The meta object literal for the '<em><b>Assumptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_REQUIREMENT__ASSUMPTIONS = eINSTANCE.getBaseRequirement_Assumptions();

		/**
		 * The meta object literal for the '<em><b>Rationale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_REQUIREMENT__RATIONALE = eINSTANCE.getBaseRequirement_Rationale();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_REQUIREMENT__IMAGE = eINSTANCE.getBaseRequirement_Image();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_REQUIREMENT__ANNOTATIONS = eINSTANCE.getBaseRequirement_Annotations();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_REQUIREMENT__OWNED_REL = eINSTANCE.getBaseRequirement_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Applicability</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_REQUIREMENT__APPLICABILITY = eINSTANCE.getBaseRequirement_Applicability();

		/**
		 * The meta object literal for the '<em><b>Sub Requirement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_REQUIREMENT__SUB_REQUIREMENT = eINSTANCE.getBaseRequirement_SubRequirement();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactImpl <em>Base Artefact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseArtefact()
		 * @generated
		 */
		EClass BASE_ARTEFACT = eINSTANCE.getBaseArtefact();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ARTEFACT__REFERENCE = eINSTANCE.getBaseArtefact_Reference();

		/**
		 * The meta object literal for the '<em><b>Constraining Requirement</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ARTEFACT__CONSTRAINING_REQUIREMENT = eINSTANCE.getBaseArtefact_ConstrainingRequirement();

		/**
		 * The meta object literal for the '<em><b>Applicable Technique</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ARTEFACT__APPLICABLE_TECHNIQUE = eINSTANCE.getBaseArtefact_ApplicableTechnique();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ARTEFACT__OWNED_REL = eINSTANCE.getBaseArtefact_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ARTEFACT__PROPERTY = eINSTANCE.getBaseArtefact_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl <em>Base Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseActivity()
		 * @generated
		 */
		EClass BASE_ACTIVITY = eINSTANCE.getBaseActivity();

		/**
		 * The meta object literal for the '<em><b>Objective</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ACTIVITY__OBJECTIVE = eINSTANCE.getBaseActivity_Objective();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ACTIVITY__SCOPE = eINSTANCE.getBaseActivity_Scope();

		/**
		 * The meta object literal for the '<em><b>Required Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__REQUIRED_ARTEFACT = eINSTANCE.getBaseActivity_RequiredArtefact();

		/**
		 * The meta object literal for the '<em><b>Produced Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__PRODUCED_ARTEFACT = eINSTANCE.getBaseActivity_ProducedArtefact();

		/**
		 * The meta object literal for the '<em><b>Sub Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__SUB_ACTIVITY = eINSTANCE.getBaseActivity_SubActivity();

		/**
		 * The meta object literal for the '<em><b>Preceding Activity</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__PRECEDING_ACTIVITY = eINSTANCE.getBaseActivity_PrecedingActivity();

		/**
		 * The meta object literal for the '<em><b>Owned Requirement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__OWNED_REQUIREMENT = eINSTANCE.getBaseActivity_OwnedRequirement();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__ROLE = eINSTANCE.getBaseActivity_Role();

		/**
		 * The meta object literal for the '<em><b>Applicable Technique</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__APPLICABLE_TECHNIQUE = eINSTANCE.getBaseActivity_ApplicableTechnique();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__OWNED_REL = eINSTANCE.getBaseActivity_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Applicability</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY__APPLICABILITY = eINSTANCE.getBaseActivity_Applicability();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementRelImpl <em>Base Requirement Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRequirementRelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRequirementRel()
		 * @generated
		 */
		EClass BASE_REQUIREMENT_REL = eINSTANCE.getBaseRequirementRel();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_REQUIREMENT_REL__TARGET = eINSTANCE.getBaseRequirementRel_Target();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_REQUIREMENT_REL__SOURCE = eINSTANCE.getBaseRequirementRel_Source();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_REQUIREMENT_REL__TYPE = eINSTANCE.getBaseRequirementRel_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRoleImpl <em>Base Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRoleImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRole()
		 * @generated
		 */
		EClass BASE_ROLE = eINSTANCE.getBaseRole();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityLevelImpl <em>Base Applicability Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityLevelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseApplicabilityLevel()
		 * @generated
		 */
		EClass BASE_APPLICABILITY_LEVEL = eINSTANCE.getBaseApplicabilityLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityLevelImpl <em>Base Criticality Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityLevelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseCriticalityLevel()
		 * @generated
		 */
		EClass BASE_CRITICALITY_LEVEL = eINSTANCE.getBaseCriticalityLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseTechniqueImpl <em>Base Technique</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseTechniqueImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseTechnique()
		 * @generated
		 */
		EClass BASE_TECHNIQUE = eINSTANCE.getBaseTechnique();

		/**
		 * The meta object literal for the '<em><b>Critic Applic</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_TECHNIQUE__CRITIC_APPLIC = eINSTANCE.getBaseTechnique_CriticApplic();

		/**
		 * The meta object literal for the '<em><b>Aim</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_TECHNIQUE__AIM = eINSTANCE.getBaseTechnique_Aim();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl <em>Base Artefact Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseArtefactRel()
		 * @generated
		 */
		EClass BASE_ARTEFACT_REL = eINSTANCE.getBaseArtefactRel();

		/**
		 * The meta object literal for the '<em><b>Max Multiplicity Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE = eINSTANCE.getBaseArtefactRel_MaxMultiplicitySource();

		/**
		 * The meta object literal for the '<em><b>Min Multiplicity Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE = eINSTANCE.getBaseArtefactRel_MinMultiplicitySource();

		/**
		 * The meta object literal for the '<em><b>Max Multiplicity Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET = eINSTANCE.getBaseArtefactRel_MaxMultiplicityTarget();

		/**
		 * The meta object literal for the '<em><b>Min Multiplicity Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET = eINSTANCE.getBaseArtefactRel_MinMultiplicityTarget();

		/**
		 * The meta object literal for the '<em><b>Modification Effect</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ARTEFACT_REL__MODIFICATION_EFFECT = eINSTANCE.getBaseArtefactRel_ModificationEffect();

		/**
		 * The meta object literal for the '<em><b>Revocation Effect</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ARTEFACT_REL__REVOCATION_EFFECT = eINSTANCE.getBaseArtefactRel_RevocationEffect();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ARTEFACT_REL__SOURCE = eINSTANCE.getBaseArtefactRel_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ARTEFACT_REL__TARGET = eINSTANCE.getBaseArtefactRel_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityApplicabilityImpl <em>Base Criticality Applicability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseCriticalityApplicabilityImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseCriticalityApplicability()
		 * @generated
		 */
		EClass BASE_CRITICALITY_APPLICABILITY = eINSTANCE.getBaseCriticalityApplicability();

		/**
		 * The meta object literal for the '<em><b>Applic Level</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_CRITICALITY_APPLICABILITY__APPLIC_LEVEL = eINSTANCE.getBaseCriticalityApplicability_ApplicLevel();

		/**
		 * The meta object literal for the '<em><b>Critic Level</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_CRITICALITY_APPLICABILITY__CRITIC_LEVEL = eINSTANCE.getBaseCriticalityApplicability_CriticLevel();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_CRITICALITY_APPLICABILITY__COMMENT = eINSTANCE.getBaseCriticalityApplicability_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityRelImpl <em>Base Activity Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseActivityRelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseActivityRel()
		 * @generated
		 */
		EClass BASE_ACTIVITY_REL = eINSTANCE.getBaseActivityRel();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_ACTIVITY_REL__TYPE = eINSTANCE.getBaseActivityRel_Type();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY_REL__SOURCE = eINSTANCE.getBaseActivityRel_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ACTIVITY_REL__TARGET = eINSTANCE.getBaseActivityRel_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseAssurableElementImpl <em>Base Assurable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseAssurableElementImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseAssurableElement()
		 * @generated
		 */
		EClass BASE_ASSURABLE_ELEMENT = eINSTANCE.getBaseAssurableElement();

		/**
		 * The meta object literal for the '<em><b>Equivalence Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP = eINSTANCE.getBaseAssurableElement_EquivalenceMap();

		/**
		 * The meta object literal for the '<em><b>Compliance Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP = eINSTANCE.getBaseAssurableElement_ComplianceMap();

		/**
		 * The meta object literal for the '<em><b>Ref Assurable Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT = eINSTANCE.getBaseAssurableElement_RefAssurableElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseIndependencyLevelImpl <em>Base Independency Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseIndependencyLevelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseIndependencyLevel()
		 * @generated
		 */
		EClass BASE_INDEPENDENCY_LEVEL = eINSTANCE.getBaseIndependencyLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseRecommendationLevelImpl <em>Base Recommendation Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseRecommendationLevelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseRecommendationLevel()
		 * @generated
		 */
		EClass BASE_RECOMMENDATION_LEVEL = eINSTANCE.getBaseRecommendationLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseControlCategoryImpl <em>Base Control Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseControlCategoryImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseControlCategory()
		 * @generated
		 */
		EClass BASE_CONTROL_CATEGORY = eINSTANCE.getBaseControlCategory();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityImpl <em>Base Applicability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseApplicability()
		 * @generated
		 */
		EClass BASE_APPLICABILITY = eINSTANCE.getBaseApplicability();

		/**
		 * The meta object literal for the '<em><b>Applic Critic</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_APPLICABILITY__APPLIC_CRITIC = eINSTANCE.getBaseApplicability_ApplicCritic();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_APPLICABILITY__COMMENTS = eINSTANCE.getBaseApplicability_Comments();

		/**
		 * The meta object literal for the '<em><b>Applic Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_APPLICABILITY__APPLIC_TARGET = eINSTANCE.getBaseApplicability_ApplicTarget();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_APPLICABILITY__OWNED_REL = eINSTANCE.getBaseApplicability_OwnedRel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityRelImpl <em>Base Applicability Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseApplicabilityRelImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseApplicabilityRel()
		 * @generated
		 */
		EClass BASE_APPLICABILITY_REL = eINSTANCE.getBaseApplicabilityRel();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_APPLICABILITY_REL__TYPE = eINSTANCE.getBaseApplicabilityRel_Type();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_APPLICABILITY_REL__SOURCE = eINSTANCE.getBaseApplicabilityRel_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_APPLICABILITY_REL__TARGET = eINSTANCE.getBaseApplicabilityRel_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaselineElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselineElementImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaselineElement()
		 * @generated
		 */
		EClass BASELINE_ELEMENT = eINSTANCE.getBaselineElement();

		/**
		 * The meta object literal for the '<em><b>Is Selected</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASELINE_ELEMENT__IS_SELECTED = eINSTANCE.getBaselineElement_IsSelected();

		/**
		 * The meta object literal for the '<em><b>Selection Justification</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASELINE_ELEMENT__SELECTION_JUSTIFICATION = eINSTANCE.getBaselineElement_SelectionJustification();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseEquivalenceMapImpl <em>Base Equivalence Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseEquivalenceMapImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseEquivalenceMap()
		 * @generated
		 */
		EClass BASE_EQUIVALENCE_MAP = eINSTANCE.getBaseEquivalenceMap();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_EQUIVALENCE_MAP__TARGET = eINSTANCE.getBaseEquivalenceMap_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseComplianceMapImpl <em>Base Compliance Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaseComplianceMapImpl
		 * @see org.eclipse.opencert.apm.baseline.baseline.impl.BaselinePackageImpl#getBaseComplianceMap()
		 * @generated
		 */
		EClass BASE_COMPLIANCE_MAP = eINSTANCE.getBaseComplianceMap();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_COMPLIANCE_MAP__TARGET = eINSTANCE.getBaseComplianceMap_Target();

	}

} //BaselinePackage
