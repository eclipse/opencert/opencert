/*******************************************************************************
 * Copyright (c) 2017, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Irfan Sljivo <irfan.sljivo@mdh.se || irfan.sljivo@gmail.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.opencert.chess.argumentGenerator.argumentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.eclipse.capra.core.CapraException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.chess.argumentGenerator.argumentation.DawnArgDiagramUtil;
import org.eclipse.opencert.chess.traceability.util.Utils;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ContractClaimLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.FormalPropertyClaimLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelFactory;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedChallenge;
import org.eclipse.opencert.sam.arg.arg.AssertedContext;
import org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.Assertion;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.CitationElementType;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;
import org.eclipse.papyrus.sysml.requirements.Requirement;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.polarsys.chess.contracts.profile.chesscontract.ComponentInstance;
import org.polarsys.chess.contracts.profile.chesscontract.Contract;
import org.polarsys.chess.contracts.profile.chesscontract.ContractProperty;
import org.polarsys.chess.contracts.profile.chesscontract.ContractRefinement;
import org.polarsys.chess.contracts.profile.chesscontract.FormalProperty;
import org.polarsys.chess.contracts.profile.chesscontract.DataTypes.Concerns;
import org.polarsys.chess.contracts.profile.chesscontract.DataTypes.ContractStatus;
import org.polarsys.chess.contracts.profile.chesscontract.DataTypes.ContractTypes;
import org.polarsys.chess.patterns.profile.PatternsProfile.Pattern;

public class CHESSContract2OpencertArgumentGenerator {
	
	private CDOView view;
	private CDOResourceFolder argFolder;
	private static final String ARGUMENTATION_NsPrefix = ".arg";
	private static final String EVIDENCE_NsPrefix = ".evidence";
	HashMap<Object, Object> options = new HashMap<Object, Object>();
	private static final String UTF_8 = "UTF-8";
	private String systemQN;
	private Boolean checkWeakContracts;
	private IProgressMonitor monitor;
	private Model model;
	
	private static final String BLOCK = "SysML::Blocks::Block";
	private static final String FLOWPORT = "SysML::PortAndFlows::FlowPort";
	private static final String FLOWPORTMARTE = "MARTE::MARTE_DesignModel::GCM::FlowPort";
	private static final String SYSVIEW = "CHESS::Core::CHESSViews::SystemView";
	private static final String COMPVIEW = "CHESS::Core::CHESSViews::ComponentView";
	public static final String REQUIREMENT_VIEW = "CHESS::Core::CHESSViews::RequirementView";
	private static final String FORMPROP = "CHESSContract::FormalProperty";
	private static final String SYSTEM = "CHESSContract::System";
	private static final String SUBSYSTEM = "CHESSContract::SubSystem";
	private static final String CONTRACTPROP = "CHESSContract::ContractProperty";
	private static final String CONTRACT = "CHESSContract::Contract";	
	private static final String COMPINST = "CHESSContract::ComponentInstance";
	private static final String COMPIMPL ="CHESS::ComponentModel::ComponentImplementation";
	private static final String SRACompQN = "CHESSContract::SRAComponent";
	private static final String BOUNDEDSUBTYPE = "MARTE::MARTE_Annexes::VSL::DataTypes::BoundedSubtype";
	private static final String COLLECTIONTYPE = "MARTE::MARTE_Annexes::VSL::DataTypes::CollectionType";
	public static final String REQUIREMENT = "SysML::Requirements::Requirement";
	private static final String PATTERN_APPLICATION = "PatternsProfile::PatternApplication";
	private static final String PATTERN_ROLE = "PatternsProfile::PatternRole";
	private static final String PATTERN = "PatternsProfile::Pattern";

	private List<Class> allBlocks = new ArrayList<Class>();
	private List<Class> allComps = new ArrayList<Class>();
	private List<Class> sysBlock = new ArrayList<Class>();	
	private List<Port> allFlowPorts = new ArrayList<Port>();
	private List<Property> allCompInst = new ArrayList<Property>();
	private List<Property> allProperties = new ArrayList<Property>();
	private List<Association> allAssociations = new ArrayList<Association>();
	private List<Connector> allConnectors = new ArrayList<Connector>();
	private List<Constraint> allFormalProps = new ArrayList<Constraint>();
	private List<Property> allContractProperties = new ArrayList<Property>();
	private List<Parameter> allParameters = new ArrayList<Parameter>();
	private List<Contract> allContracts = new ArrayList<Contract>();
	private List<Class> allReqs = new ArrayList<Class>();
	private List<ComponentArgumentationElementLink> allCompClaimLinks= new ArrayList<ComponentArgumentationElementLink>();
	private List<ContractClaimLink> allContClaimLinks= new ArrayList<ContractClaimLink>();
	private List<FormalPropertyClaimLink> allPropClaimLinks= new ArrayList<FormalPropertyClaimLink>();
	private List<CollaborationUse> allPatternApplications= new ArrayList<CollaborationUse>();
	private List<Pattern> allPatterns= new ArrayList<Pattern>();
	private List<Element> allPatternRoles= new ArrayList<Element>();//type Element since different elements can have patternRoles

	public CHESSContract2OpencertArgumentGenerator(IProgressMonitor monitor,CDOResourceFolder argFolder,Model model, String systemQN,Boolean weakContracts) {
	
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
		
		view = CDOConnectionUtil.instance.openView(session);
		options.put(XMLResource.OPTION_ENCODING, UTF_8);
		this.monitor=monitor;
		this.argFolder=argFolder;
		this.systemQN=systemQN;
		this.checkWeakContracts=weakContracts;
		this.model=model;
	}

	
	public void generateArgumentationFragments()
	{
		extractModelInformation();
		//first generate argument fragments for the system level
		for (Iterator<Class> it = sysBlock.iterator();it.hasNext();)
		{
			Class block= it.next();
			generateSingleArgumentFragment(block);			
		}
		//then generate argument fragments for each component instance

		for (Iterator<Property> it = allCompInst.iterator();it.hasNext();)
		{
			Property prop= it.next();
			generateSingleArgumentFragment(prop);			
		}
		
		for (Iterator<CollaborationUse> it = allPatternApplications.iterator();it.hasNext();)
		{
			CollaborationUse coll= it.next();
			generateSingleArgumentFragment(coll);			
		}		
	}
	private void generateSingleArgumentFragment(Element elem)//Class block, Property part)
	{		
		CDOTransaction transaction = view.getSession().openTransaction();
		//System.out.println(argFolder.getPath());
		String projectPath=argFolder.getPath().substring(0, argFolder.getPath().lastIndexOf("/"));
		CDOResourceFolder assuranceprojectFolder= transaction.getResourceFolder(projectPath);
		Class block=null;
		Property part=null;
		CollaborationUse patternApp=null;
		String componentName="";
		if(elem instanceof Class)
		{
			block=(Class) elem;
			componentName=block.getName(); 
		}
		else if (elem instanceof Property)
		{
			part=(Property) elem;	
			block=(Class) part.getType();
			componentName=block.getName()+"-"+part.getName();
		}
		else if (elem instanceof CollaborationUse)
		{
			patternApp=(CollaborationUse) elem;
			block=(Class) patternApp.getOwner();
			componentName=block.getName()+"-"+patternApp.getName();
		}
		else return;

		try {
		CDOResource argResourceDiagramDelete  = transaction.getOrCreateResource(argFolder.getPath() + "/" + componentName + ARGUMENTATION_NsPrefix + "_diagram");
		argResourceDiagramDelete.delete(options);	
		//System.out.println(argResourceDiagramDelete.getPath());
		
		CDOResource argResourceDelete  = transaction.getOrCreateResource(argFolder.getPath() + "/" + componentName + ARGUMENTATION_NsPrefix);

		argResourceDelete.delete(options);	
		
		//Delete the possible existing models with the same name
		transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			monitor.done();
		}
		
		URI targetModelURI=null;

		try {
			
	        synchronized (transaction)
	        {
	        CDOResource targetModelResource  = transaction.getOrCreateResource(argFolder.getPath() + "/" + componentName + ARGUMENTATION_NsPrefix);    		
    		targetModelURI = targetModelResource.getURI();

    		//build the argument fragment case and add it to the target components arg case to be committed to cdo
    		if(patternApp!=null)
    			targetModelResource.getContents().add(buildPatternArgumentFragment(patternApp));
    		else
    			targetModelResource.getContents().add(buildComponentArgumentFragment(block,part));
    
			transaction.commit();
	        }
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			monitor.done();
		}


		try {
	        synchronized (transaction)
	        {
	        CDOResource targetModelDiagramResource  = transaction.getOrCreateResource(argFolder.getPath() + "/" + componentName + ARGUMENTATION_NsPrefix + "_diagram");
	        DawnArgDiagramUtil myDiagram = new DawnArgDiagramUtil(targetModelURI, targetModelDiagramResource.getURI(), argFolder, transaction);
			myDiagram.generateDiagram(monitor);
			
			Set<CDOObject> conf = transaction.getConflicts();
			for (Iterator<CDOObject>it = conf.iterator(); it.hasNext();) {
				CDOObject nextObject = it.next();
				System.out.println("createCreateCommands: confict "+nextObject.toString());
			}
				transaction.commit();
	        }
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			monitor.done();
		}
	
		try {
		//Read the assuranceproject assetsPackage to add the new argumentation
		CDOResource	assuranceprojectResource  = transaction.getResource(projectPath+ "/ASSURANCE_PROJECT/" + projectPath.substring(projectPath.lastIndexOf("/")) + ".assuranceproject" );
		EList<EObject> contents = assuranceprojectResource.getContents();
		
		// Add the initial model object to the contents.
		EObject rootObject =contents.get(0);
		AssuranceProject asurproj = (AssuranceProject)rootObject;
		AssetsPackage assPackageconfig =asurproj.getAssetsPackage().get(0);

		CDOResource argResource = transaction.getResource(targetModelURI.path());
		EList<EObject> argContents = argResource.getContents();
		EObject argRootObject =argContents.get(0);
		Case argCase = (Case)argRootObject;

		assPackageconfig.getArgumentationModel().add(argCase);
			
		// Save the contents of the resource to the file system.
			options = new HashMap<Object, Object>();
			options.put(XMLResource.OPTION_ENCODING, UTF_8);
		
			assuranceprojectResource.save(options);
			//transaction.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			monitor.done();//MCP
		}	
		//End MCP
	
		//if no problems in all the previous process
		//create capra traces after everything completed successfully

		try 
		{
			//since there is only one top claim we get the case from the CDOResource using its cdoID, find the claim in the argument and make the link
			ComponentArgumentationElementLink conn = allCompClaimLinks.get(0);
			ArgumentationElement argElement = conn.getTargets().get(0);
			CDOObject cdoArgElement=view.getObject(argElement.cdoID());
			Case cdoCase=(Case) cdoArgElement.cdoResource().getContents().get(0);
			ArgumentElement argel=null;
			for (Iterator<ArgumentElement> it =cdoCase.getArgument().iterator();it.hasNext();)
				{
				ArgumentElement currElement= it.next();
				if(currElement.cdoID().equals(argElement.cdoID()))
					{
					argel=currElement;	
					break;
					}
				}	
			if(argel!=null)
				Utils.trace(conn.getSources(),argel);
			
			//then we add all the contract/claim links
			for (Iterator<ContractClaimLink> it = allContClaimLinks.iterator();it.hasNext();)
			{
				ContractClaimLink conn1= it.next();
				Claim theClaim=conn1.getTargets().get(0);
				argel=null;
				for (Iterator<ArgumentElement> iter =cdoCase.getArgument().iterator();iter.hasNext();)
				{
				ArgumentElement currElement= iter.next();
				if(currElement.cdoID().equals(theClaim.cdoID()))
					{
					argel=currElement;	
					break;
					}
				}
				if(argel!=null)
					Utils.trace(conn1.getSources(),(Claim)argel);
			}
			
			//and finally, formalProperty/claim links
			for (Iterator<FormalPropertyClaimLink> it = allPropClaimLinks.iterator();it.hasNext();)
			{
				FormalPropertyClaimLink conn2= it.next();
				Claim theClaim=conn2.getTargets().get(0);
				argel=null;
				for (Iterator<ArgumentElement> iter =cdoCase.getArgument().iterator();iter.hasNext();)
				{
				ArgumentElement currElement= iter.next();
				if(currElement.cdoID().equals(theClaim.cdoID()))
					{
					argel=currElement;	
					break;
					}
				}
				if(argel!=null)
					Utils.trace(conn2.getSources(),(Claim)argel);
			}
			
			//we clear all the saved links for this argument-fragment, so they are ready for the next one
			allCompClaimLinks.clear();
			allContClaimLinks.clear();
			allPropClaimLinks.clear();
			//transaction.close();	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		monitor.done();//MCP
		
			
	}
	
	
	private void extractModelInformation() {
		Package sysView = null;
		Package compView = null;
		Package reqView = null;
		getPatternElements(model);
		for (Package pkg : model.getNestedPackages()) {
			if(pkg.getAppliedStereotype(SYSVIEW) != null){
				sysView = pkg;
			}
			if(pkg.getAppliedStereotype(COMPVIEW) != null){
				compView = pkg;
			}
			if(pkg.getAppliedStereotype(REQUIREMENT_VIEW) != null){
				reqView = pkg;
			}
		} 

		// Browse through the model and get all blocks, ports, properties and associations
		

		if (sysView != null)
		
			for (Element elem : sysView.allOwnedElements()) {
				if (elem.getAppliedStereotype(BLOCK) != null || elem.getAppliedStereotype(SUBSYSTEM) != null ||
						elem.getAppliedStereotype(SYSTEM) != null) {
					allBlocks.add((Class) elem);
					}
				if (elem.getAppliedStereotype(SYSTEM) != null){
					sysBlock.add((Class) elem);
				}
				if (elem.getAppliedStereotype(COMPIMPL) != null || elem.getAppliedStereotype(SRACompQN) != null) {
					allComps.add((Class) elem);
					//argComps.add((Class) elem);
				}
				if (elem.getAppliedStereotype(COMPINST) != null ) {
					allCompInst.add((Property) elem);
				}
				if (elem.getAppliedStereotype(PATTERN_APPLICATION) != null ) {
					allPatternApplications.add((CollaborationUse) elem);
					}
				if (elem.getAppliedStereotype(PATTERN_ROLE) != null ) {
					allPatternRoles.add(elem);
				}
				if (elem instanceof Property){
					if (elem.getAppliedStereotype(FLOWPORT) != null || elem.getAppliedStereotype(FLOWPORTMARTE) != null){
						allFlowPorts.add((Port) elem);
					}else if(elem.getAppliedStereotype(CONTRACTPROP) != null){
						allContractProperties.add((Property) elem);
					}else if(!(elem instanceof Port)){
						allProperties.add((Property) elem);
					}
				}
				if (elem instanceof Association){
					allAssociations.add((Association) elem);
				}
				if (elem instanceof Connector){
					allConnectors.add((Connector) elem);
				}
				if (elem instanceof Constraint && elem.getAppliedStereotype(FORMPROP) != null){
					allFormalProps.add((Constraint)elem);
				}
				if (elem instanceof Parameter){
					if (((Parameter) elem).getOperation()!= null)
						allParameters.add((Parameter) elem);
				}
				if (elem instanceof Contract){
					allContracts.add((Contract) elem);
				}
			}
		
		if (compView != null)
			for (Element elem : compView.allOwnedElements()) {
				
				if (elem.getAppliedStereotype(COMPIMPL) != null || elem.getAppliedStereotype(SRACompQN) != null) {
					allComps.add((Class) elem);
				}
				
				if (elem instanceof Constraint && elem.getAppliedStereotype(FORMPROP) != null){
					allFormalProps.add((Constraint)elem);
				}
				if (elem.getAppliedStereotype(COMPINST) != null ) {
					allCompInst.add((Property) elem);
				}
				if (elem instanceof Property){
					if (elem.getAppliedStereotype(CONTRACTPROP) != null){
						allContractProperties.add((Property) elem);
					}else if(!(elem instanceof Port) && !(elem.getOwner() instanceof DataType)){
						allProperties.add((Property) elem);
					}
				}
				if (elem instanceof Parameter){
					if (((Parameter) elem).getOperation()!= null)
					allParameters.add((Parameter) elem);
				}
				if (elem instanceof Contract){
					allContracts.add((Contract) elem);
				}
			}
		
		if (reqView != null)
			for (Element elem : reqView.allOwnedElements()) {
				
				if (elem.getAppliedStereotype(REQUIREMENT) != null) {
					allReqs.add((Class) elem);
				}
			}

		/*for(Class elem:sysBlock)
		{
			System.out.println(elem.getName());
		}
		for(Property elem:allCompInst)
		{
			System.out.println(elem.getName());
		}
		for(Property prop:allContractProperties)
		{
			System.out.println(prop.getName());
		}*/

	}
	
	private void getPatternElements(Package pkg) {
		for(Element el : pkg.getMembers()) {
			if(el instanceof Collaboration){
				Stereotype stereo = el.getAppliedStereotype(PATTERN);
				if(stereo != null){
					Pattern pattern = (Pattern) el.getStereotypeApplication(stereo);
					if(pattern != null && !allPatterns.contains(pattern)){
						allPatterns.add(pattern);
					}
				}
			}
			if(el instanceof Package) {
				getPatternElements((Package)el);
			}
		}
	}
	
	private Case buildComponentArgumentFragment(Class block, Property part)
	{
		String compName;
		ComponentInstance compInst=null;
		if (block==null)
			try {
				throw new Exception("Something went wrong. Cannot create argumentation for elements that are either not blocks or dont have a block element as a type");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		if(part!=null)
			{
				compName=block.getName()+"-"+part.getName();;
				compInst = (ComponentInstance) part.getStereotypeApplication(part.getAppliedStereotype(COMPINST));
			}
		else 
			compName=block.getName(); 

		List <Property> ownContracts;
		ownContracts=getOwnedContractsFromBlock(block);
		
		Case arggCase =	ArgFactory.eINSTANCE.createCase();
		String topClaimDescription=null;
		if(part!=null)
			topClaimDescription="The contracts of the component "+compName+" are validated with sufficient confidence in the system context";
		else
			topClaimDescription="The system contracts are validated with sufficient confidence";
		Claim top=instantiateClaim(compName+"_conf",topClaimDescription,false,ownContracts.isEmpty(),ownContracts.isEmpty());
		arggCase.getArgument().add(top);
		
			try{
				createComponentAndClaimLink(block, top);
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		for (Property prop:ownContracts)
		{
			Contract theContract=null;
			String contractDescription="";

			ContractProperty contractProp = (ContractProperty) prop.getStereotypeApplication(prop.getAppliedStereotype(CONTRACTPROP));
			//if we are assuring component instance and if we are not checking all weak contracts, then we should only include the selected weak contracts in this particular instance
			//otherwise if we are checking all weak contracts in the refinement, then include only the validated weak contracts in this context
			if(compInst!=null && contractProp.getContractType().equals(ContractTypes.WEAK) && 
					((checkWeakContracts.equals(false) && !compInst.getWeakGuarantees().contains(contractProp)) || 
							(checkWeakContracts.equals(true) && contractProp.getStatus().equals(ContractStatus.NOT_VALIDATED))))
				continue;
			
			Class actualContractElement=(Class) prop.getType();
			
			if (actualContractElement!=null) 
				theContract = (Contract) actualContractElement.getStereotypeApplication(actualContractElement.getAppliedStereotype(CONTRACT));;
			
			//if there is no contract connected to this contract property, there is no information to generate the argument
			if (theContract==null)
				continue;
			contractDescription=theContract.getContextStatement();
			
			
				
			Concerns concern=theContract.getGuarantee().getConcern();
			String topName;
			if(concern != null) topName=concern.getName()+"_"+prop.getName()+"_sat";
			else topName=prop.getName()+"_sat";
			Claim contrClaim=instantiateClaim(topName,
					"The contract '" + prop.getName()+"' is satisfied with sufficient confidence",false,false,false);
			arggCase.getArgument().add(contrClaim);
			arggCase.getArgument().add(connectTwoClaims(top, contrClaim));
			
			try{
				createContractAndClaimLink(theContract, contrClaim);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(contractDescription!=null && !contractDescription.isEmpty() && contractDescription!="")
			{
				InformationElementCitation contractContext = instantiateInformationElement( prop.getName()+"_context", contractDescription,InformationElementType.CONTEXT);
				arggCase.getArgument().add(contractContext);
				arggCase.getArgument().add(attachContext(contrClaim,contractContext));
			}
				

			for(Requirement req:theContract.getFormalize())
			{
				InformationElementCitation reqContext = instantiateInformationElement( prop.getName()+"_"+req.getId(), "Contract "+prop.getName()+" formalizes requirement "+req.getId()+": "+req.getText(),InformationElementType.CONTEXT);
				arggCase.getArgument().add(reqContext);
				arggCase.getArgument().add(attachContext(contrClaim,reqContext));
			}
		
			InformationElementCitation refinementResult = instantiateInformationElement("RefinementCheckResult", "The result of the contract refinement check in the given context.",InformationElementType.SOLUTION);
			arggCase.getArgument().add(refinementResult);
			
			if (contractProp.getStatus().equals(ContractStatus.VALIDATED))
			{
				Claim assumptionSat=instantiateClaim(prop.getName()+"_assumptions","The contract assumptions are met in the given context",false,false,false);
				arggCase.getArgument().add(assumptionSat);
				arggCase.getArgument().add(connectTwoClaims(contrClaim,assumptionSat));
				
				arggCase.getArgument().add(attachEvidence(assumptionSat,refinementResult));
			
				try{
					createFormalPropertyAndClaimLink(theContract.getAssume(), assumptionSat);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (contractProp.getStatus().equals(ContractStatus.NOT_VALIDATED))
			{
	
				Claim challenge=instantiateClaim(prop.getName()+"_not_validated","The contract has not been validated in the given context.",false,false,false);
				arggCase.getArgument().add(challenge);
				arggCase.getArgument().add(attachChallenge(contrClaim,challenge));
				

				arggCase.getArgument().add(attachEvidence(challenge,refinementResult));
			}
			else
			{
				Claim assumptionSat=instantiateClaim(prop.getName()+"_assumptions","The contract assumptions are met in the given context",false,true,true);
				arggCase.getArgument().add(assumptionSat);
				arggCase.getArgument().add(connectTwoClaims(contrClaim,assumptionSat));
			
				
				try{
					createFormalPropertyAndClaimLink(theContract.getAssume(), assumptionSat);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
			
			if(!contractProp.getRefinedBy().isEmpty())
			{
				Claim decompositionCor=instantiateClaim(prop.getName()+"_decomposition","The contract "+prop.getName()+" decomposition is correct",false,false,false);
				arggCase.getArgument().add(decompositionCor);
				arggCase.getArgument().add(connectTwoClaims(contrClaim,decompositionCor));
				
				for(ContractRefinement conRef:contractProp.getRefinedBy())
				{
					ContractProperty conProp=conRef.getContract();
					Claim refinesContr=instantiateClaim(conProp.getBase_Property().getName()+"_refines_"+prop.getName(),"The contract "+conProp.getBase_Property().getName()+" correctly refines the "+prop.getName()+ " contract",false,false,false);
					arggCase.getArgument().add(refinesContr);
					arggCase.getArgument().add(connectTwoClaims(decompositionCor,refinesContr));
					
					//Evidence
					arggCase.getArgument().add(attachEvidence(refinesContr,refinementResult));
					
					//Away goal
					ArgumentElementCitation argElementCit= instantiateArgumentElementCitation(conProp.getBase_Property().getName()+"_awayGoal", "The contract '" + conProp.getBase_Property().getName()+"' is satisfied with sufficient confidence", CitationElementType.CLAIM, conProp.getBase_Property().getName()+"_sat");
					arggCase.getArgument().add(argElementCit);
					arggCase.getArgument().add(attachArgumentElementCitation(refinesContr,argElementCit));
					
				}

			}
			List<Artefact> supportingArtefacts = Utils.getTracedArtefacts(theContract);
			
			if(supportingArtefacts.isEmpty())
			{
				Claim complSat=instantiateClaim(prop.getName()+"_confidence","The contract "+prop.getName()+" is sufficiently complete",false,true,true);
				arggCase.getArgument().add(complSat);
				arggCase.getArgument().add(connectTwoClaims(contrClaim,complSat));
			}
			else
			{
				Claim complSat=instantiateClaim(prop.getName()+"_confidence","The contract "+prop.getName()+" is sufficiently complete",false,false,false);
				arggCase.getArgument().add(complSat);
				arggCase.getArgument().add(connectTwoClaims(contrClaim,complSat));
				
				for(Artefact art:supportingArtefacts)
				{
					InformationElementCitation artSolution = instantiateInformationElement(art.getId(), art.getDescription(),InformationElementType.SOLUTION);
					artSolution.setName(art.getName());
					artSolution.getArtefact().add(art);
					arggCase.getArgument().add(artSolution);
					arggCase.getArgument().add(attachEvidence(complSat,artSolution));
				}
			}

		}
		return arggCase;
	}
	
	
	private Case buildPatternArgumentFragment(CollaborationUse patternApplication)
	{
		if (patternApplication==null)
		try {
			throw new Exception("Something went wrong.");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String compName="";
		Collaboration el1 = patternApplication.getType();
		Pattern patternObject = (Pattern) el1.getStereotypeApplication(el1.getAppliedStereotype(PATTERN));
		Class patternAppOwner=(Class)patternApplication.getOwner();

		compName=patternAppOwner.getName();
	
		Case arggCase =	ArgFactory.eINSTANCE.createCase();
		String topClaimDescription=null;
		//where to get the sysProblem from?, associating the pattern application with a requirement?
		topClaimDescription="The corresponding system problem is adequately addressed by applying "+patternObject.getPatternName();
		
		
		Claim topClaim=instantiateClaim(patternApplication.getName(),topClaimDescription,false,false,false);
		arggCase.getArgument().add(topClaim);
		
		InformationElementCitation systemProblemContext = instantiateInformationElement( patternApplication.getName()+"_context", "Define the system problem being addressed by applying this pattern",InformationElementType.CONTEXT);
		arggCase.getArgument().add(systemProblemContext);
		arggCase.getArgument().add(attachContext(topClaim,systemProblemContext));
		
			try{
				createComponentAndClaimLink(patternAppOwner, topClaim);
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//ASSUMPTIONS
			Claim patternAssumptionsClaim=instantiateClaim(patternObject.getPatternName()+"_assumptions", "The pattern assumptions are satisfied with sufficient confidence",false,false,false);
			arggCase.getArgument().add(patternAssumptionsClaim);
			arggCase.getArgument().add(connectTwoClaims(topClaim, patternAssumptionsClaim));
			
			EList<String> assumptions=patternObject.getPatternAssumptions();
			for (String patAssumption:assumptions)
			{	
				Claim patternAssumptionClaim=instantiateClaim(patternObject.getPatternName()+"_assumption_"+assumptions.indexOf(patAssumption), 
						"\""+patAssumption+"\"is satisfied",false,true,true);
				arggCase.getArgument().add(patternAssumptionClaim);
				arggCase.getArgument().add(connectTwoClaims(patternAssumptionsClaim,patternAssumptionClaim));

			}

			//GUARANTEES
			Claim patternGuaranteesClaim=instantiateClaim(patternObject.getPatternName()+"_guarantees", "The pattern guarantees meet the relevant requirements",false,false,false);
			arggCase.getArgument().add(patternGuaranteesClaim);
			arggCase.getArgument().add(connectTwoClaims(topClaim, patternGuaranteesClaim));

			EList<String> gaurantees=patternObject.getPatternGuarantees();
			for (String patGuarantee:gaurantees)
			{	
				Claim patternGuaranteeClaim=instantiateClaim(patternObject.getPatternName()+"_guarantee_"+gaurantees.indexOf(patGuarantee), 
						"\""+patGuarantee+"\"meets the relevant requirements",false,true,true);
				arggCase.getArgument().add(patternGuaranteeClaim);
				arggCase.getArgument().add(connectTwoClaims(patternGuaranteesClaim,patternGuaranteeClaim));

				InformationElementCitation guaranteeContext = instantiateInformationElement( patternObject.getPatternName()+"_guarantee_"+gaurantees.indexOf(patGuarantee)+"_context", 
						"List of the relevant requirements",InformationElementType.CONTEXT);
				arggCase.getArgument().add(guaranteeContext);
				arggCase.getArgument().add(attachContext(patternGuaranteeClaim,guaranteeContext));
			}

			//IMPLEMENTATION
			Claim patternImplementationClaim=instantiateClaim(patternObject.getPatternName()+"_implementation", "The pattern application correctly implements the pattern",false,true,true);
			arggCase.getArgument().add(patternImplementationClaim);
			arggCase.getArgument().add(connectTwoClaims(topClaim, patternImplementationClaim));

			EList<String> patternImplementation=patternObject.getImplementation();
			for (String patImpl:patternImplementation)
			{
				InformationElementCitation implementationContext = instantiateInformationElement( patternObject.getPatternName()+"_implementation_context", 
						patImpl,InformationElementType.CONTEXT);
			arggCase.getArgument().add(implementationContext);
			arggCase.getArgument().add(attachContext(patternImplementationClaim,implementationContext));
			}
			
			//RATIONALE
			Claim patternRationaleClaim=instantiateClaim(patternObject.getPatternName()+"_rationale", patternObject.getPatternName()+" is adequate to resolve the particular system problem",false,false,false);
			arggCase.getArgument().add(patternRationaleClaim);
			arggCase.getArgument().add(connectTwoClaims(topClaim, patternRationaleClaim));

			//-->Consequences
			Claim patternConsequencesClaim=instantiateClaim(patternObject.getPatternName()+"_consequences", patternObject.getPatternName()+" consequences are acceptable in the context of the system",false,true,true);
			arggCase.getArgument().add(patternConsequencesClaim);
			arggCase.getArgument().add(connectTwoClaims(patternRationaleClaim,patternConsequencesClaim));

			EList<String> patternConsequences=patternObject.getConsequences();
			for (String patCons:patternConsequences)
			{
				InformationElementCitation consequencesContext = instantiateInformationElement( patternObject.getPatternName()+"_consequences_context", 
						patCons,InformationElementType.CONTEXT);
			arggCase.getArgument().add(consequencesContext);
			arggCase.getArgument().add(attachContext(patternConsequencesClaim,consequencesContext));
			}
			
			//-->Intent
			Claim patternIntentClaim=instantiateClaim(patternObject.getPatternName()+"_intent", patternObject.getPatternName()+" intent is suitable to address the system problem",false,false,false);
			arggCase.getArgument().add(patternIntentClaim);
			arggCase.getArgument().add(connectTwoClaims(patternRationaleClaim,patternIntentClaim));

			//-->Intent-->problem
			Claim patternProblemClaim=instantiateClaim(patternObject.getPatternName()+"_problem", "The problem that "+patternObject.getPatternName()+" is design to address matches the system problem",false,true,true);
			arggCase.getArgument().add(patternProblemClaim);
			arggCase.getArgument().add(connectTwoClaims(patternIntentClaim,patternProblemClaim));
			
			InformationElementCitation problemContext = instantiateInformationElement( patternObject.getPatternName()+"_problem_context", 
					patternObject.getProblem(),InformationElementType.CONTEXT);
			arggCase.getArgument().add(problemContext);
			arggCase.getArgument().add(attachContext(patternProblemClaim,problemContext));
		
			//-->Intent-->Solution
			Claim patternSolutionClaim=instantiateClaim(patternObject.getPatternName()+"_solution", "The solution that "+patternObject.getPatternName()+" provides is suitable to address the system problem",false,true,true);
			arggCase.getArgument().add(patternSolutionClaim);
			arggCase.getArgument().add(connectTwoClaims(patternIntentClaim,patternSolutionClaim));
			
			InformationElementCitation solutionContext = instantiateInformationElement( patternObject.getPatternName()+"_solution_context", 
					patternObject.getSolution(),InformationElementType.CONTEXT);
			arggCase.getArgument().add(solutionContext);
			arggCase.getArgument().add(attachContext(patternSolutionClaim,solutionContext));
			
		return arggCase;
	}
	private List<Property> getOwnedContractsFromBlock(Class block)
	{
		List <Property> result = new ArrayList<Property>();
		for(Property elem:block.allAttributes())
			if (elem.getAppliedStereotype(CONTRACTPROP) != null)
				{
					result.add(elem);
				}
			return result;
	}
	
	protected void createComponentAndClaimLink(Class component, Claim claim) throws CapraException{
	 	   
		 ComponentArgumentationElementLink createLink= OpenCertTraceLinkMetaModelFactory.eINSTANCE.createComponentArgumentationElementLink();
		 createLink.setSources(component);
		 createLink.getTargets().add(claim);
		 allCompClaimLinks.add(createLink);
	   }
	 protected void createContractAndClaimLink(Contract contract, Claim claim) throws CapraException{
	   
		 ContractClaimLink createLink= OpenCertTraceLinkMetaModelFactory.eINSTANCE.createContractClaimLink();
		 createLink.setSources(contract);
		 createLink.getTargets().add(claim);
		 allContClaimLinks.add(createLink);
	 	}
	 protected void createFormalPropertyAndClaimLink(FormalProperty prop, Claim claim) throws CapraException{
		   
		 FormalPropertyClaimLink createLink= OpenCertTraceLinkMetaModelFactory.eINSTANCE.createFormalPropertyClaimLink();
		 createLink.setSources(prop);
		 createLink.getTargets().add(claim);
		 allPropClaimLinks.add(createLink);
		}
	 
	private Claim instantiateClaim(String name, String description, Boolean assumed, Boolean toBeInstantiated, Boolean toBeSupported)
	{
		Claim x=ArgFactory.eINSTANCE.createClaim();
		x.setId(name);
		x.setName(name);
		x.setAssumed(assumed);
		x.setDescription(description);
		x.setToBeInstantiated(toBeInstantiated);
		x.setToBeSupported(toBeSupported);
		return x;
	}
	private ArgumentElementCitation instantiateArgumentElementCitation(String name, String description, CitationElementType type, String reference)
	{
		ArgumentElementCitation x=ArgFactory.eINSTANCE.createArgumentElementCitation();
		x.setId(name);
		x.setName(name);
		x.setCitedType(type);
		x.setDescription(description);
		x.setArgumentationReference(reference);
		return x;
	}
	
	private InformationElementCitation instantiateInformationElement(String name, String description,InformationElementType type)
	{
		InformationElementCitation x=ArgFactory.eINSTANCE.createInformationElementCitation();
		x.setId(name);
		x.setName(name);
		x.setDescription(description);
		x.setType(type);
		return x;
	}
	
	private AssertedInference connectTwoClaims(Claim a, Claim b)
	{
		AssertedInference x=ArgFactory.eINSTANCE.createAssertedInference();
		x.getSource().add(a);
		x.getTarget().add(b);
		return x;
	}
	private AssertedInference attachArgumentElementCitation(Claim a, ArgumentElementCitation b)
	{
		AssertedInference x=ArgFactory.eINSTANCE.createAssertedInference();
		x.getSource().add(a);
		x.getTarget().add(b);
		return x;
	}
	private AssertedEvidence attachEvidence(Claim a, InformationElementCitation b)
	{
		AssertedEvidence x=ArgFactory.eINSTANCE.createAssertedEvidence();
		x.getSource().add(a);
		x.getTarget().add(b);
		return x;
	}
	private AssertedContext attachContext(Claim a, InformationElementCitation b)
	{
		AssertedContext x=ArgFactory.eINSTANCE.createAssertedContext();
		x.getSource().add(a);
		x.getTarget().add(b);
		return x;
	}
	private AssertedChallenge attachChallenge(Claim a, Assertion b)
	{
		AssertedChallenge x=ArgFactory.eINSTANCE.createAssertedChallenge();
		x.getSource().add(a);
		x.getTarget().add(b);
		return x;
	}
	private AssertedCounterEvidence attachCounterEvidence(InformationElementCitation a, Assertion b)
	{
		AssertedCounterEvidence x=ArgFactory.eINSTANCE.createAssertedCounterEvidence();
		x.getSource().add(a);
		x.getTarget().add(b);
		return x;
	}
}
