/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package figures;

import org.eclipse.draw2d.ImageFigure;
import org.eclipse.opencert.pkm.refframework.utils.figures.activator.PluginActivator;


public class RefRoleFigure extends ImageFigure {
	
	public RefRoleFigure() { 
		
		super(PluginActivator.imageDescriptorFromPlugin(PluginActivator.ID,"images/refrole.png").createImage(), 0);
		
	  }
	//To display selected icon
	public RefRoleFigure(int notSelected) { 
		
		super(PluginActivator.imageDescriptorFromPlugin(PluginActivator.ID,"images/refroleNotSel.png").createImage(), 0);
		
	  }
	
}
