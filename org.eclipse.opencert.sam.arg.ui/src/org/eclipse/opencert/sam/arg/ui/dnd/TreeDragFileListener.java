/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.dnd;





import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.opencert.sam.arg.ui.util.TreeListFiles.TreeObject;




	
	public class TreeDragFileListener implements DragSourceListener {

	  private final Viewer viewer;
	  private final String sDir;
	  
	  public TreeDragFileListener(Viewer viewer, String dir) {
	    this.viewer = viewer;
	    this.sDir = dir;
	  }

	  @Override
	  public void dragFinished(DragSourceEvent event) {
	    System.out.println("Finshed Drag");
	  }

	  @Override
	  public void dragSetData(DragSourceEvent event) {
		  System.out.println("dragSetData input");
	    // Here you do the convertion to the type which is expected.
	    IStructuredSelection selection = (IStructuredSelection) viewer
	    .getSelection();
		TreeObject firstElement = (TreeObject)((IStructuredSelection)selection).getFirstElement();
	    
	    //if (TextTransfer.getInstance().isSupportedType(event.dataType)) 
	    {
	       // aqui debo construir los datos a itercambiar!!!!
	       //event.data = firstElement.getFullName() + " 2 " + sDir ;
	    	event.data = firstElement.getFullName();
	       System.out.println("dragSetData: event->data ." + event.data);	      
	    }
	  }

	  @Override
	  public void dragStart(DragSourceEvent event) {
	    System.out.println("Start Drag");
	  }

	public String getsDir() {
		return sDir;
	}

	} 
