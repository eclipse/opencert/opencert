/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.dnd;





import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.opencert.sam.arg.ui.util.TreeListFiles.TreeObject;




	
	public class DragPatternListener implements DragSourceListener {

	  private final Viewer viewer;
	  private CDOView viewCDO=null;
//	  private final String sDir;
	  
	  public DragPatternListener(Viewer viewer) {
	    this.viewer = viewer;
//	    this.sDir = dir;
	  }

	  @Override
	  public void dragFinished(DragSourceEvent event) {
	    System.out.println("Finished Drag");
	  }

	  @Override
	  public void dragSetData(DragSourceEvent event) {
		  System.out.println("dragSetData input");
	    // Here you do the convertion to the type which is expected.
	    IStructuredSelection selection = (IStructuredSelection) viewer
	    .getSelection();
		String firstElement = (String)((IStructuredSelection)selection).getFirstElement();
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);
		CDOResource patternSelected = viewCDO.getResource(firstElement);
		event.data = patternSelected;
        System.out.println("dragSetData: event->data ." + event.data);	      

	  }

	  @Override
	  public void dragStart(DragSourceEvent event) {
	    System.out.println("Start Drag");
	  }


	} 
