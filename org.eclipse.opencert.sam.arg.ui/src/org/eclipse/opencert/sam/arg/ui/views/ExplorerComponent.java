/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.views;



import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.jface.viewers.*;
import org.eclipse.ui.*;
import org.eclipse.swt.SWT;
import org.eclipse.opencert.sam.arg.ui.util.TreeSWTUtil;
import org.eclipse.opencert.sam.arg.ui.util.TreeListFiles;




/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class ExplorerComponent {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.eclipse.opencert.sam.arg.ui.views.ModuleExplorerComponent";
	
	private String sTitle;

	public TreeViewer viewer;
	
	private IViewSite viewSite;
	
	private String sDirectory;
	
	String getTitle() {
		return sTitle;
	}

		
	class ViewContentProvider implements IStructuredContentProvider, 
										   ITreeContentProvider {
		private TreeListFiles.TreeParent invisibleRoot;

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			/*
			if (parent.equals(getViewSite())) {
				if (invisibleRoot==null) initialize();
				return getChildren(invisibleRoot);
			}
			return getChildren(parent);
			*/
			if (invisibleRoot==null) initialize();
			return getChildren(invisibleRoot);

		}
		public Object getParent(Object child) {
			if (child instanceof TreeListFiles.TreeObject) {
				return ((TreeListFiles.TreeObject)child).getParent();
			}
			return null;
		}
		public Object [] getChildren(Object parent) {
			if (parent instanceof TreeListFiles.TreeParent) {
				return ((TreeListFiles.TreeParent)parent).getChildren();
			}
			return new Object[0];
		}
		public boolean hasChildren(Object parent) {
			if (parent instanceof TreeListFiles.TreeParent)
				return ((TreeListFiles.TreeParent)parent).hasChildren();
			return false;
		}
/*
 * We will set up a dummy model to initialize tree heararchy.
 * In a real code, you will connect to a real model and
 * expose its hierarchy.
 */
		public TreeListFiles.TreeParent initialize() {
			TreeSWTUtil lf = new TreeSWTUtil();
			invisibleRoot = lf.getListFiles(sDirectory, Integer.MAX_VALUE);
			return invisibleRoot;
		}
	}
	class ViewLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			return obj.toString();
		}
		public Image getImage(Object obj) {
			String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
			if (obj instanceof TreeListFiles.TreeParent)
			   imageKey = ISharedImages.IMG_OBJ_FOLDER;
			return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
		}
	}
	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public ExplorerComponent(IViewSite vs, String sDir, String sTit) {
		viewSite = vs;
		sDirectory = sDir;
		sTitle = sTit;
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		//DrillDownAdapter drillDownAdapter = new DrillDownAdapter(viewer);

		
		ViewContentProvider cp = new ViewContentProvider();
		viewer.setContentProvider(cp);
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		//viewer.setInput(pgetViewSite());
		//viewer.setInput(cp.initialize());
//		cp.initialize();
		viewer.setInput(viewSite);
		//viewer.expandAll();
		viewer.expandToLevel(AbstractTreeViewer.ALL_LEVELS);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}
