/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.relations.traverse;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.impactanalysis.relations.ArtefactRelationType;
import org.eclipse.opencert.impactanalysis.relations.ArtefactsRelationImpactFactory;
import org.eclipse.opencert.impactanalysis.relations.IArtefactRelationImpact;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

public class DirectAndBaseRelationsTraverser
    extends AbstractRelationsTraverser
{
    @Override
    public List<IArtefactRelationImpact> generateArtefactRelationImpactsList(CDOTransaction cdoTransaction, Long impactingArtefactCdoId,
            IArtefactRelationImpact sourceArtefactRelationImpact, int recursionDepth, EventKind impactingEventKind)
    {
        List<IArtefactRelationImpact> artefactRelationImpacts = new ArrayList<IArtefactRelationImpact>();
       
        ArtefactRelationType sourceArtefactRelationType = null;
        if (sourceArtefactRelationImpact != null) {
            sourceArtefactRelationType = sourceArtefactRelationImpact.getArtefactRelationType();
        }
        
        //If no relations before were given (so this is 1st impact) or DIRECT_EVIDENCE_RELATION or we left the baseline
        //traversing via BASELINE_TO_EVIDENCE_VIA_MAP_RELATION we expect only Artefacts or mapped BaseArtefacts to be affected 
        if (sourceArtefactRelationType == null 
                || ArtefactRelationType.DIRECT_EVIDENCE_RELATION.equals(sourceArtefactRelationType)
                || ArtefactRelationType.BASELINE_TO_EVIDENCE_VIA_MAP_RELATION.equals(sourceArtefactRelationType))
        {
            addDirectArtefactRelations(cdoTransaction, impactingArtefactCdoId, impactingEventKind, artefactRelationImpacts, recursionDepth);
            
            addEvidenceToBaselineMappedArtefactRelations(cdoTransaction, impactingArtefactCdoId, impactingEventKind, sourceArtefactRelationImpact, 
                    artefactRelationImpacts, recursionDepth);
        
            return artefactRelationImpacts;
        }
        
        //otherwise we expect BaseArtefact->BaseArtefact only if we are already traversing the baseline
        //or already entered it via EVIDENCE_TO_BASELINE_VIA_MAP_RELATION
        if (ArtefactRelationType.DIRECT_BASELINE_RELATION.equals(sourceArtefactRelationType) 
                || ArtefactRelationType.EVIDENCE_TO_BASELINE_VIA_MAP_RELATION.equals(sourceArtefactRelationType))
        {
            addDirectBaseArtefactRelations(cdoTransaction, impactingArtefactCdoId, impactingEventKind, artefactRelationImpacts, 
                    recursionDepth);
        }
        
        //we expect BaseArtefact->Artefact mapped relation only after at least one DIRECT_BASELINE_RELATION 
        if (ArtefactRelationType.DIRECT_BASELINE_RELATION.equals(sourceArtefactRelationType)) {
            
            addBaselineToEvidenceMappedArtefactRelations(cdoTransaction, impactingArtefactCdoId, impactingEventKind, sourceArtefactRelationImpact, artefactRelationImpacts, recursionDepth);
        }
        
        return artefactRelationImpacts;
    }

    
    private void addDirectBaseArtefactRelations(CDOTransaction cdoTransaction,
            Long impactingArtefactCdoId, EventKind impactingEventKind,
            List<IArtefactRelationImpact> artefactRelationImpacts, int recursionDepth)
    {
        CDOQuery query = cdoTransaction.createQuery("sql", "select * from baseline_baseartefactrel where target = " 
                + impactingArtefactCdoId + CDOStorageUtil.getMandatoryCDOQuerySuffix());
        
        List<BaseArtefactRel> impactedBaseArtefactRelations = query.getResult(BaseArtefactRel.class);
        
        for (BaseArtefactRel baseArtefactRel : impactedBaseArtefactRelations) {
            
            IArtefactRelationImpact artefactRelationImpact = 
                    ArtefactsRelationImpactFactory.createArtefactsRelationImpact(recursionDepth, baseArtefactRel, impactingEventKind);
            
            if (artefactRelationImpact == null) {
                continue;
            }
            
            artefactRelationImpacts.add(artefactRelationImpact);
        }
    }


    private void addEvidenceToBaselineMappedArtefactRelations(CDOTransaction cdoTransaction,
            Long impactingArtefactCdoId, EventKind impactingEventKind,
            IArtefactRelationImpact sourceArtefactRelationImpact,
            List<IArtefactRelationImpact> artefactRelationImpacts, int recursionDepth)
    {
        
        CDOQuery query2 = cdoTransaction.createQuery("sql", "select * from evidence_artefact where cdo_id = " + impactingArtefactCdoId 
                + CDOStorageUtil.getMandatoryCDOQuerySuffix() );
        
        Artefact impactingArtefact = query2.getResultValue(Artefact.class);
        
        
        CDOQuery query = cdoTransaction.createQuery("sql", 
                "select cdo_source " //--base artefact cdoid
                    + "from baseline_baseartefact_compliancemap_list "
                    + "where (cdo_version >= 0) and cdo_value in "
                        + "(select cdo_id "//--map cdoid
                        + "from baseline_basecompliancemap where " + CDOStorageUtil.getMandatoryCDOQuerySuffix(false) + " and cdo_id in "
                            + "(select cdo_source from baseline_basecompliancemap_target_list where cdo_value = " + impactingArtefactCdoId
                            + CDOStorageUtil.getMandatoryCDOQuerySuffix()
                            + "));"
                );
        
        List<BaseArtefact> impactedMappedBaseArtefacts = query.getResult(BaseArtefact.class);
        
        for (BaseArtefact impactedMappedBaseArtefact : impactedMappedBaseArtefacts) {
            
            IArtefactRelationImpact artefactRelationImpact = 
                    ArtefactsRelationImpactFactory.createEvidenceMappedToBaseArtefactImpact(recursionDepth, impactingArtefact, impactedMappedBaseArtefact,
                            impactingEventKind, sourceArtefactRelationImpact);
            
            if (artefactRelationImpact == null) {
                continue;
            }
            
            artefactRelationImpacts.add(artefactRelationImpact);
        }
    }
    
    
    private void addBaselineToEvidenceMappedArtefactRelations(
            CDOTransaction cdoTransaction, Long impactingArtefactCdoId,
            EventKind impactingEventKind, IArtefactRelationImpact sourceArtefactRelationImpact,
            List<IArtefactRelationImpact> artefactRelationImpacts, int recursionDepth)
    {
        
        CDOQuery query2 = cdoTransaction.createQuery("sql", "select * from baseline_baseartefact where cdo_id = " + impactingArtefactCdoId 
                + CDOStorageUtil.getMandatoryCDOQuerySuffix() );
        
        BaseArtefact baseArtefact = query2.getResultValue(BaseArtefact.class); 
        
        
        CDOQuery query = cdoTransaction.createQuery("sql", 
                "select cdo_value "// --all mapped artefacts
                + "from baseline_basecompliancemap_target_list where cdo_source in "
                    + "(select cdo_id " //compliancemap cdoid of correctly versioned compliance maps
                    + "from baseline_basecompliancemap where (cdo_revised = 0 and cdo_version >= 0) and cdo_id in "
                        + "(select cdo_value " //compliancemap cdoid
                        + "from baseline_baseartefact_compliancemap_list where (cdo_version >= 0) and cdo_source = " + impactingArtefactCdoId + "))");
        
        List<Artefact> mappedArtefacts = query.getResult(Artefact.class);
        
        for (Artefact mappedArtefact : mappedArtefacts) {
            
            IArtefactRelationImpact artefactRelationImpact = 
                    ArtefactsRelationImpactFactory.createBaseArtefactMappedToEvidenceImpact(recursionDepth, baseArtefact, mappedArtefact, impactingEventKind,
                            sourceArtefactRelationImpact);
            
            if (artefactRelationImpact == null) {
                continue;
            }
            
            artefactRelationImpacts.add(artefactRelationImpact);
        }
    }
}