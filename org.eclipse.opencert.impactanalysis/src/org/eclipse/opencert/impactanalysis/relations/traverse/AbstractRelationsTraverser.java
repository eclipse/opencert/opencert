/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.relations.traverse;

import java.util.List;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.impactanalysis.relations.ArtefactsRelationImpactFactory;
import org.eclipse.opencert.impactanalysis.relations.IArtefactRelationImpact;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

abstract class AbstractRelationsTraverser
    implements IArtefactsRelationsTraverser
{
    protected void addDirectArtefactRelations(CDOTransaction cdoTransaction,
            Long impactingArtefactCdoId, EventKind impactingEventKind,
            List<IArtefactRelationImpact> artefactRelationImpacts,
            int recursionDepth)
    {
        CDOQuery query = cdoTransaction.createQuery("sql", "select * from evidence_artefactrel where target = " 
                + impactingArtefactCdoId + CDOStorageUtil.getMandatoryCDOQuerySuffix());
        
        List<ArtefactRel> impactedArtefactRelations = query.getResult(ArtefactRel.class);
        
        for (ArtefactRel artefactRel : impactedArtefactRelations) {
            
            IArtefactRelationImpact artefactRelationImpact = 
                    ArtefactsRelationImpactFactory.createArtefactsRelationImpact(recursionDepth, artefactRel, impactingEventKind);
            
            if (artefactRelationImpact == null) {
                continue;
            }
            
            artefactRelationImpacts.add(artefactRelationImpact);
        }
    }
}
