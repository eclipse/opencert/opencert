/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.eventcreators;

import org.eclipse.opencert.infra.general.general.ChangeEffectKind;

public class ChangeDrivenEventsCreatorsFactory
{
    public static IChangeDrivenEventsCreator getProperChangeDrivenEventsCreator(ChangeEffectKind changeEffectKind)
    {
        switch (changeEffectKind) {
            
            case MODIFY : 
                return new ModifyChangeDrivenEventsCreator();
            
            case REVOKE :
                return new RevokeChangeDrivenEventsCreator();
            
            case VALIDATE :
                return new ValidateChangeDrivenEventsCreator();
                
            default :
                return null;
        }
    }
}
