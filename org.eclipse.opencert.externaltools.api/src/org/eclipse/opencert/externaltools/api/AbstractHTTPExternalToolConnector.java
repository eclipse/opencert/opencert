/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

public abstract class AbstractHTTPExternalToolConnector
        extends AbstractExternalToolConnector
{
    
    static {
        disableSslVerification(); // TEMPORARY, this code omits problem with bad
                                  // certificate
    }
    
    public abstract String getUrlSuffix();

    public abstract int processHttpResponse(String httpResponse) throws IOException;

    
    private static void disableSslVerification()
    {
        try {
            TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers()
                {
                    return null;
                }

                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException
                {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException
                {
                }
            } };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session)
                {
                    return true;
                }
            };

            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
    }

    protected String getDataFromServer(String pathString) throws IOException
    {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            System.out.println(getSettingsValue("URL") + pathString);

            URL url = new URL(getSettingsValue("URL") + pathString);
            URLConnection urlConnection = setUsernamePassword(url);

            reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }     
        }

        return sb.toString();
    }

    private URLConnection setUsernamePassword(URL url) throws IOException
    {
        URLConnection urlConnection = url.openConnection();

        String authString = getSettingsValue("User") + ":" + getSettingsValue("Password");
        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));

        urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);

        return urlConnection;
    }
}
