/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ExternalToolQuerySetting
        implements Serializable
{
    private static final long serialVersionUID = -6520887474432178413L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long externalToolQuerySettingId;

    private String name;

    private String value;

    public ExternalToolQuerySetting()
    {

    }

    public ExternalToolQuerySetting(String name, String value)
    {
        this.setName(name);
        this.setValue(value);
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public long getExternalToolQuerySettingId()
    {
        return externalToolQuerySettingId;
    }

    public void setExternalToolQuerySettingId(long externalToolQuerySettingId)
    {
        this.externalToolQuerySettingId = externalToolQuerySettingId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ExternalToolQuerySetting other = (ExternalToolQuerySetting) obj;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        if (getValue() == null) {
            if (other.getValue() != null)
                return false;
        } else if (!getValue().equals(other.getValue()))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "ExternalToolQuerySetting [externalToolQuerySettingId=" + externalToolQuerySettingId + ", name=" + name + ", value=" + value + "]";
    }

    
}
