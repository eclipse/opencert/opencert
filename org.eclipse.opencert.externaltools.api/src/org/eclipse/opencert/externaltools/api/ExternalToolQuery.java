/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class ExternalToolQuery
        implements Serializable
{
    private static final long serialVersionUID = -3030098717808457507L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long externalToolQueryId;

    private long baseElementId;

    private String externalToolConnectorId;

    private String instanceLabel;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "QUERY_SETTING", joinColumns = { @JoinColumn(name = "externalToolQueryId") }, inverseJoinColumns = { @JoinColumn(name = "externalToolQuerySettingId") })
    private Set<ExternalToolQuerySetting> externalToolQuerySettings = new LinkedHashSet<ExternalToolQuerySetting>();

    private boolean isYellowEnabled;
    private int greenStatusMaxValue;
    private int yellowStatusMinValue;
    private int yellowStatusMaxValue;
    private int redStatusMinValue;

    private Date editDate;

    private String greenStatusMsg;
    private String yellowStatusMsg;
    private String redStatusMsg;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "externalToolQuery")
    @OrderBy("date DESC")
    private Set<ExternalToolQueryResult> externalToolQueryResults = new TreeSet<ExternalToolQueryResult>();

    public ExternalToolQuery()
    {

    }

    public ExternalToolQuery(String externalToolConnectorId)
    {
        this.setExternalToolConnectorId(externalToolConnectorId);
    }

    public void setStatusRanges(int greenStatusMaxValue, int yellowStatusMinValue, int yellowStatusMaxValue, boolean yellowRangeEnabled, int redStatusMinValue)
    {
        this.setGreenStatusMaxValue(greenStatusMaxValue);
        this.setYellowStatusMinValue(yellowStatusMinValue);
        this.setYellowStatusMaxValue(yellowStatusMaxValue);
        setYellowEnabled(yellowRangeEnabled);
        this.setRedStatusMinValue(redStatusMinValue);
    }

    public void setStatusLabels(String greenStatusLabel, String yellowStatusLabel, String redStatusLabel)
    {
        this.setGreenStatusMsg(greenStatusLabel);
        this.setYellowStatusMsg(yellowStatusLabel);
        this.setRedStatusMsg(redStatusLabel);
    }

    public long getExternalToolQueryId()
    {
        return externalToolQueryId;
    }

    public void setExternalToolQueryId(long externalToolQueryId)
    {
        this.externalToolQueryId = externalToolQueryId;
    }

    public long getBaseElementId()
    {
        return baseElementId;
    }

    public void setBaseElementId(long baseElementId)
    {
        this.baseElementId = baseElementId;
    }

    public String getExternalToolConnectorId()
    {
        return externalToolConnectorId;
    }

    public void setExternalToolConnectorId(String externalToolConnectorId)
    {
        this.externalToolConnectorId = externalToolConnectorId;
    }

    public String getInstanceLabel()
    {
        return instanceLabel;
    }

    public void setInstanceLabel(String instanceLabel)
    {
        this.instanceLabel = instanceLabel;
    }

    public Set<ExternalToolQuerySetting> getExternalToolQuerySettings()
    {
        return externalToolQuerySettings;
    }

    public void setExternalToolQuerySettings(Set<ExternalToolQuerySetting> externalToolQuerySettings)
    {
        this.externalToolQuerySettings = externalToolQuerySettings;
    }

    public int getGreenStatusMaxValue()
    {
        return greenStatusMaxValue;
    }

    public void setGreenStatusMaxValue(int greenStatusMaxValue)
    {
        this.greenStatusMaxValue = greenStatusMaxValue;
    }

    public int getYellowStatusMinValue()
    {
        return yellowStatusMinValue;
    }

    public void setYellowStatusMinValue(int yellowStatusMinValue)
    {
        this.yellowStatusMinValue = yellowStatusMinValue;
    }

    public int getYellowStatusMaxValue()
    {
        return yellowStatusMaxValue;
    }

    public void setYellowStatusMaxValue(int yellowStatusMaxValue)
    {
        this.yellowStatusMaxValue = yellowStatusMaxValue;
    }

    public int getRedStatusMinValue()
    {
        return redStatusMinValue;
    }

    public void setRedStatusMinValue(int redStatusMinValue)
    {
        this.redStatusMinValue = redStatusMinValue;
    }

    public Date getEditDate()
    {
        return editDate;
    }

    public void setEditDate(Date editDate)
    {
        this.editDate = editDate;
    }

    public String getGreenStatusMsg()
    {
        return greenStatusMsg;
    }

    public void setGreenStatusMsg(String greenStatusLabel)
    {
        this.greenStatusMsg = greenStatusLabel;
    }

    public String getYellowStatusMsg()
    {
        return yellowStatusMsg;
    }

    public void setYellowStatusMsg(String yellowStatusLabel)
    {
        this.yellowStatusMsg = yellowStatusLabel;
    }

    public String getRedStatusMsg()
    {
        return redStatusMsg;
    }

    public void setRedStatusMsg(String redStatusLabel)
    {
        this.redStatusMsg = redStatusLabel;
    }

    public Set<ExternalToolQueryResult> getExternalToolQueryResults()
    {
        return externalToolQueryResults;
    }

    public void setExternalToolQueryResults(Set<ExternalToolQueryResult> externalToolQueryResults)
    {
        this.externalToolQueryResults = externalToolQueryResults;
    }
    
    public Map<String, String> getSettingsAsMap()
    {       
        Map<String, String> settingsMap = new LinkedHashMap<String, String>();
        
        for (ExternalToolQuerySetting querySetting : externalToolQuerySettings) {
            settingsMap.put(querySetting.getName(), querySetting.getValue());
        }
        
        return settingsMap;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (getBaseElementId() ^ (getBaseElementId() >>> 32));
        result = prime * result + ((getEditDate() == null) ? 0 : getEditDate().hashCode());
        result = prime * result + ((getExternalToolConnectorId() == null) ? 0 : getExternalToolConnectorId().hashCode());
        result = prime * result + ((getExternalToolQueryResults() == null) ? 0 : getExternalToolQueryResults().hashCode());
        result = prime * result + ((getExternalToolQuerySettings() == null) ? 0 : getExternalToolQuerySettings().hashCode());
        result = prime * result + ((getGreenStatusMsg() == null) ? 0 : getGreenStatusMsg().hashCode());
        result = prime * result + getGreenStatusMaxValue();
        result = prime * result + ((getInstanceLabel() == null) ? 0 : getInstanceLabel().hashCode());
        result = prime * result + ((getRedStatusMsg() == null) ? 0 : getRedStatusMsg().hashCode());
        result = prime * result + getRedStatusMinValue();
        result = prime * result + ((getYellowStatusMsg() == null) ? 0 : getYellowStatusMsg().hashCode());
        result = prime * result + getYellowStatusMaxValue();
        result = prime * result + getYellowStatusMinValue();
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ExternalToolQuery))
            return false;
        ExternalToolQuery other = (ExternalToolQuery) obj;
        if (getBaseElementId() != other.getBaseElementId())
            return false;
        if (getEditDate() == null) {
            if (other.getEditDate() != null)
                return false;
        } else if (!getEditDate().equals(other.getEditDate()))
            return false;
        if (getExternalToolConnectorId() == null) {
            if (other.getExternalToolConnectorId() != null)
                return false;
        } else if (!getExternalToolConnectorId().equals(other.getExternalToolConnectorId()))
            return false;
        if (getExternalToolQueryResults() == null) {
            if (other.getExternalToolQueryResults() != null)
                return false;
        } else if (!getExternalToolQueryResults().equals(other.getExternalToolQueryResults()))
            return false;
        if (getExternalToolQuerySettings() == null) {
            if (other.getExternalToolQuerySettings() != null)
                return false;
        } else if (!getExternalToolQuerySettings().equals(other.getExternalToolQuerySettings()))
            return false;
        if (getGreenStatusMsg() == null) {
            if (other.getGreenStatusMsg() != null)
                return false;
        } else if (!getGreenStatusMsg().equals(other.getGreenStatusMsg()))
            return false;
        if (getGreenStatusMaxValue() != other.getGreenStatusMaxValue())
            return false;
        if (getInstanceLabel() == null) {
            if (other.getInstanceLabel() != null)
                return false;
        } else if (!getInstanceLabel().equals(other.getInstanceLabel()))
            return false;
        if (getRedStatusMsg() == null) {
            if (other.getRedStatusMsg() != null)
                return false;
        } else if (!getRedStatusMsg().equals(other.getRedStatusMsg()))
            return false;
        if (getRedStatusMinValue() != other.getRedStatusMinValue())
            return false;
        if (getYellowStatusMsg() == null) {
            if (other.getYellowStatusMsg() != null)
                return false;
        } else if (!getYellowStatusMsg().equals(other.getYellowStatusMsg()))
            return false;
        if (getYellowStatusMaxValue() != other.getYellowStatusMaxValue())
            return false;
        if (getYellowStatusMinValue() != other.getYellowStatusMinValue())
            return false;
        return true;
    }

    public boolean isYellowEnabled()
    {
        return isYellowEnabled;
    }

    public void setYellowEnabled(boolean isYellowEnabled)
    {
        this.isYellowEnabled = isYellowEnabled;
    }
    
}
