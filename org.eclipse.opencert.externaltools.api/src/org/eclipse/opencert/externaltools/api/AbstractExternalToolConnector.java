/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.externaltools.api;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public abstract class AbstractExternalToolConnector implements IExternalToolConnector
{
    public static final String USER_KEY = "User";
    public static final String URL_KEY = "URL";
    
    private Map<String, String> settingsNameAndValueMap = new LinkedHashMap<String, String>();
    
    
    @Override
    public Integer[] getDefaultResultRanges()
    {
        return new Integer[] { 10, 0, 0, 11 };
    }

    @Override
    public String[] getDefaultResultMsgs()
    {
        return new String[] { "OK - the result is below the warning threshold", "Warning! The result is within the range of warning threshold.", "Caution! The result exceeds the warning threshold!" };
    }
    
    @Override
    public ExternalToolQuery bootstrapExternalToolQueryInstance()
    {
        ExternalToolQuery externalToolQuery = new ExternalToolQuery(getExternalToolConnectorId());

        bootstrapSettingsList(externalToolQuery);

        bootstrapResultSettingsList(externalToolQuery);

        return externalToolQuery;
    }

    private void bootstrapResultSettingsList(ExternalToolQuery externalToolQuery)
    {
        Integer[] defaultStatusRanges = getDefaultResultRanges();
        if (defaultStatusRanges != null) {
            if (defaultStatusRanges.length != 4) {
                throw new RuntimeException("defaultStatusRanges must contain 6 values in " + getExternalToolConnectorId());
            }

            boolean yellowRangeEnabled = true;
            if (defaultStatusRanges[1] == 0 && defaultStatusRanges[2] == 0) {
                yellowRangeEnabled = false;
            }
            externalToolQuery.setStatusRanges(defaultStatusRanges[0], defaultStatusRanges[1], defaultStatusRanges[2], yellowRangeEnabled, defaultStatusRanges[3]);
        }

        String[] defaultStatusLabels = getDefaultResultMsgs();
        if (defaultStatusLabels != null) {
            if (defaultStatusLabels.length != 3) {
                throw new RuntimeException("defaultStatusLabels must contain 3 values in " + getExternalToolConnectorId());
            }

            externalToolQuery.setStatusLabels(defaultStatusLabels[0], defaultStatusLabels[1], defaultStatusLabels[2]);
        }
    }

    @Override
    public String getExternalToolConnectorId()
    {
        return this.getClass().getName();
    }

    private void bootstrapSettingsList(ExternalToolQuery externalToolQuery)
    {
        Set<ExternalToolQuerySetting> externalToolConnectorSettings = externalToolQuery.getExternalToolQuerySettings();

        settingsNameAndValueMap = getSettingDefaultValues();

        for (Map.Entry<String, String> entry : settingsNameAndValueMap.entrySet()) {
            externalToolConnectorSettings.add(new ExternalToolQuerySetting(entry.getKey(), entry.getValue()));
        }
    }

    public String getSettingsValue(String key)
    {
        return settingsNameAndValueMap.get(key);
    }

    public void putSettingsIntoConnectorSettingsMap(Map<String, String> settingsMap)
    {
        settingsNameAndValueMap.putAll(settingsMap);
    }

    @Override
    public String toString()
    {
        return "AbstractExternalToolConnector [settingsNameAndValueMap=" + settingsNameAndValueMap + "]";
    }
}
