/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choice</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Choice#getSourceMultiextension <em>Source Multiextension</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Choice#getSourceCardinality <em>Source Cardinality</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Choice#getOptionality <em>Optionality</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getChoice()
 * @model annotation="gmf.node label='optionality' label.placement='external' label.icon='false' figure='org.eclipse.opencert.sam.arg.ui.figures.Choice' tool.small.path='GSN_tooling_icons/Optionality.gif' tool.large.path='GSN_tooling_icons/Optionality.gif' tool.small.bundle='org.eclipse.opencert.sam.arg' tool.large.bundle='org.eclipse.opencert.sam.arg' size='40,40'"
 * @generated
 */
public interface Choice extends ArgumentElement {
	/**
	 * Returns the value of the '<em><b>Source Multiextension</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Multiextension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Multiextension</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension
	 * @see #setSourceMultiextension(AssertedByMultiplicityExtension)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getChoice_SourceMultiextension()
	 * @model
	 * @generated
	 */
	AssertedByMultiplicityExtension getSourceMultiextension();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.Choice#getSourceMultiextension <em>Source Multiextension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Multiextension</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension
	 * @see #getSourceMultiextension()
	 * @generated
	 */
	void setSourceMultiextension(AssertedByMultiplicityExtension value);

	/**
	 * Returns the value of the '<em><b>Source Cardinality</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Cardinality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Cardinality</em>' attribute.
	 * @see #setSourceCardinality(String)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getChoice_SourceCardinality()
	 * @model default=""
	 * @generated
	 */
	String getSourceCardinality();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.Choice#getSourceCardinality <em>Source Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Cardinality</em>' attribute.
	 * @see #getSourceCardinality()
	 * @generated
	 */
	void setSourceCardinality(String value);

	/**
	 * Returns the value of the '<em><b>Optionality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optionality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optionality</em>' attribute.
	 * @see #setOptionality(String)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getChoice_Optionality()
	 * @model
	 * @generated
	 */
	String getOptionality();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.Choice#getOptionality <em>Optionality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optionality</em>' attribute.
	 * @see #getOptionality()
	 * @generated
	 */
	void setOptionality(String value);

} // Choice
