/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argumentation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Argumentation#getLocation <em>Location</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Argumentation#getArgumentation <em>Argumentation</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Argumentation#getConsistOf <em>Consist Of</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentation()
 * @model annotation="gmf.node label='id' label.icon='false' figure='gsnfigures.GSNArgumentModule' tool.small.bundle='org.eclipse.opencert.sam.arg' tool.small.path='GSN_tooling_icons/ArgumentModule.gif' tool.large.bundle='org.eclipse.opencert.sam.arg' tool.large.path='GSN_tooling_icons/ArgumentModule.gif' size='120,80'"
 * @generated
 */
public interface Argumentation extends ArgumentationElement, ManageableAssuranceAsset {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentation_Location()
	 * @model
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.Argumentation#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Argumentation</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Argumentation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argumentation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argumentation</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentation_Argumentation()
	 * @model
	 * @generated
	 */
	EList<Argumentation> getArgumentation();

	/**
	 * Returns the value of the '<em><b>Consist Of</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.ArgumentElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consist Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consist Of</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentation_ConsistOf()
	 * @model
	 * @generated
	 */
	EList<ArgumentElement> getConsistOf();

} // Argumentation
