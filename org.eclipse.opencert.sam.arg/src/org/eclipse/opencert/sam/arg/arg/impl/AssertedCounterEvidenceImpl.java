/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.AssertedCounterEvidence;
import org.eclipse.opencert.sam.arg.arg.Assertion;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asserted Counter Evidence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedCounterEvidenceImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedCounterEvidenceImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssertedCounterEvidenceImpl extends AssertedRelationshipImpl implements AssertedCounterEvidence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedCounterEvidenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.ASSERTED_COUNTER_EVIDENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<InformationElementCitation> getSource() {
		return (EList<InformationElementCitation>)eDynamicGet(ArgPackage.ASSERTED_COUNTER_EVIDENCE__SOURCE, ArgPackage.Literals.ASSERTED_COUNTER_EVIDENCE__SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Assertion> getTarget() {
		return (EList<Assertion>)eDynamicGet(ArgPackage.ASSERTED_COUNTER_EVIDENCE__TARGET, ArgPackage.Literals.ASSERTED_COUNTER_EVIDENCE__TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__SOURCE:
				return getSource();
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__TARGET:
				return getTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__SOURCE:
				getSource().clear();
				getSource().addAll((Collection<? extends InformationElementCitation>)newValue);
				return;
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__TARGET:
				getTarget().clear();
				getTarget().addAll((Collection<? extends Assertion>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__SOURCE:
				getSource().clear();
				return;
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__TARGET:
				getTarget().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__SOURCE:
				return !getSource().isEmpty();
			case ArgPackage.ASSERTED_COUNTER_EVIDENCE__TARGET:
				return !getTarget().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssertedCounterEvidenceImpl
