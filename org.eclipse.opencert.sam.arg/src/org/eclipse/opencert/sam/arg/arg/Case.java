/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Case#getArgument <em>Argument</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Case#getArgumentation <em>Argumentation</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Case#getAgreement <em>Agreement</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Case#getCited <em>Cited</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.Case#getInformation <em>Information</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getCase()
 * @model annotation="gmf.diagram foo='bar'"
 * @extends CDOObject
 * @generated
 */
public interface Case extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Argument</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.ArgumentElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argument</em>' containment reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getCase_Argument()
	 * @model containment="true"
	 * @generated
	 */
	EList<ArgumentElement> getArgument();

	/**
	 * Returns the value of the '<em><b>Argumentation</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Argumentation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argumentation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argumentation</em>' containment reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getCase_Argumentation()
	 * @model containment="true"
	 * @generated
	 */
	EList<Argumentation> getArgumentation();

	/**
	 * Returns the value of the '<em><b>Agreement</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Agreement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agreement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agreement</em>' containment reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getCase_Agreement()
	 * @model containment="true"
	 * @generated
	 */
	EList<Agreement> getAgreement();

	/**
	 * Returns the value of the '<em><b>Cited</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cited</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cited</em>' containment reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getCase_Cited()
	 * @model containment="true"
	 * @generated
	 */
	EList<ArgumentElementCitation> getCited();

	/**
	 * Returns the value of the '<em><b>Information</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Information</em>' containment reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getCase_Information()
	 * @model containment="true"
	 * @generated
	 */
	EList<InformationElementCitation> getInformation();

} // Case
