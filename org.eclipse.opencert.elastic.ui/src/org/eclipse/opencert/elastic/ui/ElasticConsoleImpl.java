/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import java.text.MessageFormat;

import org.eclipse.opencert.elastic.ElasticConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * Implementation of {@link ElasticConsole} delegating all output to {@link ElasticConsoleFactory#INSTANCE}.
 *
 * @author mauersberger
 */
public class ElasticConsoleImpl implements ElasticConsole {

	MessageConsoleStream stream;

	@Override
	public void log(Object any) {
		log0(any);
	}

	@Override
	public void info(String format, Object... arguments) {
		log(format, arguments);
	}

	/**
	 * @param format
	 *            a format according to {@link java.text.MessageFormat}
	 * @param arguments
	 *            arguments according to {@link java.text.MessageFormat}
	 * @since CS1
	 */
	@Override
	public void log(String format, Object... arguments) {
		log0(MessageFormat.format(format, arguments));
	}

	@Override
	public void error(String format, Object... arguments) {
		log0(MessageFormat.format(format, arguments));
	}

	/*
	 * The real logging.
	 */
	private synchronized void log0(Object any) {
		if (stream == null) {
			stream = ElasticConsoleFactory.INSTANCE.newMessageStream();
		}
		stream.println(String.valueOf(any));
	}
}
