/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleFactory;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;

/**
 * Factory to show {@link ElasticConsoleImpl}.
 * 
 * @author mauersberger
 */
public class ElasticConsoleFactory implements IConsoleFactory {

	/**
	 * There is just one console.
	 */
	public static MessageConsole INSTANCE = new MessageConsole("Elastic", null); //$NON-NLS-1$

	@Override
	public void openConsole() {
		MessageConsole console = INSTANCE;
		IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();
		boolean exists = exists(manager, console);

		if (!exists) {
			manager.addConsoles(new IConsole[] { console });
		}
		manager.showConsoleView(console);
	}

	/*
	 * Helper to decide whether a given console exists in a given manager.
	 */
	private boolean exists(IConsoleManager manager, MessageConsole console) {
		IConsole[] existing = manager.getConsoles();
		boolean exists = false;
		for (IConsole element : existing) {
			if (console == element) {
				exists = true;
			}
		}
		return exists;
	}
}
