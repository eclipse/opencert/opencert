/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Framework</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getRev <em>Rev</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getPurpose <em>Purpose</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getIssued <em>Issued</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getOwnedActivities <em>Owned Activities</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getOwnedArtefact <em>Owned Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getOwnedRequirement <em>Owned Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getOwnedApplicLevel <em>Owned Applic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getOwnedCriticLevel <em>Owned Critic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getOwnedRole <em>Owned Role</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl#getOwnedTechnique <em>Owned Technique</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefFrameworkImpl extends DescribableElementImpl implements RefFramework {
	/**
	 * The default value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected static final String SCOPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getRev() <em>Rev</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRev()
	 * @generated
	 * @ordered
	 */
	protected static final String REV_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getPurpose() <em>Purpose</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPurpose()
	 * @generated
	 * @ordered
	 */
	protected static final String PURPOSE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getPublisher() <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected static final String PUBLISHER_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIssued() <em>Issued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIssued()
	 * @generated
	 * @ordered
	 */
	protected static final Date ISSUED_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefFrameworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_FRAMEWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScope() {
		return (String)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__SCOPE, RefframeworkPackage.Literals.REF_FRAMEWORK__SCOPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(String newScope) {
		eDynamicSet(RefframeworkPackage.REF_FRAMEWORK__SCOPE, RefframeworkPackage.Literals.REF_FRAMEWORK__SCOPE, newScope);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRev() {
		return (String)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__REV, RefframeworkPackage.Literals.REF_FRAMEWORK__REV, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRev(String newRev) {
		eDynamicSet(RefframeworkPackage.REF_FRAMEWORK__REV, RefframeworkPackage.Literals.REF_FRAMEWORK__REV, newRev);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPurpose() {
		return (String)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__PURPOSE, RefframeworkPackage.Literals.REF_FRAMEWORK__PURPOSE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPurpose(String newPurpose) {
		eDynamicSet(RefframeworkPackage.REF_FRAMEWORK__PURPOSE, RefframeworkPackage.Literals.REF_FRAMEWORK__PURPOSE, newPurpose);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPublisher() {
		return (String)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__PUBLISHER, RefframeworkPackage.Literals.REF_FRAMEWORK__PUBLISHER, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublisher(String newPublisher) {
		eDynamicSet(RefframeworkPackage.REF_FRAMEWORK__PUBLISHER, RefframeworkPackage.Literals.REF_FRAMEWORK__PUBLISHER, newPublisher);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getIssued() {
		return (Date)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__ISSUED, RefframeworkPackage.Literals.REF_FRAMEWORK__ISSUED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIssued(Date newIssued) {
		eDynamicSet(RefframeworkPackage.REF_FRAMEWORK__ISSUED, RefframeworkPackage.Literals.REF_FRAMEWORK__ISSUED, newIssued);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefActivity> getOwnedActivities() {
		return (EList<RefActivity>)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__OWNED_ACTIVITIES, RefframeworkPackage.Literals.REF_FRAMEWORK__OWNED_ACTIVITIES, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefArtefact> getOwnedArtefact() {
		return (EList<RefArtefact>)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__OWNED_ARTEFACT, RefframeworkPackage.Literals.REF_FRAMEWORK__OWNED_ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefRequirement> getOwnedRequirement() {
		return (EList<RefRequirement>)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__OWNED_REQUIREMENT, RefframeworkPackage.Literals.REF_FRAMEWORK__OWNED_REQUIREMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefApplicabilityLevel> getOwnedApplicLevel() {
		return (EList<RefApplicabilityLevel>)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__OWNED_APPLIC_LEVEL, RefframeworkPackage.Literals.REF_FRAMEWORK__OWNED_APPLIC_LEVEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefCriticalityLevel> getOwnedCriticLevel() {
		return (EList<RefCriticalityLevel>)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__OWNED_CRITIC_LEVEL, RefframeworkPackage.Literals.REF_FRAMEWORK__OWNED_CRITIC_LEVEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefRole> getOwnedRole() {
		return (EList<RefRole>)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__OWNED_ROLE, RefframeworkPackage.Literals.REF_FRAMEWORK__OWNED_ROLE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefTechnique> getOwnedTechnique() {
		return (EList<RefTechnique>)eDynamicGet(RefframeworkPackage.REF_FRAMEWORK__OWNED_TECHNIQUE, RefframeworkPackage.Literals.REF_FRAMEWORK__OWNED_TECHNIQUE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ACTIVITIES:
				return ((InternalEList<?>)getOwnedActivities()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ARTEFACT:
				return ((InternalEList<?>)getOwnedArtefact()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_REQUIREMENT:
				return ((InternalEList<?>)getOwnedRequirement()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_APPLIC_LEVEL:
				return ((InternalEList<?>)getOwnedApplicLevel()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_CRITIC_LEVEL:
				return ((InternalEList<?>)getOwnedCriticLevel()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ROLE:
				return ((InternalEList<?>)getOwnedRole()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_TECHNIQUE:
				return ((InternalEList<?>)getOwnedTechnique()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_FRAMEWORK__SCOPE:
				return getScope();
			case RefframeworkPackage.REF_FRAMEWORK__REV:
				return getRev();
			case RefframeworkPackage.REF_FRAMEWORK__PURPOSE:
				return getPurpose();
			case RefframeworkPackage.REF_FRAMEWORK__PUBLISHER:
				return getPublisher();
			case RefframeworkPackage.REF_FRAMEWORK__ISSUED:
				return getIssued();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ACTIVITIES:
				return getOwnedActivities();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ARTEFACT:
				return getOwnedArtefact();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_REQUIREMENT:
				return getOwnedRequirement();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_APPLIC_LEVEL:
				return getOwnedApplicLevel();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_CRITIC_LEVEL:
				return getOwnedCriticLevel();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ROLE:
				return getOwnedRole();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_TECHNIQUE:
				return getOwnedTechnique();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_FRAMEWORK__SCOPE:
				setScope((String)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__REV:
				setRev((String)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__PURPOSE:
				setPurpose((String)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__PUBLISHER:
				setPublisher((String)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__ISSUED:
				setIssued((Date)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ACTIVITIES:
				getOwnedActivities().clear();
				getOwnedActivities().addAll((Collection<? extends RefActivity>)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ARTEFACT:
				getOwnedArtefact().clear();
				getOwnedArtefact().addAll((Collection<? extends RefArtefact>)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_REQUIREMENT:
				getOwnedRequirement().clear();
				getOwnedRequirement().addAll((Collection<? extends RefRequirement>)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_APPLIC_LEVEL:
				getOwnedApplicLevel().clear();
				getOwnedApplicLevel().addAll((Collection<? extends RefApplicabilityLevel>)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_CRITIC_LEVEL:
				getOwnedCriticLevel().clear();
				getOwnedCriticLevel().addAll((Collection<? extends RefCriticalityLevel>)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ROLE:
				getOwnedRole().clear();
				getOwnedRole().addAll((Collection<? extends RefRole>)newValue);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_TECHNIQUE:
				getOwnedTechnique().clear();
				getOwnedTechnique().addAll((Collection<? extends RefTechnique>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_FRAMEWORK__SCOPE:
				setScope(SCOPE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__REV:
				setRev(REV_EDEFAULT);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__PURPOSE:
				setPurpose(PURPOSE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__PUBLISHER:
				setPublisher(PUBLISHER_EDEFAULT);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__ISSUED:
				setIssued(ISSUED_EDEFAULT);
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ACTIVITIES:
				getOwnedActivities().clear();
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ARTEFACT:
				getOwnedArtefact().clear();
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_REQUIREMENT:
				getOwnedRequirement().clear();
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_APPLIC_LEVEL:
				getOwnedApplicLevel().clear();
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_CRITIC_LEVEL:
				getOwnedCriticLevel().clear();
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ROLE:
				getOwnedRole().clear();
				return;
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_TECHNIQUE:
				getOwnedTechnique().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_FRAMEWORK__SCOPE:
				return SCOPE_EDEFAULT == null ? getScope() != null : !SCOPE_EDEFAULT.equals(getScope());
			case RefframeworkPackage.REF_FRAMEWORK__REV:
				return REV_EDEFAULT == null ? getRev() != null : !REV_EDEFAULT.equals(getRev());
			case RefframeworkPackage.REF_FRAMEWORK__PURPOSE:
				return PURPOSE_EDEFAULT == null ? getPurpose() != null : !PURPOSE_EDEFAULT.equals(getPurpose());
			case RefframeworkPackage.REF_FRAMEWORK__PUBLISHER:
				return PUBLISHER_EDEFAULT == null ? getPublisher() != null : !PUBLISHER_EDEFAULT.equals(getPublisher());
			case RefframeworkPackage.REF_FRAMEWORK__ISSUED:
				return ISSUED_EDEFAULT == null ? getIssued() != null : !ISSUED_EDEFAULT.equals(getIssued());
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ACTIVITIES:
				return !getOwnedActivities().isEmpty();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ARTEFACT:
				return !getOwnedArtefact().isEmpty();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_REQUIREMENT:
				return !getOwnedRequirement().isEmpty();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_APPLIC_LEVEL:
				return !getOwnedApplicLevel().isEmpty();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_CRITIC_LEVEL:
				return !getOwnedCriticLevel().isEmpty();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_ROLE:
				return !getOwnedRole().isEmpty();
			case RefframeworkPackage.REF_FRAMEWORK__OWNED_TECHNIQUE:
				return !getOwnedTechnique().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RefFrameworkImpl
