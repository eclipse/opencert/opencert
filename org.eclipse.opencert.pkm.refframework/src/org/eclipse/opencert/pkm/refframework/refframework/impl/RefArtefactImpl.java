/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.infra.properties.property.Property;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Artefact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl#getEquivalence <em>Equivalence</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl#getConstrainingRequirement <em>Constraining Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl#getApplicableTechnique <em>Applicable Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl#getProperty <em>Property</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefArtefactImpl extends DescribableElementImpl implements RefArtefact {
	/**
	 * The default value of the '{@link #getReference() <em>Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCE_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefArtefactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_ARTEFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefEquivalenceMap> getEquivalence() {
		return (EList<RefEquivalenceMap>)eDynamicGet(RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE, RefframeworkPackage.Literals.REF_ASSURABLE_ELEMENT__EQUIVALENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReference() {
		return (String)eDynamicGet(RefframeworkPackage.REF_ARTEFACT__REFERENCE, RefframeworkPackage.Literals.REF_ARTEFACT__REFERENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(String newReference) {
		eDynamicSet(RefframeworkPackage.REF_ARTEFACT__REFERENCE, RefframeworkPackage.Literals.REF_ARTEFACT__REFERENCE, newReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefRequirement> getConstrainingRequirement() {
		return (EList<RefRequirement>)eDynamicGet(RefframeworkPackage.REF_ARTEFACT__CONSTRAINING_REQUIREMENT, RefframeworkPackage.Literals.REF_ARTEFACT__CONSTRAINING_REQUIREMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefTechnique> getApplicableTechnique() {
		return (EList<RefTechnique>)eDynamicGet(RefframeworkPackage.REF_ARTEFACT__APPLICABLE_TECHNIQUE, RefframeworkPackage.Literals.REF_ARTEFACT__APPLICABLE_TECHNIQUE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefArtefactRel> getOwnedRel() {
		return (EList<RefArtefactRel>)eDynamicGet(RefframeworkPackage.REF_ARTEFACT__OWNED_REL, RefframeworkPackage.Literals.REF_ARTEFACT__OWNED_REL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Property> getProperty() {
		return (EList<Property>)eDynamicGet(RefframeworkPackage.REF_ARTEFACT__PROPERTY, RefframeworkPackage.Literals.REF_ARTEFACT__PROPERTY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE:
				return ((InternalEList<?>)getEquivalence()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_ARTEFACT__OWNED_REL:
				return ((InternalEList<?>)getOwnedRel()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE:
				return getEquivalence();
			case RefframeworkPackage.REF_ARTEFACT__REFERENCE:
				return getReference();
			case RefframeworkPackage.REF_ARTEFACT__CONSTRAINING_REQUIREMENT:
				return getConstrainingRequirement();
			case RefframeworkPackage.REF_ARTEFACT__APPLICABLE_TECHNIQUE:
				return getApplicableTechnique();
			case RefframeworkPackage.REF_ARTEFACT__OWNED_REL:
				return getOwnedRel();
			case RefframeworkPackage.REF_ARTEFACT__PROPERTY:
				return getProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE:
				getEquivalence().clear();
				getEquivalence().addAll((Collection<? extends RefEquivalenceMap>)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT__REFERENCE:
				setReference((String)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT__CONSTRAINING_REQUIREMENT:
				getConstrainingRequirement().clear();
				getConstrainingRequirement().addAll((Collection<? extends RefRequirement>)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT__APPLICABLE_TECHNIQUE:
				getApplicableTechnique().clear();
				getApplicableTechnique().addAll((Collection<? extends RefTechnique>)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT__OWNED_REL:
				getOwnedRel().clear();
				getOwnedRel().addAll((Collection<? extends RefArtefactRel>)newValue);
				return;
			case RefframeworkPackage.REF_ARTEFACT__PROPERTY:
				getProperty().clear();
				getProperty().addAll((Collection<? extends Property>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE:
				getEquivalence().clear();
				return;
			case RefframeworkPackage.REF_ARTEFACT__REFERENCE:
				setReference(REFERENCE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ARTEFACT__CONSTRAINING_REQUIREMENT:
				getConstrainingRequirement().clear();
				return;
			case RefframeworkPackage.REF_ARTEFACT__APPLICABLE_TECHNIQUE:
				getApplicableTechnique().clear();
				return;
			case RefframeworkPackage.REF_ARTEFACT__OWNED_REL:
				getOwnedRel().clear();
				return;
			case RefframeworkPackage.REF_ARTEFACT__PROPERTY:
				getProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE:
				return !getEquivalence().isEmpty();
			case RefframeworkPackage.REF_ARTEFACT__REFERENCE:
				return REFERENCE_EDEFAULT == null ? getReference() != null : !REFERENCE_EDEFAULT.equals(getReference());
			case RefframeworkPackage.REF_ARTEFACT__CONSTRAINING_REQUIREMENT:
				return !getConstrainingRequirement().isEmpty();
			case RefframeworkPackage.REF_ARTEFACT__APPLICABLE_TECHNIQUE:
				return !getApplicableTechnique().isEmpty();
			case RefframeworkPackage.REF_ARTEFACT__OWNED_REL:
				return !getOwnedRel().isEmpty();
			case RefframeworkPackage.REF_ARTEFACT__PROPERTY:
				return !getProperty().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (derivedFeatureID) {
				case RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE: return RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (baseFeatureID) {
				case RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE: return RefframeworkPackage.REF_ARTEFACT__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //RefArtefactImpl
