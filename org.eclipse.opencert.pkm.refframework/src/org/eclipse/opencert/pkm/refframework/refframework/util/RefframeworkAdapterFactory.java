/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap;
import org.eclipse.opencert.infra.mappings.mapping.Map;
import org.eclipse.opencert.pkm.refframework.refframework.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage
 * @generated
 */
public class RefframeworkAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RefframeworkPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefframeworkAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RefframeworkPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefframeworkSwitch<Adapter> modelSwitch =
		new RefframeworkSwitch<Adapter>() {
			@Override
			public Adapter caseRefFramework(RefFramework object) {
				return createRefFrameworkAdapter();
			}
			@Override
			public Adapter caseRefRequirement(RefRequirement object) {
				return createRefRequirementAdapter();
			}
			@Override
			public Adapter caseRefArtefact(RefArtefact object) {
				return createRefArtefactAdapter();
			}
			@Override
			public Adapter caseRefActivity(RefActivity object) {
				return createRefActivityAdapter();
			}
			@Override
			public Adapter caseRefRequirementRel(RefRequirementRel object) {
				return createRefRequirementRelAdapter();
			}
			@Override
			public Adapter caseRefRole(RefRole object) {
				return createRefRoleAdapter();
			}
			@Override
			public Adapter caseRefApplicabilityLevel(RefApplicabilityLevel object) {
				return createRefApplicabilityLevelAdapter();
			}
			@Override
			public Adapter caseRefCriticalityLevel(RefCriticalityLevel object) {
				return createRefCriticalityLevelAdapter();
			}
			@Override
			public Adapter caseRefTechnique(RefTechnique object) {
				return createRefTechniqueAdapter();
			}
			@Override
			public Adapter caseRefArtefactRel(RefArtefactRel object) {
				return createRefArtefactRelAdapter();
			}
			@Override
			public Adapter caseRefCriticalityApplicability(RefCriticalityApplicability object) {
				return createRefCriticalityApplicabilityAdapter();
			}
			@Override
			public Adapter caseRefActivityRel(RefActivityRel object) {
				return createRefActivityRelAdapter();
			}
			@Override
			public Adapter caseRefAssurableElement(RefAssurableElement object) {
				return createRefAssurableElementAdapter();
			}
			@Override
			public Adapter caseRefIndependencyLevel(RefIndependencyLevel object) {
				return createRefIndependencyLevelAdapter();
			}
			@Override
			public Adapter caseRefRecommendationLevel(RefRecommendationLevel object) {
				return createRefRecommendationLevelAdapter();
			}
			@Override
			public Adapter caseRefControlCategory(RefControlCategory object) {
				return createRefControlCategoryAdapter();
			}
			@Override
			public Adapter caseRefApplicability(RefApplicability object) {
				return createRefApplicabilityAdapter();
			}
			@Override
			public Adapter caseRefApplicabilityRel(RefApplicabilityRel object) {
				return createRefApplicabilityRelAdapter();
			}
			@Override
			public Adapter caseRefEquivalenceMap(RefEquivalenceMap object) {
				return createRefEquivalenceMapAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseDescribableElement(DescribableElement object) {
				return createDescribableElementAdapter();
			}
			@Override
			public Adapter caseMap(Map object) {
				return createMapAdapter();
			}
			@Override
			public Adapter caseEquivalenceMap(EquivalenceMap object) {
				return createEquivalenceMapAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework <em>Ref Framework</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework
	 * @generated
	 */
	public Adapter createRefFrameworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement <em>Ref Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement
	 * @generated
	 */
	public Adapter createRefRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact <em>Ref Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefact
	 * @generated
	 */
	public Adapter createRefArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity <em>Ref Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity
	 * @generated
	 */
	public Adapter createRefActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel <em>Ref Requirement Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel
	 * @generated
	 */
	public Adapter createRefRequirementRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRole <em>Ref Role</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRole
	 * @generated
	 */
	public Adapter createRefRoleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel <em>Ref Applicability Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel
	 * @generated
	 */
	public Adapter createRefApplicabilityLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel <em>Ref Criticality Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel
	 * @generated
	 */
	public Adapter createRefCriticalityLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefTechnique <em>Ref Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefTechnique
	 * @generated
	 */
	public Adapter createRefTechniqueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel <em>Ref Artefact Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel
	 * @generated
	 */
	public Adapter createRefArtefactRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability <em>Ref Criticality Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability
	 * @generated
	 */
	public Adapter createRefCriticalityApplicabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel <em>Ref Activity Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel
	 * @generated
	 */
	public Adapter createRefActivityRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement <em>Ref Assurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement
	 * @generated
	 */
	public Adapter createRefAssurableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefIndependencyLevel <em>Ref Independency Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefIndependencyLevel
	 * @generated
	 */
	public Adapter createRefIndependencyLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRecommendationLevel <em>Ref Recommendation Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRecommendationLevel
	 * @generated
	 */
	public Adapter createRefRecommendationLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefControlCategory <em>Ref Control Category</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefControlCategory
	 * @generated
	 */
	public Adapter createRefControlCategoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicability <em>Ref Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicability
	 * @generated
	 */
	public Adapter createRefApplicabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel <em>Ref Applicability Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel
	 * @generated
	 */
	public Adapter createRefApplicabilityRelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap <em>Ref Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap
	 * @generated
	 */
	public Adapter createRefEquivalenceMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.general.general.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.general.general.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.general.general.DescribableElement <em>Describable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.general.general.DescribableElement
	 * @generated
	 */
	public Adapter createDescribableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.mappings.mapping.Map <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.mappings.mapping.Map
	 * @generated
	 */
	public Adapter createMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap <em>Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap
	 * @generated
	 */
	public Adapter createEquivalenceMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RefframeworkAdapterFactory
