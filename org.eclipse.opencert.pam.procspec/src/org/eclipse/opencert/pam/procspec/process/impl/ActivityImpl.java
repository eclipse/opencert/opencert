/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.ManageableAssuranceAssetImpl;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ActivityRel;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pam.procspec.process.Technique;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getSubActivity <em>Sub Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getPrecedingActivity <em>Preceding Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getParticipant <em>Participant</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getTechnique <em>Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getRequiredArtefact <em>Required Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getProducedArtefact <em>Produced Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ActivityImpl#getAssetEvent <em>Asset Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityImpl extends ManageableAssuranceAssetImpl implements Activity {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date START_TIME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date END_TIME_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcessPackage.Literals.ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eDynamicGet(ProcessPackage.ACTIVITY__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eDynamicSet(ProcessPackage.ACTIVITY__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eDynamicGet(ProcessPackage.ACTIVITY__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eDynamicSet(ProcessPackage.ACTIVITY__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return (String)eDynamicGet(ProcessPackage.ACTIVITY__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		eDynamicSet(ProcessPackage.ACTIVITY__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, newDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartTime() {
		return (Date)eDynamicGet(ProcessPackage.ACTIVITY__START_TIME, ProcessPackage.Literals.ACTIVITY__START_TIME, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Date newStartTime) {
		eDynamicSet(ProcessPackage.ACTIVITY__START_TIME, ProcessPackage.Literals.ACTIVITY__START_TIME, newStartTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEndTime() {
		return (Date)eDynamicGet(ProcessPackage.ACTIVITY__END_TIME, ProcessPackage.Literals.ACTIVITY__END_TIME, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(Date newEndTime) {
		eDynamicSet(ProcessPackage.ACTIVITY__END_TIME, ProcessPackage.Literals.ACTIVITY__END_TIME, newEndTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Activity> getSubActivity() {
		return (EList<Activity>)eDynamicGet(ProcessPackage.ACTIVITY__SUB_ACTIVITY, ProcessPackage.Literals.ACTIVITY__SUB_ACTIVITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Activity> getPrecedingActivity() {
		return (EList<Activity>)eDynamicGet(ProcessPackage.ACTIVITY__PRECEDING_ACTIVITY, ProcessPackage.Literals.ACTIVITY__PRECEDING_ACTIVITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Participant> getParticipant() {
		return (EList<Participant>)eDynamicGet(ProcessPackage.ACTIVITY__PARTICIPANT, ProcessPackage.Literals.ACTIVITY__PARTICIPANT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Technique> getTechnique() {
		return (EList<Technique>)eDynamicGet(ProcessPackage.ACTIVITY__TECHNIQUE, ProcessPackage.Literals.ACTIVITY__TECHNIQUE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ActivityRel> getOwnedRel() {
		return (EList<ActivityRel>)eDynamicGet(ProcessPackage.ACTIVITY__OWNED_REL, ProcessPackage.Literals.ACTIVITY__OWNED_REL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Artefact> getRequiredArtefact() {
		return (EList<Artefact>)eDynamicGet(ProcessPackage.ACTIVITY__REQUIRED_ARTEFACT, ProcessPackage.Literals.ACTIVITY__REQUIRED_ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Artefact> getProducedArtefact() {
		return (EList<Artefact>)eDynamicGet(ProcessPackage.ACTIVITY__PRODUCED_ARTEFACT, ProcessPackage.Literals.ACTIVITY__PRODUCED_ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAssetEvent> getAssetEvent() {
		return (EList<AssuranceAssetEvent>)eDynamicGet(ProcessPackage.ACTIVITY__ASSET_EVENT, ProcessPackage.Literals.ACTIVITY__ASSET_EVENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY__SUB_ACTIVITY:
				return ((InternalEList<?>)getSubActivity()).basicRemove(otherEnd, msgs);
			case ProcessPackage.ACTIVITY__OWNED_REL:
				return ((InternalEList<?>)getOwnedRel()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY__ID:
				return getId();
			case ProcessPackage.ACTIVITY__NAME:
				return getName();
			case ProcessPackage.ACTIVITY__DESCRIPTION:
				return getDescription();
			case ProcessPackage.ACTIVITY__START_TIME:
				return getStartTime();
			case ProcessPackage.ACTIVITY__END_TIME:
				return getEndTime();
			case ProcessPackage.ACTIVITY__SUB_ACTIVITY:
				return getSubActivity();
			case ProcessPackage.ACTIVITY__PRECEDING_ACTIVITY:
				return getPrecedingActivity();
			case ProcessPackage.ACTIVITY__PARTICIPANT:
				return getParticipant();
			case ProcessPackage.ACTIVITY__TECHNIQUE:
				return getTechnique();
			case ProcessPackage.ACTIVITY__OWNED_REL:
				return getOwnedRel();
			case ProcessPackage.ACTIVITY__REQUIRED_ARTEFACT:
				return getRequiredArtefact();
			case ProcessPackage.ACTIVITY__PRODUCED_ARTEFACT:
				return getProducedArtefact();
			case ProcessPackage.ACTIVITY__ASSET_EVENT:
				return getAssetEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY__ID:
				setId((String)newValue);
				return;
			case ProcessPackage.ACTIVITY__NAME:
				setName((String)newValue);
				return;
			case ProcessPackage.ACTIVITY__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ProcessPackage.ACTIVITY__START_TIME:
				setStartTime((Date)newValue);
				return;
			case ProcessPackage.ACTIVITY__END_TIME:
				setEndTime((Date)newValue);
				return;
			case ProcessPackage.ACTIVITY__SUB_ACTIVITY:
				getSubActivity().clear();
				getSubActivity().addAll((Collection<? extends Activity>)newValue);
				return;
			case ProcessPackage.ACTIVITY__PRECEDING_ACTIVITY:
				getPrecedingActivity().clear();
				getPrecedingActivity().addAll((Collection<? extends Activity>)newValue);
				return;
			case ProcessPackage.ACTIVITY__PARTICIPANT:
				getParticipant().clear();
				getParticipant().addAll((Collection<? extends Participant>)newValue);
				return;
			case ProcessPackage.ACTIVITY__TECHNIQUE:
				getTechnique().clear();
				getTechnique().addAll((Collection<? extends Technique>)newValue);
				return;
			case ProcessPackage.ACTIVITY__OWNED_REL:
				getOwnedRel().clear();
				getOwnedRel().addAll((Collection<? extends ActivityRel>)newValue);
				return;
			case ProcessPackage.ACTIVITY__REQUIRED_ARTEFACT:
				getRequiredArtefact().clear();
				getRequiredArtefact().addAll((Collection<? extends Artefact>)newValue);
				return;
			case ProcessPackage.ACTIVITY__PRODUCED_ARTEFACT:
				getProducedArtefact().clear();
				getProducedArtefact().addAll((Collection<? extends Artefact>)newValue);
				return;
			case ProcessPackage.ACTIVITY__ASSET_EVENT:
				getAssetEvent().clear();
				getAssetEvent().addAll((Collection<? extends AssuranceAssetEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY__ID:
				setId(ID_EDEFAULT);
				return;
			case ProcessPackage.ACTIVITY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ProcessPackage.ACTIVITY__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ProcessPackage.ACTIVITY__START_TIME:
				setStartTime(START_TIME_EDEFAULT);
				return;
			case ProcessPackage.ACTIVITY__END_TIME:
				setEndTime(END_TIME_EDEFAULT);
				return;
			case ProcessPackage.ACTIVITY__SUB_ACTIVITY:
				getSubActivity().clear();
				return;
			case ProcessPackage.ACTIVITY__PRECEDING_ACTIVITY:
				getPrecedingActivity().clear();
				return;
			case ProcessPackage.ACTIVITY__PARTICIPANT:
				getParticipant().clear();
				return;
			case ProcessPackage.ACTIVITY__TECHNIQUE:
				getTechnique().clear();
				return;
			case ProcessPackage.ACTIVITY__OWNED_REL:
				getOwnedRel().clear();
				return;
			case ProcessPackage.ACTIVITY__REQUIRED_ARTEFACT:
				getRequiredArtefact().clear();
				return;
			case ProcessPackage.ACTIVITY__PRODUCED_ARTEFACT:
				getProducedArtefact().clear();
				return;
			case ProcessPackage.ACTIVITY__ASSET_EVENT:
				getAssetEvent().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProcessPackage.ACTIVITY__ID:
				return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
			case ProcessPackage.ACTIVITY__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
			case ProcessPackage.ACTIVITY__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? getDescription() != null : !DESCRIPTION_EDEFAULT.equals(getDescription());
			case ProcessPackage.ACTIVITY__START_TIME:
				return START_TIME_EDEFAULT == null ? getStartTime() != null : !START_TIME_EDEFAULT.equals(getStartTime());
			case ProcessPackage.ACTIVITY__END_TIME:
				return END_TIME_EDEFAULT == null ? getEndTime() != null : !END_TIME_EDEFAULT.equals(getEndTime());
			case ProcessPackage.ACTIVITY__SUB_ACTIVITY:
				return !getSubActivity().isEmpty();
			case ProcessPackage.ACTIVITY__PRECEDING_ACTIVITY:
				return !getPrecedingActivity().isEmpty();
			case ProcessPackage.ACTIVITY__PARTICIPANT:
				return !getParticipant().isEmpty();
			case ProcessPackage.ACTIVITY__TECHNIQUE:
				return !getTechnique().isEmpty();
			case ProcessPackage.ACTIVITY__OWNED_REL:
				return !getOwnedRel().isEmpty();
			case ProcessPackage.ACTIVITY__REQUIRED_ARTEFACT:
				return !getRequiredArtefact().isEmpty();
			case ProcessPackage.ACTIVITY__PRODUCED_ARTEFACT:
				return !getProducedArtefact().isEmpty();
			case ProcessPackage.ACTIVITY__ASSET_EVENT:
				return !getAssetEvent().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case ProcessPackage.ACTIVITY__ID: return GeneralPackage.NAMED_ELEMENT__ID;
				case ProcessPackage.ACTIVITY__NAME: return GeneralPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (derivedFeatureID) {
				case ProcessPackage.ACTIVITY__DESCRIPTION: return GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.NAMED_ELEMENT__ID: return ProcessPackage.ACTIVITY__ID;
				case GeneralPackage.NAMED_ELEMENT__NAME: return ProcessPackage.ACTIVITY__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION: return ProcessPackage.ACTIVITY__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ActivityImpl
