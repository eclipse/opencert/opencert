/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedActivity <em>Owned Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedParticipant <em>Owned Participant</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.ProcessModel#getOwnedTechnique <em>Owned Technique</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getProcessModel()
 * @model
 * @generated
 */
public interface ProcessModel extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Owned Activity</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Activity</em>' containment reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getProcessModel_OwnedActivity()
	 * @model containment="true"
	 * @generated
	 */
	EList<Activity> getOwnedActivity();

	/**
	 * Returns the value of the '<em><b>Owned Participant</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.Participant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Participant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Participant</em>' containment reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getProcessModel_OwnedParticipant()
	 * @model containment="true"
	 * @generated
	 */
	EList<Participant> getOwnedParticipant();

	/**
	 * Returns the value of the '<em><b>Owned Technique</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.Technique}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Technique</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Technique</em>' containment reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getProcessModel_OwnedTechnique()
	 * @model containment="true"
	 * @generated
	 */
	EList<Technique> getOwnedTechnique();

} // ProcessModel
