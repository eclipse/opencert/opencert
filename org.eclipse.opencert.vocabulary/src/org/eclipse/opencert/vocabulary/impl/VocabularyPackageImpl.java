/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.opencert.vocabulary.Category;
import org.eclipse.opencert.vocabulary.SourceOfDefinition;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyElement;
import org.eclipse.opencert.vocabulary.VocabularyFactory;
import org.eclipse.opencert.vocabulary.VocabularyPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VocabularyPackageImpl extends EPackageImpl implements VocabularyPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass vocabularyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass categoryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass termEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sourceOfDefinitionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass vocabularyElementEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private VocabularyPackageImpl()
  {
    super(eNS_URI, VocabularyFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link VocabularyPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static VocabularyPackage init()
  {
    if (isInited) return (VocabularyPackage)EPackage.Registry.INSTANCE.getEPackage(VocabularyPackage.eNS_URI);

    // Obtain or create and register package
    VocabularyPackageImpl theVocabularyPackage = (VocabularyPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VocabularyPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VocabularyPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theVocabularyPackage.createPackageContents();

    // Initialize created meta-data
    theVocabularyPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theVocabularyPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(VocabularyPackage.eNS_URI, theVocabularyPackage);
    return theVocabularyPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVocabulary()
  {
    return vocabularyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVocabulary_Terms()
  {
    return (EReference)vocabularyEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVocabulary_Categories()
  {
    return (EReference)vocabularyEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVocabulary_SourcesOfDefinition()
  {
    return (EReference)vocabularyEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCategory()
  {
    return categoryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCategory_Terms()
  {
    return (EReference)categoryEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCategory_SubCategories()
  {
    return (EReference)categoryEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTerm()
  {
    return termEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTerm_Definitions()
  {
    return (EAttribute)termEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTerm_Notes()
  {
    return (EAttribute)termEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTerm_Examples()
  {
    return (EAttribute)termEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTerm_Synonyms()
  {
    return (EAttribute)termEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTerm_DefinedBy()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTerm_IsA()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTerm_HasA()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTerm_RefersTo()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSourceOfDefinition()
  {
    return sourceOfDefinitionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSourceOfDefinition_Uri()
  {
    return (EAttribute)sourceOfDefinitionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVocabularyElement()
  {
    return vocabularyElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVocabularyElement_Name()
  {
    return (EAttribute)vocabularyElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVocabularyElement_Description()
  {
    return (EAttribute)vocabularyElementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VocabularyFactory getVocabularyFactory()
  {
    return (VocabularyFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    vocabularyEClass = createEClass(VOCABULARY);
    createEReference(vocabularyEClass, VOCABULARY__TERMS);
    createEReference(vocabularyEClass, VOCABULARY__CATEGORIES);
    createEReference(vocabularyEClass, VOCABULARY__SOURCES_OF_DEFINITION);

    categoryEClass = createEClass(CATEGORY);
    createEReference(categoryEClass, CATEGORY__TERMS);
    createEReference(categoryEClass, CATEGORY__SUB_CATEGORIES);

    termEClass = createEClass(TERM);
    createEAttribute(termEClass, TERM__DEFINITIONS);
    createEAttribute(termEClass, TERM__NOTES);
    createEAttribute(termEClass, TERM__EXAMPLES);
    createEAttribute(termEClass, TERM__SYNONYMS);
    createEReference(termEClass, TERM__DEFINED_BY);
    createEReference(termEClass, TERM__IS_A);
    createEReference(termEClass, TERM__HAS_A);
    createEReference(termEClass, TERM__REFERS_TO);

    sourceOfDefinitionEClass = createEClass(SOURCE_OF_DEFINITION);
    createEAttribute(sourceOfDefinitionEClass, SOURCE_OF_DEFINITION__URI);

    vocabularyElementEClass = createEClass(VOCABULARY_ELEMENT);
    createEAttribute(vocabularyElementEClass, VOCABULARY_ELEMENT__NAME);
    createEAttribute(vocabularyElementEClass, VOCABULARY_ELEMENT__DESCRIPTION);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    vocabularyEClass.getESuperTypes().add(this.getVocabularyElement());
    categoryEClass.getESuperTypes().add(this.getVocabularyElement());
    termEClass.getESuperTypes().add(this.getVocabularyElement());
    sourceOfDefinitionEClass.getESuperTypes().add(this.getVocabularyElement());

    // Initialize classes and features; add operations and parameters
    initEClass(vocabularyEClass, Vocabulary.class, "Vocabulary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getVocabulary_Terms(), this.getTerm(), null, "terms", null, 0, -1, Vocabulary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getVocabulary_Categories(), this.getCategory(), null, "categories", null, 0, -1, Vocabulary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getVocabulary_SourcesOfDefinition(), this.getSourceOfDefinition(), null, "sourcesOfDefinition", null, 0, -1, Vocabulary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(categoryEClass, Category.class, "Category", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCategory_Terms(), this.getTerm(), null, "terms", null, 0, -1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCategory_SubCategories(), this.getCategory(), null, "subCategories", null, 0, -1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(termEClass, Term.class, "Term", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTerm_Definitions(), ecorePackage.getEString(), "definitions", null, 1, -1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTerm_Notes(), ecorePackage.getEString(), "notes", null, 0, -1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTerm_Examples(), ecorePackage.getEString(), "examples", null, 0, -1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTerm_Synonyms(), ecorePackage.getEString(), "synonyms", null, 0, -1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTerm_DefinedBy(), this.getSourceOfDefinition(), null, "definedBy", null, 0, 1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTerm_IsA(), this.getTerm(), null, "isA", null, 0, -1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTerm_HasA(), this.getTerm(), null, "hasA", null, 0, -1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTerm_RefersTo(), this.getTerm(), null, "refersTo", null, 0, -1, Term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(sourceOfDefinitionEClass, SourceOfDefinition.class, "SourceOfDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSourceOfDefinition_Uri(), ecorePackage.getEString(), "uri", null, 0, 1, SourceOfDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(vocabularyElementEClass, VocabularyElement.class, "VocabularyElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getVocabularyElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, VocabularyElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getVocabularyElement_Description(), ecorePackage.getEString(), "description", null, 0, 1, VocabularyElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // emf.gen
    createEmfAnnotations();
    // gmf
    createGmfAnnotations();
    // gmf.diagram
    createGmf_1Annotations();
    // gmf.node
    createGmf_2Annotations();
    // gmf.link
    createGmf_3Annotations();
    // gmf.label
    createGmf_4Annotations();
  }

  /**
   * Initializes the annotations for <b>emf.gen</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createEmfAnnotations()
  {
    String source = "emf.gen";		
    addAnnotation
      (this, 
       source, 
       new String[] 
       {
       "basePackage", "org.eclipse.opencert"
       });		
    addAnnotation
      (this, 
       source, 
       new String[] 
       {
       "modelDirectory", "org.eclipse.opencert.vocabulary/src-gen"
       });																	
  }

  /**
   * Initializes the annotations for <b>gmf</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmfAnnotations()
  {
    String source = "gmf";				
    addAnnotation
      (this, 
       source, 
       new String[] 
       {
       });																
  }

  /**
   * Initializes the annotations for <b>gmf.diagram</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_1Annotations()
  {
    String source = "gmf.diagram";					
    addAnnotation
      (vocabularyEClass, 
       source, 
       new String[] 
       {
       });															
  }

  /**
   * Initializes the annotations for <b>gmf.node</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_2Annotations()
  {
    String source = "gmf.node";						
    addAnnotation
      (categoryEClass, 
       source, 
       new String[] 
       {
       "label", "name",
       "color", "252,252,230",
       "tool.name", "Category",
       "tool.description", "Create a category for terms.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/famfamfam_silk_icons_v013/icons/folder.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/famfamfam_silk_icons_v013/icons/folder.png",
       "label.icon", "false"
       });				
    addAnnotation
      (termEClass, 
       source, 
       new String[] 
       {
       "label", "name",
       "color", "245,245,245",
       "tool.name", "Term",
       "tool.description", "Create a term.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/gray_text.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/gray_text.png",
       "label.icon", "false"
       });											
    addAnnotation
      (sourceOfDefinitionEClass, 
       source, 
       new String[] 
       {
       "label", "name",
       "color", "240,252,240",
       "tool.name", "Source of Definition",
       "tool.description", "Create a sorce of definitions where the terms were originally defined.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/famfamfam_silk_icons_v013/icons/book_next.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/famfamfam_silk_icons_v013/icons/book_next.png",
       "label.icon", "false"
       });	
  }

  /**
   * Initializes the annotations for <b>gmf.link</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_3Annotations()
  {
    String source = "gmf.link";							
    addAnnotation
      (getCategory_Terms(), 
       source, 
       new String[] 
       {
       "label.text", "categorized term",
       "color", "50,50,50",
       "target.decoration", "arrow",
       "tool.name", "categorized term",
       "tool.description", "Create a connection between a category and a term.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/link.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/link.png"
       });		
    addAnnotation
      (getCategory_SubCategories(), 
       source, 
       new String[] 
       {
       "label.text", "subcategory",
       "color", "50,50,50",
       "target.decoration", "arrow",
       "tool.name", "subcategory",
       "tool.description", "Create a subcategory relationship between two categories.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/link.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/link.png"
       });								
    addAnnotation
      (getTerm_DefinedBy(), 
       source, 
       new String[] 
       {
       "label.text", "defined by",
       "color", "50,50,50",
       "target.decoration", "arrow",
       "tool.name", "defined by",
       "tool.description", "Create a \'defined by\' relationship between a term and its source of definition.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/link.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/link.png"
       });		
    addAnnotation
      (getTerm_IsA(), 
       source, 
       new String[] 
       {
       "label.text", "is a",
       "color", "50,50,50",
       "target.decoration", "arrow",
       "tool.name", "is a",
       "tool.description", "Create a \'is a\' relationship between two terms.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/link.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/link.png"
       });		
    addAnnotation
      (getTerm_HasA(), 
       source, 
       new String[] 
       {
       "label.text", "has a",
       "color", "50,50,50",
       "target.decoration", "arrow",
       "tool.name", "has a",
       "tool.description", "Create a \'has a\' relationship between two terms.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/link.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/link.png"
       });		
    addAnnotation
      (getTerm_RefersTo(), 
       source, 
       new String[] 
       {
       "label.text", "refers to",
       "color", "50,50,50",
       "target.decoration", "arrow",
       "tool.name", "refers to",
       "tool.description", "Create a \'refers to\' relationship between two terms.",
       "tool.small.bundle", "org.eclipse.opencert.vocabulary",
       "tool.small.path", "icons/link.png",
       "tool.large.bundle", "org.eclipse.opencert.vocabulary",
       "tool.large.path", "icons/link.png"
       });		
  }

  /**
   * Initializes the annotations for <b>gmf.label</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_4Annotations()
  {
    String source = "gmf.label";											
    addAnnotation
      (getTerm_Definitions(), 
       source, 
       new String[] 
       {
       });										
    addAnnotation
      (getSourceOfDefinition_Uri(), 
       source, 
       new String[] 
       {
       });
  }

} //VocabularyPackageImpl
