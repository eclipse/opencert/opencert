/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.opencert.vocabulary.Category;
import org.eclipse.opencert.vocabulary.SourceOfDefinition;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vocabulary</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.VocabularyImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.VocabularyImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.VocabularyImpl#getTerms <em>Terms</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.VocabularyImpl#getCategories <em>Categories</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.impl.VocabularyImpl#getSourcesOfDefinition <em>Sources Of Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VocabularyImpl extends EObjectImpl implements Vocabulary
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getTerms() <em>Terms</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTerms()
   * @generated
   * @ordered
   */
  protected EList<Term> terms;

  /**
   * The cached value of the '{@link #getCategories() <em>Categories</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCategories()
   * @generated
   * @ordered
   */
  protected EList<Category> categories;

  /**
   * The cached value of the '{@link #getSourcesOfDefinition() <em>Sources Of Definition</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSourcesOfDefinition()
   * @generated
   * @ordered
   */
  protected EList<SourceOfDefinition> sourcesOfDefinition;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VocabularyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return VocabularyPackage.Literals.VOCABULARY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, VocabularyPackage.VOCABULARY__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, VocabularyPackage.VOCABULARY__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Term> getTerms()
  {
    if (terms == null)
    {
      terms = new EObjectContainmentEList<Term>(Term.class, this, VocabularyPackage.VOCABULARY__TERMS);
    }
    return terms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Category> getCategories()
  {
    if (categories == null)
    {
      categories = new EObjectContainmentEList<Category>(Category.class, this, VocabularyPackage.VOCABULARY__CATEGORIES);
    }
    return categories;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SourceOfDefinition> getSourcesOfDefinition()
  {
    if (sourcesOfDefinition == null)
    {
      sourcesOfDefinition = new EObjectContainmentEList<SourceOfDefinition>(SourceOfDefinition.class, this, VocabularyPackage.VOCABULARY__SOURCES_OF_DEFINITION);
    }
    return sourcesOfDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case VocabularyPackage.VOCABULARY__TERMS:
        return ((InternalEList<?>)getTerms()).basicRemove(otherEnd, msgs);
      case VocabularyPackage.VOCABULARY__CATEGORIES:
        return ((InternalEList<?>)getCategories()).basicRemove(otherEnd, msgs);
      case VocabularyPackage.VOCABULARY__SOURCES_OF_DEFINITION:
        return ((InternalEList<?>)getSourcesOfDefinition()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case VocabularyPackage.VOCABULARY__NAME:
        return getName();
      case VocabularyPackage.VOCABULARY__DESCRIPTION:
        return getDescription();
      case VocabularyPackage.VOCABULARY__TERMS:
        return getTerms();
      case VocabularyPackage.VOCABULARY__CATEGORIES:
        return getCategories();
      case VocabularyPackage.VOCABULARY__SOURCES_OF_DEFINITION:
        return getSourcesOfDefinition();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case VocabularyPackage.VOCABULARY__NAME:
        setName((String)newValue);
        return;
      case VocabularyPackage.VOCABULARY__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case VocabularyPackage.VOCABULARY__TERMS:
        getTerms().clear();
        getTerms().addAll((Collection<? extends Term>)newValue);
        return;
      case VocabularyPackage.VOCABULARY__CATEGORIES:
        getCategories().clear();
        getCategories().addAll((Collection<? extends Category>)newValue);
        return;
      case VocabularyPackage.VOCABULARY__SOURCES_OF_DEFINITION:
        getSourcesOfDefinition().clear();
        getSourcesOfDefinition().addAll((Collection<? extends SourceOfDefinition>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case VocabularyPackage.VOCABULARY__NAME:
        setName(NAME_EDEFAULT);
        return;
      case VocabularyPackage.VOCABULARY__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case VocabularyPackage.VOCABULARY__TERMS:
        getTerms().clear();
        return;
      case VocabularyPackage.VOCABULARY__CATEGORIES:
        getCategories().clear();
        return;
      case VocabularyPackage.VOCABULARY__SOURCES_OF_DEFINITION:
        getSourcesOfDefinition().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case VocabularyPackage.VOCABULARY__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case VocabularyPackage.VOCABULARY__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case VocabularyPackage.VOCABULARY__TERMS:
        return terms != null && !terms.isEmpty();
      case VocabularyPackage.VOCABULARY__CATEGORIES:
        return categories != null && !categories.isEmpty();
      case VocabularyPackage.VOCABULARY__SOURCES_OF_DEFINITION:
        return sourcesOfDefinition != null && !sourcesOfDefinition.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", description: ");
    result.append(description);
    result.append(')');
    return result.toString();
  }

} //VocabularyImpl
