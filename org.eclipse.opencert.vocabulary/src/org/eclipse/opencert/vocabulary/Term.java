/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getDefinitions <em>Definitions</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getNotes <em>Notes</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getExamples <em>Examples</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getSynonyms <em>Synonyms</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getDefinedBy <em>Defined By</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getIsA <em>Is A</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getHasA <em>Has A</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Term#getRefersTo <em>Refers To</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm()
 * @model annotation="gmf.node label='name' color='245,245,245' tool.name='Term' tool.description='Create a term.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/gray_text.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/gray_text.png' label.icon='false'"
 * @generated
 */
public interface Term extends VocabularyElement
{
  /**
   * Returns the value of the '<em><b>Definitions</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * The definition or the definitions of the lemma.
   * <!-- end-model-doc -->
   * @return the value of the '<em>Definitions</em>' attribute list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_Definitions()
   * @model required="true"
   * @generated
   */
  EList<String> getDefinitions();

  /**
   * Returns the value of the '<em><b>Notes</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * Additional notes beyond the definitions to help understand the meaning of the word.
   * <!-- end-model-doc -->
   * @return the value of the '<em>Notes</em>' attribute list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_Notes()
   * @model
   * @generated
   */
  EList<String> getNotes();

  /**
   * Returns the value of the '<em><b>Examples</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * Examples which show how the lemma is used in a context.
   * <!-- end-model-doc -->
   * @return the value of the '<em>Examples</em>' attribute list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_Examples()
   * @model
   * @generated
   */
  EList<String> getExamples();

  /**
   * Returns the value of the '<em><b>Synonyms</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * Different words which have the same meaning as the lemma.
   * <!-- end-model-doc -->
   * @return the value of the '<em>Synonyms</em>' attribute list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_Synonyms()
   * @model
   * @generated
   */
  EList<String> getSynonyms();

  /**
   * Returns the value of the '<em><b>Defined By</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Defined By</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Defined By</em>' reference.
   * @see #setDefinedBy(SourceOfDefinition)
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_DefinedBy()
   * @model annotation="gmf.link label.text='defined by' color='50,50,50' target.decoration='arrow' tool.name='defined by' tool.description='Create a \'defined by\' relationship between a term and its source of definition.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/link.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/link.png'"
   * @generated
   */
  SourceOfDefinition getDefinedBy();

  /**
   * Sets the value of the '{@link org.eclipse.opencert.vocabulary.Term#getDefinedBy <em>Defined By</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Defined By</em>' reference.
   * @see #getDefinedBy()
   * @generated
   */
  void setDefinedBy(SourceOfDefinition value);

  /**
   * Returns the value of the '<em><b>Is A</b></em>' reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.Term}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Is A</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Is A</em>' reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_IsA()
   * @model annotation="gmf.link label.text='is a' color='50,50,50' target.decoration='arrow' tool.name='is a' tool.description='Create a \'is a\' relationship between two terms.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/link.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/link.png'"
   * @generated
   */
  EList<Term> getIsA();

  /**
   * Returns the value of the '<em><b>Has A</b></em>' reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.Term}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Has A</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Has A</em>' reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_HasA()
   * @model annotation="gmf.link label.text='has a' color='50,50,50' target.decoration='arrow' tool.name='has a' tool.description='Create a \'has a\' relationship between two terms.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/link.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/link.png'"
   * @generated
   */
  EList<Term> getHasA();

  /**
   * Returns the value of the '<em><b>Refers To</b></em>' reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.Term}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Refers To</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Refers To</em>' reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getTerm_RefersTo()
   * @model annotation="gmf.link label.text='refers to' color='50,50,50' target.decoration='arrow' tool.name='refers to' tool.description='Create a \'refers to\' relationship between two terms.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/link.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/link.png'"
   * @generated
   */
  EList<Term> getRefersTo();

} // Term
