/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Of Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.vocabulary.SourceOfDefinition#getUri <em>Uri</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getSourceOfDefinition()
 * @model annotation="gmf.node label='name' color='240,252,240' tool.name='Source of Definition' tool.description='Create a sorce of definitions where the terms were originally defined.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/famfamfam_silk_icons_v013/icons/book_next.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/famfamfam_silk_icons_v013/icons/book_next.png' label.icon='false'"
 * @generated
 */
public interface SourceOfDefinition extends VocabularyElement
{
  /**
   * Returns the value of the '<em><b>Uri</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uri</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uri</em>' attribute.
   * @see #setUri(String)
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getSourceOfDefinition_Uri()
   * @model
   * @generated
   */
  String getUri();

  /**
   * Sets the value of the '{@link org.eclipse.opencert.vocabulary.SourceOfDefinition#getUri <em>Uri</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uri</em>' attribute.
   * @see #getUri()
   * @generated
   */
  void setUri(String value);

} // SourceOfDefinition
