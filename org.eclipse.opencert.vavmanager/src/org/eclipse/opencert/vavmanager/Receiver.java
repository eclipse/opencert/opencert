/*******************************************************************************
 * Copyright (c) 2017 Honeywell, Inc.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Vit Koksa  <Vit.Koksa@honeywell.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.vavmanager;

import java.io.BufferedReader;
//import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Locale;

import org.eclipse.jface.dialogs.MessageDialog;

/**
 * @author Vit Koksa
 * The Receiver shall monitor the status of verification and, most importantly, it shall collect the results of verification and validation.
 */
public class Receiver {
	
	InputStream inputStream = null;
	
	Receiver(){
	}
	
	public String receive(HttpURLConnection conn) {
		int responseCode;
		String responseMessage;
		String result = null;
		StringBuffer sBuf = new StringBuffer();
		try {
			responseCode = conn.getResponseCode();
			responseMessage = conn.getResponseMessage();
			String line = null;
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader bufferedReader = null;
				inputStream = conn.getInputStream();
				String charset = getCharsetFromConnection(conn);
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset));
				while ((line = bufferedReader.readLine()) != null) {
					//result += "\n" + line;
					sBuf.append("\n" + line);
				}
			} else {
				MessageDialog.openWarning(VAndVManagerCommand.shell, "Unexpected HTTP response from the Verification server.", 
						responseCode + " : " + responseMessage);
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			result = sBuf.toString();
			return result;
		}
		result = sBuf.toString();
		return result;
	}
	
	String getCharsetFromConnection(HttpURLConnection connection) {
		//String contentType = connection.getHeaderField("Content-Type");
		String contentType = connection.getRequestProperty("Content-Type");
		//String contentType = connection.getContentType();
		String[] values = contentType.split(";"); // values.length should be 2
		String charset = "";

		for (String value : values) {
		    value = value.trim();

		    if (value.toLowerCase(Locale.ENGLISH).startsWith("charset=")) {
		        charset = value.substring("charset=".length());
		    }
		}

		if ("".equals(charset)) {
		    charset = "UTF-8"; //Assumption
		}
		return charset;
	}
}
