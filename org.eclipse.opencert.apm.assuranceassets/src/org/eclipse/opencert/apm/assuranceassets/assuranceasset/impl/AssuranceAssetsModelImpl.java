/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetsModel;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assurance Assets Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetsModelImpl#getAssuranceAsset <em>Assurance Asset</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssuranceAssetsModelImpl extends DescribableElementImpl implements AssuranceAssetsModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssuranceAssetsModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssuranceassetPackage.Literals.ASSURANCE_ASSETS_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAsset> getAssuranceAsset() {
		return (EList<AssuranceAsset>)eGet(AssuranceassetPackage.Literals.ASSURANCE_ASSETS_MODEL__ASSURANCE_ASSET, true);
	}

} //AssuranceAssetsModelImpl
