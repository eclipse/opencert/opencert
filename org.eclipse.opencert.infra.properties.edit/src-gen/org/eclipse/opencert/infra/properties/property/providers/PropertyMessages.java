/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.properties.property.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class PropertyMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.infra.properties.property.providers.propertyMessages"; //$NON-NLS-1$

	
	public static String PropertyModelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertyPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String PropertyModel_ReadOnly;

	
	public static String PropertyModel_Part_Title;

	
	public static String Property_ReadOnly;

	
	public static String Property_Part_Title;


	
	public static String PropertyModelPropertiesEditionPart_IdLabel;

	
	public static String PropertyModelPropertiesEditionPart_NameLabel;

	
	public static String PropertyModelPropertiesEditionPart_DescriptionLabel;

	
	public static String PropertyModelPropertiesEditionPart_HasPropertyLabel;

	
	public static String PropertyPropertiesEditionPart_IdLabel;

	
	public static String PropertyPropertiesEditionPart_NameLabel;

	
	public static String PropertyPropertiesEditionPart_DatatypeLabel;

	
	public static String PropertyPropertiesEditionPart_EnumValuesLabel;

	
	public static String PropertyPropertiesEditionPart_UnitLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, PropertyMessages.class);
	}

	
	private PropertyMessages() {
		//protect instanciation
	}
}
