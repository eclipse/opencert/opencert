/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.properties.property.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;
import org.eclipse.opencert.infra.properties.property.parts.PropertyViewsRepository;
import org.eclipse.opencert.infra.properties.property.parts.forms.PropertyModelPropertiesEditionPartForm;
import org.eclipse.opencert.infra.properties.property.parts.forms.PropertyPropertiesEditionPartForm;
import org.eclipse.opencert.infra.properties.property.parts.impl.PropertyModelPropertiesEditionPartImpl;
import org.eclipse.opencert.infra.properties.property.parts.impl.PropertyPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class PropertyPropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == PropertyViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == PropertyViewsRepository.PropertyModel.class) {
			if (kind == PropertyViewsRepository.SWT_KIND)
				return new PropertyModelPropertiesEditionPartImpl(component);
			if (kind == PropertyViewsRepository.FORM_KIND)
				return new PropertyModelPropertiesEditionPartForm(component);
		}
		if (key == PropertyViewsRepository.Property_.class) {
			if (kind == PropertyViewsRepository.SWT_KIND)
				return new PropertyPropertiesEditionPartImpl(component);
			if (kind == PropertyViewsRepository.FORM_KIND)
				return new PropertyPropertiesEditionPartForm(component);
		}
		return null;
	}

}
