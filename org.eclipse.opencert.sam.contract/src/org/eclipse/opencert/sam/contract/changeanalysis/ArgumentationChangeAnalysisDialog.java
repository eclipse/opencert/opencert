/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.contract.changeanalysis;

import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.layout.GridData;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.contract.wizards.CDOResourceListLabelProvider;
import org.eclipse.opencert.sam.contract.wizards.ContractModelBuilder;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class ArgumentationChangeAnalysisDialog extends Dialog {
	
	private CDOView view;
	private ArrayList refList = new ArrayList();
	private ArrayList<String> refListDir = new ArrayList();
	private ComboViewer comboViewerSelectArgModel;

	
	public ArgumentationChangeAnalysisDialog(Shell parentShell) {
		super(parentShell);
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
		view = CDOConnectionUtil.instance.openView(session);
		
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));
		
		Label lblSelectASystem = new Label(container, SWT.NONE);
		lblSelectASystem.setText("Select a system integration argumentation model");
		
		comboViewerSelectArgModel = new ComboViewer(container, SWT.NONE);
		Combo combo = comboViewerSelectArgModel.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		comboViewerSelectArgModel.setContentProvider(new ArrayContentProvider());
		comboViewerSelectArgModel.setLabelProvider(new CDOResourceListLabelProvider());
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Searching for argumentation files", 20);

		try {			
			populateArgModels();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		monitor.worked(1);
		dialog.close();
		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				ContractModelBuilder cBuilder = new ContractModelBuilder();
				ArrayList results  = cBuilder.argumentModuleChangeImpactAnalysis(((CDOResource)((IStructuredSelection)comboViewerSelectArgModel.getSelection()).getFirstElement()), view, ArgumentationChangeAnalysisDialog.this.getParentShell() );
				if(results.size() == 0)
				{
					MessageDialog dialog = new MessageDialog(ArgumentationChangeAnalysisDialog.this.getParentShell(), "Argumentation Change Analysis", null,
							"No Changes Detected", MessageDialog.INFORMATION, new String[] { "OK",}, 0);
						int result = dialog.open();
						ArgumentationChangeAnalysisDialog.this.close();
				}
				else
				{
					MessageDialog dialog = new MessageDialog(ArgumentationChangeAnalysisDialog.this.getParentShell(), "Argumentation Change Analysis", null,
							"Changes Detected  ", MessageDialog.ERROR, new String[] { "OK",}, 0);
						int result = dialog.open();
						AnalysisResultsDialog resultsDialog = new AnalysisResultsDialog(ArgumentationChangeAnalysisDialog.this.getParentShell(), results);
						int result2 = resultsDialog.open();
						ArgumentationChangeAnalysisDialog.this.close();
				}
			}
		});
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(450, 300);
	}
	

	public void populateArgModels()
	{
		addFilesOfDir();
		setArgModelListInput(refList.toArray());
	}
	
	public int addFilesOfDir()
	{      
		  int sumStd=0;
		  CDOResourceNode[] listR=  view.getElements();   
	      for (int i=0; i<listR.length; i++){
	            if(listR[i] instanceof CDOResourceFolder){
	                  checkFolderContents((CDOResourceFolder)listR[i],sumStd);
	            }
	            else if (listR[i].getName().endsWith(".arg")){
	            	refList.add(listR[i]);
	                refListDir.add(listR[i].getPath());
	                sumStd++;
	            }                      
	      }
	      return sumStd;
	}
	
	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, int sumStd) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
			for (int i=0; i<listN.size(); i++){
				if(listN.get(i) instanceof CDOResourceFolder){
					checkFolderContents((CDOResourceFolder)listN.get(i),sumStd);
		        }
				else if (listN.get(i).getName().endsWith(".arg")){
					refList.add(listN.get(i));
		            refListDir.add(listN.get(i).getPath());
		            sumStd++;
		        }
		   }
		}
	}
	
	public void setArgModelListInput(Object[] cdoResource)
	{
		comboViewerSelectArgModel.setInput(cdoResource);
		comboViewerSelectArgModel.setSorter(new ViewerSorter() {
			public int compare(
					Viewer viewer, Object c1, Object c2) {
						return ((CDOResource) c1).getPath().compareToIgnoreCase(((CDOResource) c2).getPath());
					}
			});
	}
}
