/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import java.util.ArrayList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedContext;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.CitationElementType;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;

public class ContractModelBuilder {
	
	private ArgPackage argPackage = ArgPackage.eINSTANCE;
	private ArgFactory argFactory = argPackage.getArgFactory();
	CDOResourceFolder projectFolder;
	
	public void createDiagram(URI modelURI, URI diagramURI, IProgressMonitor monitor, Case assuranceCase)
	{
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOTransaction transaction = sessionCDO.openTransaction();
	
		try
		{
			String modelResourceStr = modelURI.toString().substring("cdo://opencert".length(), modelURI.toString().length());
			String modelResourcePathStr = modelResourceStr.substring(0,modelResourceStr.lastIndexOf('/'));
			String diagramResourceStr = diagramURI.toString().substring("dawn://opencert".length(), diagramURI.toString().length());
		
			if(transaction.hasResource(modelResourceStr) == true)
			{
				CDOResource targetResource  = transaction.getResource(modelResourceStr);
				targetResource.delete(null);
			}
		
			if(transaction.hasResource(diagramResourceStr) == true)
			{
				CDOResource targetModelDiagramResource  = transaction.getResource(diagramResourceStr);
				targetModelDiagramResource.delete(null);
			}
		
			CDOResource modelResource = transaction.createResource(modelResourceStr);
			modelResource.getContents().add(assuranceCase);
			CDOResource targetModelDiagramResource  = transaction.createResource(diagramResourceStr);
        
			synchronized (transaction)
			{
				projectFolder=transaction.getOrCreateResourceFolder(modelResourcePathStr);
				DawnArgDiagramUtil myDiagram = new DawnArgDiagramUtil(modelResource.getURI(), targetModelDiagramResource.getURI(), projectFolder, transaction);
				myDiagram.generateDiagram(monitor);
				myDiagram.openDiagram(targetModelDiagramResource);
			}
			transaction.commit();
        
		}
		catch(Exception exc) {exc.printStackTrace();}
	}
	
	public String formatInheritedContext(EObject element, CDOResource cdoResource, boolean topDown, boolean rootElement)
	{
		String contextAssociationsString = "";
		ArrayList<ContextAssociation> contextAssociations = computeInheritedContext(element, cdoResource, topDown, rootElement);
		for(int i = 0; i < contextAssociations.size(); i++)
		{
			ContextAssociation ca = contextAssociations.get(i);
			contextAssociationsString = contextAssociationsString + ca.getSource().getId() + " " + ca.getSource().getDescription() +  " --> " + ca.getTarget().getId() + " " + ca.getTarget().getDescription() + " " + "\n";
		}
		return contextAssociationsString;
	}
	
	public  ArrayList<ContextAssociation> computeInheritedContext(EObject element, CDOResource cdoResource, boolean topDown, boolean rootElement)
	{
		ArrayList<ContextAssociation> contextAssociations2 = new ArrayList<ContextAssociation>();
		TreeIterator modelTree = cdoResource.eAllContents();
		while(modelTree.hasNext())
		{
			EObject modelElement = (EObject) modelTree.next();
			if(modelElement.eClass().getName() =="AssertedInference")
			{
				EList sourceList = ((AssertedInference)modelElement).getSource();
				ArgumentationElement source = (ArgumentationElement) sourceList.get(0);
				EList targetList = ((AssertedInference)modelElement).getTarget();
				ArgumentationElement target = (ArgumentationElement) targetList.get(0);
				ArgumentationElement toTraverse;
				ArgumentationElement toMatch;
				if(topDown == true)
				{
					toTraverse = target;
					toMatch = source;
				}
				else
				{
					toTraverse = source;
					toMatch = target;
				}
				
				if(toMatch == element)
				{
					ArrayList contextTraverse = hasContext(toTraverse,cdoResource);
					ArrayList justificationTraverse = hasJustification(toTraverse,cdoResource);
					ArrayList assumptionTraverse = hasAssumption(toTraverse,cdoResource);
					
					if(rootElement == true)
					{
						ArrayList contextMatch = hasContext(toMatch,cdoResource);
						if(contextMatch != null)
						{
							contextAssociations2.addAll(contextMatch);
						}
						ArrayList justificationMatch = hasJustification(toMatch,cdoResource);
						if(justificationMatch != null)
						{
							contextAssociations2.addAll(justificationMatch);
						}
						ArrayList assumptionMatch = hasAssumption(toMatch,cdoResource);
						if(assumptionMatch != null)
						{
							contextAssociations2.addAll(assumptionMatch);
						}
						
					}
					if(contextTraverse != null)
					{
						contextAssociations2.addAll(contextTraverse);
						
					}
					if(justificationTraverse != null)
					{
						contextAssociations2.addAll(justificationTraverse);
					}
					
					if(assumptionTraverse != null)
					{
						contextAssociations2.addAll(assumptionTraverse);
					}
					contextAssociations2.addAll(computeInheritedContext(toTraverse, cdoResource, topDown, false));
					
				}
				
			}
		}
		return contextAssociations2;
	}
	
	
	public ArrayList hasContext(ArgumentationElement element,CDOResource cdoResource)
	{
		ArrayList results = new ArrayList();
		TreeIterator modelTree = cdoResource.eAllContents();
		while(modelTree.hasNext())
		{
			EObject modelElement = (EObject) modelTree.next();
			if(modelElement.eClass().getName() =="AssertedContext")
			{
				EList sourceList = ((AssertedContext)modelElement).getSource();
				ArgumentationElement source = (ArgumentationElement) sourceList.get(0);
				EList targetList = ((AssertedContext)modelElement).getTarget();
				ArgumentationElement target = (ArgumentationElement) targetList.get(0);
				if(source == element)
				{
					if(target.eClass().getName() == "InformationElementCitation")
					{
						if(((InformationElementCitation)target).getType() == InformationElementType.CONTEXT)
						{
							ContextAssociation ca = new ContextAssociation(element, target);
							results.add(ca);
						}
					}
					
				}
			}
		}
		if(results.size() == 0)
		{
			return null;
		}
		else
		{
			return results;
		}
	}
	
	public ArrayList hasJustification(ArgumentationElement element,CDOResource cdoResource)
	{
		ArrayList results = new ArrayList();
		TreeIterator modelTree = cdoResource.eAllContents();
		while(modelTree.hasNext())
		{
			EObject modelElement = (EObject) modelTree.next();
			if(modelElement.eClass().getName() =="AssertedContext")
			{
				EList sourceList = ((AssertedContext)modelElement).getSource();
				ArgumentationElement source = (ArgumentationElement) sourceList.get(0);
				EList targetList = ((AssertedContext)modelElement).getTarget();
				ArgumentationElement target = (ArgumentationElement) targetList.get(0);
				if(source == element)
				{
					if(target.eClass().getName() == "InformationElementCitation")
					{
						if(((InformationElementCitation)target).getType() == InformationElementType.JUSTIFICATION)
						{
							ContextAssociation ca = new ContextAssociation(element, target);
							results.add(ca);
						}
					}
					
				}
			}
		}
		if(results.size() == 0)
		{
			return null;
		}
		else
		{
			return results;
		}
	}
	
	public ArrayList hasAssumption(ArgumentationElement element,CDOResource cdoResource)
	{
		ArrayList results = new ArrayList();
		TreeIterator modelTree = cdoResource.eAllContents();
		while(modelTree.hasNext())
		{
			EObject modelElement = (EObject) modelTree.next();
			if(modelElement.eClass().getName() =="AssertedInference")
			{
				EList sourceList = ((AssertedInference)modelElement).getSource();
				ArgumentationElement source = (ArgumentationElement) sourceList.get(0);
				EList targetList = ((AssertedInference)modelElement).getTarget();
				ArgumentationElement target = (ArgumentationElement) targetList.get(0);
				if(source == element)
				{
					if(target.eClass().getName() == "Claim")
					{
						if(((Claim)target).getAssumed() == true)
						{
							ContextAssociation ca = new ContextAssociation(element, target);
							results.add(ca);
						}
					}	
				}
			}
		}
		if(results.size() == 0)
		{
			return null;
		}
		else
		{
			return results;
		}
	}
	
	public Case buildContractModel(ArrayList<ContractModelElement> requires, ArrayList<ContractModelElement> provides, String justification)
	{
		String requiresGoalID = ((Claim)requires.get(0).getModelElement()).getId();
		String requiresGoalFullyQualifiedName = ((Claim)requires.get(0).getModelElement()).getName();
		String requiresGoalDescr = ((Claim)requires.get(0).getModelElement()).getDescription();
		String requiresGoalModule = ((Argumentation)requires.get(0).getEnclosingModule()).getId();
		
		Case assuranceCase = argFactory.createCase();
		Argumentation argModule = argFactory.createArgumentation();
		argModule.setId("ContractToResolve " + requiresGoalModule + "-" + requiresGoalID);
		assuranceCase.getArgumentation().add(argModule);
		
		ArgumentElementCitation awayGoalRequiringSupport = argFactory.createArgumentElementCitation();
		awayGoalRequiringSupport.setId(requiresGoalID);
		awayGoalRequiringSupport.setName(requires.get(0).getCdoResource().getPath() + "/" + requiresGoalID);
		awayGoalRequiringSupport.setDescription(requiresGoalDescr);
		awayGoalRequiringSupport.setArgumentationReference(requiresGoalModule);
		
		assuranceCase.getArgument().add(awayGoalRequiringSupport);
		argModule.getConsistOf().add(awayGoalRequiringSupport);
		
		InformationElementCitation requiredContext = argFactory.createInformationElementCitation();
		requiredContext.setType(InformationElementType.CONTEXT);
		requiredContext.setId("C1 Inherited context associations of " + requiresGoalID);
		
		String requiresContextString = formatInheritedContext(requires.get(0).getModelElement(), requires.get(0).getCdoResource(), false, true);
		if(requiresContextString != "")
		{
			requiredContext.setDescription(requiresContextString);
			assuranceCase.getArgument().add(requiredContext);
			argModule.getConsistOf().add(requiredContext);
			createLink(awayGoalRequiringSupport, requiredContext, assuranceCase, "AssertedContext");
		}
	
		
		ArgumentReasoning strategyProvidingGoals = argFactory.createArgumentReasoning();
		strategyProvidingGoals.setId("St1");
		strategyProvidingGoals.setDescription("Argument over goals/solutions from other modules providing support");
		assuranceCase.getArgument().add(strategyProvidingGoals);
		argModule.getConsistOf().add(strategyProvidingGoals);
		
		createLink(awayGoalRequiringSupport, strategyProvidingGoals, assuranceCase, "AssertedInference");
		
		InformationElementCitation strategyContext = argFactory.createInformationElementCitation();
		strategyContext.setType(InformationElementType.CONTEXT);
		strategyContext.setId("C2");
		strategyContext.setDescription("Goals/solutions identified in the contract to support " + requiresGoalID);
		assuranceCase.getArgument().add(strategyContext);
		argModule.getConsistOf().add(strategyContext);
		
		createLink(strategyProvidingGoals, strategyContext, assuranceCase, "AssertedContext");
		
		InformationElementCitation strategyJustification = argFactory.createInformationElementCitation();
		strategyJustification.setType(InformationElementType.JUSTIFICATION);
		strategyJustification.setId("J1");
		if(justification != "")
		{
			strategyJustification.setDescription(justification);
			assuranceCase.getArgument().add(strategyJustification);
			argModule.getConsistOf().add(strategyJustification);
			createLink(strategyProvidingGoals, strategyJustification, assuranceCase, "AssertedInference");
		}
		
		for(int k = 0; k < provides.size(); k++)
		{
			ContractModelElement cme2 = provides.get(k);
			EObject module2 = cme2.getEnclosingModule();
			String type = cme2.getModelElement().eClass().getName();
			String id = (String) cme2.getModelElement().eGet(cme2.getModelElement().eClass().getEStructuralFeature("id"));
			String description = (String) cme2.getModelElement().eGet(cme2.getModelElement().eClass().getEStructuralFeature("description"));
			String modRef = (String) cme2.getEnclosingModule().eGet(cme2.getModelElement().eClass().getEStructuralFeature("id"));
			InformationElementCitation providesContext = argFactory.createInformationElementCitation();
			providesContext.setType(InformationElementType.CONTEXT);
			providesContext.setId("C" + String.valueOf(k+3) + " Inherited context associations of " + id);
			String contextAssociationString = formatInheritedContext(cme2.getModelElement(),cme2.getCdoResource(),true, true);
			providesContext.setDescription(contextAssociationString);
		
			
			if(type.equals("Claim"))
			{
				ArgumentElementCitation awayGoalProvidingSupport = argFactory.createArgumentElementCitation();
				awayGoalProvidingSupport.setId(id);
				awayGoalProvidingSupport.setName(cme2.getCdoResource().getPath() + "/" + id);
				awayGoalProvidingSupport.setCitedType(CitationElementType.CLAIM);
				awayGoalProvidingSupport.setDescription(description);
				awayGoalProvidingSupport.setArgumentationReference(modRef);
				assuranceCase.getArgument().add(awayGoalProvidingSupport);
				argModule.getConsistOf().add(awayGoalProvidingSupport);
				
				createLink(strategyProvidingGoals,awayGoalProvidingSupport, assuranceCase, "AssertedInference");
				if(contextAssociationString != "")
				{
					assuranceCase.getArgument().add(providesContext);
					argModule.getConsistOf().add(providesContext);
					createLink(awayGoalProvidingSupport, providesContext, assuranceCase, "AssertedContext");
				}
			}
			else if(type.equals("InformationElementCitation"))
			{
				ArgumentElementCitation awaySolutionProvidingSupport = argFactory.createArgumentElementCitation();
				awaySolutionProvidingSupport.setId(id);
				awaySolutionProvidingSupport.setDescription(description);
				awaySolutionProvidingSupport.setName(cme2.getCdoResource().getPath() + "/" + id);
				awaySolutionProvidingSupport.setArgumentationReference(modRef);
				awaySolutionProvidingSupport.setCitedType(CitationElementType.SOLUTION);
				assuranceCase.getArgument().add(awaySolutionProvidingSupport);
				argModule.getConsistOf().add(awaySolutionProvidingSupport);
				createLink(strategyProvidingGoals, awaySolutionProvidingSupport, assuranceCase, "AssertedInference");
				if(contextAssociationString != "")
				{
					assuranceCase.getArgument().add(providesContext);
					argModule.getConsistOf().add(providesContext);
					createLink(awaySolutionProvidingSupport, providesContext, assuranceCase, "AssertedContext");
				}
			}

				
		}
		return assuranceCase;
	}
	
	public void createLink(EObject sourceObject, EObject targetObject, Case assuranceCase,String linkType)
	{
		if(linkType.equals("AssertedInference"))
		{
			AssertedInference assertedInference = argFactory.createAssertedInference();
			ArrayList sourceList = new ArrayList();
			ArrayList targetList = new ArrayList();
			sourceList.add(sourceObject);
			targetList.add(targetObject);
			assertedInference.eSet(assertedInference.eClass().getEStructuralFeature("source"), (ArrayList<? extends ArgumentationElement>) sourceList);
			assertedInference.eSet(assertedInference.eClass().getEStructuralFeature("target"), (ArrayList<? extends ArgumentationElement>) targetList);
			assuranceCase.getArgument().add(assertedInference);
			assuranceCase.getArgumentation().get(0).getConsistOf().add(assertedInference);
		}
		else if(linkType.equals("AssertedContext"))
		{
			AssertedContext assertedContext = argFactory.createAssertedContext();
			ArrayList sourceList = new ArrayList();
			ArrayList targetList = new ArrayList();
			sourceList.add(sourceObject);
			targetList.add(targetObject);
			assertedContext.eSet(assertedContext.eClass().getEStructuralFeature("source"), (ArrayList<? extends ArgumentationElement>) sourceList);
			assertedContext.eSet(assertedContext.eClass().getEStructuralFeature("target"), (ArrayList<? extends ArgumentationElement>) targetList);
			assuranceCase.getArgument().add(assertedContext);
			assuranceCase.getArgumentation().get(0).getConsistOf().add(assertedContext);
		}
		
	}
	
	public ArrayList validateAndIntegrateArgumentModules(ArrayList systemModules, ArrayList contractModules, Case assuranceCase)
	{
		ArrayList results = new ArrayList();
		int undevelopedGoalCount = 0;
		
		Argumentation argModule = argFactory.createArgumentation();
		argModule.setId("System Integration Argumnetation Module");
		assuranceCase.getArgumentation().add(argModule);
		
		Claim interModuleDependenciesClaim = argFactory.createClaim();
		interModuleDependenciesClaim.setId("G1");
		interModuleDependenciesClaim.setDescription("Intermodule dependencies between safety case case modules are resolved");
		assuranceCase.getArgument().add(interModuleDependenciesClaim);
		argModule.getConsistOf().add(interModuleDependenciesClaim);
		
		InformationElementCitation interModuleContext = argFactory.createInformationElementCitation();
		interModuleContext.setType(InformationElementType.CONTEXT);
		interModuleContext.setId("C1");
		interModuleContext.setDescription("Safety case modules");
		
		assuranceCase.getArgument().add(interModuleContext);
		argModule.getConsistOf().add(interModuleContext);
		
		createLink(interModuleDependenciesClaim, interModuleContext, assuranceCase, "AssertedContext");
		
		
		
		boolean isResolved;
		for(int i = 0; i < systemModules.size(); i ++)
		{
			ContractModelElement cmeModule = (ContractModelElement) systemModules.get(i);
			Argumentation module = (Argumentation) cmeModule.getEnclosingModule();
			EList<ArgumentElement> moduleElements = module.getConsistOf();
			
			Claim interModuleDependenciesPerModule = argFactory.createClaim();
			interModuleDependenciesPerModule.setId("G1" + "." + i);
			interModuleDependenciesPerModule.setDescription("Intermodule dependencies of " + module.getId() + " addressed");
			assuranceCase.getArgument().add(interModuleDependenciesPerModule);
			argModule.getConsistOf().add(interModuleDependenciesPerModule);
			createLink(interModuleDependenciesClaim,interModuleDependenciesPerModule, assuranceCase, "AssertedInference");
			
			Claim assumptionsContradiction = argFactory.createClaim();
			assumptionsContradiction.setId("G" + (2+i));
			assumptionsContradiction.setDescription("Assumptions of " + module.getId() + " are not contradicted");
			assuranceCase.getArgument().add(assumptionsContradiction);
			argModule.getConsistOf().add(assumptionsContradiction);
			createLink(interModuleDependenciesPerModule,assumptionsContradiction, assuranceCase, "AssertedInference");
			
			InformationElementCitation review = argFactory.createInformationElementCitation();
			review.setType(InformationElementType.SOLUTION);
			review.setId("S" + (1+i));
			review.setDescription("Review");
			review.setToBeInstantiated(true);
			assuranceCase.getArgument().add(review);
			argModule.getConsistOf().add(review);
			createLink(assumptionsContradiction,review, assuranceCase, "AssertedInference");
			
			ArgumentReasoning strategyResolveDependencies = argFactory.createArgumentReasoning();
			strategyResolveDependencies.setId("St" + (1+i));
			strategyResolveDependencies.setDescription("Argument over all undevloped goals of " + module.getId());
			assuranceCase.getArgument().add(strategyResolveDependencies);
			argModule.getConsistOf().add(strategyResolveDependencies);
			
			createLink(interModuleDependenciesPerModule, strategyResolveDependencies, assuranceCase, "AssertedInference");
			boolean noUndevelopedGoals = true;
			undevelopedGoalCount = 0;
			for(int j = 0; j < moduleElements.size(); j++)
			{
				
				ArgumentElement element = moduleElements.get(j);
				if(element.eClass().getName().equals("Claim") && ((Boolean) element.eGet(element.eClass().getEStructuralFeature("toBeSupported"))).booleanValue() == true)
				{
					noUndevelopedGoals = false;
					ArgumentElementCitation awayGoalRequiringSupport = argFactory.createArgumentElementCitation();
					awayGoalRequiringSupport.setId(element.getId());
					awayGoalRequiringSupport.setCitedType(CitationElementType.CLAIM);
					awayGoalRequiringSupport.setDescription(element.getDescription());
					isResolved = isResolvedByContract(awayGoalRequiringSupport, module, contractModules);
					if(isResolved == false)
					{
						results.add(new String(element.getId() + " in " + module.getId() + " is not resolved by contract"));
					}
					
					Claim goalRequiringSupport = argFactory.createClaim();
					goalRequiringSupport.setId("G1" + "." + i + "." + undevelopedGoalCount);
					goalRequiringSupport.setDescription(element.getId() + " requiring support in " + module.getId());
					assuranceCase.getArgument().add(goalRequiringSupport);
					argModule.getConsistOf().add(goalRequiringSupport);
					createLink(strategyResolveDependencies, goalRequiringSupport, assuranceCase, "AssertedInference");
					
					assuranceCase.getArgument().add(awayGoalRequiringSupport);
					argModule.getConsistOf().add(awayGoalRequiringSupport);
					createLink(goalRequiringSupport,awayGoalRequiringSupport, assuranceCase, "AssertedInference");
					
					undevelopedGoalCount++;
				}
			}
			if(noUndevelopedGoals == true)
			{
				Claim noUndeveloped = argFactory.createClaim();
				noUndeveloped.setId("G1" + "." + i + "." + 0);
				noUndeveloped.setDescription("No undeveloped goals in module " + module.getId());
				assuranceCase.getArgument().add(noUndeveloped);
				argModule.getConsistOf().add(noUndeveloped);
				createLink(strategyResolveDependencies,noUndeveloped, assuranceCase, "AssertedInference");
			}
		}
		return results;
	}
	
	public boolean isResolvedByContract(ArgumentElementCitation undevelopedClaim, Argumentation systemModule, ArrayList contractModules)
	{
		boolean result = false;
		for(int i = 0; i < contractModules.size(); i ++)
		{
			ContractModelElement cmeContractModule = (ContractModelElement) contractModules.get(i);
			Argumentation contractModule = (Argumentation) cmeContractModule.getEnclosingModule();
			EList<ArgumentElement> contractModuleElements = contractModule.getConsistOf();
			for(int j = 0; j < contractModuleElements.size(); j++)
			{
				ArgumentElement contractElement = (ArgumentElement) contractModuleElements.get(j);
				if(contractElement.eClass().getName().equals("ArgumentElementCitation") && ((CitationElementType)contractElement.eGet(contractElement.eClass().getEStructuralFeature("citedType")) == CitationElementType.CLAIM))
				{
					if(contractElement.getId().equals(undevelopedClaim.getId()) && ((ArgumentElementCitation)contractElement).getArgumentationReference().equals(systemModule.getId()))
					{
						undevelopedClaim.setArgumentationReference(contractModule.getId());
						undevelopedClaim.setName(cmeContractModule.getCdoResource().getPath() + "/" + undevelopedClaim.getId() );
						result = true;
					}
					
				}
			}
		}
		return result;
	}
	
	public ArrayList argumentModuleChangeImpactAnalysis(CDOResource integrationModel, CDOView view, Shell shell)
	{
		ArrayList results = new ArrayList();
		TreeIterator integrationModelTree = integrationModel.eAllContents();
		EObject integrationModelElement;
		TreeIterator contractModelTree;
		CDOResource contractModel;
		EObject contractModelElement;
		TreeIterator systemModelTree;
		CDOResource systemModel;
		String contractModelURI;
		String systemModelURI;
		EObject systemModelElement;
		boolean foundContractElementInSystemModule;
		boolean tmpElementMatch;
		boolean tmpContextAssociationMatch;
		
		while(integrationModelTree.hasNext())
		{
			integrationModelElement = (EObject) integrationModelTree.next();
			if(integrationModelElement.eClass().getName() =="ArgumentElementCitation")
			{
				ArgumentElementCitation aecIntegration = (ArgumentElementCitation) integrationModelElement;
				contractModelURI = aecIntegration.getName().substring(0,aecIntegration.getName().lastIndexOf('/'));
				contractModel = view.getResource(contractModelURI);
				contractModelTree = contractModel.eAllContents();
				while(contractModelTree.hasNext())
				{
					contractModelElement = (EObject) contractModelTree.next();
					if(contractModelElement.eClass().getName() =="ArgumentElementCitation")
					{
						ArgumentElementCitation aecContract = (ArgumentElementCitation) contractModelElement;
						systemModelURI = aecContract.getName().substring(0,aecContract.getName().lastIndexOf('/'));
						systemModel = view.getResource(systemModelURI);
						systemModelTree = systemModel.eAllContents();
						foundContractElementInSystemModule = false;
						while(systemModelTree.hasNext())
						{
							systemModelElement = (EObject) systemModelTree.next();
							if(systemModelElement.eClass().getName() == "Claim")
							{
								if(((Boolean)systemModelElement.eGet(systemModelElement.eClass().getEStructuralFeature("public"))).booleanValue() == true && ((Boolean)systemModelElement.eGet(systemModelElement.eClass().getEStructuralFeature("toBeSupported"))).booleanValue() == true)
								{
									
									tmpElementMatch = compareElements((ArgumentationElement) contractModelElement,(ArgumentationElement) systemModelElement, systemModel,contractModel, false);
				
									if(tmpElementMatch == true)
									{
										foundContractElementInSystemModule = true;
										tmpContextAssociationMatch = compareContextAssociations((ArgumentationElement) contractModelElement,(ArgumentationElement) systemModelElement, systemModel,contractModel, false);
										if(tmpContextAssociationMatch == false)
										{
											
											results.add(new String("The context associations of argument element  " + ((ArgumentationElement)contractModelElement).getId() + " referenced in the contract" + contractModelURI + " are different from " + systemModelURI));
										}
									}
								}
								else if(((Boolean)systemModelElement.eGet(systemModelElement.eClass().getEStructuralFeature("public"))).booleanValue() == true && ((Boolean)systemModelElement.eGet(systemModelElement.eClass().getEStructuralFeature("toBeSupported"))).booleanValue() == false)
								{
									tmpElementMatch = compareElements((ArgumentationElement) contractModelElement,(ArgumentationElement) systemModelElement, systemModel,contractModel, true);
									if(tmpElementMatch == true)
									{
										foundContractElementInSystemModule = true;
										tmpContextAssociationMatch = compareContextAssociations((ArgumentationElement) contractModelElement,(ArgumentationElement) systemModelElement, systemModel,contractModel, true);
										if(tmpContextAssociationMatch == false)
										{
											results.add(new String("The context associations of argument element  " + ((ArgumentationElement)contractModelElement).getId() + " referenced in the contract" + contractModelURI + " are different from " + systemModelURI));
										}
									}
								}
								
							}
							else if(systemModelElement.eClass().getName() == "InformationElementCitation")
							{
								if(((InformationElementType)systemModelElement.eGet(systemModelElement.eClass().getEStructuralFeature("type"))).getName() =="Solution")
								{
									tmpElementMatch = compareElements((ArgumentationElement) contractModelElement,(ArgumentationElement) systemModelElement, systemModel,contractModel, true);
									if(tmpElementMatch == true)
									{
										foundContractElementInSystemModule = true;
										tmpContextAssociationMatch = compareContextAssociations((ArgumentationElement) contractModelElement,(ArgumentationElement) systemModelElement, systemModel,contractModel, true);
										if(tmpContextAssociationMatch == false)
										{
											results.add(new String("The context associations of argument element  " + ((ArgumentationElement)contractModelElement).getId() + " referenced in the contract" + contractModelURI + " are different from " + systemModelURI));
										}
									}
								}
							}
						}
						if(foundContractElementInSystemModule == false)
						{
							results.add(new String("Argument Element  " + ((ArgumentationElement)contractModelElement).getId() + " referenced in the contract" + contractModelURI + " has been modified or removed from " + systemModelURI));
						}
					}
				}
			}
		}
		return results;
	}
	
	public boolean compareElements(ArgumentationElement contractModelElement, ArgumentationElement systemModelElement, CDOResource sysCdoResource, CDOResource conCdoResource, boolean topDown)
	{
		if(systemModelElement.getId().equals(contractModelElement.getId()))
		{
			if(systemModelElement.getDescription() != null)
			{
				if(systemModelElement.getDescription().equals(contractModelElement.getDescription()))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				if(contractModelElement.getDescription() == null)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	
	public boolean compareContextAssociations(ArgumentationElement contractModelElement, ArgumentationElement systemModelElement, CDOResource sysCdoResource, CDOResource conCdoResource, boolean topDown)
	{
		ArrayList contexts = hasContext(contractModelElement, conCdoResource);
		String caSys = formatInheritedContext(systemModelElement, sysCdoResource, topDown, true);
		if(contexts != null)
		{
			ContextAssociation ca = (ContextAssociation) contexts.get(0);
			if(((ArgumentElement)ca.getTarget()).getDescription().replaceAll("\\s+","").equals(caSys.replaceAll("\\s+","")))
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		else
		{
			if(caSys == "")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
