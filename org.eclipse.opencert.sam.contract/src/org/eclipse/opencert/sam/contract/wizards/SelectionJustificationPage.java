/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;

public class SelectionJustificationPage extends WizardPage {
	private Text text;

	public SelectionJustificationPage(String name) {
		super(name);
	
	}

	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		setControl(container);
		container.setLayout(new GridLayout(1, false));
		
		Label lblEnterJustificationoptional = new Label(container, SWT.NONE);
		lblEnterJustificationoptional.setText("Enter Justification (Optional)");
		
		text = new Text(container, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		GridData gd_text = new GridData(GridData.FILL_HORIZONTAL);
		gd_text.heightHint = GridData.FILL_VERTICAL / 2;
		text.setLayoutData(gd_text);
	}
	
	public boolean canFlipToNextPage()
	{
		return true;
	}
	
	public String getSelectionJustification()
	{
		return text.getText();
	}

}
