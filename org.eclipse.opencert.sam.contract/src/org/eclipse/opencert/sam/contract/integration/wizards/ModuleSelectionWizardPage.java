/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.integration.wizards;

import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;
import org.eclipse.opencert.sam.contract.wizards.CDOResourceListLabelProvider;
import org.eclipse.opencert.sam.contract.wizards.CDOResourceTableLabelProvider;
import org.eclipse.opencert.sam.contract.wizards.ContractModelElement;

public class ModuleSelectionWizardPage extends WizardPage {
	
	private Table tblSelectModule;
	private CDOView viewCDO;
	private Table tblModulesSelected;
	private ArrayList selectedModules = new ArrayList();
	private ArrayList<EObject> selectedModulesEObjects = new ArrayList<EObject>();
	private ArrayList refList = new ArrayList();
	private ArrayList<String> refListDir = new ArrayList();
	private ComboViewer comboViewerSelectArgModel;
	private boolean isModuleSelection;
	
	protected ModuleSelectionWizardPage(String pageName,  CDOView view, boolean isModuleSelection) {
		super(pageName);
		viewCDO = view;
		this.isModuleSelection = isModuleSelection;
	}

	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		final GridLayout gridLayout = new GridLayout(1, false);
		setControl(container);
		container.setLayout(gridLayout);
		Label lblSelectArgModel = new Label(container, SWT.NONE);
		lblSelectArgModel.setText("Select Argumentation Model");
		comboViewerSelectArgModel = new ComboViewer(container, SWT.NONE);
		Combo comboSelectArgModel = comboViewerSelectArgModel.getCombo();
		comboSelectArgModel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		new Label(container, SWT.NONE);
		
		Label lblSelectModule = new Label(container, SWT.NONE);
		lblSelectModule.setText("Select Argumentation Modules that Constitute the System Assurance Case");
		
		final TableViewer tblViewerSelectModules = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblSelectModule = tblViewerSelectModules.getTable();
		GridData gd_tblSelectModule = new GridData(GridData.FILL_HORIZONTAL);
		gd_tblSelectModule.heightHint = GridData.FILL_VERTICAL / 6;
		tblSelectModule.setLayoutData(gd_tblSelectModule);
		tblSelectModule.setHeaderVisible(true);
		tblSelectModule.setLinesVisible(true);
		
		String[] columnNames = new String[] {
				"ID", "Description", "CDOResource"};
		int[] columnWidths = new int[] {
				150, 250, 200};
		int[] columnAlignments = new int[] {
			SWT.LEFT, SWT.LEFT, SWT.LEFT};
		for (int i = 0; i < columnNames.length; i++) {
			TableColumn tableColumn =
				new TableColumn(tblSelectModule, columnAlignments[i]);
			tableColumn.setText(columnNames[i]);
			tableColumn.setWidth(columnWidths[i]);
		}
		tblViewerSelectModules.setLabelProvider(
				new CDOResourceTableLabelProvider());
		tblViewerSelectModules.setContentProvider(
				new ArrayContentProvider());
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		
		Label lblModulesSelected = new Label(container, SWT.NONE);
		lblModulesSelected.setText("Modules Selected");
		
		final TableViewer tblViewerModulesSelected = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		tblModulesSelected = tblViewerModulesSelected.getTable();
	
		String[] columnNames2 = new String[] {
				"ID", "Description", "CDOResource"};
		int[] columnWidths2 = new int[] {
			150, 250, 200};
		int[] columnAlignments2 = new int[] {
			SWT.LEFT, SWT.LEFT, SWT.LEFT};
		for (int i = 0; i < columnNames2.length; i++) {
			TableColumn tableColumn2 =
				new TableColumn(tblModulesSelected, columnAlignments2[i]);
			tableColumn2.setText(columnNames2[i]);
			tableColumn2.setWidth(columnWidths2[i]);
		}
		
		tblViewerModulesSelected.setLabelProvider(
				new CDOResourceTableLabelProvider());
		tblViewerModulesSelected.setContentProvider(
				new ArrayContentProvider());
		tblModulesSelected.setHeaderVisible(true);
		tblModulesSelected.setLinesVisible(true);
		GridData gd_table = new GridData(GridData.FILL_HORIZONTAL);
		gd_table.heightHint = GridData.FILL_VERTICAL / 10;
		tblModulesSelected.setLayoutData(gd_table);
		
		comboViewerSelectArgModel.setContentProvider(new ArrayContentProvider());
		comboViewerSelectArgModel.setLabelProvider(new CDOResourceListLabelProvider());
		
		comboViewerSelectArgModel.addSelectionChangedListener(
				new ISelectionChangedListener()
				{
					public void selectionChanged(SelectionChangedEvent event)
					{
						IStructuredSelection selection = (IStructuredSelection) event.getSelection();
						ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
						dialog.open();
						IProgressMonitor monitor = dialog.getProgressMonitor();
						monitor.beginTask("Searching for Modules", 20);
						try {			
							ArrayList results = getCandidateElements(selection);
							tblViewerSelectModules.setInput(results.toArray());
						} 
						catch (Exception e) {
							e.printStackTrace();
						}
						monitor.worked(1);
						dialog.close();
					}
				});
		
		tblViewerSelectModules.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
				IStructuredSelection selection = (IStructuredSelection) arg0.getSelection();
				ContractModelElement cme = (ContractModelElement)selection.getFirstElement();
				EObject selEObj = (EObject) cme.getModelElement();
				selectedModules.add(cme);
				selectedModulesEObjects.add(selEObj);
				tblViewerModulesSelected.setInput(selectedModules.toArray());
				getWizard().getContainer().updateButtons();
			}
		});
		
		tblViewerModulesSelected.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent arg0) {
				IStructuredSelection selection = (IStructuredSelection) arg0.getSelection();
				selectedModulesEObjects.remove(((ContractModelElement)selection.getFirstElement()).getModelElement());
				selectedModules.remove(selection.getFirstElement());
				tblViewerModulesSelected.setInput(selectedModules.toArray());
				getWizard().getContainer().updateButtons();
			}
		});
		
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Searching for argumnetation files", 20);

		try {			
			populateArgModels();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		monitor.worked(1);
		dialog.close();
		
	}
	
	public ArrayList getCandidateElements(IStructuredSelection selection)
	{
		EObject eobj;
		CDOResource resource = (CDOResource) selection.getFirstElement();
		TreeIterator ti = resource.eAllContents();
		ArrayList results = new ArrayList();

		while(ti.hasNext())
		{
			eobj = (EObject) ti.next();
			if(eobj.eClass().getName() == "Argumentation")
			{
				ContractModelElement cme = new ContractModelElement();
				cme.setEnclosingModule(eobj);
				cme.setCdoResource(resource);
				cme.setModelElement(eobj);
				results.add(cme);
			}		 
		}
		return results;
	}
	
	public void populateArgModels()
	{
		addFilesOfDir();
		setArgModelListInput(refList.toArray());
	}
	
	public int addFilesOfDir()
	{      
		 int sumStd=0;
	     CDOResourceNode[] listR=  viewCDO.getElements();   
	     for (int i=0; i<listR.length; i++){
	    	 if(listR[i] instanceof CDOResourceFolder){
	    		 checkFolderContents((CDOResourceFolder)listR[i],sumStd);
	         }
	         else if (listR[i].getName().endsWith(".arg")){
	        	 refList.add(listR[i]);
	             refListDir.add(listR[i].getPath());
	             sumStd++;
	         }                      
	     }
	      return sumStd;
	}
	
	public void setArgModelListInput(Object[] cdoResource)
	{
		comboViewerSelectArgModel.setInput(cdoResource);
		comboViewerSelectArgModel.setSorter(new ViewerSorter() {
			public int compare(
					Viewer viewer, Object c1, Object c2) {
						return ((CDOResource) c1).getPath().compareToIgnoreCase(((CDOResource) c2).getPath());
					}
			});
	}

	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, int sumStd) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
			for (int i=0; i<listN.size(); i++){
				if(listN.get(i) instanceof CDOResourceFolder){
					checkFolderContents((CDOResourceFolder)listN.get(i),sumStd);
		        }
				else if (listN.get(i).getName().endsWith(".arg")){
					refList.add(listN.get(i));
		            refListDir.add(listN.get(i).getPath());
		            sumStd++;
		        }
		   }
		}
	}
	
	public ArrayList<EObject> getSelectedEObjects()
	{
		return selectedModulesEObjects;
	}
	
	public ArrayList getContractModelElements()
	{
		return selectedModules;
	}
	
	public boolean canFlipToNextPage()
	{
		if((isModuleSelection == true) && selectedModules.size() > 1)
		{
			return true;
		}
		else if((isModuleSelection == false) && selectedModules.size() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
