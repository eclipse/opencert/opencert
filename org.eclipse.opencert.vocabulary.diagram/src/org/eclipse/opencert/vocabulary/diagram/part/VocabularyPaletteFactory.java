/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.part;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes;

/**
 * @generated
 */
public class VocabularyPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createObjects1Group());
		paletteRoot.add(createConnections2Group());
	}

	/**
	 * Creates "Objects" palette tool group
	 * @generated
	 */
	private PaletteContainer createObjects1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Objects1Group_title);
		paletteContainer.setId("createObjects1Group"); //$NON-NLS-1$
		paletteContainer.add(createCategory1CreationTool());
		paletteContainer.add(createSourceofDefinition2CreationTool());
		paletteContainer.add(createTerm3CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Connections" palette tool group
	 * @generated
	 */
	private PaletteContainer createConnections2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Connections2Group_title);
		paletteContainer.setId("createConnections2Group"); //$NON-NLS-1$
		paletteContainer.add(createCategorizedterm1CreationTool());
		paletteContainer.add(createDefinedby2CreationTool());
		paletteContainer.add(createHasa3CreationTool());
		paletteContainer.add(createIsa4CreationTool());
		paletteContainer.add(createRefersto5CreationTool());
		paletteContainer.add(createSubcategory6CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCategory1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Category1CreationTool_title,
				Messages.Category1CreationTool_desc,
				Collections.singletonList(VocabularyElementTypes.Category_2002));
		entry.setId("createCategory1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/famfamfam_silk_icons_v013/icons/folder.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/famfamfam_silk_icons_v013/icons/folder.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createSourceofDefinition2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.SourceofDefinition2CreationTool_title,
				Messages.SourceofDefinition2CreationTool_desc,
				Collections
						.singletonList(VocabularyElementTypes.SourceOfDefinition_2003));
		entry.setId("createSourceofDefinition2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/famfamfam_silk_icons_v013/icons/book_next.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/famfamfam_silk_icons_v013/icons/book_next.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createTerm3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Term3CreationTool_title,
				Messages.Term3CreationTool_desc,
				Collections.singletonList(VocabularyElementTypes.Term_2001));
		entry.setId("createTerm3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/gray_text.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/gray_text.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCategorizedterm1CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Categorizedterm1CreationTool_title,
				Messages.Categorizedterm1CreationTool_desc,
				Collections
						.singletonList(VocabularyElementTypes.CategoryTerms_4001));
		entry.setId("createCategorizedterm1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createDefinedby2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Definedby2CreationTool_title,
				Messages.Definedby2CreationTool_desc,
				Collections
						.singletonList(VocabularyElementTypes.TermDefinedBy_4003));
		entry.setId("createDefinedby2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createHasa3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Hasa3CreationTool_title,
				Messages.Hasa3CreationTool_desc,
				Collections.singletonList(VocabularyElementTypes.TermHasA_4005));
		entry.setId("createHasa3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createIsa4CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Isa4CreationTool_title,
				Messages.Isa4CreationTool_desc,
				Collections.singletonList(VocabularyElementTypes.TermIsA_4004));
		entry.setId("createIsa4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRefersto5CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Refersto5CreationTool_title,
				Messages.Refersto5CreationTool_desc,
				Collections
						.singletonList(VocabularyElementTypes.TermRefersTo_4006));
		entry.setId("createRefersto5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createSubcategory6CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Subcategory6CreationTool_title,
				Messages.Subcategory6CreationTool_desc,
				Collections
						.singletonList(VocabularyElementTypes.CategorySubCategories_4002));
		entry.setId("createSubcategory6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		entry.setLargeIcon(VocabularyDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.vocabulary/icons/link.png")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
