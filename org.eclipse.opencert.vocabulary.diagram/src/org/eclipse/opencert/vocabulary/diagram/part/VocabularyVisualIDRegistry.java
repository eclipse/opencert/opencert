/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyPackage;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategorySubCategoriesEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategorySubCategoriesExternalLabelEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryTermsEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryTermsExternalLabelEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionUriEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermDefinedByEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermDefinedByExternalLabelEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermDefinitionsEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermHasAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermHasAExternalLabelEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermIsAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermIsAExternalLabelEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermRefersToEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermRefersToExternalLabelEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.VocabularyEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class VocabularyVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "org.eclipse.opencert.vocabulary.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (VocabularyEditPart.MODEL_ID.equals(view.getType())) {
				return VocabularyEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				VocabularyDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (VocabularyPackage.eINSTANCE.getVocabulary().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((Vocabulary) domainElement)) {
			return VocabularyEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
				.getModelID(containerView);
		if (!VocabularyEditPart.MODEL_ID.equals(containerModelID)
				&& !"vocabulary".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (VocabularyEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = VocabularyEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case VocabularyEditPart.VISUAL_ID:
			if (VocabularyPackage.eINSTANCE.getTerm().isSuperTypeOf(
					domainElement.eClass())) {
				return TermEditPart.VISUAL_ID;
			}
			if (VocabularyPackage.eINSTANCE.getCategory().isSuperTypeOf(
					domainElement.eClass())) {
				return CategoryEditPart.VISUAL_ID;
			}
			if (VocabularyPackage.eINSTANCE.getSourceOfDefinition()
					.isSuperTypeOf(domainElement.eClass())) {
				return SourceOfDefinitionEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
				.getModelID(containerView);
		if (!VocabularyEditPart.MODEL_ID.equals(containerModelID)
				&& !"vocabulary".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (VocabularyEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = VocabularyEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case VocabularyEditPart.VISUAL_ID:
			if (TermEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CategoryEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SourceOfDefinitionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TermEditPart.VISUAL_ID:
			if (TermNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (TermDefinitionsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CategoryEditPart.VISUAL_ID:
			if (CategoryNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SourceOfDefinitionEditPart.VISUAL_ID:
			if (SourceOfDefinitionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SourceOfDefinitionUriEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CategoryTermsEditPart.VISUAL_ID:
			if (CategoryTermsExternalLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CategorySubCategoriesEditPart.VISUAL_ID:
			if (CategorySubCategoriesExternalLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TermDefinedByEditPart.VISUAL_ID:
			if (TermDefinedByExternalLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TermIsAEditPart.VISUAL_ID:
			if (TermIsAExternalLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TermHasAEditPart.VISUAL_ID:
			if (TermHasAExternalLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TermRefersToEditPart.VISUAL_ID:
			if (TermRefersToExternalLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(Vocabulary element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case VocabularyEditPart.VISUAL_ID:
			return false;
		case TermEditPart.VISUAL_ID:
		case CategoryEditPart.VISUAL_ID:
		case SourceOfDefinitionEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
