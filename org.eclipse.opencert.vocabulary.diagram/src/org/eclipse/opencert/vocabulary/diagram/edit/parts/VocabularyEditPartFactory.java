/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;
import org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry;

/**
 * @generated
 */
public class VocabularyEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (VocabularyVisualIDRegistry.getVisualID(view)) {

			case VocabularyEditPart.VISUAL_ID:
				return new VocabularyEditPart(view);

			case TermEditPart.VISUAL_ID:
				return new TermEditPart(view);

			case TermNameEditPart.VISUAL_ID:
				return new TermNameEditPart(view);

			case TermDefinitionsEditPart.VISUAL_ID:
				return new TermDefinitionsEditPart(view);

			case CategoryEditPart.VISUAL_ID:
				return new CategoryEditPart(view);

			case CategoryNameEditPart.VISUAL_ID:
				return new CategoryNameEditPart(view);

			case SourceOfDefinitionEditPart.VISUAL_ID:
				return new SourceOfDefinitionEditPart(view);

			case SourceOfDefinitionNameEditPart.VISUAL_ID:
				return new SourceOfDefinitionNameEditPart(view);

			case SourceOfDefinitionUriEditPart.VISUAL_ID:
				return new SourceOfDefinitionUriEditPart(view);

			case CategoryTermsEditPart.VISUAL_ID:
				return new CategoryTermsEditPart(view);

			case CategoryTermsExternalLabelEditPart.VISUAL_ID:
				return new CategoryTermsExternalLabelEditPart(view);

			case CategorySubCategoriesEditPart.VISUAL_ID:
				return new CategorySubCategoriesEditPart(view);

			case CategorySubCategoriesExternalLabelEditPart.VISUAL_ID:
				return new CategorySubCategoriesExternalLabelEditPart(view);

			case TermDefinedByEditPart.VISUAL_ID:
				return new TermDefinedByEditPart(view);

			case TermDefinedByExternalLabelEditPart.VISUAL_ID:
				return new TermDefinedByExternalLabelEditPart(view);

			case TermIsAEditPart.VISUAL_ID:
				return new TermIsAEditPart(view);

			case TermIsAExternalLabelEditPart.VISUAL_ID:
				return new TermIsAExternalLabelEditPart(view);

			case TermHasAEditPart.VISUAL_ID:
				return new TermHasAEditPart(view);

			case TermHasAExternalLabelEditPart.VISUAL_ID:
				return new TermHasAExternalLabelEditPart(view);

			case TermRefersToEditPart.VISUAL_ID:
				return new TermRefersToEditPart(view);

			case TermRefersToExternalLabelEditPart.VISUAL_ID:
				return new TermRefersToExternalLabelEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
