/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.CommonParserHint;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategorySubCategoriesEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryTermsEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermDefinedByEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermHasAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermIsAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermNameEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermRefersToEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.VocabularyEditPart;
import org.eclipse.opencert.vocabulary.diagram.part.VocabularyDiagramEditorPlugin;
import org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyParserProvider;

/**
 * @generated
 */
public class VocabularyNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		VocabularyDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		VocabularyDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof VocabularyNavigatorItem
				&& !isOwnView(((VocabularyNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof VocabularyNavigatorGroup) {
			VocabularyNavigatorGroup group = (VocabularyNavigatorGroup) element;
			return VocabularyDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof VocabularyNavigatorItem) {
			VocabularyNavigatorItem navigatorItem = (VocabularyNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (VocabularyVisualIDRegistry.getVisualID(view)) {
		case VocabularyEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://opencert.org/vocabulary/2.0?Vocabulary", VocabularyElementTypes.Vocabulary_1000); //$NON-NLS-1$
		case TermEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://opencert.org/vocabulary/2.0?Term", VocabularyElementTypes.Term_2001); //$NON-NLS-1$
		case CategoryEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://opencert.org/vocabulary/2.0?Category", VocabularyElementTypes.Category_2002); //$NON-NLS-1$
		case SourceOfDefinitionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://opencert.org/vocabulary/2.0?SourceOfDefinition", VocabularyElementTypes.SourceOfDefinition_2003); //$NON-NLS-1$
		case CategoryTermsEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://opencert.org/vocabulary/2.0?Category?terms", VocabularyElementTypes.CategoryTerms_4001); //$NON-NLS-1$
		case CategorySubCategoriesEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://opencert.org/vocabulary/2.0?Category?subCategories", VocabularyElementTypes.CategorySubCategories_4002); //$NON-NLS-1$
		case TermDefinedByEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://opencert.org/vocabulary/2.0?Term?definedBy", VocabularyElementTypes.TermDefinedBy_4003); //$NON-NLS-1$
		case TermIsAEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://opencert.org/vocabulary/2.0?Term?isA", VocabularyElementTypes.TermIsA_4004); //$NON-NLS-1$
		case TermHasAEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://opencert.org/vocabulary/2.0?Term?hasA", VocabularyElementTypes.TermHasA_4005); //$NON-NLS-1$
		case TermRefersToEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://opencert.org/vocabulary/2.0?Term?refersTo", VocabularyElementTypes.TermRefersTo_4006); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = VocabularyDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& VocabularyElementTypes.isKnownElementType(elementType)) {
			image = VocabularyElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof VocabularyNavigatorGroup) {
			VocabularyNavigatorGroup group = (VocabularyNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof VocabularyNavigatorItem) {
			VocabularyNavigatorItem navigatorItem = (VocabularyNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (VocabularyVisualIDRegistry.getVisualID(view)) {
		case VocabularyEditPart.VISUAL_ID:
			return getVocabulary_1000Text(view);
		case TermEditPart.VISUAL_ID:
			return getTerm_2001Text(view);
		case CategoryEditPart.VISUAL_ID:
			return getCategory_2002Text(view);
		case SourceOfDefinitionEditPart.VISUAL_ID:
			return getSourceOfDefinition_2003Text(view);
		case CategoryTermsEditPart.VISUAL_ID:
			return getCategoryTerms_4001Text(view);
		case CategorySubCategoriesEditPart.VISUAL_ID:
			return getCategorySubCategories_4002Text(view);
		case TermDefinedByEditPart.VISUAL_ID:
			return getTermDefinedBy_4003Text(view);
		case TermIsAEditPart.VISUAL_ID:
			return getTermIsA_4004Text(view);
		case TermHasAEditPart.VISUAL_ID:
			return getTermHasA_4005Text(view);
		case TermRefersToEditPart.VISUAL_ID:
			return getTermRefersTo_4006Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getVocabulary_1000Text(View view) {
		Vocabulary domainModelElement = (Vocabulary) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTerm_2001Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.Term_2001,
				view.getElement() != null ? view.getElement() : view,
				VocabularyVisualIDRegistry.getType(TermNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCategory_2002Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.Category_2002,
				view.getElement() != null ? view.getElement() : view,
				VocabularyVisualIDRegistry
						.getType(CategoryNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSourceOfDefinition_2003Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.SourceOfDefinition_2003, view
						.getElement() != null ? view.getElement() : view,
				VocabularyVisualIDRegistry
						.getType(SourceOfDefinitionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCategoryTerms_4001Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.CategoryTerms_4001,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCategorySubCategories_4002Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.CategorySubCategories_4002,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTermDefinedBy_4003Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.TermDefinedBy_4003,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTermIsA_4004Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.TermIsA_4004,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTermHasA_4005Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.TermHasA_4005,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTermRefersTo_4006Text(View view) {
		IParser parser = VocabularyParserProvider.getParser(
				VocabularyElementTypes.TermRefersTo_4006,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			VocabularyDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return VocabularyEditPart.MODEL_ID.equals(VocabularyVisualIDRegistry
				.getModelID(view));
	}

}
