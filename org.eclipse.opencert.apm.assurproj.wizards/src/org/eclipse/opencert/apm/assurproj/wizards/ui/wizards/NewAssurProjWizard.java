/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.eclipse.opencert.apm.assurproj.wizards.WizardEMFContext;




public class NewAssurProjWizard extends Wizard implements IExecutableExtension, INewWizard {
	private static final String REFERENCE_FRAMEWORK_SELECTION = "Reference Framework Selection";

	private static final String CREATE_ASSURANCE_PROJECT = "Create Assurance Project";

	/**
	 * Logger.
	 */
	private static final Log logger = LogFactory.getLog(NewAssurProjWizard.class);
		
	private AssurProjRefFrameworkSelectionWizardPage refSelectionPage;
	private ProjectName project;	
	protected String qrefDir;

	public NewAssurProjWizard() {
		super();
	    setNeedsProgressMonitor(true);

	}
	
	@Override
	public boolean performFinish() {
		CDOTransaction transaction =null;
		DawnExplorer repoView =null;
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if ("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer".equals(viewReferences[i].getId())) {
				repoView = (DawnExplorer)viewReferences[i].getView(false);
				break;
			}
		}
		
		if(repoView!=null){
			transaction=repoView.getView().getSession().openTransaction();					
		}
		else{
			CDOConnectionUtil.instance.init(
			PreferenceConstants.getRepositoryName(),
			PreferenceConstants.getProtocol(),
			PreferenceConstants.getServerName());
			CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
			CDOView view = sessionCDO.openView();
			transaction=sessionCDO.openTransaction();			
		}	
		
		CDOResourceFolder assuranceProject = transaction.getOrCreateResourceFolder(project.getProjectFolder() + "/" +  project.getName());
		if(!assuranceProject.getNodes().isEmpty()){
			MessageDialog.openError(getShell(), "Error", "Already exists another Assurance Project with the same name. Please, change Assurance Project Name");
			return false;
		}					
		
		boolean confirmation =  MessageDialog.openConfirm(getShell(), "Assurance Project generation", "This process could take a several minutes.\n\nDo you want to continue?");
		if (!confirmation) {
			return false;
		}
		
		transaction.close();
		
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask(CREATE_ASSURANCE_PROJECT, 20);
		
	
		//boolean bool = super.performFinish();
		monitor.worked(1);

		AssurProjInitializationHelper projectInitializer = createProjectInitializationHelper(project.getName(),project.getProjectFolder(), project.generateArgumentation());		
		monitor.worked(1);
		
		//ResourceSet resourceSet = wizardExecutionContext.getEditingDomain().getResourceSet(); 
		Object[] refRequirements = refSelectionPage.getSelectedRefReq();
		qrefDir = refSelectionPage.getQRef();
		try {			
			projectInitializer.createCreateCommands( monitor, this.getContainer(),refRequirements, refSelectionPage.getSelectedCritLevels(),refSelectionPage.getSelectedApliLevels(), qrefDir,refSelectionPage.getBaselineName(), null);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		monitor.worked(1);
		return true;
	}	

	@Override
	public void addPages() {
		
		super.addPages();
		project = new ProjectName();
		this.addPage(project);	
		refSelectionPage = createAssurProjReferenceSelectionPage();
		this.addPage(refSelectionPage);	
	}
	
	/**
	 * Method is intended to be subclassed.
	 * @return projectPage.
	 */
	/*protected NewAssurProjWizardPage createAssurProjPage() {
		return new NewAssurProjWizardPage("New Assurance Project");
	}*/
	/**
	 * Method is intended to be subclassed.
	 * @return projectPage.
	 */
	protected AssurProjRefFrameworkSelectionWizardPage createAssurProjReferenceSelectionPage() {
		return new AssurProjRefFrameworkSelectionWizardPage(REFERENCE_FRAMEWORK_SELECTION);
	}

	/**
	 * Method is intended to be subclassed.
	 * 
	 * @param project 
	 * @return
	 */
	protected AssurProjInitializationHelper createProjectInitializationHelper(final String projectName, final String cdoFolder, final boolean generateArgumentation) {
		return new AssurProjInitializationHelper(projectName,cdoFolder,generateArgumentation);
	}

	protected String getqrefDir() {
		return qrefDir;
	}

	@Override
	public void init(IWorkbench arg0, IStructuredSelection arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInitializationData(IConfigurationElement arg0, String arg1,
			Object arg2) throws CoreException {
		// TODO Auto-generated method stub
		
	}
	
}
