/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.apm.assurproj.wizards.WizardEMFContext;


public class NewBaselineWizard extends Wizard implements INewWizard {

	private static final String CREATE_PROJECT_BASELINE = "Create Project Baseline";
	private SelectProjectWizardPage projectSelection;
	private AssurProjRefFrameworkSelectionWizardPage refSelectionPage;
	protected String qrefDir;
	private WizardEMFContext wizardExecutionContext;
	CDOTransaction transaction = null;
	private Button generateArgModel;
	
	public NewBaselineWizard() {
		// TODO Auto-generated constructor stub
		this.wizardExecutionContext = new WizardEMFContext(this);
		
	}

	@Override
	public void addPages() {
		super.addPages();
		projectSelection=createProjSelectionPage();
		this.addPage(projectSelection);	
		refSelectionPage = createAssurProjSelectionPage();
		this.addPage(refSelectionPage);	
		
		
	}
	

	protected SelectProjectWizardPage createProjSelectionPage() {
		/*CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOView view = sessionCDO.openView();
		*/
		//DawnExplorer view = (DawnExplorer)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(). showView("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer");
		
	DawnExplorer repoView =null;
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if ("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer".equals(viewReferences[i].getId())) {
				repoView = (DawnExplorer)viewReferences[i].getView(false);
				break;
			}
		}
		if(repoView!=null){
			transaction=repoView.getView().getSession().openTransaction();
		
			return new SelectProjectWizardPage("assuranceproject",true,repoView.getView());
		}
		else{
			CDOConnectionUtil.instance.init(
			PreferenceConstants.getRepositoryName(),
			PreferenceConstants.getProtocol(),
			PreferenceConstants.getServerName());
			CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
			CDOView view = sessionCDO.openView();
			transaction=sessionCDO.openTransaction();
			return new SelectProjectWizardPage("assuranceproject",true,view);
		}		
	}
	
	protected AssurProjRefFrameworkSelectionWizardPage createAssurProjSelectionPage() {
		return new AssurProjRefFrameworkSelectionWizardPage(AssurProjRefFrameworkSelectionWizardPage.REFERENCE_FRAMEWORK_SELECTION);
	}
	
	
	
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean performFinish() {
		
		URI project = projectSelection.getURI();
		
		String confirmationText="";
		if(project!=null){
			String path = project.path();
			path=path.substring(0, path.lastIndexOf("/"));
			CDOResourceFolder assuranceprojectFolder= transaction.getResourceFolder(path);
			CDOResource targetResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + refSelectionPage.getBaselineName() + ".baseline");
			//The user has chosen the same name for baseline. I have to delete the baseline model, the baseline diagram, the argumentation model and the argumentation diagram
			
			
			
			
			if(targetResource.isExisting()){
				confirmationText = "Selected Assurance Project already has a baseline model with the same name and will be replaced.\n";				
				
				if(projectSelection.generateArgumentation()){
					String argFolderPath = assuranceprojectFolder.getPath().substring(0, assuranceprojectFolder.getPath().lastIndexOf("/"));
					
					
					CDOResource argResource  = transaction.getOrCreateResource(argFolderPath + "/ARGUMENTATION/" + refSelectionPage.getBaselineName() + ".arg");
					if(argResource.isExisting()){
						confirmationText = confirmationText + "\nSelected Assurance Project already has an argumentation model with the same name and will be replaced.\n";
					}
										
				}
				confirmationText = confirmationText + "\n\nDo you want to continue the process?\n";
				
				boolean confirmation = MessageDialog.openConfirm(getShell(), "Updatation process", confirmationText);
				if (!confirmation) {
					return false;
				}
			}
		}
		
		
		confirmationText="This process could take several minutes.\n\nDo you want to continue?";
		boolean confirmation =  MessageDialog.openConfirm(getShell(), "Baseline creation or updatation process", confirmationText);
		if (!confirmation) {
			return false;
		}	
			
		transaction.close();		
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask(CREATE_PROJECT_BASELINE, 20);
		
		
		
		
		AssurProjInitializationHelper projectInitializer = new AssurProjInitializationHelper("Name","",projectSelection.generateArgumentation());		
		monitor.worked(1);
		
		//ResourceSet resourceSet = wizardExecutionContext.getEditingDomain().getResourceSet(); 
		Object[] refRequirements = refSelectionPage.getSelectedRefReq();
		qrefDir = refSelectionPage.getQRef();
		try {
			projectInitializer.createCreateCommands( monitor, this.getContainer(),refRequirements, refSelectionPage.getSelectedCritLevels(),refSelectionPage.getSelectedApliLevels(),qrefDir, refSelectionPage.getBaselineName(), project);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		monitor.worked(1);		

		return true;
	}
	
	public static IProject getCurrentSelectedProject() {
	    IProject project = null;
	    ISelectionService selectionService = 
	        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();

	    ISelection selection = selectionService.getSelection();

	    if(selection instanceof IStructuredSelection) {
	        Object element = ((IStructuredSelection)selection).getFirstElement();

	        if (element instanceof IResource) {
	            project= ((IResource)element).getProject();
	        } /*else if (element instanceof PackageFragmentRoot) {
	            IJavaProject jProject = 
	                ((PackageFragmentRoot)element).getJavaProject();
	            project = jProject.getProject();
	        } else if (element instanceof IJavaElement) {
	            IJavaProject jProject= ((IJavaElement)element).getJavaProject();
	            project = jProject.getProject();
	        }*/
	    }
	    return project;
	}

}
