/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
// Start MCP
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
// End MCP
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.presentation.AssuranceprojectEditorPlugin;
import org.eclipse.opencert.apm.assurproj.wizards.ui.wizards.Etl.CDOTransformHandler;
import org.eclipse.opencert.apm.assurproj.wizards.ui.wizards.Etl.RedirectSystemErr;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;


public class AssurProjInitializationHelper {

	private static final String COMPLIANCE = "Compliance";

	private static final String GROUP = "Group";

	private static final String MAPMODEL_CLASS = "MapModel";

	private static final String MAPGROUP_CLASS = "MapGroup";

	private static final String PERMISSIONCONFIG_CLASS = "PermissionConfig";

	private static final String ASSETSPACKAGE_CLASS = "AssetsPackage";

	private static final String BASELINECONFIG_CLASS = "BaselineConfig";

	private static final String ASSURANCEPROJECT_CLASS = "AssuranceProject";

	private static final String BASELINE_DIAGRAM = ".baseline_diagram";

	// Start MCP
	private static final String ARGUMENTATION_NsPrefix = ".arg";
	private static final String REFFRAMEWORK_DIAGRAM = ".refframework_diagram";
	// End MCP

	private static final String UTF_8 = "UTF-8";

	private static final String CREATE_MODELS = "Create models";
	
	
	private static Map<EObject, EObject> map = new HashMap<EObject, EObject>();
	
	/**
	 * Logger.
	 */
	private static final Log logger = LogFactory.getLog(AssurProjInitializationHelper.class);

	/**
	 * Project that has to be initialized.
	 */
	private String projectName;
	private String cdoFolder;
	

	protected AssuranceprojectPackage dprojectPackage = AssuranceprojectPackage.eINSTANCE;
	protected AssuranceprojectFactory dprojectFactory = dprojectPackage.getAssuranceprojectFactory();

	protected BaselinePackage baselinePackage = BaselinePackage.eINSTANCE;
	protected BaselineFactory baselineFactory = baselinePackage.getBaselineFactory();

	protected MappingPackage mappingPackage = MappingPackage.eINSTANCE;
	protected MappingFactory mappingFactory = mappingPackage.getMappingFactory();


	protected IWorkbench workbench;
	protected Object [] selectedReqs;
	protected String qrefDirectoryStandard;
	static int lastj = 0;
	static int nitems = 0;
	protected String baselineName="";

	
	CDOResourceFolder projectFolder;
	CDOResourceFolder assuranceprojectFolder;
	CDOResourceFolder evidenceFolder;
	CDOResourceFolder argFolder;
	CDOResourceFolder procFolder;

	private boolean generateArgumentation;
			

	public AssurProjInitializationHelper(final String projectName,final String cdoFolder, final boolean generateArgumentation) {
		RedirectSystemErr.out("prueba0");
		this.projectName = projectName;
		this.cdoFolder=cdoFolder;
		this.generateArgumentation=generateArgumentation;
	}

	// MCP multidiagram
	/**
	 * @generated NOT
	 */
	private List<CDOResource> getDiagramsFromModel(CDOTransaction transaction, CDOResource modelCDO, CDOResourceFolder frmFolder) {
		List<CDOResource> res = new ArrayList<CDOResource>();
		
		if(frmFolder==null){
			String temp = modelCDO.getPath();
			String sourceFolderPath = temp.substring(0, temp.lastIndexOf("/"));
			 frmFolder=transaction.getResourceFolder(sourceFolderPath);
		}
		EList<CDOResourceNode> listN = frmFolder.getNodes();
		for ( CDOResourceNode node : listN) {
			if (node instanceof CDOResourceFolder) {
				System.out.println("getDiagramsFromModel: found subfolder =."+ node.toString());
				List<CDOResource> res2 = getDiagramsFromModel(transaction, null, (CDOResourceFolder) node);
				res.addAll(res2);
			} else if (node instanceof CDOResource) {					
				CDOResource elem = (CDOResource)node;
				if(elem.getPath().contains(REFFRAMEWORK_DIAGRAM)){
					EObject model = (EObject) ((DiagramImpl)elem.getContents().get(0)).basicGetElement();
					//if (elem.getContents().get(0).eResource() == modelCDO)
					if (model.eResource().getURI() == modelCDO.getURI())
					{
						res.add((CDOResource)elem.getContents().get(0).eResource());
					}
				}
			}
		}
		
		return res;
	}
	// MCP multidiagram

	//When projectModel is null the assurance project an baseline will be created
	//When projectModel is not null only the baseline of the selected project will modified or add
	public void createCreateCommands(			
			final IProgressMonitor monitor,					
			final IWizardContainer container,
			final Object [] reqs,
			final Object [] critLevels,
			final Object [] aplicLevels,
			final String qrefStandard, final String baselineName, final URI projectModel) throws ExecutionException {

		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOTransaction transaction = sessionCDO.openTransaction();
		CDOResource assuranceprojectResource=null;
		if(projectModel==null){
			 projectFolder=transaction.getOrCreateResourceFolder(cdoFolder + "/" + projectName);
			 assuranceprojectFolder=projectFolder.addResourceFolder("ASSURANCE_PROJECT");
			 evidenceFolder=projectFolder.addResourceFolder("EVIDENCE");
			 argFolder=projectFolder.addResourceFolder("ARGUMENTATION");
			 procFolder=projectFolder.addResourceFolder("PROCESSES");
			
			try {
				transaction.commit();
			} catch (ConcurrentAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CommitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}
		else{
			//Tengo que saber el URI para machacar el baseline
			String path = projectModel.path();
			path=path.substring(0, path.lastIndexOf("/"));
			assuranceprojectFolder= transaction.getResourceFolder(path);
			
			//assuranceprojectResource = transaction.getResourceFolder(assuranceprojectFolder + "/ASSURANCE_PROJECT");
			
			String argFolderPath = assuranceprojectFolder.getPath().substring(0, assuranceprojectFolder.getPath().lastIndexOf("/"));
			argFolderPath=argFolderPath + "/" + "ARGUMENTATION";
			argFolder= transaction.getResourceFolder(argFolderPath);
			
			
		}

				
		//selectedReqs = reqs;
		qrefDirectoryStandard = qrefStandard;
		this.baselineName=baselineName;
		
		
		HashMap<Object, Object> options = new HashMap<Object, Object>();
		options.put(XMLResource.OPTION_ENCODING, UTF_8);

		// MCP. Ahora variable accesible a este nivel
		
		//CDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");
		//CDOResource diagramResource  = assuranceprojectFolder.addResource(baselineName + ".baseline_diagram");
		
		CDOResource targetResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline");
		//CDOResource diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
				
		boolean sameBaselineName=false;
		try {
			//The user has chosen the same name for baseline. I have to delete the baseline model, the baseline diagram, the argumentation model and the argumentation diagram
			if(targetResource.isExisting()) {
				sameBaselineName=true;
				//CDOTransaction deleteTran = sessionCDO.openTransaction();							
				options = new HashMap<Object, Object>();
				options.put(XMLResource.OPTION_ENCODING, UTF_8);
				
				//Close the editor of the model to delete if opened to avoid after Error messages	
				IEditorReference[] edi = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
				for (IEditorReference editor : edi) {
					IEditorInput input=null;
					try {
						input = editor.getEditorInput();
					} catch (PartInitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						monitor.done();
					}
					if(input !=null){
						//When editors are re-opened automatically are URIEditorInput
						String editorPath = "";
						if (input instanceof DawnEditorInput || input instanceof URIEditorInput) {
							if (input instanceof DawnEditorInput){
								editorPath =((DawnEditorInput) input).getURI().toString();
							}
							else{
								editorPath =((URIEditorInput) input).getURI().toString();
							}
							if(editorPath.equals(assuranceprojectFolder.getURI() + "/" + baselineName + ".baseline") ||
								editorPath.equals(assuranceprojectFolder.getURI() + "/" + baselineName + ".baseline_diagram") ||
								editorPath.equals(assuranceprojectFolder.getURI() + "/" + baselineName + "_2.baseline_diagram") ||
								editorPath.equals(assuranceprojectFolder.getURI() + "/" + baselineName + "_3.baseline_diagram") ||
								editorPath.equals(assuranceprojectFolder.getURI() + "/" + baselineName + "_4.baseline_diagram")){								
								IEditorReference[] editorReferencesClose= new IEditorReference[1];
								editorReferencesClose[0]=editor;
								PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditors(editorReferencesClose,false);
								//Closed, exit for													
							}						
						}
										
					}
				}
				
				
				CDOResource diagramResourceDelete  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
				diagramResourceDelete.delete(options);
				
				CDOResource diagramResource2Delete  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + "_2.baseline_diagram");
				diagramResource2Delete.delete(options);
				
				CDOResource diagramResource3Delete  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + "_3.baseline_diagram");
				diagramResource3Delete.delete(options);
				
				CDOResource diagramResource4Delete  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + "_4.baseline_diagram");
				diagramResource4Delete.delete(options);
				
				targetResource.delete(options);
				
				if(generateArgumentation){
					
					//Close the editor of the model to delete if opened to avoid after Error messages	
					edi = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
					for (IEditorReference editor : edi) {
						IEditorInput input=null;
						try {
							input = editor.getEditorInput();
						} catch (PartInitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							monitor.done();
						}
						if(input !=null){
							//When editors are re-opened automatically are URIEditorInput
							String editorPath = "";
							if (input instanceof DawnEditorInput || input instanceof URIEditorInput) {
								if (input instanceof DawnEditorInput){
									editorPath =((DawnEditorInput) input).getURI().toString();
								}
								else{
									editorPath =((URIEditorInput) input).getURI().toString();
								}
								String pathArgFolder= assuranceprojectFolder.getURI().toString().replace("ASSURANCE_PROJECT", "ARGUMENTATION");
								if(editorPath.equals(pathArgFolder + "/" + baselineName + ".arg") ||
									editorPath.equals(pathArgFolder + "/" + baselineName + ".arg_diagram")){								
									IEditorReference[] editorReferencesClose= new IEditorReference[1];
									editorReferencesClose[0]=editor;
									PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditors(editorReferencesClose,false);
									//Closed, exit for														
								}						
							}
											
						}
					}
					
					
					
					CDOResource argResourceDiagramDelete  = transaction.getOrCreateResource(argFolder.getPath() + "/" + baselineName + ARGUMENTATION_NsPrefix + "_diagram");
					argResourceDiagramDelete.delete(options);	

					CDOResource argResourceDelete  = transaction.getOrCreateResource(argFolder.getPath() + "/" + baselineName + ARGUMENTATION_NsPrefix);

					EList<EObject> argContents = argResourceDelete.getContents();
					EObject argRootObject =argContents.get(0);

					CDOResource project = transaction.getOrCreateResource(projectModel.path());
					EList<EObject> contents = project.getContents();

					// Add the initial model object to the contents.
					EObject rootObject =contents.get(0);
					AssuranceProject asurproj = (AssuranceProject)rootObject;
					AssetsPackage assPackageconfig =asurproj.getAssetsPackage().get(0);								

					if(sameBaselineName){					
						assPackageconfig.getArgumentationModel().remove(argRootObject);
					}

					argResourceDelete.delete(options);	
				}
				//DElete all the possible existing models
				transaction.commit();
				//transaction.close();
				 targetResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline");
				 //diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
				
			}
				
							
			 //To copy the refframework as baseline
			 //MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");
						   
			  CDOResource sourceResource  = transaction.getResource(qrefStandard);
			    
			 // Create empty roots
				for (EObject aEObject : sourceResource.getContents()) {
					targetResource.getContents().add(getCorrespondingEObject(aEObject));
				}
			    
				
				Object []copia= ArrayUtils.clone(reqs);
				
				Object []copiaCL= ArrayUtils.clone(critLevels);
				
				Object []copiaAL= ArrayUtils.clone(aplicLevels);
				
				
				// Copy EObjects, containment in the resource is done automatically since we have
				// already attached the corresponding root EObject to the target resource
				for (Iterator<EObject> iterator = sourceResource.getAllContents(); iterator.hasNext();) {
					EObject aEObject = iterator.next();
					EObject bEObject = getCorrespondingEObject(aEObject);
										
					if(aEObject.eClass().getName().startsWith("Ref") || aEObject.eClass().getName().equals("MapJustification")|| aEObject.eClass().getName().equals("MapGroup")){
						copyAEObjectToBEObject(aEObject, bEObject,copia,copiaCL,copiaAL);
					}
				}			    
			    			    			    			    
			    try {
			    	targetResource.save(options);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					transaction.rollback();
				}
			
			   
			    
			    
			  //Create the assurance project model
				if(projectModel==null){					
				
				   String refFrameworkName = ((org.eclipse.opencert.pkm.refframework.refframework.RefFramework) sourceResource.getContents().get(0)).getName();
				
				   if(refFrameworkName!=null){
					   refFrameworkName=projectName;
				   }
				   //Create the complianceMap
				   CDOResource complianceResource  = assuranceprojectFolder.addResource(projectName + ".mapping");    
				   EObject rootObjectCompliance = createInitialCompliance(refFrameworkName); 
				   complianceResource.getContents().add(rootObjectCompliance); 
				   complianceResource.save(options);
				   
				   //Create the assuranceproject
				   assuranceprojectResource  = assuranceprojectFolder.addResource(projectName + ".assuranceproject");    
				   				   
				   EObject rootObjectAssurProject = createInitialAssurProject((BaseFramework)targetResource.getContents().get(0),((org.eclipse.opencert.infra.mappings.mapping.MapModel)rootObjectCompliance).getMapGroupModel().get(0)); 
				   assuranceprojectResource.getContents().add(rootObjectAssurProject); 
				   assuranceprojectResource.save(options);

				}
				else{
					//Read the assuranceproject to add the new baseline if has a different name
					assuranceprojectResource  = transaction.getResource(projectModel.path());
					EList<EObject> contents = assuranceprojectResource.getContents();

					// Add the initial model object to the contents.
					EObject rootObject =contents.get(0);

					if (rootObject != null && rootObject instanceof AssuranceProject) {
						AssuranceProject asurproj = (AssuranceProject)rootObject;
						//Delete the baseline 
						if(sameBaselineName){
							for (BaselineConfig bc: asurproj.getBaselineConfig()){
								if(bc.getName().equals(baselineName + BASELINECONFIG_CLASS)){
									asurproj.getBaselineConfig().remove(bc);
									break; 
								}
							}
						}

						BaselineConfig baseconfig = dprojectFactory.createBaselineConfig();
						baseconfig.setId("BC" + (asurproj.getBaselineConfig().size() + 1));
						baseconfig.setName(baselineName + BASELINECONFIG_CLASS);
						baseconfig.setIsActive(false);
						baseconfig.getRefFramework().add((BaseFramework)targetResource.getContents().get(0));
						asurproj.getBaselineConfig().add(baseconfig);																
					}					        
					// Save the contents of the resource to the file system.
					options = new HashMap<Object, Object>();
					options.put(XMLResource.OPTION_ENCODING, UTF_8);
					assuranceprojectResource.save(options);

				}			   							
		}
		catch (Exception exception) {
			AssuranceprojectEditorPlugin.INSTANCE.log(exception);
			transaction.rollback();
			monitor.done();
		}


		// Creating model resource can be done after resources are created: TODO !!!
		//editingDomain.getCommandStack().execute(compoundCommand);

		// Saving model resource can be done after resources are created: TODO !!!
		//compoundCommand = new CompoundCommand(INITIALIZE_MODELS);
	//	editingDomain.getCommandStack().execute(compoundCommand);
		
		// Start MCP
		// crear ficheros de diagrama
		try {			
			//CDOResource diagramResource  = assuranceprojectFolder.addResource(baselineName + ".baseline_diagram");
			
	        synchronized (transaction)
	        {
	        /* FUNCIONA PERO CREA UN DIAGRAMA PARA EL MODELO COMPLETO!!!
	        CDOResource diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");	        	
			DawnBaselineDiagramUtil myDiagram = new DawnBaselineDiagramUtil(targetResource.getURI(), diagramResource.getURI(), assuranceprojectFolder, transaction);
			myDiagram.generateDiagram(monitor);
			*/
			
	        	CDOResource sourceResource  = transaction.getResource(qrefStandard);
				List<CDOResource> listDiagrams = getDiagramsFromModel(transaction, sourceResource,null);
				int contDiag=0;
				for(CDOResource sourceDiagramResource: listDiagrams)
				{
					contDiag++;
					// lista de objetos en diagrama
					List<EObject> listRootObjectsSource = ((Diagram)sourceDiagramResource.getContents().get(0)).getPersistedChildren();
					List<EObject> listRootObjects = new ArrayList<EObject>();
					for(EObject element : listRootObjectsSource)
					{
						EObject objsource = ((NodeImpl)element).getElement();
				        		       
						//EObject obj = eIDToObjectMap.get(cdoID.toString());
				        EObject obj = getCorrespondingEObject(objsource);
						listRootObjects.add(obj);
					}
					// crear el recurso diagram vacio
					//CDOResourceFolder baselineFolder=transaction.getResourceFolder(targetFolderPath);
					String targetPath = assuranceprojectFolder.getPath() + "/";
					//String targetDiagram = targetPath + sourceDiagramResource.getName();
					String targetDiagram = targetPath + baselineName + BASELINE_DIAGRAM;
					//targetDiagram = targetDiagram.replace(REFFRAMEWORK_DIAGRAM, BASELINE_DIAGRAM);
					CDOResource targetDiagramResource  = transaction.getOrCreateResource(targetDiagram);
					if(contDiag>1){						
						
						 targetDiagram = targetPath + baselineName + "_" + contDiag + BASELINE_DIAGRAM;
						//targetDiagram = targetDiagram.replace(REFFRAMEWORK_DIAGRAM, BASELINE_DIAGRAM);
						 targetDiagramResource  = transaction.getOrCreateResource(targetDiagram);
							 						
					}
					DawnBaselineDiagramUtil myDiagram = new DawnBaselineDiagramUtil(sourceDiagramResource, listRootObjects, targetResource.getURI(), targetDiagramResource.getURI(), argFolder, transaction);
					myDiagram.generateDiagram(monitor);
				}
	        	
	        	
	        	
			
			
			transaction.commit();
	        }
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			monitor.done();
		}
		
		if(generateArgumentation){
			URI targetModelURI=null;
	
			try {
				
		        synchronized (transaction)
		        {	    		
	    		
	    		//CDOResource targetModelResource  = argFolder.addResource(baselineName + ARGUMENTATION_NsPrefix);
		        CDOResource targetModelResource  = transaction.getOrCreateResource(argFolder.getPath() + "/" + baselineName + ARGUMENTATION_NsPrefix);    		
	    		targetModelURI = targetModelResource.getURI();
		        	
				//CDOTransformHandler transfo = new CDOTransformHandler(assuranceprojectFolder, sourceModelURI, targetModelURI);
	    		RedirectSystemErr.out("prueba1");
				CDOTransformHandler transfo = new CDOTransformHandler(assuranceprojectFolder, targetResource, targetModelResource, transaction);
				RedirectSystemErr.out("prueba2");
				transfo.execute();
				RedirectSystemErr.out("prueba3");
				transaction.commit();
				RedirectSystemErr.out("prueba4");
		        }
			} catch (Exception e) {
				e.printStackTrace();
				transaction.rollback();
				monitor.done();
			}


			try {
		        synchronized (transaction)
		        {
				// crear diagrama asociado al modelo arg
				//CDOResource targetModelDiagramResource  = argFolder.addResource(baselineName + ARGUMENTATION_NsPrefix + "_diagram");
		        CDOResource targetModelDiagramResource  = transaction.getOrCreateResource(argFolder.getPath() + "/" + baselineName + ARGUMENTATION_NsPrefix + "_diagram");
		        RedirectSystemErr.out("prueba5");
				DawnArgDiagramUtil myDiagram = new DawnArgDiagramUtil(targetModelURI, targetModelDiagramResource.getURI(), argFolder, transaction);
				myDiagram.generateDiagram(monitor);
				RedirectSystemErr.out("prueba6");
				
				//MCP: se lanza LocalCommitConflictException!!!; los anteriores commit son sin parametro pero sigue el problema
				Set<CDOObject> conf = transaction.getConflicts();
				for (Iterator<CDOObject>it = conf.iterator(); it.hasNext();) {
					CDOObject nextObject = it.next();
					System.out.println("createCreateCommands: confict "+nextObject.toString());
				}
				RedirectSystemErr.out("prueba7");
				//transaction.commit(monitor); 
				transaction.commit();
		        }
			} catch (Exception e) {
				e.printStackTrace();
				transaction.rollback();
				monitor.done();
			}

			RedirectSystemErr.out("prueba8");
		
			try {
			//Read the assuranceproject assetsPackage to add the new argumentation
			
			//CDOResource assuranceprojectResource  = transaction.getResource(projectModel.path());
			EList<EObject> contents = assuranceprojectResource.getContents();
			
			// Add the initial model object to the contents.
			EObject rootObject =contents.get(0);
			AssuranceProject asurproj = (AssuranceProject)rootObject;
			AssetsPackage assPackageconfig =asurproj.getAssetsPackage().get(0);
			
			RedirectSystemErr.out("prueba9");
			CDOResource argResource = transaction.getResource(targetModelURI.path());
			EList<EObject> argContents = argResource.getContents();
			EObject argRootObject =argContents.get(0);
			Case argCase = (Case)argRootObject;
			
			if(sameBaselineName){
				//Deberia buscar el case anterior y ver si es el mismo
				assPackageconfig.getArgumentationModel().add(argCase);
			}
			else{
				assPackageconfig.getArgumentationModel().add(argCase);
			
			}	
			RedirectSystemErr.out("prueba10");
			// Save the contents of the resource to the file system.
				options = new HashMap<Object, Object>();
				options.put(XMLResource.OPTION_ENCODING, UTF_8);
			
				assuranceprojectResource.save(options);
				transaction.close();
				RedirectSystemErr.out("prueba11");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				monitor.done();//MCP
			}	
			//End MCP
		}
		//if no problems in all the previous process
		monitor.done();
	}
	


	/**
	 * Create a new baseline.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EObject createInitialAssurProject(org.eclipse.opencert.apm.baseline.baseline.BaseFramework baseline, org.eclipse.opencert.infra.mappings.mapping.MapGroup mapp) {
		EClass eClass = (EClass)dprojectPackage.getEClassifier(ASSURANCEPROJECT_CLASS);
		EObject rootObject = dprojectFactory.create(eClass);
		((AssuranceProject)rootObject).setName(projectName);


		BaselineConfig baseconfig = dprojectFactory.createBaselineConfig();
		baseconfig.setId("BC01");
		baseconfig.setName(baselineName + BASELINECONFIG_CLASS);
		baseconfig.setIsActive(true);
		baseconfig.getRefFramework().add((org.eclipse.opencert.apm.baseline.baseline.BaseFramework)baseline); //setRefBaseFramework(rootObjectBaseline);
		baseconfig.setComplianceMapGroup(mapp);


		AssetsPackage assetsPackage = dprojectFactory.createAssetsPackage();
		assetsPackage.setId("AP01");
		assetsPackage.setIsActive(true);
		assetsPackage.setName(projectName + ASSETSPACKAGE_CLASS);

		PermissionConfig permissionConfig = dprojectFactory.createPermissionConfig();
		permissionConfig.setId("PC01");
		permissionConfig.setName(projectName + PERMISSIONCONFIG_CLASS);
		permissionConfig.setIsActive(true);


		((AssuranceProject)rootObject).getBaselineConfig().add(baseconfig);
		((AssuranceProject)rootObject).getAssetsPackage().add(assetsPackage);
		((AssuranceProject)rootObject).getPermissionConf().add(permissionConfig);

		return rootObject;
	}


	/**
	 * Create a new baseline.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	/*protected EObject createInitialBaseline() {
		EClass eClass = (EClass)baselinePackage.getEClassifier(REFFRAMEWORK_CLASS);
		EObject rootObject = baselineFactory.create(eClass);
		((org.eclipse.opencert.apm.baseline.baseline.BaseFramework)rootObject).setName(projectName);

		return rootObject;
	}*/


	protected EObject createInitialCompliance(String name) {
		EClass eGroup = (EClass)mappingPackage.getEClassifier(MAPGROUP_CLASS);
		EObject groupObject = mappingFactory.create(eGroup);
		((org.eclipse.opencert.infra.mappings.mapping.MapGroup)groupObject).setId("MG1");
		((org.eclipse.opencert.infra.mappings.mapping.MapGroup)groupObject).setName(name+GROUP);

		EClass eClass = (EClass)mappingPackage.getEClassifier(MAPMODEL_CLASS);
		EObject rootObject = mappingFactory.create(eClass);
		((org.eclipse.opencert.infra.mappings.mapping.MapModel)rootObject).setId("MM1");
		((org.eclipse.opencert.infra.mappings.mapping.MapModel)rootObject).setName(name+COMPLIANCE);

		((org.eclipse.opencert.infra.mappings.mapping.MapModel)rootObject).getMapGroupModel().add((org.eclipse.opencert.infra.mappings.mapping.MapGroup)groupObject);

		return rootObject;
	}

	/**
	 * AddSubset adds a new development project subset, with their associated qualificaton reference 
	 * @param rootObject parent object
	 * @param ref qualification reference
	 * @param resource resouce to store the new development project
	 * @return the new object
	 */
	/*protected EObject AddSubset(EObject rootObject,QualificationReference ref,Resource resource){
		EClass eClass = (EClass)dprojectPackage.getEClassifier("DevelopmentProject");
		EObject newobject = dprojectFactory.create(eClass);
		DevelopmentProject subset = (DevelopmentProject)newobject;
		subset.setName(ref.getId()+"-"+ref.getDescription());
		subset.setDependsOnRef(ref);
		resource.getContents().add(subset);
		DevelopmentProject fatherRef = (DevelopmentProject)rootObject;
		fatherRef.getHasSubprojects().add((DevelopmentProject) subset);
		nitems++;
		return newobject;
	}*/	
	
	
	/**
	 * Copies an {@link EObject} instance of a class of {@link APackage} to
	 * an {@link EObject} instance of a class of {@link BPackage} considering 
	 * the well-known correspondences between both {@link EPackage}s
	 * 
	 * @param aEObject Reffframework EObject
	 * @param bEObject The correpondent Baseline EObject
	 * @param reqs The selected Refframework elements
	 */
	private static void copyAEObjectToBEObject(EObject aEObject, EObject bEObject, Object[] reqs, Object[] critLevels , Object[] aplicLevels) {
		
		//check if the object has been selected in the wizard		
		for(Object unSel:reqs){																

			if(unSel instanceof EObject){
				EObject eObjectSel = (EObject)unSel;				
				if(eObjectSel.toString().equals(aEObject.toString())){
					EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("isSelected");
					if(bFeature!= null){
						bEObject.eSet(bFeature, Boolean.TRUE);
					}
					//Encontre la metaclase, lo borro del seleccionados 
					reqs= ArrayUtils.removeElement(reqs,unSel);
					//y sigo adelante buscando seleccionados si quedan seleccionados, sino salgo de aqui
					break;
				}
			}
		}
			
		//check if the criticality level was selected in the wizard		
		for(Object unSel:critLevels){																

			if(unSel instanceof EObject){
				EObject eObjectSel = (EObject)unSel;				
				if(eObjectSel.toString().equals(aEObject.toString())){
					EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("description");
					if(bFeature!= null){
						bEObject.eSet(bFeature, "**** Selected in Assurance Project Generation Wizard ****\r\n\r\n");
					}
					//Encontre la metaclase, lo borro del seleccionados 
					critLevels= ArrayUtils.removeElement(critLevels,unSel);
					//y sigo adelante buscando seleccionados si quedan seleccionados, sino salgo de aqui
					break;
				}
			}
		}
				
		//check if the aplicability level was selected in the wizard		
		for(Object unSel:aplicLevels){																

			if(unSel instanceof EObject){
				EObject eObjectSel = (EObject)unSel;				
				if(eObjectSel.toString().equals(aEObject.toString())){
					EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("description");
					if(bFeature!= null){
						bEObject.eSet(bFeature, "**** Selected in Assurance Project Generation Wizard ****\r\n\r\n");
					}
					//Encontre la metaclase, lo borro del seleccionados 
					aplicLevels= ArrayUtils.removeElement(aplicLevels,unSel);
					//y sigo adelante buscando seleccionados si quedan seleccionados, sino salgo de aqui
					break;
				}
			}
		}
		
		
				/*if(aEObject.getClass().equals(((EObject)unSel).getClass())){											
						
					//aEObject.eGet().equals(eObjectSel.eGet(aEObject.eClass().getEStructuralFeature("id"))) &&
					//aEObject.eGet(aEObject.eClass().getEStructuralFeature("name")).equals(eObjectSel.eGet(aEObject.eClass().getEStructuralFeature("name")))){
					Object idFeature= aEObject.eGet(aEObject.eClass().getEStructuralFeature("id"));
					Object nameFeature= aEObject.eGet(aEObject.eClass().getEStructuralFeature("name"));
					
					Object selIdFeature= eObjectSel.eGet(eObjectSel.eClass().getEStructuralFeature("id"));
					Object selNameFeature= eObjectSel.eGet(eObjectSel.eClass().getEStructuralFeature("name"));
					
					
					if(idFeature!=null && nameFeature!=null && selIdFeature!=null && selNameFeature!=null ){											
						if(idFeature.equals(selIdFeature) && nameFeature.equals(selNameFeature)){
							EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("isSelected");
							if(bFeature!= null){
								bEObject.eSet(bFeature, Boolean.TRUE);
							}
							//Encontre la metaclase, lo borro del seleccionados 
							reqs= ArrayUtils.removeElement(reqs,unSel);
							//y sigo adelante buscando seleccionados si quedan seleccionados, sino salgo de aqui
							break;
						}
					}
					else if(idFeature==null && nameFeature==null && selIdFeature==null && selNameFeature==null){
						EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("isSelected");
						if(bFeature!= null){
							bEObject.eSet(bFeature, Boolean.TRUE);
						}
						//Encontre la metaclase, lo borro del seleccionados 
						reqs= ArrayUtils.removeElement(reqs,unSel);
						//y sigo adelante buscando seleccionados si quedan seleccionados, sino salgo de aqui
						break;
					}
					else if(idFeature!=null && nameFeature==null && selIdFeature!=null && selNameFeature==null){
						if(idFeature.equals(selIdFeature)){
							EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("isSelected");
							if(bFeature!= null){
								bEObject.eSet(bFeature, Boolean.TRUE);
							}
							//Encontre la metaclase, lo borro del seleccionados 
							reqs= ArrayUtils.removeElement(reqs,unSel);
							//y sigo adelante buscando seleccionados si quedan seleccionados, sino salgo de aqui
							break;
						}
					}
					else if(idFeature==null && nameFeature!=null && selIdFeature==null && selNameFeature!=null){
						if(nameFeature.equals(selNameFeature)){
							EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("isSelected");
							if(bFeature!= null){
								bEObject.eSet(bFeature, Boolean.TRUE);
							}
							//Encontre la metaclase, lo borro del seleccionados 
							reqs= ArrayUtils.removeElement(reqs,unSel);
							//y sigo adelante buscando seleccionados si quedan seleccionados, sino salgo de aqui
							break;
						}
					}
				}			
			}
		}*/
		
		for (EStructuralFeature aFeature : aEObject.eClass().getEAllStructuralFeatures()) {
			if (aEObject.eIsSet(aFeature)) {
				// Get the corresponding feature in the target EClass.
				// Get simply the feature with the same name
				
				EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature(aFeature.getName());
				
				if(aFeature.getName().equals("equivalence")){
					 bFeature = bEObject.eClass().getEStructuralFeature("equivalenceMap");
				}
				
				if(bFeature != null){
					if (aFeature instanceof EAttribute) {
						if(aFeature.getName().equals("description") && bEObject.eGet(aFeature)!=null && (aEObject.eClass().getName().equals("RefCriticalityLevel") || aEObject.eClass().getName().equals("RefApplicabilityLevel"))){
							 							 
							 String des=  bEObject.eGet(aFeature).toString() +aEObject.eGet(aFeature).toString();
							 bEObject.eSet(bFeature, des);
						}
						else{						
							bEObject.eSet(bFeature, aEObject.eGet(aFeature));
						}
					} else { // EReference
																													
						if(aEObject.eClass().getName().equals("RefEquivalenceMap") && aFeature.getName().equals("target") ){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						else if (aFeature.getName().equals("mapJustification")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							bEObject.eSet(bFeature, copy);
						}
						/*else if (aFeature.getName().equals("ownedTechnique")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							bEObject.eSet(bFeature, copy);
						}*/
						else if (aFeature.getName().equals("mapGroup")){
							//EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							//bEObject.eSet(bFeature, copy);
							
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						else{		
							if(aEObject.eGet(aFeature) instanceof EList){
								@SuppressWarnings("unchecked")
								EList<EObject> aList = (EList<EObject>) aEObject.eGet(aFeature);
								EList<EObject> bList = new BasicEList<EObject>(); 
								for (int i = 0; i < aList.size(); i++) {
									bList.add(getCorrespondingEObject(aList.get(i)));
								}
								bEObject.eSet(bFeature, bList);
							}
							else{
								
								//System.out.println("Feature " + aFeature.getName() + " of " + aEObject.eClass().getName() + " not copied to Baseline");
								EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
								//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
								
								bEObject.eSet(bFeature, copy);
							}
						}
					}
				}
			}
		}
		
		//Add the linkf to the refframeork model concepts.
		
		if(aEObject instanceof RefFramework){
			 EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("refFramework");
			 bEObject.eSet(bFeature,aEObject);
		}
		else if(bEObject instanceof BaseAssurableElement){
			EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("refAssurableElement");
			 bEObject.eSet(bFeature,aEObject);
		}
		
	}
	
	/**
	 * Returns the {@link EObject} an {@link EObject} instance of a class of
	 * {@link BPackage} that corresponds to an {@link EObject} instance of a
	 * class of {@link APackage}
	 * 
	 * @param eObject
	 * @return
	 */
	private static EObject getCorrespondingEObject(EObject aEObject) {
		EObject bEObject = map.get(aEObject);
		if (bEObject == null) {
			EClass aEClass = aEObject.eClass();			
			if(aEClass.getName().startsWith("Ref")){
			
				String bClassName = aEClass.getName().replaceFirst("Ref", "Base");
				// Second search the corresponding EClass in BPackage
				EClass bEClass = (EClass) BaselinePackage.eINSTANCE.getEClassifier(bClassName);
				// Create a new empty instance and register to avoid dups
				bEObject = EcoreUtil.create(bEClass);
				map.put(aEObject, bEObject);
			}
			else if (aEClass.getName().equals("MapJustification")){
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			}
			else if (aEClass.getName().equals("MapGroup")){
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			}
			else{
				bEObject=aEObject;
				map.put(aEObject, aEObject);
			}
		}
		return bEObject;
	}

	public static Log getLogger() {
		return logger;
	}
}
