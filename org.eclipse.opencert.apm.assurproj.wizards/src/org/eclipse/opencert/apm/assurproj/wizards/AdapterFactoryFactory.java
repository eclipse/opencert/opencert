/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;

//import es.esi.gemde.certification.qreference.reference.util.ReferenceAdapterFactory;


/**
 * A factory to create a CompositeAdapterFactory, composing all contributions to the adapterFactories extension point, as well as some auxiliary
 * adapter factories.
 * 
 * @author Any
 * 
 */
public class AdapterFactoryFactory {

	public static final AdapterFactoryFactory eINSTANCE = new AdapterFactoryFactory();

	private static final String A_FACTORY = "factory"; //$NON-NLS-1$

	private static final String EXT_POINT_FACTORIES = "adapterFactories"; //$NON-NLS-1$

	// private static final String E_FACTORY = "adapterFactory"; //$NON-NLS-1$

	private static final Log logger = LogFactory.getLog(AdapterFactoryFactory.class);

	private AdapterFactoryFactory() {
		super();
	}

	protected AdapterFactory createAdapterFactory() {
		ComposedAdapterFactory result = new ComposedAdapterFactory();
		List<ReflectiveItemProviderAdapterFactory> reflectiveFactories = new ArrayList<ReflectiveItemProviderAdapterFactory>();
		IConfigurationElement[] configs = Platform.getExtensionRegistry().getConfigurationElementsFor(Activator.PLUGIN_ID,
			EXT_POINT_FACTORIES);
		for (int i = 0; i < configs.length; i++) {
			String factoryClass = configs[i].getAttribute(A_FACTORY);
			if ((factoryClass != null) && (factoryClass.trim().length() != 0)) {
				try {
					AdapterFactory factory = (AdapterFactory) configs[i].createExecutableExtension(A_FACTORY);
					if (factory instanceof ReflectiveItemProviderAdapterFactory) {
						// Do not add reflective ItemProviderAdapterFactories until all non-reflective
						// ItemProviderAdapterFactories have been added.
						reflectiveFactories.add((ReflectiveItemProviderAdapterFactory) factory);
					}
					else {
						result.addAdapterFactory(factory);
					}
				}
				catch (CoreException e) {
					logger.error(e);
				}
			}
		}

		// add some default factories
		result.addAdapterFactory(new ResourceItemProviderAdapterFactory());
		result.addAdapterFactory(new EcoreItemProviderAdapterFactory());
		// result.addAdapterFactory(new GenModelAdapterFactory());
		// Now add reflective ItemProviderAdapterFactories
		for (ReflectiveItemProviderAdapterFactory rFactory : reflectiveFactories) {
			result.addAdapterFactory(rFactory);
		}
		result.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		/** <NAO!>**/
		//result.addAdapterFactory(new ReferenceAdapterFactory());
		return result;
	}
}
