/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards.Etl;

public class EtlTransformParameters {

	private String pluginID;
	private String etlTransform;
	private String sourceName;
	private String sourceMetaModelURI;
	//MCP
	//CRF problem	
	//private Object oldEPackage;
	private Object IoldEPackage;
	private Object OoldEPackage;	
	//CRF problem	
	private String sourceMetaModelFilePath;
	//MCP
	private String sourceModelFilePath;
	private boolean sourceReadOnLoad;
	private boolean sourceStoreOnDisposal;
	private String targetName;
	private String targetMetaModelURI;
	//CRF problem	
	private String targetMetaModelFilePath;
	//CRF problem	
	private String targetModelFilePath;
	private boolean targetReadOnLoad;
	private boolean targetStoreOnDisposal;


	public String getPluginID() {
		return pluginID;
	}

	public void setPluginID(String pluginID) {
		this.pluginID = pluginID;
	}

	public String getEtlTransform() {
		return etlTransform;
	}

	public void setEtlTransform(String etlTransform) {
		this.etlTransform = etlTransform;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceMetaModelURI() {
		return sourceMetaModelURI;
	}

	public void setSourceMetaModelURI(String sourceMetaModelURI) {
		this.sourceMetaModelURI = sourceMetaModelURI;
	}

	//MCP
	//CRF problem
	/*
	public Object getOldEPackages() {
		return this.oldEPackage;
	}
	public void setOldEPackages(Object oldEPackage) {
		this.oldEPackage = oldEPackage;
	}
	*/
	public Object getIOldEPackages() {
		return this.IoldEPackage;
	}
	public void setIOldEPackages(Object oldEPackage) {
		this.IoldEPackage = oldEPackage;
	}
	public Object getOOldEPackages() {
		return this.OoldEPackage;
	}
	public void setOOldEPackages(Object oldEPackage) {
		this.OoldEPackage = oldEPackage;
	}
	//CRF problem
	
	public String getSourceMetaModelFilePath() {
		return sourceMetaModelFilePath;
	}

	public void setSourceMetaModelFilePath(String sourceMetaModelFilePath) {
		this.sourceMetaModelFilePath = sourceMetaModelFilePath;
	}
	//MCP
	
	public String getSourceModelFilePath() {
		return sourceModelFilePath;
	}

	public void setSourceModelFilePath(String sourceModelFilePath) {
		this.sourceModelFilePath = sourceModelFilePath;
	}

	public boolean isSourceReadOnLoad() {
		return sourceReadOnLoad;
	}

	public void setSourceReadOnLoad(boolean sourceReadOnLoad) {
		this.sourceReadOnLoad = sourceReadOnLoad;
	}

	public boolean isSourceStoreOnDisposal() {
		return sourceStoreOnDisposal;
	}

	public void setSourceStoreOnDisposal(boolean sourceStoreOnDisposal) {
		this.sourceStoreOnDisposal = sourceStoreOnDisposal;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public String getTargetMetaModelURI() {
		return targetMetaModelURI;
	}

	public void setTargetMetaModelURI(String targetMetaModelURI) {
		this.targetMetaModelURI = targetMetaModelURI;
	}

	//CRF problem
	public String getTargetMetaModelFilePath() {
		return targetMetaModelFilePath;
	}

	public void setTargetMetaModelFilePath(String targetMetaModelFilePath) {
		this.targetMetaModelFilePath = targetMetaModelFilePath;
	}
	//CRF problem	
	
	public String getTargetModelFilePath() {
		return targetModelFilePath;
	}

	public void setTargetModelFilePath(String targetModelFilePath) {
		this.targetModelFilePath = targetModelFilePath;
	}

	public boolean isTargetReadOnLoad() {
		return targetReadOnLoad;
	}

	public void setTargetReadOnLoad(boolean targetReadOnLoad) {
		this.targetReadOnLoad = targetReadOnLoad;
	}

	public boolean isTargetStoreOnDisposal() {
		return targetStoreOnDisposal;
	}

	public void setTargetStoreOnDisposal(boolean targetStoreOnDisposal) {
		this.targetStoreOnDisposal = targetStoreOnDisposal;
	}

}
