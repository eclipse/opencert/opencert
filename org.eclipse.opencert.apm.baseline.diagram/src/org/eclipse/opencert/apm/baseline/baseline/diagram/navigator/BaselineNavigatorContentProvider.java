/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.navigator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonContentProvider;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartmentEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityPrecedingActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityProducedArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRequiredArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.Messages;

/**
 * @generated
 */
public class BaselineNavigatorContentProvider implements ICommonContentProvider {

	/**
	 * @generated
	 */
	private static final Object[] EMPTY_ARRAY = new Object[0];

	/**
	 * @generated
	 */
	private Viewer myViewer;

	/**
	 * @generated
	 */
	private AdapterFactoryEditingDomain myEditingDomain;

	/**
	 * @generated
	 */
	private WorkspaceSynchronizer myWorkspaceSynchronizer;

	/**
	 * @generated
	 */
	private Runnable myViewerRefreshRunnable;

	/**
	 * @generated
	 */
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public BaselineNavigatorContentProvider() {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		myEditingDomain = (AdapterFactoryEditingDomain) editingDomain;
		myEditingDomain.setResourceToReadOnlyMap(new HashMap() {
			public Object get(Object key) {
				if (!containsKey(key)) {
					put(key, Boolean.TRUE);
				}
				return super.get(key);
			}
		});
		myViewerRefreshRunnable = new Runnable() {
			public void run() {
				if (myViewer != null) {
					myViewer.refresh();
				}
			}
		};
		myWorkspaceSynchronizer = new WorkspaceSynchronizer(editingDomain,
				new WorkspaceSynchronizer.Delegate() {
					public void dispose() {
					}

					public boolean handleResourceChanged(final Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceDeleted(Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceMoved(Resource resource,
							final URI newURI) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}
				});
	}

	/**
	 * @generated
	 */
	public void dispose() {
		myWorkspaceSynchronizer.dispose();
		myWorkspaceSynchronizer = null;
		myViewerRefreshRunnable = null;
		myViewer = null;
		unloadAllResources();
		((TransactionalEditingDomain) myEditingDomain).dispose();
		myEditingDomain = null;
	}

	/**
	 * @generated
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		myViewer = viewer;
	}

	/**
	 * @generated
	 */
	void unloadAllResources() {
		for (Resource nextResource : myEditingDomain.getResourceSet()
				.getResources()) {
			nextResource.unload();
		}
	}

	/**
	 * @generated
	 */
	void asyncRefresh() {
		if (myViewer != null && !myViewer.getControl().isDisposed()) {
			myViewer.getControl().getDisplay()
					.asyncExec(myViewerRefreshRunnable);
		}
	}

	/**
	 * @generated
	 */
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			IFile file = (IFile) parentElement;
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath()
					.toString(), true);
			Resource resource = myEditingDomain.getResourceSet().getResource(
					fileURI, true);
			ArrayList<BaselineNavigatorItem> result = new ArrayList<BaselineNavigatorItem>();
			ArrayList<View> topViews = new ArrayList<View>(resource
					.getContents().size());
			for (EObject o : resource.getContents()) {
				if (o instanceof View) {
					topViews.add((View) o);
				}
			}
			result.addAll(createNavigatorItems(
					selectViewsByType(topViews, BaseFrameworkEditPart.MODEL_ID),
					file, false));
			return result.toArray();
		}

		if (parentElement instanceof BaselineNavigatorGroup) {
			BaselineNavigatorGroup group = (BaselineNavigatorGroup) parentElement;
			return group.getChildren();
		}

		if (parentElement instanceof BaselineNavigatorItem) {
			BaselineNavigatorItem navigatorItem = (BaselineNavigatorItem) parentElement;
			if (navigatorItem.isLeaf() || !isOwnView(navigatorItem.getView())) {
				return EMPTY_ARRAY;
			}
			return getViewChildren(navigatorItem.getView(), parentElement);
		}

		/*
		 * Due to plugin.xml restrictions this code will be called only for views representing
		 * shortcuts to this diagram elements created on other diagrams. 
		 */
		if (parentElement instanceof IAdaptable) {
			View view = (View) ((IAdaptable) parentElement)
					.getAdapter(View.class);
			if (view != null) {
				return getViewChildren(view, parentElement);
			}
		}

		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Object[] getViewChildren(View view, Object parentElement) {
		switch (BaselineVisualIDRegistry.getVisualID(view)) {

		case BaseFrameworkEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			result.addAll(getForeignShortcuts((Diagram) view, parentElement));
			Diagram sv = (Diagram) view;
			BaselineNavigatorGroup links = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseFramework_1000_links,
					"icons/linksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseArtefactEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseRoleEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRequiredArtefactEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityProducedArtefactEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityPrecedingActivityEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRoleEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			if (!links.isEmpty()) {
				result.add(links);
			}
			return result.toArray();
		}

		case BaseActivityEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Node sv = (Node) view;
			BaselineNavigatorGroup outgoinglinks = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivity_2001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			BaselineNavigatorGroup incominglinks = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivity_2001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityBaseActivitySubActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					BaselineVisualIDRegistry
							.getType(BaseActivity2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRequiredArtefactEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityProducedArtefactEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityPrecedingActivityEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityPrecedingActivityEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRoleEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case BaseArtefactEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Node sv = (Node) view;
			BaselineNavigatorGroup incominglinks = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseArtefact_2002_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRequiredArtefactEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityProducedArtefactEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case BaseRoleEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Node sv = (Node) view;
			BaselineNavigatorGroup incominglinks = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseRole_2003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRoleEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case BaseActivity2EditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Node sv = (Node) view;
			BaselineNavigatorGroup outgoinglinks = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivity_3001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			BaselineNavigatorGroup incominglinks = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivity_3001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityBaseActivitySubActivityCompartment2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					BaselineVisualIDRegistry
							.getType(BaseActivity2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRequiredArtefactEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityProducedArtefactEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityPrecedingActivityEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityPrecedingActivityEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityRoleEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case BaseActivityRequiredArtefactEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			BaselineNavigatorGroup target = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityRequiredArtefact_4001_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			BaselineNavigatorGroup source = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityRequiredArtefact_4001_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseArtefactEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivity2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case BaseActivityProducedArtefactEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			BaselineNavigatorGroup target = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityProducedArtefact_4002_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			BaselineNavigatorGroup source = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityProducedArtefact_4002_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseArtefactEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivity2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case BaseActivityPrecedingActivityEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			BaselineNavigatorGroup target = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityPrecedingActivity_4003_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			BaselineNavigatorGroup source = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityPrecedingActivity_4003_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivity2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivity2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case BaseActivityRoleEditPart.VISUAL_ID: {
			LinkedList<BaselineAbstractNavigatorItem> result = new LinkedList<BaselineAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			BaselineNavigatorGroup target = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityRole_4004_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			BaselineNavigatorGroup source = new BaselineNavigatorGroup(
					Messages.NavigatorGroupName_BaseActivityRole_4004_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseRoleEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivityEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					BaselineVisualIDRegistry
							.getType(BaseActivity2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}
		}
		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksSourceByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeSource = nextEdge.getSource();
			if (type.equals(nextEdgeSource.getType())
					&& isOwnView(nextEdgeSource)) {
				result.add(nextEdgeSource);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksTargetByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeTarget = nextEdge.getTarget();
			if (type.equals(nextEdgeTarget.getType())
					&& isOwnView(nextEdgeTarget)) {
				result.add(nextEdgeTarget);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getOutgoingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getSourceEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getIncomingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getTargetEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getChildrenByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getChildren(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getDiagramLinksByType(
			Collection<Diagram> diagrams, String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (Diagram nextDiagram : diagrams) {
			result.addAll(selectViewsByType(nextDiagram.getEdges(), type));
		}
		return result;
	}

	// TODO refactor as static method
	/**
	 * @generated
	 */
	private Collection<View> selectViewsByType(Collection<View> views,
			String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (View nextView : views) {
			if (type.equals(nextView.getType()) && isOwnView(nextView)) {
				result.add(nextView);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return BaseFrameworkEditPart.MODEL_ID.equals(BaselineVisualIDRegistry
				.getModelID(view));
	}

	/**
	 * @generated
	 */
	private Collection<BaselineNavigatorItem> createNavigatorItems(
			Collection<View> views, Object parent, boolean isLeafs) {
		ArrayList<BaselineNavigatorItem> result = new ArrayList<BaselineNavigatorItem>(
				views.size());
		for (View nextView : views) {
			result.add(new BaselineNavigatorItem(nextView, parent, isLeafs));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<BaselineNavigatorItem> getForeignShortcuts(
			Diagram diagram, Object parent) {
		LinkedList<View> result = new LinkedList<View>();
		for (Iterator<View> it = diagram.getChildren().iterator(); it.hasNext();) {
			View nextView = it.next();
			if (!isOwnView(nextView)
					&& nextView.getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				result.add(nextView);
			}
		}
		return createNavigatorItems(result, parent, false);
	}

	/**
	 * @generated
	 */
	public Object getParent(Object element) {
		if (element instanceof BaselineAbstractNavigatorItem) {
			BaselineAbstractNavigatorItem abstractNavigatorItem = (BaselineAbstractNavigatorItem) element;
			return abstractNavigatorItem.getParent();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean hasChildren(Object element) {
		return element instanceof IFile || getChildren(element).length > 0;
	}

}
