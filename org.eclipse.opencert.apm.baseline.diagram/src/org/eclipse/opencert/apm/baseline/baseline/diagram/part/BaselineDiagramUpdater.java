/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartmentEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityPrecedingActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityProducedArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRequiredArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;

/**
 * @generated
 */
public class BaselineDiagramUpdater {

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null
				|| view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List<BaselineNodeDescriptor> getSemanticChildren(View view) {
		switch (BaselineVisualIDRegistry.getVisualID(view)) {
		case BaseFrameworkEditPart.VISUAL_ID:
			return getBaseFramework_1000SemanticChildren(view);
		case BaseActivityBaseActivitySubActivityCompartmentEditPart.VISUAL_ID:
			return getBaseActivityBaseActivitySubActivityCompartment_7001SemanticChildren(view);
		case BaseActivityBaseActivitySubActivityCompartment2EditPart.VISUAL_ID:
			return getBaseActivityBaseActivitySubActivityCompartment_7002SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineNodeDescriptor> getBaseFramework_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		BaseFramework modelElement = (BaseFramework) view.getElement();
		LinkedList<BaselineNodeDescriptor> result = new LinkedList<BaselineNodeDescriptor>();
		for (Iterator<?> it = modelElement.getOwnedActivities().iterator(); it
				.hasNext();) {
			BaseActivity childElement = (BaseActivity) it.next();
			int visualID = BaselineVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == BaseActivityEditPart.VISUAL_ID) {
				result.add(new BaselineNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getOwnedArtefact().iterator(); it
				.hasNext();) {
			BaseArtefact childElement = (BaseArtefact) it.next();
			int visualID = BaselineVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == BaseArtefactEditPart.VISUAL_ID) {
				result.add(new BaselineNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getOwnedRole().iterator(); it
				.hasNext();) {
			BaseRole childElement = (BaseRole) it.next();
			int visualID = BaselineVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == BaseRoleEditPart.VISUAL_ID) {
				result.add(new BaselineNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineNodeDescriptor> getBaseActivityBaseActivitySubActivityCompartment_7001SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		BaseActivity modelElement = (BaseActivity) containerView.getElement();
		LinkedList<BaselineNodeDescriptor> result = new LinkedList<BaselineNodeDescriptor>();
		for (Iterator<?> it = modelElement.getSubActivity().iterator(); it
				.hasNext();) {
			BaseActivity childElement = (BaseActivity) it.next();
			int visualID = BaselineVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == BaseActivity2EditPart.VISUAL_ID) {
				result.add(new BaselineNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineNodeDescriptor> getBaseActivityBaseActivitySubActivityCompartment_7002SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		BaseActivity modelElement = (BaseActivity) containerView.getElement();
		LinkedList<BaselineNodeDescriptor> result = new LinkedList<BaselineNodeDescriptor>();
		for (Iterator<?> it = modelElement.getSubActivity().iterator(); it
				.hasNext();) {
			BaseActivity childElement = (BaseActivity) it.next();
			int visualID = BaselineVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == BaseActivity2EditPart.VISUAL_ID) {
				result.add(new BaselineNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getContainedLinks(View view) {
		switch (BaselineVisualIDRegistry.getVisualID(view)) {
		case BaseFrameworkEditPart.VISUAL_ID:
			return getBaseFramework_1000ContainedLinks(view);
		case BaseActivityEditPart.VISUAL_ID:
			return getBaseActivity_2001ContainedLinks(view);
		case BaseArtefactEditPart.VISUAL_ID:
			return getBaseArtefact_2002ContainedLinks(view);
		case BaseRoleEditPart.VISUAL_ID:
			return getBaseRole_2003ContainedLinks(view);
		case BaseActivity2EditPart.VISUAL_ID:
			return getBaseActivity_3001ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getIncomingLinks(View view) {
		switch (BaselineVisualIDRegistry.getVisualID(view)) {
		case BaseActivityEditPart.VISUAL_ID:
			return getBaseActivity_2001IncomingLinks(view);
		case BaseArtefactEditPart.VISUAL_ID:
			return getBaseArtefact_2002IncomingLinks(view);
		case BaseRoleEditPart.VISUAL_ID:
			return getBaseRole_2003IncomingLinks(view);
		case BaseActivity2EditPart.VISUAL_ID:
			return getBaseActivity_3001IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getOutgoingLinks(View view) {
		switch (BaselineVisualIDRegistry.getVisualID(view)) {
		case BaseActivityEditPart.VISUAL_ID:
			return getBaseActivity_2001OutgoingLinks(view);
		case BaseArtefactEditPart.VISUAL_ID:
			return getBaseArtefact_2002OutgoingLinks(view);
		case BaseRoleEditPart.VISUAL_ID:
			return getBaseRole_2003OutgoingLinks(view);
		case BaseActivity2EditPart.VISUAL_ID:
			return getBaseActivity_3001OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseFramework_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseActivity_2001ContainedLinks(
			View view) {
		BaseActivity modelElement = (BaseActivity) view.getElement();
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseArtefact_2002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseRole_2003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseActivity_3001ContainedLinks(
			View view) {
		BaseActivity modelElement = (BaseActivity) view.getElement();
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseActivity_2001IncomingLinks(
			View view) {
		BaseActivity modelElement = (BaseActivity) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseArtefact_2002IncomingLinks(
			View view) {
		BaseArtefact modelElement = (BaseArtefact) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_BaseActivity_RequiredArtefact_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_BaseActivity_ProducedArtefact_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseRole_2003IncomingLinks(
			View view) {
		BaseRole modelElement = (BaseRole) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_BaseActivity_Role_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseActivity_3001IncomingLinks(
			View view) {
		BaseActivity modelElement = (BaseActivity) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseActivity_2001OutgoingLinks(
			View view) {
		BaseActivity modelElement = (BaseActivity) view.getElement();
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseArtefact_2002OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseRole_2003OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<BaselineLinkDescriptor> getBaseActivity_3001OutgoingLinks(
			View view) {
		BaseActivity modelElement = (BaseActivity) view.getElement();
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_BaseActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getIncomingFeatureModelFacetLinks_BaseActivity_RequiredArtefact_4001(
			BaseArtefact target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == BaselinePackage.eINSTANCE
					.getBaseActivity_RequiredArtefact()) {
				result.add(new BaselineLinkDescriptor(setting.getEObject(),
						target,
						BaselineElementTypes.BaseActivityRequiredArtefact_4001,
						BaseActivityRequiredArtefactEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getIncomingFeatureModelFacetLinks_BaseActivity_ProducedArtefact_4002(
			BaseArtefact target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == BaselinePackage.eINSTANCE
					.getBaseActivity_ProducedArtefact()) {
				result.add(new BaselineLinkDescriptor(setting.getEObject(),
						target,
						BaselineElementTypes.BaseActivityProducedArtefact_4002,
						BaseActivityProducedArtefactEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getIncomingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(
			BaseActivity target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == BaselinePackage.eINSTANCE
					.getBaseActivity_PrecedingActivity()) {
				result.add(new BaselineLinkDescriptor(
						setting.getEObject(),
						target,
						BaselineElementTypes.BaseActivityPrecedingActivity_4003,
						BaseActivityPrecedingActivityEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getIncomingFeatureModelFacetLinks_BaseActivity_Role_4004(
			BaseRole target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == BaselinePackage.eINSTANCE
					.getBaseActivity_Role()) {
				result.add(new BaselineLinkDescriptor(setting.getEObject(),
						target, BaselineElementTypes.BaseActivityRole_4004,
						BaseActivityRoleEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getOutgoingFeatureModelFacetLinks_BaseActivity_RequiredArtefact_4001(
			BaseActivity source) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		for (Iterator<?> destinations = source.getRequiredArtefact().iterator(); destinations
				.hasNext();) {
			BaseArtefact destination = (BaseArtefact) destinations.next();
			result.add(new BaselineLinkDescriptor(source, destination,
					BaselineElementTypes.BaseActivityRequiredArtefact_4001,
					BaseActivityRequiredArtefactEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getOutgoingFeatureModelFacetLinks_BaseActivity_ProducedArtefact_4002(
			BaseActivity source) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		for (Iterator<?> destinations = source.getProducedArtefact().iterator(); destinations
				.hasNext();) {
			BaseArtefact destination = (BaseArtefact) destinations.next();
			result.add(new BaselineLinkDescriptor(source, destination,
					BaselineElementTypes.BaseActivityProducedArtefact_4002,
					BaseActivityProducedArtefactEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getOutgoingFeatureModelFacetLinks_BaseActivity_PrecedingActivity_4003(
			BaseActivity source) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		for (Iterator<?> destinations = source.getPrecedingActivity()
				.iterator(); destinations.hasNext();) {
			BaseActivity destination = (BaseActivity) destinations.next();
			result.add(new BaselineLinkDescriptor(source, destination,
					BaselineElementTypes.BaseActivityPrecedingActivity_4003,
					BaseActivityPrecedingActivityEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<BaselineLinkDescriptor> getOutgoingFeatureModelFacetLinks_BaseActivity_Role_4004(
			BaseActivity source) {
		LinkedList<BaselineLinkDescriptor> result = new LinkedList<BaselineLinkDescriptor>();
		for (Iterator<?> destinations = source.getRole().iterator(); destinations
				.hasNext();) {
			BaseRole destination = (BaseRole) destinations.next();
			result.add(new BaselineLinkDescriptor(source, destination,
					BaselineElementTypes.BaseActivityRole_4004,
					BaseActivityRoleEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<BaselineNodeDescriptor> getSemanticChildren(View view) {
			return BaselineDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<BaselineLinkDescriptor> getContainedLinks(View view) {
			return BaselineDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<BaselineLinkDescriptor> getIncomingLinks(View view) {
			return BaselineDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<BaselineLinkDescriptor> getOutgoingLinks(View view) {
			return BaselineDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
