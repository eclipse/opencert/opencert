/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityName2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleNameEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.parsers.MessageFormatParser;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;

/**
 * @generated
 */
public class BaselineParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser baseActivityName_5002Parser;

	/**
	 * @generated
	 */
	private IParser getBaseActivityName_5002Parser() {
		if (baseActivityName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			baseActivityName_5002Parser = parser;
		}
		return baseActivityName_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser baseArtefactName_5003Parser;

	/**
	 * @generated
	 */
	private IParser getBaseArtefactName_5003Parser() {
		if (baseArtefactName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			baseArtefactName_5003Parser = parser;
		}
		return baseArtefactName_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser baseRoleName_5004Parser;

	/**
	 * @generated
	 */
	private IParser getBaseRoleName_5004Parser() {
		if (baseRoleName_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			baseRoleName_5004Parser = parser;
		}
		return baseRoleName_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser baseActivityName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getBaseActivityName_5001Parser() {
		if (baseActivityName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			baseActivityName_5001Parser = parser;
		}
		return baseActivityName_5001Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case BaseActivityNameEditPart.VISUAL_ID:
			return getBaseActivityName_5002Parser();
		case BaseArtefactNameEditPart.VISUAL_ID:
			return getBaseArtefactName_5003Parser();
		case BaseRoleNameEditPart.VISUAL_ID:
			return getBaseRoleName_5004Parser();
		case BaseActivityName2EditPart.VISUAL_ID:
			return getBaseActivityName_5001Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(BaselineVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(BaselineVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (BaselineElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
