/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes;
import org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineModelingAssistantProvider;

/**
 * @generated
 */
public class BaselineModelingAssistantProviderOfBaseActivityEditPart extends
		BaselineModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((BaseActivityEditPart) sourceEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSource(BaseActivityEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(4);
		types.add(BaselineElementTypes.BaseActivityRequiredArtefact_4001);
		types.add(BaselineElementTypes.BaseActivityProducedArtefact_4002);
		types.add(BaselineElementTypes.BaseActivityPrecedingActivity_4003);
		types.add(BaselineElementTypes.BaseActivityRole_4004);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget(
				(BaseActivityEditPart) sourceEditPart, targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSourceAndTarget(
			BaseActivityEditPart source, IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof BaseArtefactEditPart) {
			types.add(BaselineElementTypes.BaseActivityRequiredArtefact_4001);
		}
		if (targetEditPart instanceof BaseArtefactEditPart) {
			types.add(BaselineElementTypes.BaseActivityProducedArtefact_4002);
		}
		if (targetEditPart instanceof BaseActivityEditPart) {
			types.add(BaselineElementTypes.BaseActivityPrecedingActivity_4003);
		}
		if (targetEditPart instanceof BaseActivity2EditPart) {
			types.add(BaselineElementTypes.BaseActivityPrecedingActivity_4003);
		}
		if (targetEditPart instanceof BaseRoleEditPart) {
			types.add(BaselineElementTypes.BaseActivityRole_4004);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((BaseActivityEditPart) sourceEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForTarget(BaseActivityEditPart source,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == BaselineElementTypes.BaseActivityRequiredArtefact_4001) {
			types.add(BaselineElementTypes.BaseArtefact_2002);
		} else if (relationshipType == BaselineElementTypes.BaseActivityProducedArtefact_4002) {
			types.add(BaselineElementTypes.BaseArtefact_2002);
		} else if (relationshipType == BaselineElementTypes.BaseActivityPrecedingActivity_4003) {
			types.add(BaselineElementTypes.BaseActivity_2001);
			types.add(BaselineElementTypes.BaseActivity_3001);
		} else if (relationshipType == BaselineElementTypes.BaseActivityRole_4004) {
			types.add(BaselineElementTypes.BaseRole_2003);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((BaseActivityEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(BaseActivityEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(1);
		types.add(BaselineElementTypes.BaseActivityPrecedingActivity_4003);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((BaseActivityEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(BaseActivityEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == BaselineElementTypes.BaseActivityPrecedingActivity_4003) {
			types.add(BaselineElementTypes.BaseActivity_2001);
			types.add(BaselineElementTypes.BaseActivity_3001);
		}
		return types;
	}

}
