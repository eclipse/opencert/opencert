/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;

/**
 * @generated
 */
public class BaselineEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (BaselineVisualIDRegistry.getVisualID(view)) {

			case BaseFrameworkEditPart.VISUAL_ID:
				return new BaseFrameworkEditPart(view);

			case BaseActivityEditPart.VISUAL_ID:
				return new BaseActivityEditPart(view);

			case BaseActivityNameEditPart.VISUAL_ID:
				return new BaseActivityNameEditPart(view);

			case BaseArtefactEditPart.VISUAL_ID:
				return new BaseArtefactEditPart(view);

			case BaseArtefactNameEditPart.VISUAL_ID:
				return new BaseArtefactNameEditPart(view);

			case BaseRoleEditPart.VISUAL_ID:
				return new BaseRoleEditPart(view);

			case BaseRoleNameEditPart.VISUAL_ID:
				return new BaseRoleNameEditPart(view);

			case BaseActivity2EditPart.VISUAL_ID:
				return new BaseActivity2EditPart(view);

			case BaseActivityName2EditPart.VISUAL_ID:
				return new BaseActivityName2EditPart(view);

			case BaseActivityBaseActivitySubActivityCompartmentEditPart.VISUAL_ID:
				return new BaseActivityBaseActivitySubActivityCompartmentEditPart(
						view);

			case BaseActivityBaseActivitySubActivityCompartment2EditPart.VISUAL_ID:
				return new BaseActivityBaseActivitySubActivityCompartment2EditPart(
						view);

			case BaseActivityRequiredArtefactEditPart.VISUAL_ID:
				return new BaseActivityRequiredArtefactEditPart(view);

			case WrappingLabelEditPart.VISUAL_ID:
				return new WrappingLabelEditPart(view);

			case BaseActivityProducedArtefactEditPart.VISUAL_ID:
				return new BaseActivityProducedArtefactEditPart(view);

			case WrappingLabel2EditPart.VISUAL_ID:
				return new WrappingLabel2EditPart(view);

			case BaseActivityPrecedingActivityEditPart.VISUAL_ID:
				return new BaseActivityPrecedingActivityEditPart(view);

			case WrappingLabel3EditPart.VISUAL_ID:
				return new WrappingLabel3EditPart(view);

			case BaseActivityRoleEditPart.VISUAL_ID:
				return new BaseActivityRoleEditPart(view);

			case BaseRoleLabelEditPart.VISUAL_ID:
				return new BaseRoleLabelEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
