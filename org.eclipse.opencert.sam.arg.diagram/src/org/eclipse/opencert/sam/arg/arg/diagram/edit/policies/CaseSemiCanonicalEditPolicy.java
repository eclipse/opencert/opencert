/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.impl.CDOResourceImpl;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.internal.cdo.object.CDOLegacyAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.DeferredLayoutCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.SetViewMutabilityCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.emf.core.resources.GMFResource;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.AssertedChallenge;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.ModelElement;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedChallengeEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedCounterEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.Choice2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramUpdater;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgLinkDescriptor;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgNodeDescriptor;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.sam.arg.arg.impl.CaseImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl;

/**
 * @generated NOT
 */
/**
 * The Class CaseSemiCanonicalEditPolicy.
 * 
 * @author  M� Carmen Palacios
 */
public class CaseSemiCanonicalEditPolicy extends CanonicalEditPolicy {
	// Start MCP
	private Map<EObject, String> eObjectToIDMap = null;
	private List<EObject> listRootObjects = null;
	public void setObjects(List<EObject> lro)
	{
		listRootObjects = lro;
		for(EObject element : this.listRootObjects)
		{
			if(element instanceof CaseImpl)
				System.out.println("CaseSemiCanonicalEditPolicy.setObject(): Error object not allowed - " + element.getClass().getCanonicalName());
		}
	}
	// End MCP

	/**
	 * @generated
	 */
	private Set<EStructuralFeature> myFeaturesToSynchronize;

	/**
	 * @generated
	 */
	protected void refreshOnActivate() {
		// Need to activate editpart children before invoking the canonical refresh for EditParts to add event listeners
		List<?> c = getHost().getChildren();
		for (int i = 0; i < c.size(); i++) {
			((EditPart) c.get(i)).activate();
		}
		super.refreshOnActivate();
	}

	/**
	 * @generated
	 */
	protected Set getFeaturesToSynchronize() {
		if (myFeaturesToSynchronize == null) {
			myFeaturesToSynchronize = new HashSet<EStructuralFeature>();
			myFeaturesToSynchronize
					.add(ArgPackage.eINSTANCE.getCase_Argument());
			myFeaturesToSynchronize.add(ArgPackage.eINSTANCE
					.getCase_Argumentation());
		}
		return myFeaturesToSynchronize;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		LinkedList<EObject> result = new LinkedList<EObject>();
		List<ArgNodeDescriptor> childDescriptors = ArgDiagramUpdater
				.getCase_1000SemanticChildren(viewObject);
		for (ArgNodeDescriptor d : childDescriptors) {
			result.add(d.getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection<EObject> semanticChildren,
			final View view) {
		if (isShortcut(view)) {
			return ArgDiagramUpdater.isShortcutOrphaned(view);
		}
		return isMyDiagramElement(view)
				&& !semanticChildren.contains(view.getElement());
	}

	/**
	 * @generated NOT
	 */
	private boolean isMyDiagramElement(View view) {
		int visualID = ArgVisualIDRegistry.getVisualID(view);
		// Start MCP
		boolean res2 = false;
		if(view.getElement() != null)
		{
			DiagramImpl	diagramImpl =	(DiagramImpl)getHost().getModel();
			CaseImpl cs = (CaseImpl) diagramImpl.basicGetElement();
			GMFResource aa = null; CDOResource bb = null;
			if(cs.eResource() instanceof GMFResource)
				aa =(GMFResource) cs.eResource();
			else if(cs.eResource() instanceof CDOResource)
			{
				bb =(CDOResource) cs.eResource();
			}

			switch (visualID) {
			case ClaimEditPart.VISUAL_ID:
			case ArgumentReasoningEditPart.VISUAL_ID:
			case InformationElementCitationEditPart.VISUAL_ID:
			case ArgumentElementCitationEditPart.VISUAL_ID:
			case ChoiceEditPart.VISUAL_ID:
			case AgreementEditPart.VISUAL_ID:
			case ArgumentationEditPart.VISUAL_ID:
				ModelElement me = (ModelElement)ViewUtil.resolveSemanticElement(view);
				if(me != null) 
				{
					String meID = "-1";
					if (aa != null) meID = aa.getID(me);
					else if (bb != null) meID = getID(me);
					
					for(EObject elem : this.listRootObjects)
					{
						String rootObjectId = "-1";
						if (aa != null) rootObjectId= aa.getID(elem);
						else if (bb != null) rootObjectId = getID(elem);
						
						if(rootObjectId == null) // Dawn cdo. a veces incluyo las relaciones y dan problemas y otras veces no
						{
							continue;
						}
						
						if(meID!= null && meID.compareToIgnoreCase(rootObjectId) == 0 &&
						   me.getClass().equals(elem.getClass())) // hay varios elementos con el mismo UUID
						{
							res2 = true;
							break;
						}
					}
				}
			}
		}
        // End MCP
		
		switch (visualID) {
		case ClaimEditPart.VISUAL_ID:
		case ArgumentReasoningEditPart.VISUAL_ID:
		case InformationElementCitationEditPart.VISUAL_ID:
		case ArgumentElementCitationEditPart.VISUAL_ID:
		case ChoiceEditPart.VISUAL_ID:
		case AgreementEditPart.VISUAL_ID:
		case ArgumentationEditPart.VISUAL_ID:
			// Start MCP
			// return true;
			return true && res2;
			// End MCP
		}
		
		return false;
	}

	/**
	 * @generated
	 */
	protected static boolean isShortcut(View view) {
		return view.getEAnnotation("Shortcut") != null; //$NON-NLS-1$
	}
	
	/**
	 * @generated NOT
	 */
	// Start MCP
	private void setIDs() {
		eObjectToIDMap = new HashMap<EObject, String>();
		
		List<EObject> result = getSemanticChildrenList();
		
		for(EObject elem : result)
		{
			/* Si CDO:
			CDOLegacyAdapter cdaA = (CDOLegacyAdapter)elem.eAdapters().get(0);
			eObjectToIDMap.put(elem, cdaA.cdoID().toString());
			*/
    		// Si CDO Native
	        CDOID cdoID = CDOUtil.getCDOObject(elem).cdoID();
	        Long  cdoIdLong = new Long(cdoID.toURIFragment());
			eObjectToIDMap.put(elem, cdoID.toString());
		}
	}	
	// NO VALE!!!! SON LOS IDs del modelo no de indexacion
	/*
	private void setIDs(CaseImpl cs2) {
		eObjectToIDMap = new HashMap<EObject, String>();
		
		for(Agreement elem : cs2.getAgreement())
		{
			eObjectToIDMap.put(elem, elem.getId());
		}
		for(ArgumentElement elem : cs2.getArgument())
		{
			eObjectToIDMap.put(elem, elem.getId());
		}
		for(Argumentation elem : cs2.getArgumentation())
		{
			eObjectToIDMap.put(elem, elem.getId());
		}
		for(ArgumentElementCitation elem : cs2.getCited())
		{
			eObjectToIDMap.put(elem, elem.getId());
		}
		for(InformationElementCitation elem : cs2.getInformation())
		{
			eObjectToIDMap.put(elem, elem.getId());
		}
	}
	*/
	// End MCP	
	
	/**
	 * @generated NOT
	 */
	// Start MCP	
	private String getID(EObject eObject) {
	    if (eObjectToIDMap == null)
	    {
	      return null;
	    }
	    else
	    {
	      return eObjectToIDMap.get(eObject);
	    }
	  }
	// End MCP

	/**
	 * @generated NOT
	 */
	protected void refreshSemantic() {
		if (resolveSemanticElement() == null) {
			return;
		}
		LinkedList<IAdaptable> createdViews = new LinkedList<IAdaptable>();
		List<ArgNodeDescriptor> childDescriptors = ArgDiagramUpdater
				.getCase_1000SemanticChildren((View) getHost().getModel());
		// Start MCP
		//eliminar los hijos que no son de este diagram
		DiagramImpl	diagramImpl =	(DiagramImpl)getHost().getModel();
		CaseImpl cs = (CaseImpl) diagramImpl.basicGetElement();
		GMFResource aa = null; CDOResource bb = null;
		if(cs.eResource() instanceof GMFResource)
			aa =(GMFResource) cs.eResource();
		else if(cs.eResource() instanceof CDOResource)
		{
			bb =(CDOResource) cs.eResource();
			//setIDs((CaseImpl)bb.eContents().get(0));
			setIDs();
		}

		Set<String> rootObjectIds = new HashSet<String>();
		for(EObject elem : this.listRootObjects)
		{
			if(false ==  elem instanceof CaseImpl)
			{
				String rootObjectId = "-1";
				if (aa != null) rootObjectId= aa.getID(elem);
				else if (bb != null) rootObjectId = getID(elem); 
				rootObjectIds.add(rootObjectId);
			}
		}
		
		/* Start Dawn cdo */
		/* MCP: MCP: ASI NO SE CONSIGUE NADA!!!. CASCA al crear la view!!!
		for(EObject elem : this.listRootObjects)
		{
			if(true ==  elem instanceof ChoiceImpl)
			{			
				childDescriptors.add(new ArgNodeDescriptor(elem, Choice2EditPart.VISUAL_ID));
			}
		}
		*/
		/* End Dawn cdo */
		
		LinkedList<ArgNodeDescriptor> c = new LinkedList<ArgNodeDescriptor>();
		for(ArgNodeDescriptor next : childDescriptors)
		{
			ModelElement me = (ModelElement)next.getModelElement();
			if(me != null) 
			{
				String meID = "-1";
				if (aa != null) meID = aa.getID(me);
				else if (bb != null) meID = getID(me);
				
				if(meID == null || !rootObjectIds.contains(meID))
				{
					c.add(next);
				}
				else if (meID != null && rootObjectIds.contains(meID))
				{
					boolean OK = false;
					for(EObject elem : this.listRootObjects) // hay varios elementos con el mismo UUID
					{
						if(me.getClass().equals(elem.getClass()))
						{
							OK = true;
							break;
						}
					}
					if(!OK) c.add(next);
				}
			}			
		}
		childDescriptors.removeAll(c);
		//End MCP
		LinkedList<View> orphaned = new LinkedList<View>();
		// we care to check only views we recognize as ours and not shortcuts
		LinkedList<View> knownViewChildren = new LinkedList<View>();
		for (View v : getViewChildren()) {
			if (isShortcut(v)) {
				if (ArgDiagramUpdater.isShortcutOrphaned(v)) {
					orphaned.add(v);
				}
				continue;
			}
			if (isMyDiagramElement(v)) {
				knownViewChildren.add(v);
			}
		}
		// alternative to #cleanCanonicalSemanticChildren(getViewChildren(), semanticChildren)
		//
		// iteration happens over list of desired semantic elements, trying to find best matching View, while original CEP
		// iterates views, potentially losing view (size/bounds) information - i.e. if there are few views to reference same EObject, only last one 
		// to answer isOrphaned == true will be used for the domain element representation, see #cleanCanonicalSemanticChildren()
		for (Iterator<ArgNodeDescriptor> descriptorsIterator = childDescriptors
				.iterator(); descriptorsIterator.hasNext();) {
			ArgNodeDescriptor next = descriptorsIterator.next();
			String hint = ArgVisualIDRegistry.getType(next.getVisualID());
			LinkedList<View> perfectMatch = new LinkedList<View>(); // both semanticElement and hint match that of NodeDescriptor
			for (View childView : getViewChildren()) {
				EObject semanticElement = childView.getElement();
				if (next.getModelElement().equals(semanticElement)) {
					if (hint.equals(childView.getType())) {
						perfectMatch.add(childView);
						// actually, can stop iteration over view children here, but
						// may want to use not the first view but last one as a 'real' match (the way original CEP does
						// with its trick with viewToSemanticMap inside #cleanCanonicalSemanticChildren
					}
				}
			}
			if (perfectMatch.size() > 0) {
				descriptorsIterator.remove(); // precise match found no need to create anything for the NodeDescriptor
				// use only one view (first or last?), keep rest as orphaned for further consideration
				knownViewChildren.remove(perfectMatch.getFirst());
			}
		}
		// those left in knownViewChildren are subject to removal - they are our diagram elements we didn't find match to,
		// or those we have potential matches to, and thus need to be recreated, preserving size/location information.
		orphaned.addAll(knownViewChildren);
		//
		ArrayList<CreateViewRequest.ViewDescriptor> viewDescriptors = new ArrayList<CreateViewRequest.ViewDescriptor>(
				childDescriptors.size());
		for (ArgNodeDescriptor next : childDescriptors) {
			String hint = ArgVisualIDRegistry.getType(next.getVisualID());
			IAdaptable elementAdapter = new CanonicalElementAdapter(
					next.getModelElement(), hint);
			CreateViewRequest.ViewDescriptor descriptor = new CreateViewRequest.ViewDescriptor(
					elementAdapter, Node.class, hint, ViewUtil.APPEND, false,
					host().getDiagramPreferencesHint());
			viewDescriptors.add(descriptor);
		}

		boolean changed = deleteViews(orphaned.iterator());
		//
		CreateViewRequest request = getCreateViewRequest(viewDescriptors);
		Command cmd = getCreateViewCommand(request);
		if (cmd != null && cmd.canExecute()) {
			SetViewMutabilityCommand.makeMutable(
					new EObjectAdapter(host().getNotationView())).execute();
			executeCommand(cmd);
			@SuppressWarnings("unchecked")
			List<IAdaptable> nl = (List<IAdaptable>) request.getNewObject();
			createdViews.addAll(nl);
		}
		if (changed || createdViews.size() > 0) {
			postProcessRefreshSemantic(createdViews);
		}

		Collection<IAdaptable> createdConnectionViews = refreshConnections();

		if (createdViews.size() > 1) {
			// perform a layout of the container
			DeferredLayoutCommand layoutCmd = new DeferredLayoutCommand(host()
					.getEditingDomain(), createdViews, host());
			executeCommand(new ICommandProxy(layoutCmd));
		}

		createdViews.addAll(createdConnectionViews);

		makeViewsImmutable(createdViews);
	}

	/**
	 * @generated
	 */
	private Collection<IAdaptable> refreshConnections() {
		Domain2Notation domain2NotationMap = new Domain2Notation();
		Collection<ArgLinkDescriptor> linkDescriptors = collectAllLinks(
				getDiagram(), domain2NotationMap);
		Collection existingLinks = new LinkedList(getDiagram().getEdges());
		for (Iterator linksIterator = existingLinks.iterator(); linksIterator
				.hasNext();) {
			Edge nextDiagramLink = (Edge) linksIterator.next();
			int diagramLinkVisualID = ArgVisualIDRegistry
					.getVisualID(nextDiagramLink);
			if (diagramLinkVisualID == -1) {
				if (nextDiagramLink.getSource() != null
						&& nextDiagramLink.getTarget() != null) {
					linksIterator.remove();
				}
				continue;
			}
			EObject diagramLinkObject = nextDiagramLink.getElement();
			EObject diagramLinkSrc = nextDiagramLink.getSource().getElement();
			EObject diagramLinkDst = nextDiagramLink.getTarget().getElement();
			for (Iterator<ArgLinkDescriptor> linkDescriptorsIterator = linkDescriptors
					.iterator(); linkDescriptorsIterator.hasNext();) {
				ArgLinkDescriptor nextLinkDescriptor = linkDescriptorsIterator
						.next();
				if (diagramLinkObject == nextLinkDescriptor.getModelElement()
						&& diagramLinkSrc == nextLinkDescriptor.getSource()
						&& diagramLinkDst == nextLinkDescriptor
								.getDestination()
						&& diagramLinkVisualID == nextLinkDescriptor
								.getVisualID()) {
					linksIterator.remove();
					linkDescriptorsIterator.remove();
					break;
				}
			}
		}
		deleteViews(existingLinks.iterator());
		return createConnections(linkDescriptors, domain2NotationMap);
	}

	/**
	 * @generated
	 */
	private Collection<ArgLinkDescriptor> collectAllLinks(View view,
			Domain2Notation domain2NotationMap) {
		if (!CaseEditPart.MODEL_ID.equals(ArgVisualIDRegistry.getModelID(view))) {
			return Collections.emptyList();
		}
		LinkedList<ArgLinkDescriptor> result = new LinkedList<ArgLinkDescriptor>();
		switch (ArgVisualIDRegistry.getVisualID(view)) {
		case CaseEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getCase_1000ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case ClaimEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getClaim_2001ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case ArgumentReasoningEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getArgumentReasoning_2003ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case InformationElementCitationEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getInformationElementCitation_2005ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case ArgumentElementCitationEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getArgumentElementCitation_2006ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case ChoiceEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getChoice_2007ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case AgreementEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getAgreement_2002ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case ArgumentationEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getArgumentation_2004ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case Choice2EditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getChoice_3001ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case AssertedInferenceEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getAssertedInference_4001ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case AssertedEvidenceEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getAssertedEvidence_4002ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case AssertedContextEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getAssertedContext_4003ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case AssertedChallengeEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getAssertedChallenge_4004ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		case AssertedCounterEvidenceEditPart.VISUAL_ID: {
			if (!domain2NotationMap.containsKey(view.getElement())) {
				result.addAll(ArgDiagramUpdater
						.getAssertedCounterEvidence_4005ContainedLinks(view));
			}
			domain2NotationMap.putView(view.getElement(), view);
			break;
		}
		}
		for (Iterator children = view.getChildren().iterator(); children
				.hasNext();) {
			result.addAll(collectAllLinks((View) children.next(),
					domain2NotationMap));
		}
		for (Iterator edges = view.getSourceEdges().iterator(); edges.hasNext();) {
			result.addAll(collectAllLinks((View) edges.next(),
					domain2NotationMap));
		}
		return result;
	}

	/**
	 * @generated NOT
	 */
	private Collection<IAdaptable> createConnections(
			Collection<ArgLinkDescriptor> linkDescriptors,
			Domain2Notation domain2NotationMap) {
		LinkedList<IAdaptable> adapters = new LinkedList<IAdaptable>();
		// Start MCP
		// OJO: NO FUNCIONA!!!!!
		// Problemas con AssertedChallenge. Deben ser las ultimas en crearse por si apuntan a otro edge
		Collection<ArgLinkDescriptor> linkDescriptors2 = new LinkedList<ArgLinkDescriptor>();
		List<ArgLinkDescriptor> lastEdges = new ArrayList<ArgLinkDescriptor>();
		for (ArgLinkDescriptor nextLinkDescriptor : linkDescriptors) {
			if(nextLinkDescriptor.getModelElement() instanceof AssertedChallenge)
			{
				lastEdges.add(nextLinkDescriptor);
			}
			else
			{
				linkDescriptors2.add(nextLinkDescriptor);
			}
		}
		// annadir AssertedChallenge edges al final
		if(!lastEdges.isEmpty())
		{
			/// OJO : Falta reordenar cuando un AssertedChallenge apunta a otro AssertedChallenge 
			for(ArgLinkDescriptor nextLinkDescriptor : lastEdges)
			{
				linkDescriptors2.add(nextLinkDescriptor);
			}
		}
		// End MCP
		for (ArgLinkDescriptor nextLinkDescriptor : linkDescriptors2) {
			EditPart sourceEditPart = getSourceEditPart(nextLinkDescriptor,
					domain2NotationMap);
			EditPart targetEditPart = getTargetEditPart(nextLinkDescriptor,
					domain2NotationMap);
			if (sourceEditPart == null || targetEditPart == null) {
				continue;
			}
			CreateConnectionViewRequest.ConnectionViewDescriptor descriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(
					nextLinkDescriptor.getSemanticAdapter(),
					ArgVisualIDRegistry.getType(nextLinkDescriptor
							.getVisualID()), ViewUtil.APPEND, false,
					((IGraphicalEditPart) getHost())
							.getDiagramPreferencesHint());
			CreateConnectionViewRequest ccr = new CreateConnectionViewRequest(
					descriptor);
			ccr.setType(RequestConstants.REQ_CONNECTION_START);
			ccr.setSourceEditPart(sourceEditPart);
			sourceEditPart.getCommand(ccr);
			ccr.setTargetEditPart(targetEditPart);
			ccr.setType(RequestConstants.REQ_CONNECTION_END);
			Command cmd = targetEditPart.getCommand(ccr);
			if (cmd != null && cmd.canExecute()) {
				executeCommand(cmd);
				IAdaptable viewAdapter = (IAdaptable) ccr.getNewObject();
				if (viewAdapter != null) {
					adapters.add(viewAdapter);
				}
			}
		}
		return adapters;
	}

	/**
	 * @generated
	 */
	private EditPart getEditPart(EObject domainModelElement,
			Domain2Notation domain2NotationMap) {
		View view = (View) domain2NotationMap.get(domainModelElement);
		if (view != null) {
			return (EditPart) getHost().getViewer().getEditPartRegistry()
					.get(view);
		}
		return null;
	}

	/**
	 * @generated
	 */
	private Diagram getDiagram() {
		return ((View) getHost().getModel()).getDiagram();
	}

	/**
	 * @generated
	 */
	private EditPart getSourceEditPart(UpdaterLinkDescriptor descriptor,
			Domain2Notation domain2NotationMap) {
		return getEditPart(descriptor.getSource(), domain2NotationMap);
	}

	/**
	 * @generated
	 */
	private EditPart getTargetEditPart(UpdaterLinkDescriptor descriptor,
			Domain2Notation domain2NotationMap) {
		return getEditPart(descriptor.getDestination(), domain2NotationMap);
	}

	/**
	 * @generated
	 */
	protected final EditPart getHintedEditPart(EObject domainModelElement,
			Domain2Notation domain2NotationMap, int hintVisualId) {
		View view = (View) domain2NotationMap.getHinted(domainModelElement,
				ArgVisualIDRegistry.getType(hintVisualId));
		if (view != null) {
			return (EditPart) getHost().getViewer().getEditPartRegistry()
					.get(view);
		}
		return null;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("serial")
	protected static class Domain2Notation extends HashMap<EObject, View> {
		/**
		 * @generated
		 */
		public boolean containsDomainElement(EObject domainElement) {
			return this.containsKey(domainElement);
		}

		/**
		 * @generated
		 */
		public View getHinted(EObject domainEObject, String hint) {
			return this.get(domainEObject);
		}

		/**
		 * @generated
		 */
		public void putView(EObject domainElement, View view) {
			if (!containsKey(view.getElement()) || !isShortcut(view)) {
				this.put(domainElement, view);
			}
		}

	}
}
