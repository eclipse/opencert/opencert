package org.eclipse.opencert.sam.arg.arg.diagram.instantiation;

import java.util.List;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditor;
import org.eclipse.opencert.userguidance.labelparser.LabelParserUtil;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem.ItemHeaderType;
import org.eclipse.opencert.userguidance.labelparser.tokens.Token;

public class InstantiationEMFCommand extends RecordingCommand {

	private String itemBody;
	private String replacement;
	private ArgDiagramEditor editor;

	public InstantiationEMFCommand(String commandName, String itemBody,
			String replacement, ArgDiagramEditor editor) {
		super(editor.getEditingDomain(), commandName);
		this.itemBody = itemBody;
		this.replacement = replacement;
		this.editor = editor;
	}

	@Override
	protected void doExecute() {
		Diagram diagram = (Diagram) editor.getDiagramEditPart().getModel();
		Case rootElement = (Case) diagram.getElement();

		for (ArgumentElement argumentElement : rootElement.getArgument()) {
			while (replaceInArgumentElement(argumentElement)) {
			}

		}
	}

	private boolean replaceInArgumentElement(ArgumentElement argumentElement) {
		List<Token> tokens = LabelParserUtil.parse(argumentElement
				.getDescription());
		for (Token token : tokens) {
			if (token instanceof HighlightItem) {
				HighlightItem item = (HighlightItem) token;
				if (item.getHeaderType() == ItemHeaderType.var
						&& item.getBody().equalsIgnoreCase(itemBody)) {
					String newDescription = argumentElement.getDescription()
							.replaceAll(item.getRawString(), replacement);
					argumentElement.setDescription(newDescription);
					return true;
				}
			}
		}

		return false;
	}

}
