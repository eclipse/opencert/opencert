/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.parts;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.opencert.gsn.figures.GSNSolution;
import org.eclipse.opencert.gsn.figures.LayoutUtil;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.InformationElementCitationItemSemanticEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.userguidance.label.WrappingLabel2;

/**
 * @generated
 */
public class InformationElementCitationEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2005;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public InformationElementCitationEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new InformationElementCitationItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated NOT
	 */
	protected IFigure createNodeShape() {
		// Start MCP
		//return primaryShape = new InformationElementCitationFigure();
		if (!(((View) this.getModel()).getElement() instanceof InformationElementCitation)) {
			System.out.println("createNodeShape(): Error model not allowed");
			return primaryShape = new InformationElementCitationFigure();
		}

		InformationElementCitation newElem = (InformationElementCitation) ((Node) this
				.getModel()).getElement();
		primaryShape = new InformationElementCitationFigure();
		if (newElem.getType() == InformationElementType.JUSTIFICATION) {
			primaryShape = new InformationElementCitationFigure();
			((InformationElementCitationFigure) (primaryShape)).setShape(2);
		} else if (newElem.getType() == InformationElementType.CONTEXT) {
			primaryShape = new InformationElementCitationFigure();
			((InformationElementCitationFigure) (primaryShape)).setShape(3);
		} else // Similar to SOLUTION
		{
			primaryShape = new InformationElementCitationFigure();
			((InformationElementCitationFigure) (primaryShape)).setShape(4); //OJO: no debe ser 0
		}

		if (newElem.getToBeInstantiated() == true) {
			int s2 = ((InformationElementCitationFigure) (primaryShape))
					.getShape();
			s2 += 10;
			((InformationElementCitationFigure) (primaryShape)).setShape(s2);
		}

		return primaryShape;
		// End MCP
	}

	/**
	 * @generated
	 */
	public InformationElementCitationFigure getPrimaryShape() {
		return (InformationElementCitationFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof InformationElementCitationIdEditPart) {
			((InformationElementCitationIdEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureInformationElementCitationLabelFigure());
			return true;
		}
		if (childEditPart instanceof InformationElementCitationDescriptionEditPart) {
			((InformationElementCitationDescriptionEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentationElementDescriptionLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof InformationElementCitationIdEditPart) {
			return true;
		}
		if (childEditPart instanceof InformationElementCitationDescriptionEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(100, 100);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(ArgVisualIDRegistry
				.getType(InformationElementCitationIdEditPart.VISUAL_ID));
	}

	/**
	 * @generated NOT
	 */
	protected void handleNotificationEvent(Notification event) {
		// Start MCP
		/* 
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			super.handleNotificationEvent(event);
		}
		 */
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {

			if (event.getNotifier() instanceof InformationElementCitation) {

				InformationElementCitation elem = (InformationElementCitation) ((Node) this
						.getModel()).getElement();
				InformationElementCitationFigure figure = (InformationElementCitationFigure) this
						.getPrimaryShape();

				if (elem.getType() == InformationElementType.JUSTIFICATION) {
					figure.setShape(2);
				} else if (elem.getType() == InformationElementType.CONTEXT) {
					figure.setShape(3);
				} else // Similar to SOLUTION
				{
					figure.setShape(4); //OJO: no debe ser 0
				}

				if (elem.getToBeInstantiated() == true) {
					int s2 = figure.getShape();
					s2 += 10;
					figure.setShape(s2);
				}
			}

			super.handleNotificationEvent(event);
		}
		// End MCP
	}

	/**
	 * @generated NOT
	 */
	public class InformationElementCitationFigure extends GSNSolution {
		// Start MCP
		/**
		 * @generated NOT
		 */
		@Override
		public void paint(Graphics graphics) {
			if ((getShape() % 10) == 2) { //InformationElementType.JUSTIFICATION
				paintGSNJustification(graphics);
			} else if ((getShape() % 10) == 3) { //InformationElementType.CONTEXT
				paintGSNContext(graphics);
			} else // Similar to SOLUTION
			{
				paintGSNSolution(graphics);
			}
		}

		// End MCP

		/**
		 * @generated NOT
		 */
		private void paintGSNJustification(Graphics graphics) {
			Rectangle r = getBounds();

			// Start MCP
			//if (getShape()==0){
			//if ((getShape() % 10) == 2) {
			if (getShape() < 10) {
				graphics.drawOval(r.x, r.y, r.width, r.height);
				graphics.drawText("J", new Point(r.x + 27 * r.width / 30, r.y
						+ 6 * r.height / 8));
			} else
			// End MCP
			{
				graphics.drawOval(r.x, r.y, r.width, 3 * r.height / 4);
				graphics.drawText("J", new Point(r.x + 18 * r.width / 20, r.y
						+ 2 * r.height / 3));

				Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height / 4);
				Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7 * r.height
						/ 8);
				Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
				Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7 * r.height
						/ 8);
				PointList pointList = new PointList();
				pointList.addPoint(p5);
				pointList.addPoint(p6);
				pointList.addPoint(p7);
				pointList.addPoint(p8);
				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.drawLine(p5, p6);
				graphics.drawLine(p6, p8);
				graphics.drawLine(p5, p8);
			}
			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);

		}

		/**
		 * @generated NOT
		 */
		private void paintGSNContext(Graphics graphics) {

			Rectangle r = getBounds();
			if (getVisibility()) {
				Point p1 = new Point(r.x + 15 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p2 = new Point(r.x + 16 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p3 = new Point(r.x + 16 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p4 = new Point(r.x + 15 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p5 = new Point(r.x + 17 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p6 = new Point(r.x + 15 * r.width / 18, r.y + 6
						* r.height / 18);
				Point p7 = new Point(r.x + 17 * r.width / 18, r.y + 6
						* r.height / 18);
				PointList pointList = new PointList();
				pointList.addPoint(p1);
				pointList.addPoint(p2);
				pointList.addPoint(p3);
				pointList.addPoint(p4);
				pointList.addPoint(p5);
				pointList.addPoint(p6);
				pointList.addPoint(p7);
				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.setLineWidth(1);

				graphics.drawLine(p1, p2);
				graphics.drawLine(p2, p3);
				graphics.drawLine(p1, p4);
				graphics.drawLine(p4, p5);
				graphics.drawLine(p5, p7);
				graphics.drawLine(p6, p7);
				graphics.drawLine(p6, p4);

			}

			graphics.setLineWidth(0);
			// Start MCP
			//if (getShape()==0){
			//if ((getShape() % 10) == 3) {
			if (getShape() < 10) {
				graphics.drawRoundRectangle(r, r.width / 4, r.height / 4);
			} else
			// End MCP
			{
				Rectangle r2 = new Rectangle(r.x, r.y, r.width,
						3 * r.height / 4);
				graphics.drawRoundRectangle(r2, r.width / 4, r.height / 4);
				Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height / 4);
				Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7 * r.height
						/ 8);
				Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
				Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7 * r.height
						/ 8);
				PointList pointList = new PointList();
				pointList.addPoint(p5);
				pointList.addPoint(p6);
				pointList.addPoint(p7);
				pointList.addPoint(p8);
				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.drawLine(p5, p6);
				graphics.drawLine(p6, p8);
				graphics.drawLine(p5, p8);
			}
			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);
		}

		/**
		 * @generated NOT
		 */
		private void paintGSNSolution(Graphics graphics) {
			Rectangle r = getBounds();
			if (getVisibility()) {
				Point p1 = new Point(r.x + 15 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p2 = new Point(r.x + 16 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p3 = new Point(r.x + 16 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p4 = new Point(r.x + 15 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p5 = new Point(r.x + 17 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p6 = new Point(r.x + 15 * r.width / 18, r.y + 6
						* r.height / 18);
				Point p7 = new Point(r.x + 17 * r.width / 18, r.y + 6
						* r.height / 18);
				PointList pointList = new PointList();
				pointList.addPoint(p1);
				pointList.addPoint(p2);
				pointList.addPoint(p3);
				pointList.addPoint(p4);
				pointList.addPoint(p5);
				pointList.addPoint(p6);
				pointList.addPoint(p7);
				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.setLineWidth(1);

				graphics.drawLine(p1, p2);
				graphics.drawLine(p2, p3);
				graphics.drawLine(p1, p4);
				graphics.drawLine(p4, p5);
				graphics.drawLine(p5, p7);
				graphics.drawLine(p6, p7);
				graphics.drawLine(p6, p4);

			}
			graphics.setLineWidth(0);
			// Start MCP
			//if (getShape()==0){
			//if ((getShape() % 10) == 4) {
			if (getShape() < 10) {
				graphics.drawOval(r.x, r.y, r.width, r.height);
			} else
			// End MCP
			{
				graphics.drawOval(r.x, r.y, r.width, 3 * r.height / 4);

				Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height / 4);
				Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7 * r.height
						/ 8);
				Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
				Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7 * r.height
						/ 8);
				PointList pointList = new PointList();
				pointList.addPoint(p5);
				pointList.addPoint(p6);
				pointList.addPoint(p7);
				pointList.addPoint(p8);
				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.drawLine(p5, p6);
				graphics.drawLine(p6, p8);
				graphics.drawLine(p5, p8);
			}
			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);

		}

		/**
		 * @generated
		 */
		private WrappingLabel fFigureInformationElementCitationLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentationElementDescriptionLabelFigure;

		/**
		 * @generated
		 */
		public InformationElementCitationFigure() {
			this.setPreferredSize(new Dimension(getMapMode().DPtoLP(100),
					getMapMode().DPtoLP(100)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureInformationElementCitationLabelFigure = new WrappingLabel();

			fFigureInformationElementCitationLabelFigure
					.setText("InformationElementCitation");

			fFigureInformationElementCitationLabelFigure
					.setFont(FFIGUREINFORMATIONELEMENTCITATIONLABELFIGURE_FONT);

			this.add(fFigureInformationElementCitationLabelFigure);

			fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel2();
			//fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel();
			
			fFigureArgumentationElementDescriptionLabelFigure.setText("");

			fFigureArgumentationElementDescriptionLabelFigure
					.setFont(FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT);

			this.add(fFigureArgumentationElementDescriptionLabelFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureInformationElementCitationLabelFigure() {
			return fFigureInformationElementCitationLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentationElementDescriptionLabelFigure() {
			return fFigureArgumentationElementDescriptionLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Font FFIGUREINFORMATIONELEMENTCITATIONLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

}
