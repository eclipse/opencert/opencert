/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.parts;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderedShapeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.BorderItemSelectionEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.BorderItemLocator;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.edit.policies.reparent.CreationEditPolicyWithCustomReparent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.opencert.gsn.figures.GSNGoal;
import org.eclipse.opencert.gsn.figures.LayoutUtil;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.ClaimCanonicalEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.ClaimItemSemanticEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.userguidance.label.WrappingLabel2;

/**
 * @generated
 */
public class ClaimEditPart extends AbstractBorderedShapeEditPart {
	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2001;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public ClaimEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicyWithCustomReparent(
						ArgVisualIDRegistry.TYPED_INSTANCE));
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new ClaimItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new DragDropEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new ClaimCanonicalEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				View childView = (View) child.getModel();
				switch (ArgVisualIDRegistry.getVisualID(childView)) {
				case Choice2EditPart.VISUAL_ID:
					return new BorderItemSelectionEditPolicy();
				}
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated NOT
	 */
	protected IFigure createNodeShape() {
		// Start MCP
		//return primaryShape = new ClaimFigure();
		if (!(((View) this.getModel()).getElement() instanceof Claim)) {
			System.out.println("createNodeShape(): Error model not allowed");
			return primaryShape = new ClaimFigure();
		}

		Claim newElem = (Claim) ((Node) this.getModel()).getElement();
		primaryShape = new ClaimFigure();
		if (newElem.getToBeSupported() == true
				&& newElem.getToBeInstantiated() == false) {
			((ClaimFigure) (primaryShape)).setShape(1);

		} else if (newElem.getToBeInstantiated() == true
				&& newElem.getToBeSupported() == false) {
			((ClaimFigure) (primaryShape)).setShape(2);

		} else if (newElem.getToBeSupported() == true
				&& newElem.getToBeInstantiated() == true) {
			((ClaimFigure) (primaryShape)).setShape(3);
		} /*else if (newElem.getToBeSupportedByContract() == true) {
			((ClaimFigure) (primaryShape)).setShape(4);
			} */else {
			((ClaimFigure) (primaryShape)).setShape(0);
		}
		if (newElem.getAssumed() == true) {
			int s2 = ((ClaimFigure) (primaryShape)).getShape();
			s2 += 10;
			((ClaimFigure) (primaryShape)).setShape(s2);
		}

		if (newElem.getPublic() == true)
			((ClaimFigure) (primaryShape)).setVisibility(true);

		return primaryShape;
		// End MCP
	}

	/**
	 * @generated
	 */
	public ClaimFigure getPrimaryShape() {
		return (ClaimFigure) primaryShape;
	}

	/**
	 * @generated NOT
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ClaimIdEditPart) {
			((ClaimIdEditPart) childEditPart).setLabel(getPrimaryShape()
					.getFigureClaimLabelFigure());
			return true;
		}
		if (childEditPart instanceof ClaimDescriptionEditPart) {
			((ClaimDescriptionEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentationElementDescriptionLabelFigure());
			return true;
		}
		if (childEditPart instanceof Choice2EditPart) {
			BorderItemLocator locator = new BorderItemLocator(getMainFigure(),
			//Start MCP
			//PositionConstants.NORTH);
					PositionConstants.SOUTH);
			//End MCP
			getBorderedFigure().getBorderItemContainer().add(
					((Choice2EditPart) childEditPart).getFigure(), locator);
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ClaimIdEditPart) {
			return true;
		}
		if (childEditPart instanceof ClaimDescriptionEditPart) {
			return true;
		}
		if (childEditPart instanceof Choice2EditPart) {
			getBorderedFigure().getBorderItemContainer().remove(
					((Choice2EditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof IBorderItemEditPart) {
			return getBorderedFigure().getBorderItemContainer();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(120, 80);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createMainFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(ArgVisualIDRegistry
				.getType(ClaimIdEditPart.VISUAL_ID));
	}

	/**
	 * @generated NOT
	 */
	protected void handleNotificationEvent(Notification event) {
		// Start MCP
		/* 
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			super.handleNotificationEvent(event);
		}
		 */

		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {

			if (event.getNotifier() instanceof Claim) {

				Claim elem = (Claim) ((Node) this.getModel()).getElement();
				ClaimFigure figure = (ClaimFigure) this.getPrimaryShape();
				if (elem.getToBeSupported() == true
						&& elem.getToBeInstantiated() == false) {
					figure.setShape(1);
				} else if (elem.getToBeInstantiated() == true
						&& elem.getToBeSupported() == false) {
					figure.setShape(2);
				} else if (elem.getToBeSupported() == true
						&& elem.getToBeInstantiated() == true) {
					figure.setShape(3);
				} /*else if (elem.getToBeSupportedByContract() == true) {
					figure.setShape(4);
					} */else {
					figure.setShape(0);
				}

				if (elem.getAssumed() == true) {
					int s2 = figure.getShape();
					s2 += 10;
					figure.setShape(s2);
				}

				if (elem.getPublic() == true) {
					figure.setVisibility(true);
				} else {
					figure.setVisibility(false);
				}
			}
			super.handleNotificationEvent(event);
		}
		// End MCP
	}

	/**
	 * @generated NOT
	 */
	public class ClaimFigure extends GSNGoal {
		// Start MCP
		/**
		 * @generated NOT
		 */
		@Override
		public void paint(Graphics graphics) {
			// Similar to GOAL
			//super.paint(graphics);
			if (getShape() >= 10) //ASSUMED
				paintAssumedClaim(graphics);
			else
				paintGSNGoal(graphics);
		}

		/**
		 * @generated NOT
		 */
		private void paintAssumedClaim(Graphics graphics) {
			Rectangle r = getBounds();

			graphics.drawText("A", new Point(r.x + 27 * r.width / 30, r.y + 6
					* r.height / 8));

			if (getVisibility()) {
				Point p1 = new Point(r.x + 15 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p2 = new Point(r.x + 16 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p3 = new Point(r.x + 16 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p4 = new Point(r.x + 15 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p5 = new Point(r.x + 17 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p6 = new Point(r.x + 15 * r.width / 18, r.y + 6
						* r.height / 18);
				Point p7 = new Point(r.x + 17 * r.width / 18, r.y + 6
						* r.height / 18);
				PointList pointList = new PointList();
				pointList.addPoint(p1);
				pointList.addPoint(p2);
				pointList.addPoint(p3);
				pointList.addPoint(p4);
				pointList.addPoint(p5);
				pointList.addPoint(p6);
				pointList.addPoint(p7);
				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.setLineWidth(1);

				graphics.drawLine(p1, p2);
				graphics.drawLine(p2, p3);
				graphics.drawLine(p1, p4);
				graphics.drawLine(p4, p5);
				graphics.drawLine(p5, p7);
				graphics.drawLine(p6, p7);
				graphics.drawLine(p6, p4);

			}
			graphics.setLineWidth(0);

			//undefined element status
			//if (getShape()==0){
			if ((getShape() % 10) == 0) {
				// Define the points of a parallelogram
				Point p1 = new Point(r.x, r.y);
				Point p2 = new Point(r.x + r.width, r.y);
				Point p3 = new Point(r.x, r.y + 5 * r.height / 6);
				Point p4 = new Point(r.x + r.width, r.y + 5 * r.height / 6);

				// Define the shape
				Rectangle r2 = new Rectangle(r.x, r.y, r.width,
						5 * r.height / 6);

				// Draw the outline
				//graphics.drawOval(r.x, r.y, r.width, r.height);
				graphics.drawRoundRectangle(r2, r2.width / 6, r2.height / 6);
			} else {
				// Define the points of a parallelogram
				Point p1 = new Point(r.x, r.y);
				Point p2 = new Point(r.x + r.width, r.y);
				Point p3 = new Point(r.x, r.y + 3 * r.height / 4);
				Point p4 = new Point(r.x + r.width, r.y + 3 * r.height / 4);

				//undeveloped element status
				//if(getShape()==1){
				if ((getShape() % 10) == 1) {
					Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height
							/ 4);
					Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7
							* r.height / 8);
					Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
					Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7
							* r.height / 8);
					PointList pointList = new PointList();
					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					// Fill the shape
					graphics.fillPolygon(pointList);

					// Define the shape
					Rectangle r2 = new Rectangle(r.x, r.y, r.width,
							3 * r.height / 4);

					// Draw the outline
					graphics.drawRoundRectangle(r2, r2.width / 6, r2.height / 6);

					// Draw the outline
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p7);
					graphics.drawLine(p7, p8);
					graphics.drawLine(p5, p8);

				}
				//uninstantiated element status
				//else if(getShape()==2){
				else if ((getShape() % 10) == 2) {

					Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height
							/ 4);
					Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7
							* r.height / 8);
					Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
					Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7
							* r.height / 8);
					PointList pointList = new PointList();
					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					// Fill the shape
					graphics.fillPolygon(pointList);

					// Define the shape
					Rectangle r2 = new Rectangle(r.x, r.y, r.width,
							3 * r.height / 4);

					// Draw the outline
					graphics.drawRoundRectangle(r2, r2.width / 6, r2.height / 6);

					// Draw the outline
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p8);
					graphics.drawLine(p5, p8);

				}
				//undeveloped and uninstantiated element status
				//else if(getShape()==3){
				else if ((getShape() % 10) == 3) {

					Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height
							/ 4);
					Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7
							* r.height / 8);
					Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
					Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7
							* r.height / 8);
					PointList pointList = new PointList();
					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					// Fill the shape
					graphics.fillPolygon(pointList);

					// Define the shape
					Rectangle r2 = new Rectangle(r.x, r.y, r.width,
							3 * r.height / 4);

					// Draw the outline
					graphics.drawRoundRectangle(r2, r2.width / 6, r2.height / 6);

					// Draw the outline
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p7);
					graphics.drawLine(p7, p8);
					graphics.drawLine(p5, p8);
					graphics.drawLine(p6, p8);
				}
				//else if(getShape()==4){
				else if ((getShape() % 10) == 4) {

					Point p5 = new Point(r.x + 6 * r.width / 16, r.y + 12
							* r.height / 16);
					Point p6 = new Point(r.x + 7 * r.width / 16, r.y + 12
							* r.height / 16);
					Point p7 = new Point(r.x + 7 * r.width / 16, r.y + 13
							* r.height / 16);
					Point p8 = new Point(r.x + 6 * r.width / 16, r.y + 13
							* r.height / 16);
					Point p9 = new Point(r.x + 10 * r.width / 16, r.y + 13
							* r.height / 16);
					Point p10 = new Point(r.x + 6 * r.width / 16, r.y + 15
							* r.height / 16);
					Point p11 = new Point(r.x + 10 * r.width / 16, r.y + 15
							* r.height / 16);
					Point p12 = new Point(r.x + 9 * r.width / 16, r.y
							+ r.height);
					Point p13 = new Point(r.x + 9 * r.width / 16, r.y + 15
							* r.height / 16);
					Point p14 = new Point(r.x + 10 * r.width / 16, r.y
							+ r.height);

					PointList pointList = new PointList();
					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					pointList.addPoint(p9);
					pointList.addPoint(p10);
					pointList.addPoint(p11);
					pointList.addPoint(p12);
					pointList.addPoint(p13);
					pointList.addPoint(p14);
					// Fill the shape
					graphics.fillPolygon(pointList);

					// Define the shape
					Rectangle r2 = new Rectangle(r.x, r.y, r.width,
							3 * r.height / 4);

					// Draw the outline
					graphics.drawRoundRectangle(r2, r2.width / 6, r2.height / 6);

					// Draw the outline					
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p7);
					graphics.drawLine(p5, p8);
					graphics.drawLine(p8, p9);
					graphics.drawLine(p9, p11);
					graphics.drawLine(p10, p11);
					graphics.drawLine(p10, p8);
					graphics.drawLine(p12, p13);
					graphics.drawLine(p12, p14);
					graphics.drawLine(p14, p11);

				}
			}
			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);
		}

		/**
		 * @generated NOT
		 */
		public void paintGSNGoal(Graphics graphics) {

			Rectangle r = getBounds();

			if (getVisibility()) {
				Point p1 = new Point(r.x + 15 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p2 = new Point(r.x + 16 * r.width / 18, r.y + 3
						* r.height / 18);
				Point p3 = new Point(r.x + 16 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p4 = new Point(r.x + 15 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p5 = new Point(r.x + 17 * r.width / 18, r.y + 4
						* r.height / 18);
				Point p6 = new Point(r.x + 15 * r.width / 18, r.y + 6
						* r.height / 18);
				Point p7 = new Point(r.x + 17 * r.width / 18, r.y + 6
						* r.height / 18);
				PointList pointList = new PointList();
				pointList.addPoint(p1);
				pointList.addPoint(p2);
				pointList.addPoint(p3);
				pointList.addPoint(p4);
				pointList.addPoint(p5);
				pointList.addPoint(p6);
				pointList.addPoint(p7);
				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.setLineWidth(1);

				graphics.drawLine(p1, p2);
				graphics.drawLine(p2, p3);
				graphics.drawLine(p1, p4);
				graphics.drawLine(p4, p5);
				graphics.drawLine(p5, p7);
				graphics.drawLine(p6, p7);
				graphics.drawLine(p6, p4);
			}

			graphics.setLineWidth(0);

			//undefined element status
			//if (getShape()==0){
			if ((getShape() % 10) == 0) {
				// Define the points of a parallelogram
				Point p1 = new Point(r.x, r.y);
				Point p2 = new Point(r.x + r.width, r.y);
				Point p3 = new Point(r.x, r.y + r.height);
				Point p4 = new Point(r.x + r.width, r.y + r.height);
				PointList pointList = new PointList();
				pointList.addPoint(p1);
				pointList.addPoint(p2);
				pointList.addPoint(p3);
				pointList.addPoint(p4);

				// Fill the shape
				graphics.fillPolygon(pointList);

				// Draw the outline
				graphics.drawLine(p1, p2);
				graphics.drawLine(p1, p3);
				graphics.drawLine(p3, p4);
				graphics.drawLine(p2, p4);
			} else {
				// Define the points of a parallelogram
				Point p1 = new Point(r.x, r.y);
				Point p2 = new Point(r.x + r.width, r.y);
				Point p3 = new Point(r.x, r.y + 3 * r.height / 4);
				Point p4 = new Point(r.x + r.width, r.y + 3 * r.height / 4);

				PointList pointList = new PointList();
				pointList.addPoint(p1);
				pointList.addPoint(p2);
				pointList.addPoint(p3);
				pointList.addPoint(p4);
				//undeveloped element status
				//if(getShape()==1){
				if ((getShape() % 10) == 1) {
					Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height
							/ 4);
					Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7
							* r.height / 8);
					Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
					Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7
							* r.height / 8);

					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					// Fill the shape
					graphics.fillPolygon(pointList);

					// Draw the outline
					graphics.drawLine(p1, p2);
					graphics.drawLine(p1, p3);
					graphics.drawLine(p3, p4);
					graphics.drawLine(p2, p4);

					// Draw the outline
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p7);
					graphics.drawLine(p7, p8);
					graphics.drawLine(p5, p8);

				}
				//uninstantiated element status
				//else if(getShape()==2){
				else if ((getShape() % 10) == 2) {

					Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height
							/ 4);
					Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7
							* r.height / 8);
					Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
					Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7
							* r.height / 8);

					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					// Fill the shape
					graphics.fillPolygon(pointList);

					// Draw the outline
					graphics.drawLine(p1, p2);
					graphics.drawLine(p1, p3);
					graphics.drawLine(p3, p4);
					graphics.drawLine(p2, p4);

					// Draw the outline
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p8);
					graphics.drawLine(p5, p8);

				}
				//undeveloped and uninstantiated element status
				//else if(getShape()==3){
				else if ((getShape() % 10) == 3) {

					Point p5 = new Point(r.x + r.width / 2, r.y + 3 * r.height
							/ 4);
					Point p6 = new Point(r.x + 5 * r.width / 8, r.y + 7
							* r.height / 8);
					Point p7 = new Point(r.x + r.width / 2, r.y + r.height);
					Point p8 = new Point(r.x + 3 * r.width / 8, r.y + 7
							* r.height / 8);

					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					// Fill the shape
					graphics.fillPolygon(pointList);

					// Draw the outline
					graphics.drawLine(p1, p2);
					graphics.drawLine(p1, p3);
					graphics.drawLine(p3, p4);
					graphics.drawLine(p2, p4);

					// Draw the outline
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p7);
					graphics.drawLine(p7, p8);
					graphics.drawLine(p5, p8);
					graphics.drawLine(p6, p8);

				}
				//else if(getShape()==4){
				else if ((getShape() % 10) == 4) {

					Point p5 = new Point(r.x + 6 * r.width / 16, r.y + 12
							* r.height / 16);
					Point p6 = new Point(r.x + 7 * r.width / 16, r.y + 12
							* r.height / 16);
					Point p7 = new Point(r.x + 7 * r.width / 16, r.y + 13
							* r.height / 16);
					Point p8 = new Point(r.x + 6 * r.width / 16, r.y + 13
							* r.height / 16);
					Point p9 = new Point(r.x + 10 * r.width / 16, r.y + 13
							* r.height / 16);
					Point p10 = new Point(r.x + 6 * r.width / 16, r.y + 15
							* r.height / 16);
					Point p11 = new Point(r.x + 10 * r.width / 16, r.y + 15
							* r.height / 16);
					Point p12 = new Point(r.x + 9 * r.width / 16, r.y
							+ r.height);
					Point p13 = new Point(r.x + 9 * r.width / 16, r.y + 15
							* r.height / 16);
					Point p14 = new Point(r.x + 10 * r.width / 16, r.y
							+ r.height);

					pointList = new PointList();

					pointList.addPoint(p5);
					pointList.addPoint(p6);
					pointList.addPoint(p7);
					pointList.addPoint(p8);
					pointList.addPoint(p9);
					pointList.addPoint(p10);
					pointList.addPoint(p11);
					pointList.addPoint(p12);
					pointList.addPoint(p13);
					pointList.addPoint(p14);
					// Fill the shape
					graphics.fillPolygon(pointList);
					graphics.drawLine(p1, p2);
					graphics.drawLine(p1, p3);
					graphics.drawLine(p3, p4);
					graphics.drawLine(p2, p4);
					graphics.drawLine(p5, p6);
					graphics.drawLine(p6, p7);
					graphics.drawLine(p5, p8);
					graphics.drawLine(p8, p9);
					graphics.drawLine(p9, p11);
					graphics.drawLine(p10, p11);
					graphics.drawLine(p10, p8);
					graphics.drawLine(p12, p13);
					graphics.drawLine(p12, p14);
					graphics.drawLine(p14, p11);

				}
			}
			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);
		}

		// End MCP

		/**
		 * @generated
		 */
		private WrappingLabel fFigureClaimLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentationElementDescriptionLabelFigure;

		/**
		 * @generated
		 */
		public ClaimFigure() {
			this.setPreferredSize(new Dimension(getMapMode().DPtoLP(120),
					getMapMode().DPtoLP(80)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureClaimLabelFigure = new WrappingLabel();

			fFigureClaimLabelFigure.setText("Goal");

			fFigureClaimLabelFigure.setFont(FFIGURECLAIMLABELFIGURE_FONT);

			this.add(fFigureClaimLabelFigure);

			fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel2();
			//fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel();

			fFigureArgumentationElementDescriptionLabelFigure.setText("");

			fFigureArgumentationElementDescriptionLabelFigure
					.setFont(FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT);

			this.add(fFigureArgumentationElementDescriptionLabelFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureClaimLabelFigure() {
			return fFigureClaimLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentationElementDescriptionLabelFigure() {
			return fFigureArgumentationElementDescriptionLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Font FFIGURECLAIMLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

}
