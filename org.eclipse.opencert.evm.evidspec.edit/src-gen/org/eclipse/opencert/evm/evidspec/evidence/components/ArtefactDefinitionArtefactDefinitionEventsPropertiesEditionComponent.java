/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ARTEFACTDEFINITIONEVENTS_PART = "ArtefactDefinitionEvents"; //$NON-NLS-1$

	
	/**
	 * Settings for lifecycleEvent ReferencesTable
	 */
	protected ReferencesTableSettings lifecycleEventSettings;
	
	/**
	 * Settings for lifecycleEventTable ReferencesTable
	 */
	protected ReferencesTableSettings lifecycleEventTableSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject artefactDefinition, String editing_mode) {
		super(editingContext, artefactDefinition, editing_mode);
		parts = new String[] { ARTEFACTDEFINITIONEVENTS_PART };
		repositoryKey = EvidenceViewsRepository.class;
		partKey = EvidenceViewsRepository.ArtefactDefinitionEvents.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final ArtefactDefinition artefactDefinition = (ArtefactDefinition)elt;
			final ArtefactDefinitionEventsPropertiesEditionPart artefactDefinitionEventsPart = (ArtefactDefinitionEventsPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent)) {
				lifecycleEventSettings = new ReferencesTableSettings(artefactDefinition, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent());
				artefactDefinitionEventsPart.initLifecycleEvent(lifecycleEventSettings);
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable)) {
				lifecycleEventTableSettings = new ReferencesTableSettings(artefactDefinition, AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent());
				artefactDefinitionEventsPart.initLifecycleEventTable(lifecycleEventTableSettings);
			}
			// init filters
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent)) {
				artefactDefinitionEventsPart.addFilterToLifecycleEvent(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvent); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for lifecycleEvent
				// End of user code
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable)) {
				artefactDefinitionEventsPart.addFilterToLifecycleEventTable(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof AssuranceAssetEvent); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for lifecycleEventTable
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable) {
			return AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		ArtefactDefinition artefactDefinition = (ArtefactDefinition)semanticObject;
		if (EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, lifecycleEventSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				lifecycleEventSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				lifecycleEventSettings.move(event.getNewIndex(), (AssuranceAssetEvent) event.getNewValue());
			}
		}
		if (EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, lifecycleEventTableSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				lifecycleEventTableSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				lifecycleEventTableSettings.move(event.getNewIndex(), (AssuranceAssetEvent) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ArtefactDefinitionEventsPropertiesEditionPart artefactDefinitionEventsPart = (ArtefactDefinitionEventsPropertiesEditionPart)editingPart;
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEvent))
				artefactDefinitionEventsPart.updateLifecycleEvent();
			if (AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactDefinitionEvents.Properties.lifecycleEventTable))
				artefactDefinitionEventsPart.updateLifecycleEventTable();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent(),
			AssuranceassetPackage.eINSTANCE.getManageableAssuranceAsset_LifecycleEvent()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
