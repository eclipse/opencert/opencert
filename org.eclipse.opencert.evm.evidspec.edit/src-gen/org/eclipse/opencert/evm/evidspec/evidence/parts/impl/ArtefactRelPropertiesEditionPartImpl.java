/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactRelPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ArtefactRelPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected EMFComboViewer modificationEffect;
	protected EMFComboViewer revocationEffect;
	protected EObjectFlatComboViewer source;
	protected EObjectFlatComboViewer target;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactRelPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence artefactRelStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactRelStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.class);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.id);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.name);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.description);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.source);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactRel.Properties.target);
		
		
		composer = new PartComposer(artefactRelStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.id) {
					return createIdText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.name) {
					return createNameText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect) {
					return createModificationEffectEMFComboViewer(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect) {
					return createRevocationEffectEMFComboViewer(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.source) {
					return createSourceFlatComboViewer(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactRel.Properties.target) {
					return createTargetFlatComboViewer(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ArtefactRelPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactRel.Properties.id, EvidenceMessages.ArtefactRelPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, EvidenceViewsRepository.ArtefactRel.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactRel.Properties.id, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactRel.Properties.name, EvidenceMessages.ArtefactRelPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, EvidenceViewsRepository.ArtefactRel.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactRel.Properties.name, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactRel.Properties.description, EvidenceMessages.ArtefactRelPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, EvidenceViewsRepository.ArtefactRel.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactRel.Properties.description, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createModificationEffectEMFComboViewer(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect, EvidenceMessages.ArtefactRelPropertiesEditionPart_ModificationEffectLabel);
		modificationEffect = new EMFComboViewer(parent);
		modificationEffect.setContentProvider(new ArrayContentProvider());
		modificationEffect.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData modificationEffectData = new GridData(GridData.FILL_HORIZONTAL);
		modificationEffect.getCombo().setLayoutData(modificationEffectData);
		modificationEffect.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getModificationEffect()));
			}

		});
		modificationEffect.setID(EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createModificationEffectEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createRevocationEffectEMFComboViewer(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect, EvidenceMessages.ArtefactRelPropertiesEditionPart_RevocationEffectLabel);
		revocationEffect = new EMFComboViewer(parent);
		revocationEffect.setContentProvider(new ArrayContentProvider());
		revocationEffect.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData revocationEffectData = new GridData(GridData.FILL_HORIZONTAL);
		revocationEffect.getCombo().setLayoutData(revocationEffectData);
		revocationEffect.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getRevocationEffect()));
			}

		});
		revocationEffect.setID(EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRevocationEffectEMFComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createSourceFlatComboViewer(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactRel.Properties.source, EvidenceMessages.ArtefactRelPropertiesEditionPart_SourceLabel);
		source = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(EvidenceViewsRepository.ArtefactRel.Properties.source, EvidenceViewsRepository.SWT_KIND));
		source.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		source.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.source, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getSource()));
			}

		});
		GridData sourceData = new GridData(GridData.FILL_HORIZONTAL);
		source.setLayoutData(sourceData);
		source.setID(EvidenceViewsRepository.ArtefactRel.Properties.source);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactRel.Properties.source, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createSourceFlatComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createTargetFlatComboViewer(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactRel.Properties.target, EvidenceMessages.ArtefactRelPropertiesEditionPart_TargetLabel);
		target = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(EvidenceViewsRepository.ArtefactRel.Properties.target, EvidenceViewsRepository.SWT_KIND));
		target.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		target.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactRelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactRel.Properties.target, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getTarget()));
			}

		});
		GridData targetData = new GridData(GridData.FILL_HORIZONTAL);
		target.setLayoutData(targetData);
		target.setID(EvidenceViewsRepository.ArtefactRel.Properties.target);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactRel.Properties.target, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createTargetFlatComboViewer

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		Shell thisS= view.getShell();
		MessageDialog.openInformation(thisS, "Confirm", "Artefact Rel Changed");
		// End of user code
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#getModificationEffect()
	 * 
	 */
	public Enumerator getModificationEffect() {
		Enumerator selection = (Enumerator) ((StructuredSelection) modificationEffect.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#initModificationEffect(Object input, Enumerator current)
	 */
	public void initModificationEffect(Object input, Enumerator current) {
		modificationEffect.setInput(input);
		modificationEffect.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect);
		if (eefElementEditorReadOnlyState && modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(false);
			modificationEffect.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setModificationEffect(Enumerator newValue)
	 * 
	 */
	public void setModificationEffect(Enumerator newValue) {
		modificationEffect.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.modificationEffect);
		if (eefElementEditorReadOnlyState && modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(false);
			modificationEffect.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#getRevocationEffect()
	 * 
	 */
	public Enumerator getRevocationEffect() {
		Enumerator selection = (Enumerator) ((StructuredSelection) revocationEffect.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#initRevocationEffect(Object input, Enumerator current)
	 */
	public void initRevocationEffect(Object input, Enumerator current) {
		revocationEffect.setInput(input);
		revocationEffect.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect);
		if (eefElementEditorReadOnlyState && revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(false);
			revocationEffect.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setRevocationEffect(Enumerator newValue)
	 * 
	 */
	public void setRevocationEffect(Enumerator newValue) {
		revocationEffect.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.revocationEffect);
		if (eefElementEditorReadOnlyState && revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(false);
			revocationEffect.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#getSource()
	 * 
	 */
	public EObject getSource() {
		if (source.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) source.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#initSource(EObjectFlatComboSettings)
	 */
	public void initSource(EObjectFlatComboSettings settings) {
		source.setInput(settings);
		if (current != null) {
			source.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.source);
		if (eefElementEditorReadOnlyState && source.isEnabled()) {
			source.setEnabled(false);
			source.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !source.isEnabled()) {
			source.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setSource(EObject newValue)
	 * 
	 */
	public void setSource(EObject newValue) {
		if (newValue != null) {
			source.setSelection(new StructuredSelection(newValue));
		} else {
			source.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.source);
		if (eefElementEditorReadOnlyState && source.isEnabled()) {
			source.setEnabled(false);
			source.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !source.isEnabled()) {
			source.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setSourceButtonMode(ButtonsModeEnum newValue)
	 */
	public void setSourceButtonMode(ButtonsModeEnum newValue) {
		source.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#addFilterSource(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSource(ViewerFilter filter) {
		source.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#addBusinessFilterSource(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSource(ViewerFilter filter) {
		source.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#getTarget()
	 * 
	 */
	public EObject getTarget() {
		if (target.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) target.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#initTarget(EObjectFlatComboSettings)
	 */
	public void initTarget(EObjectFlatComboSettings settings) {
		target.setInput(settings);
		if (current != null) {
			target.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.target);
		if (eefElementEditorReadOnlyState && target.isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.isEnabled()) {
			target.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setTarget(EObject newValue)
	 * 
	 */
	public void setTarget(EObject newValue) {
		if (newValue != null) {
			target.setSelection(new StructuredSelection(newValue));
		} else {
			target.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactRel.Properties.target);
		if (eefElementEditorReadOnlyState && target.isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(EvidenceMessages.ArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.isEnabled()) {
			target.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#setTargetButtonMode(ButtonsModeEnum newValue)
	 */
	public void setTargetButtonMode(ButtonsModeEnum newValue) {
		target.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#addFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTarget(ViewerFilter filter) {
		target.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactRelPropertiesEditionPart#addBusinessFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTarget(ViewerFilter filter) {
		target.addBusinessRuleFilter(filter);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactRel_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
