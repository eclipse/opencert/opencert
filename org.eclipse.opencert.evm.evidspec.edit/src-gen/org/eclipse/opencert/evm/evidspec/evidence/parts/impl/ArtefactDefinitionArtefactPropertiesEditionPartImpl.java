/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactDefinitionArtefactPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ArtefactDefinitionArtefactPropertiesEditionPart {

	protected ReferencesTable artefact;
	protected List<ViewerFilter> artefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactFilters = new ArrayList<ViewerFilter>();
	protected TableViewer artefactTable;
	protected List<ViewerFilter> artefactTableBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactTableFilters = new ArrayList<ViewerFilter>();
	protected Button addArtefactTable;
	protected Button removeArtefactTable;
	protected Button editArtefactTable;

	// Start IRR
	protected Program programExplorer;
	// End IRR

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactDefinitionArtefactPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence artefactDefinitionArtefactStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactDefinitionArtefactStep.addStep(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.class);
		// Start IRR
		// propertiesStep.addStep(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact);
		// End IRR
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable);
		
		
		composer = new PartComposer(artefactDefinitionArtefactStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact) {
					return createArtefactAdvancedTableComposition(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable) {
					return createArtefactTableTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ArtefactDefinitionArtefactPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactAdvancedTableComposition(Composite parent) {
		this.artefact = new ReferencesTable(getDescription(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact, EvidenceMessages.ArtefactDefinitionArtefactPropertiesEditionPart_ArtefactLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				artefact.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				artefact.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				artefact.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				artefact.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.artefactFilters) {
			this.artefact.addFilter(filter);
		}
		this.artefact.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact, EvidenceViewsRepository.SWT_KIND));
		this.artefact.createControls(parent);
		this.artefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData artefactData = new GridData(GridData.FILL_HORIZONTAL);
		artefactData.horizontalSpan = 3;
		this.artefact.setLayoutData(artefactData);
		this.artefact.setLowerBound(0);
		this.artefact.setUpperBound(-1);
		artefact.setID(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact);
		artefact.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createArtefactAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactTableTableComposition(Composite container) {
		Composite tableContainer = new Composite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableArtefactTable = new org.eclipse.swt.widgets.Table(tableContainer, SWT.FULL_SELECTION);
		tableArtefactTable.setHeaderVisible(true);
		GridData gdArtefactTable = new GridData();
		gdArtefactTable.grabExcessHorizontalSpace = true;
		gdArtefactTable.horizontalAlignment = GridData.FILL;
		gdArtefactTable.grabExcessVerticalSpace = true;
		gdArtefactTable.verticalAlignment = GridData.FILL;
		tableArtefactTable.setLayoutData(gdArtefactTable);
		tableArtefactTable.setLinesVisible(true);

		// Start of user code for columns definition for ArtefactTable
		// Start IRR
		final Table tableFile = new Table (container, SWT.BORDER | SWT.MULTI);
		tableFile.setLinesVisible (true);
		tableFile.setHeaderVisible (true);
				
		GridData tableData = new GridData(GridData.FILL_BOTH);
		tableData.horizontalSpan = 3;
		tableFile.setLayoutData(tableData);
		tableFile.setItemCount (10);
		
		TableColumn col1 = new TableColumn(tableFile, SWT.NONE);
		col1.setWidth(80);
		col1.setText("File ID"); //$NON-NLS-1$

		TableColumn col2 = new TableColumn(tableFile, SWT.NONE);
		col2.setWidth(150);
		col2.setText("Name"); //$NON-NLS-1$

		TableColumn col3 = new TableColumn(tableFile, SWT.NONE);
		col3.setWidth(250);
		col3.setText("Description"); 
		
		
		Text messageText = new Text(container, 0);
		GridData textData = new GridData(GridData.FILL_HORIZONTAL);
		messageText.setLayoutData(textData);
		messageText.setText("In order to open a file, it's necessary edit the desired version artefact.");
		
		// End IRR
		//Start IRR

		TableColumn name3 = new TableColumn(tableArtefactTable, SWT.NONE);
		name3.setWidth(80);
		name3.setText("Name"); //$NON-NLS-1$
		
		TableColumn name = new TableColumn(tableArtefactTable, SWT.NONE);
		name.setWidth(80);
		name.setText("Version ID"); //$NON-NLS-1$

		TableColumn name1 = new TableColumn(tableArtefactTable, SWT.NONE);
		name1.setWidth(150);
		name1.setText("Date"); //$NON-NLS-1$

		TableColumn name2 = new TableColumn(tableArtefactTable, SWT.NONE);
		name2.setWidth(80);
		name2.setText("Last Version"); 

		//End IRR
		// End of user code

		artefactTable = new TableViewer(tableArtefactTable);
		artefactTable.setContentProvider(new ArrayContentProvider());
		artefactTable.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for ArtefactTable
						public String getColumnText(Object object, int columnIndex) {
							// Start IRR
							/*AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
							if (object instanceof EObject) {
								switch (columnIndex) {
								case 0:
									return labelProvider.getText(object);
								}
							}*/
							
							
							if (object instanceof EObject) {
								Artefact artefactObject = (Artefact)object;
								switch (columnIndex) {
								case 0:							
									return artefactObject.getName();
								case 1:							
									return artefactObject.getVersionID();
								case 2:							
									if (artefactObject.getDate() == null)
										return "";
									else
										return artefactObject.getDate().toString();
								case 3:
									if (artefactObject.isIsLastVersion())
										return "Yes";
									else
										return "No";
								}
							}

							// End IRR
							return ""; //$NON-NLS-1$
						}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		artefactTable.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (artefactTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactTable.refresh();
					}
				}
			}

		});
		
		
		// Start IRR
		artefactTable.getTable().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				if (artefactTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactTable
							.getSelection();
					Artefact artefactArtf = (Artefact) selection.getFirstElement();
					EList<Resource> lResource = artefactArtf.getResource();

					tableFile.removeAll();
					if (lResource != null) {
						for (int i = 0; i < lResource.size(); i++) {
							TableItem tableItem = new TableItem(tableFile, i);
							tableItem.setText(0, lResource.get(i).getId());
							if (lResource.get(i).getName() != null) {
								tableItem.setText(1, lResource.get(i).getName());
							}
							if (lResource.get(i).getDescription() != null) {
								tableItem.setText(2, lResource.get(i)
										.getDescription());
							}
						}
					}
				}
			}
		});
						
		tableFile.addListener(SWT.MouseDoubleClick, new Listener() {
			public void handleEvent(final Event event) {

				JOptionPane
						.showMessageDialog(
								null,
								"In order to open a file, it's necessary edit the desired version artefact.",
								"Information", JOptionPane.INFORMATION_MESSAGE);

			}
		});

		// End IRR	
		
		
		
		
		
		
		GridData artefactTableData = new GridData(GridData.FILL_HORIZONTAL);
		artefactTableData.minimumHeight = 120;
		artefactTableData.heightHint = 120;
		artefactTable.getTable().setLayoutData(artefactTableData);
		for (ViewerFilter filter : this.artefactTableFilters) {
			artefactTable.addFilter(filter);
		}
		EditingUtils.setID(artefactTable.getTable(), EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable);
		EditingUtils.setEEFtype(artefactTable.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createArtefactTablePanel(tableContainer);
		// Start of user code for createArtefactTableTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactTablePanel(Composite container) {
		Composite artefactTablePanel = new Composite(container, SWT.NONE);
		GridLayout artefactTablePanelLayout = new GridLayout();
		artefactTablePanelLayout.numColumns = 1;
		artefactTablePanel.setLayout(artefactTablePanelLayout);
		addArtefactTable = new Button(artefactTablePanel, SWT.NONE);
		addArtefactTable.setText(EvidenceMessages.PropertiesEditionPart_AddTableViewerLabel);
		GridData addArtefactTableData = new GridData(GridData.FILL_HORIZONTAL);
		addArtefactTable.setLayoutData(addArtefactTableData);
		addArtefactTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				artefactTable.refresh();
			}
		});
		EditingUtils.setID(addArtefactTable, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable);
		EditingUtils.setEEFtype(addArtefactTable, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeArtefactTable = new Button(artefactTablePanel, SWT.NONE);
		removeArtefactTable.setText(EvidenceMessages.PropertiesEditionPart_RemoveTableViewerLabel);
		GridData removeArtefactTableData = new GridData(GridData.FILL_HORIZONTAL);
		removeArtefactTable.setLayoutData(removeArtefactTableData);
		removeArtefactTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						artefactTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeArtefactTable, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable);
		EditingUtils.setEEFtype(removeArtefactTable, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editArtefactTable = new Button(artefactTablePanel, SWT.NONE);
		editArtefactTable.setText(EvidenceMessages.PropertiesEditionPart_EditTableViewerLabel);
		GridData editArtefactTableData = new GridData(GridData.FILL_HORIZONTAL);
		editArtefactTable.setLayoutData(editArtefactTableData);
		editArtefactTable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (artefactTable.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) artefactTable.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactDefinitionArtefactPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						artefactTable.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editArtefactTable, EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable);
		EditingUtils.setEEFtype(editArtefactTable, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createArtefactTablePanel

		// End of user code
		return artefactTablePanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#initArtefact(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefact.setContentProvider(contentProvider);
		artefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact);
		if (eefElementEditorReadOnlyState && artefact.isEnabled()) {
			artefact.setEnabled(false);
			artefact.setToolTipText(EvidenceMessages.ArtefactDefinitionArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefact.isEnabled()) {
			artefact.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#updateArtefact()
	 * 
	 */
	public void updateArtefact() {
	artefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#addFilterArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefact(ViewerFilter filter) {
		artefactFilters.add(filter);
		if (this.artefact != null) {
			this.artefact.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#addBusinessFilterArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefact(ViewerFilter filter) {
		artefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#isContainedInArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactTable(EObject element) {
		return ((ReferencesTableSettings)artefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#initArtefactTable(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArtefactTable(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefactTable.setContentProvider(contentProvider);
		artefactTable.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable);
		if (eefElementEditorReadOnlyState && artefactTable.getTable().isEnabled()) {
			artefactTable.getTable().setEnabled(false);
			artefactTable.getTable().setToolTipText(EvidenceMessages.ArtefactDefinitionArtefact_ReadOnly);
			addArtefactTable.setEnabled(false);
			addArtefactTable.setToolTipText(EvidenceMessages.ArtefactDefinitionArtefact_ReadOnly);
			removeArtefactTable.setEnabled(false);
			removeArtefactTable.setToolTipText(EvidenceMessages.ArtefactDefinitionArtefact_ReadOnly);
			editArtefactTable.setEnabled(false);
			editArtefactTable.setToolTipText(EvidenceMessages.ArtefactDefinitionArtefact_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefactTable.getTable().isEnabled()) {
			artefactTable.getTable().setEnabled(true);
			addArtefactTable.setEnabled(true);
			removeArtefactTable.setEnabled(true);
			editArtefactTable.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#updateArtefactTable()
	 * 
	 */
	public void updateArtefactTable() {
	artefactTable.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#addFilterArtefactTable(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefactTable(ViewerFilter filter) {
		artefactTableFilters.add(filter);
		if (this.artefactTable != null) {
			this.artefactTable.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#addBusinessFilterArtefactTable(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefactTable(ViewerFilter filter) {
		artefactTableBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart#isContainedInArtefactTableTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactTableTable(EObject element) {
		return ((ReferencesTableSettings)artefactTable.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactDefinitionArtefact_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
