/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts;

/**
 * 
 * 
 */
public class EvidenceViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * ArtefactModel view descriptor
	 * 
	 */
	public static class ArtefactModel {
		public static class Properties {
	
			
			public static String id = "evidence::ArtefactModel::properties::id";
			
			
			public static String name = "evidence::ArtefactModel::properties::name";
			
			
			public static String description = "evidence::ArtefactModel::properties::description";
			
			
			public static String artefact = "evidence::ArtefactModel::properties::artefact";
			
			
			public static String repoUrl = "evidence::ArtefactModel::properties::repoUrl";
			
			
			public static String repoUser = "evidence::ArtefactModel::properties::repoUser";
			
			
			public static String repoPassword = "evidence::ArtefactModel::properties::repoPassword";
			
			
			public static String repoLocalPath = "evidence::ArtefactModel::properties::repoLocalPath";
			
			
			public static String repoUsesLocal = "evidence::ArtefactModel::properties::repoUsesLocal";
			
	
		}
	
	}

	/**
	 * ArtefactDefinition view descriptor
	 * 
	 */
	public static class ArtefactDefinition {
		public static class Properties {
	
			
			public static String id = "evidence::ArtefactDefinition::properties::id";
			
			
			public static String name = "evidence::ArtefactDefinition::properties::name";
			
			
			public static String description = "evidence::ArtefactDefinition::properties::description";
			
	
		}
	
	}

	/**
	 * Artefact view descriptor
	 * 
	 */
	public static class Artefact {
		public static class Properties {
	
			
			public static String id = "evidence::Artefact::properties::id";
			
			
			public static String name = "evidence::Artefact::properties::name";
			
			
			public static String description = "evidence::Artefact::properties::description";
			
			
			public static String precedentVersion = "evidence::Artefact::properties::precedentVersion";
			
			
			public static String ownedRel = "evidence::Artefact::properties::ownedRel";
			
			
			public static String artefactPart = "evidence::Artefact::properties::artefactPart";
			
	
		}
	
	}

	/**
	 * Value view descriptor
	 * 
	 */
	public static class Value {
		public static class Properties {
	
			
			public static String name = "evidence::Value::properties::name";
			
			
			public static String value_ = "evidence::Value::properties::value_";
			
			
			public static String propertyReference = "evidence::Value::properties::propertyReference";
			
			
			public static String propertyCombo = "evidence::Value::properties::propertyCombo";
			
	
		}
	
	}

	/**
	 * Resource view descriptor
	 * 
	 */
	public static class Resource {
		public static class Properties {
	
			
			public static String id = "evidence::Resource::properties::id";
			
			
			public static String name = "evidence::Resource::properties::name";
			
			
			public static String description = "evidence::Resource::properties::description";
			
			
			public static String location = "evidence::Resource::properties::location";
			
			
			public static String format = "evidence::Resource::properties::format";
			
	
		}
	
	}

	/**
	 * ArtefactRel view descriptor
	 * 
	 */
	public static class ArtefactRel {
		public static class Properties {
	
			
			public static String id = "evidence::ArtefactRel::properties::id";
			
			
			public static String name = "evidence::ArtefactRel::properties::name";
			
			
			public static String description = "evidence::ArtefactRel::properties::description";
			
			
			public static String modificationEffect = "evidence::ArtefactRel::properties::modificationEffect";
			
			
			public static String revocationEffect = "evidence::ArtefactRel::properties::revocationEffect";
			
			
			public static String source = "evidence::ArtefactRel::properties::source";
			
			
			public static String target = "evidence::ArtefactRel::properties::target";
			
	
		}
	
	}

	/**
	 * ArtefactDefinitionArtefact view descriptor
	 * 
	 */
	public static class ArtefactDefinitionArtefact {
		public static class Properties {
	
			
			public static String artefact = "evidence::ArtefactDefinitionArtefact::properties::artefact";
			
			
			public static String artefactTable = "evidence::ArtefactDefinitionArtefact::properties::artefactTable";
			
	
		}
	
	}

	/**
	 * ArtefactDefinitionEvaluation view descriptor
	 * 
	 */
	public static class ArtefactDefinitionEvaluation {
		public static class Properties {
	
			
			public static String evaluation = "evidence::ArtefactDefinitionEvaluation::properties::evaluation";
			
			
			public static String evaluationTable = "evidence::ArtefactDefinitionEvaluation::properties::evaluationTable";
			
	
		}
	
	}

	/**
	 * ArtefactDefinitionEvents view descriptor
	 * 
	 */
	public static class ArtefactDefinitionEvents {
		public static class Properties {
	
			
			public static String lifecycleEvent = "evidence::ArtefactDefinitionEvents::properties::lifecycleEvent";
			
			
			public static String lifecycleEventTable = "evidence::ArtefactDefinitionEvents::properties::lifecycleEventTable";
			
	
		}
	
	}

	/**
	 * ArtefactVersion view descriptor
	 * 
	 */
	public static class ArtefactVersion {
		public static class Properties {
	
			
			public static String versionID = "evidence::ArtefactVersion::properties::versionID";
			
			
			public static String date = "evidence::ArtefactVersion::properties::date";
			
			
			public static String changes = "evidence::ArtefactVersion::properties::changes";
			
			
			public static String isLastVersion = "evidence::ArtefactVersion::properties::isLastVersion";
			
			
			public static String isTemplate = "evidence::ArtefactVersion::properties::isTemplate";
			
			
			public static String isConfigurable = "evidence::ArtefactVersion::properties::isConfigurable";
			
			
			public static String resource = "evidence::ArtefactVersion::properties::resource";
			
	
		}
	
	}

	/**
	 * ArtefactPropertyValue view descriptor
	 * 
	 */
	public static class ArtefactPropertyValue {
		public static class Properties {
	
			
			public static String propertyValue = "evidence::ArtefactPropertyValue::properties::propertyValue";
			
			
			public static String propertyValueTable = "evidence::ArtefactPropertyValue::properties::propertyValueTable";
			
	
		}
	
	}

	/**
	 * ArtefactEvaluation view descriptor
	 * 
	 */
	public static class ArtefactEvaluation {
		public static class Properties {
	
			
			public static String evaluation = "evidence::ArtefactEvaluation::properties::evaluation";
			
			
			public static String evaluationTable = "evidence::ArtefactEvaluation::properties::evaluationTable";
			
	
		}
	
	}

	/**
	 * ArtefactEvents view descriptor
	 * 
	 */
	public static class ArtefactEvents {
		public static class Properties {
	
			
			public static String lifecycleEvent = "evidence::ArtefactEvents::properties::lifecycleEvent";
			
			
			public static String lifecycleEventTable = "evidence::ArtefactEvents::properties::lifecycleEventTable";
			
	
		}
	
	}

}
