/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ARTEFACTDEFINITIONARTEFACT_PART = "ArtefactDefinitionArtefact"; //$NON-NLS-1$

	
	/**
	 * Settings for artefact ReferencesTable
	 */
	protected ReferencesTableSettings artefactSettings;
	
	/**
	 * Settings for artefactTable ReferencesTable
	 */
	protected ReferencesTableSettings artefactTableSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject artefactDefinition, String editing_mode) {
		super(editingContext, artefactDefinition, editing_mode);
		parts = new String[] { ARTEFACTDEFINITIONARTEFACT_PART };
		repositoryKey = EvidenceViewsRepository.class;
		partKey = EvidenceViewsRepository.ArtefactDefinitionArtefact.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final ArtefactDefinition artefactDefinition = (ArtefactDefinition)elt;
			final ArtefactDefinitionArtefactPropertiesEditionPart artefactDefinitionArtefactPart = (ArtefactDefinitionArtefactPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact)) {
				artefactSettings = new ReferencesTableSettings(artefactDefinition, EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact());
				artefactDefinitionArtefactPart.initArtefact(artefactSettings);
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable)) {
				artefactTableSettings = new ReferencesTableSettings(artefactDefinition, EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact());
				artefactDefinitionArtefactPart.initArtefactTable(artefactTableSettings);
			}
			// init filters
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact)) {
				artefactDefinitionArtefactPart.addFilterToArtefact(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Artefact); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for artefact
				// End of user code
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable)) {
				artefactDefinitionArtefactPart.addFilterToArtefactTable(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Artefact); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for artefactTable
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact) {
			return EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable) {
			return EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		ArtefactDefinition artefactDefinition = (ArtefactDefinition)semanticObject;
		if (EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, artefactSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				artefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				artefactSettings.move(event.getNewIndex(), (Artefact) event.getNewValue());
			}
		}
		if (EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, artefactTableSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				artefactTableSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				artefactTableSettings.move(event.getNewIndex(), (Artefact) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ArtefactDefinitionArtefactPropertiesEditionPart artefactDefinitionArtefactPart = (ArtefactDefinitionArtefactPropertiesEditionPart)editingPart;
			if (EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefact))
				artefactDefinitionArtefactPart.updateArtefact();
			if (EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactDefinitionArtefact.Properties.artefactTable))
				artefactDefinitionArtefactPart.updateArtefactTable();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact(),
			EvidencePackage.eINSTANCE.getArtefactDefinition_Artefact()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
