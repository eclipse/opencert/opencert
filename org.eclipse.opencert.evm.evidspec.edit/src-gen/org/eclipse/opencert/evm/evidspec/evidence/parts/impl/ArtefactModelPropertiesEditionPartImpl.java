/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactModelPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ArtefactModelPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected ReferencesTable artefact;
	protected List<ViewerFilter> artefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> artefactFilters = new ArrayList<ViewerFilter>();
	protected Text repoUrl;
	protected Text repoUser;
	protected Text repoPassword;
	protected Text repoLocalPath;
	protected Button repoUsesLocal;

	
	// Start IRR
		protected Button copyRepo;
		private static final String SVNInfo_REPOSITORY_TYPE = "OPENCERT_SVN_REPOSITORY_TYPE";
		private static final String SVNInfo_LOCAL_REPOSITORY_URL = "OPENCERT_SVN_LOCAL_REPOSITORY_URL";
		private static final String SVNInfo_REMOTE_REPOSITORY_URL = "OPENCERT_SVN_REMOTE_REPOSITORY_URL";
		private static final String SVNInfo_USER = "OPENCERT_SVN_USER";
		private static final String SVNInfo_PASS = "OPENCERT_SVN_PASS";
		// End IRR


	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactModelPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence artefactModelStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactModelStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.class);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.id);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.name);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.description);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.artefact);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.repoUrl);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.repoUser);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.repoPassword);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal);
		
		
		composer = new PartComposer(artefactModelStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.id) {
					return createIdText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.name) {
					return createNameText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.artefact) {
					return createArtefactAdvancedTableComposition(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.repoUrl) {
					return createRepoUrlText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.repoUser) {
					return createRepoUserText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.repoPassword) {
					return createRepoPasswordText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath) {
					return createRepoLocalPathText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal) {
					return createRepoUsesLocalCheckbox(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ArtefactModelPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactModel.Properties.id, EvidenceMessages.ArtefactModelPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, EvidenceViewsRepository.ArtefactModel.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.id, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactModel.Properties.name, EvidenceMessages.ArtefactModelPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, EvidenceViewsRepository.ArtefactModel.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.name, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactModel.Properties.description, EvidenceMessages.ArtefactModelPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, EvidenceViewsRepository.ArtefactModel.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.description, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createArtefactAdvancedTableComposition(Composite parent) {
		this.artefact = new ReferencesTable(getDescription(EvidenceViewsRepository.ArtefactModel.Properties.artefact, EvidenceMessages.ArtefactModelPropertiesEditionPart_ArtefactLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				artefact.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				artefact.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				artefact.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.artefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				artefact.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.artefactFilters) {
			this.artefact.addFilter(filter);
		}
		this.artefact.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.artefact, EvidenceViewsRepository.SWT_KIND));
		this.artefact.createControls(parent);
		this.artefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.artefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData artefactData = new GridData(GridData.FILL_HORIZONTAL);
		artefactData.horizontalSpan = 3;
		this.artefact.setLayoutData(artefactData);
		this.artefact.setLowerBound(0);
		this.artefact.setUpperBound(-1);
		artefact.setID(EvidenceViewsRepository.ArtefactModel.Properties.artefact);
		artefact.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createArtefactAdvancedTableComposition

		// End of user code
		return parent;
	}

	
	protected Composite createRepoUrlText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactModel.Properties.repoUrl, EvidenceMessages.ArtefactModelPropertiesEditionPart_RepoUrlLabel);
		repoUrl = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData repoUrlData = new GridData(GridData.FILL_HORIZONTAL);
		repoUrl.setLayoutData(repoUrlData);
		repoUrl.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUrl, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoUrl.getText()));
			}

		});
		repoUrl.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUrl, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoUrl.getText()));
				}
			}

		});
		EditingUtils.setID(repoUrl, EvidenceViewsRepository.ArtefactModel.Properties.repoUrl);
		EditingUtils.setEEFtype(repoUrl, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.repoUrl, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRepoUrlText

		// End of user code
		return parent;
	}

	
	protected Composite createRepoUserText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactModel.Properties.repoUser, EvidenceMessages.ArtefactModelPropertiesEditionPart_RepoUserLabel);
		repoUser = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData repoUserData = new GridData(GridData.FILL_HORIZONTAL);
		repoUser.setLayoutData(repoUserData);
		repoUser.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUser, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoUser.getText()));
			}

		});
		repoUser.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUser, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoUser.getText()));
				}
			}

		});
		EditingUtils.setID(repoUser, EvidenceViewsRepository.ArtefactModel.Properties.repoUser);
		EditingUtils.setEEFtype(repoUser, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.repoUser, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRepoUserText

		// End of user code
		return parent;
	}

	
	protected Composite createRepoPasswordText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactModel.Properties.repoPassword, EvidenceMessages.ArtefactModelPropertiesEditionPart_RepoPasswordLabel);
		repoPassword = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData repoPasswordData = new GridData(GridData.FILL_HORIZONTAL);
		repoPassword.setLayoutData(repoPasswordData);
		
		// Start IRR
		repoPassword.setEchoChar('*');
		// End IRR
		
		repoPassword.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoPassword, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoPassword.getText()));
			}

		});
		repoPassword.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoPassword, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoPassword.getText()));
				}
			}

		});
		EditingUtils.setID(repoPassword, EvidenceViewsRepository.ArtefactModel.Properties.repoPassword);
		EditingUtils.setEEFtype(repoPassword, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.repoPassword, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRepoPasswordText

		// End of user code
		return parent;
	}

	
	protected Composite createRepoLocalPathText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath, EvidenceMessages.ArtefactModelPropertiesEditionPart_RepoLocalPathLabel);
		repoLocalPath = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData repoLocalPathData = new GridData(GridData.FILL_HORIZONTAL);
		repoLocalPath.setLayoutData(repoLocalPathData);
		repoLocalPath.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoLocalPath.getText()));
			}

		});
		repoLocalPath.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoLocalPath.getText()));
				}
			}

		});
		EditingUtils.setID(repoLocalPath, EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath);
		EditingUtils.setEEFtype(repoLocalPath, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRepoLocalPathText

		// End of user code
		return parent;
	}

	
	protected Composite createRepoUsesLocalCheckbox(Composite parent) {
		repoUsesLocal = new Button(parent, SWT.CHECK);
		repoUsesLocal.setText(getDescription(EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal, EvidenceMessages.ArtefactModelPropertiesEditionPart_RepoUsesLocalLabel));
		repoUsesLocal.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(repoUsesLocal.getSelection())));
			}

		});
		GridData repoUsesLocalData = new GridData(GridData.FILL_HORIZONTAL);
		repoUsesLocalData.horizontalSpan = 2;
		repoUsesLocal.setLayoutData(repoUsesLocalData);
		
		// Start IRR
		copyRepo = new Button(parent, 0);
		copyRepo.setText("Copy Preferences");
		GridData copyRepoData = new GridData(GridData.FILL_HORIZONTAL);
		copyRepoData.horizontalSpan = 2;
		copyRepo.setLayoutData(copyRepoData);
		
		copyRepo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int option = JOptionPane.showConfirmDialog(null, "This action removes the value of repository's variables and assigns the default value defined in opencert. �is this OK?", "Copy Repository opencert Preferences",JOptionPane.OK_CANCEL_OPTION ,JOptionPane.INFORMATION_MESSAGE);
				if (option == JOptionPane.OK_OPTION) {
					boolean useLocalRepository = PlatformUI.getPreferenceStore().getBoolean(SVNInfo_REPOSITORY_TYPE);

					if (PlatformUI.getPreferenceStore().getString(SVNInfo_LOCAL_REPOSITORY_URL) != null)
						repoLocalPath.setText(PlatformUI.getPreferenceStore().getString(SVNInfo_LOCAL_REPOSITORY_URL));
					repoUsesLocal.setSelection(useLocalRepository);
					if (PlatformUI.getPreferenceStore().getString(SVNInfo_REMOTE_REPOSITORY_URL) != null)
						repoUrl.setText(PlatformUI.getPreferenceStore().getString(SVNInfo_REMOTE_REPOSITORY_URL));   
					if (PlatformUI.getPreferenceStore().getString(SVNInfo_USER) != null)
						repoUser.setText(PlatformUI.getPreferenceStore().getString(SVNInfo_USER));
					if (PlatformUI.getPreferenceStore().getString(SVNInfo_PASS) != null)
						repoPassword.setText(PlatformUI.getPreferenceStore().getString(SVNInfo_PASS));

					if (propertiesEditionComponent != null){
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoLocalPath.getText()));	
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoUsesLocal.getSelection()));
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUrl, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoUrl.getText()));
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoUser, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoUser.getText()));
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactModelPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactModel.Properties.repoPassword, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, repoPassword.getText()));
					}
				}
			}

		});
		// End IRR
		

		
		
		
		
		
		EditingUtils.setID(repoUsesLocal, EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal);
		EditingUtils.setEEFtype(repoUsesLocal, "eef::Checkbox"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createRepoUsesLocalCheckbox

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#initArtefact(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		artefact.setContentProvider(contentProvider);
		artefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.artefact);
		if (eefElementEditorReadOnlyState && artefact.isEnabled()) {
			artefact.setEnabled(false);
			artefact.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !artefact.isEnabled()) {
			artefact.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#updateArtefact()
	 * 
	 */
	public void updateArtefact() {
	artefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#addFilterArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToArtefact(ViewerFilter filter) {
		artefactFilters.add(filter);
		if (this.artefact != null) {
			this.artefact.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#addBusinessFilterArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToArtefact(ViewerFilter filter) {
		artefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#isContainedInArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInArtefactTable(EObject element) {
		return ((ReferencesTableSettings)artefact.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getRepoUrl()
	 * 
	 */
	public String getRepoUrl() {
		return repoUrl.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setRepoUrl(String newValue)
	 * 
	 */
	public void setRepoUrl(String newValue) {
		if (newValue != null) {
			repoUrl.setText(newValue);
		} else {
			repoUrl.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.repoUrl);
		if (eefElementEditorReadOnlyState && repoUrl.isEnabled()) {
			repoUrl.setEnabled(false);
			repoUrl.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !repoUrl.isEnabled()) {
			repoUrl.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getRepoUser()
	 * 
	 */
	public String getRepoUser() {
		return repoUser.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setRepoUser(String newValue)
	 * 
	 */
	public void setRepoUser(String newValue) {
		if (newValue != null) {
			repoUser.setText(newValue);
		} else {
			repoUser.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.repoUser);
		if (eefElementEditorReadOnlyState && repoUser.isEnabled()) {
			repoUser.setEnabled(false);
			repoUser.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !repoUser.isEnabled()) {
			repoUser.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getRepoPassword()
	 * 
	 */
	public String getRepoPassword() {
		return repoPassword.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setRepoPassword(String newValue)
	 * 
	 */
	public void setRepoPassword(String newValue) {
		if (newValue != null) {
			repoPassword.setText(newValue);
		} else {
			repoPassword.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.repoPassword);
		if (eefElementEditorReadOnlyState && repoPassword.isEnabled()) {
			repoPassword.setEnabled(false);
			repoPassword.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !repoPassword.isEnabled()) {
			repoPassword.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getRepoLocalPath()
	 * 
	 */
	public String getRepoLocalPath() {
		return repoLocalPath.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setRepoLocalPath(String newValue)
	 * 
	 */
	public void setRepoLocalPath(String newValue) {
		if (newValue != null) {
			repoLocalPath.setText(newValue);
		} else {
			repoLocalPath.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.repoLocalPath);
		if (eefElementEditorReadOnlyState && repoLocalPath.isEnabled()) {
			repoLocalPath.setEnabled(false);
			repoLocalPath.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !repoLocalPath.isEnabled()) {
			repoLocalPath.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#getRepoUsesLocal()
	 * 
	 */
	public Boolean getRepoUsesLocal() {
		return Boolean.valueOf(repoUsesLocal.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactModelPropertiesEditionPart#setRepoUsesLocal(Boolean newValue)
	 * 
	 */
	public void setRepoUsesLocal(Boolean newValue) {
		if (newValue != null) {
			repoUsesLocal.setSelection(newValue.booleanValue());
		} else {
			repoUsesLocal.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactModel.Properties.repoUsesLocal);
		if (eefElementEditorReadOnlyState && repoUsesLocal.isEnabled()) {
			repoUsesLocal.setEnabled(false);
			repoUsesLocal.setToolTipText(EvidenceMessages.ArtefactModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !repoUsesLocal.isEnabled()) {
			repoUsesLocal.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactModel_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
