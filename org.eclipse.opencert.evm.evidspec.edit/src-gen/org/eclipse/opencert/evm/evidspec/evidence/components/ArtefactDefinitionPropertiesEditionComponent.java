/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;

import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionArtefactPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEvaluationPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionEventsPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactDefinitionPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class ArtefactDefinitionPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private ArtefactDefinitionPropertiesEditionPart basePart;

	/**
	 * The ArtefactDefinitionBasePropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactDefinitionBasePropertiesEditionComponent artefactDefinitionBasePropertiesEditionComponent;

	/**
	 * The ArtefactDefinitionArtefact part
	 * 
	 */
	private ArtefactDefinitionArtefactPropertiesEditionPart artefactDefinitionArtefactPart;

	/**
	 * The ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent artefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent;

	/**
	 * The ArtefactDefinitionEvaluation part
	 * 
	 */
	private ArtefactDefinitionEvaluationPropertiesEditionPart artefactDefinitionEvaluationPart;

	/**
	 * The ArtefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent artefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent;

	/**
	 * The ArtefactDefinitionEvents part
	 * 
	 */
	private ArtefactDefinitionEventsPropertiesEditionPart artefactDefinitionEventsPart;

	/**
	 * The ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent artefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param artefactDefinition the EObject to edit
	 * 
	 */
	public ArtefactDefinitionPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject artefactDefinition, String editing_mode) {
		super(editingContext, editing_mode);
		if (artefactDefinition instanceof ArtefactDefinition) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefactDefinition, PropertiesEditingProvider.class);
			artefactDefinitionBasePropertiesEditionComponent = (ArtefactDefinitionBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactDefinitionBasePropertiesEditionComponent.BASE_PART, ArtefactDefinitionBasePropertiesEditionComponent.class);
			addSubComponent(artefactDefinitionBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefactDefinition, PropertiesEditingProvider.class);
			artefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent = (ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent.ARTEFACTDEFINITIONARTEFACT_PART, ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent.class);
			addSubComponent(artefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefactDefinition, PropertiesEditingProvider.class);
			artefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent = (ArtefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent.ARTEFACTDEFINITIONEVALUATION_PART, ArtefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent.class);
			addSubComponent(artefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefactDefinition, PropertiesEditingProvider.class);
			artefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent = (ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent.ARTEFACTDEFINITIONEVENTS_PART, ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent.class);
			addSubComponent(artefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (ArtefactDefinitionBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (ArtefactDefinitionPropertiesEditionPart)artefactDefinitionBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (ArtefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent.ARTEFACTDEFINITIONARTEFACT_PART.equals(key)) {
			artefactDefinitionArtefactPart = (ArtefactDefinitionArtefactPropertiesEditionPart)artefactDefinitionArtefactDefinitionArtefactPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactDefinitionArtefactPart;
		}
		if (ArtefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent.ARTEFACTDEFINITIONEVALUATION_PART.equals(key)) {
			artefactDefinitionEvaluationPart = (ArtefactDefinitionEvaluationPropertiesEditionPart)artefactDefinitionArtefactDefinitionEvaluationPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactDefinitionEvaluationPart;
		}
		if (ArtefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent.ARTEFACTDEFINITIONEVENTS_PART.equals(key)) {
			artefactDefinitionEventsPart = (ArtefactDefinitionEventsPropertiesEditionPart)artefactDefinitionArtefactDefinitionEventsPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactDefinitionEventsPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (EvidenceViewsRepository.ArtefactDefinition.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (ArtefactDefinitionPropertiesEditionPart)propertiesEditionPart;
		}
		if (EvidenceViewsRepository.ArtefactDefinitionArtefact.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactDefinitionArtefactPart = (ArtefactDefinitionArtefactPropertiesEditionPart)propertiesEditionPart;
		}
		if (EvidenceViewsRepository.ArtefactDefinitionEvaluation.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactDefinitionEvaluationPart = (ArtefactDefinitionEvaluationPropertiesEditionPart)propertiesEditionPart;
		}
		if (EvidenceViewsRepository.ArtefactDefinitionEvents.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactDefinitionEventsPart = (ArtefactDefinitionEventsPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == EvidenceViewsRepository.ArtefactDefinition.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == EvidenceViewsRepository.ArtefactDefinitionArtefact.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == EvidenceViewsRepository.ArtefactDefinitionEvaluation.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == EvidenceViewsRepository.ArtefactDefinitionEvents.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
