/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ArtefactVersionPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ArtefactVersionPropertiesEditionPart {

	protected Text versionID;
	protected Text date;
	protected Text changes;
	protected Button isLastVersion;
	protected Button isTemplate;
	protected Button isConfigurable;
	protected ReferencesTable resource;
	protected List<ViewerFilter> resourceBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> resourceFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ArtefactVersionPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence artefactVersionStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = artefactVersionStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.class);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.versionID);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.date);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.changes);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable);
		propertiesStep.addStep(EvidenceViewsRepository.ArtefactVersion.Properties.resource);
		
		
		composer = new PartComposer(artefactVersionStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.versionID) {
					return createVersionIDText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.date) {
					return createDateText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.changes) {
					return createChangesText(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion) {
					return createIsLastVersionCheckbox(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate) {
					return createIsTemplateCheckbox(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable) {
					return createIsConfigurableCheckbox(parent);
				}
				if (key == EvidenceViewsRepository.ArtefactVersion.Properties.resource) {
					return createResourceAdvancedTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ArtefactVersionPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createVersionIDText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactVersion.Properties.versionID, EvidenceMessages.ArtefactVersionPropertiesEditionPart_VersionIDLabel);
		versionID = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData versionIDData = new GridData(GridData.FILL_HORIZONTAL);
		versionID.setLayoutData(versionIDData);
		versionID.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.versionID, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, versionID.getText()));
			}

		});
		versionID.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.versionID, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, versionID.getText()));
				}
			}

		});
		EditingUtils.setID(versionID, EvidenceViewsRepository.ArtefactVersion.Properties.versionID);
		EditingUtils.setEEFtype(versionID, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactVersion.Properties.versionID, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createVersionIDText

		// End of user code
		return parent;
	}

	
	protected Composite createDateText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactVersion.Properties.date, EvidenceMessages.ArtefactVersionPropertiesEditionPart_DateLabel);
		date = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData dateData = new GridData(GridData.FILL_HORIZONTAL);
		date.setLayoutData(dateData);
		date.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.date, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, date.getText()));
			}

		});
		date.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.date, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, date.getText()));
				}
			}

		});
		EditingUtils.setID(date, EvidenceViewsRepository.ArtefactVersion.Properties.date);
		EditingUtils.setEEFtype(date, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactVersion.Properties.date, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDateText

		// End of user code
		return parent;
	}

	
	protected Composite createChangesText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.ArtefactVersion.Properties.changes, EvidenceMessages.ArtefactVersionPropertiesEditionPart_ChangesLabel);
		changes = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData changesData = new GridData(GridData.FILL_HORIZONTAL);
		changes.setLayoutData(changesData);
		changes.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.changes, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, changes.getText()));
			}

		});
		changes.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.changes, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, changes.getText()));
				}
			}

		});
		EditingUtils.setID(changes, EvidenceViewsRepository.ArtefactVersion.Properties.changes);
		EditingUtils.setEEFtype(changes, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactVersion.Properties.changes, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createChangesText

		// End of user code
		return parent;
	}

	
	protected Composite createIsLastVersionCheckbox(Composite parent) {
		isLastVersion = new Button(parent, SWT.CHECK);
		isLastVersion.setText(getDescription(EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion, EvidenceMessages.ArtefactVersionPropertiesEditionPart_IsLastVersionLabel));
		isLastVersion.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(isLastVersion.getSelection())));
			}

		});
		GridData isLastVersionData = new GridData(GridData.FILL_HORIZONTAL);
		isLastVersionData.horizontalSpan = 2;
		isLastVersion.setLayoutData(isLastVersionData);
		EditingUtils.setID(isLastVersion, EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion);
		EditingUtils.setEEFtype(isLastVersion, "eef::Checkbox"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIsLastVersionCheckbox

		// End of user code
		return parent;
	}

	
	protected Composite createIsTemplateCheckbox(Composite parent) {
		isTemplate = new Button(parent, SWT.CHECK);
		isTemplate.setText(getDescription(EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate, EvidenceMessages.ArtefactVersionPropertiesEditionPart_IsTemplateLabel));
		isTemplate.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(isTemplate.getSelection())));
			}

		});
		GridData isTemplateData = new GridData(GridData.FILL_HORIZONTAL);
		isTemplateData.horizontalSpan = 2;
		isTemplate.setLayoutData(isTemplateData);
		EditingUtils.setID(isTemplate, EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate);
		EditingUtils.setEEFtype(isTemplate, "eef::Checkbox"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIsTemplateCheckbox

		// End of user code
		return parent;
	}

	
	protected Composite createIsConfigurableCheckbox(Composite parent) {
		isConfigurable = new Button(parent, SWT.CHECK);
		isConfigurable.setText(getDescription(EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable, EvidenceMessages.ArtefactVersionPropertiesEditionPart_IsConfigurableLabel));
		isConfigurable.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 *
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 	
			 */
			public void widgetSelected(SelectionEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new Boolean(isConfigurable.getSelection())));
			}

		});
		GridData isConfigurableData = new GridData(GridData.FILL_HORIZONTAL);
		isConfigurableData.horizontalSpan = 2;
		isConfigurable.setLayoutData(isConfigurableData);
		EditingUtils.setID(isConfigurable, EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable);
		EditingUtils.setEEFtype(isConfigurable, "eef::Checkbox"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIsConfigurableCheckbox

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createResourceAdvancedTableComposition(Composite parent) {
		this.resource = new ReferencesTable(getDescription(EvidenceViewsRepository.ArtefactVersion.Properties.resource, EvidenceMessages.ArtefactVersionPropertiesEditionPart_ResourceLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.resource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				resource.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.resource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				resource.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.resource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				resource.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.resource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				resource.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.resourceFilters) {
			this.resource.addFilter(filter);
		}
		this.resource.setHelpText(propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.ArtefactVersion.Properties.resource, EvidenceViewsRepository.SWT_KIND));
		this.resource.createControls(parent);
		this.resource.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ArtefactVersionPropertiesEditionPartImpl.this, EvidenceViewsRepository.ArtefactVersion.Properties.resource, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData resourceData = new GridData(GridData.FILL_HORIZONTAL);
		resourceData.horizontalSpan = 3;
		this.resource.setLayoutData(resourceData);
		this.resource.setLowerBound(0);
		this.resource.setUpperBound(-1);
		resource.setID(EvidenceViewsRepository.ArtefactVersion.Properties.resource);
		resource.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createResourceAdvancedTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
			Shell thisS= view.getShell();
			MessageDialog.openInformation(thisS, "Confirm", "Artefact Changed");
			// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#getVersionID()
	 * 
	 */
	public String getVersionID() {
		return versionID.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#setVersionID(String newValue)
	 * 
	 */
	public void setVersionID(String newValue) {
		if (newValue != null) {
			versionID.setText(newValue);
		} else {
			versionID.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactVersion.Properties.versionID);
		if (eefElementEditorReadOnlyState && versionID.isEnabled()) {
			versionID.setEnabled(false);
			versionID.setToolTipText(EvidenceMessages.ArtefactVersion_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !versionID.isEnabled()) {
			versionID.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#getDate()
	 * 
	 */
	public String getDate() {
		return date.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#setDate(String newValue)
	 * 
	 */
	public void setDate(String newValue) {
		if (newValue != null) {
			date.setText(newValue);
		} else {
			date.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactVersion.Properties.date);
		if (eefElementEditorReadOnlyState && date.isEnabled()) {
			date.setEnabled(false);
			date.setToolTipText(EvidenceMessages.ArtefactVersion_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !date.isEnabled()) {
			date.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#getChanges()
	 * 
	 */
	public String getChanges() {
		return changes.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#setChanges(String newValue)
	 * 
	 */
	public void setChanges(String newValue) {
		if (newValue != null) {
			changes.setText(newValue);
		} else {
			changes.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactVersion.Properties.changes);
		if (eefElementEditorReadOnlyState && changes.isEnabled()) {
			changes.setEnabled(false);
			changes.setToolTipText(EvidenceMessages.ArtefactVersion_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !changes.isEnabled()) {
			changes.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#getIsLastVersion()
	 * 
	 */
	public Boolean getIsLastVersion() {
		return Boolean.valueOf(isLastVersion.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#setIsLastVersion(Boolean newValue)
	 * 
	 */
	public void setIsLastVersion(Boolean newValue) {
		if (newValue != null) {
			isLastVersion.setSelection(newValue.booleanValue());
		} else {
			isLastVersion.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactVersion.Properties.isLastVersion);
		if (eefElementEditorReadOnlyState && isLastVersion.isEnabled()) {
			isLastVersion.setEnabled(false);
			isLastVersion.setToolTipText(EvidenceMessages.ArtefactVersion_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !isLastVersion.isEnabled()) {
			isLastVersion.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#getIsTemplate()
	 * 
	 */
	public Boolean getIsTemplate() {
		return Boolean.valueOf(isTemplate.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#setIsTemplate(Boolean newValue)
	 * 
	 */
	public void setIsTemplate(Boolean newValue) {
		if (newValue != null) {
			isTemplate.setSelection(newValue.booleanValue());
		} else {
			isTemplate.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactVersion.Properties.isTemplate);
		if (eefElementEditorReadOnlyState && isTemplate.isEnabled()) {
			isTemplate.setEnabled(false);
			isTemplate.setToolTipText(EvidenceMessages.ArtefactVersion_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !isTemplate.isEnabled()) {
			isTemplate.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#getIsConfigurable()
	 * 
	 */
	public Boolean getIsConfigurable() {
		return Boolean.valueOf(isConfigurable.getSelection());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#setIsConfigurable(Boolean newValue)
	 * 
	 */
	public void setIsConfigurable(Boolean newValue) {
		if (newValue != null) {
			isConfigurable.setSelection(newValue.booleanValue());
		} else {
			isConfigurable.setSelection(false);
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactVersion.Properties.isConfigurable);
		if (eefElementEditorReadOnlyState && isConfigurable.isEnabled()) {
			isConfigurable.setEnabled(false);
			isConfigurable.setToolTipText(EvidenceMessages.ArtefactVersion_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !isConfigurable.isEnabled()) {
			isConfigurable.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#initResource(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initResource(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		resource.setContentProvider(contentProvider);
		resource.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.ArtefactVersion.Properties.resource);
		if (eefElementEditorReadOnlyState && resource.isEnabled()) {
			resource.setEnabled(false);
			resource.setToolTipText(EvidenceMessages.ArtefactVersion_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !resource.isEnabled()) {
			resource.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#updateResource()
	 * 
	 */
	public void updateResource() {
	resource.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#addFilterResource(ViewerFilter filter)
	 * 
	 */
	public void addFilterToResource(ViewerFilter filter) {
		resourceFilters.add(filter);
		if (this.resource != null) {
			this.resource.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#addBusinessFilterResource(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToResource(ViewerFilter filter) {
		resourceBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart#isContainedInResourceTable(EObject element)
	 * 
	 */
	public boolean isContainedInResourceTable(EObject element) {
		return ((ReferencesTableSettings)resource.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.ArtefactVersion_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
