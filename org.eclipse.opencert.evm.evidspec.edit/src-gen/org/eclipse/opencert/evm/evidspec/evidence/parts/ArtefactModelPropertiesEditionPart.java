/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ArtefactModelPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);




	/**
	 * Init the artefact
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initArtefact(ReferencesTableSettings settings);

	/**
	 * Update the artefact
	 * @param newValue the artefact to update
	 * 
	 */
	public void updateArtefact();

	/**
	 * Adds the given filter to the artefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToArtefact(ViewerFilter filter);

	/**
	 * Adds the given filter to the artefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToArtefact(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the artefact table
	 * 
	 */
	public boolean isContainedInArtefactTable(EObject element);


	/**
	 * @return the repoUrl
	 * 
	 */
	public String getRepoUrl();

	/**
	 * Defines a new repoUrl
	 * @param newValue the new repoUrl to set
	 * 
	 */
	public void setRepoUrl(String newValue);


	/**
	 * @return the repoUser
	 * 
	 */
	public String getRepoUser();

	/**
	 * Defines a new repoUser
	 * @param newValue the new repoUser to set
	 * 
	 */
	public void setRepoUser(String newValue);


	/**
	 * @return the repoPassword
	 * 
	 */
	public String getRepoPassword();

	/**
	 * Defines a new repoPassword
	 * @param newValue the new repoPassword to set
	 * 
	 */
	public void setRepoPassword(String newValue);


	/**
	 * @return the repoLocalPath
	 * 
	 */
	public String getRepoLocalPath();

	/**
	 * Defines a new repoLocalPath
	 * @param newValue the new repoLocalPath to set
	 * 
	 */
	public void setRepoLocalPath(String newValue);


	/**
	 * @return the repoUsesLocal
	 * 
	 */
	public Boolean getRepoUsesLocal();

	/**
	 * Defines a new repoUsesLocal
	 * @param newValue the new repoUsesLocal to set
	 * 
	 */
	public void setRepoUsesLocal(Boolean newValue);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
