/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class EvidenceMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.evm.evidspec.evidence.providers.evidenceMessages"; //$NON-NLS-1$

	
	public static String ArtefactModelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactDefinitionPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ValuePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ResourcePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactDefinitionArtefactPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactDefinitionEvaluationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactDefinitionEventsPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactPropertyValuePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactEvaluationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactEventsPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String ArtefactModel_ReadOnly;

	
	public static String ArtefactModel_Part_Title;

	
	public static String ArtefactDefinition_ReadOnly;

	
	public static String ArtefactDefinition_Part_Title;

	
	public static String Artefact_ReadOnly;

	
	public static String Artefact_Part_Title;

	
	public static String Value_ReadOnly;

	
	public static String Value_Part_Title;

	
	public static String Resource_ReadOnly;

	
	public static String Resource_Part_Title;

	
	public static String ArtefactRel_ReadOnly;

	
	public static String ArtefactRel_Part_Title;

	
	public static String ArtefactDefinitionArtefact_ReadOnly;

	
	public static String ArtefactDefinitionArtefact_Part_Title;

	
	public static String ArtefactDefinitionEvaluation_ReadOnly;

	
	public static String ArtefactDefinitionEvaluation_Part_Title;

	
	public static String ArtefactDefinitionEvents_ReadOnly;

	
	public static String ArtefactDefinitionEvents_Part_Title;

	
	public static String ArtefactVersion_ReadOnly;

	
	public static String ArtefactVersion_Part_Title;

	
	public static String ArtefactPropertyValue_ReadOnly;

	
	public static String ArtefactPropertyValue_Part_Title;

	
	public static String ArtefactEvaluation_ReadOnly;

	
	public static String ArtefactEvaluation_Part_Title;

	
	public static String ArtefactEvents_ReadOnly;

	
	public static String ArtefactEvents_Part_Title;


	
	public static String ArtefactModelPropertiesEditionPart_IdLabel;

	
	public static String ArtefactModelPropertiesEditionPart_NameLabel;

	
	public static String ArtefactModelPropertiesEditionPart_DescriptionLabel;

	
	public static String ArtefactModelPropertiesEditionPart_ArtefactLabel;

	
	public static String ArtefactModelPropertiesEditionPart_RepoUrlLabel;

	
	public static String ArtefactModelPropertiesEditionPart_RepoUserLabel;

	
	public static String ArtefactModelPropertiesEditionPart_RepoPasswordLabel;

	
	public static String ArtefactModelPropertiesEditionPart_RepoLocalPathLabel;

	
	public static String ArtefactModelPropertiesEditionPart_RepoUsesLocalLabel;

	
	public static String ArtefactDefinitionPropertiesEditionPart_IdLabel;

	
	public static String ArtefactDefinitionPropertiesEditionPart_NameLabel;

	
	public static String ArtefactDefinitionPropertiesEditionPart_DescriptionLabel;

	
	public static String ArtefactPropertiesEditionPart_IdLabel;

	
	public static String ArtefactPropertiesEditionPart_NameLabel;

	
	public static String ArtefactPropertiesEditionPart_DescriptionLabel;

	
	public static String ArtefactPropertiesEditionPart_PrecedentVersionLabel;

	
	public static String ArtefactPropertiesEditionPart_OwnedRelLabel;

	
	public static String ArtefactPropertiesEditionPart_ArtefactPartLabel;

	
	public static String ValuePropertiesEditionPart_NameLabel;

	
	public static String ValuePropertiesEditionPart_ValueLabel;

	
	public static String ValuePropertiesEditionPart_PropertyReferenceLabel;

	
	public static String ValuePropertiesEditionPart_PropertyComboLabel;

	
	public static String ResourcePropertiesEditionPart_IdLabel;

	
	public static String ResourcePropertiesEditionPart_NameLabel;

	
	public static String ResourcePropertiesEditionPart_DescriptionLabel;

	
	public static String ResourcePropertiesEditionPart_LocationLabel;

	
	public static String ResourcePropertiesEditionPart_FormatLabel;

	
	public static String ArtefactRelPropertiesEditionPart_IdLabel;

	
	public static String ArtefactRelPropertiesEditionPart_NameLabel;

	
	public static String ArtefactRelPropertiesEditionPart_DescriptionLabel;

	
	public static String ArtefactRelPropertiesEditionPart_ModificationEffectLabel;

	
	public static String ArtefactRelPropertiesEditionPart_RevocationEffectLabel;

	
	public static String ArtefactRelPropertiesEditionPart_SourceLabel;

	
	public static String ArtefactRelPropertiesEditionPart_TargetLabel;

	
	public static String ArtefactDefinitionArtefactPropertiesEditionPart_ArtefactLabel;

	
	public static String ArtefactDefinitionArtefactPropertiesEditionPart_ArtefactTableLabel;

	
	public static String ArtefactDefinitionEvaluationPropertiesEditionPart_EvaluationLabel;

	
	public static String ArtefactDefinitionEvaluationPropertiesEditionPart_EvaluationTableLabel;

	
	public static String ArtefactDefinitionEventsPropertiesEditionPart_LifecycleEventLabel;

	
	public static String ArtefactDefinitionEventsPropertiesEditionPart_LifecycleEventTableLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_VersionIDLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_DateLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_ChangesLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_IsLastVersionLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_IsTemplateLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_IsConfigurableLabel;

	
	public static String ArtefactVersionPropertiesEditionPart_ResourceLabel;

	
	public static String ArtefactPropertyValuePropertiesEditionPart_PropertyValueLabel;

	
	public static String ArtefactPropertyValuePropertiesEditionPart_PropertyValueTableLabel;

	
	public static String ArtefactEvaluationPropertiesEditionPart_EvaluationLabel;

	
	public static String ArtefactEvaluationPropertiesEditionPart_EvaluationTableLabel;

	
	public static String ArtefactEventsPropertiesEditionPart_LifecycleEventLabel;

	
	public static String ArtefactEventsPropertiesEditionPart_LifecycleEventTableLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, EvidenceMessages.class);
	}

	
	private EvidenceMessages() {
		//protect instanciation
	}
}
