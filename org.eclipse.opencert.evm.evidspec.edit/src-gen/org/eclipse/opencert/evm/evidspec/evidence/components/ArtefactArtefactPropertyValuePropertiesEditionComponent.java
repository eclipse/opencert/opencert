/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.evm.evidspec.evidence.Value;

import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ArtefactArtefactPropertyValuePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ARTEFACTPROPERTYVALUE_PART = "ArtefactPropertyValue"; //$NON-NLS-1$

	
	/**
	 * Settings for propertyValue ReferencesTable
	 */
	protected ReferencesTableSettings propertyValueSettings;
	
	/**
	 * Settings for propertyValueTable ReferencesTable
	 */
	protected ReferencesTableSettings propertyValueTableSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ArtefactArtefactPropertyValuePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject artefact, String editing_mode) {
		super(editingContext, artefact, editing_mode);
		parts = new String[] { ARTEFACTPROPERTYVALUE_PART };
		repositoryKey = EvidenceViewsRepository.class;
		partKey = EvidenceViewsRepository.ArtefactPropertyValue.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Artefact artefact = (Artefact)elt;
			final ArtefactPropertyValuePropertiesEditionPart artefactPropertyValuePart = (ArtefactPropertyValuePropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue)) {
				propertyValueSettings = new ReferencesTableSettings(artefact, EvidencePackage.eINSTANCE.getArtefact_PropertyValue());
				artefactPropertyValuePart.initPropertyValue(propertyValueSettings);
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable)) {
				propertyValueTableSettings = new ReferencesTableSettings(artefact, EvidencePackage.eINSTANCE.getArtefact_PropertyValue());
				artefactPropertyValuePart.initPropertyValueTable(propertyValueTableSettings);
			}
			// init filters
			if (isAccessible(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue)) {
				artefactPropertyValuePart.addFilterToPropertyValue(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Value); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for propertyValue
				// End of user code
			}
			if (isAccessible(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable)) {
				artefactPropertyValuePart.addFilterToPropertyValueTable(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Value); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for propertyValueTable
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue) {
			return EvidencePackage.eINSTANCE.getArtefact_PropertyValue();
		}
		if (editorKey == EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable) {
			return EvidencePackage.eINSTANCE.getArtefact_PropertyValue();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Artefact artefact = (Artefact)semanticObject;
		if (EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, propertyValueSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				propertyValueSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				propertyValueSettings.move(event.getNewIndex(), (Value) event.getNewValue());
			}
		}
		if (EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, propertyValueTableSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				propertyValueTableSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				propertyValueTableSettings.move(event.getNewIndex(), (Value) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ArtefactPropertyValuePropertiesEditionPart artefactPropertyValuePart = (ArtefactPropertyValuePropertiesEditionPart)editingPart;
			if (EvidencePackage.eINSTANCE.getArtefact_PropertyValue().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValue))
				artefactPropertyValuePart.updatePropertyValue();
			if (EvidencePackage.eINSTANCE.getArtefact_PropertyValue().equals(msg.getFeature()) && isAccessible(EvidenceViewsRepository.ArtefactPropertyValue.Properties.propertyValueTable))
				artefactPropertyValuePart.updatePropertyValueTable();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			EvidencePackage.eINSTANCE.getArtefact_PropertyValue(),
			EvidencePackage.eINSTANCE.getArtefact_PropertyValue()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == EvidenceViewsRepository.ArtefactVersion.Properties.versionID;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
