/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEvaluationPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactEventsPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactPropertyValuePropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ArtefactVersionPropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class ArtefactPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private ArtefactPropertiesEditionPart basePart;

	/**
	 * The ArtefactBasePropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactBasePropertiesEditionComponent artefactBasePropertiesEditionComponent;

	/**
	 * The ArtefactVersion part
	 * 
	 */
	private ArtefactVersionPropertiesEditionPart artefactVersionPart;

	/**
	 * The ArtefactArtefactVersionPropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactArtefactVersionPropertiesEditionComponent artefactArtefactVersionPropertiesEditionComponent;

	/**
	 * The ArtefactPropertyValue part
	 * 
	 */
	private ArtefactPropertyValuePropertiesEditionPart artefactPropertyValuePart;

	/**
	 * The ArtefactArtefactPropertyValuePropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactArtefactPropertyValuePropertiesEditionComponent artefactArtefactPropertyValuePropertiesEditionComponent;

	/**
	 * The ArtefactEvaluation part
	 * 
	 */
	private ArtefactEvaluationPropertiesEditionPart artefactEvaluationPart;

	/**
	 * The ArtefactArtefactEvaluationPropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactArtefactEvaluationPropertiesEditionComponent artefactArtefactEvaluationPropertiesEditionComponent;

	/**
	 * The ArtefactEvents part
	 * 
	 */
	private ArtefactEventsPropertiesEditionPart artefactEventsPart;

	/**
	 * The ArtefactArtefactEventsPropertiesEditionComponent sub component
	 * 
	 */
	protected ArtefactArtefactEventsPropertiesEditionComponent artefactArtefactEventsPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param artefact the EObject to edit
	 * 
	 */
	public ArtefactPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject artefact, String editing_mode) {
		super(editingContext, editing_mode);
		if (artefact instanceof Artefact) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefact, PropertiesEditingProvider.class);
			artefactBasePropertiesEditionComponent = (ArtefactBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactBasePropertiesEditionComponent.BASE_PART, ArtefactBasePropertiesEditionComponent.class);
			addSubComponent(artefactBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefact, PropertiesEditingProvider.class);
			artefactArtefactVersionPropertiesEditionComponent = (ArtefactArtefactVersionPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactArtefactVersionPropertiesEditionComponent.ARTEFACTVERSION_PART, ArtefactArtefactVersionPropertiesEditionComponent.class);
			addSubComponent(artefactArtefactVersionPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefact, PropertiesEditingProvider.class);
			artefactArtefactPropertyValuePropertiesEditionComponent = (ArtefactArtefactPropertyValuePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactArtefactPropertyValuePropertiesEditionComponent.ARTEFACTPROPERTYVALUE_PART, ArtefactArtefactPropertyValuePropertiesEditionComponent.class);
			addSubComponent(artefactArtefactPropertyValuePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefact, PropertiesEditingProvider.class);
			artefactArtefactEvaluationPropertiesEditionComponent = (ArtefactArtefactEvaluationPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactArtefactEvaluationPropertiesEditionComponent.ARTEFACTEVALUATION_PART, ArtefactArtefactEvaluationPropertiesEditionComponent.class);
			addSubComponent(artefactArtefactEvaluationPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(artefact, PropertiesEditingProvider.class);
			artefactArtefactEventsPropertiesEditionComponent = (ArtefactArtefactEventsPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ArtefactArtefactEventsPropertiesEditionComponent.ARTEFACTEVENTS_PART, ArtefactArtefactEventsPropertiesEditionComponent.class);
			addSubComponent(artefactArtefactEventsPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (ArtefactBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (ArtefactPropertiesEditionPart)artefactBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (ArtefactArtefactVersionPropertiesEditionComponent.ARTEFACTVERSION_PART.equals(key)) {
			artefactVersionPart = (ArtefactVersionPropertiesEditionPart)artefactArtefactVersionPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactVersionPart;
		}
		if (ArtefactArtefactPropertyValuePropertiesEditionComponent.ARTEFACTPROPERTYVALUE_PART.equals(key)) {
			artefactPropertyValuePart = (ArtefactPropertyValuePropertiesEditionPart)artefactArtefactPropertyValuePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactPropertyValuePart;
		}
		if (ArtefactArtefactEvaluationPropertiesEditionComponent.ARTEFACTEVALUATION_PART.equals(key)) {
			artefactEvaluationPart = (ArtefactEvaluationPropertiesEditionPart)artefactArtefactEvaluationPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactEvaluationPart;
		}
		if (ArtefactArtefactEventsPropertiesEditionComponent.ARTEFACTEVENTS_PART.equals(key)) {
			artefactEventsPart = (ArtefactEventsPropertiesEditionPart)artefactArtefactEventsPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactEventsPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (EvidenceViewsRepository.Artefact.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (ArtefactPropertiesEditionPart)propertiesEditionPart;
		}
		if (EvidenceViewsRepository.ArtefactVersion.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactVersionPart = (ArtefactVersionPropertiesEditionPart)propertiesEditionPart;
		}
		if (EvidenceViewsRepository.ArtefactPropertyValue.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactPropertyValuePart = (ArtefactPropertyValuePropertiesEditionPart)propertiesEditionPart;
		}
		if (EvidenceViewsRepository.ArtefactEvaluation.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactEvaluationPart = (ArtefactEvaluationPropertiesEditionPart)propertiesEditionPart;
		}
		if (EvidenceViewsRepository.ArtefactEvents.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactEventsPart = (ArtefactEventsPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == EvidenceViewsRepository.Artefact.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == EvidenceViewsRepository.ArtefactVersion.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == EvidenceViewsRepository.ArtefactPropertyValue.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == EvidenceViewsRepository.ArtefactEvaluation.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == EvidenceViewsRepository.ArtefactEvents.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
