package org.eclipse.opencert.evm.evidspec.evidence.util;
/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JOptionPane;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ItemProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.opencert.infra.svnkit.MainClass;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNNodeKind;



public class SVNTreeDialog extends Dialog {
	protected ILabelProvider labelProvider;
	protected IContentProvider contentProvider;	
	protected String displayName;
	protected ItemProvider values;
	protected String result;
	protected MainClass validateInstance;
	
	protected Boolean onlyFolders;
	public Tree choiceTree;
	
	public final String EVDLG_IMGS_DIR_OPEN = "dir_open";
	public final String EVDLG_IMGS_DIR_CLOSED = "dir_closed";
	public final String EVDLG_IMGS_FILE = "file";
//	private ImageRegistry imageRegistry;
	private ImageUtils imageUtility;
    

	  
	public SVNTreeDialog
		(Shell parent, 
		ILabelProvider labelProvider, 
		MainClass validateInstance, 
		boolean onlyFolders, 
		String currentValues, 
		String displayName,
		ImageUtils imageUtil)
	{
		super(parent);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.labelProvider = labelProvider;
		this.validateInstance = validateInstance;
		this.onlyFolders = onlyFolders;
		this.displayName = displayName;
		this.imageUtility = imageUtil;
		
		AdapterFactory adapterFactory = new ComposedAdapterFactory(Collections.<AdapterFactory>emptyList());
		values = new ItemProvider(adapterFactory, currentValues);
		contentProvider = new AdapterFactoryContentProvider(adapterFactory);
	}

	/*public SVNTreeDialog
	    (Shell parent, 
	     ILabelProvider labelProvider, 
	     EObject eObject, 
	     EStructuralFeature eStructuralFeature, 
	     String displayName,
	     ImageUtils imageUtil)
	{
		this(parent,
				labelProvider,
				eObject,
				eStructuralFeature.getEType(),
				(String)eObject.eGet(eStructuralFeature),
				displayName,
				imageUtil);
	}*/

	
	@Override
	protected void configureShell(Shell shell) 
	{
		super.configureShell(shell);
		shell.setText(this.displayName);
			
		//shell.setImage(labelProvider.getImage(object));
	}

	@Override
	protected Control createDialogArea(Composite parent) 
	{
		// creates an image registry and adds icons to the image registry.
//	    imageRegistry = new ImageRegistry();

//	    ImageDescriptor defaultIcon =
//	      ImageDescriptor.createFromFile(getResourceLocator().getClass(), "dir_closed.ico");
//	    imageRegistry.put(EVDLG_IMGS_DIR_CLOSED, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER));
//	    imageRegistry.put(EVDLG_IMGS_DIR_OPEN, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER));
//	    imageRegistry.put(EVDLG_IMGS_FILE, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FILE));
//	    imageRegistry.put("default_file", defaultIcon);

		Composite contents = (Composite)super.createDialogArea(parent);

		GridLayout contentsGridLayout = (GridLayout)contents.getLayout();
		contentsGridLayout.numColumns = 1;

		GridData contentsGridData = (GridData)contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		Composite choiceComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			choiceComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			choiceComposite.setLayout(layout);
		}
		choiceTree = new Tree(choiceComposite, SWT.BORDER|SWT.FULL_SELECTION);
		GridData choiceTreeGridData = new GridData();
		choiceTreeGridData.verticalAlignment = SWT.FILL;
		choiceTreeGridData.horizontalAlignment = SWT.FILL;
		choiceTreeGridData.widthHint = Display.getCurrent().getBounds().width / 5;
		choiceTreeGridData.heightHint = Display.getCurrent().getBounds().height / 5;
		choiceTreeGridData.grabExcessHorizontalSpace = true;
		choiceTreeGridData.grabExcessVerticalSpace = true;
		choiceTree.setLayoutData(choiceTreeGridData);
		
		
		ArrayList<SVNDirEntry> roots = validateInstance.listEntries("", onlyFolders);
		
		for (int i = 0; i < roots.size(); i++) {
			TreeItem root = new TreeItem(choiceTree, 0);			
			SVNDirEntry entry = roots.get(i);
			root.setText(entry.getName());
			if(entry.getKind() == SVNNodeKind.DIR){
				root.setImage(this.imageUtility.getClosedDirImage());
			}
			else{
				this.imageUtility.PutImage(root, entry.getName());
			}
			root.setData(entry.getName());
			new TreeItem(root, 0);
		}
		choiceTree.addListener(SWT.Expand, new Listener()
		{
			public void handleEvent(final Event event)
			{
				final TreeItem root = (TreeItem) event.item;
				TreeItem[] items = root.getItems();
				for (int i = 0; i < items.length; i++)
				{
					if (items[i].getData() != null)
						return;
					items[i].dispose();
				}
				String filePath = (String) root.getData();
				
				//root.setImage(imageUtility.getOpenDirImage());
				//if (((SVNDirEntry) root.getData()).getKind() == SVNNodeKind.DIR) {
					ArrayList<SVNDirEntry> files =validateInstance.listEntries(filePath, onlyFolders);
					if (files == null)
						return;					
				
					for (int i = 0; i < files.size(); i++)
					{
						TreeItem item = new TreeItem(root, 0);
						SVNDirEntry subEntry = files.get(i);							
						item.setText(subEntry.getName());
						if(subEntry.getKind() == SVNNodeKind.DIR){
							item.setImage(imageUtility.getClosedDirImage());
						}
						else
						{
							imageUtility.PutImage(item, subEntry.getName());
						}
						item.setData(filePath + "/" + subEntry.getName());
						if (subEntry.getKind() == SVNNodeKind.DIR)
						{
							new TreeItem(item, 0);
						}						
	
					}
				//}
			}
		});

		/*Composite controlButtons = new Composite(contents, SWT.NONE);
		GridData controlButtonsGridData = new GridData();
		controlButtonsGridData.verticalAlignment = SWT.FILL;
		controlButtonsGridData.horizontalAlignment = SWT.FILL;
		controlButtons.setLayoutData(controlButtonsGridData);
		
		GridLayout controlsButtonGridLayout = new GridLayout();
		controlButtons.setLayout(controlsButtonGridLayout);
		
		new Label(controlButtons, SWT.NONE);

		final Button addEvidenceButton = new Button(controlButtons, SWT.PUSH);
		addEvidenceButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Add_label"));
		GridData addButtonGridData = new GridData();
		addButtonGridData.verticalAlignment = SWT.FILL;
		addButtonGridData.horizontalAlignment = SWT.FILL;
		addEvidenceButton.setLayoutData(addButtonGridData);
		
		final Button removeEvidenceButton = new Button(controlButtons, SWT.PUSH);
		removeEvidenceButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Remove_label"));
		GridData removeButtonGridData = new GridData();
		removeButtonGridData.verticalAlignment = SWT.FILL;
		removeButtonGridData.horizontalAlignment = SWT.FILL;
		removeEvidenceButton.setLayoutData(removeButtonGridData);
		
		Label spaceLabel = new Label(controlButtons, SWT.NONE);
		GridData spaceLabelGridData = new GridData();
		spaceLabelGridData.verticalSpan = 2;
		spaceLabel.setLayoutData(spaceLabelGridData);
		
		final Button upEvidenceButton = new Button(controlButtons, SWT.PUSH);
		upEvidenceButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Up_label"));
		GridData upButtonGridData = new GridData();
		upButtonGridData.verticalAlignment = SWT.FILL;
		upButtonGridData.horizontalAlignment = SWT.FILL;
		upEvidenceButton.setLayoutData(upButtonGridData);
		
		final Button downEvidenceButton = new Button(controlButtons, SWT.PUSH);
		downEvidenceButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Down_label"));
		GridData downButtonGridData = new GridData();
		downButtonGridData.verticalAlignment = SWT.FILL;
		downButtonGridData.horizontalAlignment = SWT.FILL;
		downEvidenceButton.setLayoutData(downButtonGridData);
	
		Composite featureComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			featureComposite.setLayoutData(data);
	
			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			featureComposite.setLayout(layout);
		}
	
		Label featureLabel = new Label(featureComposite, SWT.NONE);
		featureLabel.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Feature_label"));
		GridData featureLabelGridData = new GridData();
		featureLabelGridData.horizontalSpan = 2;
		featureLabelGridData.horizontalAlignment = SWT.FILL;
		featureLabelGridData.verticalAlignment = SWT.FILL;
		featureLabel.setLayoutData(featureLabelGridData);
		
		final Table featureTable = new Table(featureComposite, SWT.MULTI | SWT.BORDER);
		GridData featureTableGridData = new GridData();
		featureTableGridData.widthHint = Display.getCurrent().getBounds().width / 5;
		featureTableGridData.heightHint = Display.getCurrent().getBounds().height / 3;
		featureTableGridData.verticalAlignment = SWT.FILL;
		featureTableGridData.horizontalAlignment = SWT.FILL;
		featureTableGridData.grabExcessHorizontalSpace= true;
		featureTableGridData.grabExcessVerticalSpace= true;
		featureTable.setLayoutData(featureTableGridData);
		
		final TableViewer featureTableViewer = new TableViewer(featureTable);
		featureTableViewer.setContentProvider(contentProvider);
		featureTableViewer.setLabelProvider(labelProvider);
		featureTableViewer.setInput(values);
		if (!values.getChildren().isEmpty())
		{
			featureTableViewer.setSelection(new StructuredSelection(values.getChildren().get(0)));
		}*/
	
    
/*		upEvidenceButton.addSelectionListener(
				new SelectionAdapter()
				{
					@Override
					public void widgetSelected(SelectionEvent event)
					{
						IStructuredSelection selection = (IStructuredSelection)featureTableViewer.getSelection();
						int minIndex = 0;
						for (Iterator<?> i = selection.iterator(); i.hasNext();)
						{
							Object value = i.next();
							int index = values.getChildren().indexOf(value);
							values.getChildren().move(Math.max(index - 1, minIndex++), value);
						}
					}
				});

		downEvidenceButton.addSelectionListener(
				new SelectionAdapter()
				{
					@Override
					public void widgetSelected(SelectionEvent event)
					{
						IStructuredSelection selection = (IStructuredSelection)featureTableViewer.getSelection();
						int maxIndex = values.getChildren().size() - selection.size();
						for (Iterator<?> i = selection.iterator(); i.hasNext();)
						{
							Object value = i.next();
							int index = values.getChildren().indexOf(value);
							values.getChildren().move(Math.min(index + 1, maxIndex++), value);
						}
					}
				});

		addEvidenceButton.addSelectionListener(
				new SelectionAdapter()
				{
					// event is null when choiceTableViewer is double clicked
					@Override
					public void widgetSelected(SelectionEvent event)
					{
						if (choiceTree != null)
						{
							try
							{
								TreeItem[] selection = choiceTree.getSelection();
								TreeItem item = selection[0];
								Object value = item.getData().toString();
								values.getChildren().add(value);
								featureTableViewer.setSelection(new StructuredSelection(value));
							}
							catch (RuntimeException exception)
							{
								// Ignore
							}
						}
					}
				});*/

		/*removeEvidenceButton.addSelectionListener(
				new SelectionAdapter()
				{
					// event is null when featureTableViewer is double clicked 
					@Override
					public void widgetSelected(SelectionEvent event)
					{
						IStructuredSelection selection = (IStructuredSelection)featureTableViewer.getSelection();
						Object firstValue = null;
						for (Iterator<?> i = selection.iterator(); i.hasNext();)
						{
							Object value = i.next();
							if (firstValue == null)
							{
								firstValue = value;
							}
							values.getChildren().remove(value);
						}
	
						if (!values.getChildren().isEmpty())
						{
							featureTableViewer.setSelection(new StructuredSelection(values.getChildren().get(0)));
						}
					}
				});   */ 
        
		return contents;
	}

//	private Image[] overlayImage(Object object2, Object image) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	protected void okPressed()
	{
		String value = "";
		
		if (choiceTree != null)
		{
			try
			{
				TreeItem[] selection = choiceTree.getSelection();
				TreeItem item = selection[0];
				value = item.getData().toString();
				
				//Check a file is selected
				if(!onlyFolders){
					if(!validateInstance.existsFileRepo(value)){
						JOptionPane.showMessageDialog(null,
								"A file must be selected",
								"Error",
								JOptionPane.INFORMATION_MESSAGE); 
						return;
					}
				}
				
				values.getChildren().add(value);
			}
			catch (RuntimeException exception)
			{
				// Ignore
			}
		}
		
		result = value;
		super.okPressed();
	}
	@Override
	public boolean close()
	{
		contentProvider.dispose();
		return super.close();
	}

	public String getResult()
	{
		return result;
	}
//	/**
	/*public ResourceLocator getResourceLocator() {
		return DprojectEditPlugin.INSTANCE;
	}*/
//	private void PutImage (TreeItem item, File root){
//		Image itemImage = null;
//		Program program = null;
//		if (root.isDirectory())
//			itemImage = imageRegistry.get(EVDLG_IMGS_DIR_CLOSED);
//		else {
//    		int	dot	=	root.getName().lastIndexOf('.');
//    		if (dot	!= -1) {
//    			String extension = root.getName().substring(dot);
//    			if (imageRegistry.get(extension)== null){
//    				program	=	Program.findProgram(extension);
//        			if (program	!= null) {
//        				ImageData	imageData	=	program.getImageData();
//        				if (imageData	!= null) {
//        					itemImage	=	new	Image(null,	imageData, imageData.getTransparencyMask());
//        				    imageRegistry.put(extension, itemImage);
//        				}
//        			}
//        			else{
//        				itemImage = imageRegistry.get(EVDLG_IMGS_FILE);
//        			}
//    			}else{
//    				itemImage = imageRegistry.get(extension);
//    			}
//    		}
//		}
//		if (itemImage == null) {
//			itemImage = imageRegistry.get(EVDLG_IMGS_FILE);
//		}
//    	item.setImage(itemImage);
//	}

}