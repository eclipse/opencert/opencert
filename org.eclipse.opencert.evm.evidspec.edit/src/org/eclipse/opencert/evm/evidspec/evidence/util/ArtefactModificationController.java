/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.evidence.util;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

public class ArtefactModificationController {
	private static ArtefactModificationController instance;
	
	private static EList<Artefact> modifiedArtefact;
	private static EList<Artefact> evaluatedArtefact;
	
	private ArtefactModificationController(){};
	
	
	public static ArtefactModificationController getInstance(){
		if(instance==null){
			instance= new ArtefactModificationController();
			 modifiedArtefact = new BasicEList<Artefact>();
			 evaluatedArtefact= new BasicEList<Artefact>();
		}
		return instance;
	}
	
	public EList<Artefact> getModifiedArtefacts(){
		return modifiedArtefact;
	}
	
	public EList<Artefact> getEvaluatedArtefacts(){
		return evaluatedArtefact;
	}
	
	public static Artefact getRootArtefact(Artefact oneArtefact){
		EObject  parent = oneArtefact;		
		while(parent.eContainer() instanceof Artefact){
			parent=parent.eContainer();
		}
		return (Artefact)parent;						
		
	}
	
	
	
}
