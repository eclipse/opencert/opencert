/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory
 * @model kind="package"
 * @generated
 */
public interface EvidencePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "evidence";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://evidence/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "evidence";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EvidencePackage eINSTANCE = org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl <em>Artefact Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefactModel()
	 * @generated
	 */
	int ARTEFACT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Artefact</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__ARTEFACT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Repo Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__REPO_URL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Repo User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__REPO_USER = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Repo Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__REPO_PASSWORD = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Repo Local Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__REPO_LOCAL_PATH = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Repo Uses Local</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL__REPO_USES_LOCAL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Artefact Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Artefact Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_MODEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl <em>Artefact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefact()
	 * @generated
	 */
	int ARTEFACT = 1;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__EVALUATION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__LIFECYCLE_EVENT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__ID = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__NAME = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__DESCRIPTION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Property Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__PROPERTY_VALUE = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Artefact Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__ARTEFACT_PART = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__OWNED_REL = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__VERSION_ID = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__DATE = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Changes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__CHANGES = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Is Last Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__IS_LAST_VERSION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__RESOURCE = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Precedent Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__PRECEDENT_VERSION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Is Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__IS_TEMPLATE = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Is Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__IS_CONFIGURABLE = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_FEATURE_COUNT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 14;

	/**
	 * The number of operations of the '<em>Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_OPERATION_COUNT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ValueImpl <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ValueImpl
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Property Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__PROPERTY_REFERENCE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__NAME = 2;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ResourceImpl
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__LOCATION = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__FORMAT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl <em>Artefact Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefactRel()
	 * @generated
	 */
	int ARTEFACT_REL = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Modification Effect</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL__MODIFICATION_EFFECT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Revocation Effect</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL__REVOCATION_EFFECT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL__SOURCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL__TARGET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Artefact Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Artefact Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_REL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl <em>Artefact Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl
	 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefactDefinition()
	 * @generated
	 */
	int ARTEFACT_DEFINITION = 5;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION__EVALUATION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION__LIFECYCLE_EVENT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION__ID = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION__NAME = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION__DESCRIPTION = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Artefact</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION__ARTEFACT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Artefact Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION_FEATURE_COUNT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Artefact Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_DEFINITION_OPERATION_COUNT = AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel <em>Artefact Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artefact Model</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel
	 * @generated
	 */
	EClass getArtefactModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getArtefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Artefact</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getArtefact()
	 * @see #getArtefactModel()
	 * @generated
	 */
	EReference getArtefactModel_Artefact();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUrl <em>Repo Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repo Url</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUrl()
	 * @see #getArtefactModel()
	 * @generated
	 */
	EAttribute getArtefactModel_RepoUrl();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUser <em>Repo User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repo User</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUser()
	 * @see #getArtefactModel()
	 * @generated
	 */
	EAttribute getArtefactModel_RepoUser();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoPassword <em>Repo Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repo Password</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoPassword()
	 * @see #getArtefactModel()
	 * @generated
	 */
	EAttribute getArtefactModel_RepoPassword();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoLocalPath <em>Repo Local Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repo Local Path</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoLocalPath()
	 * @see #getArtefactModel()
	 * @generated
	 */
	EAttribute getArtefactModel_RepoLocalPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#isRepoUsesLocal <em>Repo Uses Local</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repo Uses Local</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#isRepoUsesLocal()
	 * @see #getArtefactModel()
	 * @generated
	 */
	EAttribute getArtefactModel_RepoUsesLocal();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artefact</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact
	 * @generated
	 */
	EClass getArtefact();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getPropertyValue <em>Property Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Value</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getPropertyValue()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_PropertyValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getArtefactPart <em>Artefact Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Artefact Part</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getArtefactPart()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_ArtefactPart();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getOwnedRel()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_OwnedRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getVersionID <em>Version ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version ID</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getVersionID()
	 * @see #getArtefact()
	 * @generated
	 */
	EAttribute getArtefact_VersionID();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getDate()
	 * @see #getArtefact()
	 * @generated
	 */
	EAttribute getArtefact_Date();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getChanges <em>Changes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Changes</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getChanges()
	 * @see #getArtefact()
	 * @generated
	 */
	EAttribute getArtefact_Changes();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsLastVersion <em>Is Last Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Last Version</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsLastVersion()
	 * @see #getArtefact()
	 * @generated
	 */
	EAttribute getArtefact_IsLastVersion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resource</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getResource()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_Resource();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getPrecedentVersion <em>Precedent Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Precedent Version</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#getPrecedentVersion()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_PrecedentVersion();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsTemplate <em>Is Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Template</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsTemplate()
	 * @see #getArtefact()
	 * @generated
	 */
	EAttribute getArtefact_IsTemplate();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsConfigurable <em>Is Configurable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Configurable</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsConfigurable()
	 * @see #getArtefact()
	 * @generated
	 */
	EAttribute getArtefact_IsConfigurable();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.evm.evidspec.evidence.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Value#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Value#getValue()
	 * @see #getValue()
	 * @generated
	 */
	EAttribute getValue_Value();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.evm.evidspec.evidence.Value#getPropertyReference <em>Property Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property Reference</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Value#getPropertyReference()
	 * @see #getValue()
	 * @generated
	 */
	EReference getValue_PropertyReference();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Value#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Value#getName()
	 * @see #getValue()
	 * @generated
	 */
	EAttribute getValue_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.evm.evidspec.evidence.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Resource#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Resource#getLocation()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_Location();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.Resource#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Format</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.Resource#getFormat()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_Format();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel <em>Artefact Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artefact Rel</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel
	 * @generated
	 */
	EClass getArtefactRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getModificationEffect <em>Modification Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Modification Effect</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getModificationEffect()
	 * @see #getArtefactRel()
	 * @generated
	 */
	EAttribute getArtefactRel_ModificationEffect();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getRevocationEffect <em>Revocation Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Revocation Effect</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getRevocationEffect()
	 * @see #getArtefactRel()
	 * @generated
	 */
	EAttribute getArtefactRel_RevocationEffect();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getSource()
	 * @see #getArtefactRel()
	 * @generated
	 */
	EReference getArtefactRel_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getTarget()
	 * @see #getArtefactRel()
	 * @generated
	 */
	EReference getArtefactRel_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition <em>Artefact Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artefact Definition</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition
	 * @generated
	 */
	EClass getArtefactDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition#getArtefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Artefact</em>'.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition#getArtefact()
	 * @see #getArtefactDefinition()
	 * @generated
	 */
	EReference getArtefactDefinition_Artefact();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EvidenceFactory getEvidenceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl <em>Artefact Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefactModel()
		 * @generated
		 */
		EClass ARTEFACT_MODEL = eINSTANCE.getArtefactModel();

		/**
		 * The meta object literal for the '<em><b>Artefact</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT_MODEL__ARTEFACT = eINSTANCE.getArtefactModel_Artefact();

		/**
		 * The meta object literal for the '<em><b>Repo Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT_MODEL__REPO_URL = eINSTANCE.getArtefactModel_RepoUrl();

		/**
		 * The meta object literal for the '<em><b>Repo User</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT_MODEL__REPO_USER = eINSTANCE.getArtefactModel_RepoUser();

		/**
		 * The meta object literal for the '<em><b>Repo Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT_MODEL__REPO_PASSWORD = eINSTANCE.getArtefactModel_RepoPassword();

		/**
		 * The meta object literal for the '<em><b>Repo Local Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT_MODEL__REPO_LOCAL_PATH = eINSTANCE.getArtefactModel_RepoLocalPath();

		/**
		 * The meta object literal for the '<em><b>Repo Uses Local</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT_MODEL__REPO_USES_LOCAL = eINSTANCE.getArtefactModel_RepoUsesLocal();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl <em>Artefact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefact()
		 * @generated
		 */
		EClass ARTEFACT = eINSTANCE.getArtefact();

		/**
		 * The meta object literal for the '<em><b>Property Value</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__PROPERTY_VALUE = eINSTANCE.getArtefact_PropertyValue();

		/**
		 * The meta object literal for the '<em><b>Artefact Part</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__ARTEFACT_PART = eINSTANCE.getArtefact_ArtefactPart();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__OWNED_REL = eINSTANCE.getArtefact_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Version ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT__VERSION_ID = eINSTANCE.getArtefact_VersionID();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT__DATE = eINSTANCE.getArtefact_Date();

		/**
		 * The meta object literal for the '<em><b>Changes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT__CHANGES = eINSTANCE.getArtefact_Changes();

		/**
		 * The meta object literal for the '<em><b>Is Last Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT__IS_LAST_VERSION = eINSTANCE.getArtefact_IsLastVersion();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__RESOURCE = eINSTANCE.getArtefact_Resource();

		/**
		 * The meta object literal for the '<em><b>Precedent Version</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__PRECEDENT_VERSION = eINSTANCE.getArtefact_PrecedentVersion();

		/**
		 * The meta object literal for the '<em><b>Is Template</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT__IS_TEMPLATE = eINSTANCE.getArtefact_IsTemplate();

		/**
		 * The meta object literal for the '<em><b>Is Configurable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT__IS_CONFIGURABLE = eINSTANCE.getArtefact_IsConfigurable();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ValueImpl <em>Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ValueImpl
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getValue()
		 * @generated
		 */
		EClass VALUE = eINSTANCE.getValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE__VALUE = eINSTANCE.getValue_Value();

		/**
		 * The meta object literal for the '<em><b>Property Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE__PROPERTY_REFERENCE = eINSTANCE.getValue_PropertyReference();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE__NAME = eINSTANCE.getValue_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ResourceImpl
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__LOCATION = eINSTANCE.getResource_Location();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__FORMAT = eINSTANCE.getResource_Format();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl <em>Artefact Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactRelImpl
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefactRel()
		 * @generated
		 */
		EClass ARTEFACT_REL = eINSTANCE.getArtefactRel();

		/**
		 * The meta object literal for the '<em><b>Modification Effect</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT_REL__MODIFICATION_EFFECT = eINSTANCE.getArtefactRel_ModificationEffect();

		/**
		 * The meta object literal for the '<em><b>Revocation Effect</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTEFACT_REL__REVOCATION_EFFECT = eINSTANCE.getArtefactRel_RevocationEffect();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT_REL__SOURCE = eINSTANCE.getArtefactRel_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT_REL__TARGET = eINSTANCE.getArtefactRel_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl <em>Artefact Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl
		 * @see org.eclipse.opencert.evm.evidspec.evidence.impl.EvidencePackageImpl#getArtefactDefinition()
		 * @generated
		 */
		EClass ARTEFACT_DEFINITION = eINSTANCE.getArtefactDefinition();

		/**
		 * The meta object literal for the '<em><b>Artefact</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT_DEFINITION__ARTEFACT = eINSTANCE.getArtefactDefinition_Artefact();

	}

} //EvidencePackage
