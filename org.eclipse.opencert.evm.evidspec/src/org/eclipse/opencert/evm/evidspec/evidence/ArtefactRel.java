/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence;

import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artefact Rel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getModificationEffect <em>Modification Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getRevocationEffect <em>Revocation Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactRel()
 * @model
 * @generated
 */
public interface ArtefactRel extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Modification Effect</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.infra.general.general.ChangeEffectKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modification Effect</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modification Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #setModificationEffect(ChangeEffectKind)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactRel_ModificationEffect()
	 * @model
	 * @generated
	 */
	ChangeEffectKind getModificationEffect();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getModificationEffect <em>Modification Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Modification Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #getModificationEffect()
	 * @generated
	 */
	void setModificationEffect(ChangeEffectKind value);

	/**
	 * Returns the value of the '<em><b>Revocation Effect</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.infra.general.general.ChangeEffectKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Revocation Effect</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Revocation Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #setRevocationEffect(ChangeEffectKind)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactRel_RevocationEffect()
	 * @model
	 * @generated
	 */
	ChangeEffectKind getRevocationEffect();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getRevocationEffect <em>Revocation Effect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Revocation Effect</em>' attribute.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see #getRevocationEffect()
	 * @generated
	 */
	void setRevocationEffect(ChangeEffectKind value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Artefact)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactRel_Source()
	 * @model required="true"
	 * @generated
	 */
	Artefact getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Artefact value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Artefact)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactRel_Target()
	 * @model required="true"
	 * @generated
	 */
	Artefact getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Artefact value);

} // ArtefactRel
