/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artefact Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getArtefact <em>Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUrl <em>Repo Url</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUser <em>Repo User</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoPassword <em>Repo Password</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoLocalPath <em>Repo Local Path</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#isRepoUsesLocal <em>Repo Uses Local</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactModel()
 * @model
 * @generated
 */
public interface ArtefactModel extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Artefact</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Artefact</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artefact</em>' containment reference list.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactModel_Artefact()
	 * @model containment="true"
	 * @generated
	 */
	EList<ArtefactDefinition> getArtefact();

	/**
	 * Returns the value of the '<em><b>Repo Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repo Url</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repo Url</em>' attribute.
	 * @see #setRepoUrl(String)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactModel_RepoUrl()
	 * @model
	 * @generated
	 */
	String getRepoUrl();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUrl <em>Repo Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repo Url</em>' attribute.
	 * @see #getRepoUrl()
	 * @generated
	 */
	void setRepoUrl(String value);

	/**
	 * Returns the value of the '<em><b>Repo User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repo User</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repo User</em>' attribute.
	 * @see #setRepoUser(String)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactModel_RepoUser()
	 * @model
	 * @generated
	 */
	String getRepoUser();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoUser <em>Repo User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repo User</em>' attribute.
	 * @see #getRepoUser()
	 * @generated
	 */
	void setRepoUser(String value);

	/**
	 * Returns the value of the '<em><b>Repo Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repo Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repo Password</em>' attribute.
	 * @see #setRepoPassword(String)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactModel_RepoPassword()
	 * @model
	 * @generated
	 */
	String getRepoPassword();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoPassword <em>Repo Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repo Password</em>' attribute.
	 * @see #getRepoPassword()
	 * @generated
	 */
	void setRepoPassword(String value);

	/**
	 * Returns the value of the '<em><b>Repo Local Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repo Local Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repo Local Path</em>' attribute.
	 * @see #setRepoLocalPath(String)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactModel_RepoLocalPath()
	 * @model
	 * @generated
	 */
	String getRepoLocalPath();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#getRepoLocalPath <em>Repo Local Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repo Local Path</em>' attribute.
	 * @see #getRepoLocalPath()
	 * @generated
	 */
	void setRepoLocalPath(String value);

	/**
	 * Returns the value of the '<em><b>Repo Uses Local</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repo Uses Local</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repo Uses Local</em>' attribute.
	 * @see #setRepoUsesLocal(boolean)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefactModel_RepoUsesLocal()
	 * @model
	 * @generated
	 */
	boolean isRepoUsesLocal();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel#isRepoUsesLocal <em>Repo Uses Local</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repo Uses Local</em>' attribute.
	 * @see #isRepoUsesLocal()
	 * @generated
	 */
	void setRepoUsesLocal(boolean value);

} // ArtefactModel
