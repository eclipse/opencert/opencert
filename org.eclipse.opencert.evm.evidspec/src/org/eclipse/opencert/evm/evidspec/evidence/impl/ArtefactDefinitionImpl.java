/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.ManageableAssuranceAssetImpl;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artefact Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl#getArtefact <em>Artefact</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArtefactDefinitionImpl extends ManageableAssuranceAssetImpl implements ArtefactDefinition {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtefactDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EvidencePackage.Literals.ARTEFACT_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT_DEFINITION__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eDynamicSet(EvidencePackage.ARTEFACT_DEFINITION__ID, GeneralPackage.Literals.NAMED_ELEMENT__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT_DEFINITION__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eDynamicSet(EvidencePackage.ARTEFACT_DEFINITION__NAME, GeneralPackage.Literals.NAMED_ELEMENT__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return (String)eDynamicGet(EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		eDynamicSet(EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION, GeneralPackage.Literals.DESCRIBABLE_ELEMENT__DESCRIPTION, newDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Artefact> getArtefact() {
		return (EList<Artefact>)eDynamicGet(EvidencePackage.ARTEFACT_DEFINITION__ARTEFACT, EvidencePackage.Literals.ARTEFACT_DEFINITION__ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_DEFINITION__ARTEFACT:
				return ((InternalEList<?>)getArtefact()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_DEFINITION__ID:
				return getId();
			case EvidencePackage.ARTEFACT_DEFINITION__NAME:
				return getName();
			case EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION:
				return getDescription();
			case EvidencePackage.ARTEFACT_DEFINITION__ARTEFACT:
				return getArtefact();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_DEFINITION__ID:
				setId((String)newValue);
				return;
			case EvidencePackage.ARTEFACT_DEFINITION__NAME:
				setName((String)newValue);
				return;
			case EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case EvidencePackage.ARTEFACT_DEFINITION__ARTEFACT:
				getArtefact().clear();
				getArtefact().addAll((Collection<? extends Artefact>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_DEFINITION__ID:
				setId(ID_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_DEFINITION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case EvidencePackage.ARTEFACT_DEFINITION__ARTEFACT:
				getArtefact().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EvidencePackage.ARTEFACT_DEFINITION__ID:
				return ID_EDEFAULT == null ? getId() != null : !ID_EDEFAULT.equals(getId());
			case EvidencePackage.ARTEFACT_DEFINITION__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
			case EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? getDescription() != null : !DESCRIPTION_EDEFAULT.equals(getDescription());
			case EvidencePackage.ARTEFACT_DEFINITION__ARTEFACT:
				return !getArtefact().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case EvidencePackage.ARTEFACT_DEFINITION__ID: return GeneralPackage.NAMED_ELEMENT__ID;
				case EvidencePackage.ARTEFACT_DEFINITION__NAME: return GeneralPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (derivedFeatureID) {
				case EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION: return GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.NAMED_ELEMENT__ID: return EvidencePackage.ARTEFACT_DEFINITION__ID;
				case GeneralPackage.NAMED_ELEMENT__NAME: return EvidencePackage.ARTEFACT_DEFINITION__NAME;
				default: return -1;
			}
		}
		if (baseClass == DescribableElement.class) {
			switch (baseFeatureID) {
				case GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION: return EvidencePackage.ARTEFACT_DEFINITION__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ArtefactDefinitionImpl
