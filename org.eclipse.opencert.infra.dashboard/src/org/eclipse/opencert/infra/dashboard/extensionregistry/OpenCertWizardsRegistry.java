/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.extensionregistry;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.opencert.infra.dashboard.DashboardActivator;
import org.eclipse.ui.INewWizard;

/**
 * Registry for new wizard extension point.
 */
public final class OpenCertWizardsRegistry {

	/** Registry Singleton. */
	private static final OpenCertWizardsRegistry SINGLETON_REGISTRY = new OpenCertWizardsRegistry();

	/** ID for new wizard extension point. */
	private static final String NEW_WIZARD_EXTENSION_POINT_ID = "org.eclipse.ui.newWizards"; //$NON-NLS-1$

	/** Name of wizard class element. */
	private static final String WIZARD_CLASS_ELEMENT_NAME = "class"; //$NON-NLS-1$

	/** Name of wizard id element. */
	private static final String WIZARD_ID_ELEMENT_NAME = "id"; //$NON-NLS-1$

	/** The dawn refframework new wizard. */
	private INewWizard mDawnRefframeworkNewWizard = null;

	/** The method library new wizard. */
	private INewWizard mMethodLibraryNewWizard = null;

	/** The assurance project new wizard. */
	private INewWizard mAssuranceProjectNewWizard = null;

	/** The CHESS project new wizard. */
	private INewWizard mChessProjectNewWizard = null;

	/**
	 * Does not allow to instantiate this class.
	 */
	private OpenCertWizardsRegistry() {
		initialiseNewWizards();
	}

	/**
	 * @return Registry instance singleton
	 */
	public static OpenCertWizardsRegistry getInstance() {
		return SINGLETON_REGISTRY;
	}

	/**
	 * @return The registered dawn refframework new wizard or <code>null</code> if not found
	 */
	public INewWizard getDawnRefframeworkNewWizard() {
		return mDawnRefframeworkNewWizard;
	}

	/**
	 * @return The registered method library new wizard or <code>null</code> if not found
	 */
	public INewWizard getMethodLibraryNewWizard() {
		return mMethodLibraryNewWizard;
	}

	/**
	 * @return The registered assurance project new wizard or <code>null</code> if not found
	 */
	public INewWizard getAssuranceProjectNewWizard() {
		return mAssuranceProjectNewWizard;
	}

	/**
	 * @return The registered Chess project new wizard or <code>null</code> if not found
	 */
	public INewWizard getChessProjectNewWizard() {
		return mChessProjectNewWizard;
	}

	/**
	 * Initialise all registered new wizards.
	 */
	private void initialiseNewWizards() {

		// Get registry
		IExtensionRegistry vExtensionRegistry = Platform.getExtensionRegistry();

		// Get extension point with the newWizards ID
		for (IConfigurationElement vConfigElement : vExtensionRegistry.getExtensionPoint(NEW_WIZARD_EXTENSION_POINT_ID)
				.getConfigurationElements()) {

			String vWizardIdAttribute = vConfigElement.getAttribute(WIZARD_ID_ELEMENT_NAME);

			// Flag to check if the configuration corresponds to the relevant new wizard
			boolean vIsRefframeWorkNewWizard = vWizardIdAttribute
					.matches(OpenCertNewWizardIDs.DAWN_REFFRAMEWORK_NEW_WIZARD_ID);
			boolean vIsMethodLibNewWizard = vWizardIdAttribute
					.matches(OpenCertNewWizardIDs.METHOD_LIBRARY_NEW_WIZARD_ID);
			boolean vIsAssuranceProjectNewWizard = vWizardIdAttribute
					.matches(OpenCertNewWizardIDs.ASSURANCE_PROJECT_NEW_WIZARD_ID);
			boolean vIsChessProjectNewWizard = vWizardIdAttribute
					.matches(OpenCertNewWizardIDs.CHESS_PROJECT_NEW_WIZARD_ID);

			boolean vToCreateWizard = vIsRefframeWorkNewWizard || vIsMethodLibNewWizard || vIsAssuranceProjectNewWizard
					|| vIsChessProjectNewWizard;

			if (vToCreateWizard) {
				try {
					// Instantiate it
					INewWizard vNewWizard = (INewWizard) vConfigElement
							.createExecutableExtension(WIZARD_CLASS_ELEMENT_NAME);

					// Remember it
					if (vIsRefframeWorkNewWizard) {
						mDawnRefframeworkNewWizard = vNewWizard;
					} else if (vIsMethodLibNewWizard) {
						mMethodLibraryNewWizard = vNewWizard;
					} else if (vIsAssuranceProjectNewWizard) {
						mAssuranceProjectNewWizard = vNewWizard;
					} else if (vIsChessProjectNewWizard) {
						mChessProjectNewWizard = vNewWizard;
					}
				} catch (final CoreException pException) {
					DashboardActivator.getDefault().getLog().log(pException.getStatus());
				}
			}
		}
	}
}
