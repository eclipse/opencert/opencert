/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.actions;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.navigator.CommonActionProvider;
import org.eclipse.ui.navigator.ICommonActionExtensionSite;
import org.eclipse.ui.navigator.ICommonMenuConstants;

/**
 * Provider that declares actions for the Project Explorer.
 */
public class DashboardActionProvider extends CommonActionProvider {

	/** Action used to open Dashboard. */
	private OpenDashboardAction mOpenDashboardAction = null;

	/** Action used to go to Dashboard. */
	private GoToDashboardAction mGoToDashboardAction = null;

	/**
	 * Default constructor.
	 */
	public DashboardActionProvider() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		ISelectionProvider vSelectionProvider = getActionSite().getViewSite().getSelectionProvider();

		if (mOpenDashboardAction != null) {
			vSelectionProvider.removeSelectionChangedListener(mOpenDashboardAction);
			mOpenDashboardAction = null;
		}

		if (mGoToDashboardAction != null) {
			vSelectionProvider.removeSelectionChangedListener(mGoToDashboardAction);
			mGoToDashboardAction = null;
		}

		super.dispose();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fillActionBars(final IActionBars pActionBars) {
		// Nothing to do
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fillContextMenu(final IMenuManager pMenu) {
		updateActionBars();

		if (mOpenDashboardAction.canAddedToMenu()) {
			pMenu.insertBefore(ICommonMenuConstants.GROUP_NEW, mOpenDashboardAction);
		}

		if (mGoToDashboardAction.canAddedToMenu()) {
			pMenu.insertBefore(ICommonMenuConstants.GROUP_NEW, mGoToDashboardAction);
		}

		pMenu.insertBefore(ICommonMenuConstants.GROUP_NEW, new Separator());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(final ICommonActionExtensionSite pSite) {
		super.init(pSite);

		ISelectionProvider vSelectionProvider = pSite.getViewSite().getSelectionProvider();
		ISelection vSelection = vSelectionProvider.getSelection();

		mOpenDashboardAction = new OpenDashboardAction();
		vSelectionProvider.addSelectionChangedListener(mOpenDashboardAction);

		if (!vSelection.isEmpty()) {
			mOpenDashboardAction.selectionChanged(new SelectionChangedEvent(vSelectionProvider, vSelection));
		}

		mGoToDashboardAction = new GoToDashboardAction();
		if (!vSelection.isEmpty()) {
			mGoToDashboardAction.selectionChanged(new SelectionChangedEvent(vSelectionProvider, vSelection));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateActionBars() {
		IStructuredSelection vSelection = (IStructuredSelection) getContext().getSelection();
		mOpenDashboardAction.selectionChanged(vSelection);
		mGoToDashboardAction.selectionChanged(vSelection);
	}
}
