/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.helpers;

/**
 * Utility class that contains all the file extensions for OpenCert.
 */
public final class OpenCertExtensions {

	/** CHESS UML file extension. */
	public static final String UML_EXTENSION = "uml";

	/** CHESS Notation file extension. */
	public static final String NOTATION_EXTENSION = "notation";

	/** CHESS di file extension. */
	public static final String DI_EXTENSION = "di";

	/** Baseline file extension. */
	public static final String BASELINE_EXTENSION = "baseline";

	/** Baseline diagram file extension. */
	public static final String BASELINE_DIAGRAM_EXTENSION = "baseline_diagram";

	/** Assurance project file extension. */
	public static final String ASSURANCE_PROJECT_EXTENSION = "assuranceproject";

	/** Mapping file extension. */
	public static final String MAPPING_EXTENSION = "mapping";

	/** Ref framework file extension. */
	public static final String REFFRAMEWORK_EXTENSION = "refframework";

	/** Ref framework diagram file extension. */
	public static final String REFFRAMEWORK_DIAGRAM_EXTENSION = "refframework_diagram";

	/** Vocabulary file extension. */
	public static final String VOCABULARY_EXTENSION = "vocabulary";

	/** Array of extensions that must be searched in the project. */
	public static final String[] ALL_EXTENSIONS_ARRAY = { UML_EXTENSION, NOTATION_EXTENSION,
			DI_EXTENSION, BASELINE_EXTENSION, BASELINE_DIAGRAM_EXTENSION,
			ASSURANCE_PROJECT_EXTENSION, MAPPING_EXTENSION, REFFRAMEWORK_EXTENSION, REFFRAMEWORK_DIAGRAM_EXTENSION,
			VOCABULARY_EXTENSION };

	/** Array of graphical extensions that must be searched in the project. */
	public static final String[] GRAPHICAL_EXTENSIONS_ARRAY = { DI_EXTENSION, BASELINE_DIAGRAM_EXTENSION,
			REFFRAMEWORK_DIAGRAM_EXTENSION };

	/**
	 * Private constructor to avoid instantiation.
	 */
	private OpenCertExtensions() {
		super();
	}
}
