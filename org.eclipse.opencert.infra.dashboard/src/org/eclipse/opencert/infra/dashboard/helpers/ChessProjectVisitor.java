/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.ECollections;

/**
 * Implemented resource visitor to get files for a Chess project.
 */
public class ChessProjectVisitor implements IResourceVisitor {

	/** UML files found in Chess project. */
	private List<IFile> mUmlFiles = new ArrayList<>();

	/** Graphical files. */
	private List<IFile> mGraphicalFiles = new ArrayList<>();

	/** Notation files. */
	private List<IFile> mNotationFiles = new ArrayList<>();

	/**
	 * <code>true</code> if only one file of each type is searched,
	 * <code>false</code> otherwise.
	 */
	private boolean mSingleFileSearch = false;

	/**
	 * Constructor for a visitor only searching one file of each type.
	 */
	public ChessProjectVisitor() {
		this(true);
	}

	/**
	 * Default constructor.
	 *
	 * @param pSingleFile
	 *            <code>true</code> if only one file of each type is searched,
	 *            <code>false</code> otherwise
	 */
	public ChessProjectVisitor(final boolean pSingleFile) {
		mSingleFileSearch = pSingleFile;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean visit(final IResource pResource) throws CoreException {
		boolean vContinue = checkContinue();

		if (vContinue && pResource instanceof IFile) {
			IFile file = (IFile) pResource;

			switch (file.getFileExtension()) {
			case OpenCertExtensions.UML_EXTENSION:
				mUmlFiles.add(file);
				break;

			case OpenCertExtensions.NOTATION_EXTENSION:
				mNotationFiles.add(file);
				break;

			case OpenCertExtensions.DI_EXTENSION:
				mGraphicalFiles.add(file);
				break;

			default:
				break;
			}

			vContinue = checkContinue();
		}

		return vContinue;
	}

	/**
	 * Check condition for continuing visit.
	 *
	 * @return <code>false</code> if all files are found, <code>true</code>
	 *         otherwise
	 */
	private boolean checkContinue() {
		boolean vContinue = !mSingleFileSearch;
		vContinue = vContinue || mUmlFiles.isEmpty();
		vContinue = vContinue || mNotationFiles.isEmpty();
		vContinue = vContinue || mGraphicalFiles.isEmpty();

		return vContinue;
	}

	/**
	 * @return List of UML files found in project
	 */
	public List<IFile> getUMLFiles() {
		return ECollections.unmodifiableEList(mUmlFiles);
	}

	/**
	 * @return List of graphical files found in project
	 */
	public List<IFile> getGraphicalFiles() {
		return Collections.unmodifiableList(mGraphicalFiles);
	}

	/**
	 * @return List of notation files found in project
	 */
	public List<IFile> getNotationFiles() {
		return Collections.unmodifiableList(mNotationFiles);
	}

	/**
	 * @return First found UML file in project or <code>null</code> if no UML file is found
	 */
	public IFile getFirstUMLFile() {
		IFile vUmlFile = null;

		if (!mUmlFiles.isEmpty()) {
			vUmlFile = mUmlFiles.get(0);
		}

		return vUmlFile;
	}

	/**
	 * @return First graphical file find in project or <code>null</code> if no
	 *         graphical file is found
	 */
	public IFile getFirstGraphicalFile() {
		IFile vGraphicalFile = null;

		if (!mGraphicalFiles.isEmpty()) {
			vGraphicalFile = mGraphicalFiles.get(0);
		}

		return vGraphicalFile;
	}

	/**
	 * @return First notation file in project or <code>null</code> if no notation file is found
	 */
	public IFile getFirstNotationFile() {
		IFile vNotationFile = null;

		if (!mNotationFiles.isEmpty()) {
			vNotationFile = mNotationFiles.get(0);
		}

		return vNotationFile;
	}
}
