/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;

/**
 * Class used to find a file in a project.
 */
public class ProjectFinder {

    /** Current project for which research is done. */
    private IProject mProject = null;

    /**
     * Default constructor.
     *
     * @param pProject Context for research
     */
    public ProjectFinder(final IProject pProject) {
        mProject = pProject;
    }

    /**
     * Find first file in current project with given extension.
     *
     * @param pExtension Search parameter
     * @return {@link IFile} found, <code>null</code> otherwise
     * @throws CoreException If problem occurs during visit of project
     */
    public IFile findFirstFile(final String pExtension) throws CoreException {
        IFile vResult = null;

        if (mProject != null && mProject.isAccessible()) {
            FileVisitorFinder vFinder = new FileVisitorFinder(pExtension);
            mProject.accept(vFinder);

            vResult = vFinder.getFile();
        }

        return vResult;
    }

    /**
     * Find all files in current project with given extensions.
     *
     * @param pExtensions Search parameter
     * @return {@link List} of {@link IFile} containing found files
     * @throws CoreException If problem occurs during visit of project
     */
    public List<IFile> findAllFiles(final String[] pExtensions) throws CoreException {
        List<IFile> vResultList = new ArrayList<>();

        if (mProject != null && mProject.isAccessible()) {
            AllFileVisitor vVisitor = new AllFileVisitor(pExtensions);
            mProject.accept(vVisitor);

            vResultList.addAll(vVisitor.getFiles());
        }

        return vResultList;
    }

    /**
     * Class to visit project and find only one file.
 */
    private class FileVisitorFinder
        implements IResourceVisitor {

        /** Extension filter. */
        private String mExtensionFilter = null;

        /** First found file with right extension. */
        private IFile mFirstFile = null;

        /**
         * Default constructor.
         *
         * @param pExtension Filter for research
         */
        FileVisitorFinder(final String pExtension) {
            mExtensionFilter = pExtension;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean visit(final IResource pResource) throws CoreException {
            boolean vContinue = mFirstFile == null;

            if (pResource.getType() == IResource.FILE && vContinue && pResource.getName().endsWith(mExtensionFilter)) {
                mFirstFile = (IFile) pResource;

                vContinue = false;
            }

            return vContinue;
        }

        /**
         * @return The found file
         */
        public IFile getFile() {
            return mFirstFile;
        }
    }

    /**
     * Visitor for project which find all files.
 */
    private class AllFileVisitor
        implements IResourceVisitor {

        /** Found files in project. */
        private List<IFile> mFilesList = null;

        /** Extension filter. */
        private List<String> mExtensionFilesList = null;

        /**
         * Default constructor.
         *
         * @param pFileExtensions Filter for research
         */
        AllFileVisitor(final String[] pFileExtensions) {
            mFilesList = new ArrayList<>();
            mExtensionFilesList = Arrays.asList(pFileExtensions);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean visit(final IResource pResource) throws CoreException {
            // Check resource
            if (pResource instanceof IFile
                && (mExtensionFilesList.contains(pResource.getFileExtension()) || mExtensionFilesList.isEmpty())) {

                mFilesList.add((IFile) pResource);
            }

            // Explore the whole project
            return true;
        }

        /**
         * @return Found files in project.
         */
        public List<IFile> getFiles() {
            return mFilesList;
        }
    }
}
