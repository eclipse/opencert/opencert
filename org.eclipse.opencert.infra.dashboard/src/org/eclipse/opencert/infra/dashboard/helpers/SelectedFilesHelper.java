/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.helpers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.opencert.infra.dashboard.DashboardActivator;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.FileEditorInput;

/**
 * Helper allowing to find from the current selection what is the current
 * project and access to its content, like its files.
 *
 * The content can be filtered according to what is needed, for example to
 * access to the propagation files only, etc.
 */
public class SelectedFilesHelper implements ISelectionHelper {

	/** File filters extension. */
	private String[] mExtensionFilters = ArrayUtils.EMPTY_STRING_ARRAY;

	/** List of filtered files in root project of selection. */
	private List<IFile> mFilteredFiles = new ArrayList<>();

	/** Selected files. */
	private IFile[] mSelectedFiles = new IFile[0];

	/** Helper allowing to find the current project. */
	private SelectedProjectHelper mSelectedProjectHelper = new SelectedProjectHelper();

	/**
	 * Default constructor.
	 */
	public SelectedFilesHelper() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initFromEvent(final ExecutionEvent pEvent) {
		mFilteredFiles.clear();
		List<IFile> vSelectedFiles = new ArrayList<>();

		// Protect against null parameter
		if (pEvent != null) {
			// Initialise the helper to which the files search is delegated
			mSelectedProjectHelper.initFromEvent(pEvent);

			// Get all the files included in the project, with the right extension
			mFilteredFiles.addAll(getFilesFromProject(mSelectedProjectHelper.getSelectedProject()));

			vSelectedFiles.addAll(searchFilesFromSelection(pEvent));

			if (vSelectedFiles.isEmpty()) {
				vSelectedFiles.addAll(searchFilesFromEditor(pEvent));
			}
		}

		mSelectedFiles = vSelectedFiles.toArray(new IFile[vSelectedFiles.size()]);
	}

	/**
	 * Search the project from the selection in the active part.
	 *
	 * @param pEvent
	 *            Execution event defining the context of this helper
	 *            initialisation
	 * @return The found project, or <code>null</code> if none found
	 */
	private List<IFile> searchFilesFromSelection(final ExecutionEvent pEvent) {
		List<IFile> vSelectedFiles = new ArrayList<>();

		// Get active workbench window
		IWorkbenchWindow vWorkenchWindow = HandlerUtil.getActiveWorkbenchWindow(pEvent);

		if (vWorkenchWindow != null) {
			// Get selection from the active part
			ISelection vCurrentSelection = getCurrentSelection(vWorkenchWindow);

			// Ensure that the selection is a tree selection, to be able to
			// work with the path of the selected object
			if (vCurrentSelection instanceof ITreeSelection) {
				// Check if valid file is selected in the active part
				vSelectedFiles.addAll(getSelectedFiles((ITreeSelection) vCurrentSelection));
			}
		}

		return vSelectedFiles;
	}

	/**
	 * Search the project from the active editor if it is the active part.
	 *
	 * @param pEvent
	 *            Execution event defining the context of this helper
	 *            initialisation
	 * @return The found project, or <code>null</code> if none found
	 */
	private List<IFile> searchFilesFromEditor(final ExecutionEvent pEvent) {
		List<IFile> vSelectedFiles = new ArrayList<>();

		// Get the active part and editor
		IWorkbenchPart vActivePart = HandlerUtil.getActivePart(pEvent);
		IEditorPart vActiveEditor = HandlerUtil.getActiveEditor(pEvent);

		// Check if the active editor is the active part
		if (vActivePart != null && vActivePart.equals(vActiveEditor)) {
			// Get the editor input
			IEditorInput vEditorInput = vActiveEditor.getEditorInput();

			if (vEditorInput instanceof FileEditorInput) {
				// Get the file used as editor input
				IFile vFile = ((FileEditorInput) vEditorInput).getFile();

				if (isValidFile(vFile)) {
					vSelectedFiles.add(vFile);
				}

			} else if (vEditorInput instanceof URIEditorInput) {
				// Get the URI used as editor input
				URI vURI = ((URIEditorInput) vEditorInput).getURI();

				IFile vFile = getFileFromURI(vURI);

				if (isValidFile(vFile)) {
					vSelectedFiles.add(vFile);
				}
			}
		}

		return vSelectedFiles;
	}

	/**
	 * Check if the file is not <code>null</code> and valid according to the
	 * extension filters.
	 *
	 * @param pFile
	 *            The file to check
	 * @return <code>true</code> if the file is valid, <code>false</code>
	 *         otherwise
	 */
	private boolean isValidFile(final IFile pFile) {
		boolean vValid = false;

		if (pFile != null) {
			vValid = ArrayUtils.isEmpty(mExtensionFilters)
					|| ArrayUtils.contains(mExtensionFilters, pFile.getFileExtension());
		}

		return vValid;
	}

	/**
	 * Check the current selection to determine if the user selected a file
	 * matching the extension. If yes, this selection is saved.
	 *
	 * @param pSelection
	 *            Selection to check
	 * @return The list of correct selected files
	 */
	private List<IFile> getSelectedFiles(final IStructuredSelection pSelection) {
		List<IFile> vSelectedFiles = new ArrayList<>();

		// Loop on the selected objects
		for (Object vSelectedObject : pSelection.toList()) {

			// Check that the current selected object is a file and cast it
			if (vSelectedObject instanceof IFile) {
				IFile vSelectedFile = (IFile) vSelectedObject;

				// Add the file in the final list if there is no filter or if it
				// pass it
				if (isValidFile(vSelectedFile)) {
					vSelectedFiles.add(vSelectedFile);
				}
			}
		}

		return vSelectedFiles;
	}

	/**
	 * Get current selection from active workbench window.
	 *
	 * @param pWorkbenchWindow
	 *            Workbench windows from which get current selection
	 * @return Current selection
	 */
	private ISelection getCurrentSelection(final IWorkbenchWindow pWorkbenchWindow) {
		return pWorkbenchWindow.getSelectionService().getSelection();
	}

	/**
	 * Get the file corresponding to the given URI.
	 *
	 * @param pURI
	 *            The URI
	 * @return The corresponding file, or <code>null</code> if not found
	 */
	private IFile getFileFromURI(final URI pURI) {
		IFile vFile = null;

		if (pURI != null) {
			URI vURI = pURI.trimFragment();

			IWorkspaceRoot vRoot = ResourcesPlugin.getWorkspace().getRoot();

			// Get the file in the workspace
			if (vURI.isPlatform()) {
				vFile = vRoot.getFile(new Path(vURI.toPlatformString(true)));
			} else if (vURI.isFile()) {
				vFile = vRoot.getFile(new Path(vURI.toFileString()));
			}
		}

		return vFile;
	}

	/**
	 * Get all files according to extension filter into project.
	 *
	 * @param pProject
	 *            Project which is explored
	 * @return List of files found
	 */
	private List<IFile> getFilesFromProject(final IProject pProject) {
		List<IFile> vProjectSAFiles = new ArrayList<>();

		// Protect against null parameter and closed project
		if (pProject != null && pProject.isAccessible()) {
			ProjectFinder vFinder = new ProjectFinder(pProject);

			try {
				vProjectSAFiles.addAll(vFinder.findAllFiles(mExtensionFilters));
			} catch (final CoreException pException) {
				DashboardActivator.getDefault().getLog().log(pException.getStatus());
			}
		}

		return vProjectSAFiles;
	}

	/**
	 * @return All files from root project selection according to extension
	 *         filter
	 */
	public List<IFile> getFilteredFiles() {
		return mFilteredFiles;
	}

	/**
	 * Get the files under selection.
	 * 
	 * @return Files under selection, otherwise an empty array
	 */
	public IFile[] getSelectedFiles() {
		return mSelectedFiles;
	}

	/**
	 * @param pExtensionFilters
	 *            Extension filters for selection
	 */
	public void setExtensionFilters(final String[] pExtensionFilters) {
		mExtensionFilters = pExtensionFilters;
	}

	/**
	 * @return The extension filters
	 */
	public String[] getExtensionFilters() {
		return mExtensionFilters;
	}

	/**
	 * Return the selected project found from the current selection. This search
	 * is delegated to a specific helper.
	 *
	 * @return The selected project found, or <code>null</code> otherwise
	 */
	public IProject getSelectedProject() {
		return mSelectedProjectHelper.getSelectedProject();
	}
}
