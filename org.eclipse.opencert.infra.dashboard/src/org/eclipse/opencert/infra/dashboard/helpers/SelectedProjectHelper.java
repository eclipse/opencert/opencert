/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.helpers;

import java.util.Objects;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Basic helper used to find the current project from the current selection in
 * the active part.
 */
public class SelectedProjectHelper implements ISelectionHelper {

	/** Current project. */
	private IProject mSelectedProject = null;

	/**
	 * Default constructor.
	 */
	public SelectedProjectHelper() {
		super();
	}

	/**
	 * {@inheritDoc}
	 *
	 * This will find the current project from the event given in parameter.
	 */
	@Override
	public void initFromEvent(final ExecutionEvent pEvent) {
		mSelectedProject = null;

		// Protect against null parameter
		if (Objects.nonNull(pEvent)) {
			// Search the project from the selection
			mSelectedProject = searchProjectFromSelection(pEvent);

			if (Objects.isNull(mSelectedProject)) {
				// Otherwise, search the project from the editor if it is the active part
				mSelectedProject = searchProjectFromEditor(pEvent);
			}
		}
	}

	/**
	 * Search the project from the selection in the active part.
	 *
	 * @param pEvent
	 *            Execution event defining the context of this helper
	 *            initialisation
	 * @return The found project, or <code>null</code> if not found
	 */
	private IProject searchProjectFromSelection(final ExecutionEvent pEvent) {
		IProject vProject = null;

		// Get active workbench window
		IWorkbenchWindow vWorkenchWindow = HandlerUtil.getActiveWorkbenchWindow(pEvent);

		if (vWorkenchWindow != null) {
			// Get selection
			ISelection vCurrentSelection = getCurrentSelection(vWorkenchWindow);

			// Ensure that the selection is a tree selection, to be able to
			// work with the path of the selected object
			if (vCurrentSelection instanceof ITreeSelection) {
				// Find the current project from the selection
				vProject = findSelectedProject((ITreeSelection) vCurrentSelection);
			}
		}

		return vProject;
	}

	/**
	 * Search the project from the active editor if it is the active part.
	 *
	 * @param pEvent
	 *            Execution event defining the context of this helper
	 *            initialisation
	 * @return The found project, or <code>null</code> if not found
	 */
	private IProject searchProjectFromEditor(final ExecutionEvent pEvent) {
		IProject vProject = null;

		// Get the active part and editor
		IWorkbenchPart vActivePart = HandlerUtil.getActivePart(pEvent);
		IEditorPart vActiveEditor = HandlerUtil.getActiveEditor(pEvent);

		// Check if the active editor is the active part
		if (vActivePart != null && vActivePart.equals(vActiveEditor)) {
			vProject = getProjectFromEditor(vActiveEditor);
		}

		return vProject;
	}

	/**
	 * Get current selection from active workbench window.
	 *
	 * @param pWorkbenchWindow
	 *            Workbench window from which the current selection is get
	 * @return The current selection
	 */
	private ISelection getCurrentSelection(final IWorkbenchWindow pWorkbenchWindow) {
		return pWorkbenchWindow.getSelectionService().getSelection();
	}

	/**
	 * Get the current project from the given selection.
	 *
	 * @param pTreeSelection
	 *            Selection from where the project is searched. Must not be
	 *            <code>null</code>
	 * @return Current project in which selection is made, otherwise
	 *         <code>null</code>
	 */
	private IProject findSelectedProject(final ITreeSelection pTreeSelection) {
		IProject vProject = null;

		// Get the paths of the selected objects
		TreePath[] vPathsArray = pTreeSelection.getPaths();

		// Ensure that at least one path has been found
		// NB : It means that at least one object is selected
		if (vPathsArray.length > 0) {
			// Get the path of the first selected object
			TreePath vSelectedPath = vPathsArray[0];

			int vIndex = 0;
			Object vCurrentObject = null;

			// Loop on the selected object of this tree selection until a
			// project is found
			// NB : In most of the case it's directly the root, but the user can
			// show working sets
			// as root elements, etc.
			while (vProject == null && vIndex < vSelectedPath.getSegmentCount()) {
				// Get the current object
				vCurrentObject = vSelectedPath.getSegment(vIndex);

				// Check if this is a resource (project or file) and get its
				// project
				if (vCurrentObject instanceof IResource) {
					// Remember of the project, this will stop the loop
					vProject = ((IResource) vCurrentObject).getProject();
				}

				vIndex++;
			}
		}

		return vProject;
	}

	/**
	 * Return the selected project based on the current selection, if it has
	 * been found during this helper initialisation.
	 *
	 * @return The project found, or <code>null</code> otherwise
	 */
	public IProject getSelectedProject() {
		return mSelectedProject;
	}

	/**
	 * Get the project from the given editor, i.e. the project of the resource used as editor input.
	 *
	 * @param pEditorPart
	 *            The editor part
	 * @return The found project, or <code>null</code> if not found
	 */
	private IProject getProjectFromEditor(final IEditorPart pEditorPart) {
		IProject vProject = null;

		if (pEditorPart != null) {
			// Get the editor input
			IEditorInput vEditorInput = pEditorPart.getEditorInput();

			if (vEditorInput instanceof IFileEditorInput) {
				// Get the file used as editor input
				IFile vFile = ((IFileEditorInput) vEditorInput).getFile();

				if (vFile != null) {
					vProject = vFile.getProject();
				}

			} else if (vEditorInput instanceof URIEditorInput) {
				// Get the URI used as editor input
				URI vURI = ((URIEditorInput) vEditorInput).getURI();

				vProject = getProjectFromURI(vURI);
			}
		}

		return vProject;
	}

	/**
	 * Get the project of the file with the given URI.
	 *
	 * @param pURI
	 *            The URI
	 * @return The found project, or <code>null</code> if not found
	 */
	private IProject getProjectFromURI(final URI pURI) {
		IProject vProject = null;

		if (pURI != null) {
			URI vURI = pURI.trimFragment();

			IFile vFile = null;

			IWorkspaceRoot vRoot = ResourcesPlugin.getWorkspace().getRoot();

			// Get the file in the workspace
			if (vURI.isPlatform()) {
				vFile = vRoot.getFile(new Path(vURI.toPlatformString(true)));
			} else if (vURI.isFile()) {
				vFile = vRoot.getFile(new Path(vURI.toFileString()));
			}

			// If a file has been found, get its project
			if (vFile != null) {
				vProject = vFile.getProject();
			}
		}

		return vProject;
	}
}
