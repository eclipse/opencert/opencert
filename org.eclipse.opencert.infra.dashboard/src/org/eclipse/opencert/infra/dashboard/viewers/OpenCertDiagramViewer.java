/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.viewers;

import org.eclipse.amalgam.explorer.activity.ui.api.editor.pages.BasicSessionActivityExplorerPage;
import org.eclipse.amalgam.explorer.activity.ui.api.editor.pages.viewers.DiagramViewer;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.TreeViewer;

/**
 * A custom diagram viewer to be used in the Dashboard. This viewer contains no
 * context menus.
 */
public class OpenCertDiagramViewer extends DiagramViewer {

	/**
	 * Default constructor.
	 *
	 * @param pActivityExplorerPage
	 *            The activity explorer page which contains this viewer
	 */
	public OpenCertDiagramViewer(final BasicSessionActivityExplorerPage pActivityExplorerPage) {
		super(pActivityExplorerPage);
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to delete all context menus in this viewer.
	 */
	@Override
	protected MenuManager initMenuToViewer(final TreeViewer pTreeViewer) {
		return null;
	}
}
