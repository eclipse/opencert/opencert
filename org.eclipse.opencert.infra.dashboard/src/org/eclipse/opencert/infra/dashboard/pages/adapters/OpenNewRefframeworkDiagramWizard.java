/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import org.eclipse.opencert.infra.dashboard.extensionregistry.OpenCertNewWizardIDs;
import org.eclipse.opencert.infra.dashboard.extensionregistry.OpenCertWizardsRegistry;
import org.eclipse.opencert.infra.dashboard.helpers.EclipseHelper;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkCreationWizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.forms.events.HyperlinkAdapter;

/**
 * A {@link HyperlinkAdapter} to open the new refframework diagram wizard.
 */
public class OpenNewRefframeworkDiagramWizard extends AbstractOpenWizardHyperLinkAdapter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected INewWizard getWizard() {
		return OpenCertWizardsRegistry.getInstance().getDawnRefframeworkNewWizard();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isCorrectWizardInstance(INewWizard pWizard) {
		return pWizard instanceof RefframeworkCreationWizard;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void initWizard(INewWizard pWizard) {
		RefframeworkCreationWizard vRefframeworkCreationWizard = (RefframeworkCreationWizard) pWizard;

		// Initialise the wizard
		vRefframeworkCreationWizard.init(EclipseHelper.getWorkbenchWindow().getWorkbench(),
				getCurrentSelectionFromProjectExplorer(OpenCertNewWizardIDs.DAWN_REFFRAMEWORK_NEW_WIZARD_ID));
	}
}
