package org.eclipse.opencert.vocabulary.importer.importWizards;

/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/

import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.vocabulary.importer.Activator;

/**
 * Provides content for a tree viewer that shows only containers.
 */
@SuppressWarnings("all")
public class CdoContainerContentProvider implements ITreeContentProvider {
	public static class CdoLabelProvider extends LabelProvider {
		private static Image folderImage;
		private static Image documentImage;

		public CdoLabelProvider() {
			if (folderImage == null) {
				folderImage = ImageDescriptor
						.createFromURL(
								FileLocator
										.find(Activator.getDefault()
												.getBundle(),
												new Path(
														"icons/famfamfam_silk_icons_v013/icons/folder_database.png"),
												null)).createImage();
			}
			if (documentImage == null) {
				documentImage = ImageDescriptor
						.createFromURL(
								FileLocator
										.find(Activator.getDefault()
												.getBundle(),
												new Path(
														"icons/famfamfam_silk_icons_v013/icons/page_white_database.png"),
												null)).createImage();
			}
		}

		@Override
		public String getText(Object element) {
			String result = null;
			if (element instanceof Root) {
				return "/";
			} else if (element instanceof CDOResourceNode) {
				result = ((CDOResourceNode) element).getName();
			}

			return result;
		}

		@Override
		public Image getImage(Object element) {
			Image result = null;
			if (element instanceof Root) {
				result = folderImage;
			} else if (element instanceof CDOResourceFolder) {
				result = folderImage;
			} else if (element instanceof CDOResource) {
				result = documentImage;
			}

			return result;
		}
	}

	public static class Root {
		private List<CDOResourceNode> nodes;

		public List<CDOResourceNode> getNodes() {
			return nodes;
		}

		public void setNodes(List<CDOResourceNode> nodes) {
			this.nodes = nodes;
		}

		public static String displayString() {
			return "/";
		}
	}

	Root root;

	/**
	 * Creates a new ContainerContentProvider.
	 */
	public CdoContainerContentProvider() {
	}

	/**
	 * The visual part that is using this content provider is about to be
	 * disposed. Deallocate all allocated SWT resources.
	 */
	public void dispose() {
	}

	/*
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.
	 * Object)
	 */
	public Object[] getChildren(Object element) {
		if (element instanceof Root) {
			return root.getNodes().toArray();
		} else if (element instanceof CDOResourceFolder) {
			return ((CDOResourceFolder) element).getNodes().toArray();
		}

		return new Object[] {};
	}

	/*
	 * @see
	 * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java
	 * .lang.Object)
	 */
	public Object[] getElements(Object element) {
		return new Object[] { root };
	}

	/*
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object
	 * )
	 */
	public Object getParent(Object element) {
		CDOResourceNode node = (CDOResourceNode) element;
		CDOResourceFolder parent = node.getFolder();
		if (parent == null) {
			return root;
		}

		return parent;
	}

	/*
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.
	 * Object)
	 */
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	/*
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		root = new Root();
		root.setNodes((List<CDOResourceNode>) newInput);
	}
}
