/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.vocabulary.importer.importWizards;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.opencert.userguidance.util.CdoConnection;
import org.eclipse.opencert.userguidance.util.CdoUtil;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.importer.parser.Parser;

public class CdoVocabularyImportWizard extends Wizard implements IImportWizard {
	// private static final String vocabularySuffix = ".vocabulary";

	CdoImportWizardPage mainPage;
	private List<CDOResourceNode> cdoNodes;
	private CdoConnection connection;

	public CdoVocabularyImportWizard() {
		super();
		connection = new CdoConnection();
	}

	public boolean performFinish() {
		
		try {
			String inputFilename = mainPage.getInputFilename();
			InputStream inputStream = new FileInputStream(new File(
					inputFilename));
			Vocabulary vocabulary = Parser.parseVocabulary(inputStream);
			String targetResourceName = mainPage.getResourceName();

			CDOTransaction transaction = connection
					.getTransaction("Vocabulary Import");
			CDOResource resource = transaction
					.createResource(targetResourceName);
			resource.getContents().add(vocabulary);
			resource.save(Collections.EMPTY_MAP);
			transaction.commit();
			transaction.close();
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("Vocabulary Import Wizard"); // NON-NLS-1
		setNeedsProgressMonitor(true);
	}

	public void addPages() {
		cdoNodes = CdoUtil.getResources(connection, false);
		mainPage = new CdoImportWizardPage("Import from File", cdoNodes); // NON-NLS-1
		addPage(mainPage);
	}
	
	@Override
	public void dispose() {
		if (connection != null) {
//			connection.dispose();
		}
		
		super.dispose();
	}

}
