/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityName2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityPrecedingActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityProducedArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRefActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRefActivitySubActivityCompartmentEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRequiredArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefFrameworkEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.WrappingLabel2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.WrappingLabel3EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.WrappingLabel4EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.WrappingLabelEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class RefframeworkVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "org.eclipse.opencert.pkm.refframework.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (RefFrameworkEditPart.MODEL_ID.equals(view.getType())) {
				return RefFrameworkEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				RefframeworkDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (RefframeworkPackage.eINSTANCE.getRefFramework().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((RefFramework) domainElement)) {
			return RefFrameworkEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
				.getModelID(containerView);
		if (!RefFrameworkEditPart.MODEL_ID.equals(containerModelID)
				&& !"refframework".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (RefFrameworkEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RefFrameworkEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case RefFrameworkEditPart.VISUAL_ID:
			if (RefframeworkPackage.eINSTANCE.getRefActivity().isSuperTypeOf(
					domainElement.eClass())) {
				return RefActivityEditPart.VISUAL_ID;
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefact().isSuperTypeOf(
					domainElement.eClass())) {
				return RefArtefactEditPart.VISUAL_ID;
			}
			if (RefframeworkPackage.eINSTANCE.getRefRole().isSuperTypeOf(
					domainElement.eClass())) {
				return RefRoleEditPart.VISUAL_ID;
			}
			break;
		case RefActivityRefActivitySubActivityCompartmentEditPart.VISUAL_ID:
			if (RefframeworkPackage.eINSTANCE.getRefActivity().isSuperTypeOf(
					domainElement.eClass())) {
				return RefActivity2EditPart.VISUAL_ID;
			}
			break;
		case RefActivityRefActivitySubActivityCompartment2EditPart.VISUAL_ID:
			if (RefframeworkPackage.eINSTANCE.getRefActivity().isSuperTypeOf(
					domainElement.eClass())) {
				return RefActivity2EditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
				.getModelID(containerView);
		if (!RefFrameworkEditPart.MODEL_ID.equals(containerModelID)
				&& !"refframework".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (RefFrameworkEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RefFrameworkEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case RefFrameworkEditPart.VISUAL_ID:
			if (RefActivityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RefArtefactEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RefRoleEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivityEditPart.VISUAL_ID:
			if (RefActivityNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RefActivityRefActivitySubActivityCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefArtefactEditPart.VISUAL_ID:
			if (RefArtefactNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefRoleEditPart.VISUAL_ID:
			if (RefRoleNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivity2EditPart.VISUAL_ID:
			if (RefActivityName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RefActivityRefActivitySubActivityCompartment2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivityRefActivitySubActivityCompartmentEditPart.VISUAL_ID:
			if (RefActivity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivityRefActivitySubActivityCompartment2EditPart.VISUAL_ID:
			if (RefActivity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivityRequiredArtefactEditPart.VISUAL_ID:
			if (WrappingLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivityProducedArtefactEditPart.VISUAL_ID:
			if (WrappingLabel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivityPrecedingActivityEditPart.VISUAL_ID:
			if (WrappingLabel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RefActivityRoleEditPart.VISUAL_ID:
			if (WrappingLabel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(RefFramework element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		switch (visualID) {
		case RefActivityRefActivitySubActivityCompartmentEditPart.VISUAL_ID:
		case RefActivityRefActivitySubActivityCompartment2EditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case RefFrameworkEditPart.VISUAL_ID:
			return false;
		case RefArtefactEditPart.VISUAL_ID:
		case RefRoleEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
