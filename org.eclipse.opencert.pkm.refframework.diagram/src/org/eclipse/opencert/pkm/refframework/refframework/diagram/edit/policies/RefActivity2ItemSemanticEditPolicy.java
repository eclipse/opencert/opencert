/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.ICompositeCommand;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityPrecedingActivityCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityPrecedingActivityReorientCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityProducedArtefactCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityProducedArtefactReorientCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityRequiredArtefactCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityRequiredArtefactReorientCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityRoleCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityRoleReorientCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityPrecedingActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityProducedArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRefActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRequiredArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes;

/**
 * @generated
 */
public class RefActivity2ItemSemanticEditPolicy extends
		RefframeworkBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RefActivity2ItemSemanticEditPolicy() {
		super(RefframeworkElementTypes.RefActivity_3001);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(
				getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator<?> it = view.getTargetEdges().iterator(); it.hasNext();) {
			Edge incomingLink = (Edge) it.next();
			if (RefframeworkVisualIDRegistry.getVisualID(incomingLink) == RefActivityPrecedingActivityEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						incomingLink.getSource().getElement(), null,
						incomingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		for (Iterator<?> it = view.getSourceEdges().iterator(); it.hasNext();) {
			Edge outgoingLink = (Edge) it.next();
			if (RefframeworkVisualIDRegistry.getVisualID(outgoingLink) == RefActivityRequiredArtefactEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						outgoingLink.getSource().getElement(), null,
						outgoingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (RefframeworkVisualIDRegistry.getVisualID(outgoingLink) == RefActivityProducedArtefactEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						outgoingLink.getSource().getElement(), null,
						outgoingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (RefframeworkVisualIDRegistry.getVisualID(outgoingLink) == RefActivityPrecedingActivityEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						outgoingLink.getSource().getElement(), null,
						outgoingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (RefframeworkVisualIDRegistry.getVisualID(outgoingLink) == RefActivityRoleEditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(
						outgoingLink.getSource().getElement(), null,
						outgoingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null) {
			// there are indirectly referenced children, need extra commands: false
			addDestroyChildNodesCommand(cmd);
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		} else {
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	private void addDestroyChildNodesCommand(ICompositeCommand cmd) {
		View view = (View) getHost().getModel();
		for (Iterator<?> nit = view.getChildren().iterator(); nit.hasNext();) {
			Node node = (Node) nit.next();
			switch (RefframeworkVisualIDRegistry.getVisualID(node)) {
			case RefActivityRefActivitySubActivityCompartment2EditPart.VISUAL_ID:
				for (Iterator<?> cit = node.getChildren().iterator(); cit
						.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (RefframeworkVisualIDRegistry.getVisualID(cnode)) {
					case RefActivity2EditPart.VISUAL_ID:
						for (Iterator<?> it = cnode.getTargetEdges().iterator(); it
								.hasNext();) {
							Edge incomingLink = (Edge) it.next();
							if (RefframeworkVisualIDRegistry
									.getVisualID(incomingLink) == RefActivityPrecedingActivityEditPart.VISUAL_ID) {
								DestroyReferenceRequest r = new DestroyReferenceRequest(
										incomingLink.getSource().getElement(),
										null, incomingLink.getTarget()
												.getElement(), false);
								cmd.add(new DestroyReferenceCommand(r));
								cmd.add(new DeleteCommand(getEditingDomain(),
										incomingLink));
								continue;
							}
						}
						for (Iterator<?> it = cnode.getSourceEdges().iterator(); it
								.hasNext();) {
							Edge outgoingLink = (Edge) it.next();
							if (RefframeworkVisualIDRegistry
									.getVisualID(outgoingLink) == RefActivityRequiredArtefactEditPart.VISUAL_ID) {
								DestroyReferenceRequest r = new DestroyReferenceRequest(
										outgoingLink.getSource().getElement(),
										null, outgoingLink.getTarget()
												.getElement(), false);
								cmd.add(new DestroyReferenceCommand(r));
								cmd.add(new DeleteCommand(getEditingDomain(),
										outgoingLink));
								continue;
							}
							if (RefframeworkVisualIDRegistry
									.getVisualID(outgoingLink) == RefActivityProducedArtefactEditPart.VISUAL_ID) {
								DestroyReferenceRequest r = new DestroyReferenceRequest(
										outgoingLink.getSource().getElement(),
										null, outgoingLink.getTarget()
												.getElement(), false);
								cmd.add(new DestroyReferenceCommand(r));
								cmd.add(new DeleteCommand(getEditingDomain(),
										outgoingLink));
								continue;
							}
							if (RefframeworkVisualIDRegistry
									.getVisualID(outgoingLink) == RefActivityPrecedingActivityEditPart.VISUAL_ID) {
								DestroyReferenceRequest r = new DestroyReferenceRequest(
										outgoingLink.getSource().getElement(),
										null, outgoingLink.getTarget()
												.getElement(), false);
								cmd.add(new DestroyReferenceCommand(r));
								cmd.add(new DeleteCommand(getEditingDomain(),
										outgoingLink));
								continue;
							}
							if (RefframeworkVisualIDRegistry
									.getVisualID(outgoingLink) == RefActivityRoleEditPart.VISUAL_ID) {
								DestroyReferenceRequest r = new DestroyReferenceRequest(
										outgoingLink.getSource().getElement(),
										null, outgoingLink.getTarget()
												.getElement(), false);
								cmd.add(new DestroyReferenceCommand(r));
								cmd.add(new DeleteCommand(getEditingDomain(),
										outgoingLink));
								continue;
							}
						}
						cmd.add(new DestroyElementCommand(
								new DestroyElementRequest(getEditingDomain(),
										cnode.getElement(), false))); // directlyOwned: true
						// don't need explicit deletion of cnode as parent's view deletion would clean child views as well 
						// cmd.add(new org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand(getEditingDomain(), cnode));
						break;
					}
				}
				break;
			}
		}
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (RefframeworkElementTypes.RefActivityRequiredArtefact_4001 == req
				.getElementType()) {
			return getGEFWrapper(new RefActivityRequiredArtefactCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		if (RefframeworkElementTypes.RefActivityProducedArtefact_4002 == req
				.getElementType()) {
			return getGEFWrapper(new RefActivityProducedArtefactCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		if (RefframeworkElementTypes.RefActivityPrecedingActivity_4003 == req
				.getElementType()) {
			return getGEFWrapper(new RefActivityPrecedingActivityCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		if (RefframeworkElementTypes.RefActivityRole_4004 == req
				.getElementType()) {
			return getGEFWrapper(new RefActivityRoleCreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (RefframeworkElementTypes.RefActivityRequiredArtefact_4001 == req
				.getElementType()) {
			return null;
		}
		if (RefframeworkElementTypes.RefActivityProducedArtefact_4002 == req
				.getElementType()) {
			return null;
		}
		if (RefframeworkElementTypes.RefActivityPrecedingActivity_4003 == req
				.getElementType()) {
			return getGEFWrapper(new RefActivityPrecedingActivityCreateCommand(
					req, req.getSource(), req.getTarget()));
		}
		if (RefframeworkElementTypes.RefActivityRole_4004 == req
				.getElementType()) {
			return null;
		}
		return null;
	}

	/**
	 * Returns command to reorient EReference based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(
			ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case RefActivityRequiredArtefactEditPart.VISUAL_ID:
			return getGEFWrapper(new RefActivityRequiredArtefactReorientCommand(
					req));
		case RefActivityProducedArtefactEditPart.VISUAL_ID:
			return getGEFWrapper(new RefActivityProducedArtefactReorientCommand(
					req));
		case RefActivityPrecedingActivityEditPart.VISUAL_ID:
			return getGEFWrapper(new RefActivityPrecedingActivityReorientCommand(
					req));
		case RefActivityRoleEditPart.VISUAL_ID:
			return getGEFWrapper(new RefActivityRoleReorientCommand(req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
