/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityName2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleNameEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.parsers.MessageFormatParser;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry;

/**
 * @generated
 */
public class RefframeworkParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser refActivityName_5002Parser;

	/**
	 * @generated
	 */
	private IParser getRefActivityName_5002Parser() {
		if (refActivityName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			refActivityName_5002Parser = parser;
		}
		return refActivityName_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser refArtefactName_5003Parser;

	/**
	 * @generated
	 */
	private IParser getRefArtefactName_5003Parser() {
		if (refArtefactName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			refArtefactName_5003Parser = parser;
		}
		return refArtefactName_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser refRoleName_5004Parser;

	/**
	 * @generated
	 */
	private IParser getRefRoleName_5004Parser() {
		if (refRoleName_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			refRoleName_5004Parser = parser;
		}
		return refRoleName_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser refActivityName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getRefActivityName_5001Parser() {
		if (refActivityName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { GeneralPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			refActivityName_5001Parser = parser;
		}
		return refActivityName_5001Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case RefActivityNameEditPart.VISUAL_ID:
			return getRefActivityName_5002Parser();
		case RefArtefactNameEditPart.VISUAL_ID:
			return getRefArtefactName_5003Parser();
		case RefRoleNameEditPart.VISUAL_ID:
			return getRefRoleName_5004Parser();
		case RefActivityName2EditPart.VISUAL_ID:
			return getRefActivityName_5001Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(RefframeworkVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(RefframeworkVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (RefframeworkElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
