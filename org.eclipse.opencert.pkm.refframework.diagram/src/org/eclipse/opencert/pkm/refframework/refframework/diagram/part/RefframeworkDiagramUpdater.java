/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityPrecedingActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityProducedArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRefActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRefActivitySubActivityCompartmentEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRequiredArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefFrameworkEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes;

/**
 * @generated
 */
public class RefframeworkDiagramUpdater {

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null
				|| view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkNodeDescriptor> getSemanticChildren(View view) {
		switch (RefframeworkVisualIDRegistry.getVisualID(view)) {
		case RefFrameworkEditPart.VISUAL_ID:
			return getRefFramework_1000SemanticChildren(view);
		case RefActivityRefActivitySubActivityCompartmentEditPart.VISUAL_ID:
			return getRefActivityRefActivitySubActivityCompartment_7001SemanticChildren(view);
		case RefActivityRefActivitySubActivityCompartment2EditPart.VISUAL_ID:
			return getRefActivityRefActivitySubActivityCompartment_7002SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkNodeDescriptor> getRefFramework_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		RefFramework modelElement = (RefFramework) view.getElement();
		LinkedList<RefframeworkNodeDescriptor> result = new LinkedList<RefframeworkNodeDescriptor>();
		for (Iterator<?> it = modelElement.getOwnedActivities().iterator(); it
				.hasNext();) {
			RefActivity childElement = (RefActivity) it.next();
			int visualID = RefframeworkVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RefActivityEditPart.VISUAL_ID) {
				result.add(new RefframeworkNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getOwnedArtefact().iterator(); it
				.hasNext();) {
			RefArtefact childElement = (RefArtefact) it.next();
			int visualID = RefframeworkVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RefArtefactEditPart.VISUAL_ID) {
				result.add(new RefframeworkNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getOwnedRole().iterator(); it
				.hasNext();) {
			RefRole childElement = (RefRole) it.next();
			int visualID = RefframeworkVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RefRoleEditPart.VISUAL_ID) {
				result.add(new RefframeworkNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkNodeDescriptor> getRefActivityRefActivitySubActivityCompartment_7001SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		RefActivity modelElement = (RefActivity) containerView.getElement();
		LinkedList<RefframeworkNodeDescriptor> result = new LinkedList<RefframeworkNodeDescriptor>();
		for (Iterator<?> it = modelElement.getSubActivity().iterator(); it
				.hasNext();) {
			RefActivity childElement = (RefActivity) it.next();
			int visualID = RefframeworkVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RefActivity2EditPart.VISUAL_ID) {
				result.add(new RefframeworkNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkNodeDescriptor> getRefActivityRefActivitySubActivityCompartment_7002SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		RefActivity modelElement = (RefActivity) containerView.getElement();
		LinkedList<RefframeworkNodeDescriptor> result = new LinkedList<RefframeworkNodeDescriptor>();
		for (Iterator<?> it = modelElement.getSubActivity().iterator(); it
				.hasNext();) {
			RefActivity childElement = (RefActivity) it.next();
			int visualID = RefframeworkVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RefActivity2EditPart.VISUAL_ID) {
				result.add(new RefframeworkNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getContainedLinks(View view) {
		switch (RefframeworkVisualIDRegistry.getVisualID(view)) {
		case RefFrameworkEditPart.VISUAL_ID:
			return getRefFramework_1000ContainedLinks(view);
		case RefActivityEditPart.VISUAL_ID:
			return getRefActivity_2001ContainedLinks(view);
		case RefArtefactEditPart.VISUAL_ID:
			return getRefArtefact_2002ContainedLinks(view);
		case RefRoleEditPart.VISUAL_ID:
			return getRefRole_2003ContainedLinks(view);
		case RefActivity2EditPart.VISUAL_ID:
			return getRefActivity_3001ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getIncomingLinks(View view) {
		switch (RefframeworkVisualIDRegistry.getVisualID(view)) {
		case RefActivityEditPart.VISUAL_ID:
			return getRefActivity_2001IncomingLinks(view);
		case RefArtefactEditPart.VISUAL_ID:
			return getRefArtefact_2002IncomingLinks(view);
		case RefRoleEditPart.VISUAL_ID:
			return getRefRole_2003IncomingLinks(view);
		case RefActivity2EditPart.VISUAL_ID:
			return getRefActivity_3001IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getOutgoingLinks(View view) {
		switch (RefframeworkVisualIDRegistry.getVisualID(view)) {
		case RefActivityEditPart.VISUAL_ID:
			return getRefActivity_2001OutgoingLinks(view);
		case RefArtefactEditPart.VISUAL_ID:
			return getRefArtefact_2002OutgoingLinks(view);
		case RefRoleEditPart.VISUAL_ID:
			return getRefRole_2003OutgoingLinks(view);
		case RefActivity2EditPart.VISUAL_ID:
			return getRefActivity_3001OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefFramework_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefActivity_2001ContainedLinks(
			View view) {
		RefActivity modelElement = (RefActivity) view.getElement();
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefArtefact_2002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefRole_2003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefActivity_3001ContainedLinks(
			View view) {
		RefActivity modelElement = (RefActivity) view.getElement();
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefActivity_2001IncomingLinks(
			View view) {
		RefActivity modelElement = (RefActivity) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefArtefact_2002IncomingLinks(
			View view) {
		RefArtefact modelElement = (RefArtefact) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_RefActivity_RequiredArtefact_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_RefActivity_ProducedArtefact_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefRole_2003IncomingLinks(
			View view) {
		RefRole modelElement = (RefRole) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_RefActivity_Role_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefActivity_3001IncomingLinks(
			View view) {
		RefActivity modelElement = (RefActivity) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefActivity_2001OutgoingLinks(
			View view) {
		RefActivity modelElement = (RefActivity) view.getElement();
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefArtefact_2002OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefRole_2003OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<RefframeworkLinkDescriptor> getRefActivity_3001OutgoingLinks(
			View view) {
		RefActivity modelElement = (RefActivity) view.getElement();
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_RequiredArtefact_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_ProducedArtefact_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RefActivity_Role_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getIncomingFeatureModelFacetLinks_RefActivity_RequiredArtefact_4001(
			RefArtefact target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == RefframeworkPackage.eINSTANCE
					.getRefActivity_RequiredArtefact()) {
				result.add(new RefframeworkLinkDescriptor(
						setting.getEObject(),
						target,
						RefframeworkElementTypes.RefActivityRequiredArtefact_4001,
						RefActivityRequiredArtefactEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getIncomingFeatureModelFacetLinks_RefActivity_ProducedArtefact_4002(
			RefArtefact target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == RefframeworkPackage.eINSTANCE
					.getRefActivity_ProducedArtefact()) {
				result.add(new RefframeworkLinkDescriptor(
						setting.getEObject(),
						target,
						RefframeworkElementTypes.RefActivityProducedArtefact_4002,
						RefActivityProducedArtefactEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getIncomingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(
			RefActivity target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == RefframeworkPackage.eINSTANCE
					.getRefActivity_PrecedingActivity()) {
				result.add(new RefframeworkLinkDescriptor(
						setting.getEObject(),
						target,
						RefframeworkElementTypes.RefActivityPrecedingActivity_4003,
						RefActivityPrecedingActivityEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getIncomingFeatureModelFacetLinks_RefActivity_Role_4004(
			RefRole target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == RefframeworkPackage.eINSTANCE
					.getRefActivity_Role()) {
				result.add(new RefframeworkLinkDescriptor(setting.getEObject(),
						target, RefframeworkElementTypes.RefActivityRole_4004,
						RefActivityRoleEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getOutgoingFeatureModelFacetLinks_RefActivity_RequiredArtefact_4001(
			RefActivity source) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		for (Iterator<?> destinations = source.getRequiredArtefact().iterator(); destinations
				.hasNext();) {
			RefArtefact destination = (RefArtefact) destinations.next();
			result.add(new RefframeworkLinkDescriptor(source, destination,
					RefframeworkElementTypes.RefActivityRequiredArtefact_4001,
					RefActivityRequiredArtefactEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getOutgoingFeatureModelFacetLinks_RefActivity_ProducedArtefact_4002(
			RefActivity source) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		for (Iterator<?> destinations = source.getProducedArtefact().iterator(); destinations
				.hasNext();) {
			RefArtefact destination = (RefArtefact) destinations.next();
			result.add(new RefframeworkLinkDescriptor(source, destination,
					RefframeworkElementTypes.RefActivityProducedArtefact_4002,
					RefActivityProducedArtefactEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getOutgoingFeatureModelFacetLinks_RefActivity_PrecedingActivity_4003(
			RefActivity source) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		for (Iterator<?> destinations = source.getPrecedingActivity()
				.iterator(); destinations.hasNext();) {
			RefActivity destination = (RefActivity) destinations.next();
			result.add(new RefframeworkLinkDescriptor(source, destination,
					RefframeworkElementTypes.RefActivityPrecedingActivity_4003,
					RefActivityPrecedingActivityEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<RefframeworkLinkDescriptor> getOutgoingFeatureModelFacetLinks_RefActivity_Role_4004(
			RefActivity source) {
		LinkedList<RefframeworkLinkDescriptor> result = new LinkedList<RefframeworkLinkDescriptor>();
		for (Iterator<?> destinations = source.getRole().iterator(); destinations
				.hasNext();) {
			RefRole destination = (RefRole) destinations.next();
			result.add(new RefframeworkLinkDescriptor(source, destination,
					RefframeworkElementTypes.RefActivityRole_4004,
					RefActivityRoleEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<RefframeworkNodeDescriptor> getSemanticChildren(View view) {
			return RefframeworkDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<RefframeworkLinkDescriptor> getContainedLinks(View view) {
			return RefframeworkDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<RefframeworkLinkDescriptor> getIncomingLinks(View view) {
			return RefframeworkDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<RefframeworkLinkDescriptor> getOutgoingLinks(View view) {
			return RefframeworkDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
