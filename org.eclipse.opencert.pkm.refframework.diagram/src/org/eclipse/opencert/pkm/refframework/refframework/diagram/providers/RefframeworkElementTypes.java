/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityPrecedingActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityProducedArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRequiredArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefFrameworkEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditorPlugin;

/**
 * @generated
 */
public class RefframeworkElementTypes {

	/**
	 * @generated
	 */
	private RefframeworkElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			RefframeworkDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType RefFramework_1000 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefFramework_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefActivity_2001 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefActivity_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefArtefact_2002 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefArtefact_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefRole_2003 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefRole_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefActivity_3001 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefActivity_3001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefActivityRequiredArtefact_4001 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefActivityRequiredArtefact_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefActivityProducedArtefact_4002 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefActivityProducedArtefact_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefActivityPrecedingActivity_4003 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefActivityPrecedingActivity_4003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RefActivityRole_4004 = getElementType("org.eclipse.opencert.pkm.refframework.diagram.RefActivityRole_4004"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(RefFramework_1000,
					RefframeworkPackage.eINSTANCE.getRefFramework());

			elements.put(RefActivity_2001,
					RefframeworkPackage.eINSTANCE.getRefActivity());

			elements.put(RefArtefact_2002,
					RefframeworkPackage.eINSTANCE.getRefArtefact());

			elements.put(RefRole_2003,
					RefframeworkPackage.eINSTANCE.getRefRole());

			elements.put(RefActivity_3001,
					RefframeworkPackage.eINSTANCE.getRefActivity());

			elements.put(RefActivityRequiredArtefact_4001,
					RefframeworkPackage.eINSTANCE
							.getRefActivity_RequiredArtefact());

			elements.put(RefActivityProducedArtefact_4002,
					RefframeworkPackage.eINSTANCE
							.getRefActivity_ProducedArtefact());

			elements.put(RefActivityPrecedingActivity_4003,
					RefframeworkPackage.eINSTANCE
							.getRefActivity_PrecedingActivity());

			elements.put(RefActivityRole_4004,
					RefframeworkPackage.eINSTANCE.getRefActivity_Role());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(RefFramework_1000);
			KNOWN_ELEMENT_TYPES.add(RefActivity_2001);
			KNOWN_ELEMENT_TYPES.add(RefArtefact_2002);
			KNOWN_ELEMENT_TYPES.add(RefRole_2003);
			KNOWN_ELEMENT_TYPES.add(RefActivity_3001);
			KNOWN_ELEMENT_TYPES.add(RefActivityRequiredArtefact_4001);
			KNOWN_ELEMENT_TYPES.add(RefActivityProducedArtefact_4002);
			KNOWN_ELEMENT_TYPES.add(RefActivityPrecedingActivity_4003);
			KNOWN_ELEMENT_TYPES.add(RefActivityRole_4004);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case RefFrameworkEditPart.VISUAL_ID:
			return RefFramework_1000;
		case RefActivityEditPart.VISUAL_ID:
			return RefActivity_2001;
		case RefArtefactEditPart.VISUAL_ID:
			return RefArtefact_2002;
		case RefRoleEditPart.VISUAL_ID:
			return RefRole_2003;
		case RefActivity2EditPart.VISUAL_ID:
			return RefActivity_3001;
		case RefActivityRequiredArtefactEditPart.VISUAL_ID:
			return RefActivityRequiredArtefact_4001;
		case RefActivityProducedArtefactEditPart.VISUAL_ID:
			return RefActivityProducedArtefact_4002;
		case RefActivityPrecedingActivityEditPart.VISUAL_ID:
			return RefActivityPrecedingActivity_4003;
		case RefActivityRoleEditPart.VISUAL_ID:
			return RefActivityRole_4004;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
