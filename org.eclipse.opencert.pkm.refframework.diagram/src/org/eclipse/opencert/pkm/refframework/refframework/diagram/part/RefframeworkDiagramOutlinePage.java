/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramEditor;
import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramEditorPlugin;
import org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage;
import org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractModelNavigator;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.IPageSite;

/**
* A customized outline page 
* 
*/
public class RefframeworkDiagramOutlinePage extends AbstractDiagramsOutlinePage {

/**
* Constructor
* 
* @param editor
*            the Editor
*/
	
public RefframeworkDiagramOutlinePage(DiagramEditor editor) {
	super(editor);
	}
    
            /**
             * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage#createNavigator(org.eclipse.swt.widgets.Composite,
             *      org.eclipse.ui.part.IPageSite)
             */
            @Override
            protected AbstractModelNavigator createNavigator(Composite parent, IPageSite pageSite) {
            	                    
            	return new RefframeworkModelNavigator(parent, getEditor(), pageSite);            	
            
            }
               
            /**
             * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage#getPreferenceStore()
             */
            @Override
            protected IPreferenceStore getPreferenceStore() {
                    return EcoreDiagramEditorPlugin.getInstance().getPreferenceStore();
            }
    
            /**
             * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage#getEditorID()
             */
            @Override
            protected String getEditorID() {
                    return "org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramEditorID"; //$NON-NLS-1$
            }
    
            /**
             * Open the new diagram in the same editor
             */
            @Override
            protected void handleDoubleClickEvent() {
                    IStructuredSelection selection = (IStructuredSelection) getSelection();
                    Object selectedObject = selection.getFirstElement();
                    if (selectedObject != null && selectedObject instanceof Diagram && getEditor().getDiagram() != selectedObject
                                    && getEditor().getDiagram().eResource().equals(((Diagram) selectedObject).eResource())) {
                            ((EcoreDiagramEditor) getEditor()).setDiagram((Diagram) selectedObject);
                   }
            }
    }


