/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.opencert.chess.traceability.propertytab;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.opencert.chess.traceability.util.Utils;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;

import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Button;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Class;



public class ComponentOpenCertSection extends AbstractPropertySection{
	
	private Class component; //uml component or sysml block
	
	private Button removeArgumentationElementButton;
	private ArgumentationElementTreeDropAdapter argumentationElementDropAdapter;
	private TableViewer argumentationElementViewer;
	

	
	public ComponentOpenCertSection(){

	}
	

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		
		super.createControls(parent, aTabbedPropertySheetPage);		
		
		Composite composite = getWidgetFactory().createComposite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(4, false));
				
		GridData gd = new GridData(SWT.FILL, SWT.FILL, true, false);
		
		ImageDescriptor remImageDes= AbstractUIPlugin.imageDescriptorFromPlugin("org.polarsys.chess.contracts.chessextension", "/icons/rem_co.gif");
		
		
		//////////////////////
		//supportedBy property
		gd = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd.horizontalSpan = 3;
		Label supportedByLabel = getWidgetFactory().createLabel(composite, "Argumentation Element", SWT.NONE);
		supportedByLabel.setLayoutData(gd);
		
		removeArgumentationElementButton = getWidgetFactory().createButton(composite, "", SWT.NONE);
		
		remImageDes= AbstractUIPlugin.imageDescriptorFromPlugin("org.polarsys.chess.contracts.chessextension", "/icons/rem_co.gif");
		removeArgumentationElementButton.setImage(remImageDes.createImage());
		
		
		removeArgumentationElementButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				ISelection selection = argumentationElementViewer.getSelection();
				ArgumentationElement arg = (ArgumentationElement) ((StructuredSelection) selection).getFirstElement();
				Utils.removeComponentArgumentationElementTrace(component, arg);
				argumentationElementViewer.remove(selection);
				argumentationElementViewer.refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

		    });
		   
		 //
		gd = new GridData(SWT.END, SWT.FILL, false, false);
		gd.horizontalSpan = 1;
		
		removeArgumentationElementButton.setLayoutData(gd);
		
		argumentationElementViewer = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL
                | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		// create the columns
		createArtefactColumns(parent,argumentationElementViewer);
		
		final Table tableart = argumentationElementViewer.getTable();
		tableart.setHeaderVisible(true);
		tableart.setLinesVisible(true);
		
		//claimViewer.setContentProvider(ArrayContentProvider.getInstance());
		argumentationElementViewer.setContentProvider(ClaimTreeContentProvider.getInstance());
		
		argumentationElementDropAdapter= new ArgumentationElementTreeDropAdapter(argumentationElementViewer);
		//Transfer[] transfers = new Transfer[] { ResourceTransfer.getInstance(), LocalSelectionTransfer.getTransfer(), PluginTransfer.getInstance() };
		Transfer[] transfers = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		int ops = DND.DROP_COPY | DND.DROP_MOVE;
		argumentationElementViewer.addDropSupport(ops, transfers, argumentationElementDropAdapter);
		gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.horizontalSpan = 4;
		
		argumentationElementViewer.getControl().setLayoutData(gd);
		argumentationElementViewer.addDoubleClickListener(new ClickListener());
		///
			
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		cleanPropertyTab();
		if (!(selection instanceof IStructuredSelection)){	
			 return;
		}
		List<?> selectionList = ((IStructuredSelection) selection).toList();
		if (selectionList.size() == 1) {
			Object selected = selectionList.get(0);
			
			EObject selectedEObject = EMFHelper.getEObject(selected);
			if (selectedEObject != null){
			    //do something
			}
			if(selected instanceof GraphicalEditPart){
				Class clazz = (Class) (((GraphicalEditPart)selected).resolveSemanticElement());
				
				component = clazz;
				
			}
		}
		
		//fill claims, artefact, agreement list
		if(component != null){

			argumentationElementViewer.setInput(Utils.getTracedArgumentationElement(component));

		}
		
		argumentationElementDropAdapter.setComponent(component);
		
	}
	


	private void cleanPropertyTab() {
		argumentationElementViewer.setInput(null);
		argumentationElementDropAdapter.setComponent(null);
		component = null;
	}

	
	
    private TableViewerColumn createTableViewerColumn(TableViewer viewer, String title, int bound, final int colNumber) {
            final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
                            SWT.NONE);
            final TableColumn column = viewerColumn.getColumn();
            column.setText(title);
            column.setWidth(bound);
            column.setResizable(true);
            column.setMoveable(true);
            return viewerColumn;
    }
    
 // create the columns for the  table.
    private void createArtefactColumns(final Composite parent, final TableViewer viewer) {
            String[] titles = { "Name", "Argumentation Model"};
            int[] bounds = { 400, 200};

            // first column is for the  description
            TableViewerColumn col = createTableViewerColumn(viewer, titles[0], bounds[0], 0);
  
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                    	ArgumentationElement arg = (ArgumentationElement) element;
                            return arg.getId()+" "+arg.getName();
                    }
            });
            
         // second column is for the  resource
            col = createTableViewerColumn(viewer, titles[1], bounds[1], 1);
            col.setLabelProvider(new ColumnLabelProvider() {
                    @Override
                    public String getText(Object element) {
                    	ArgumentationElement arg = (ArgumentationElement) element;
                            return UIutils.getPathStringFor(arg);
                    }
            });

    }
    

    
    private class ClickListener implements IDoubleClickListener {
		@Override
		public void doubleClick(DoubleClickEvent event) {
			
			EObject sel = (EObject) ((StructuredSelection) event.getSelection()).getFirstElement();
			UIutils.selectCDOObjectInProjectExplorer(sel);	
		}
	}
}
