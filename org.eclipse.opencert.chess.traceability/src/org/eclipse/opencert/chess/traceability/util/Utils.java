/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.opencert.chess.traceability.util;

import java.util.Collections;
import java.util.List;

import org.eclipse.capra.core.CapraException;
import org.eclipse.capra.core.operations.CreateConnection;
import org.eclipse.opencert.aida.tracemodel.AnalysisContextArtefactLinkAdapter;
import org.eclipse.opencert.aida.tracemodel.ComponentArgumentationElementLinkAdapter;
import org.eclipse.opencert.aida.tracemodel.ContractAgreementLinkAdapter;
import org.eclipse.opencert.aida.tracemodel.ContractArtefactLinkAdapter;
import org.eclipse.opencert.aida.tracemodel.ContractClaimLinkAdapter;
import org.eclipse.opencert.aida.tracemodel.FormalPropertyClaimLinkAdapter;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.uml2.uml.Class;
import org.polarsys.chess.contracts.profile.chesscontract.Contract;
import org.polarsys.chess.contracts.profile.chesscontract.FormalProperty;

public class Utils {

	/**
	 * creates a trace in the Capra model for the given entities
	 * 
	 * @param component
	 *            source of the trace
	 * @param argumentationElement
	 *            target of the trace
	 * @throws CapraException
	 */
	public static void trace(Class component, ArgumentationElement argumentationElement) {

		List<Object> sources = Collections.singletonList(component);
		List<Object> targets = Collections.singletonList(argumentationElement);
		CreateConnection createOperation;
		try {
			createOperation = new CreateConnection(sources, targets,
					ComponentArgumentationElementLinkAdapter.COMPONENT_ARGUMENTATIONELEMENT_LINK_TYPE);
			createOperation.execute();
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * creates a trace in the Capra model for the given entities
	 * 
	 * @param the
	 *            base class stereotyped with analysisContext, source of the
	 *            trace;
	 * @param artefact
	 *            target of the trace
	 * @throws CapraException
	 */
	public static void traceAnalysisContextToArtefact(Class analysisContext_baseClass, Artefact artefact) {

		List<Object> sources = Collections.singletonList(analysisContext_baseClass);
		List<Object> targets = Collections.singletonList(artefact);
		CreateConnection createOperation;
		try {
			createOperation = new CreateConnection(sources, targets,
					AnalysisContextArtefactLinkAdapter.ANALYSISCONTEXT_ARTEFACT_LINK_TYPE);
			createOperation.execute();
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * creates a trace in the Capra model for the given entities
	 * 
	 * @param FormalProperty
	 *            source of the trace
	 * @param claim
	 *            target of the trace
	 * @throws CapraException
	 */
	public static void trace(FormalProperty formalProperty, Claim claim) {

		List<Object> sources = Collections.singletonList(formalProperty);
		List<Object> targets = Collections.singletonList(claim);
		CreateConnection createOperation;
		try {
			createOperation = new CreateConnection(sources, targets,
					FormalPropertyClaimLinkAdapter.FORMALPROPERTY_CLAIM_LINK_TYPE);
			createOperation.execute();
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * creates a trace in the Capra model for the given entities
	 * 
	 * @param contract
	 *            source of the trace
	 * @param claim
	 *            target of the trace
	 * @throws CapraException
	 */
	public static void trace(Contract contract, Claim claim) {
		List<Object> sources = Collections.singletonList(contract);
		List<Object> targets = Collections.singletonList(claim);
		CreateConnection createOperation;
		try {
			createOperation = new CreateConnection(sources, targets,
					ContractClaimLinkAdapter.CONTRACT_CLAIM_LINK_TYPE);
			createOperation.execute();
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * creates a trace in the Capra model for the given entities
	 * 
	 * @param contract
	 *            source of the trace
	 * @param agreement
	 *            target of the trace
	 * @throws CapraException
	 */
	public static void trace(Contract contract, Agreement agreement) {

		List<Object> sources = Collections.singletonList(contract);
		List<Object> targets = Collections.singletonList(agreement);
		CreateConnection createOperation;
		try {
			createOperation = new CreateConnection(sources, targets,
					ContractAgreementLinkAdapter.CONTRACT_AGREEMENT_LINK_TYPE);
			createOperation.execute();
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * creates a trace in the Capra model for the given entities
	 * 
	 * @param contract
	 *            source of the trace
	 * @param claim
	 *            target of the trace
	 * @throws CapraException
	 */
	public static void trace(Contract contract, Artefact artefact) {

		List<Object> sources = Collections.singletonList(contract);
		List<Object> targets = Collections.singletonList(artefact);
		CreateConnection createOperation;
		try {
			createOperation = new CreateConnection(sources, targets,
					ContractArtefactLinkAdapter.CONTRACT_ARTEFACT_LINK_TYPE);
			createOperation.execute();
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * get claims currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 * @deprecated
	 *
	 * @Deprecated public static Map<GenericTraceLinkImpl, Claim>
	 *             getTracedClaim(Contract contract){ return
	 *             org.eclipse.opencert.chess.traceability.internal.util.Utils.getLinkedClaim(
	 *             contract.getBase_Class()); }
	 */

	/**
	 * get claims currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 */
	public static List<Claim> getTracedClaims(Contract contract) {
		try {
			return org.eclipse.opencert.chess.traceability.internal.util.Utils.getLinkedClaim(contract);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	/**
	 * get claims currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 *
	 * @Deprecated public static Map<GenericTraceLinkImpl, Claim>
	 *             getTracedClaim(FormalProperty formalProp){ return
	 *             org.eclipse.opencert.chess.traceability.internal.util.Utils.getLinkedClaim(formalProp.getBase_Constraint());
	 *             }
	 */

	/**
	 * get claims currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 */
	public static List<Claim> getTracedClaims(FormalProperty property) {
		try {
			return org.eclipse.opencert.chess.traceability.internal.util.Utils.getLinkedClaim(property);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	/**
	 * get claims currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 *
	 * @Deprecated public static Map<GenericTraceLinkImpl, Artefact>
	 *             getTracedEvidence(Contract contract){
	 *             Map<GenericTraceLinkImpl, Artefact> map = new
	 *             HashMap<GenericTraceLinkImpl, Artefact> (); Resource res =
	 *             contract.getBase_Class().eResource(); PersistenceAdapter
	 *             persistenceAdapter =
	 *             ExtensionPointUtil.getTracePersistenceAdapter().get(); try {
	 *             Object traceModel = persistenceAdapter.getTraceModel(new
	 *             ResourceSetImpl()); if (traceModel instanceof
	 *             GenericTraceModelImpl){ GenericTraceModelImpl model =
	 *             (GenericTraceModelImpl) traceModel; GenericTraceLinkImpl
	 *             traceLink = null; for (EObject trace : model.getTraces()){ if
	 *             (trace instanceof GenericTraceLinkImpl){ traceLink =
	 *             (GenericTraceLinkImpl) trace;
	 * 
	 *             for (EObject tobj : traceLink.getSources()){ if
	 *             (tobj.eIsProxy()){ Object resolved =
	 *             EMFUtil.safeResolve(tobj, res.getResourceSet()); if (resolved
	 *             != null && resolved.equals(contract.getBase_Class())){ for
	 *             (EObject eobj : traceLink.getTargets()){ if (eobj instanceof
	 *             Artefact){ //claims.add((Claim) eobj); map.put(traceLink,
	 *             (Artefact)eobj); } } break; } }else{ if
	 *             (EcoreUtil.getURI(tobj).equals(EcoreUtil.getURI(contract.getBase_Class()))){
	 *             for (EObject eobj : traceLink.getTargets()){ if (eobj
	 *             instanceof Artefact){ //claims.add((Claim) eobj);
	 *             map.put(traceLink, (Artefact)eobj); } } break; } } }
	 * 
	 *             }
	 * 
	 *             } }
	 * 
	 *             } catch (CapraException e) { // TODO Auto-generated catch
	 *             block e.printStackTrace(); } return map; }
	 */

	/**
	 * get claims currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 */
	public static List<Artefact> getTracedArtefacts(Contract contract) {
		try {
			return org.eclipse.opencert.chess.traceability.internal.util.Utils.getLinkedArtefact(contract);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	/**
	 * get artefacts currently traced to the given analysis context via the
	 * Capra model
	 * 
	 * @param analysisContext_baseClass
	 *            the base class representing the analysisContext
	 * @return
	 */
	public static List<Artefact> getArtefactsTracedToAnalysisContext(Class analysisContext_baseClass) {
		try {
			return org.eclipse.opencert.chess.traceability.internal.util.Utils
					.getArtefactLinkedtoAnalysisContext(analysisContext_baseClass);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	/**
	 * get Agreeemtns currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 *
	 * @Deprecated public static Map<GenericTraceLinkImpl, Agreement>
	 *             getTracedAgreement(Contract contract){
	 *             Map<GenericTraceLinkImpl, Agreement> map = new
	 *             HashMap<GenericTraceLinkImpl, Agreement> (); Resource res =
	 *             contract.getBase_Class().eResource(); PersistenceAdapter
	 *             persistenceAdapter =
	 *             ExtensionPointUtil.getTracePersistenceAdapter().get(); try {
	 *             Object traceModel = persistenceAdapter.getTraceModel(new
	 *             ResourceSetImpl()); if (traceModel instanceof
	 *             GenericTraceModelImpl){ GenericTraceModelImpl model =
	 *             (GenericTraceModelImpl) traceModel; GenericTraceLinkImpl
	 *             traceLink = null; for (EObject trace : model.getTraces()){ if
	 *             (trace instanceof GenericTraceLinkImpl){ traceLink =
	 *             (GenericTraceLinkImpl) trace;
	 * 
	 *             for (EObject tobj : traceLink.getSources()){ if
	 *             (tobj.eIsProxy()){ Object resolved =
	 *             EMFUtil.safeResolve(tobj, res.getResourceSet()); if (resolved
	 *             != null && resolved.equals(contract.getBase_Class())){ for
	 *             (EObject eobj : traceLink.getTargets()){ if (eobj instanceof
	 *             Agreement){
	 * 
	 *             map.put(traceLink, (Agreement)eobj); } } break; } }else{ if
	 *             (EcoreUtil.getURI(tobj).equals(EcoreUtil.getURI(contract.getBase_Class()))){
	 *             for (EObject eobj : traceLink.getTargets()){ if (eobj
	 *             instanceof Agreement){ //claims.add((Claim) eobj);
	 *             map.put(traceLink, (Agreement)eobj); } } break; } } }
	 * 
	 *             }
	 * 
	 *             } }
	 * 
	 *             } catch (CapraException e) { // TODO Auto-generated catch
	 *             block e.printStackTrace(); } return map; }
	 */

	/**
	 * get claims currently traced to the given contract via the Capra model
	 * 
	 * @param contract
	 * @return
	 */
	public static List<Agreement> getTracedAgreements(Contract contract) {
		try {
			return org.eclipse.opencert.chess.traceability.internal.util.Utils.getLinkedAgreement(contract);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	/**
	 * get ArgumentationElement currently traced to the given Component via the
	 * Capra model
	 * 
	 * @param contract
	 * @return
	 */
	public static List<ArgumentationElement> getTracedArgumentationElement(Class component) {
		try {
			return org.eclipse.opencert.chess.traceability.internal.util.Utils.getLinkedArgumentationElement(component);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	public static void removeContractClaimTrace(Contract contract, Claim claim) {
		try {
			org.eclipse.opencert.chess.traceability.internal.util.Utils.removeLink(contract, claim);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void removeContractEvidenceTrace(Contract contract, Artefact artefact) {
		try {
			org.eclipse.opencert.chess.traceability.internal.util.Utils.removeLink(contract, artefact);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void removeFormalPropertyClaimTrace(FormalProperty formalProperty, Claim claim) {
		try {
			org.eclipse.opencert.chess.traceability.internal.util.Utils.removeLink(formalProperty, claim);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void removeContractAgreementTrace(Contract contract, Agreement agreement) {
		try {
			org.eclipse.opencert.chess.traceability.internal.util.Utils.removeLink(contract, agreement);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void removeAnalysisContextEvidenceTrace(Class analysisContext_baseClass, Artefact artefact) {
		try {
			org.eclipse.opencert.chess.traceability.internal.util.Utils
					.removeAnalysisContextArtefactLink(analysisContext_baseClass, artefact);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void removeComponentArgumentationElementTrace(Class component, ArgumentationElement arg) {
		try {
			org.eclipse.opencert.chess.traceability.internal.util.Utils.removeLink(component, arg);
		} catch (CapraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
