/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Samina Kanwal, Faiz Ul Muram and Muhammad Atif Javed
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.epf.transformRequirements.transformation.popup.action;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
//import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartmentEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
//import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseFrameworkCanonicalEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseFrameworkPersistedCanonicalEditPolicy;
//import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditorPlugin;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditorUtil;
//import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditorUtil;//
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
//import org.eclipse.opencert.apm.baseline.baseline.diagram.part.DawnBaselineDiagramEditor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.Messages;
import org.eclipse.opencert.epf.transformRequirements.transformation.popup.action.BaselineGUI;
import org.eclipse.opencert.epf.transformRequirements.transformation.popup.action.BaselineTransformation;

//import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
//import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.CasePersistedCanonicalEditPolicy;
//import org.eclipse.opencert.sam.arg.arg.diagram.part.*;
//org.eclipse.opencert.apm.baseline.diagram.dawn/src/org/eclipse/opencert/apm/baseline/baseline/diagram/part/DawnBaselineDiagramEditor.java
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
//import java.util.Iterator;
//import java.util.LinkedList;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.cdo.common.model.EMFUtil;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
//import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.epf.uma.impl.ContentPackageImpl;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;




public class BaselineTransformationHandler implements IObjectActionDelegate {

	private Shell shell;
	private IWorkbenchPage page;
	private IWorkbenchWindow window;

	private URI domainModelURI;
	private EObject diagramRoot=null;
	private boolean transfDiagramOk=false;
	private Resource diagramResource;
	private IProgressMonitor monitor2;
	private String name;
	
	/**
	 * Constructor for Action1.
	 */
	public BaselineTransformationHandler() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
		page = targetPart.getSite().getPage();
		window = targetPart.getSite().getWorkbenchWindow();	
	} 


	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		
		MessageDialog dg = new MessageDialog(
	            shell,
	            "Select directory",
	            null,
	            "Please select the target assurance project from the CDO Repository",
	            MessageDialog.INFORMATION,
	            new String[]{
	                "Browse...", 
	                IDialogConstants.CANCEL_LABEL},
	            -1//SWT.CLOSE
	            );
	    
if(dg.open()==0){
			
			//Connect to the CDO Repository
			CDOConnectionUtil.instance.init(
					PreferenceConstants.getRepositoryName(),
					PreferenceConstants.getProtocol(),
					PreferenceConstants.getServerName());
			CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
			CDOView view = session.openView();
			CDOTransaction transaction = session.openTransaction();
			
			BaselineGUI gui = new BaselineGUI(shell,view);
			gui.create();
			gui.open();
			
			String selection = gui.resourceName();
	
			if(selection != null){
				
				
				//Get the current contentElements
				ISelection sel = page.getSelection();
				TreeSelection tree = (TreeSelection) sel;
				ContentPackageImpl contentElements = (ContentPackageImpl) tree.getFirstElement();
						
				name = contentElements.getName();	
				

				//Invoke transformation
				BaselineTransformation transformation = new BaselineTransformation(page);
			
				ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
				dialog.open();
				IProgressMonitor monitor = dialog.getProgressMonitor();
				monitor.beginTask("Generating Baseline Requirement Model ... ", 15);
				monitor.worked(1);
				monitor.subTask("Performing the transformation...");
	
				transformation.execute(monitor);  
				
				monitor.worked(5);
				monitor.subTask("Creating CDO Resources...");

				
				//Create the model CDO Resource
				CDOResource baselineModel;
				if (transaction.getOrCreateResource("/"+selection+"/BASELINE/"+name+".baseline").isExisting()) {
					try {
						transaction.getResource("/"+selection+"/BASELINE/"+name+".baseline").delete(Collections.EMPTY_MAP);
						transaction.commit();
					} catch (IOException | CommitException e) {
						e.printStackTrace();
					}
	
					baselineModel = transaction.getOrCreateResource("/"+selection+"/BASELINE/"+name+".baseline");
				}else{
					baselineModel = transaction.getOrCreateResource("/"+selection+"/BASELINE/"+name+".baseline");
				}
				
				String baselineModelFile = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()+"/Baseline/"+name+".baseline";
				URI baselineXmiUri = URI.createFileURI(baselineModelFile);
				URI baselineCDOUri = baselineModel.getURI();
				
				ResourceSet resourceSet = new ResourceSetImpl();
				resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
			
				Resource baselineXmiResource1 = resourceSet.createResource(baselineXmiUri);
				File newFile = new File(baselineModelFile);
				
				monitor.worked(6);
				monitor.subTask("Loading model to CDO Repository...");
	
				//Load the model contents into the created resource
				try {
					FileInputStream baselineFileInStream = new FileInputStream(newFile);
					baselineXmiResource1.load(baselineFileInStream,Collections.EMPTY_MAP);
					baselineXmiResource1.setURI(baselineXmiUri);	
				} catch (IOException e) {
					e.printStackTrace();
				}
			
				EMFUtil.safeResolveAll(resourceSet);
			
				CDOResource baselineCDOResource = transaction.getOrCreateResource(baselineCDOUri.path());
				baselineCDOResource.getContents().addAll(baselineXmiResource1.getContents());
	
				EMFUtil.safeResolveAll(resourceSet);
				
				monitor.worked(7);
			
				try {
					baselineCDOResource.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				
				// Create the Process model CDO Resource
				
				
				CDOResource procModel;
				if (transaction.getOrCreateResource("/"+selection+"/PROCESSES/"+name+".process").isExisting()) {
					try {
						transaction.getResource("/"+selection+"/PROCESSES/"+name+".process").delete(Collections.EMPTY_MAP);
						transaction.commit();
					} catch (IOException | CommitException e) {
						e.printStackTrace();
					}
	
					procModel = transaction.getOrCreateResource("/"+selection+"/PROCESSES/"+name+".process");
				}else{
					procModel = transaction.getOrCreateResource("/"+selection+"/PROCESSES/"+name+".process");
				}
				
				String procModelFile = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()+"/Process/"+name+".process";
				URI procXmiUri = URI.createFileURI(procModelFile);
				URI procCDOUri = procModel.getURI();
				
				ResourceSet resourceSet1 = new ResourceSetImpl();
				resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
			
				Resource procXmiResource1 = resourceSet1.createResource(baselineXmiUri);
				File newFile1 = new File(procModelFile);
				
				monitor.worked(6);
				monitor.subTask("Loading model to CDO Repository...");
	
				//Load the model contents into the created resource
				try {
					FileInputStream procFileInStream = new FileInputStream(newFile1);
					procXmiResource1.load(procFileInStream,Collections.EMPTY_MAP);
					procXmiResource1.setURI(procXmiUri);	
				} catch (IOException e) {
					e.printStackTrace();
				}
			
				EMFUtil.safeResolveAll(resourceSet);
			
				CDOResource procCDOResource = transaction.getOrCreateResource(procCDOUri.path());
				procCDOResource.getContents().addAll(procXmiResource1.getContents());
	
				EMFUtil.safeResolveAll(resourceSet1);
				
				monitor.worked(7);
			
				try {
					procCDOResource.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					e.printStackTrace();
				}		
					
				
				
				monitor.worked(8);
				monitor.subTask("Creating diagram...");
				
				//Create diagram
				createbaselineDiagram();
			
				
				monitor.worked(9);
				monitor.subTask("Creating CDO Resources...");
				
			

				//Create the diagram CDO Resource
			
				CDOResource baselineDiagram;
				
				if (transaction.getOrCreateResource("/"+selection+"/BASELINE/"+name+".baseline_diagram").isExisting()) {
					try {
						transaction.getResource("/"+selection+"/BASELINE/"+name+".baseline_diagram").delete(Collections.EMPTY_MAP);
						transaction.commit();
					} catch (IOException | CommitException e) {
						e.printStackTrace();
					}
	
					baselineDiagram = transaction.getOrCreateResource("/"+selection+"/BASELINE/"+name+".baseline_diagram");
				}else{
					baselineDiagram = transaction.getOrCreateResource("/"+selection+"/BASELINE/"+name+".baseline_diagram");
				}
				
				monitor.worked(10);
				
				//Load the diagram contents into the created resource
				String baselineDiagramFile = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()+"/Baseline/"+name+".baseline_diagram";
				URI baselineDiagUri = URI.createFileURI(baselineDiagramFile);
			    URI baselineDiagCdoURI = baselineDiagram.getURI(); 
				
				ResourceSet resourceSet2 = new ResourceSetImpl();
				resourceSet2.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
				
				Resource baselineDiagramXmiResource = resourceSet.createResource(baselineDiagUri);
				File newFile2 = new File(baselineDiagramFile);
				
				monitor.worked(11);
				monitor.subTask("Loading diagram to CDO Repository...");
				
				try {
					FileInputStream baselineFileInStream2 = new FileInputStream(newFile2);
					baselineDiagramXmiResource.load(baselineFileInStream2,Collections.EMPTY_MAP);
					baselineDiagramXmiResource.setURI(baselineDiagUri);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				EMFUtil.safeResolveAll(resourceSet2);
				
				CDOResource baselineDiagramCDOResource = transaction.getOrCreateResource(baselineDiagCdoURI.path());
				baselineDiagramCDOResource.getContents().addAll(baselineDiagramXmiResource.getContents());
				
				EMFUtil.safeResolveAll(resourceSet2);
				
				monitor.worked(12);
				
				try {
					baselineDiagramCDOResource.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				monitor.worked(13);
				
				//Commit the transaction
				try {
					transaction.commit();
				} catch (ConcurrentAccessException e) {
					e.printStackTrace();
				} catch (CommitException e) {
					e.printStackTrace();
				}
				
				
				monitor.worked(14);
				monitor.done();
				dialog.close();
				
				//if(transfDiagramOk == true){
					try{
						MessageDialog.openInformation(shell, "Transformation completed", 
									"The Baseline Diagram and Model is generated under:\n\n"
									+"    - BASELINE folders of the project "+selection+" in the CDO Repository\n"
									+"    -  project in the current workspace");
					}catch(Exception e){
						System.out.println("Hello");
					}
				}
					
				
				ProgressMonitorDialog dialog2 = new ProgressMonitorDialog(shell);
				dialog2.open();
				monitor2 = dialog2.getProgressMonitor();
				monitor2.beginTask("Opening diagram... ", 6);
				monitor2.worked(1);
				
				//Change the perspective
				if (PlatformUI.getWorkbench() != null) {
					IPerspectiveDescriptor descriptor = window.getWorkbench()
					    .getPerspectiveRegistry().findPerspectiveWithId("org.eclipse.ui.resourcePerspective");
		
		    		PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					    .getActivePage().setPerspective(descriptor);
		        }
				
				monitor2.worked(2); 
				
				generateDiagram(new NullProgressMonitor());
				
				monitor2.worked(5);
				monitor2.done();
				dialog2.close();
			
			}else{
				MessageDialog.openError(shell, "Selection error", "An assurance project must be selected in order to perform the transformation"); 
			}
		}
	//}
	
	/* Copied and modified from org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineNewDiagramFileWizard.java */
	public void createbaselineDiagram(){
		IPath workspace = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		domainModelURI = URI.createPlatformResourceURI("Baseline/"+name+".baseline", true);
		LinkedList<IFile> affectedFiles = new LinkedList<IFile>();
		File file = new File(workspace+"/Baseline/"+name+".baseline_diagram");
		
		try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		IPath location= Path.fromOSString(file.getPath()); 
		IFile diagramFile= ResourcesPlugin.getWorkspace().getRoot().getFileForLocation(location);
		BaselineDiagramEditorUtil.setCharset(diagramFile);
		affectedFiles.add(diagramFile);
		URI diagramModelURI = URI.createPlatformResourceURI("Baseline/"+name+".baseline_diagram", true);
		
		TransactionalEditingDomain myEditingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		ResourceSet resourceSet = myEditingDomain.getResourceSet();
		
		try {
			Resource resource = resourceSet.getResource(domainModelURI, true);
			diagramRoot = (EObject) resource.getContents().get(0);
		} catch (WrappedException ex) {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Unable to load resource: " + domainModelURI, ex); //$NON-NLS-1$
		}
	
			diagramResource = resourceSet.createResource(diagramModelURI);
		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				myEditingDomain,
				Messages.BaselineNewDiagramFileWizard_InitDiagramCommand,
				affectedFiles) {

			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				int diagramVID = BaselineVisualIDRegistry
						.getDiagramVisualID(diagramRoot);
				if (diagramVID != BaseFrameworkEditPart.VISUAL_ID) {
					return CommandResult
							.newErrorCommandResult(Messages.BaselineNewDiagramFileWizard_IncorrectRootError);
				}
				Diagram diagram = ViewService.createDiagram(
						diagramRoot,
						BaseFrameworkEditPart.MODEL_ID,
						BaselineDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
				diagramResource.getContents().add(diagram);
				return CommandResult.newOKCommandResult();
			}
		}; //done
		try {
			OperationHistoryFactory.getOperationHistory().execute(command,
					new NullProgressMonitor(), null);
			diagramResource.save(BaselineDiagramEditorUtil.getSaveOptions());
			BaselineDiagramEditorUtil.openDiagram(diagramResource);

		} catch (ExecutionException e) {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Unable to create model and diagram", e); //$NON-NLS-1$
		} catch (IOException ex) {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Save operation failed for: " + diagramModelURI, ex); //$NON-NLS-1$
		} catch (PartInitException ex) {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Unable to open editor", ex);
		}	
		
		transfDiagramOk = true;
	}
	
	
	public void generateDiagram(IProgressMonitor monitor) {
		try {
			generateDiagram0(monitor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
	
	
	
	public void generateDiagram0(IProgressMonitor monitor) throws IOException {
		
		if (diagramResource != null) {
			try {
				BaselineDiagramEditorUtil.openDiagram(diagramResource);

				IEditorPart editorPart = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.getActiveEditor();
				if (editorPart instanceof IDiagramWorkbenchPart) {
					DiagramEditPart  editp = ((IDiagramWorkbenchPart) editorPart).getDiagramEditPart();
				editp.removeEditPolicy(EditPolicyRoles.CANONICAL_ROLE);
				editp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
						new BaseFrameworkPersistedCanonicalEditPolicy());
				
				
				

 					
 					monitor2.worked(4);
 					
 					DiagramEditor dawnEditorPart = (DiagramEditor)editorPart;
	        		dawnEditorPart.doSave(monitor); 



	        		BaseActivityBaseActivitySubActivityCompartmentEditPart.NOT_FROM_REFFRAMEWORK = 1;
					BaseActivityBaseActivitySubActivityCompartment2EditPart.NOT_FROM_REFFRAMEWORK = 1;
					//BaselinePackage.Literals.BASE_REQUIREMENT__OWNED_REL = 1;
					
				
				}

				}catch (PartInitException e) {
				System.out.println(e);
			}
		}
	} 
	


	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {

	}


	}
	
