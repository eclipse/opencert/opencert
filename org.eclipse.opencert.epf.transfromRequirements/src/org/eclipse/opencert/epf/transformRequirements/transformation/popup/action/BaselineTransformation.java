/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Samina Kanwal, Faiz Ul Muram and Muhammad Atif Javed
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.epf.transformRequirements.transformation.popup.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
//import org.eclipse.core.runtime.Platform;
import org.eclipse.epf.library.edit.LibraryEditPlugin;
import org.eclipse.epf.uma.Activity;
import org.eclipse.epf.uma.ContentElement;
import org.eclipse.epf.uma.Practice;
import org.eclipse.epf.uma.impl.ContentPackageImpl;
import org.eclipse.epf.uma.impl.DeliveryProcessImpl;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.etl.EtlModule;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IWorkbenchPage;


public class BaselineTransformation {

	private IWorkbenchPage page;
	private String name;
	


	public BaselineTransformation(IWorkbenchPage page){
		this.page=page;
	}

	public void execute(IProgressMonitor monitor ){

		EtlModule etlModule = new EtlModule();
		
		//Get the current  ContentPackageImpl
		ISelection sel = page.getSelection();
		TreeSelection tree = (TreeSelection) sel;
	
		
		ContentPackageImpl contentElements = (ContentPackageImpl) tree.getFirstElement();
		
		name = contentElements.getName();
	
		//Get the needed model files
		String pluginPathWS = contentElements.eContainer().eResource().getURI().toFileString();
		
		File plugin2 = new File(pluginPathWS); //REQ 
		String configPath = null;
		
		//<-----------New---------->
		
		List<ContentElement> contentElements1 = contentElements.getContentElements();
	
		for(ContentElement contentElement: contentElements1){
			//Get the Practice
			if(contentElement instanceof Practice){
				Practice practice = (Practice) contentElement;
				//Get Activity Reference, DeliveryProcess
				List<Activity> dpreference = practice.getActivityReferences();
		for(Activity dpreferences: dpreference){
			if(dpreferences instanceof DeliveryProcessImpl){
				String dpreferenceElement =  dpreferences.eResource().getURI().toFileString();
				int deliveryprocess = dpreferenceElement.indexOf("deliveryprocesses");
				String procPluginPath = dpreferenceElement.substring(0,deliveryprocess);
	
		File Plugin1 = new File(procPluginPath+"/plugin.xmi"); //ROL
		File deliveryProcess = new File(dpreferenceElement); //LIB
	
		// new path jar path
		
		String spluginPath = LibraryEditPlugin.getPlugin().getBundle().getLocation();
		if(spluginPath.contains("plugins")) // Yes, in jar mode.
		{			
			String pluginPath = spluginPath.substring(15, spluginPath.indexOf("plugins"));
			configPath = pluginPath + "configuration";
			
			try {
				String etlPath = configPath + "/" + "epsilon" +"/" + "epf2Baseline.etl";
				etlModule.parse(new File(etlPath));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			monitor.worked(2);						
		}
		
		String modelPath = configPath + "/" + "model" +"/" + "uma.ecore";
		
		//Create the source models
		EmfModel reqModel = createEMFSourceModel("REQ", modelPath, plugin2, true, false);//REQ 
		EmfModel rolModel = createEMFSourceModel("ROL", modelPath, Plugin1, true, false);//ROL
		EmfModel libModel = createEMFSourceModel("LIB", modelPath, deliveryProcess, true, false);//LIB
		
		//Create the project into the workspace
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject("Baseline");
		String projectLocation = root.getLocation().toString()+"/Baseline";
		if(project.exists()){
			try {
				project.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}else{
			try {
				project.create(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
			try {
				project.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}
		
		//Create the Process project into the workspace
		IWorkspaceRoot root1 = ResourcesPlugin.getWorkspace().getRoot();
		IProject project1 = root1.getProject("Process");
		String projectLocation1 = root1.getLocation().toString()+"/Process";
		if(project1.exists()){
			try {
				project1.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}else{
			try {
				project1.create(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
			try {
				project1.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}

		monitor.worked(3);
		//Model paths
		String modelPath_ = configPath + "/" + "model" +"/" + "baseline.ecore";
		String modelPath__ = configPath + "/" + "model" +"/" + "process.ecore"; 
		//Create the target model
		EmfModel baselineModel = createEMFTargetModel("Baseline","http://baseline/1.0", modelPath_, projectLocation+"/"+name+".baseline", false, true);
		//Create the target model
		EmfModel procModel = createEMFTargetModel("PRO","http://process/1.0", modelPath__, projectLocation1+"/"+name+".process", false, true);

	
		etlModule.getContext().getModelRepository().addModel(baselineModel);
		etlModule.getContext().getModelRepository().addModel(procModel);
		etlModule.getContext().getModelRepository().addModel(reqModel);
		etlModule.getContext().getModelRepository().addModel(rolModel);
		etlModule.getContext().getModelRepository().addModel(libModel);

		//Running the transformation
		try {
			etlModule.execute();
		} catch (EolRuntimeException e) {
			e.printStackTrace();
		}
		
		monitor.worked(4);
		
		baselineModel.dispose();
		procModel.dispose();
		reqModel.dispose();
		rolModel.dispose();
		libModel.dispose();

		etlModule.getContext().getModelRepository().dispose();	

							}	
						}
					}
				}
			}
		
	protected EmfModel createEMFSourceModel(String name, String sourceMetaModelFilePath,
			File sourceModelFilePath, Boolean sourceReadOnLoad, Boolean sourceStoreOnDisposal){
		
		EmfModel emfModel= new EmfModel();
		emfModel.setName(name);
		
		if(sourceMetaModelFilePath != null && !sourceMetaModelFilePath.isEmpty()) { 
			if(sourceMetaModelFilePath.contains(",")) {
				String[] metaModelURIs = sourceMetaModelFilePath.split(",");
				List<String> files = new ArrayList<String>(metaModelURIs.length);
				
				for(int i=0;i<metaModelURIs.length; i++)
				{
					files.add(metaModelURIs[i].trim());
				};
				
				for(int i=0; i<metaModelURIs.length; i++){
					files.add(metaModelURIs[i].trim());
				}
				
				emfModel.setMetamodelFiles(files);
			}else {
				emfModel.setMetamodelFile(sourceMetaModelFilePath);
			}
		}
			
		emfModel.setModelFile(sourceModelFilePath.getAbsolutePath());
		emfModel.setReadOnLoad(sourceReadOnLoad);
		emfModel.setStoredOnDisposal(sourceStoreOnDisposal);
		
		// MCP
		emfModel.setCachingEnabled(true);
		emfModel.setExpand(true);
		
		// MCP
		try {
			emfModel.load();
		} catch (EolModelLoadingException e) {
			e.printStackTrace();
		} 
		return emfModel;
	}
	
	
	protected EmfModel createEMFTargetModel(String name, String targetMetaModelURI, String targetMetaModelFilePath,
			String targetModelFilePath, Boolean targetReadOnLoad, Boolean targetStoreOnDisposal){
		
		EmfModel emfModel= new EmfModel();
		emfModel.setName(name);

		if(targetMetaModelURI != null && !targetMetaModelURI.isEmpty()) 
		{ 
			if(targetMetaModelURI.contains(","))
			{
				String[] metaModelURIs = targetMetaModelURI.split(",");
				List<String> uris =new ArrayList<String>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					uris.add(metaModelURIs[i].trim());
				};
				emfModel.setMetamodelUris(uris);
			}
			else
			{
				emfModel.setMetamodelUri(targetMetaModelURI);
			}
		}
		
		if(targetMetaModelFilePath != null && !targetMetaModelFilePath.isEmpty()) { 
			if(targetMetaModelFilePath.contains(",")) {
				String[] metaModelURIs = targetMetaModelFilePath.split(",");
				List<String> files = new ArrayList<String>(metaModelURIs.length);
				
				for(int i=0;i<metaModelURIs.length; i++)
				{
					files.add(metaModelURIs[i].trim());
				};
				
				for(int i=0; i<metaModelURIs.length; i++){
					files.add(metaModelURIs[i].trim());
				}
				emfModel.setMetamodelFiles(files);
			}else {
				emfModel.setMetamodelFile(targetMetaModelFilePath);
			}
		}
			
		emfModel.setModelFile(targetModelFilePath);
		emfModel.setReadOnLoad(targetReadOnLoad);
		emfModel.setStoredOnDisposal(targetStoreOnDisposal);
		
		// MCP
		emfModel.setCachingEnabled(true);
		emfModel.setExpand(true);
		
		// MCP
		try {
			emfModel.load(); //254
		} catch (EolModelLoadingException e) {
			e.printStackTrace();
		} 
		return emfModel; 
						
	}
}
		


