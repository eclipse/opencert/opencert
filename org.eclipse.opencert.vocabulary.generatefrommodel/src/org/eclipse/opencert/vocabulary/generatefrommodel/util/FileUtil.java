/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.generatefrommodel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

class FileUtil {
	public static List<EObject> getModels(IProject iProject) {
		List<EObject> result = new ArrayList<EObject>();

		getModelsRecursively(iProject, result);

		return result;
	}

	private static void getModelsRecursively(IResource iResource, List<EObject> result) {
		if (!iResource.isAccessible() || iResource.isHidden()
				|| !iResource.exists()) {
			return;
		}

		if (iResource instanceof IProject) {
			if (!((IProject) iResource).isOpen()) {
				return;
			}
		}

		if (iResource instanceof IContainer) {
			IContainer iContainer = (IContainer) iResource;
			try {
				for (IResource childResource : iContainer.members()) {
					getModelsRecursively(childResource, result);
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}

		} else if (iResource instanceof IFile) {
			List<EObject> eObjects = loadResource((IFile) iResource);
			result.addAll(eObjects);
		}

	}

	private static List<EObject> loadResource(IFile file) {
		List<EObject> result = null;
		String path = file.getLocation().toString();

		XMIResourceImpl resource = new XMIResourceImpl();
		File source = new File(path);
		try {
			resource.load(new FileInputStream(source),
					new HashMap<Object, Object>());
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}

		result = resource.getContents();
		return result;
	}
}
