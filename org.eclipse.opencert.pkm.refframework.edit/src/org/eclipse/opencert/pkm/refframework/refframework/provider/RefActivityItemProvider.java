/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.opencert.infra.general.general.provider.DescribableElementItemProvider;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RefActivityItemProvider
	extends DescribableElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefActivityItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addObjectivePropertyDescriptor(object);
			addScopePropertyDescriptor(object);
			addRequiredArtefactPropertyDescriptor(object);
			addProducedArtefactPropertyDescriptor(object);
			addPrecedingActivityPropertyDescriptor(object);
			addRolePropertyDescriptor(object);
			addApplicableTechniquePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Objective feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectivePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RefActivity_objective_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RefActivity_objective_feature", "_UI_RefActivity_type"),
				 RefframeworkPackage.Literals.REF_ACTIVITY__OBJECTIVE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RefActivity_scope_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RefActivity_scope_feature", "_UI_RefActivity_type"),
				 RefframeworkPackage.Literals.REF_ACTIVITY__SCOPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Required Artefact feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequiredArtefactPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RefActivity_requiredArtefact_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RefActivity_requiredArtefact_feature", "_UI_RefActivity_type"),
				 RefframeworkPackage.Literals.REF_ACTIVITY__REQUIRED_ARTEFACT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Produced Artefact feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProducedArtefactPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RefActivity_producedArtefact_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RefActivity_producedArtefact_feature", "_UI_RefActivity_type"),
				 RefframeworkPackage.Literals.REF_ACTIVITY__PRODUCED_ARTEFACT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Preceding Activity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrecedingActivityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RefActivity_precedingActivity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RefActivity_precedingActivity_feature", "_UI_RefActivity_type"),
				 RefframeworkPackage.Literals.REF_ACTIVITY__PRECEDING_ACTIVITY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Role feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRolePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RefActivity_role_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RefActivity_role_feature", "_UI_RefActivity_type"),
				 RefframeworkPackage.Literals.REF_ACTIVITY__ROLE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Applicable Technique feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addApplicableTechniquePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RefActivity_applicableTechnique_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RefActivity_applicableTechnique_feature", "_UI_RefActivity_type"),
				 RefframeworkPackage.Literals.REF_ACTIVITY__APPLICABLE_TECHNIQUE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RefframeworkPackage.Literals.REF_ASSURABLE_ELEMENT__EQUIVALENCE);
			childrenFeatures.add(RefframeworkPackage.Literals.REF_ACTIVITY__SUB_ACTIVITY);
			childrenFeatures.add(RefframeworkPackage.Literals.REF_ACTIVITY__OWNED_REQUIREMENT);
			childrenFeatures.add(RefframeworkPackage.Literals.REF_ACTIVITY__OWNED_REL);
			childrenFeatures.add(RefframeworkPackage.Literals.REF_ACTIVITY__APPLICABILITY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns RefActivity.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RefActivity"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((RefActivity)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_RefActivity_type") :
			getString("_UI_RefActivity_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RefActivity.class)) {
			case RefframeworkPackage.REF_ACTIVITY__OBJECTIVE:
			case RefframeworkPackage.REF_ACTIVITY__SCOPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE:
			case RefframeworkPackage.REF_ACTIVITY__SUB_ACTIVITY:
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REQUIREMENT:
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REL:
			case RefframeworkPackage.REF_ACTIVITY__APPLICABILITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RefframeworkPackage.Literals.REF_ASSURABLE_ELEMENT__EQUIVALENCE,
				 RefframeworkFactory.eINSTANCE.createRefEquivalenceMap()));

		newChildDescriptors.add
			(createChildParameter
				(RefframeworkPackage.Literals.REF_ACTIVITY__SUB_ACTIVITY,
				 RefframeworkFactory.eINSTANCE.createRefActivity()));

		newChildDescriptors.add
			(createChildParameter
				(RefframeworkPackage.Literals.REF_ACTIVITY__OWNED_REQUIREMENT,
				 RefframeworkFactory.eINSTANCE.createRefRequirement()));

		newChildDescriptors.add
			(createChildParameter
				(RefframeworkPackage.Literals.REF_ACTIVITY__OWNED_REL,
				 RefframeworkFactory.eINSTANCE.createRefActivityRel()));

		newChildDescriptors.add
			(createChildParameter
				(RefframeworkPackage.Literals.REF_ACTIVITY__APPLICABILITY,
				 RefframeworkFactory.eINSTANCE.createRefApplicability()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RefframeworkEditPlugin.INSTANCE;
	}

}
