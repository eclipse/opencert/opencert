/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

import org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefArtefactArtefactEquivalenceMapPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ARTEFACTEQUIVALENCEMAP_PART = "ArtefactEquivalenceMap"; //$NON-NLS-1$

	
	/**
	 * Settings for equivalence ReferencesTable
	 */
	protected ReferencesTableSettings equivalenceSettings;
	
	/**
	 * Settings for artefactEquivalenceMap ReferencesTable
	 */
	protected ReferencesTableSettings artefactEquivalenceMapSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefArtefactArtefactEquivalenceMapPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refArtefact, String editing_mode) {
		super(editingContext, refArtefact, editing_mode);
		parts = new String[] { ARTEFACTEQUIVALENCEMAP_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.ArtefactEquivalenceMap.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefArtefact refArtefact = (RefArtefact)elt;
			final ArtefactEquivalenceMapPropertiesEditionPart artefactEquivalenceMapPart = (ArtefactEquivalenceMapPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence)) {
				equivalenceSettings = new ReferencesTableSettings(refArtefact, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
				artefactEquivalenceMapPart.initEquivalence(equivalenceSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_)) {
				artefactEquivalenceMapSettings = new ReferencesTableSettings(refArtefact, RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence());
				artefactEquivalenceMapPart.initArtefactEquivalenceMap(artefactEquivalenceMapSettings);
			}
			// init filters
			if (isAccessible(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence)) {
				artefactEquivalenceMapPart.addFilterToEquivalence(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefEquivalenceMap); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for equivalence
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_)) {
				artefactEquivalenceMapPart.addFilterToArtefactEquivalenceMap(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefEquivalenceMap); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for artefactEquivalenceMap
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence) {
			return RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence();
		}
		if (editorKey == RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_) {
			return RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefArtefact refArtefact = (RefArtefact)semanticObject;
		if (RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, equivalenceSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				equivalenceSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				equivalenceSettings.move(event.getNewIndex(), (RefEquivalenceMap) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_ == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, artefactEquivalenceMapSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				artefactEquivalenceMapSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				artefactEquivalenceMapSettings.move(event.getNewIndex(), (RefEquivalenceMap) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ArtefactEquivalenceMapPropertiesEditionPart artefactEquivalenceMapPart = (ArtefactEquivalenceMapPropertiesEditionPart)editingPart;
			if (RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.equivalence))
				artefactEquivalenceMapPart.updateEquivalence();
			if (RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.ArtefactEquivalenceMap.Properties.artefactEquivalenceMap_))
				artefactEquivalenceMapPart.updateArtefactEquivalenceMap();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence(),
			RefframeworkPackage.eINSTANCE.getRefAssurableElement_Equivalence()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
