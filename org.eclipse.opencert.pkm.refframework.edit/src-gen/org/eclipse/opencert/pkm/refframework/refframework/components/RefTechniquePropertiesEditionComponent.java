/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefTechniquePropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart;

// End of user code

/**
 * 
 * 
 */
public class RefTechniquePropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private RefTechniquePropertiesEditionPart basePart;

	/**
	 * The RefTechniqueBasePropertiesEditionComponent sub component
	 * 
	 */
	protected RefTechniqueBasePropertiesEditionComponent refTechniqueBasePropertiesEditionComponent;

	/**
	 * The TechniqueEquivalenceMap part
	 * 
	 */
	private TechniqueEquivalenceMapPropertiesEditionPart techniqueEquivalenceMapPart;

	/**
	 * The RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent refTechniqueTechniqueEquivalenceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param refTechnique the EObject to edit
	 * 
	 */
	public RefTechniquePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refTechnique, String editing_mode) {
		super(editingContext, editing_mode);
		if (refTechnique instanceof RefTechnique) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refTechnique, PropertiesEditingProvider.class);
			refTechniqueBasePropertiesEditionComponent = (RefTechniqueBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefTechniqueBasePropertiesEditionComponent.BASE_PART, RefTechniqueBasePropertiesEditionComponent.class);
			addSubComponent(refTechniqueBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refTechnique, PropertiesEditingProvider.class);
			refTechniqueTechniqueEquivalenceMapPropertiesEditionComponent = (RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.TECHNIQUEEQUIVALENCEMAP_PART, RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(refTechniqueTechniqueEquivalenceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (RefTechniqueBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (RefTechniquePropertiesEditionPart)refTechniqueBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (RefTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.TECHNIQUEEQUIVALENCEMAP_PART.equals(key)) {
			techniqueEquivalenceMapPart = (TechniqueEquivalenceMapPropertiesEditionPart)refTechniqueTechniqueEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)techniqueEquivalenceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (RefframeworkViewsRepository.RefTechnique.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (RefTechniquePropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.TechniqueEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			techniqueEquivalenceMapPart = (TechniqueEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == RefframeworkViewsRepository.RefTechnique.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.TechniqueEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
