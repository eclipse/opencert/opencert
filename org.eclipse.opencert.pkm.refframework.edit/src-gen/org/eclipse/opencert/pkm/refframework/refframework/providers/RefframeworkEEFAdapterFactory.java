/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.providers;

import org.eclipse.emf.common.notify.Adapter;

import org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory;

/**
 * 
 * 
 */
public class RefframeworkEEFAdapterFactory extends RefframeworkAdapterFactory {

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefFrameworkAdapter()
	 * 
	 */
	public Adapter createRefFrameworkAdapter() {
		return new RefFrameworkPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefRequirementAdapter()
	 * 
	 */
	public Adapter createRefRequirementAdapter() {
		return new RefRequirementPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefArtefactAdapter()
	 * 
	 */
	public Adapter createRefArtefactAdapter() {
		return new RefArtefactPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefActivityAdapter()
	 * 
	 */
	public Adapter createRefActivityAdapter() {
		return new RefActivityPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefRequirementRelAdapter()
	 * 
	 */
	public Adapter createRefRequirementRelAdapter() {
		return new RefRequirementRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefRoleAdapter()
	 * 
	 */
	public Adapter createRefRoleAdapter() {
		return new RefRolePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefApplicabilityLevelAdapter()
	 * 
	 */
	public Adapter createRefApplicabilityLevelAdapter() {
		return new RefApplicabilityLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefCriticalityLevelAdapter()
	 * 
	 */
	public Adapter createRefCriticalityLevelAdapter() {
		return new RefCriticalityLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefTechniqueAdapter()
	 * 
	 */
	public Adapter createRefTechniqueAdapter() {
		return new RefTechniquePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefArtefactRelAdapter()
	 * 
	 */
	public Adapter createRefArtefactRelAdapter() {
		return new RefArtefactRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefCriticalityApplicabilityAdapter()
	 * 
	 */
	public Adapter createRefCriticalityApplicabilityAdapter() {
		return new RefCriticalityApplicabilityPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefActivityRelAdapter()
	 * 
	 */
	public Adapter createRefActivityRelAdapter() {
		return new RefActivityRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefIndependencyLevelAdapter()
	 * 
	 */
	public Adapter createRefIndependencyLevelAdapter() {
		return new RefIndependencyLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefRecommendationLevelAdapter()
	 * 
	 */
	public Adapter createRefRecommendationLevelAdapter() {
		return new RefRecommendationLevelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefControlCategoryAdapter()
	 * 
	 */
	public Adapter createRefControlCategoryAdapter() {
		return new RefControlCategoryPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefApplicabilityAdapter()
	 * 
	 */
	public Adapter createRefApplicabilityAdapter() {
		return new RefApplicabilityPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefApplicabilityRelAdapter()
	 * 
	 */
	public Adapter createRefApplicabilityRelAdapter() {
		return new RefApplicabilityRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.pkm.refframework.refframework.util.RefframeworkAdapterFactory#createRefEquivalenceMapAdapter()
	 * 
	 */
	public Adapter createRefEquivalenceMapAdapter() {
		return new RefEquivalenceMapPropertiesEditionProvider();
	}

}
