/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.providers;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.providers.impl.PropertiesEditingProviderImpl;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

import org.eclipse.opencert.pkm.refframework.refframework.components.RefRequirementBasePropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefRequirementPropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefRequirementRequirementApplicabilityPropertiesEditionComponent;
import org.eclipse.opencert.pkm.refframework.refframework.components.RefRequirementRequirementEquivalenceMapPropertiesEditionComponent;

/**
 * 
 * 
 */
public class RefRequirementPropertiesEditionProvider extends PropertiesEditingProviderImpl {

	/**
	 * Constructor without provider for super types.
	 */
	public RefRequirementPropertiesEditionProvider() {
		super();
	}

	/**
	 * Constructor with providers for super types.
	 * @param superProviders providers to use for super types.
	 */
	public RefRequirementPropertiesEditionProvider(List<PropertiesEditingProvider> superProviders) {
		super(superProviders);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext) {
		return (editingContext.getEObject() instanceof RefRequirement) 
					&& (RefframeworkPackage.Literals.REF_REQUIREMENT == editingContext.getEObject().eClass());
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public boolean provides(PropertiesEditingContext editingContext, String part) {
		return (editingContext.getEObject() instanceof RefRequirement) && (RefRequirementBasePropertiesEditionComponent.BASE_PART.equals(part) || RefRequirementRequirementApplicabilityPropertiesEditionComponent.REQUIREMENTAPPLICABILITY_PART.equals(part) || RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.REQUIREMENTEQUIVALENCEMAP_PART.equals(part));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof RefRequirement) && (refinement == RefRequirementBasePropertiesEditionComponent.class || refinement == RefRequirementRequirementApplicabilityPropertiesEditionComponent.class || refinement == RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.class);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#provides(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.Class)
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public boolean provides(PropertiesEditingContext editingContext, String part, java.lang.Class refinement) {
		return (editingContext.getEObject() instanceof RefRequirement) && ((RefRequirementBasePropertiesEditionComponent.BASE_PART.equals(part) && refinement == RefRequirementBasePropertiesEditionComponent.class) || (RefRequirementRequirementApplicabilityPropertiesEditionComponent.REQUIREMENTAPPLICABILITY_PART.equals(part) && refinement == RefRequirementRequirementApplicabilityPropertiesEditionComponent.class) || (RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.REQUIREMENTEQUIVALENCEMAP_PART.equals(part) && refinement == RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.class));
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode) {
		if (editingContext.getEObject() instanceof RefRequirement) {
			return new RefRequirementPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String)
	 * 
	 */
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part) {
		if (editingContext.getEObject() instanceof RefRequirement) {
			if (RefRequirementBasePropertiesEditionComponent.BASE_PART.equals(part))
				return new RefRequirementBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefRequirementRequirementApplicabilityPropertiesEditionComponent.REQUIREMENTAPPLICABILITY_PART.equals(part))
				return new RefRequirementRequirementApplicabilityPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.REQUIREMENTEQUIVALENCEMAP_PART.equals(part))
				return new RefRequirementRequirementEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider#getPropertiesEditingComponent(org.eclipse.emf.eef.runtime.context.PropertiesEditingContext, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	public IPropertiesEditionComponent getPropertiesEditingComponent(PropertiesEditingContext editingContext, String mode, String part, java.lang.Class refinement) {
		if (editingContext.getEObject() instanceof RefRequirement) {
			if (RefRequirementBasePropertiesEditionComponent.BASE_PART.equals(part)
				&& refinement == RefRequirementBasePropertiesEditionComponent.class)
				return new RefRequirementBasePropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefRequirementRequirementApplicabilityPropertiesEditionComponent.REQUIREMENTAPPLICABILITY_PART.equals(part)
				&& refinement == RefRequirementRequirementApplicabilityPropertiesEditionComponent.class)
				return new RefRequirementRequirementApplicabilityPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
			if (RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.REQUIREMENTEQUIVALENCEMAP_PART.equals(part)
				&& refinement == RefRequirementRequirementEquivalenceMapPropertiesEditionComponent.class)
				return new RefRequirementRequirementEquivalenceMapPropertiesEditionComponent(editingContext, editingContext.getEObject(), mode);
		}
		return super.getPropertiesEditingComponent(editingContext, mode, part, refinement);
	}

	/**
	 * Provides the filter used by the plugin.xml to assign part forms.
	 */
	public static class EditionFilter implements IFilter {
		
		/**
		 * {@inheritDoc}
		 * 
		 * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
		 */
		public boolean select(Object toTest) {
			EObject eObj = EEFUtils.resolveSemanticObject(toTest);
			return eObj != null && RefframeworkPackage.Literals.REF_REQUIREMENT == eObj.eClass();
		}
		
	}

}
