/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.diagram.edit.parts;

import org.eclipse.opencert.vocabulary.diagram.part.VocabularyVisualIDRegistry;

import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;

public class DawnVocabularyEditPartFactory extends VocabularyEditPartFactory {

	public DawnVocabularyEditPartFactory() {
		super();
	}

	@Override
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (VocabularyVisualIDRegistry.getVisualID(view)) {
				case DawnVocabularyEditPart.VISUAL_ID :
					return new DawnVocabularyEditPart(view);
			}
		}

		return super.createEditPart(context, model);
	}
}
