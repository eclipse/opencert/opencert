/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.diagram.part;

import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewDiagramResourceWizardPage;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewResourceWizardPage;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;

import org.eclipse.emf.common.util.URI;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

import java.lang.reflect.InvocationTargetException;

public class DawnVocabularyCreationWizard extends VocabularyCreationWizard {
	private CDOView view;

	private DawnCreateNewDiagramResourceWizardPage dawnDiagramModelFilePage;

	private DawnCreateNewResourceWizardPage dawnDomainModelFilePage;

	public DawnVocabularyCreationWizard() {
		super();
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
		view = CDOConnectionUtil.instance.openView(session);
	}

	@Override
	public boolean performFinish() {
		IRunnableWithProgress op = new WorkspaceModifyOperation(null) {
			@Override
			protected void execute(IProgressMonitor monitor)
					throws CoreException, InterruptedException {
				URI diagramResourceURI = dawnDiagramModelFilePage.getURI();
				URI domainModelResourceURI = dawnDomainModelFilePage.getURI();

				diagram = DawnVocabularyDiagramEditorUtil.createDiagram(
						diagramResourceURI, domainModelResourceURI, monitor);

				if (isOpenNewlyCreatedDiagramEditor() && diagram != null) {
					try {
						DawnVocabularyDiagramEditorUtil.openDiagram(diagram);
					} catch (PartInitException e) {
						ErrorDialog
								.openError(
										getContainer().getShell(),
										Messages.VocabularyCreationWizardOpenEditorError,
										null, e.getStatus());
					}
				}
			}
		};
		try {
			getContainer().run(false, true, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			if (e.getTargetException() instanceof CoreException) {
				ErrorDialog.openError(getContainer().getShell(),
						Messages.VocabularyCreationWizardCreationError, null,
						((CoreException) e.getTargetException()).getStatus());
			} else {
				VocabularyDiagramEditorPlugin.getInstance().logError(
						"Error creating diagram", e.getTargetException()); //$NON-NLS-1$
			}
			return false;
		}
		return diagram != null;
	}

	@Override
	public void addPages() {

		dawnDiagramModelFilePage = new DawnCreateNewDiagramResourceWizardPage(
				"vocabulary_diagram", false, view);
		dawnDiagramModelFilePage
				.setTitle(Messages.VocabularyCreationWizard_DiagramModelFilePageTitle);
		dawnDiagramModelFilePage
				.setDescription(Messages.VocabularyCreationWizard_DiagramModelFilePageDescription);
		dawnDiagramModelFilePage.setCreateAutomaticResourceName(true);
		addPage(dawnDiagramModelFilePage);

		dawnDomainModelFilePage = new DawnCreateNewResourceWizardPage(
				"vocabulary", true, view) {
			@Override
			public void setVisible(boolean visible) {
				if (visible) {
					URI uri = dawnDiagramModelFilePage.getURI();
					String fileName = uri.lastSegment();
					fileName = fileName.substring(0, fileName.length()
							- ".vocabulary_diagram".length()); //$NON-NLS-1$
					fileName += ".vocabulary";
					dawnDomainModelFilePage.setResourceNamePrefix(fileName);
					dawnDomainModelFilePage
							.setResourcePath(dawnDiagramModelFilePage
									.getResourcePath());
				}
				super.setVisible(visible);
			}
		};
		dawnDomainModelFilePage
				.setTitle(Messages.VocabularyCreationWizard_DomainModelFilePageTitle);
		dawnDomainModelFilePage
				.setDescription(Messages.VocabularyCreationWizard_DomainModelFilePageDescription);

		dawnDomainModelFilePage
				.setResourceValidationType(DawnCreateNewResourceWizardPage.VALIDATION_WARN);
		addPage(dawnDomainModelFilePage);
	}

	@Override
	public void dispose() {
		view.close();
	}
}
