/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.mappings.mapping.parts;

/**
 * 
 * 
 */
public class MappingViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * MapModel view descriptor
	 * 
	 */
	public static class MapModel {
		public static class Properties {
	
			
			public static String id = "mapping::MapModel::properties::id";
			
			
			public static String name = "mapping::MapModel::properties::name";
			
			
			public static String description = "mapping::MapModel::properties::description";
			
			
			public static String mapGroupModel = "mapping::MapModel::properties::mapGroupModel";
			
			
			public static String mapModel_ = "mapping::MapModel::properties::mapModel_";
			
	
		}
	
	}

	/**
	 * MapGroup view descriptor
	 * 
	 */
	public static class MapGroup {
		public static class Properties {
	
			
			public static String id = "mapping::MapGroup::properties::id";
			
			
			public static String name = "mapping::MapGroup::properties::name";
			
			
			public static String description = "mapping::MapGroup::properties::description";
			
	
		}
	
	}

	/**
	 * Map view descriptor
	 * 
	 */
	public static class Map {
		public static class Properties {
	
			
			public static String id = "mapping::Map::properties::id";
			
			
			public static String name = "mapping::Map::properties::name";
			
			
			public static String mapGroup = "mapping::Map::properties::mapGroup";
			
			
			public static String type = "mapping::Map::properties::type";
			
	
		}
	
	}

	/**
	 * MapJustification view descriptor
	 * 
	 */
	public static class MapJustification {
		public static class Properties {
	
			
			public static String explanation = "mapping::MapJustification::properties::explanation";
			
	
		}
	
	}

	/**
	 * ComplianceMap view descriptor
	 * 
	 */
	public static class ComplianceMap {
		public static class Properties {
	
			
			public static String id = "mapping::ComplianceMap::properties::id";
			
			
			public static String name = "mapping::ComplianceMap::properties::name";
			
			
			public static String mapGroup = "mapping::ComplianceMap::properties::mapGroup";
			
			
			public static String type = "mapping::ComplianceMap::properties::type";
			
	
		}
	
	}

	/**
	 * EquivalenceMap view descriptor
	 * 
	 */
	public static class EquivalenceMap {
		public static class Properties {
	
			
			public static String id = "mapping::EquivalenceMap::properties::id";
			
			
			public static String name = "mapping::EquivalenceMap::properties::name";
			
			
			public static String mapGroup = "mapping::EquivalenceMap::properties::mapGroup";
			
			
			public static String type = "mapping::EquivalenceMap::properties::type";
			
	
		}
	
	}

}
