/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.mappings.mapping.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart;
import org.eclipse.opencert.infra.mappings.mapping.parts.MappingViewsRepository;

import org.eclipse.opencert.infra.mappings.mapping.providers.MappingMessages;

// End of user code

/**
 * 
 * 
 */
public class MapModelPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, MapModelPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected ReferencesTable mapGroupModel;
	protected List<ViewerFilter> mapGroupModelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> mapGroupModelFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable mapModel;
	protected List<ViewerFilter> mapModelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> mapModelFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public MapModelPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence mapModelStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = mapModelStep.addStep(MappingViewsRepository.MapModel.Properties.class);
		propertiesStep.addStep(MappingViewsRepository.MapModel.Properties.id);
		propertiesStep.addStep(MappingViewsRepository.MapModel.Properties.name);
		propertiesStep.addStep(MappingViewsRepository.MapModel.Properties.description);
		propertiesStep.addStep(MappingViewsRepository.MapModel.Properties.mapGroupModel);
		propertiesStep.addStep(MappingViewsRepository.MapModel.Properties.mapModel_);
		
		
		composer = new PartComposer(mapModelStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == MappingViewsRepository.MapModel.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == MappingViewsRepository.MapModel.Properties.id) {
					return createIdText(parent);
				}
				if (key == MappingViewsRepository.MapModel.Properties.name) {
					return createNameText(parent);
				}
				if (key == MappingViewsRepository.MapModel.Properties.description) {
					return createDescriptionText(parent);
				}
				if (key == MappingViewsRepository.MapModel.Properties.mapGroupModel) {
					return createMapGroupModelAdvancedTableComposition(parent);
				}
				if (key == MappingViewsRepository.MapModel.Properties.mapModel_) {
					return createMapModelAdvancedTableComposition(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(MappingMessages.MapModelPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, MappingViewsRepository.MapModel.Properties.id, MappingMessages.MapModelPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, MappingViewsRepository.MapModel.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(MappingViewsRepository.MapModel.Properties.id, MappingViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, MappingViewsRepository.MapModel.Properties.name, MappingMessages.MapModelPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, MappingViewsRepository.MapModel.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(MappingViewsRepository.MapModel.Properties.name, MappingViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(Composite parent) {
		createDescription(parent, MappingViewsRepository.MapModel.Properties.description, MappingMessages.MapModelPropertiesEditionPart_DescriptionLabel);
		description = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
			}

		});
		description.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}

		});
		EditingUtils.setID(description, MappingViewsRepository.MapModel.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(MappingViewsRepository.MapModel.Properties.description, MappingViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createMapGroupModelAdvancedTableComposition(Composite parent) {
		this.mapGroupModel = new ReferencesTable(getDescription(MappingViewsRepository.MapModel.Properties.mapGroupModel, MappingMessages.MapModelPropertiesEditionPart_MapGroupModelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapGroupModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				mapGroupModel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapGroupModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				mapGroupModel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapGroupModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				mapGroupModel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapGroupModel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				mapGroupModel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.mapGroupModelFilters) {
			this.mapGroupModel.addFilter(filter);
		}
		this.mapGroupModel.setHelpText(propertiesEditionComponent.getHelpContent(MappingViewsRepository.MapModel.Properties.mapGroupModel, MappingViewsRepository.SWT_KIND));
		this.mapGroupModel.createControls(parent);
		this.mapGroupModel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapGroupModel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData mapGroupModelData = new GridData(GridData.FILL_HORIZONTAL);
		mapGroupModelData.horizontalSpan = 3;
		this.mapGroupModel.setLayoutData(mapGroupModelData);
		this.mapGroupModel.setLowerBound(0);
		this.mapGroupModel.setUpperBound(-1);
		mapGroupModel.setID(MappingViewsRepository.MapModel.Properties.mapGroupModel);
		mapGroupModel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createMapGroupModelAdvancedTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createMapModelAdvancedTableComposition(Composite parent) {
		this.mapModel = new ReferencesTable(getDescription(MappingViewsRepository.MapModel.Properties.mapModel_, MappingMessages.MapModelPropertiesEditionPart_MapModelLabel), new ReferencesTableListener() {
			public void handleAdd() { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapModel_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				mapModel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapModel_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				mapModel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapModel_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				mapModel.refresh();
			}
			public void handleRemove(EObject element) { 
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapModel_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				mapModel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.mapModelFilters) {
			this.mapModel.addFilter(filter);
		}
		this.mapModel.setHelpText(propertiesEditionComponent.getHelpContent(MappingViewsRepository.MapModel.Properties.mapModel_, MappingViewsRepository.SWT_KIND));
		this.mapModel.createControls(parent);
		this.mapModel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(MapModelPropertiesEditionPartImpl.this, MappingViewsRepository.MapModel.Properties.mapModel_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData mapModelData = new GridData(GridData.FILL_HORIZONTAL);
		mapModelData.horizontalSpan = 3;
		this.mapModel.setLayoutData(mapModelData);
		this.mapModel.setLowerBound(0);
		this.mapModel.setUpperBound(-1);
		mapModel.setID(MappingViewsRepository.MapModel.Properties.mapModel_);
		mapModel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createMapModelAdvancedTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(MappingViewsRepository.MapModel.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(MappingMessages.MapModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(MappingViewsRepository.MapModel.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(MappingMessages.MapModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(MappingViewsRepository.MapModel.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(MappingMessages.MapModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#initMapGroupModel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initMapGroupModel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		mapGroupModel.setContentProvider(contentProvider);
		mapGroupModel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(MappingViewsRepository.MapModel.Properties.mapGroupModel);
		if (eefElementEditorReadOnlyState && mapGroupModel.isEnabled()) {
			mapGroupModel.setEnabled(false);
			mapGroupModel.setToolTipText(MappingMessages.MapModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapGroupModel.isEnabled()) {
			mapGroupModel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#updateMapGroupModel()
	 * 
	 */
	public void updateMapGroupModel() {
	mapGroupModel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#addFilterMapGroupModel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToMapGroupModel(ViewerFilter filter) {
		mapGroupModelFilters.add(filter);
		if (this.mapGroupModel != null) {
			this.mapGroupModel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#addBusinessFilterMapGroupModel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToMapGroupModel(ViewerFilter filter) {
		mapGroupModelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#isContainedInMapGroupModelTable(EObject element)
	 * 
	 */
	public boolean isContainedInMapGroupModelTable(EObject element) {
		return ((ReferencesTableSettings)mapGroupModel.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#initMapModel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initMapModel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		mapModel.setContentProvider(contentProvider);
		mapModel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(MappingViewsRepository.MapModel.Properties.mapModel_);
		if (eefElementEditorReadOnlyState && mapModel.isEnabled()) {
			mapModel.setEnabled(false);
			mapModel.setToolTipText(MappingMessages.MapModel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapModel.isEnabled()) {
			mapModel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#updateMapModel()
	 * 
	 */
	public void updateMapModel() {
	mapModel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#addFilterMapModel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToMapModel(ViewerFilter filter) {
		mapModelFilters.add(filter);
		if (this.mapModel != null) {
			this.mapModel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#addBusinessFilterMapModel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToMapModel(ViewerFilter filter) {
		mapModelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.mappings.mapping.parts.MapModelPropertiesEditionPart#isContainedInMapModelTable(EObject element)
	 * 
	 */
	public boolean isContainedInMapModelTable(EObject element) {
		return ((ReferencesTableSettings)mapModel.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return MappingMessages.MapModel_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
