/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.providers;

import org.eclipse.emf.common.notify.Adapter;

import org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory;

/**
 * 
 * 
 */
public class ArgEEFAdapterFactory extends ArgAdapterFactory {

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createCaseAdapter()
	 * 
	 */
	public Adapter createCaseAdapter() {
		return new Case_PropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createAssuranceCaseAdapter()
	 * 
	 */
	public Adapter createAssuranceCaseAdapter() {
		return new AssuranceCasePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createArgumentationAdapter()
	 * 
	 */
	public Adapter createArgumentationAdapter() {
		return new ArgumentationPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createInformationElementCitationAdapter()
	 * 
	 */
	public Adapter createInformationElementCitationAdapter() {
		return new InformationElementCitationPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createArgumentElementCitationAdapter()
	 * 
	 */
	public Adapter createArgumentElementCitationAdapter() {
		return new ArgumentElementCitationPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createArgumentReasoningAdapter()
	 * 
	 */
	public Adapter createArgumentReasoningAdapter() {
		return new ArgumentReasoningPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createClaimAdapter()
	 * 
	 */
	public Adapter createClaimAdapter() {
		return new ClaimPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createChoiceAdapter()
	 * 
	 */
	public Adapter createChoiceAdapter() {
		return new ChoicePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createAssertedInferenceAdapter()
	 * 
	 */
	public Adapter createAssertedInferenceAdapter() {
		return new AssertedInferencePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createAssertedEvidenceAdapter()
	 * 
	 */
	public Adapter createAssertedEvidenceAdapter() {
		return new AssertedEvidencePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createAssertedContextAdapter()
	 * 
	 */
	public Adapter createAssertedContextAdapter() {
		return new AssertedContextPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createAssertedChallengeAdapter()
	 * 
	 */
	public Adapter createAssertedChallengeAdapter() {
		return new AssertedChallengePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createAssertedCounterEvidenceAdapter()
	 * 
	 */
	public Adapter createAssertedCounterEvidenceAdapter() {
		return new AssertedCounterEvidencePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.sam.arg.arg.util.ArgAdapterFactory#createAgreementAdapter()
	 * 
	 */
	public Adapter createAgreementAdapter() {
		return new AgreementPropertiesEditionProvider();
	}

}
