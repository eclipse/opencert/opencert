/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts;

/**
 * 
 * 
 */
public class ArgViewsRepository {

	public static final int SWT_KIND = 0;

	public static final int FORM_KIND = 1;


	/**
	 * Case view descriptor
	 * 
	 */
	public static class Case_ {
		public static class Properties {
	
			
			public static String argument = "arg::Case::properties::argument";
			
			
			public static String argumentation = "arg::Case::properties::argumentation";
			
			
			public static String agreement = "arg::Case::properties::agreement";
			
			
			public static String cited = "arg::Case::properties::cited";
			
			
			public static String information = "arg::Case::properties::information";
			
	
		}
	
	}

	/**
	 * AssuranceCase view descriptor
	 * 
	 */
	public static class AssuranceCase {
		public static class Properties {
	
			
			public static String id = "arg::AssuranceCase::properties::id";
			
			
			public static String name = "arg::AssuranceCase::properties::name";
			
			
			public static String hasArgument = "arg::AssuranceCase::properties::hasArgument";
			
			
			public static String hasEvidence = "arg::AssuranceCase::properties::hasEvidence";
			
			
			public static String composedOf = "arg::AssuranceCase::properties::composedOf";
			
	
		}
	
	}

	/**
	 * Argumentation view descriptor
	 * 
	 */
	public static class Argumentation {
		public static class Properties {
	
			
			public static String id = "arg::Argumentation::properties::id";
			
			
			public static String name = "arg::Argumentation::properties::name";
			
			
			public static String description = "arg::Argumentation::properties::description";
			
			
			public static String content = "arg::Argumentation::properties::content";
			
			
			public static String evaluation = "arg::Argumentation::properties::evaluation";
			
			
			public static String lifecycleEvent = "arg::Argumentation::properties::lifecycleEvent";
			
			
			public static String location = "arg::Argumentation::properties::location";
			
			
			public static String argumentation_ = "arg::Argumentation::properties::argumentation_";
			
			
			public static String consistOf = "arg::Argumentation::properties::consistOf";
			
	
		}
	
	}

	/**
	 * InformationElementCitation view descriptor
	 * 
	 */
	public static class InformationElementCitation {
		public static class Properties {
	
			
			public static String id = "arg::InformationElementCitation::properties::id";
			
			
			public static String name = "arg::InformationElementCitation::properties::name";
			
			
			public static String description = "arg::InformationElementCitation::properties::description";
			
			
			public static String content = "arg::InformationElementCitation::properties::content";
			
			
			public static String type = "arg::InformationElementCitation::properties::type";
			
			
			public static String toBeInstantiated = "arg::InformationElementCitation::properties::toBeInstantiated";
			
			
			public static String url = "arg::InformationElementCitation::properties::url";
			
			
			public static String artefact = "arg::InformationElementCitation::properties::artefact";
			
	
		}
	
	}

	/**
	 * ArgumentElementCitation view descriptor
	 * 
	 */
	public static class ArgumentElementCitation {
		public static class Properties {
	
			
			public static String id = "arg::ArgumentElementCitation::properties::id";
			
			
			public static String name = "arg::ArgumentElementCitation::properties::name";
			
			
			public static String description = "arg::ArgumentElementCitation::properties::description";
			
			
			public static String content = "arg::ArgumentElementCitation::properties::content";
			
			
			public static String citedType = "arg::ArgumentElementCitation::properties::citedType";
			
			
			public static String argumentationReference = "arg::ArgumentElementCitation::properties::argumentationReference";
			
	
		}
	
	}

	/**
	 * ArgumentReasoning view descriptor
	 * 
	 */
	public static class ArgumentReasoning {
		public static class Properties {
	
			
			public static String id = "arg::ArgumentReasoning::properties::id";
			
			
			public static String name = "arg::ArgumentReasoning::properties::name";
			
			
			public static String description = "arg::ArgumentReasoning::properties::description";
			
			
			public static String content = "arg::ArgumentReasoning::properties::content";
			
			
			public static String toBeSupported = "arg::ArgumentReasoning::properties::toBeSupported";
			
			
			public static String toBeInstantiated = "arg::ArgumentReasoning::properties::toBeInstantiated";
			
			
			public static String hasStructure = "arg::ArgumentReasoning::properties::hasStructure";
			
	
		}
	
	}

	/**
	 * Claim view descriptor
	 * 
	 */
	public static class Claim {
		public static class Properties {
	
			
			public static String id = "arg::Claim::properties::id";
			
			
			public static String name = "arg::Claim::properties::name";
			
			
			public static String description = "arg::Claim::properties::description";
			
			
			public static String content = "arg::Claim::properties::content";
			
			
			public static String evaluation = "arg::Claim::properties::evaluation";
			
			
			public static String lifecycleEvent = "arg::Claim::properties::lifecycleEvent";
			
			
			public static String public_ = "arg::Claim::properties::public";
			
			
			public static String assumed = "arg::Claim::properties::assumed";
			
			
			public static String toBeSupported = "arg::Claim::properties::toBeSupported";
			
			
			public static String toBeInstantiated = "arg::Claim::properties::toBeInstantiated";
			
	
		}
	
	}

	/**
	 * Choice view descriptor
	 * 
	 */
	public static class Choice {
		public static class Properties {
	
			
			public static String id = "arg::Choice::properties::id";
			
			
			public static String name = "arg::Choice::properties::name";
			
			
			public static String description = "arg::Choice::properties::description";
			
			
			public static String content = "arg::Choice::properties::content";
			
			
			public static String sourceMultiextension = "arg::Choice::properties::sourceMultiextension";
			
			
			public static String sourceCardinality = "arg::Choice::properties::sourceCardinality";
			
			
			public static String optionality = "arg::Choice::properties::optionality";
			
	
		}
	
	}

	/**
	 * AssertedInference view descriptor
	 * 
	 */
	public static class AssertedInference {
		public static class Properties {
	
			
			public static String id = "arg::AssertedInference::properties::id";
			
			
			public static String name = "arg::AssertedInference::properties::name";
			
			
			public static String description = "arg::AssertedInference::properties::description";
			
			
			public static String content = "arg::AssertedInference::properties::content";
			
			
			public static String multiextension = "arg::AssertedInference::properties::multiextension";
			
			
			public static String cardinality = "arg::AssertedInference::properties::cardinality";
			
			
			public static String source = "arg::AssertedInference::properties::source";
			
			
			public static String target = "arg::AssertedInference::properties::target";
			
	
		}
	
	}

	/**
	 * AssertedEvidence view descriptor
	 * 
	 */
	public static class AssertedEvidence {
		public static class Properties {
	
			
			public static String id = "arg::AssertedEvidence::properties::id";
			
			
			public static String name = "arg::AssertedEvidence::properties::name";
			
			
			public static String description = "arg::AssertedEvidence::properties::description";
			
			
			public static String content = "arg::AssertedEvidence::properties::content";
			
			
			public static String multiextension = "arg::AssertedEvidence::properties::multiextension";
			
			
			public static String cardinality = "arg::AssertedEvidence::properties::cardinality";
			
			
			public static String source = "arg::AssertedEvidence::properties::source";
			
			
			public static String target = "arg::AssertedEvidence::properties::target";
			
	
		}
	
	}

	/**
	 * AssertedContext view descriptor
	 * 
	 */
	public static class AssertedContext {
		public static class Properties {
	
			
			public static String id = "arg::AssertedContext::properties::id";
			
			
			public static String name = "arg::AssertedContext::properties::name";
			
			
			public static String description = "arg::AssertedContext::properties::description";
			
			
			public static String content = "arg::AssertedContext::properties::content";
			
			
			public static String multiextension = "arg::AssertedContext::properties::multiextension";
			
			
			public static String cardinality = "arg::AssertedContext::properties::cardinality";
			
			
			public static String source = "arg::AssertedContext::properties::source";
			
			
			public static String target = "arg::AssertedContext::properties::target";
			
	
		}
	
	}

	/**
	 * AssertedChallenge view descriptor
	 * 
	 */
	public static class AssertedChallenge {
		public static class Properties {
	
			
			public static String id = "arg::AssertedChallenge::properties::id";
			
			
			public static String name = "arg::AssertedChallenge::properties::name";
			
			
			public static String description = "arg::AssertedChallenge::properties::description";
			
			
			public static String content = "arg::AssertedChallenge::properties::content";
			
			
			public static String source = "arg::AssertedChallenge::properties::source";
			
			
			public static String target = "arg::AssertedChallenge::properties::target";
			
	
		}
	
	}

	/**
	 * AssertedCounterEvidence view descriptor
	 * 
	 */
	public static class AssertedCounterEvidence {
		public static class Properties {
	
			
			public static String id = "arg::AssertedCounterEvidence::properties::id";
			
			
			public static String name = "arg::AssertedCounterEvidence::properties::name";
			
			
			public static String description = "arg::AssertedCounterEvidence::properties::description";
			
			
			public static String content = "arg::AssertedCounterEvidence::properties::content";
			
			
			public static String source = "arg::AssertedCounterEvidence::properties::source";
			
			
			public static String target = "arg::AssertedCounterEvidence::properties::target";
			
	
		}
	
	}

	/**
	 * Agreement view descriptor
	 * 
	 */
	public static class Agreement {
		public static class Properties {
	
			
			public static String id = "arg::Agreement::properties::id";
			
			
			public static String name = "arg::Agreement::properties::name";
			
			
			public static String description = "arg::Agreement::properties::description";
			
			
			public static String content = "arg::Agreement::properties::content";
			
			
			public static String evaluation = "arg::Agreement::properties::evaluation";
			
			
			public static String lifecycleEvent = "arg::Agreement::properties::lifecycleEvent";
			
			
			public static String location = "arg::Agreement::properties::location";
			
			
			public static String argumentation = "arg::Agreement::properties::argumentation";
			
			
			public static String consistOf = "arg::Agreement::properties::consistOf";
			
			
			public static String between = "arg::Agreement::properties::between";
			
	
		}
	
	}

}
