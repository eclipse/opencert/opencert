/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.AssuranceCase;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.AssuranceCasePropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class AssuranceCasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for hasArgument ReferencesTable
	 */
	protected ReferencesTableSettings hasArgumentSettings;
	
	/**
	 * Settings for hasEvidence ReferencesTable
	 */
	protected ReferencesTableSettings hasEvidenceSettings;
	
	/**
	 * Settings for composedOf ReferencesTable
	 */
	private ReferencesTableSettings composedOfSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public AssuranceCasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject assuranceCase, String editing_mode) {
		super(editingContext, assuranceCase, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ArgViewsRepository.class;
		partKey = ArgViewsRepository.AssuranceCase.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final AssuranceCase assuranceCase = (AssuranceCase)elt;
			final AssuranceCasePropertiesEditionPart basePart = (AssuranceCasePropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceCase.getId()));
			
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceCase.getName()));
			
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.hasArgument)) {
				hasArgumentSettings = new ReferencesTableSettings(assuranceCase, ArgPackage.eINSTANCE.getAssuranceCase_HasArgument());
				basePart.initHasArgument(hasArgumentSettings);
			}
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.hasEvidence)) {
				hasEvidenceSettings = new ReferencesTableSettings(assuranceCase, ArgPackage.eINSTANCE.getAssuranceCase_HasEvidence());
				basePart.initHasEvidence(hasEvidenceSettings);
			}
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.composedOf)) {
				composedOfSettings = new ReferencesTableSettings(assuranceCase, ArgPackage.eINSTANCE.getAssuranceCase_ComposedOf());
				basePart.initComposedOf(composedOfSettings);
			}
			// init filters
			
			
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.hasArgument)) {
				basePart.addFilterToHasArgument(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Argumentation); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for hasArgument
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.hasEvidence)) {
				basePart.addFilterToHasEvidence(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Artefact); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for hasEvidence
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.AssuranceCase.Properties.composedOf)) {
				basePart.addFilterToComposedOf(new EObjectFilter(ArgPackage.Literals.ASSURANCE_CASE));
				// Start of user code for additional businessfilters for composedOf
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}








	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ArgViewsRepository.AssuranceCase.Properties.id) {
			return ArgPackage.eINSTANCE.getModelElement_Id();
		}
		if (editorKey == ArgViewsRepository.AssuranceCase.Properties.name) {
			return ArgPackage.eINSTANCE.getModelElement_Name();
		}
		if (editorKey == ArgViewsRepository.AssuranceCase.Properties.hasArgument) {
			return ArgPackage.eINSTANCE.getAssuranceCase_HasArgument();
		}
		if (editorKey == ArgViewsRepository.AssuranceCase.Properties.hasEvidence) {
			return ArgPackage.eINSTANCE.getAssuranceCase_HasEvidence();
		}
		if (editorKey == ArgViewsRepository.AssuranceCase.Properties.composedOf) {
			return ArgPackage.eINSTANCE.getAssuranceCase_ComposedOf();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		AssuranceCase assuranceCase = (AssuranceCase)semanticObject;
		if (ArgViewsRepository.AssuranceCase.Properties.id == event.getAffectedEditor()) {
			assuranceCase.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.AssuranceCase.Properties.name == event.getAffectedEditor()) {
			assuranceCase.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.AssuranceCase.Properties.hasArgument == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, hasArgumentSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				hasArgumentSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				hasArgumentSettings.move(event.getNewIndex(), (Argumentation) event.getNewValue());
			}
		}
		if (ArgViewsRepository.AssuranceCase.Properties.hasEvidence == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, hasEvidenceSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				hasEvidenceSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				hasEvidenceSettings.move(event.getNewIndex(), (Artefact) event.getNewValue());
			}
		}
		if (ArgViewsRepository.AssuranceCase.Properties.composedOf == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof AssuranceCase) {
					composedOfSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				composedOfSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				composedOfSettings.move(event.getNewIndex(), (AssuranceCase) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			AssuranceCasePropertiesEditionPart basePart = (AssuranceCasePropertiesEditionPart)editingPart;
			if (ArgPackage.eINSTANCE.getModelElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.AssuranceCase.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (ArgPackage.eINSTANCE.getModelElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.AssuranceCase.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (ArgPackage.eINSTANCE.getAssuranceCase_HasArgument().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.AssuranceCase.Properties.hasArgument))
				basePart.updateHasArgument();
			if (ArgPackage.eINSTANCE.getAssuranceCase_HasEvidence().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.AssuranceCase.Properties.hasEvidence))
				basePart.updateHasEvidence();
			if (ArgPackage.eINSTANCE.getAssuranceCase_ComposedOf().equals(msg.getFeature())  && isAccessible(ArgViewsRepository.AssuranceCase.Properties.composedOf))
				basePart.updateComposedOf();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ArgPackage.eINSTANCE.getModelElement_Id(),
			ArgPackage.eINSTANCE.getModelElement_Name(),
			ArgPackage.eINSTANCE.getAssuranceCase_HasArgument(),
			ArgPackage.eINSTANCE.getAssuranceCase_HasEvidence(),
			ArgPackage.eINSTANCE.getAssuranceCase_ComposedOf()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (ArgViewsRepository.AssuranceCase.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.AssuranceCase.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
