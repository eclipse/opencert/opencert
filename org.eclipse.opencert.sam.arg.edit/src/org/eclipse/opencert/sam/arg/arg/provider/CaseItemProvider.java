/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Case;

/**
 * This is the item provider adapter for a {@link org.eclipse.opencert.sam.arg.arg.Case} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CaseItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ArgPackage.Literals.CASE__ARGUMENT);
			childrenFeatures.add(ArgPackage.Literals.CASE__ARGUMENTATION);
			childrenFeatures.add(ArgPackage.Literals.CASE__AGREEMENT);
			childrenFeatures.add(ArgPackage.Literals.CASE__CITED);
			childrenFeatures.add(ArgPackage.Literals.CASE__INFORMATION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Case.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Case"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_Case_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Case.class)) {
			case ArgPackage.CASE__ARGUMENT:
			case ArgPackage.CASE__ARGUMENTATION:
			case ArgPackage.CASE__AGREEMENT:
			case ArgPackage.CASE__CITED:
			case ArgPackage.CASE__INFORMATION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createInformationElementCitation()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createArgumentElementCitation()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createArgumentReasoning()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createClaim()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createChoice()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createAssertedInference()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createAssertedEvidence()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createAssertedContext()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createAssertedChallenge()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENT,
				 ArgFactory.eINSTANCE.createAssertedCounterEvidence()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENTATION,
				 ArgFactory.eINSTANCE.createArgumentation()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__ARGUMENTATION,
				 ArgFactory.eINSTANCE.createAgreement()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__AGREEMENT,
				 ArgFactory.eINSTANCE.createAgreement()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__CITED,
				 ArgFactory.eINSTANCE.createArgumentElementCitation()));

		newChildDescriptors.add
			(createChildParameter
				(ArgPackage.Literals.CASE__INFORMATION,
				 ArgFactory.eINSTANCE.createInformationElementCitation()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ArgPackage.Literals.CASE__ARGUMENT ||
			childFeature == ArgPackage.Literals.CASE__INFORMATION ||
			childFeature == ArgPackage.Literals.CASE__CITED ||
			childFeature == ArgPackage.Literals.CASE__ARGUMENTATION ||
			childFeature == ArgPackage.Literals.CASE__AGREEMENT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ArgEditPlugin.INSTANCE;
	}

}
