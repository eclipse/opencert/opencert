/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class ActivityAssuranceAssetPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ActivityAssuranceAssetPropertiesEditionPart {

	protected ReferencesTable assetEvent;
	protected List<ViewerFilter> assetEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> assetEventFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ActivityAssuranceAssetPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ActivityAssuranceAssetPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence activityAssuranceAssetStep = new BindingCompositionSequence(propertiesEditionComponent);
		activityAssuranceAssetStep
			.addStep(ProcessViewsRepository.ActivityAssuranceAsset.Properties.class)
			.addStep(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent);
		
		
		composer = new PartComposer(activityAssuranceAssetStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.ActivityAssuranceAsset.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent) {
					return createAssetEventReferencesTable(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ProcessMessages.ActivityAssuranceAssetPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * 
	 */
	protected Composite createAssetEventReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.assetEvent = new ReferencesTable(getDescription(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent, ProcessMessages.ActivityAssuranceAssetPropertiesEditionPart_AssetEventLabel), new ReferencesTableListener	() {
			public void handleAdd() { addAssetEvent(); }
			public void handleEdit(EObject element) { editAssetEvent(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveAssetEvent(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromAssetEvent(element); }
			public void navigateTo(EObject element) { }
		});
		this.assetEvent.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent, ProcessViewsRepository.FORM_KIND));
		this.assetEvent.createControls(parent, widgetFactory);
		this.assetEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityAssuranceAssetPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData assetEventData = new GridData(GridData.FILL_HORIZONTAL);
		assetEventData.horizontalSpan = 3;
		this.assetEvent.setLayoutData(assetEventData);
		this.assetEvent.disableMove();
		assetEvent.setID(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent);
		assetEvent.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createAssetEventReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addAssetEvent() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(assetEvent.getInput(), assetEventFilters, assetEventBusinessFilters,
		"assetEvent", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityAssuranceAssetPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				assetEvent.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveAssetEvent(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityAssuranceAssetPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		assetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromAssetEvent(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityAssuranceAssetPropertiesEditionPartForm.this, ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		assetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void editAssetEvent(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				assetEvent.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart#initAssetEvent(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initAssetEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		assetEvent.setContentProvider(contentProvider);
		assetEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ActivityAssuranceAsset.Properties.assetEvent);
		if (eefElementEditorReadOnlyState && assetEvent.getTable().isEnabled()) {
			assetEvent.setEnabled(false);
			assetEvent.setToolTipText(ProcessMessages.ActivityAssuranceAsset_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !assetEvent.getTable().isEnabled()) {
			assetEvent.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart#updateAssetEvent()
	 * 
	 */
	public void updateAssetEvent() {
	assetEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart#addFilterAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToAssetEvent(ViewerFilter filter) {
		assetEventFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart#addBusinessFilterAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToAssetEvent(ViewerFilter filter) {
		assetEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart#isContainedInAssetEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInAssetEventTable(EObject element) {
		return ((ReferencesTableSettings)assetEvent.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.ActivityAssuranceAsset_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
