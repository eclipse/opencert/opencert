/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ActivityActivityParticipantsPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ACTIVITYPARTICIPANTS_PART = "Activity Participants"; //$NON-NLS-1$

	
	/**
	 * Settings for participant ReferencesTable
	 */
	private ReferencesTableSettings participantSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ActivityActivityParticipantsPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject activity, String editing_mode) {
		super(editingContext, activity, editing_mode);
		parts = new String[] { ACTIVITYPARTICIPANTS_PART };
		repositoryKey = ProcessViewsRepository.class;
		partKey = ProcessViewsRepository.ActivityParticipants.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Activity activity = (Activity)elt;
			final ActivityParticipantsPropertiesEditionPart activityParticipantsPart = (ActivityParticipantsPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ProcessViewsRepository.ActivityParticipants.Properties.participant)) {
				participantSettings = new ReferencesTableSettings(activity, ProcessPackage.eINSTANCE.getActivity_Participant());
				activityParticipantsPart.initParticipant(participantSettings);
			}
			// init filters
			if (isAccessible(ProcessViewsRepository.ActivityParticipants.Properties.participant)) {
				activityParticipantsPart.addFilterToParticipant(new EObjectFilter(ProcessPackage.Literals.PARTICIPANT));
				// Start of user code for additional businessfilters for participant
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}




	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ProcessViewsRepository.ActivityParticipants.Properties.participant) {
			return ProcessPackage.eINSTANCE.getActivity_Participant();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Activity activity = (Activity)semanticObject;
		if (ProcessViewsRepository.ActivityParticipants.Properties.participant == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Participant) {
					participantSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				participantSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				participantSettings.move(event.getNewIndex(), (Participant) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ActivityParticipantsPropertiesEditionPart activityParticipantsPart = (ActivityParticipantsPropertiesEditionPart)editingPart;
			if (ProcessPackage.eINSTANCE.getActivity_Participant().equals(msg.getFeature())  && isAccessible(ProcessViewsRepository.ActivityParticipants.Properties.participant))
				activityParticipantsPart.updateParticipant();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ProcessPackage.eINSTANCE.getActivity_Participant()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
