/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pam.procspec.process.Tool;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;
import org.eclipse.opencert.pam.procspec.process.parts.ToolPropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class ToolPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for ownedArtefact ReferencesTable
	 */
	private ReferencesTableSettings ownedArtefactSettings;
	
	/**
	 * Settings for triggeredAssetEvent ReferencesTable
	 */
	private ReferencesTableSettings triggeredAssetEventSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ToolPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject tool, String editing_mode) {
		super(editingContext, tool, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ProcessViewsRepository.class;
		partKey = ProcessViewsRepository.Tool.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Tool tool = (Tool)elt;
			final ToolPropertiesEditionPart basePart = (ToolPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ProcessViewsRepository.Tool.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, tool.getId()));
			
			if (isAccessible(ProcessViewsRepository.Tool.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, tool.getName()));
			
			if (isAccessible(ProcessViewsRepository.Tool.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, tool.getDescription()));
			
			if (isAccessible(ProcessViewsRepository.Tool.Properties.version))
				basePart.setVersion(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, tool.getVersion()));
			
			if (isAccessible(ProcessViewsRepository.Tool.Properties.ownedArtefact)) {
				ownedArtefactSettings = new ReferencesTableSettings(tool, ProcessPackage.eINSTANCE.getParticipant_OwnedArtefact());
				basePart.initOwnedArtefact(ownedArtefactSettings);
			}
			if (isAccessible(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent)) {
				triggeredAssetEventSettings = new ReferencesTableSettings(tool, ProcessPackage.eINSTANCE.getParticipant_TriggeredAssetEvent());
				basePart.initTriggeredAssetEvent(triggeredAssetEventSettings);
			}
			// init filters
			
			
			
			
			if (isAccessible(ProcessViewsRepository.Tool.Properties.ownedArtefact)) {
				basePart.addFilterToOwnedArtefact(new EObjectFilter(EvidencePackage.Literals.ARTEFACT));
				// Start of user code for additional businessfilters for ownedArtefact
				// End of user code
			}
			if (isAccessible(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent)) {
				basePart.addFilterToTriggeredAssetEvent(new EObjectFilter(AssuranceassetPackage.Literals.ASSURANCE_ASSET_EVENT));
				// Start of user code for additional businessfilters for triggeredAssetEvent
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}









	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ProcessViewsRepository.Tool.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == ProcessViewsRepository.Tool.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == ProcessViewsRepository.Tool.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == ProcessViewsRepository.Tool.Properties.version) {
			return ProcessPackage.eINSTANCE.getTool_Version();
		}
		if (editorKey == ProcessViewsRepository.Tool.Properties.ownedArtefact) {
			return ProcessPackage.eINSTANCE.getParticipant_OwnedArtefact();
		}
		if (editorKey == ProcessViewsRepository.Tool.Properties.triggeredAssetEvent) {
			return ProcessPackage.eINSTANCE.getParticipant_TriggeredAssetEvent();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Tool tool = (Tool)semanticObject;
		if (ProcessViewsRepository.Tool.Properties.id == event.getAffectedEditor()) {
			tool.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ProcessViewsRepository.Tool.Properties.name == event.getAffectedEditor()) {
			tool.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ProcessViewsRepository.Tool.Properties.description == event.getAffectedEditor()) {
			tool.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ProcessViewsRepository.Tool.Properties.version == event.getAffectedEditor()) {
			tool.setVersion((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ProcessViewsRepository.Tool.Properties.ownedArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Artefact) {
					ownedArtefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedArtefactSettings.move(event.getNewIndex(), (Artefact) event.getNewValue());
			}
		}
		if (ProcessViewsRepository.Tool.Properties.triggeredAssetEvent == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof AssuranceAssetEvent) {
					triggeredAssetEventSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				triggeredAssetEventSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				triggeredAssetEventSettings.move(event.getNewIndex(), (AssuranceAssetEvent) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ToolPropertiesEditionPart basePart = (ToolPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ProcessViewsRepository.Tool.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ProcessViewsRepository.Tool.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ProcessViewsRepository.Tool.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (ProcessPackage.eINSTANCE.getTool_Version().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ProcessViewsRepository.Tool.Properties.version)) {
				if (msg.getNewValue() != null) {
					basePart.setVersion(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setVersion("");
				}
			}
			if (ProcessPackage.eINSTANCE.getParticipant_OwnedArtefact().equals(msg.getFeature())  && isAccessible(ProcessViewsRepository.Tool.Properties.ownedArtefact))
				basePart.updateOwnedArtefact();
			if (ProcessPackage.eINSTANCE.getParticipant_TriggeredAssetEvent().equals(msg.getFeature())  && isAccessible(ProcessViewsRepository.Tool.Properties.triggeredAssetEvent))
				basePart.updateTriggeredAssetEvent();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			ProcessPackage.eINSTANCE.getTool_Version(),
			ProcessPackage.eINSTANCE.getParticipant_OwnedArtefact(),
			ProcessPackage.eINSTANCE.getParticipant_TriggeredAssetEvent()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (ProcessViewsRepository.Tool.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (ProcessViewsRepository.Tool.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (ProcessViewsRepository.Tool.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (ProcessViewsRepository.Tool.Properties.version == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ProcessPackage.eINSTANCE.getTool_Version().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ProcessPackage.eINSTANCE.getTool_Version().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
