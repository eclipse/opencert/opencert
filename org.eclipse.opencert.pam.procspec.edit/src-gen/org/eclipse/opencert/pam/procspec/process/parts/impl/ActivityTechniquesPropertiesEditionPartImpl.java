/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityTechniquesPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class ActivityTechniquesPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ActivityTechniquesPropertiesEditionPart {

	protected ReferencesTable technique;
	protected List<ViewerFilter> techniqueBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> techniqueFilters = new ArrayList<ViewerFilter>();



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ActivityTechniquesPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence activityTechniquesStep = new BindingCompositionSequence(propertiesEditionComponent);
		activityTechniquesStep
			.addStep(ProcessViewsRepository.ActivityTechniques.Properties.class)
			.addStep(ProcessViewsRepository.ActivityTechniques.Properties.technique);
		
		
		composer = new PartComposer(activityTechniquesStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.ActivityTechniques.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == ProcessViewsRepository.ActivityTechniques.Properties.technique) {
					return createTechniqueAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(ProcessMessages.ActivityTechniquesPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	/**
	 * 
	 */
	protected Composite createTechniqueAdvancedReferencesTable(Composite parent) {
		String label = getDescription(ProcessViewsRepository.ActivityTechniques.Properties.technique, ProcessMessages.ActivityTechniquesPropertiesEditionPart_TechniqueLabel);		 
		this.technique = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addTechnique(); }
			public void handleEdit(EObject element) { editTechnique(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveTechnique(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromTechnique(element); }
			public void navigateTo(EObject element) { }
		});
		this.technique.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.ActivityTechniques.Properties.technique, ProcessViewsRepository.SWT_KIND));
		this.technique.createControls(parent);
		this.technique.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityTechniquesPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityTechniques.Properties.technique, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData techniqueData = new GridData(GridData.FILL_HORIZONTAL);
		techniqueData.horizontalSpan = 3;
		this.technique.setLayoutData(techniqueData);
		this.technique.disableMove();
		technique.setID(ProcessViewsRepository.ActivityTechniques.Properties.technique);
		technique.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addTechnique() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(technique.getInput(), techniqueFilters, techniqueBusinessFilters,
		"technique", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityTechniquesPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityTechniques.Properties.technique,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				technique.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveTechnique(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityTechniquesPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityTechniques.Properties.technique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		technique.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromTechnique(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityTechniquesPropertiesEditionPartImpl.this, ProcessViewsRepository.ActivityTechniques.Properties.technique, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		technique.refresh();
	}

	/**
	 * 
	 */
	protected void editTechnique(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				technique.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityTechniquesPropertiesEditionPart#initTechnique(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initTechnique(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		technique.setContentProvider(contentProvider);
		technique.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.ActivityTechniques.Properties.technique);
		if (eefElementEditorReadOnlyState && technique.getTable().isEnabled()) {
			technique.setEnabled(false);
			technique.setToolTipText(ProcessMessages.ActivityTechniques_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !technique.getTable().isEnabled()) {
			technique.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityTechniquesPropertiesEditionPart#updateTechnique()
	 * 
	 */
	public void updateTechnique() {
	technique.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityTechniquesPropertiesEditionPart#addFilterTechnique(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTechnique(ViewerFilter filter) {
		techniqueFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityTechniquesPropertiesEditionPart#addBusinessFilterTechnique(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTechnique(ViewerFilter filter) {
		techniqueBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityTechniquesPropertiesEditionPart#isContainedInTechniqueTable(EObject element)
	 * 
	 */
	public boolean isContainedInTechniqueTable(EObject element) {
		return ((ReferencesTableSettings)technique.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.ActivityTechniques_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
