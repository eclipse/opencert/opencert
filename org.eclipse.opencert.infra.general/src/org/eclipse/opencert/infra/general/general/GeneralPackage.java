/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.general.general;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.infra.general.general.GeneralFactory
 * @model kind="package"
 * @generated
 */
public interface GeneralPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "general";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://general/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "general";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GeneralPackage eINSTANCE = org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.general.general.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.general.general.impl.NamedElementImpl
	 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 1;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl <em>Describable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl
	 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getDescribableElement()
	 * @generated
	 */
	int DESCRIBABLE_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBABLE_ELEMENT__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBABLE_ELEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBABLE_ELEMENT__DESCRIPTION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Describable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBABLE_ELEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Describable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBABLE_ELEMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.general.general.RequirementRelKind <em>Requirement Rel Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.general.general.RequirementRelKind
	 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getRequirementRelKind()
	 * @generated
	 */
	int REQUIREMENT_REL_KIND = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.general.general.ChangeEffectKind <em>Change Effect Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getChangeEffectKind()
	 * @generated
	 */
	int CHANGE_EFFECT_KIND = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.general.general.ActivityRelKind <em>Activity Rel Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.general.general.ActivityRelKind
	 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getActivityRelKind()
	 * @generated
	 */
	int ACTIVITY_REL_KIND = 4;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.general.general.ApplicabilityKind <em>Applicability Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.general.general.ApplicabilityKind
	 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getApplicabilityKind()
	 * @generated
	 */
	int APPLICABILITY_KIND = 5;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.general.general.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see org.eclipse.opencert.infra.general.general.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.infra.general.general.NamedElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.eclipse.opencert.infra.general.general.NamedElement#getId()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.infra.general.general.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.opencert.infra.general.general.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.general.general.DescribableElement <em>Describable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Describable Element</em>'.
	 * @see org.eclipse.opencert.infra.general.general.DescribableElement
	 * @generated
	 */
	EClass getDescribableElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.infra.general.general.DescribableElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.opencert.infra.general.general.DescribableElement#getDescription()
	 * @see #getDescribableElement()
	 * @generated
	 */
	EAttribute getDescribableElement_Description();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.infra.general.general.RequirementRelKind <em>Requirement Rel Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Requirement Rel Kind</em>'.
	 * @see org.eclipse.opencert.infra.general.general.RequirementRelKind
	 * @generated
	 */
	EEnum getRequirementRelKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.infra.general.general.ChangeEffectKind <em>Change Effect Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Change Effect Kind</em>'.
	 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
	 * @generated
	 */
	EEnum getChangeEffectKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.infra.general.general.ActivityRelKind <em>Activity Rel Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Activity Rel Kind</em>'.
	 * @see org.eclipse.opencert.infra.general.general.ActivityRelKind
	 * @generated
	 */
	EEnum getActivityRelKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.infra.general.general.ApplicabilityKind <em>Applicability Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Applicability Kind</em>'.
	 * @see org.eclipse.opencert.infra.general.general.ApplicabilityKind
	 * @generated
	 */
	EEnum getApplicabilityKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GeneralFactory getGeneralFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.general.general.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.general.general.impl.NamedElementImpl
		 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__ID = eINSTANCE.getNamedElement_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl <em>Describable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl
		 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getDescribableElement()
		 * @generated
		 */
		EClass DESCRIBABLE_ELEMENT = eINSTANCE.getDescribableElement();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBABLE_ELEMENT__DESCRIPTION = eINSTANCE.getDescribableElement_Description();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.general.general.RequirementRelKind <em>Requirement Rel Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.general.general.RequirementRelKind
		 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getRequirementRelKind()
		 * @generated
		 */
		EEnum REQUIREMENT_REL_KIND = eINSTANCE.getRequirementRelKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.general.general.ChangeEffectKind <em>Change Effect Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.general.general.ChangeEffectKind
		 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getChangeEffectKind()
		 * @generated
		 */
		EEnum CHANGE_EFFECT_KIND = eINSTANCE.getChangeEffectKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.general.general.ActivityRelKind <em>Activity Rel Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.general.general.ActivityRelKind
		 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getActivityRelKind()
		 * @generated
		 */
		EEnum ACTIVITY_REL_KIND = eINSTANCE.getActivityRelKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.general.general.ApplicabilityKind <em>Applicability Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.general.general.ApplicabilityKind
		 * @see org.eclipse.opencert.infra.general.general.impl.GeneralPackageImpl#getApplicabilityKind()
		 * @generated
		 */
		EEnum APPLICABILITY_KIND = eINSTANCE.getApplicabilityKind();

	}

} //GeneralPackage
