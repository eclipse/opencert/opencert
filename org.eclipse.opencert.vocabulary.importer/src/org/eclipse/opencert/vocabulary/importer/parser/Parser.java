/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.importer.parser;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.opencert.vocabulary.Category;
import org.eclipse.opencert.vocabulary.SourceOfDefinition;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Parser {
	public static Vocabulary parseVocabulary(InputStream stream) {
		Vocabulary result = null;
		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(stream);

			// optional, but recommended. read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			Element vocElement = doc.getDocumentElement();
			String vocName = vocElement.getAttribute("name");
			String vocDescription = vocElement.getAttribute("description");
			result = createVocabulary(vocName, vocDescription);

			String uri = vocElement.getAttribute("uri");
			SourceOfDefinition sourceOfDefinition = VocabularyFactory.eINSTANCE
					.createSourceOfDefinition();
			sourceOfDefinition.setUri(uri);
			sourceOfDefinition.setName(vocName);
			sourceOfDefinition.setDescription(vocDescription);
			result.getSourcesOfDefinition().add(sourceOfDefinition);

			Map<String, Term> namedTerms = new HashMap<String, Term>();
			NodeList catList = vocElement.getElementsByTagName("category");
			for (int i = 0; i < catList.getLength(); i++) {
				Node catNode = catList.item(i);
				if (catNode.getNodeType() == Node.ELEMENT_NODE) {
					Element catElement = (Element) catNode;
					String catName = catElement.getAttribute("name");
					String catDescription = catElement
							.getAttribute("description");
					Category cat = createCategory(catName, catDescription);
					result.getCategories().add(cat);

					NodeList termList = vocElement.getElementsByTagName("term");
					for (int j = 0; j < termList.getLength(); j++) {
						Node termNode = termList.item(j);
						if (termNode.getNodeType() == Node.ELEMENT_NODE) {
							Element termElement = (Element) termNode;
							Term term = parseTerm(termElement);
							term.setDefinedBy(sourceOfDefinition);
							cat.getTerms().add(term);
							result.getTerms().add(term);
							namedTerms.put(term.getName(), term);
						}
					}
				}
			}

			// Term relationships
			// All categories
			for (int i = 0; i < catList.getLength(); i++) {
				Node catNode = catList.item(i);
				if (catNode.getNodeType() == Node.ELEMENT_NODE) {
					NodeList termList = vocElement.getElementsByTagName("term");
					// All terms
					for (int j = 0; j < termList.getLength(); j++) {
						Node termNode = termList.item(j);
						if (termNode.getNodeType() == Node.ELEMENT_NODE) {
							Element termElement = (Element) termNode;
							Term source = namedTerms.get(termElement
									.getAttribute("name"));
							// All possible relationship types
							
							
								String isaAttrName = "isa";
								String isaAttrValue = termElement
										.getAttribute(isaAttrName);
								if (isaAttrValue != null && isaAttrValue.length() > 0) {
									String[] split = isaAttrValue.split(",");
									// Referenced element in comma-separated
									// list
									for (String s : split) {
										Term target = namedTerms.get(s);
										if (source != null && target != null){
											source.getIsA().add(target);
										}
									}
								}
								
								String hasaAttrName = "hasa";
								String hasaAttrValue = termElement
										.getAttribute(hasaAttrName);
								if (hasaAttrValue != null && hasaAttrValue.length() > 0) {
									String[] split = hasaAttrValue.split(",");
									// Referenced element in comma-separated
									// list
									for (String s : split) {
										Term target = namedTerms.get(s);
										if (source != null && target != null){
											source.getHasA().add(target);
										}
									}
								}
								
								String refersToAttrName = "refersto";
								String refersToAttrValue = termElement
										.getAttribute(refersToAttrName);
								if (refersToAttrValue != null && refersToAttrValue.length() > 0) {
									String[] split = refersToAttrValue.split(",");
									// Referenced element in comma-separated
									// list
									for (String s : split) {
										Term target = namedTerms.get(s);
										if (source != null && target != null){
											source.getRefersTo().add(target);
										}
									}
								}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	private static Term parseTerm(Element termElement) {
		Term result = VocabularyFactory.eINSTANCE.createTerm();
		String name = termElement.getAttribute("name");
		result.setName(name);

		String synonymString = termElement.getAttribute("synonym");
		if (synonymString != null && synonymString.length() > 0) {
			for (String s : synonymString.split(",")) {
				result.getSynonyms().add(s);
			}
		}

		NodeList definitionNodes = termElement
				.getElementsByTagName("definition");
		for (int i = 0; i < definitionNodes.getLength(); i++) {
			Node definitionNode = definitionNodes.item(i);
			if (definitionNode.getNodeType() == Node.ELEMENT_NODE) {
				Element definitionElement = (Element) definitionNode;
				String definition = definitionElement.getTextContent();
				result.getDefinitions().add(definition);
			}
		}

		NodeList noteNodes = termElement.getElementsByTagName("note");
		for (int i = 0; i < noteNodes.getLength(); i++) {
			Node noteNode = noteNodes.item(i);
			if (noteNode.getNodeType() == Node.ELEMENT_NODE) {
				Element noteElement = (Element) noteNode;
				String note = noteElement.getTextContent();
				result.getNotes().add(note);
			}
		}

		NodeList exampleNodes = termElement.getElementsByTagName("example");
		for (int i = 0; i < exampleNodes.getLength(); i++) {
			Node exampleNode = exampleNodes.item(i);
			if (exampleNode.getNodeType() == Node.ELEMENT_NODE) {
				Element exampleElement = (Element) exampleNode;
				String example = exampleElement.getTextContent();
				result.getExamples().add(example);
			}
		}

		return result;
	}

	private static Vocabulary createVocabulary(String vocName,
			String vocDescription) {
		Vocabulary result = VocabularyFactory.eINSTANCE.createVocabulary();
		result.setName(vocName);
		result.setDescription(vocDescription);
		return result;
	}

	private static Category createCategory(String catName, String catDescription) {
		Category result = VocabularyFactory.eINSTANCE.createCategory();
		result.setName(catName);
		result.setDescription(catDescription);
		return result;
	}

}
