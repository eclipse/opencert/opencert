/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.util.List;
import org.eclipse.uml2.uml.Component;
import org.junit.Test;

public class ElasticClientTest {

	@Test
	public void testPing() throws Exception {
		System.out.println("Send a ping to local Elastic server...");
		ElasticClientImpl.on("localhost", 9200, "http").ping().close();
		System.out.println("Done. Server seems to be alive.");
	}

	@Test
	public void testSend() throws Exception {
		Component object = TestData.createComponent();
		ElasticDocument document = EObjectToDocument.INSTANCE.convert(object, "client-test");
		ElasticClientImpl.on("localhost", 9200, "http").store(document).close();
	}

	@Test
	public void testDelete() throws Exception {
		ElasticClientImpl.on("localhost", 9200, "http").delete("client-test").close();
	}
	
	@Test
	public void testSendMany() throws Exception {
		List<ElasticDocument> documents = EObjectToDocument.INSTANCE.convert(TestData.uml(50), "client-test");
		ElasticClientImpl.on("localhost", 9200, "http").storeAll(documents.iterator()).close();
	}
}
