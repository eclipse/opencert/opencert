/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes.impl;

import claimtypes.ClaimType;
import claimtypes.ClaimTypeRepository;
import claimtypes.ClaimtypesFactory;
import claimtypes.ClaimtypesPackage;
import claimtypes.SentenceStructure;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClaimtypesPackageImpl extends EPackageImpl implements ClaimtypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass claimTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass claimTypeRepositoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sentenceStructureEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see claimtypes.ClaimtypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClaimtypesPackageImpl() {
		super(eNS_URI, ClaimtypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ClaimtypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClaimtypesPackage init() {
		if (isInited) return (ClaimtypesPackage)EPackage.Registry.INSTANCE.getEPackage(ClaimtypesPackage.eNS_URI);

		// Obtain or create and register package
		ClaimtypesPackageImpl theClaimtypesPackage = (ClaimtypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ClaimtypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ClaimtypesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		GeneralPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theClaimtypesPackage.createPackageContents();

		// Initialize created meta-data
		theClaimtypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClaimtypesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClaimtypesPackage.eNS_URI, theClaimtypesPackage);
		return theClaimtypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClaimType() {
		return claimTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClaimType_SentenceStructures() {
		return (EReference)claimTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClaimType_Color() {
		return (EAttribute)claimTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClaimTypeRepository() {
		return claimTypeRepositoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClaimTypeRepository_ClaimTypes() {
		return (EReference)claimTypeRepositoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSentenceStructure() {
		return sentenceStructureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSentenceStructure_Content() {
		return (EAttribute)sentenceStructureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClaimtypesFactory getClaimtypesFactory() {
		return (ClaimtypesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		claimTypeEClass = createEClass(CLAIM_TYPE);
		createEReference(claimTypeEClass, CLAIM_TYPE__SENTENCE_STRUCTURES);
		createEAttribute(claimTypeEClass, CLAIM_TYPE__COLOR);

		claimTypeRepositoryEClass = createEClass(CLAIM_TYPE_REPOSITORY);
		createEReference(claimTypeRepositoryEClass, CLAIM_TYPE_REPOSITORY__CLAIM_TYPES);

		sentenceStructureEClass = createEClass(SENTENCE_STRUCTURE);
		createEAttribute(sentenceStructureEClass, SENTENCE_STRUCTURE__CONTENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GeneralPackage theGeneralPackage = (GeneralPackage)EPackage.Registry.INSTANCE.getEPackage(GeneralPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		claimTypeEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		claimTypeRepositoryEClass.getESuperTypes().add(theGeneralPackage.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(claimTypeEClass, ClaimType.class, "ClaimType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClaimType_SentenceStructures(), this.getSentenceStructure(), null, "sentenceStructures", null, 0, -1, ClaimType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClaimType_Color(), ecorePackage.getEString(), "color", null, 0, 1, ClaimType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(claimTypeRepositoryEClass, ClaimTypeRepository.class, "ClaimTypeRepository", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClaimTypeRepository_ClaimTypes(), this.getClaimType(), null, "claimTypes", null, 0, -1, ClaimTypeRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sentenceStructureEClass, SentenceStructure.class, "SentenceStructure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSentenceStructure_Content(), ecorePackage.getEString(), "content", null, 0, 1, SentenceStructure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ClaimtypesPackageImpl
