/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class SimpleClient
{
    private static final int _SLEEP = 100;

    public static void main(String[] args) throws InterruptedException
    {
        SimpleClient simpleClient = new SimpleClient();
        int iteration = 1;
        while (true) {
            long startTime = System.currentTimeMillis();
            simpleClient.executeGetRequest("http://77.252.162.49:8080/reportSimulator/services/rest/artefacts/baseElements");
            Thread.sleep(_SLEEP);
            //simpleClient.executeGetRequest("http://10.9.1.239:8080/org.eclipse.opencert.report?iteration=" + iteration);
            //Thread.sleep(_SLEEP);
//            simpleClient.executeGetRequest("http://77.252.162.49:8080/opencert-qm/services/rest/artefactRels");
//            Thread.sleep(_SLEEP);
//            simpleClient.executeGetRequest("http://77.252.162.49:8080/opencert-qm/services/rest/artefacts/5");
//            Thread.sleep(_SLEEP);
//            simpleClient.executeGetRequest("http://77.252.162.49:8080/opencert-qm/services/rest/artefacts/4144/artefactRels");
//            Thread.sleep(_SLEEP);
//            simpleClient.executeGetRequest("http://77.252.162.49:8080/opencert-qm/services/rest/artefacts/4144/artefactParts");
//            Thread.sleep(_SLEEP);
//            simpleClient.executeGetRequest("http://77.252.162.49:8080/opencert-qm/services/rest/artefactRels/4131");
//            Thread.sleep(_SLEEP);
//            simpleClient.executeGetRequest("http://77.252.162.49:8080/opencert-qm/services/rest/artefactRels/4131/source");
//            Thread.sleep(_SLEEP);
              long endTime = System.currentTimeMillis();
            System.out.println("Iteration: " + iteration++ + ", total execution time: " + (float)(endTime-startTime)/1000 + "s");
        } 
    }
    
    private void executeGetRequest(String url) 
    {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            
            HttpGet getRequest = new HttpGet(url);
            //getRequest.addHeader("accept", "application/json");
            getRequest.addHeader("Cache-Control", "no-cache");
            
            HttpResponse response = httpClient.execute(getRequest);
//            if (response.getStatusLine().getStatusCode() != 200) {
//                throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
//            }
            InputStreamReader inputStreamReader = new InputStreamReader((response.getEntity().getContent()));
            BufferedReader br = new BufferedReader(inputStreamReader);
            
            String output;
            //System.out.print(".");
            
            long length = 0;
            while ((output = br.readLine()) != null) {
                System.out.println("    " + output);
                length += output.length();
            }
            
            if (length == 0) {
                System.out.println("Prevent JVM from optimization :)");
            }
            
            httpClient.getConnectionManager().shutdown();
            httpClient.close();
            inputStreamReader.close();
            br.close();
            
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
