/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors: 
 *   Muhammad Atif Javed
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.lines.handlers;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.epf.export.services.PluginExportService;
import org.eclipse.epf.library.LibraryService;
import org.eclipse.epf.library.project.MethodLibraryProject;
import org.eclipse.epf.uma.MethodLibrary;
import org.eclipse.ui.PlatformUI;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.opencert.lines.resolving.DoResolve;
import org.eclipse.opencert.lines.wizards.OpenLibraryWizard;
import org.eclipse.swt.widgets.Display;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class LinesHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) {

		OpenLibraryWizard wizard = new OpenLibraryWizard();
		wizard.init(PlatformUI.getWorkbench(), null);

		// Instantiate the wizard container with the wizard and open it.
		WizardDialog dialog = new WizardDialog(Display.getCurrent()
				.getActiveShell(), wizard);
		dialog.create();
		dialog.open();
		
		MethodLibrary library = LibraryService.getInstance().getCurrentMethodLibrary();
		String libPath = LibraryService.getInstance().getCurrentMethodLibraryLocation();
		String copyPath = libPath+"\\"+"error_free_models";
		
		File targetFile = new File(copyPath);
		if(!targetFile.exists()){
			PluginExportService.copyDir(new File(libPath), new File(copyPath));
			try {
				DoResolve.Resolve(copyPath);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		IProject project = MethodLibraryProject.findProject(library);
		try {
			project.refreshLocal(IResource.FOLDER, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
