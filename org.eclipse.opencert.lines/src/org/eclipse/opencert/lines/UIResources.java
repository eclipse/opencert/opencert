package org.eclipse.opencert.lines;

import org.eclipse.osgi.util.NLS;

/**
 * UI resources.
 * 
 */
public class UIResources extends NLS {

	private static String BUNDLE_NAME = UIResources.class.getPackage()
			.getName()
			+ ".Resources"; //$NON-NLS-1$

	public static String pathLabel_text;
	public static String browseButton_text;
	
	public static String openLibraryWizard_title;
	public static String openLibraryWizardPage_title;
	public static String openLibraryMainWizardPage_title;
	public static String openLibraryMainWizardPage_title_2;
	public static String openLibraryDialog_newLibrary_text;
	
	public static String nameLabel_text;
	public static String uriLabel_text;
	public static String openUnlistedLibraryCheckbox_text;
	public static String libraryTypeLabel_text;
	public static String libraryLabel_text;

	public static String openLibraryError_msg;
	public static String versionMismatchDialog_text;
	public static String versionMismatchDialog_text_unknown;
	
	public static String openLibraryError_advice;
	public static String openLibraryError_reason4;
	public static String openLibraryInternlError_reason;
	
	public static String convertToSynProcessLib_msg;
	public static String upgradeLibraryDialog_text;

	static {
		NLS.initializeMessages(BUNDLE_NAME, UIResources.class);
	}

	/**
	 * Returns the localized string associated with a resource key and formatted
	 * with a given object.
	 * 
	 * @param key
	 *            A resource key.
	 * @param data
	 *            An object.
	 * @return A formatted localized string.
	 */
	public static String bind(String key, Object data) {
		return NLS.bind(key, new Object[] { data });
	}

}