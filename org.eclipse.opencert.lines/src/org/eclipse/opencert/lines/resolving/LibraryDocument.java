package org.eclipse.opencert.lines.resolving;

import java.io.File;

import org.eclipse.epf.common.utils.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * Encapsulates a method library using a DOM document.
 */
public class LibraryDocument {

	public static final String TAG_resourceDescriptors = "resourceDescriptors"; //$NON-NLS-1$
	
	protected File libFile;

	protected Document document;

	protected Element libTag = null;

	protected Element resTag = null;

	/**
	 * Creates a new instance.
	 */
	public LibraryDocument(File libFile) throws Exception {
		this.libFile = libFile;
		init();
	}

	protected void init() throws Exception {
		this.document = XMLUtil.loadXml(libFile);

		Element root = document.getDocumentElement();
		NodeList nodes = root.getElementsByTagName("org.eclipse.epf.uma:MethodLibrary"); //$NON-NLS-1$
		if (nodes != null && nodes.getLength() > 0) {
			libTag = (Element) nodes.item(0);
		}

		nodes = root
				.getElementsByTagName("org.eclipse.epf.uma.resourcemanager:ResourceManager"); //$NON-NLS-1$
		if (nodes != null && nodes.getLength() > 0) {
			resTag = (Element) nodes.item(0);
		}
	}

    /**
     * Returns resource descriptor node list
     */
	public NodeList getResourceDescriptors() {
		return resTag.getElementsByTagName(TAG_resourceDescriptors);
	}
}
