/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.aida.tracemodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.capra.core.CapraException;
import org.eclipse.capra.core.adapters.TraceLinkAdapter;
import org.eclipse.capra.core.util.TraceLinkAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.AnalysisContextArtefactLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelFactory;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

/**
 * This adapter provides access to ContractArtefactLink traces.
 * 
 * @author Stefano Puri
 */
public class AnalysisContextArtefactLinkAdapter implements TraceLinkAdapter {

	public static final String ANALYSISCONTEXT_ARTEFACT_LINK_TYPE = "AnalysisContext-Artefact Trace";

	@Override
	public boolean canAdapt(EClass traceType) {
		if (traceType.equals(OpenCertTraceLinkMetaModelPackage.eINSTANCE.getAnalysisContextArtefactLink())) {
			return true;
		}
		return false;
	}

	@Override
	public List<EObject> getSources(EObject trace) {
		assert trace instanceof AnalysisContextArtefactLink;
		AnalysisContextArtefactLink contractTraceLink = (AnalysisContextArtefactLink) trace;
		List<EObject> context = new ArrayList<EObject>();
		context.add(contractTraceLink.getSources());
		return context;
	}

	@Override
	public List<EObject> getTargets(EObject trace) {
		assert trace instanceof AnalysisContextArtefactLink;
		AnalysisContextArtefactLink genericTraceLink = (AnalysisContextArtefactLink) trace;
		List<EObject> arte = new ArrayList<EObject>();
		for (Artefact c : genericTraceLink.getTargets()) {
			arte.add(c);
		}
		return arte;
	}

	@Override
	public String getLinkType() {
		return ANALYSISCONTEXT_ARTEFACT_LINK_TYPE;
	}

	@Override
	public boolean canCreateLink(List<EObject> sources, List<EObject> targets) {
		if (sources.isEmpty() || targets.isEmpty())
			return false;
		for (EObject obj : sources) {
			if (!(obj instanceof org.eclipse.uml2.uml.Class))
				return false;
		}
		for (EObject obj : targets) {
			if (!(obj instanceof Artefact))
				return false;
		}
		return true;
	}

	
	public EObject createLink(List<EObject> sources, List<EObject> targets)  {
		AnalysisContextArtefactLink traceLink = OpenCertTraceLinkMetaModelFactory.eINSTANCE
				.createAnalysisContextArtefactLink();
		traceLink.setSources((org.eclipse.uml2.uml.Class) sources.get(0));

		for (EObject obj : targets) {
			traceLink.getTargets().add((Artefact) obj);
		}

		return traceLink;
	}

	@Override
	public EObject createLink(List<EObject> sources, List<EObject> targets, List<TraceLinkAttribute> attributes)
			throws CapraException {
		return this.createLink(sources, targets);
	}

	@Override
	public List<TraceLinkAttribute> getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabelForEObject(EObject eobject) {
		// TODO Auto-generated method stub
		return null;
	}
}
