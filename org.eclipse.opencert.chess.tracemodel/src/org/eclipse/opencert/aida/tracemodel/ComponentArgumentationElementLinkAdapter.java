/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.aida.tracemodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.capra.core.CapraException;
import org.eclipse.capra.core.adapters.TraceLinkAdapter;
import org.eclipse.capra.core.util.TraceLinkAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelFactory;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.polarsys.chess.contracts.profile.chesscontract.util.ContractEntityUtil;

/**
 * This adapter provides access to ComponentArgumentationElementLink traces.
 * 
 * @author Stefano Puri
 */
public class ComponentArgumentationElementLinkAdapter implements TraceLinkAdapter {

	public static final String COMPONENT_ARGUMENTATIONELEMENT_LINK_TYPE = "Component-ArgumentationElement Trace";

	@Override
	public boolean canAdapt(EClass traceType) {
		if (traceType.equals(OpenCertTraceLinkMetaModelPackage.eINSTANCE.getComponentArgumentationElementLink())) {
			return true;
		}
		return false;
	}

	@Override
	public List<EObject> getSources(EObject trace) {
		assert trace instanceof ComponentArgumentationElementLink;
		ComponentArgumentationElementLink contractTraceLink = (ComponentArgumentationElementLink) trace;
		List<EObject> comps = new ArrayList<EObject>();
		comps.add(contractTraceLink.getSources());
		return comps;
	}

	@Override
	public List<EObject> getTargets(EObject trace) {
		assert trace instanceof ComponentArgumentationElementLink;
		ComponentArgumentationElementLink genericTraceLink = (ComponentArgumentationElementLink) trace;
		List<EObject> claims = new ArrayList<EObject>();
		for (ArgumentationElement c : genericTraceLink.getTargets()) {
			claims.add(c);
		}
		return claims;
	}

	@Override
	public String getLinkType() {
		return COMPONENT_ARGUMENTATIONELEMENT_LINK_TYPE;
	}

	@Override
	public boolean canCreateLink(List<EObject> sources, List<EObject> targets) {
		if (sources.isEmpty() || targets.isEmpty())
			return false;
		for (EObject obj : sources) {
			if (!(obj instanceof Class) || !(obj instanceof Element) || ContractEntityUtil.getInstance().isContract((Element) obj))
				return false;
		}
		for (EObject obj : targets) {
			if (!(obj instanceof ArgumentationElement))
				return false;
		}
		return true;
	}

	
	public EObject createLink(List<EObject> sources, List<EObject> targets) {
		ComponentArgumentationElementLink traceLink = OpenCertTraceLinkMetaModelFactory.eINSTANCE
				.createComponentArgumentationElementLink();
		traceLink.setSources((Class) sources.get(0));

		for (EObject obj : targets) {
			traceLink.getTargets().add((ArgumentationElement) obj);
		}

		return traceLink;
	}

	@Override
	public EObject createLink(List<EObject> sources, List<EObject> targets, List<TraceLinkAttribute> attributes)
			throws CapraException {
		
		return this.createLink(sources, targets);
	}

	@Override
	public List<TraceLinkAttribute> getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabelForEObject(EObject eobject) {
		if (eobject instanceof Class)
			return ((Class)eobject).getName();
		return eobject.toString();
	}
}
