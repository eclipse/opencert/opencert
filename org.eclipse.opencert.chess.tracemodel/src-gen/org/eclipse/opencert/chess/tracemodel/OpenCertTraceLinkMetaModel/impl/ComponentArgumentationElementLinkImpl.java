/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.ComponentArgumentationElementLink;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.OpenCertTraceLinkMetaModelPackage;

import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Argumentation Element Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ComponentArgumentationElementLinkImpl#getSources <em>Sources</em>}</li>
 *   <li>{@link org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ComponentArgumentationElementLinkImpl#getTargets <em>Targets</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentArgumentationElementLinkImpl extends MinimalEObjectImpl.Container implements ComponentArgumentationElementLink {
	/**
	 * The cached value of the '{@link #getSources() <em>Sources</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSources()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class sources;

	/**
	 * The cached value of the '{@link #getTargets() <em>Targets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargets()
	 * @generated
	 * @ordered
	 */
	protected EList<ArgumentationElement> targets;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentArgumentationElementLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OpenCertTraceLinkMetaModelPackage.Literals.COMPONENT_ARGUMENTATION_ELEMENT_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getSources() {
		if (sources != null && sources.eIsProxy()) {
			InternalEObject oldSources = (InternalEObject)sources;
			sources = (org.eclipse.uml2.uml.Class)eResolveProxy(oldSources);
			if (sources != oldSources) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES, oldSources, sources));
			}
		}
		return sources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetSources() {
		return sources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSources(org.eclipse.uml2.uml.Class newSources) {
		org.eclipse.uml2.uml.Class oldSources = sources;
		sources = newSources;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES, oldSources, sources));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ArgumentationElement> getTargets() {
		if (targets == null) {
			targets = new EObjectResolvingEList<ArgumentationElement>(ArgumentationElement.class, this, OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS);
		}
		return targets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES:
				if (resolve) return getSources();
				return basicGetSources();
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS:
				return getTargets();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES:
				setSources((org.eclipse.uml2.uml.Class)newValue);
				return;
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS:
				getTargets().clear();
				getTargets().addAll((Collection<? extends ArgumentationElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES:
				setSources((org.eclipse.uml2.uml.Class)null);
				return;
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS:
				getTargets().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__SOURCES:
				return sources != null;
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK__TARGETS:
				return targets != null && !targets.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComponentArgumentationElementLinkImpl
