/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.labelparser.tokens;

public class HighlightItem implements Token {
	public static enum ItemHeaderType {
		none, id, uri, path, var, voc;

		public static ItemHeaderType parse(String s) {
			ItemHeaderType result = null;
			try {
				result = ItemHeaderType.valueOf(s.toLowerCase());
			} catch (Exception e) {
			} finally {
				if (result == null) {
					result = none;
				}
			}
			return result;
		}
	}

	public static enum ItemBodyType {
		unquoted, singlequoted, doublequoted;
	}

	private String rawString;
	private String body;
	private String representation;
	private ItemHeaderType headerType;
	private ItemBodyType bodyType;
	private int startIndex;

	public HighlightItem(String rawString, String body, String representation,
			ItemHeaderType headerType, ItemBodyType bodyType, int startIndex) {
		this.rawString = rawString;
		this.body = body;
		this.representation = representation;
		this.headerType = headerType;
		this.bodyType = bodyType;
		this.startIndex = startIndex;
	}

	public String getRawString() {
		return rawString;
	}

	public String getBody() {
		return body;
	}

	@Override
	public String getRepresentation() {
		return representation;
	}

	public ItemHeaderType getHeaderType() {
		return headerType;
	}

	public ItemBodyType getBodyType() {
		return bodyType;
	}

	@Override
	public int getStartIndex() {
		return startIndex;
	}
}
