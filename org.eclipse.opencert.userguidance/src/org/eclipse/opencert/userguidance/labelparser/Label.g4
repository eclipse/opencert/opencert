// Examples 
// voc:child|children
// We care about voc:"health care".



grammar Label;

options {
  language = Java;
}

@parser::header {
package org.eclipse.opencert.userguidance.labelparser.generated;
}

@lexer::header {
package org.eclipse.opencert.userguidance.labelparser.generated;
}

@parser::member {

}

// Parser rules

item : SPECIFIER SEPARATOR itembody;

itembody :
  (WORD | SEPARATOR)* ('|' (WORD | SEPARATOR)*)? |
  (SQ (WS | WORD | SEPARATOR)* ('|' (WS | WORD | SEPARATOR)*)? SQ)  |
  (DQ (WS | WORD | SEPARATOR)* ('|' (WS | WORD | SEPARATOR)*)? DQ);

// text : ((WS* item | WORD | SEPARATOR) (WS+ ( item | WORD | SEPARATOR))* )? WS* ;
text : (WS | item | WORD | SEPARATOR)*;

// Lexer rules

SEPARATOR : ':';

SQ : '\'';

DQ : '\"';

WS : (' ' | '\r' | '\t' | '\u000C' | '\n');

SPECIFIER : 'id' | 'uri' | 'path' | 'var' | 'voc';

WORD: ~(' ' | '\r' | '\t' | '\u000C' | '\n' | '\"' | ':' | '|')+;