/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.hover;

import java.awt.MouseInfo;
import java.util.List;

import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.opencert.userguidance.label.LabelHighlighter;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem;
import org.eclipse.opencert.userguidance.util.HitTestUtil;
import org.eclipse.opencert.userguidance.util.VocabFetcher;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Shell;

public class ToolTip extends MouseTrackAdapter implements MouseMoveListener, MouseListener {
	private DiagramEditor editor;
	private StyledText textLabel;
	private Shell shell;

	public ToolTip(DiagramEditor editor) {
		this.editor = editor;

		editor.getDiagramGraphicalViewer().getControl().addMouseTrackListener(this);
		editor.getDiagramGraphicalViewer().getControl().addMouseMoveListener(this);
		editor.getDiagramGraphicalViewer().getControl().addMouseListener(this);
	}

	@Override
	public void mouseHover(MouseEvent e) {
		final HighlightItem item = HitTestUtil.getHighlightItem(editor, e.x, e.y);
		if (item == null) {
			return;
		}
		final String[] itemDefinition = new String[1];
		// Look up the item in all vocabularies
		VocabFetcher fetcher = new VocabFetcher() {
			@Override
			public void readFromVocab(List<Vocabulary> vocabularies) {
				for (Vocabulary vocabulary : vocabularies) {
					if (vocabulary != null) {
						for (Term t : vocabulary.getTerms()) {
							if (t.getName().toLowerCase().equals(item.getBody().toLowerCase())) {
								itemDefinition[0] = createDescription(t);
								break;
							}
						}
						if (itemDefinition != null) {
							break;
						}
					}
				}
			}
		};
		fetcher.execute();

		if (itemDefinition[0] == null) {
			return;
		}

		// Create shell to display the tooltip
		shell = new Shell(editor.getDiagramGraphicalViewer().getControl().getShell(), SWT.TOOL | SWT.ON_TOP);
		shell.setLayout(new GridLayout());
		shell.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
		shell.setBackgroundMode(SWT.INHERIT_FORCE);
		FillLayout fillLayout = new FillLayout();
		fillLayout.type = SWT.VERTICAL;
		fillLayout.marginHeight = 10;
		fillLayout.marginWidth = 10;
		shell.setLayout(fillLayout);

		// Create label inside shell
		textLabel = new StyledText(shell, SWT.WRAP);
		LabelHighlighter highlighter = new LabelHighlighter(itemDefinition[0], textLabel.getFont(),
				textLabel.getFont());
		highlighter.calculateHighlighting();
		textLabel.setText(highlighter.getOutputText());
		textLabel.setStyleRanges(highlighter.getStylesArray());

		// Set size and position of shell and label
		org.eclipse.swt.graphics.Point textLabelSize = textLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		if (textLabelSize.x > 500) {
			textLabelSize = textLabel.computeSize(500, SWT.DEFAULT);
		}
		int x = MouseInfo.getPointerInfo().getLocation().x;
		int y = MouseInfo.getPointerInfo().getLocation().y + shell.getDisplay().getCursorSizes()[0].y - 6;
		int width = textLabelSize.x + 22;
		int height = textLabelSize.y + 22;

		int screenWidth = shell.getDisplay().getBounds().x + shell.getDisplay().getBounds().width;
		int screenHeight = shell.getDisplay().getBounds().y + shell.getDisplay().getBounds().height;
		if (width > screenWidth - 10) {
			width = screenWidth - 10;
		}
		if (x + width > screenWidth - 5) {
			x = screenWidth - width - 5;
		}
		if (y + height > screenHeight - 5) {
			y = MouseInfo.getPointerInfo().getLocation().y - height - 5;
		}
		shell.setBounds(x, y, width, height);
		shell.setVisible(true);
	}

	@Override
	public void mouseMove(MouseEvent e) {
		disposeShell();
	}

	@Override
	public void mouseDoubleClick(MouseEvent e) {
		disposeShell();
	}

	@Override
	public void mouseDown(MouseEvent e) {
		disposeShell();
	}

	@Override
	public void mouseUp(MouseEvent e) {
		disposeShell();
	}

	private void disposeShell() {
		if (textLabel != null && !textLabel.isDisposed()) {
			textLabel.dispose();
		}

		if (shell != null && !shell.isDisposed()) {
			shell.dispose();
		}
	}

	private String createDescription(Term t) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < t.getDefinitions().size(); i++) {
			if (i > 0) {
				result.append("\n\n");
			}
			result.append(t.getDefinitions().get(i));
		}
		return result.toString();
	}
}