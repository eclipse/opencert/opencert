/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.label;

import org.eclipse.swt.graphics.Font;
import org.eclipse.opencert.userguidance.labelparser.Highlighter;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem.ItemHeaderType;

public class LabelHighlighter extends Highlighter {
	public LabelHighlighter(String input, Font normalFont, Font boldFont) {
		super(input, normalFont, boldFont);
	}

	@Override
	protected String getRepresentation(HighlightItem item) {
		String result = item.getRepresentation();

		if (item.getHeaderType() == ItemHeaderType.var) {
			result = "{" + result + "}";
		}

		return result;
	}
}
