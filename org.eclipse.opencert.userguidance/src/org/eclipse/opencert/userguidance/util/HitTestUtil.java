/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.userguidance.label.WrappingLabel2;
import org.eclipse.opencert.userguidance.labelparser.tokens.HighlightItem;
import org.eclipse.opencert.userguidance.labelparser.tokens.Token;

public class HitTestUtil {
	public static HighlightItem getHighlightItemScreen(DiagramEditor editor,
			int screenX, int screenY) {
		Point editorLocation = editor.getDiagramGraphicalViewer().getControl().toDisplay(0, 0);
		int mouseEditorLocationX = screenX - editorLocation.x;
		int mouseEditorLocationY = screenY - editorLocation.y;

		return HitTestUtil.getHighlightItem(editor, mouseEditorLocationX,
				mouseEditorLocationY);
	}

	/**
	 * Finds Token in WrappingLabel2 at the mouse location in control
	 * coordinates.
	 */
	public static HighlightItem getHighlightItem(DiagramEditor editor,
			int controlX, int controlY) {
		EditPart ep = editor.getDiagramGraphicalViewer().findObjectAt(
				new org.eclipse.draw2d.geometry.Point(controlX, controlY));
		if (ep instanceof GraphicalEditPart) {
			GraphicalEditPart gep = (GraphicalEditPart) ep;

			Rectangle diagramBounds = ((DiagramEditPart) ep.getRoot()
					.getChildren().get(0)).getFigure().getBounds();
			FigureCanvas canvas = (FigureCanvas) editor
					.getDiagramGraphicalViewer().getControl();
			int scrollYOffset = canvas.getVerticalBar().getSelection();
			int scrollXOffset = canvas.getHorizontalBar().getSelection();
			int diagX = controlX + scrollXOffset + diagramBounds.x;
			int diagY = controlY + scrollYOffset + diagramBounds.y;

			WrappingLabel2 label = findWrappingLabel(gep, diagX, diagY);

			if (label != null) {
				Rectangle labelBounds = label.getBounds();
				Token token = label.getTokenAt(diagX - labelBounds.x, diagY
						- labelBounds.y);
				if (token instanceof HighlightItem) {
					return (HighlightItem) token;
				}
			}
		}

		return null;
	}

	private static WrappingLabel2 findWrappingLabel(GraphicalEditPart shape,
			int diagX, int diagY) {
		List<WrappingLabel2> labels = new ArrayList<WrappingLabel2>();
		getWrappingLabels(shape.getFigure(), labels);

		WrappingLabel2 result = null;
		for (WrappingLabel2 label : labels) {
			Rectangle labelBounds = label.getBounds();

			if (diagX > labelBounds.x
					&& diagX < labelBounds.x + labelBounds.width) {
				if (diagY > labelBounds.y
						&& diagY < labelBounds.y + labelBounds.height) {
					result = label;
					break;
				}
			}
		}

		return result;
	}

	private static void getWrappingLabels(IFigure figure,
			List<WrappingLabel2> result) {
		if (figure instanceof WrappingLabel2) {
			result.add((WrappingLabel2) figure);
		} else {
			for (Object obj : figure.getChildren()) {
				if (obj instanceof IFigure) {
					getWrappingLabels((IFigure) obj, result);
				}
			}
		}
	}
}
