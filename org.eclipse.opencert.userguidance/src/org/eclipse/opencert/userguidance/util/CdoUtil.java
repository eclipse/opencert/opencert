/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

public class CdoUtil {

	/**
	 * <code>@SuppressWarnings("unchecked")
		List<Vocabulary> vocabularies = (List<Ontology>) (List<?>) CdoUtil.fetch(VocabularyPackage.eINSTANCE.getVocabulary(), false);</code>
	 */
	public static List<EObject> fetch(CdoConnection connection, EClass cl,
			boolean readonly) {
		List<EObject> result = new ArrayList<EObject>();
		List<CDOResourceNode> nodes = getResources(connection, readonly);
		EPackage pack = cl.getEPackage();
		String packageName = pack.getName();
		getModels(nodes, result, "." + packageName);

		return result;
	}

	public static List<CDOResourceNode> getResources(CdoConnection connection,
			boolean readonly) {
		List<CDOResourceNode> result = null;
		if (readonly) {
			CDOView view = connection.getView("get CDO resources in a view");
			result = Arrays.asList(view.getElements());
		} else {
			CDOTransaction transaction = connection
					.getTransaction("get resources in a transaction");
			result = Arrays.asList(transaction.getElements());
		}

		return result;
	}

	private static void getModels(List<CDOResourceNode> nodes,
			List<EObject> result, String extension) {
		for (CDOResourceNode node : nodes) {
			if (node instanceof CDOResourceFolder) {
				CDOResourceFolder folder = (CDOResourceFolder) node;
				getModels(folder.getNodes(), result, extension);
			} else if (node instanceof CDOResource) {
				CDOResource resource = (CDOResource) node;
				String name = resource.getName();
				if (name.endsWith(extension)) {
					result.addAll(resource.eContents());
				}
			}

		}
	}
}
