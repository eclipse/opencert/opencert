/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.osgi.service.datalocation.Location;

class FileUtil {
	public static List<EObject> fetch(String extension) {
		List<EObject> result = new ArrayList<EObject>();
		List<IFile> files = findInWorkspace(extension);
		for (IFile file : files) {
			List<EObject> fileContent = loadResource(file);
			result.addAll(fileContent);
		}
		return result;
	}

	private static List<IFile> findInWorkspace(String extension) {
		List<IFile> dictFiles = new ArrayList<IFile>();
		Location workspaceLocation = Platform.getInstanceLocation();
		if (workspaceLocation.isSet()) {
			IWorkspaceRoot workspace = ResourcesPlugin.getWorkspace().getRoot();
			try {
				findFiles(dictFiles, workspace, extension);
			} catch (CoreException e) {
				System.out.println(e);
			}
		}

		return dictFiles;
	}

	private static void findFiles(List<IFile> files, IResource f,
			String fileType) throws CoreException {
		if (f instanceof IFile) {
			if (f.getFileExtension().toLowerCase()
					.equals(fileType.toLowerCase())) {
				files.add((IFile) f);
			}
		} else if (f instanceof IContainer) {
			for (IResource child : ((IContainer) f).members()) {
				findFiles(files, child, fileType);
			}
		}
	}

	private static List<EObject> loadResource(IFile file) {
		List<EObject> result = null;
		String path = file.getLocation().toString();

		XMIResourceImpl resource = new XMIResourceImpl();
		File source = new File(path);
		try {
			resource.load(new FileInputStream(source),
					new HashMap<Object, Object>());
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}

		result = resource.getContents();
		return result;
	}
}
