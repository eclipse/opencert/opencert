/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.celleditor;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.diagram.ui.label.ILabelDelegate;
import org.eclipse.gmf.runtime.gef.ui.internal.parts.CellEditorEx;
import org.eclipse.gmf.tooling.runtime.directedit.TextDirectEditManager2;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Composite;

@SuppressWarnings("restriction")
public class TextDirectEditManager3 extends TextDirectEditManager2 {

	private EObject eobj;
	private int featureID;

	public TextDirectEditManager3(GraphicalEditPart source,
			Class<?> editorType, CellEditorLocator locator, EObject eobj,
			int featureID) {
		super(source, editorType, locator);

		this.eobj = eobj;
		this.featureID = featureID;
	}

	@Override
	public void setEditText(String toEdit) {
		// Get the cell editor
		CellEditor cellEditor = getCellEditor();

		// IF the cell editor doesn't exist yet...
		if (cellEditor == null) {
			// Do nothing
			return;
		}

		// Get the Text Compartment Edit Part
		ITextAwareEditPart textEP = (ITextAwareEditPart) getEditPart();

		// Set the Figures text
		textEP.setLabelText(toEdit);

		// See RATLC00522324
		if (cellEditor instanceof CellEditorEx) {
			((CellEditorEx) cellEditor).setValueAndProcessEditOccured(toEdit);
		} else {
			cellEditor.setValue(toEdit);
		}

		// Get the Text control
		StyledText textControl = (StyledText) cellEditor.getControl();

		// Set the controls text and position the caret at the end of the text
		textControl.setSelection(toEdit.length());
	}

	protected CellEditor doCreateCellEditorOn(Composite composite) {
		ILabelDelegate label = (ILabelDelegate) getEditPart().getAdapter(
				ILabelDelegate.class);
		if (label != null && label.isTextWrapOn()) {
			int style = SWT.WRAP | SWT.MULTI;

			switch (label.getTextJustification()) {
			case PositionConstants.LEFT:
				style = style | SWT.LEAD;
				break;
			case PositionConstants.RIGHT:
				style = style | SWT.TRAIL;
				break;
			case PositionConstants.CENTER:
				style = style | SWT.CENTER;
				break;
			default:
				break;
			}
			// For the future: Provide highlighter with correct fonts
			StyledWrapTextCellEditor editor = new StyledWrapTextCellEditor(
					composite, style, eobj, featureID);
			Font font = getEditPart().getFigure().getFont();
			editor.getControl().setFont(font);
			return editor;
		} else {
			throw new IllegalStateException(
					"Error while trying to create a cell editor with a StyledText2.");
		}
	}
}
